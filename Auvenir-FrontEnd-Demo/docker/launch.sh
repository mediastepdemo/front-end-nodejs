#!/bin/bash

# -----------------------------------------------------------
# Auvenir Webapp Container Launch Script
# Maintained by: niall@auvenir.com
# -----------------------------------------------------------

# $1 - First Argument
#       The build environment [local/dev/qa/stage/prod]

# $2 - Second Argument
#       fast      - wipe database and redis, but do fast cached rebuilds
#       new       - wipe database and redis, build npms and frontend from scratch
#       npm       - force npm modules to rebuild
#       dev       - populate a dev account
#       test      - launch the test suite
#       debug     - start debug mode
#       container - start the container, without starting the app

# $3 - Third Argument
#       *         - used in conjunction with debug mode to set debug scope.



# -----------------------------------------------------------
# Configuration
# -----------------------------------------------------------

#FLAG FILES
FLAG_RUN_NPM_INSTALL="server/.flag_npm_install"
FLAG_RUN_DB="server/.flag_run_db"
FLAG_RUN_DEVELOPER="server/.flag_developer_run"
FLAG_RUN_QA="server/.flag_qa_run"
FLAG_RUN_GULP="server/.flag_gulp_build"
FLAG_WILDCARD="server/.flag*"

#ERROR CODE REPORTING
ERROR_CODE_FILE="server/.error_code"

#CERTIFICATES
CERT_TAR_BALL="localhost.tar.gz"
CERT_REMOTE_URL="https://s3.amazonaws.com/auvenir-config/localhost.tar.gz"

#ENVIRONMENT
export NODE_ENV=$1


# -----------------------------------------------------------
# Function Definitions
# -----------------------------------------------------------

# Build NPM dependencies
function build_npm {
    # When run locally remove shrinkwrap first so package.json updates are handled correctly
    # Remote installs should use the existing shrinkwrap file to build from
    [[ $1 == 'local' ]] && rm npm-shrinkwrap.json
    npm install
    # When run locally rebuild shrinkwrap to make sure it's up to date for remote installs
    [[ $1 == 'local' ]] && npm shrinkwrap
    sum=$(md5sum package.json | awk {'print $1'})
    echo "${sum}" > "${FLAG_RUN_NPM_INSTALL}"
}


# Fetch and extract the localhost certificates
function certs {
    WORKING_DIR=$(pwd)
    cd server/certs
    curl "${CERT_REMOTE_URL}" > "${CERT_TAR_BALL}"
    tar xvzf localhost.tar.gz
    cd "${WORKING_DIR}"
}


# -----------------------------------------------------------
# Begin
# -----------------------------------------------------------

# Rebuild NPMs and Frontend from scratch and clear redis and mongo
if [[ $2 == "new" ]]; then
    echo "Wiping the database, rebuilding all npm modules, rebuilding the frontend, running template scripts ..."
    rm -rf server/.flag*
    rm -rf .happypack
fi

# Use cached NPMs and Frontend, clear redis and mongo
if [[ $2 == "fast" ]]; then
    echo "Fast reset, using cached node_modules and cached front-end code, wiping redis and mongo ..."
    rm "${FLAG_RUN_DB}"
    rm -rf .happypack
fi

[[ ! -d server/certs ]] && mkdir server/certs
[[ ! -d server/data ]] && mkdir server/data
[[ ! -d server/files ]] && mkdir server/files

# Setup Self Signed Cert
if [[ $1 == "local" ]]; then
    if [[ ! -e "server/certs/${CERT_TAR_BALL}" ]]; then
        certs
    fi
fi

# The npm flag will force npm modules to rebuild
if [[ $2 == "npm" ]]; then
    rm -rf ${FLAG_RUN_NPM_INSTALL}
fi

# Setup Node Modules
if [[ ! -e "${FLAG_RUN_NPM_INSTALL}" ]]; then
    build_npm
else
    sum=$(md5sum package.json | awk {'print $1'})
    sum2=$(cat "${FLAG_RUN_NPM_INSTALL}")
    if [[ "${sum}" != "${sum2}" ]]; then
        build_npm
    fi
fi

# Populate a Dev Account
if [[ $1 == "dev" ]]; then
    if [[ ! -e "${FLAG_RUN_DEVELOPER}" ]]; then
        node scripts/create-developer.js matt@ooploo.com $1
        touch "${FLAG_RUN_DEVELOPER}"
    fi
fi

# Report Error Codes (For compatibility with the git desktop app, awkward workaround... but it works)
if [[ $2 == "test" ]]; then
    npm test
    echo $? > "${ERROR_CODE_FILE}"
    exit 0
fi

# Build the frontend
if [[ ! -e "${FLAG_RUN_GULP}" ]]; then
    npm run build
    touch "${FLAG_RUN_GULP}"
fi

# Startup debug mode for local
if [[ $2 == "debug" ]]; then
  if [[ -z $3 ]]; then
    echo "set DEBUG=auvenir-core:*"
    export DEBUG=auvenir-core:*
  else
    echo "set DEBUG=auvenir-core:*,${3}"
    export DEBUG=auvenir-core:*,$3
  fi
    npm run debug
else

  # Allow the container to launch without starting the app
  if [[ $2 == "container" ]]; then
    while true; do sleep 1; done
  else
    # Start the app normally
    npm start
  fi

fi

echo $? > "${ERROR_CODE_FILE}"
exit 0
