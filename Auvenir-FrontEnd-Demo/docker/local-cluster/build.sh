#!/bin/bash

# -----------------------------------------------------------
# Auvenir Webapp Local Cluster Build Script
# Maintained by: niall@auvenir.com
# -----------------------------------------------------------

# No arguments implemented.


# -----------------------------------------------------------
# Configuration
# -----------------------------------------------------------

# FLAG FILES
DOCKER_LOCATION_FILE="server/.docker_location"

# CONTAINER DETAILS
DOCKER_CONTAINER="webapp"
DOCKER_TAG="local"

# Default to interactive mode
OPTIONS='-it'

# SUPPORT CONTAINER VERSIONS
MONGO_VERSION="3.2"

# ERROR CODE REPORTING
ERROR_CODE_FILE="server/.error_code"


# -----------------------------------------------------------
# Begin
# -----------------------------------------------------------

# Ascend to the project root
while true; do
    if [[ ! -f Dockerfile ]]; then
      cd ..
  else
      break
  fi
done

# Import Shared Library
. docker/scripts/lib/auvenir.sh

# Ensure the Certs Folder Exists
if [[ ! -d server/certs ]]; then
    mkdir -p server/certs
fi


# Ensure the AWS-CONFIG File Gets Created
if [[ ! -f server/certs/aws-config.json ]]; then
    echo '{"accessKeyId": "unknown","secretAccessKey": "unknown","region": "us-east-1"}' > server/certs/aws-config.json
fi


# Install the npm-test Webhook
cp docker/local/pre-push.skel .git/hooks/pre-push
chmod -x .git/hooks/pre-push
chmod +x .git/hooks/pre-commit

# Wipe the Existing Code Container
wipe_docker_container "${DOCKER_CONTAINER}"

# The new command requires wiping containers, dist, and node_modules
[[ $1 == "new" ]] && wipe_docker_container mongo
[[ $1 == "new" ]] && wipe_docker_container redis
[[ $1 == "new" ]] && rm -rf dist
[[ $1 == "new" ]] && rm -rf node_modules

# The fast command wipes containers but uses cached node_modules, and dist
[[ $1 == "fast" ]] && wipe_docker_container mongo
[[ $1 == "fast" ]] && wipe_docker_container redis

# Build Code Image if Needed
check=$("${DOCKER_LOCATION}" images | grep ${DOCKER_CONTAINER} | grep ${DOCKER_TAG})
[[ -z ${check} ]] && "${DOCKER_LOCATION}" build --build-arg USER_ID=$(id -u) -f Dockerfile -t ${DOCKER_CONTAINER}:${DOCKER_TAG} .

# Activate tty on test mode for non-interactive git hooks
[[ $1 == 'test' ]] && OPTIONS='--tty'

# Ascend to the project root and copy the override
cp docker/local-cluster/override-env.json .

if [[ ! -d docker/local-cluster/ssl ]]; then
  cd docker/local-cluster
  ./mongo-certs.sh
  cd ../..
fi

cd docker/local-cluster
docker-compose up
docker-compose rm -f
cd ../..

# Ascend to the project root and remove the override
rm override-env.json

# Wipe Dangling Volumes
docker volume rm $(docker volume ls -qf dangling=true)
