Docker-Compose Production Simulation Environment
================================================

IMAGE WILL COME SOON

This environment contains:
  - A fully replicated mongo cluster
  - A dedicated redis instance
  - A single Auvenir app

This provides a comprehensive test environment to model what we're doing in AWS.<br>
To activate: ```$ docker/cluster-local/build.sh```

