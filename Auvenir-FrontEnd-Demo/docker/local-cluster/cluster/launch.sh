#!/bin/sh

# -----------------------------------------------------------
# Mongo Cluster Bootstrap script
# Maintained by: niall@auvenir.com
# -----------------------------------------------------------


bash -c 'sleep 10; mongo --ssl --sslPEMKeyFile /certs/mongodb.pem --sslCAFile /certs/CA-auvenir.pem --sslAllowInvalidHostnames admin /opt/setup.js; sleep 20 ;mongo --ssl --sslPEMKeyFile /certs/mongodb.pem --sslCAFile /certs/CA-auvenir.pem --sslAllowInvalidHostnames admin /opt/create-user.js' &
mongod -f /etc/mongod.conf.orig
