#!/bin/bash

FLAG_WILDCARD="server/.flag*"

# Ascend to the gitroot
while true; do
    if [[ ! -f Dockerfile ]]; then
	    cd ..
	else
	    break
	fi
done

# Import Shared Library
. docker/scripts/lib/auvenir.sh

# Wipe Flags
rm -rf ${FLAG_WILDCARD} 2> /dev/null

wipe_docker_container mongo
wipe_docker_container redis
wipe_docker_container webapp

# Docker Images 
wipe_docker_image webapp local

echo "All ready to rebuild."  

