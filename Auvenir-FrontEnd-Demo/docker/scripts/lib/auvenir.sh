#!/usr/bin/env bash

# Shared Auvenir Bash Functions
#
# Library Maintained by Niall Byrne <niall@auvenir.com>

DOCKER_LOCATION_FILE="server/.docker_location"

# Awkward encapsulation of the docker binary, for compatibility with the github desktop app
if [[ ! -f "${DOCKER_LOCATION_FILE}" ]]; then
    echo "$(which docker)" > "${DOCKER_LOCATION_FILE}"
    DOCKER_LOCATION="$(which docker)"
else
    DOCKER_LOCATION="$(cat server/.docker_location)"
fi


function wipe_docker_container {

    # $1 - The name of the container to wipe

    check=$("${DOCKER_LOCATION}" ps | grep "$1")
    [[ ! -z ${check} ]] && "${DOCKER_LOCATION}" kill "$1" > /dev/null && "${DOCKER_LOCATION}" rm "$1" > /dev/null
    check=$("${DOCKER_LOCATION}" ps -a | grep "$1")
    [[ ! -z ${check} ]] && "${DOCKER_LOCATION}" rm "$1" > /dev/null

    echo "Container Destroyed: <$1>"

}


function wipe_docker_image {

    # $1 - The image name to wipe
    # $2 - The image tag to wipe

    check=$("${DOCKER_LOCATION}" images | grep "$1" | grep "$2")
    [[ ! -z ${check} ]] && "${DOCKER_LOCATION}" rmi "$1":"$2"

    echo "Image Wiped: <$1:$2>"

}


function containerIsRunning {

    # $1 - The name of the container to check

    check=$("${DOCKER_LOCATION}" ps | grep "$1")
    [[ -z ${check} ]] && return 0
    return 1
}
