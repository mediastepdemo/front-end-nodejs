#!/bin/bash

case $1 in
all*)
  docker exec -it webapp /bin/bash -c "export DEBUG=seed-db && node ./scripts/seed-db.js | grep seed-db"
  ;;
user*)
  docker exec -it webapp /bin/bash -c "export {DEBUG=seed-db,SEED_DB_LEVEL=user} && node ./scripts/seed-db.js | grep seed-db"
  ;;
*)
  echo "You must specify an option: $ ./seed.sh [option]"
  echo "     all - seed users(3 clients and 2 auditors) engagements and conversations."
  echo "    user - only seed users"
  ;;
esac
