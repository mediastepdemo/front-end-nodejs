#!/usr/bin/env bash

case $1 in
backup*)
  docker exec -it mongo /bin/bash -c "mongodump -d auvenir"
  ;;
restore*)
  docker exec -it mongo /bin/bash -c "mongorestore -d auvenir dump/auvenir"
  ;;
*)
  echo "You must specify an option: $ ./database.sh [option]"
  echo "     backup  - backup an existing mongo database."
  echo "     restore - restore a backed up database."
  ;;
esac
