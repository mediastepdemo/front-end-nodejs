Dockerized Auvenir
==================

This is a dockerized version of the Auvenir application, that works seamlessly with files hosted on your local host machine.


# Scripts and Shortcuts 

1. Re-Install npm modules:<br>
   ```$ docker/local/build.sh npm```

2. Reset everything, build from scratch, no cached build data:<br>
    ```$ docker/local/build.sh new```

3. Consoles:<br>
    ```$ docker/scripts/shell [mongo|node|redis]```

# How to Install


### Local Installs (OSX)

Wait until you are about to start your next branch.  Pull down dev, and create your work branch.

IMPORTANT!<br>
Shutdown your local mongo and redis instances first.  They will interfere with the install.

Now you can setup your docker environment:

1. Install docker for OSX from [here](https://download.docker.com/mac/stable/Docker.dmg).
2. cp the 'aws-config.json' file into git repo
3. ```$ docker/local/build.sh``` Don't use sudo please, it will create problems.

The first time this runs, it will take almost 10 minutes to finish.  Subsequent builds will be shorter.

### Local Installs (Linux)

These instructions are for Ubuntu 14.04 or later.<br>
Do not use the root account to run the application, use a regular admin user.<br>

1. Install AUFS for Ubuntu:
    ```
        sudo apt-get -y install linux-image-extra-$(uname -r)
        sudo modprobe aufs
    ```
    
2. Install Docker
    ```
        sudo curl https://get.docker.com/ | sh
        sudo usermod -aG docker $(whoami)
    ```

3. Logout and then login to pickup the docker group as your current user.

4. Start Auvenir:<br>
   ```
        $ docker/local/build.sh
   ```
