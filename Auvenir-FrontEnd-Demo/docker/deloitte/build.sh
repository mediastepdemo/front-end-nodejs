#!/bin/bash

container="auvenir"
tag="deloitte"

mongo_ver="3.2"

# Ascend to the gitroot
while true; do
    if [[ ! -f Dockerfile ]]; then
	    cd ..
	else
	    break
	fi
done

# Build If Needed
check=$(docker images | grep ${container} | grep ${tag})
[[ -z ${check} ]] &&  docker build --build-arg USER_ID=$(id -u) -f Dockerfile -t ${container}:${tag} .

# Start Local Database if Needed
check=$(docker ps | grep mongo)
[[ -z ${check} ]] &&  docker run -p 27017:27017 -d --name mongo mongo:${mongo_ver}

# Start Local Session Store if Needed
check=$(docker ps | grep redis)
[[ -z ${check} ]] &&  docker run -p 6379:6379 --name redis -d redis redis-server --appendonly yes

# Start Local Auvenir Instance with Path Mappings
docker run \
    -it \
    --name auvenir \
    --link mongo:mongo \
    --link redis:redis \
    -v "$(pwd)/server":/server \
    -p 443:5443 \
    -p 80:5080 \
    ${container}:${tag} \
    /bin/bash -c "docker/launch.sh $tag"

# Clean up Docker Images, to prevent images piling up
for c in $(docker images | grep none | awk '{ print $3 }' | grep -v IMAGE); do docker rmi $c 2> /dev/null; done
for c in $(docker ps -a | awk '{ print $1 }' | grep -v CONTAINER); do docker rm $c 2> /dev/null; done
