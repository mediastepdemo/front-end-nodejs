#!/bin/bash

# FLAG FILES
DOCKER_LOCATION_FILE="server/.docker_location"

# CONTAINER DETAILS
DOCKER_CONTAINER="webapp"
DOCKER_TAG="dev"

# Default to interactive mode
OPTIONS='-it'

# SUPPORT CONTAINER VERSIONS
MONGO_VERSION="3.2"

#ERROR CODE REPORTING
ERROR_CODE_FILE="server/.error_code"



# Awkward encapsulation of the docker binary, for compatibility with the github desktop app
if [[ ! -f "${DOCKER_LOCATION_FILE}" ]]; then
    echo "$(which docker)" > "${DOCKER_LOCATION_FILE}"
    DOCKER_LOCATION="$(which docker)"
else
    DOCKER_LOCATION="$(cat server/.docker_location)"
fi

# Ascend to the project root
while true; do
    if [[ ! -f Dockerfile ]]; then
	    cd ..
	else
	    break
	fi
done

# Import Shared Library
. docker/scripts/lib/auvenir.sh

# Ensure the Certs Folder Exists
if [[ ! -d server/certs ]]; then
    mkdir -p server/certs
fi

# Install the npm-test webhook
cp docker/local/pre-push.skel .git/hooks/pre-push
chmod +x .git/hooks/pre-push

wipe_docker_container "${DOCKER_CONTAINER}"

# Build If Needed
check=$("${DOCKER_LOCATION}" images | grep ${DOCKER_CONTAINER} | grep ${DOCKER_TAG})
[[ -z ${check} ]] && "${DOCKER_LOCATION}" build --build-arg USER_ID=$(id -u) -f Dockerfile -t ${DOCKER_CONTAINER}:${DOCKER_TAG} .

# Start Local Database if Needed
check=$("${DOCKER_LOCATION}" ps | grep mongo)
[[ -z ${check} ]] &&  "${DOCKER_LOCATION}" run -p 27017:27017 -d --name mongo mongo:${MONGO_VERSION}

# Start Local Session Store if Needed
check=$("${DOCKER_LOCATION}" ps | grep redis)
[[ -z ${check} ]] && "${DOCKER_LOCATION}" run -p 6379:6379 --name redis -d redis redis-server --appendonly yes

# Activate tty on test mode for non-interactive git hooks
[[ $1 == 'test' ]] && OPTIONS='--tty'

# Start Local Auvenir Instance with Path Mappings
${DOCKER_LOCATION} run \
    ${OPTIONS} \
    --name ${DOCKER_CONTAINER} \
    --link mongo:mongo \
    --link redis:redis \
    -v "$(pwd)/server":/server \
    -v "$(pwd)":/home/webservice/Auvenir \
    -p 443:5443 \
    -p 80:5080 \
    "${DOCKER_CONTAINER}:${DOCKER_TAG}" \
    /bin/bash -c "docker/launch.sh "${DOCKER_TAG}" $1"

# Clean up Docker Images, to prevent images piling up
for c in $("${DOCKER_LOCATION}" images | grep none | awk '{ print $3 }' | grep -v IMAGE); do "${DOCKER_LOCATION}" rmi "${c}" 2> /dev/null; done
for c in $("${DOCKER_LOCATION}" ps -a | awk '{ print $1 }' | grep -v CONTAINER); do "${DOCKER_LOCATION}" rm "${c}" 2> /dev/null; done

[[ -f "${ERROR_CODE_FILE}" ]] && ec=$(cat "${ERROR_CODE_FILE}") && rm "${ERROR_CODE_FILE}" && exit ${ec}
exit 1
