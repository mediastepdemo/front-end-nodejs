#!/bin/bash

# -----------------------------------------------------------
# Auvenir Webapp Local Build Script
# Maintained by: niall@auvenir.com
# -----------------------------------------------------------

# $1 - First Argument
#       container - start the container, without starting the app
#       debug     - start debug mode
#       dev       - populate a dev account
#       fast      - wipe database and redis, but do fast cached rebuilds
#       new       - wipe database and redis, build npms and frontend from scratch
#       npm       - force npm modules to rebuild
#       test      - launch the test suite

# $2 - Second Argument
#       *         - used in conjunction with debug mode to set debug scope.


# -----------------------------------------------------------
# Configuration
# -----------------------------------------------------------

# FLAG FILES
DOCKER_LOCATION_FILE="server/.docker_location"

# CONTAINER DETAILS
DOCKER_CONTAINER="webapp"
DOCKER_TAG="local"

# Default to interactive mode
OPTIONS='-it'

# SUPPORT CONTAINER VERSIONS
MONGO_VERSION="3.2"

# ERROR CODE REPORTING
ERROR_CODE_FILE="server/.error_code"


# -----------------------------------------------------------
# Begin
# -----------------------------------------------------------

# Ascend to the project root
while true; do
    if [[ ! -f Dockerfile ]]; then
      cd ..
  else
      break
  fi
done

# Import Shared Library
. docker/scripts/lib/auvenir.sh

# Ensure the Certs Folder Exists
if [[ ! -d server/certs ]]; then
    mkdir -p server/certs
fi


# Ensure the AWS-CONFIG File Gets Created
if [[ ! -f server/certs/aws-config.json ]]; then
    echo '{"accessKeyId": "unknown","secretAccessKey": "unknown","region": "us-east-1"}' > server/certs/aws-config.json
fi


# Wipe the Existing Code Container
wipe_docker_container "${DOCKER_CONTAINER}"

# The new command requires wiping containers, dist, and node_modules
[[ $1 == "new" ]] && wipe_docker_container mongo
[[ $1 == "new" ]] && wipe_docker_container redis
[[ $1 == "new" ]] && rm -rf dist
[[ $1 == "new" ]] && rm -rf node_modules

# The fast command wipes containers but uses cached node_modules, and dist
[[ $1 == "fast" ]] && wipe_docker_container mongo
[[ $1 == "fast" ]] && wipe_docker_container redis

# Build Code Image if Needed
check=$("${DOCKER_LOCATION}" images | grep ${DOCKER_CONTAINER} | grep ${DOCKER_TAG})
[[ -z ${check} ]] && "${DOCKER_LOCATION}" build --build-arg USER_ID=$(id -u) -f Dockerfile -t ${DOCKER_CONTAINER}:${DOCKER_TAG} .

# Start Local Database if Needed
check=$("${DOCKER_LOCATION}" ps | grep mongo 2> /dev/null)
[[ -z ${check} ]] &&  "${DOCKER_LOCATION}" run -p 27017:27017 -d -v $(pwd)/dump:/dump --name mongo mongo:${MONGO_VERSION} > /dev/null && echo "Container Started: <mongo>"

# Start Local Session Store if Needed
check=$("${DOCKER_LOCATION}" ps | grep redis 2> /dev/null)
[[ -z ${check} ]] && "${DOCKER_LOCATION}" run -p 6379:6379 --name redis -d redis redis-server --appendonly yes > /dev/null && echo "Container Started: <redis>"

# Activate tty on test mode for non-interactive git hooks
[[ $1 == 'test' ]] && OPTIONS='--tty'

# Start Local Auvenir Instance with Path Mappings
${DOCKER_LOCATION} run \
    ${OPTIONS} \
    --name ${DOCKER_CONTAINER} \
    --link mongo:mongo \
    --link redis:redis \
    -v "${HOME}/.ssh/id_rsa":/home/webservice/.ssh/id_rsa \
    -v "$(pwd)/server":/server \
    -v "$(pwd)":/home/webservice/Auvenir \
    -p 443:5443 \
    -p 80:5080 \
    -p 9229:9229 \
    "${DOCKER_CONTAINER}:${DOCKER_TAG}" \
    /bin/bash -c "docker/launch.sh "${DOCKER_TAG}" $1 $2"

# Clean up Docker Images, to prevent images piling up
for c in $("${DOCKER_LOCATION}" images | grep none | awk '{ print $3 }' | grep -v IMAGE); do "${DOCKER_LOCATION}" rmi "${c}" 2> /dev/null; done
for c in $("${DOCKER_LOCATION}" ps -a | awk '{ print $1 }' | grep -v CONTAINER); do "${DOCKER_LOCATION}" rm "${c}" 2> /dev/null; done

# Wipe Dangling Volumes
docker volume rm $(docker volume ls -qf dangling=true)

[[ -f "${ERROR_CODE_FILE}" ]] && ec=$(cat "${ERROR_CODE_FILE}") && rm "${ERROR_CODE_FILE}" && exit ${ec}
exit 1
