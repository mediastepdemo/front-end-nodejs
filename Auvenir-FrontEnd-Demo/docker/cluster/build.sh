#!/bin/bash

container="auvenir"
tag="local"

# Ascend to the gitroot
while true; do
    if [[ ! -f Dockerfile ]]; then
	    cd ..
	else
	    break
	fi
done

# Create certs folder
if [[ ! -d server/certs ]]; then
    mkdir -p server/certs
fi

docker-compose down
docker-compose rm

# Store the location of the docker binary
if [[ ! -f server/.docker_location ]]; then
    echo "$(which docker)" > server/.docker_location
    docker_location=$(which docker)
else
    docker_location=$(cat server/.docker_location)
fi

# Stop If Needed
check=$(${docker_location} ps | grep auvenir)
[[ ! -z ${check} ]] && ${docker_location} kill auvenir

# Clean If Needed
check=$(${docker_location} ps -a | grep auvenir)
[[ ! -z ${check} ]] && ${docker_location} rm auvenir

# Build If Needed
check=$(${docker_location} images | grep ${container} | grep ${tag})
[[ -z ${check} ]] &&  ${docker_location} build --build-arg USER_ID=$(id -u) -f Dockerfile -t ${container}:${tag} .

# Start Local Database if Needed
check=$(${docker_location} ps | grep mongo)
check2=$(${docker_location} ps -a | grep mongo)
echo $check
echo $check2
[[ ! -z ${check} ]] && [[ -z ${check2} ]] && ${docker_location} rm mongo
[[ ! -z ${check} ]] && [[ ! -z ${check2} ]] && ${docker_location} kill mongo && ${docker_location} rm mongo

# Start Local Session Store if Needed
check=$(${docker_location} ps | grep redis)
check2=$(${docker_location} ps -a | grep redis)
[[ ! -z ${check} ]] && [[ -z ${check2} ]] && ${docker_location} rm redis
[[ ! -z ${check} ]] && [[ ! -z ${check2} ]] && ${docker_location} kill redis && ${docker_location} rm redis

docker-compose up

# Clean up Docker Images, to prevent images piling up
for c in $(docker images | grep none | awk '{ print $3 }' | grep -v IMAGE); do docker rmi $c 2> /dev/null; done
for c in $(docker ps -a | awk '{ print $1 }' | grep -v CONTAINER); do docker rm $c 2> /dev/null; done
