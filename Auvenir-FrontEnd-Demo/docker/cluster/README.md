Docker-Compose Production Simulation Environment
================================================

![Overview](overview.jpg)

This environment contains:
  - A fully replicated mongo cluster
  - A dedicated redis instance
  - 2 Auvenir Webapps
  - A loadbalancer

This provides a comprehensive test environment to model what we're doing in AWS.<br>
To activate: ```$ docker/cluster/build.sh```

