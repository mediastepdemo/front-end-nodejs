const Models = require('../../models')
const Utility = require('../../plugins/utilities/utility')
const debug = require('debug')('auvenir:event:add-team-member')
const async = require('async')
const Code = require('../../plugins/utilities/code')

var AddMultipleTeamMembers = function (data, callback) {
  debug('Executing add-team-member event')
  const {userInfo, engagementID} = data

  if (!userInfo) {
    return callback({code: 1, msg: 'Missing user information'})
  }

  if (!engagementID) {
    return callback({code: 1, msg: 'Missing engagement id'})
  }
  let isNewUser = false

  const findUser = (cbk) => {
    if (userInfo._id) {
      isNewUser = false
      // TODO: Check if userInfo has the same properties as User schema
      cbk(null, {user: userInfo, userId: userInfo._id})
    } else if (userInfo.email) {
      Models.Auvenir.Engagement.findById(engagementID, function (err, engagement) {
        if (err) {
          return cbk({code: 500, msg: 'Error on retrieving engagement'})
        }
        if (!engagement) {
          return cbk({code: 404, msg: 'Unable to find engagement information'})
        }
        Utility.findUserAccountInEngagement(userInfo.email, engagement._id, function (err, result) {
          if (err) {
            return cbk({code: 500, msg: 'Error on finding existing user'})
          }

          if (result.isInsideEngagement) {
            cbk({code: Code.ERROR_GENERAL, msg: 'User already exists within the specific engagement.'})
            return
          }
          if (result.user && result.user.type !== 'AUDITOR') {
            cbk({code: Code.ERROR_GENERAL, msg: `The email associated with this user is a ${result.user.type}.`})
            return
          }
          if (result.user) {
            isNewUser = false
            cbk(null, {user: result.user, userId: result.user._id})
            return
          } else {
            isNewUser = true
            cbk(null, {})
            return
          }
        })
      })
    }
  }
  const addUserToEngagement = ({user, userId}, cbk) => {
    if (isNewUser) {
      cbk(null, {})
      return
    }

    Utility.addUserToEngagement(userId, engagementID, false, function (err, result) {
      if (err) {
        return cbk(err)
      } else {
        if (!result.updatedEngagement) {
          return cbk({code: 404, msg: `Cannot find engagement with id ${result.updatedEngagement}`})
        } else {
          let engagement = result.updatedEngagement
          cbk(null, {user, engagement})
        }
      }
    })
  }

  const fetchAdditionalInfo = ({user, engagement}, cbk) => {
    if (isNewUser) {
      return cbk(null, {})
    }

    engagement.fetchAdditionalInfo((err, data) => {
      if (err) {
        return cbk(err)
      }
      cbk(err, {user: user, engagement: data})
    })
  }

  async.waterfall([findUser, addUserToEngagement, fetchAdditionalInfo], (err, result) => {
    if (err) {
      if (err.code && err.msg) {
        return callback(err)
      }
      return callback({code: 500, msg: 'Error on adding user to team.'})
    }
    let user
    if (!isNewUser) {
      user = result.user
    } else {
      user = userInfo
    }
    callback({code: 0, engagement: result.engagement, user: user, isNewUser: isNewUser})
  })
}

module.exports = AddMultipleTeamMembers
