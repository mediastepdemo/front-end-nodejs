const Models = require('../../models')
const Utility = require('../../plugins/utilities/utility')
const _ = require('lodash')

/**
 * Responsible for Socket Event Add Integration.
 * Update user db by adding integration info.
 */
var AddIntegration = function (data, callback) {
  const { name, uid, userID } = data

  if (!name || !_.isString(name)) {
    return callback({ code: 1, msg: 'Invalid integration was passed.' })
  }
  if (!uid || !_.isString(uid)) {
    return callback({ code: 1, msg: 'Invalid gdrive uid info was passed.' })
  }
  if (!userID || !_.isString(userID)) {
    return callback({ code: 1, msg: 'Invalid user info was passed.'})
  }

  Models.Auvenir.User.findOne({ _id: Utility.castToObjectId(userID) }, (err, oneUser) => {
    if (err) {
      return callback({ code: 1, msg: 'ERROR while finding user from db.'})
    }
    if (!oneUser) {
      return callback({ code: 1, msg: 'No business was found.'})
    }

    if (oneUser.integrations.length > 0) {
      for (let i = 0; i < oneUser.integrations.length; i++) {
        if (oneUser.integrations[i].uid === uid && oneUser.integrations[i].name === name) {
          return callback({ code: 0, msg: 'User already has setup this integration.'})
        }
      }
    }

    let newIntegration = {
      name: name,
      uid: uid
    }

    oneUser.integrations.push(newIntegration)
    oneUser.save((err) => {
      if (err) {
        return callback({ code: 1, msg: 'ERROR while saving new integration info into db.'})
      }

      return callback({ code: 0, msg: 'Saving new integration was Successful.'})
    })
  })
}

module.exports = AddIntegration
