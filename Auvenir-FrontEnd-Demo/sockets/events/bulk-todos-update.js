var Utility = require('../../plugins/utilities/utility')

var BulkTodosUpdate = function (data, callback) {
  if (!data.currentUserID) {
    callback({code: 1, msg: 'Current User is not defined.'})
    return
  }

  if (!data.engagementID) {
    callback({code: 1, msg: 'Missing Engagement ID'})
    return
  }

  Utility.bulkTodosUpdate(data.data, data.currentUserID, data.engagementID, function (err, result) {
    if (err) {
      callback(err)
    } else {
      callback({code: 0, result: result})
    }
  })
}

module.exports = BulkTodosUpdate
