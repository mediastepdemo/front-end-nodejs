const Models = require('../../models')
const Utility = require('../../plugins/utilities/utility')
const debug = require('debug')('auvenir:event:add-team-member')
const async = require('async')
const Code = require('../../plugins/utilities/code')

var SetUserPermission = function (data, callback) {
  const {engagementID, userID, role} = data

  if (!engagementID) {
    return callback({code: 1, msg: 'Missing engagement id'})
  }

  if (!userID) {
    return callback({code: 1, msg: 'Missing user id'})
  }

  if (role) {
    return callback({code: 1, msg: 'Missing role information'})
  }

  const updateUserPermission = (cbk) => {
    Models.Auvenir.Engagement.findOne({_id: Utility.castToObjectId(engagementID)}, function (err, engagement) {
      if (err) {
        return cbk({code: 500, msg: 'Error on retrieving engagement'})
      }
      if (!engagement) {
        return cbk({code: 404, msg: 'Unable to find engagement information'})
      }
      const engagementIndex = engagement.acl.findIndex((eng) => {
        return eng._id === engagementID
      })
      if (engagmentIndex === -1) {
        return cbk({code: 404, msg: 'User does not exist in specific engagement'})
      } else {
        engagement.acl[engagementIndex].isLead = (role === 'Lead')
        engagement.save(function (err, eng) {
          if (err) {
            return cbk({code: 500, msg: 'Error on updating user permission'})
          }
          return cbk(null, eng)
        })
      }
    })
  }

  const fetchAdditionalInfo = (engagement, cbk) => {
    engagement.fetchAdditionalInfo((err, data) => {
      if (err) {
        return cbk(err)
      }
      cbk(err, data)
    })
  }

  async.waterfall([updateUserPermission, fetchAdditionalInfo], (err, result) => {
    if (err) {
      if (err.code && err.msg) {
        return callback(err)
      }
      return callback({code: 500, msg: 'Cannot update user permission in engagement'})
    }

    callback({code: 0, engagement: result})
  })
}

module.exports = SetUserPermission
