var Models = require('../../models')
var Utility = require('../../plugins/utilities/utility')
var async = require('async')
// TODO - fix this file when working on file-manager page
//      - some request related stuff is included here
var UpdateFile = function (data, callback) {
  if (!data.currentUserID) {
    callback({code: 1, msg: 'Current User is not defined on request.'})
    return
  }

  var fileID = Utility.castToObjectId(data.fileID)
  var sortedByUserID = Utility.castToObjectId(data.sortedBy)
  var newRequestID = Utility.castToObjectId(data.newRequestID)
  var previousRequestID
  var engagementID = Utility.castToObjectId(data.engagementID)
  if (data.previousRequestID) {
    previousRequestID = Utility.castToObjectId(data.previousRequestID)
  }

  if (!fileID) {
    return callback({code: 1, msg: 'No File ID is given.'})
  }
  if (!sortedByUserID) {
    return callback({code: 1, msg: 'No Sorted By User ID Given'})
  }
  function removeFileFromPreviousRequest (cbAsync) {
    if (previousRequestID) {
      Models.Auvenir.Request.update({engagementID: engagementID, items: fileID}, {$pull: {items: fileID}}, {multi: true}, function (err, res) {
        if (err) {
          cbAsync(err)
        } else {
          cbAsync(null)
        }
      })
    } else {
      // IF we don't have previous Request, Do not need to pull fileID from previous Request's items.
      cbAsync(null)
    }
  }

  function addFileToNewRequest (cbAsync) {
    Models.Auvenir.Request.findOneAndUpdate({_id: newRequestID}, {$push: {items: fileID}}, function (err, res) {
      if (err) {
        cbAsync(err)
      } else {
        cbAsync(null)
      }
    })
  }

  function updateFile (cbAsync) {
    Models.Auvenir.File.findOneAndUpdate({_id: fileID}, {$set: {sortedBy: sortedByUserID}}, function (err, updatedFile) {
      if (err) {
        cbAsync({code: 1, msg: 'Error on finding Sorted By File Object'})
      } else {
        if (updatedFile) {
          var engagementID = data.engagementID
          var userID = data.currentUserID

          let activityData = {}
          activityData.currentUserID = userID
          activityData.engagementID = engagementID
          activityData.operation = 'UPDATE'
          activityData.type = 'File'
          activityData.original = updatedFile.toObject()
          activityData.updated = {requestID: newRequestID.toString()}

          /* var original = updatedFile.toObject()
          var update = {requestID: newRequestID.toString()}
          var typeCrud = 'UPDATE'
          var typeActivity = 'File'

          Utility.createActivity(userID, engagementID, typeCrud, typeActivity, original, update, function (err, res) {
            if (err) {
              callback({name: 'create-activity', code: 1, msg: 'Error on Creating Activity.'})
            } else {
              callback({name: 'create-activity', code: 0, result: res.result})
            }
          }) */
          cbAsync(null, updatedFile.path, updatedFile, activityData)
        } else {
          cbAsync({code: 1, msg: 'No file to update.'})
        }
      }
    })
  }
  function asyncComplete (err, filePath, updatedFile, activityData) {
    if (err) {
      callback(err)
    } else {
      var result = {}
      result.newRequestID = newRequestID.toString()
      result.fileID = fileID
      result.path = filePath
      result.file = updatedFile
      if (previousRequestID) {
        result.previousRequestID = previousRequestID.toString()
      }
      Models.Auvenir.User.findOne({_id: sortedByUserID}, function (err, oneUser) {
        if (err) {
          callback({code: 1, msg: 'Error on finding Sorted By User Object'})
        }
        if (data.successMsg) {
          result.successMsg = updatedFile.name + ' moved to '
        }
        result.sortedBy = {}
        result.sortedBy.id = oneUser._id
        result.sortedBy.firstName = oneUser.firstName
        result.activityData = activityData

        callback({code: 0, result: result})
      })
    }
  }
  async.waterfall([
    removeFileFromPreviousRequest,
    addFileToNewRequest,
    updateFile
  ], asyncComplete)
}
module.exports = UpdateFile
