const Models = require('../../models')
const async = require('async')

var getEngagements = function ({userId, userRole}, callback) {
  if (!userId) {
    callback({code: 1, msg: 'User Id is not defined.'})
  }
  if (!userRole) {
    callback({code: 1, msg: 'User Role is not defined.'})
  }

  Models.Auvenir.Engagement.findByAclUser({id: userId, role: userRole}, function (err, engagements) {
    if (err) {
      return callback(err)
    }
    async.map(engagements, function (engagement, cbk) {
      engagement.fetchAdditionalInfo((err, engagementObj) => {
        cbk(err, engagementObj)
      })
    }, function (err, result) {
      if (err) {
        return callback(err)
      }
      callback({code: 0, engagements: result})
    })
  })
}

module.exports = getEngagements
