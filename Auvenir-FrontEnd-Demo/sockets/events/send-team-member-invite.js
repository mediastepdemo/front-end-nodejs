const Models = require('../../models')
const Utility = require('../../plugins/utilities/utility')
const debug = require('debug')('auvenir:event:send-team-member-invite')
const async = require('async')
const Code = require('../../plugins/utilities/code')

var SendTeamMemberInvite = function ({engagementId, currentUser, teamMember}, callback) {
  debug('Executing send-team-member-invite event')

  if (!engagementId) {
    return callback({code: 1, msg: 'Missing engagement id'})
  }

  if (!currentUser) {
    return callback({code: 1, msg: 'Missing current user information'})
  }

  if (!teamMember) {
    return callback({code: 1, msg: 'Missing team member information'})
  }

  if (!teamMember.email) {
    return callback({code: 1, msg: 'Missing team member email to send the invitation'})
  }

  if (!Utility.isEmailValid(teamMember.email)) {
    return callback({code: 1, msg: 'Destination email address is invalid'})
  }

  teamMember.type = currentUser.type
  teamMember.status = 'ONBOARDING'
  const findEngagementById = (cbk) => {
    Models.Auvenir.Engagement.findById(engagementId, (err, engagement) => {
      if (err) {
        return cbk({code: 500, msg: 'Error on retrieving engagement'})
      }
      if (!engagement) {
        return cbk({code: 404, msg: 'Unable to find engagement information'})
      }

      let userInAcl = engagement.acl.findIndex((acl) => {
        return (acl.role === currentUser.type) && (acl.id.toString() === currentUser._id)
      })

      if (!~userInAcl) {
        return cbk({code: 403, msg: `${currentUser.type} with id ${currentUser._id} has no permission with engagement ${engagement._id}`})
      }

      cbk(null, engagement)
    })
  }

  const createOrGetUser = (engagement, cbk) => {
    Utility.findUserAccountInEngagement(teamMember.email, engagement._id, function (err, result) {
      if (err) {
        return cbk({code: 500, msg: 'Error on finding existing user'})
      }
      // Check if user exists within engagement given the email.
      // If not, a new user will be created.
      if (result.isInsideEngagement) {
        cbk({code: Code.ERROR_GENERAL, msg: 'User already exists within the specific engagement.'})
        return
      }
      // TODO - This is breaking the server
      // if (result.user.type !== currentUser.type) {
      //   cbk({code: Code.ERROR_GENERAL, msg: `The email associated with this user is a ${result.user.type}.`})
      //   return
      // }
      if (result.user) {
        cbk(null, {newMember: result.user, engagement, isNewUser: false})
      } else {
        Utility.createUser(teamMember, function (err, newMember) {
          if (err) {
            return cbk({code: 500, msg: 'Error on creating user'})
          }
          if (!newMember) {
            return cbk({code: 404, msg: 'Unable to create user'})
          }
          let emailType = 'sendEngagementInvite'
          cbk(null, {newMember, engagement, isNewUser: true})
          return
        })
      }
    })
  }

  const ensureTeamMember = ({newMember, engagement, isNewUser}, cbk) => {
    Models.Auvenir.User.findOne({_id: newMember._id}, (err, user) => {
      if (err) {
        return cbk({code: 500, msg: 'Error on retrieving user'})
      }
      if (!user) {
        return cbk({code: 404, msg: `cannot find user with id: ${newMember._id}`})
      }
      let token = Utility.secureRandomString(32)

      if (!user.auth) {
        user.auth = {}
      }
      let expiryTime = Date.now() + Utility.SESSION_LENGTH
      user.auth = Object.assign({}, user.auth, {
        access: {
          token: Utility.createHash(token),
          expires: expiryTime
        }
      })
      return user.save((err, user) => {
        if (err) {
          cbk({code: 500, msg: 'Error on updating user'})
          return
        }
        debug('user updated', user)
        cbk(null, {user, engagement, token, isNewUser})
        return
      })
    })
  }

  const addUserToEngagement = ({user, engagement, token, isNewUser}, cbk) => {
    Utility.addUserToEngagement(user._id, engagement._id, false, function (err, result) {
      if (err) {
        return cbk(err)
      } else {
        if (!result.updatedEngagement) {
          return cbk({code: 404, msg: `Cannot find engagement with id ${result.updatedEngagement}`})
        } else {
          let updatedEngagement = result.updatedEngagement
          cbk(null, {user, engagement: updatedEngagement, token, isNewUser})
        }
      }
    })
  }

  const fetchAdditionalInfo = ({user, engagement, token, isNewUser}, cbk) => {
    engagement.fetchAdditionalInfo((err, data) => {
      if (err) {
        return cbk(err)
      }
      cbk(err, {user, engagement: data, token, isNewUser})
    })
  }

  const sendTeamInvite = ({user, engagement, token, isNewUser}, cbk) => {
    let inviterLastNameLetter = ''
    inviterLastNameLetter = (currentUser.lastName && currentUser.lastName.trim()) ? currentUser.lastName.trim().charAt(0) : ''

    let status = ''
    if (user.type === 'AUDITOR') {
      status = 'sendEngagementInviteGeneralAuditor'
    } else {
      if (isNewUser) {
        status = 'sendEngagementInviteGeneralClient'
      } else {
        status = 'sendEngagementNotifyGeneralClient'
      }
    }
    Utility.sendEmail({
      fromEmail: 'andi@auvenir.com',
      toEmail: user.email,
      toName: `${user.firstName} ${user.lastName}`,
      status: status,
      customValues: {
        token: token,
        email: user.email,
        engagementID: engagement._id,
        engagementName: engagement.name,
        inviteeFirstName: user.firstName,
        inviteeLastName: user.lastName,
        inviterFirstName: currentUser.firstName,
        inviterLastName: inviterLastNameLetter
      }
    }, function (err, result) {
      debug(`Invitation send Result ${JSON.stringify(result)}`)
      let activityData = {}
      activityData.engagementID = engagement._id
      activityData.operation = 'INVITE'
      activityData.type = 'User'
      activityData.original = {}
      activityData.updated = user
      cbk(err, {engagement, newMember: user, activityData})
    })
  }

  async.waterfall([findEngagementById, createOrGetUser, ensureTeamMember, addUserToEngagement, fetchAdditionalInfo, sendTeamInvite], (err, result) => {
    if (err) {
      if (err.code && err.msg) {
        return callback(err)
      }
      return callback({code: 500, msg: 'Error on sending team invitation.'})
    }
    callback({code: 0, engagement: result.engagement, newMember: result.newMember, activityData: result.activityData})
  })
}

module.exports = SendTeamMemberInvite
