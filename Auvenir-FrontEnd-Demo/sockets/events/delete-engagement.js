const Models = require('../../models')
const debug = require('debug')('auvenir:event:delete-engagement')
var Utility = require('../../plugins/utilities/utility')

var DeleteEngagement = function (data, callback) {
  debug('Executing delete-engagement event')
  const {businessID, engagementID} = data

  if (!engagementID) {
    return callback({code: 1, msg: 'Missing engagement id information'})
  }

  if (businessID) {
    Models.Auvenir.Business.findOneAndRemove({_id: businessID}, function (err) {
      if (err) {
        return callback({code: 500, msg: 'Error on removing business.'})
      }

      Models.Auvenir.Engagement.findOneAndRemove({_id: engagementID}, function (err) {
        if (err) {
          return callback({code: 500, msg: 'Error on removing engagement.'})
        }

        return callback({code: 0, engagementID: engagementID})
      })
    })
  } else {
    Models.Auvenir.Engagement.findOneAndRemove({_id: engagementID}, function (err) {
      if (err) {
        return callback({code: 500, msg: 'Error on removing engagement.'})
      }

      return callback({code: 0, engagementID: engagementID})
    })
  }
}

module.exports = DeleteEngagement
