/**
 * Find all user information by their ID
 */
const Models = require('../../models')
const Utility = require('../../plugins/utilities/utility')
const async = require('async')
const debug = require('debug')('auvenir:event:find-contacts')

const FindContacts = (clients, cbk) => {
  var userArray = []
  // for (var i = 0; i > clients.length; i++) {
  for (var i = 0; i < clients.length; i++) {
    Models.Auvenir.User.findById(clients[i].id, (err, user) => {
      if (err) {
        return cbk(err)
      }
      if (!user) {
        return cbk({code: 404, msg: 'Cannot find user'})
      }
      userArray.push(user)
      if (i === clients.length) {
        cbk({code: 0, users: userArray})
      }
    })
  }
}

module.exports = FindContacts
