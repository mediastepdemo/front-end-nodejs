var Utility = require('../../plugins/utilities/utility')
const initLogger = require('../../plugins/logger/logger').init
const { info, warn, error } = initLogger(__filename)

var CreateActivity = function (data, callback) {
  var engagementID = data.engagementID
  var currentUserID = data.currentUserID
  var operation = data.operation
  var type = data.type
  if (!currentUserID) {
    return callback({code: 1, msg: 'CurrentUserId is not available'})
  }
  if (!engagementID) {
    return callback({code: 2, msg: 'engagementID is not available'})
  }
  if (!operation) {
    return callback({code: 3, msg: 'operation is not available'})
  }
  warn('Inside of CreateActivity. showing data >>> ')
  warn(data)
  Utility.createActivity(currentUserID, data.engagementID, operation,
    type, data.original, data.updated, function (err, res) {
      if (err) {
        callback({code: 4, msg: err.msg})
      } else {
        callback({code: 0, result: res.result})
      }
    }
  )
}

module.exports = CreateActivity
