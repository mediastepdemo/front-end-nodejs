var Models = require('../../models')
var Utility = require('../../plugins/utilities/utility')
var debug = require('debug')('auvenir:event:deactivate-account')

var DeactivateAccount = function (data, callback) {
  debug('Executing deactivate-account event')

  if (!data.currentUserID) {
    callback({code: 1, msg: 'Current User is not defined'})
    return
  }

  Models.Auvenir.User.findOne({'_id': data.currentUserID}, function (err, currentUser) {
    if (err) {
      callback({code: 1, result: err})
      return
    }
    if (!currentUser) {
      return callback({code: 1, msg: 'There was an error while deactivating the account.'})
    }

    var currentUserID = currentUser._id
    var userID = data.currentUserID

    if (userID.equals(currentUserID) || currentUser.type === 'ADMIN') {
      var changes = { userID: userID, status: 'INACTIVE' }

      Utility.updateUser(changes, currentUserID, function (err, result) {
        if (err) {
          callback(err)
        } else {
          var updatedUser = result.user
          updatedUser.status = changes.status
          Utility.createActivity(userID, data.engagementID, 'UPDATE', 'User', result.user, updatedUser, null)
          callback({code: 0, result: result})
        }
      })
    } else {
      callback({code: 1, msg: 'You do not have authrization for deactivating user account'})
    }
  })
}

module.exports = DeactivateAccount
