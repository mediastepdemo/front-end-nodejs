const Models = require('../../models')
const Utility = require('../../plugins/utilities/utility')
const debug = require('debug')('auvenir:event:send-team-member-invite')
const async = require('async')

var SendMultipleTeamMemberInvites = function ({engagementID, currentUser, users}, callback) {
  if (!engagementID) {
    return callback({code: 1, msg: 'Missing engagement id'})
  }

  if (!currentUser) {
    return callback({code: 1, msg: 'Missing auditor information'})
  }

  if (!users) {
    return callback({code: 1, msg: 'Missing team member information'})
  }

  if (users && users.length === 0) {
    return callback({code: 0})
  }

  Models.Auvenir.Engagement.findById(engagementID, (err, engagement) => {
    if (err) {
      return callback({code: 500, msg: 'Error on retrieving engagement'})
    }
    if (!engagement) {
      return callback({code: 404, msg: 'Unable to find engagement information'})
    }

    let auditorInAcl = engagement.acl.findIndex((acl) => {
      return (acl.role === 'AUDITOR') && (acl.id.toString() === currentUser._id)
    })

    if (!~auditorInAcl) {
      return callback({code: 403, msg: `auditor with id ${currentUser._id} has no permission with engagement ${engagement._id}`})
    }

    let resultantEngagement = null
    let activityDataList = []
    async.forEachLimit(users, 1, function (userElement, userCallback) {
      const createUser = (cbk) => {
        if (userElement.isNewUser) {
          userElement.user.type = 'AUDITOR'
          Utility.createUser(userElement.user, function (err, newMember) {
            if (err) {
              return callback({code: 500, msg: 'Error on creating user'})
            }
            if (!newMember) {
              return callback({code: 404, msg: 'Unable to create user'})
            }

            userElement.user = newMember
            cbk(null, {engagement})
            return
          })
        } else {
          cbk(null, {engagement})
          return
        }
      }

      const ensureTeamMember = ({engagement}, cbk) => {
        Models.Auvenir.User.findOne({_id: userElement.user._id}, (err, user) => {
          if (err) {
            return callback({code: 500, msg: 'Error on retrieving user'})
          }
          if (!user) {
            return callback({code: 404, msg: `cannot find user with id: ${userElement.user._id}`})
          }
          let token = Utility.secureRandomString(32)

          if (!user.auth) {
            user.auth = {}
          }
          let expiryTime = Date.now() + Utility.SESSION_LENGTH
          user.auth = Object.assign({}, user.auth, {
            access: {
              token: Utility.createHash(token),
              expires: expiryTime
            }
          })
          return user.save((err, user) => {
            if (err) {
              callback({code: 500, msg: 'Error on updating user'})
              return
            }
            debug('user updated', user)
            userElement.user = user
            cbk(null, {engagement, token})
            return
          })
        })
      }

      const addUserToEngagement = ({engagement, token}, cbk) => {
        if (!userElement.isNewUser) {
          return cbk(null, {engagement, token})
        }

        Utility.addUserToEngagement(userElement.user._id, engagement._id, false, function (err, result) {
          if (err) {
            return cbk(err)
          } else {
            if (!result.updatedEngagement) {
              return callback({code: 404, msg: `Cannot find engagement with id ${result.updatedEngagement}`})
            } else {
              let updatedEngagement = result.updatedEngagement
              cbk(null, {engagement: updatedEngagement, token})
            }
          }
        })
      }

      const createActivity = ({engagement, token}, cbk) => {
        Utility.createActivity(currentUser._id, engagement._id, (userElement.isNewUser) ? 'INVITE' : 'ONBOARD', 'User', {}, userElement.user, (err, res) => {
          if (err) {
            callback({code: 500, msg: 'Error on creating Activity'})
            return
          }
          if (res) {
            cbk(null, {engagement, token})
            return
          }
        })
      }

      const sendTeamInvite = ({engagement, token}, cbk) => {
        let inviterLastNameLetter = ''
        inviterLastNameLetter = (currentUser.lastName && currentUser.lastName.trim()) ? currentUser.lastName.trim().charAt(0) : ''

        let invitedUser = userElement.user
        let status = ''
        if (invitedUser.type === 'AUDITOR') {
          status = 'sendEngagementInviteGeneralAuditor'
        } else {
          if (userElement.isNewUser) {
            status = 'sendEngagementInviteGeneralClient'
          } else {
            status = 'sendEngagementNotifyGeneralClient'
          }
        }
        Utility.sendEmail({
          fromEmail: 'andi@auvenir.com',
          toEmail: invitedUser.email,
          toName: `${invitedUser.firstName} ${invitedUser.lastName}`,
          status: status,
          customValues: {
            token: token,
            email: invitedUser.email,
            engagementID: engagement._id,
            engagementName: engagement.name,
            inviteeFirstName: invitedUser.firstName,
            inviteeLastName: invitedUser.lastName,
            inviterFirstName: currentUser.firstName,
            inviterLastName: inviterLastNameLetter
          }
        }, function (err, result) {
          let activityData = {}
          activityData.engagementID = engagement._id.toString()
          activityData.operation = (userElement.isNewUser) ? 'INVITE' : 'ONBOARD'
          activityData.type = 'User'
          activityData.original = {}
          activityData.updated = userElement.user
          activityDataList.push(activityData)
          debug(`Invitation send Result ${JSON.stringify(result)}`)
          cbk(err, {engagement})
        })
      }

      async.waterfall([createUser, ensureTeamMember, addUserToEngagement, sendTeamInvite], (err, result) => {
        if (err) {
          if (err.code && err.msg) {
            return callback(err)
          }
          return callback({code: 500, msg: 'Error on sending team invitation.'})
        }
        resultantEngagement = result.engagement
        userCallback()
      })
    }, function (err) {
      if (err) {
        if (err.code && err.msg) {
          return callback(err)
        }
        return callback({code: 500, msg: 'Error on sending team invitation.'})
      }

      if (!resultantEngagement) {
        return callback({code: 0})
      }

      resultantEngagement.fetchAdditionalInfo((err, data) => {
        if (err) {
          return callback({code: 500, msg: 'Error on fetching additional information'})
        }
        callback({code: 0, engagement: data, activityDataList})
        return
      })
    })
  })
}

module.exports = SendMultipleTeamMemberInvites
