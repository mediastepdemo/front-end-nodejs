/* eslint-disable handle-callback-err */
/**
 * Created by Bao Nguyen on 7/27/2017.
 */

var Utility = require('../../plugins/utilities/utility')
var request = require('request')

var GetSamples = function (data, callback) {
  let url = 'http://192.168.1.162:8080/api/samples'

  if (!data) {
    url = 'http://192.168.1.162:8080/api/samples'
  } else {
    url = `${url}?skip=${data.skip}&take=${data.take}`
  }

  var options = {
    url: url,
    headers: {
      'Accept': 'application/json'
    }
  }

  request.get(options, function (error, response, body) {
    callback({ code: 0, result: body, page: options.url})
  })
}

module.exports = GetSamples
