var Utility = require('../../plugins/utilities/utility')

/**
 * Get Notifications belongs to user
 * @class GetInbox
 *
 * @param {Object} data     - data
 * @param {Object} callback - callback function
 */
var getActivities = function (data, callback) {
  if (!data.currentUserID) {
    callback({code: 1, msg: 'Current User is not defined.'})
  }
  if (!data.engagementID) {
    callback({code: 1, msg: 'Current Engagmenet ID is not defined.'})
  }

  Utility.getActivities({userID: Utility.castToObjectId(data.currentUserID), engagementID: Utility.castToObjectId(data.engagementID)}, function (err, activities) {
    if (err) {
      callback({code: 1, msg: 'Error during activities data retrieval'})
    } else {
      callback({code: 0, msg: 'Success getting activities', result: {activities: activities}})
    }
  })
}

module.exports = getActivities
