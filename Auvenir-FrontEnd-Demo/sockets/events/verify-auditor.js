var Models = require('../../models')
var Utility = require('../../plugins/utilities/utility')
var debug = require('debug')('auvenir:event:verify-auditor')
var appConfig = require('../../config')
var initLogger = require('../../plugins/logger/logger').init
const {error} = initLogger(__filename)

var VerifyAuditor = function (data, callback) {
  debug('Executing verify-auditor event')
  var defaultErrMsg = 'There was an error while onboarding user.'

  if (!data) {
    return callback({ code: 111, msg: defaultErrMsg })
  }
  if (!data.currentUserID) {
    return callback({ code: 111, msg: defaultErrMsg })
  }
  if (!data.userID) {
    return callback({ code: 111, msg: defaultErrMsg })
  }
  if (!data.protocol || !data.domain || !data.port) {
    return callback({ code: 111, msg: defaultErrMsg })
  }

  Models.Auvenir.User.findOne({ _id: data.currentUserID }, (err, currentUser) => {
    if (err) {
      callback({ code: 111, msg: defaultErrMsg })
    } else {
      if (currentUser) {
        if (currentUser.type === 'ADMIN') {
          Models.Auvenir.User.findOneAndUpdate({ _id: data.userID }, { $set: { 'status': 'ONBOARDING', 'verified': true } }, { new: false }, (err, oneUser) => {
            if (err) {
              callback({ code: 111, msg: defaultErrMsg })
            } else {
              var token = Utility.secureRandomString(64)
              if (!oneUser.auth.access) {
                oneUser.auth.access = {}
              }
              oneUser.auth.access.token = Utility.createHash(token)
              oneUser.auth.access.expires = Date.now() + Utility.SESSION_LENGTH
              oneUser.save((err) => {
                if (err) {
                  return callback({ code: 400, msg: 'Error creating login token' })
                }

                oneUser.status = 'ONBOARDING'

                var emailINFO = {
                  fromEmail: 'andi@auvenir.com',
                  toEmail: oneUser.email,
                  toName: oneUser.firstName + ' ' + oneUser.lastName,
                  status: 'verified',
                  customValues: {
                    url: appConfig.publicServer.url,
                    token: token,
                    email: oneUser.email
                  }
                }
                Utility.sendEmail(emailINFO, (err) => {
                  if (err) {
                    callback({ code: 222, msg: 'Updated ' + oneUser.email + '\'s status but error on sending Email.' })
                  } else {
                    debug(`Verified Auditor. Account verification email to ${oneUser.email} sent!`)
                    callback({ code: 0, msg: 'Verified ' + oneUser.email + ' successfully.', userID: data.userID, status: 'ONBOARDING' })
                  }
                })
              })
            }
          })
        } else {
          callback({ code: 111, msg: defaultErrMsg })
        }
      } else {
        callback({ code: 111, msg: defaultErrMsg })
      }
    }
  })
}

module.exports = VerifyAuditor
