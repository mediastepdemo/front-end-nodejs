var Models = require('../../models')
var Utility = require('../../plugins/utilities/utility')

var RemoveNotification = function (data, callback) {
  if (!data.notificationID) {
    callback({code: 1, msg: 'Missing Notification ID.'})
    return
  }

  var notifID = Utility.castToObjectId(data.notificationID)

  if (notifID === null) {
    callback({code: 1002, msg: 'Error casting notification to Object ID'})
    return
  }

  Models.Auvenir.Notification.findOneAndRemove({_id: notifID}, function (err, removedNotification) {
    if (err) {
      callback({code: 1, msg: 'Error on removing notifiaction.'})
    } else {
      callback({code: 0})
    }
  })
}

module.exports = RemoveNotification
