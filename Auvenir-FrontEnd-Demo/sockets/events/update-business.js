var Utility = require('../../plugins/utilities/utility')

var UpdateBusiness = function (data, callback) {
  if (!data.currentUserID) {
    callback({code: 1, msg: 'No Current User.'})
    return
  }

  if (!data.businessID) {
    callback({code: 1, msg: 'Missing Business ID'})
    return
  }

  var currentUserID = data.currentUserID

  Utility.updateBusiness(data, currentUserID, function (err, result) {
    if (err) {
      callback(err)
    } else {
      callback({code: 0, data: data, result: result})
    }
  })
}

module.exports = UpdateBusiness
