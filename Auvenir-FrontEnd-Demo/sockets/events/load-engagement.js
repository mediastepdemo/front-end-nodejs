/**
 * Read Operation Load Engagement
 */
var Utility = require('../../plugins/utilities/utility')
const debug = require('debug')('auvenir:event:load-engagement')

var LoadEngagement = function (data, callback) {
  if (!data.engagementID || data.engagementID === '') {
    callback({code: 1002, msg: 'No engagement ID'})
    return
  }

  var objID = Utility.castToObjectId(data.engagementID)
  var currentEngagementID = objID

  Utility.retrieveEngagementInfo({ id: currentEngagementID, userID: data.currentUserID }, function (err, res) {
    if (err) {
      callback(err)
    } else {
      if (!res) {
        callback({code: 1001, msg: 'Data was not returned properly'})
      } else {
        debug(res)
        callback({code: 0, result: res, page: data.page})
      }
    }
  })
}
module.exports = LoadEngagement
