const Utility = require('../../plugins/utilities/utility')
const initLogger = require('../../plugins/logger/logger').init
const { info, warn, error } = initLogger(__filename)

const UpdateCategory = function (data, callback) {
  if (!data.currentUserID) {
    callback({code: 1, msg: 'Current User is not defined.'})
    return
  }
  if (!data.engagementID) {
    callback({code: 1, msg: 'Missing Engagement ID'})
    return
  }

  let {currentUserID, engagementID, categories} = data
  Utility.updateCategory(categories, currentUserID, engagementID, function (err, result) {
    if (err) {
      callback(err)
    } else {
      callback({code: 0, result: result})
    }
  })
}

module.exports = UpdateCategory
