const Utility = require('../../plugins/utilities/utility')
const Access = require('../../plugins/utilities/accessControls')
const debug = require('debug')('auvenir:event:delete-file')
const defaultErrMsg = 'Error on canceling file.'
const initLogger = require('../../plugins/logger/logger').init
const {warn, error} = initLogger(__filename)

var CancelUpload = (data, callback) => {
  debug('Executing cancel-upload event')
  debug(data)
  if (!data) {
    return callback({ code: 1, msg: defaultErrMsg })
  }
  if (!data.fileID) {
    return callback({ code: 1, msg: defaultErrMsg })
  }
  if (!data.engagementID) {
    return callback({ code: 1, msg: defaultErrMsg })
  }

  var fileID = Utility.castToObjectId(data.fileID)
  if (fileID === null) {
    callback({ code: 1002, msg: defaultErrMsg })
    return
  }

  Access.file(fileID, data.engagementID, data.currentUserID, (err, results) => {
    if (err) {
      error({err})
      return callback({ code: 1, msg: defaultErrMsg })
    }

    debug('Validation passed, processing request')
    let { file } = results

    file.status = 'INACTIVE'
    file.save((err) => {
      if (err) {
        error({err})
        return callback({ code: 1, msg: defaultErrMsg })
      }
      return callback({ code: 0, result: { fileID: fileID } })
    })
  })
}
module.exports = CancelUpload
