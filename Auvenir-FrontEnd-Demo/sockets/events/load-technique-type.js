/**
 * Created by Bao Nguyen on 8/1/2017.
 */

/* eslint-disable handle-callback-err */
var Utility = require('../../plugins/utilities/utility')
var request = require('request')
const debug = require('debug')('auvenir:event:load-technique-type')

var LoadTechniqueType = function (data, callback) {
  var options = {
    url: 'http://192.168.1.162:8080/api/technique-type',
    headers: {
      'Accept': 'application/json'
    }
  }

  request.get(options, function (error, response, body) {
    callback({code: 0, result: body, page: 'working-paper-step-3'})
  })
}

module.exports = LoadTechniqueType
