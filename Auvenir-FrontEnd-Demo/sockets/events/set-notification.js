const Models = require('../../models')
const Utility = require('../../plugins/utilities/utility')
const debug = require('debug')('auvenir:event:set-notification')

var SetNotification = function ({userID, notificationType, value}, callback) {
  debug('Executing set-notification event')

  if (!userID) {
    return callback({code: 1, msg: 'Missing user id'})
  }

  if (notificationType === undefined) {
    return callback({code: 1, msg: 'Missing notification type information'})
  }

  Models.Auvenir.User.findOne({_id: Utility.castToObjectId(userID)}, (err, user) => {
    if (err) {
      return callback({code: 500, msg: 'Error on finding user'})
    }
    if (!user) {
      return callback({code: 404, msg: 'Unable to find user information'})
    }
    if (user.notifications.hasOwnProperty(notificationType)) {
      user.notifications[notificationType].email = value
      user.save((err, newUser) => {
        if (err) {
          callback({code: 500, msg: 'Error on saving user'})
        } else {
          callback({code: 0, user: newUser})
        }
      })
    } else {
      return callback({code: 404, msg: 'Notification type information is invalid'})
    }
  })
}

module.exports = SetNotification
