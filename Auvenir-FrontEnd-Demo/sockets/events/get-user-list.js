var Models = require('../../models')

var GetUserList = function (data, callback) {
  if (!data.currentUserID) {
    callback({code: 1, msg: 'Current user is not defined.'})
    return
  }
  Models.Auvenir.User.findOne({'_id': data.currentUserID}, function (err, user) {
    if (err) {
      callback({code: 23, msg: 'Error finding current user'})
    } else {
      var conditions = {}
      if (data.type !== undefined) conditions.type = data.type
      if (data.status !== undefined) conditions.status = data.status
      Models.Auvenir.User.find(conditions, function (err, users) {
        if (err) {
          callback({code: 1, msg: 'Error on finding users.'})
        } else {
          callback({code: 0, result: users})
        }
      })
    }
  })
}

module.exports = GetUserList
