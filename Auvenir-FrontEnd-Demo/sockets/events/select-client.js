/**
 * Create Operation for select a client or create a client for an engagement
 */
const Models = require('../../models')
const Utility = require('../../plugins/utilities/utility')
const async = require('async')
const debug = require('debug')('auvenir:event:create-client')

module.exports = function ({action, engagementId, clientId, user = {}, business = {}, currentUserID}, callback) {
  debug('Executing create-client event')
  if (['SELECT', 'ADD', 'UPDATE'].indexOf(action) === -1) {
    return callback({code: 400, msg: 'invalid action'})
  }

  const createClient = ({engagement}, cbk) => {
    const {firstName, lastName, email, jobTitle} = user

    Models.Auvenir.User.count({email}, (err, count) => {
      if (err) {
        return cbk(err)
      }

      if (count) {
        return cbk({code: 400, msg: `${email} is associated with another account`})
      }

      Utility.createUser({
        status: 'PENDING',
        type: 'CLIENT',
        firstName,
        lastName,
        email,
        jobTitle
      }, (err, client) => {
        if (err) {
          return cbk(err)
        }
        return cbk(null, {engagement, client})
      })
    })
  }

  const addUserToBusiness = ({engagement, client}, cbk) => {
    let adminSetting = true
    let newUser = client
    let isExisting = false

    const checkBusiness = (cbk) => {
      Models.Auvenir.Business.findOne({_id: engagement.business.id}, (err, business) => {
        if (err) return cbk(err)
        if (!business) return cbk('No Business')
        const newUserId = newUser._id.toString()
        business.acl.forEach((acl) => {
          if (acl.id.toString() === newUserId) isExisting = true
          if (acl.admin === true) adminSetting = false
        })
        return cbk(null, business)
      })
    }

    const addUser = (business, cbk) => {
      if (isExisting) {
        cbk(null, business)
        return
      }
      let acl = {
        id: newUser._id,
        admin: adminSetting
      }

      business.acl.push(acl)
      business.save((err) => {
        if (err) {
          cbk(err)
        } else {
          cbk(null, business)
        }
      })
    }

    async.waterfall([checkBusiness, addUser], (err, newBusiness) => {
      if (err) {
        return cbk(err)
      } else {
        cbk(null, {engagement, client: newUser, business: newBusiness})
      }
    })
  }

  const fetchBusiness = ({client}, cbk) => {
    Models.Auvenir.Business.findOne({acl: {$elemMatch: {id: client._id}}}, (err, business) => {
      if (err) {
        return cbk(err)
      }
      if (!business) {
        return cbk({code: 404, msg: 'Cannot find business'})
      }
      cbk(null, {client, business})
    })
  }

  const fetchClient = (cbk) => {
    Models.Auvenir.User.findById(clientId, (err, user) => {
      if (err) {
        return cbk(err)
      }
      if (!user) {
        return cbk({code: 404, msg: 'Cannot find user'})
      }
      cbk(null, {client: user})
    })
  }

  const fetchEngagementInitial = (cbk) => {
    fetchEngagement({}, (err, result) => {
      if (err) {
        return cbk(err)
      }
      return cbk(null, result)
    })
  }

  const fetchEngagement = ({client, business}, cbk) => {
    Models.Auvenir.Engagement.findById(engagementId, (err, engagement) => {
      if (err) {
        return cbk(err)
      }
      if (!engagement) {
        return cbk({code: 404, msg: 'Cannot find engagement'})
      }
      cbk(null, {engagement, client, business})
    })
  }

  const updateEngagement = ({engagement, client, business}, cbk) => {
    const clientID = client._id.toString()
    const businessID = business._id.toString()
    engagement.changeBusiness({clientID, businessID}, (err, oneEngagement) => {
      if (err) {
        return cbk(err)
      }
      oneEngagement.fetchAdditionalInfo((err, newEngagement) => {
        cbk(err, {client, business, engagement: newEngagement})
      })
    })
  }

  const updateClient = (cbk) => {
    if (!clientId) {
      return cbk({code: 400, msg: 'missing client user id'})
    }
    Models.Auvenir.User.findOneAndUpdate({_id: clientId}, {$set: user}, {new: true}, (err, client) => {
      cbk(err, {client})
    })
  }

  const updateBusiness = ({client}, cbk) => {
    Models.Auvenir.Business.findOneAndUpdate({acl: {$elemMatch: {id: client._id}}}, {$set: business}, {new: true}, (err, business) => {
      cbk(err, {client, business})
    })
  }

  let tasks

  switch (action) {
    case 'SELECT':
      if (!engagementId) {
        return callback({code: 400, msg: 'Missing engagement id'})
      }
      tasks = [fetchClient, fetchBusiness, fetchEngagement, addUserToBusiness, updateEngagement]
      break
    case 'ADD':
      tasks = [fetchEngagementInitial, createClient, addUserToBusiness, updateEngagement]
      break
    case 'UPDATE':
      tasks = [updateClient, updateBusiness]
      if (engagementId) {
        tasks = tasks.concat([fetchEngagement])
      }
      break
  }

  async.waterfall(tasks, (err, result) => {
    if (err) {
      if (err.code && err.msg) {
        return callback(err)
      }
      return callback({code: 500, msg: 'Error on creating the client and business'})
    }

    let {client, business, engagement} = result
    client = client.toJSON()
    business = business.toJSON()
    return callback({
      code: 0,
      result: {client, business, engagement}
    })
  })
}
