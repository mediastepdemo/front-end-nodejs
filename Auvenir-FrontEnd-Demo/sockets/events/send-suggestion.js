const Models = require('../../models')
const Utility = require('../../plugins/utilities/utility')
const appConfig = require('../../config')
const _ = require('lodash')
const initLogger = require('../../plugins/logger/logger').init
const {info, warn, error} = initLogger(__filename)
const defaultErrMsg = 'Something went wrong while sending feedback.'

const SendSuggestion = function (data, callback) {
  info('send-suggestion event is triggered.')
  if (!data.text) {
    warn('data.text is undefined')
    callback({code: 1, msg: defaultErrMsg})
    return
  }
  if (!data.userID) {
    warn('user is undefined')
    callback({code: 1, msg: defaultErrMsg})
    return
  }
  if (data.userID.toString() !== data.currentUserID.toString()) {
    warn('source user and current user do not match')
    callback({code: 1, msg: defaultErrMsg})
    return
  }
  if (!data.feedbackSource) {
    warn('Provide feedback source.')
    return callback({code: 1, msg: defaultErrMsg})
  }

  Models.Auvenir.User.findOne({'_id': _.isString(data.userID) ? Utility.castToObjectId(data.userID) : data.userID }, function (err, currentUser) {
    if (err) {
      error(err)
      return callback({code: 1, msg: defaultErrMsg})
    }
    if (!currentUser) {
      error('Unable to find current user in the database')
      return callback({code: 1, msg: defaultErrMsg})
    }
    var protocol = data.protocol
    var domain = data.domain
    var port = data.port
    if (currentUser) {
      var emailINFO = {
        fromEmail: currentUser.email,
        toEmail: 'feedback@auvenir.com',
        fromName: currentUser.firstName + ' ' + currentUser.lastName,
        status: 'sendSuggestion',
        customValues: {
          url: appConfig.publicServer.url,
          token: currentUser.auth.access.token,
          email: currentUser.email, // TODO - check if this is needed
          userFirstName: currentUser.firstName,
          userLastName: currentUser.lastName,
          userText: data.text,
          feedbackSource: data.feedbackSource
        }
      }

      Utility.sendEmail(emailINFO, function (err) {
        if (err) {
          error(err)
          callback({code: 1, msg: defaultErrMsg})
        } else {
          callback({code: 0})
        }
      })
    } else {
      error('User does not exist in the system.')
      callback({code: 1, msg: defaultErrMsg})
    }
  })
}

module.exports = SendSuggestion
