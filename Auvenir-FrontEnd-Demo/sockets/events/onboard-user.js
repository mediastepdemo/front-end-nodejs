var Models = require('../../models')
var Utility = require('../../plugins/utilities/utility')
var debug = require('debug')('auvenir:event:onboard-user')
var appConfig = require('../../config')

var OnboardUser = function (data, callback) {
  debug('Executing onboard-user event')
  var defaultErrMsg = 'There was an error while onboarding user.'

  if (!data) {
    return callback({ code: 111, msg: defaultErrMsg })
  }
  if (!data.currentUserID) {
    return callback({ code: 111, msg: defaultErrMsg })
  }
  if (!data.userID) {
    return callback({ code: 111, msg: defaultErrMsg })
  }
  if (!data.protocol || !data.domain || !data.port) {
    return callback({ code: 111, msg: defaultErrMsg })
  }

  Models.Auvenir.User.findOne({ _id: data.currentUserID }, (err, currentUser) => {
    if (err) {
      callback({code: 111, msg: defaultErrMsg })
    } else {
      if (currentUser) {
        if (currentUser.type === 'ADMIN') {
          Models.Auvenir.User.findOneAndUpdate({ _id: data.userID }, { $set: { 'status': 'ONBOARDING', 'verified': true } }, { new: false }, (err, oneUser) => {
            if (err) {
              callback({ code: 111, msg: defaultErrMsg })
            } else {
              debug('User Updated.')

              var emailINFO = {
                fromEmail: 'andi@auvenir.com',
                toEmail: oneUser.email,
                toName: oneUser.firstName + ' ' + oneUser.lastName,
                status: 'verified',
                customValues: {
                  url: appConfig.publicServer.url
                }
              }
              Utility.sendEmail(emailINFO, (err, emailResult) => {
                if (err) {
                  callback({ code: 222, msg: 'Updated ' + oneUser.email + '\'s status but there was an error on sending Email.' })
                } else {
                  debug(`Account verification email to ${oneUser.email} sent!`)
                  callback({ code: 0, msg: 'Onboarded ' + oneUser.email + ' successfully.', userID: data.userID, status: data.status })
                }
              })
            }
          })
        } else {
          callback({ code: 111, msg: defaultErrMsg })
        }
      } else {
        callback({ code: 111, msg: defaultErrMsg })
      }
    }
  })
}

module.exports = OnboardUser
