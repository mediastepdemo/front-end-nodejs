var Utility = require('../../plugins/utilities/utility')

var UpdateUserPassword = function (data, callback) {
  if (!data.currentUserID) {
    callback({code: 1, msg: 'Current User is not defined.'})
    return
  }

  if (!data.userID) {
    callback({code: 1, msg: 'Missing User ID'})
    return
  }

  Utility.updateUserPassword(data, data.currentUserID, function (err, result) {
    if (err) {
      callback(err)
    } else {
      callback({code: 0, data: data, result: result})
    }
  })
}

module.exports = UpdateUserPassword
