/**
 * Create Operation for Create Engagement
 */
const Models = require('../../models')
const async = require('async')
const debug = require('debug')('auvenir:event:create-engagement')
const Utility = require('../../plugins/utilities/utility')

module.exports = function ({firmID, auditorID, currentUserID, engagementInfo, businessName}, callback) {
  debug('Executing create-engagement event')
  debug(firmID, auditorID, currentUserID, businessName)

  if (!firmID) {
    return callback({code: 1, msg: 'Missing firm information'})
  }

  if (!auditorID) {
    return callback({code: 1, msg: 'Missing auditor information'})
  }

  if (!businessName) {
    return callback({code: 1, msg: 'Missing business name'})
  }

  if (currentUserID.toString() !== auditorID) {
    return callback({code: 1, msg: 'Invalid auditor id'})
  }
  let isNewBusiness = true
  const findFirm = (cbk) => {
    Models.Auvenir.Firm.findOne({_id: firmID}, function (err, firm) {
      if (err) {
        return cbk({code: 500, msg: 'Error on retrieving firm'})
      }

      if (!firm) {
        return cbk({code: 500, msg: 'Unable to find firm information'})
      }
      engagementInfo.firm = {
        id: Utility.castToObjectId(firm._id),
        creater: Utility.castToObjectId(auditorID),
        currentOwner: Utility.castToObjectId(auditorID)
      }
      return cbk(null, firm)
    })
  }
  const findOrCreateBusiness = (firm, cbk) => {
    Models.Auvenir.Business.findOne({name: businessName}, function (err, business) {
      if (err) {
        return cbk({code: 500, msg: 'Error on retrieving business'})
      }
      if (!business) {
        isNewBusiness = true
        Utility.createBusiness({name: businessName}, (err, newBusiness) => {
          if (err) {
            return cbk({code: 500, msg: 'Error on creating business'})
          }
          engagementInfo.business = {id: Utility.castToObjectId(newBusiness._id)}
          return cbk(null, {business: newBusiness})
        })
      } else {
        isNewBusiness = false
        engagementInfo.business = {id: business._id}
        return cbk(null, {business})
      }
    })
  }

  const createEngagement = ({name, code, business}, cbk) => {
    Models.Auvenir.Engagement.create({firmID, auditorID}, (err, engagement) => {
      if (err) {
        return cbk({code: 500, msg: 'Error on creating engagement'})
      }
      if (!engagement) {
        return cbk({code: 404, msg: 'Unable to create engagement'})
      } else {
        engagement.name = (engagementInfo.name) ? engagementInfo.name : 'Unknown'
        engagement.type = (engagementInfo.type) ? engagementInfo.type : ''
        engagement.business = (engagementInfo.business) ? engagementInfo.business : ''

        if (engagementInfo.bankStatementDateRange) {
          engagement.bankStatementDateRange = engagementInfo.bankStatementDateRange
        }

        engagement.dueDate = (engagementInfo.dueDate) ? engagementInfo.dueDate : ''
        engagement.save((err, eng) => {
          if (err) {
            return cbk({code: 500, msg: 'Error on creating engagement'})
          }
          cbk(null, {business, engagement: eng})
        })
      }
    })
  }

  const fetchAdditionalInfo = ({business, engagement}, cbk) => {
    engagement.fetchAdditionalInfo((err, engagementObj) => {
      if (err) {
        return cbk(err)
      }
      cbk(null, {business, engagementObj})
    })
  }

  const createActivityEngagement = ({business, engagementObj}, cbk) => {
    Utility.createActivity(currentUserID, engagementObj._id, 'CREATE', 'Engagement', {}, engagementObj, (err, result) => {
      if (err) {
        cbk({name: 'create-activity', code: 333, msg: 'Error on creating Activity'})
        return
      } else {
        cbk(null, {name: 'create-activity', code: 0, business: business, engagement: engagementObj})
        return
      }
    })
    // cbk(null, {name: 'create-activity', code: 0, business: business, engagement: engagementObj})
  }

  async.waterfall([findFirm, findOrCreateBusiness, createEngagement, fetchAdditionalInfo, createActivityEngagement], (err, {name, code, business, engagement}) => {
    if (err) {
      debug(err)
      if (err.code && err.msg) {
        return callback(err)
      }
      if (err.name === 'ValidationError') {
        debug(err.errors)
      }
      return callback({code: 500, msg: 'Error on creating the engagement'})
    }
    callback({code: 0, business: business, isNewBusiness: isNewBusiness, engagement: engagement})
  })
}
