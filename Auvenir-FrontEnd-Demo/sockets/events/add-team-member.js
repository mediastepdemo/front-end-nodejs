const Models = require('../../models')
const Utility = require('../../plugins/utilities/utility')
const debug = require('debug')('auvenir:event:add-team-member')
const async = require('async')

var AddTeamMember = function (data, callback) {
  debug('Executing add-team-member event')
  const {userID, currentUser, engagementID, role} = data

  const addUserToEngagement = (cbk) => {
    Utility.addUserToEngagement(userID, engagementID, false, function (err, result) {
      if (err) {
        return cbk(err)
      } else {
        if (!result.updatedEngagement) {
          return cbk({code: 404, msg: `Cannot find engagement with id ${engagementID}`})
        } else {
          cbk(null, result)
        }
      }
    })
  }

  const fetchAdditionalInfo = (result, cbk) => {
    let engagement = result.updatedEngagement
    engagement.fetchAdditionalInfo((err, data) => {
      if (err) {
        return cbk(err)
      }
      cbk(err, data)
    })
  }

  const ensureTeamMember = (engagement, cbk) => {
    Models.Auvenir.User.findOne({_id: userID}, (err, user) => {
      if (err) {
        return cbk({code: 500, msg: 'Error on retrieving user'})
      }
      if (!user) {
        return cbk({code: 404, msg: `cannot find user with id: ${userID}`})
      }
      let token = Utility.secureRandomString(32)

      if (!user.auth) {
        user.auth = {}
      }
      let expiryTime = Date.now() + Utility.SESSION_LENGTH
      user.auth = Object.assign({}, user.auth, {
        access: {
          token: Utility.createHash(token),
          expires: expiryTime
        }
      })
      return user.save((err, user) => {
        if (err) {
          cbk({code: 500, msg: 'Error on updating user'})
          return
        }
        let activityData = {}
        activityData.engagementID = data.engagementID
        activityData.operation = 'ONBOARD'
        activityData.type = 'User'
        activityData.original = {}
        activityData.updated = user
        debug('user updated', user)
        cbk(null, {user, engagement, token, activityData})
      })
    })
  }

  const sendInvitationEmail = ({user, engagement, token, activityData}, cbk) => {
    let inviterLastNameLetter = ''
    inviterLastNameLetter = (currentUser.lastName && currentUser.lastName.trim()) ? currentUser.lastName.trim().charAt(0) : ''

    let status = ''
    if (user.type === 'AUDITOR') {
      status = 'sendEngagementNotifyGeneralAuditor'
    } else {
      status = 'sendEngagementNotifyGeneralClient'
    }
    Utility.sendEmail({
      fromEmail: 'andi@auvenir.com',
      toEmail: user.email,
      toName: `${user.firstName} ${user.lastName}`,
      status: status,
      customValues: {
        token: token,
        email: user.email,
        engagementID: engagement._id,
        engagementName: engagement.name,
        inviteeFirstName: user.firstName,
        inviteeLastName: user.lastName,
        inviterFirstName: currentUser.firstName,
        inviterLastName: inviterLastNameLetter
      }
    }, function (err, result) {
      debug(`Invitation send Result ${JSON.stringify(result)}`)
      cbk(err, {engagement, activityData})
    })
  }

  async.waterfall([addUserToEngagement, fetchAdditionalInfo, ensureTeamMember, sendInvitationEmail], (err, result) => {
    if (err) {
      if (err.code && err.msg) {
        return callback(err)
      }
      return callback({code: 500, msg: 'Cannot add acl to engagement'})
    }

    callback({code: 0, result: result})
  })
}

module.exports = AddTeamMember
