var Models = require('../../models')
var Utility = require('../../plugins/utilities/utility')
var async = require('async')
// TODO - fix this file when working on file-manager page
//      - some request related stuff is included here
var UpdateMultipleFiles = function (data, callback) {
  if (!data.currentUserID) {
    callback({code: 1, msg: 'Current User is not defined on request.'})
    return
  }
  var newRequestID = data.newRequestID
  var sortedByUserID = data.sortedBy
  var engagementID = data.engagementID
  var fileIDs = []

  async.each(data.fileList, function (file, cbIteration) {
    var fileID = file._id
    var previousRequestID = file.requestID
    fileIDs.push(fileID)

    function removeFileFromPreviousRequest (cbNext) {
      Models.Auvenir.Request.update({engagementID: engagementID, items: fileID}, {$pull: {items: fileID}}, {multi: true}, function (err, res) {
        if (err) {
          cbNext(err)
        } else {
          cbNext(null)
        }
      })
    }
    function addFileToNewRequest (cbNext) {
      Models.Auvenir.Request.findOneAndUpdate({_id: newRequestID}, {$push: {items: fileID}}, function (err, res) {
        if (err) {
          cbNext(err)
        } else {
          cbNext(null)
        }
      })
    }
    function asyncComplete (err, filePath, updatedFile) {
      if (err) {
        callback(err)
      } else {
        var result = {}
        result.newRequestID = newRequestID.toString()
        result.fileID = fileID
        result.path = filePath
        if (previousRequestID) {
          result.previousRequestID = previousRequestID.toString()
        }
        Models.Auvenir.User.findOne({_id: sortedByUserID}, function (err, oneUser) {
          if (err) {
            callback({name: 'update-file', code: 1, msg: 'Error on finding Sorted By User Object'})
            cbIteration(err)
          }
          if (data.successMsg) {
            result.successMsg = updatedFile.name + ' moved to '
          }
          result.sortedBy = {}
          result.sortedBy.id = oneUser._id
          result.sortedBy.firstName = oneUser.firstName

          result.file = updatedFile

          callback({name: 'update-file', code: 0, result: result})
          cbIteration()
        })
      }
    }
    function updateFile (cbNext) {
      Models.Auvenir.File.findOneAndUpdate({_id: fileID}, {$set: {sortedBy: sortedByUserID}}, function (err, updatedFile) {
        if (err) {
          cbNext({code: 1, msg: 'Error on finding Sorted By File Object'})
        } else {
          if (updatedFile) {
            cbNext(null, updatedFile.path, updatedFile)
          } else {
            cbNext({code: 1, msg: 'No file to update.'})
          }
        }
      })
    }
    async.waterfall(
      [
        removeFileFromPreviousRequest,
        addFileToNewRequest,
        updateFile
      ], asyncComplete
      )
  }, function (err) {
    if (err) {
      callback({code: 222, msg: 'Error on during updating files.'})
    } else {
      let activityData = {}
      activityData.currentUserID = data.currentUserID
      activityData.engagementID = data.engagementID
      activityData.operation = 'UPDATE'
      activityData.type = 'File'
      activityData.original = {'_id': fileIDs}
      activityData.updated = {requestID: newRequestID.toString()}

      var engagementID = data.engagementID
      var userID = data.currentUserID
      var original = {'_id': fileIDs}
      var update = {requestID: newRequestID.toString()}
      var typeCrud = 'UPDATE'
      var typeActivity = 'File'

      Utility.createActivity(userID, engagementID, typeCrud, typeActivity, original, update, function (err, res) {
        if (err) {
          callback({name: 'create-activity', code: 1, msg: 'Error on Creating Activity.'})
        } else {
          callback({name: 'create-activity', code: 0, result: res.result})
        }
      })
      var successMsg = `${data.fileList.length} files moved to `
      callback({code: 0, result: {successMsg: successMsg, newRequestID: newRequestID, activityData: activityData}})
    }
  }
  )
}
module.exports = UpdateMultipleFiles
