const Utility = require('../../plugins/utilities/utility')
const Access = require('../../plugins/utilities/accessControls')
const debug = require('debug')('auvenir:event:delete-file')
const _ = require('lodash')
const async = require('async')
const defaultErrMsg = 'Error on deleting file.'
const initLogger = require('../../plugins/logger/logger').init
const { info, warn, error } = initLogger(__filename)

var DeleteFile = (data, callback) => {
  debug('Executing delete-file event')
  debug(data)
  let { engagementID, selectedItems, currentUserID } = data
  let activityList = []
  let filesOnlyArray = []
  let filteredFilesOnlyArray = []

  if (!engagementID) {
    return callback({ code: 1, msg: defaultErrMsg })
  }

  if (!selectedItems || _.isEmpty(selectedItems)) {
    return callback({ code: 1, msg: defaultErrMsg})
  }

  const getChildren = (childrenArray) => {
    childrenArray.forEach((item, i) => {
      if (item.fileType === 'folder' && item.children.length > 0) {
        getChildren(item.children)
      } else {
        filesOnlyArray.push(item)
      }
    })
  }

  const catchOnlyFiles = (cbk) => {
    selectedItems.forEach((item, i) => {
      if (item.fileType === 'folder' && item.children.length > 0) {
        getChildren(item.children)
      } else {
        filesOnlyArray.push(item)
      }

      if (i === selectedItems.length - 1) {
        cbk()
      }
    })
  }

  const accessToEngagement = (cbk) => {
    Access.engagement(engagementID, currentUserID, (err, engagement) => {
      if (err) {
        return cbk(err)
      }

      engagement.fetchAdditionalInfo((err, engagementInfo) => {
        if (err) {
          return cbk(err)
        }
        cbk(null, engagementInfo)
      })
    })
  }

  const accessToFiles = (engagement, cbk) => {
    let mongoFileObjs = []
    async.each(filesOnlyArray, (oneFile, cbk) => {
      Access.file(Utility.castToObjectId(oneFile._id), engagementID, currentUserID, (err, { file }) => {
        if (err) {
          error({ err })
          // TODO - we need discussion on this error handling.
          // Do we pre- check on the front- end side and try to avoid from there but once it's in the server-side, it should be just passing them.
          return cbk()
        }
        mongoFileObjs.push(file)
        return cbk()
      })
    }, (err) => {
      if (err) { // no error will be passed though.
        return cbk(err)
      }
      return cbk(null, engagement, mongoFileObjs)
    })
  }

  const findFilesInRequest = (engagement, mongoFileObjs, cbk) => {
    info('find Todos in Request')
    let todos = engagement.todos
    async.each(mongoFileObjs, (mongoFile, cbk) => {
      todos.forEach((todo, i) => {
        if (todo.requests && todo.requests.length > 0) {
          todo.requests.forEach((request, j) => {
            if (request.currentFile) {
              if (request.currentFile.toString() === mongoFile._id.toString()) {
                _.pull(mongoFileObjs, mongoFile)
                filteredFilesOnlyArray = _.remove(filesOnlyArray, (item) => { return item._id.toString() === mongoFile._id.toString() })
              }
            }
          })
        }
        if (i === todos.length - 1) {
          cbk()
        }
      })
    }, (err) => {
      if (err) {
        cbk(err)
      } else {
        cbk(null, engagement, mongoFileObjs)
      }
    })
  }

  const deactivateFiles = (engagement, mongoFileObjs, cbk) => {
    let originalFile = null
    let updatedField = { status: 'INACTIVE' }

    if (mongoFileObjs && mongoFileObjs.length > 0) {
      mongoFileObjs.forEach((file, i) => {
        file.status = 'INACTIVE'
        file.save((err) => {
          if (err) {
            error(`Error saving file(${file._id}) to INACTIVE state.`, { err })
            let failedFilesArray = _.remove(filesOnlyArray, (oneFile) => {
              return oneFile._id.toString() === file._id.toString()
            })
            filteredFilesOnlyArray = _.concat(filteredFilesOnlyArray, failedFilesArray)
            return cbk(null, engagement)
          } else {
            originalFile = file.toJSON()
            let updatedField = { status: 'INACTIVE' }
            // activity.push()
            let activityData = {}
            activityData.currentUserID = currentUserID
            activityData.engagementID = engagementID
            activityData.operation = 'DELETE'
            activityData.type = 'File'
            activityData.original = originalFile
            activityData.updated = updatedField
            activityList.push(activityData)
            return cbk(null, engagement)
          }
        })
      })
    } else {
      return cbk(null, engagement)
    }
  }

  async.waterfall([catchOnlyFiles, accessToEngagement, accessToFiles, findFilesInRequest, deactivateFiles], (err, engagement) => {
    if (err) {
      error({err}) // no access to engagement
      return callback({code: 1, result: { successFiles: filesOnlyArray, failFiles: filteredFilesOnlyArray, engagement, activityList }})
    }

    return callback({code: 0, result: { successFiles: filesOnlyArray, failFiles: filteredFilesOnlyArray, engagement, activityList }})
  })
}
module.exports = DeleteFile
