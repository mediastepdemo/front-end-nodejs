/* globals Logger */

var Models = require('../../models')
var debug = require('debug')('auvenir:event:update-user-status')
var initLogger = require('../../plugins/logger/logger').init
const {error} = initLogger(__filename)

var UpdateUserStatus = function (data, callback) {
  debug('Executing update-user-status event')
  var defaultErrMsg = 'There was an error while updating user status.'

  if (!data) {
    return callback({ code: 111, msg: defaultErrMsg })
  }
  if (!data.currentUserID) {
    return callback({ code: 111, msg: defaultErrMsg })
  }
  if (!data.userID) {
    return callback({ code: 111, msg: defaultErrMsg })
  }
  if (!data.status) {
    return callback({ code: 111, msg: defaultErrMsg })
  }

  Models.Auvenir.User.findOne({ _id: data.currentUserID }, (err, currentUser) => {
    if (err) {
      callback({ code: 111, msg: defaultErrMsg })
    } else {
      if (currentUser) {
        if (currentUser.type === 'ADMIN') {
          Models.Auvenir.User.findOneAndUpdate({ _id: data.userID }, { $set: { 'status': data.status } }, { new: false }, (err, oneUser) => {
            if (err) {
              callback({ code: 111, msg: defaultErrMsg })
            } else {
              debug('User Status Updated')
              oneUser.status = data.status
              callback({ code: 0, msg: `Updated ${oneUser.email}'s status to ${data.status} successfully.`, userID: data.userID, status: data.status })
            }
          })
        } else {
          callback({ code: 111, msg: defaultErrMsg })
        }
      } else {
        callback({ code: 111, msg: defaultErrMsg })
      }
    }
  })
}

module.exports = UpdateUserStatus
