var Models = require('../../models')
var Utility = require('../../plugins/utilities/utility')

var GetAdminMapping = function (data, callback) {
  if (!data.userID) {
    callback({code: 1, msg: 'Missing user to validate.'})
    return
  }

  if (data.userID.toString() !== data.currentUserID.toString()) {
    callback({code: 1, msg: 'Invalid user is trying to access Admin portal.'})
  } else {
    Models.Auvenir.User.find({}, function (err, allUsers) {
      if (err) {
        callback({code: 1, msg: 'Error on retrieving users.'})
        return
      }

      Models.Auvenir.Business.find({}, function (err, allBusinesses) {
        if (err) {
          callback({code: 1, msg: 'Error on retrieving businesses.'})
          return
        }

        Models.Auvenir.Engagement.find({}, function (err, allEngagements) {
          if (err) {
            callback({code: 1, msg: 'Error on retreiving engagements'})
            return
          }

          var result = Utility.getAdminMapping(allUsers, allBusinesses, allEngagements)
          callback({code: 0, result: result})
        })
      })
    })
  }
}

module.exports = GetAdminMapping
