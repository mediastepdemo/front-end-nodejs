const Utility = require('../../plugins/utilities/utility')
const initLogger = require('../../plugins/logger/logger').init
const {info, warn, error} = initLogger(__filename)

const UpdateTodo = function (data, callback) {
  if (!data.currentUserID) {
    callback({code: 1, msg: 'Current User is not defined.'})
    return
  }

  if (!data.engagementID) {
    callback({code: 1, msg: 'Missing Engagement ID'})
    return
  }
  warn('Inside of Update Todo')
  warn(data)

  Utility.updateTodo(data, data.currentUserID, function (err, data) {
    if (err) {
      error(err)
      callback(err)
    } else {
      callback({code: 0, result: data})
    }
  })
}

module.exports = UpdateTodo
