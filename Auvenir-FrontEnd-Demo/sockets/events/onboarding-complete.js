const Models = require('../../models')
const Code = require('../../plugins/utilities/code')
const Utility = require('../../plugins/utilities/utility')
var initLogger = require('../../plugins/logger/logger').init
const {error} = initLogger(__filename)

/**
 * Responsible for Socket Event Onboarding Complete
 * Update user status to active since it finished onboarding.
 */
var OnboardingComplete = function (data, callback) {
  if (!data.currentUserID) {
    return callback({code: 1, msg: 'Current User ID is not available.'})
  }

  Models.Auvenir.User.findOneAndUpdate({'_id': data.currentUserID}, {$set: {status: 'ACTIVE', dateCreated: new Date()}}, {new: true}, (err, oneUser) => {
    if (err) return callback({code: Code.GENERAL_ERROR, msg: 'Error updating user status to ACTIVE'})
    if (data.acceptedOn && data.agreementV) {
      oneUser.agreements.agreementID = data.agreementV
      oneUser.agreements.timeStamp = data.acceptedOn
      oneUser.status = 'ACTIVE'
    }
    Utility.storeUserPassword(oneUser, data.password, (err) => {
      if (err) return callback(err)
      oneUser.save((err) => {
        if (err) {
          error(err)
          return callback({code: Code.GENERAL_ERROR, msg: 'Error updating user status to ACTIVE'})
        }
        if (oneUser.type === 'CLIENT') {
          return Models.Auvenir.Business.findOne({acl: {$elemMatch: {role: 'member', type: 'user', id: oneUser._id}}}, (err, oneBusiness) => {
            if (err) return callback({code: Code.GENERAL_ERROR, msg: 'Unable to create the engagement dynamically'})
            Models.Auvenir.Engagement.findOne({acl: {$elemMatch: {role: oneUser.type, id: oneUser._id}}}, (err2, oneEngagement) => {
              if (oneEngagement) {
                Utility.createActivity(data.currentUserID, oneEngagement._id, 'ONBOARD', 'User', {}, oneUser)
              }
            })
            callback({code: 0, result: { user: oneUser, business: oneBusiness }})
          })
        }
        callback({code: 0, result: { user: oneUser, business: null }})
      })
    })
  })
}

module.exports = OnboardingComplete
