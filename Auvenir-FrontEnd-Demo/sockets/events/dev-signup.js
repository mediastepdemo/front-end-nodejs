var Models = require('../../models')
var Utility = require('../../plugins/utilities/utility')

/**
 * @class DevSignup
 *
 * @param {Object} io     - socket io object
 * @param {Object} socket - client socket
 */
var DevSignup = function (data, callback) {
  if (!data.currentUserID) {
    callback({code: 1, msg: 'Current User is not defined.'})
    return
  }

  Models.Auvenir.User.findOne({'_id': data.currentUserID}, function (err, oneUser) {
    if (err) {
      callback({code: 1, msg: 'Error during user retrieval from DB'})
      return
    }
    if (!oneUser) {
      callback({code: 1, msg: 'Unable to find current user in DB'})
      return
    }
    Utility.generateDeveloperCreds(function (err, result) {
      if (err) {
        callback({code: 1, msg: 'Unable to generate a unique developer API key'})
      }
      var apiKey = result.apiKey
      oneUser.auth.developer = { apiKey: apiKey }
      oneUser.save(function (err) {
        if (err) {
          callback({code: 1, msg: 'Error occured while updating the user with an API key'})
        } else {
          Utility.createActivity(data.currentUserID, data.engagementID, 'UPDATE', 'User', oneUser, oneUser, null)
          callback({code: 0, result: oneUser})
        }
      })
    })
  })
}

module.exports = DevSignup
