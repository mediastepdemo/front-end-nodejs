var Utility = require('../../plugins/utilities/utility')

var SendSMS = function (data, callback) {
  if (!data) {
    return callback({code: 1, msg: 'Something went wrong while sending sms.'})
  }
  if (!data.port) {
    return callback({code: 1, msg: 'Something went wrong while sending sms.'})
  }
  if (!data.protocol) {
    return callback({code: 1, msg: 'Something went wrong while sending sms.'})
  }
  if (!data.domain) {
    return callback({code: 1, msg: 'Something went wrong while sending sms.'})
  }
  if (!data.countryCode) {
    return callback({code: 1, msg: 'Something went wrong while sending sms.'})
  }

  Utility.sendSMS(data, function (err, result) {
    if (err) {
      callback({ code: 1, msg: err.error_message })
    } else {
      if (result.body.status === 'delivered' || result.body.status === 'sent') {
        callback({code: 0, result: result})
      } else if (result.body.status === 'undelivered') {
        callback({code: 0, result: result})
      } else if (result.body.status === 'failed') {
        callback({code: 0, result: result})
      } else {
        callback({code: 0, result: result})
      }
    }
  })
}

module.exports = SendSMS
