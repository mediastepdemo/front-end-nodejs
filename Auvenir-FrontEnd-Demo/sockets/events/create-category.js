var Utility = require('../../plugins/utilities/utility')

var CreateCategory = function (data, callback) {
  if (!data.createdBy) {
    callback({code: 1, msg: 'Current User is not defined.'})
    return
  }

  if (!data.engagementID) {
    callback({code: 1, msg: 'Missing Engagement ID'})
    return
  }

  Utility.createCategory(data, data.createdBy, function (err, result) {
    if (err) {
      callback(err)
    } else {
      callback({code: 0, result: result})
    }
  })
}

module.exports = CreateCategory
