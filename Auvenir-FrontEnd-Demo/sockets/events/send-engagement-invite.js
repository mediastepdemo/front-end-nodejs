const Models = require('../../models')
const Utility = require('../../plugins/utilities/utility')
const debug = require('debug')('auvenir:event:send-engagement-invite')
const async = require('async')
const appConfig = require('../../config')

module.exports = function ({engagementId, auditor, email, currentUserID, protocol, domain, port}, callback) {
  debug('Executing send-engagement-invite event')

  if (!currentUserID) {
    return callback({code: 1, msg: 'socket.customData.authID is unavailable'})
  }

  if (!engagementId) {
    return callback({code: 1, msg: 'Missing engagement id'})
  }

  if (!auditor) {
    return callback({code: 1, msg: 'Missing auditor information'})
  }

  if (currentUserID.toString() !== auditor._id) {
    return callback({code: 403, msg: 'source user is not matched with current user'})
  }

  if (!email) {
    return callback({code: 1, msg: 'Missing client email to send the invitation'})
  }

  if (!Utility.isEmailValid(email)) {
    return callback({code: 1, msg: 'Destination email address is invalid'})
  }

  const findEngagementById = (cbk) => {
    Models.Auvenir.Engagement.findById(engagementId, (err, engagement) => {
      if (err) {
        return cbk({code: 500, msg: 'Error on retrieving engagement'})
      }
      if (!engagement) {
        return cbk({code: 404, msg: 'Unable to find engagement information'})
      }
      cbk(null, engagement)
    })
  }

  const ensureClient = (engagement, cbk) => {
    Models.Auvenir.User.findOne({email: email}, (err, clientUser) => {
      if (err) {
        return cbk({code: 500, msg: 'Error on retrieving client'})
      }
      if (!clientUser) {
        return cbk({code: 404, msg: `cannot find user with email: ${email}`})
      }

      var token = Utility.secureRandomString(32)

      if (!clientUser.auth) {
        clientUser.auth = {}
      }
      var expiryTime = Date.now() + Utility.SESSION_LENGTH
      clientUser.auth = Object.assign({}, clientUser.auth, {
        access: {
          token: Utility.createHash(token),
          expires: expiryTime
        }
      })

      return clientUser.save((err, user) => {
        debug('client user updated', user)
        cbk(err, user, engagement, token)
      })
    })
  }

  const inviteClient = (clientUser, engagement, token, cbk) => {
    engagement.inviteClient(auditor._id, clientUser._id.toString(), (err, newEngagement) => {
      let activityData = {}
      activityData.currentUserID = currentUserID
      activityData.engagementID = engagement._id
      activityData.operation = 'INVITE'
      activityData.type = 'User'
      activityData.original = {}
      activityData.updated = clientUser
      cbk(err, clientUser, newEngagement, token, activityData)
    })
  }

  const createActivity = (clientUser, engagement, token, cbk) => {
    Utility.createActivity(currentUserID, engagementId, 'INVITE', 'User', {}, clientUser, (err, res) => {
      if (res) {
        callback({'name': 'create-activity', result: res.result, code: 0})
      }
      cbk(err, clientUser, engagement, token)
    })
  }

  const sendInvitationEmail = ({firstName, lastName}, {_id, name}, token, activityData, cbk) => {
    let auditorLastNameLetter = ''
    auditorLastNameLetter = (auditor.lastName && auditor.lastName.trim()) ? auditor.lastName.trim().charAt(0) : ''
    Utility.sendEmail({
      fromEmail: 'andi@auvenir.com',
      toEmail: email,
      toName: `${firstName} ${lastName}`,
      status: 'sendEngagementInviteLeadClient',
      customValues: {
        url: appConfig.publicServer.url,
        token: token,
        email: email,
        engagementID: _id,
        engagementName: name,
        inviteeFirstName: firstName,
        inviteeLastName: lastName,
        inviterFirstName: auditor.firstName,
        inviterLastName: auditorLastNameLetter
      }
    }, function (err, result) {
      debug(`Invitation send Result ${JSON.stringify(result)}`)
      cbk(err, activityData)
    })
  }

  async.waterfall([findEngagementById, ensureClient, inviteClient, sendInvitationEmail], (err, activityData) => {
    if (err) {
      if (err.code && err.msg) {
        return callback(err)
      }
      return callback({code: 500, msg: 'Error on creating the engagement'})
    }
    callback({code: 0, engagementId, email, activityData})
  })
}
