var Models = require('../../models')
var Code = require('../../plugins/utilities/code')
var async = require('async')
const ObjectId = require('mongodb').ObjectID
const debug = require('debug')('auvenir:event:login')
const initLogger = require('../../plugins/logger/logger').init
const {info, warn, error} = initLogger(__filename)
const _ = require('lodash')

var Login = function (data, callback) {
  async.waterfall([
    function findUser (cbk) {
      Models.Auvenir.User.findOneAndUpdate({email: data.email}, {$set: {lastLogin: new Date()}}, {new: true}, function (err, user) {
        if (err) {
          return cbk(err)
        }
        if (!user) {
          return cbk({code: Code.ERROR_GENERAL, msg: 'No User Exists with that email'})
        }
        // @TODO: DATA RETURNED FOR USER SHOULD *NOT* INCLUDE PASSWORD HASH
        if (~['LOCKED', 'INACTIVE', 'WAIT-LIST'].indexOf(user.status)) {
          return cbk({code: Code.ERROR_INVALID_SESSION, msg: 'Account Inactive', result: {user: user}})
        }
        if (user.status === 'PENDING') {
          return cbk({code: Code.ERROR_INVALID_SESSION, msg: 'Account Pending Approval', result: {user: user}})
        }
        cbk(null, user.toJSON())
      })
    },
    function retrieveRelatedInfo (userObj, cbk) {
      async.parallel({
        firm: function (cbk) {
          if (~['CLIENT', 'ADMIN'].indexOf(userObj.type)) { // client and admin doesn't have firms
            return cbk(null)
          }
          Models.Auvenir.Firm.findOne({acl: {$elemMatch: {id: ObjectId(userObj._id)}}}, function (err, firm) {
            if (err) {
              return cbk(err)
            }
            if (!firm) {
              return cbk({code: Code.ERROR_GENERAL, msg: 'No matching firm for auditor with id: ' + userObj._id})
            }
            cbk(null, firm)
          })
        },
        business: function (cbk) {
          if (~['AUDITOR', 'ADMIN'].indexOf(userObj.type)) { // auditor and admin doesn't have business
            return cbk(null)
          }
          Models.Auvenir.Business.findOne({acl: {$elemMatch: {id: userObj._id}}}, function (err, business) {
            if (err) {
              return cbk(err)
            }
            if (!business) {
              return cbk({
                code: Code.ERROR_GENERAL,
                msg: 'No matching business for client with id: ' + userObj._id
              })
            }
            cbk(null, business)
          })
        },
        engagements: function (cbk) {
          if (userObj.type !== 'ADMIN') {
            Models.Auvenir.Engagement.findByAclUser({id: userObj._id, role: userObj.type}, function (err, engagements) {
              if (err) {
                return cbk(err)
              }
              async.map(engagements, function (engagement, cbk) {
                engagement.fetchAdditionalInfo((err, engagementObj) => {
                  cbk(err, engagementObj)
                })
              }, cbk)
            })
          } else {
            cbk(null, [])
          }
        }
      }, function (err, results) {
        if (err) {
          return cbk(err)
        }
        return cbk(null, userObj, results.business || results.firm, results.engagements)
      })
    }
  ], function (err, userObj, company, engagements) {
    if (err) {
      error(err)
      if (err.code) {
        return callback(err)
      } else {
        return callback({code: Code.ERROR_GENERAL, msg: 'Error on retrieving your data'})
      }
    }
    debug(userObj)
    debug(company)
    debug(engagements)

    if (userObj.type === 'ADMIN') {
      callback({
        code: Code.NOERROR,
        result: {
          user: userObj
        }
      })
    } else if (userObj.type === 'AUDITOR') {
      callback({
        code: Code.NOERROR,
        result: {
          user: userObj,
          firm: company,
          engagements
        }
      })
    } else if (userObj.type === 'CLIENT') {
      callback({
        code: Code.NOERROR,
        result: {
          user: userObj,
          business: company,
          engagements
        }
      })
    }
  })
}

module.exports = Login
