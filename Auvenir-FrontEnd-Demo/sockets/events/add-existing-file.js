const Access = require('../../plugins/utilities/accessControls')
const initLogger = require('../../plugins/logger/logger').init
const { info, warn, error } = initLogger(__filename)

const AddExistingFile = function (data, callback) {
  warn('EVENT: add-existing-file event is running...')
  if (!data) {
    return callback({code: 1, msg: 'Data was not being passed.'})
  }

  let { currentUserID, engagementID, todoID, requestID, fileID } = data

  if (!currentUserID || !engagementID || !requestID || !todoID || !fileID) {
    return callback({code: 1, msg: 'Not all valid data were passed.'})
  }

  // Don't have to cast ids to objIDs because Access is doing that for us.
  Access.request(requestID, todoID, engagementID, currentUserID, (err, engagement) => {
    if (err) {
      error({ err })
      return callback({code: 1, msg: 'There was an error while adding a file to the request.'})
    }

    engagement.addFileToRequest(requestID, todoID, fileID, (err) => {
      if (err) {
        error({ err })
        return callback({code: 1, msg: 'There was an error while uploading a file to the request.'})
      }

      engagement.fetchAdditionalInfo((err, updatedEngagement) => {
        if (err) {
          error({ err })
          return callback({code: 1, msg: 'There was an error while uploading a file to the request.'})
        }
        return callback({code: 0, engagement: updatedEngagement, requestID, fileID, msg: 'Adding file to the request was successful.'})
      })
    })
  })
}
module.exports = AddExistingFile
