const Models = require('../../models')
const Utility = require('../../plugins/utilities/utility')
const debug = require('debug')('auvenir:event:add-team-member')
const async = require('async')

var RemoveTeamMember = function (data, callback) {
  debug('Executing remove-team-member event')
  const {userID, engagementID} = data

  const removeUserFromEngagement = (cbk) => {
    Utility.removeUserFromEngagement(userID, engagementID, function (err, result) {
      if (err) {
        return cbk(err)
      } else {
        if (result && !result.engagement) {
          return cbk({code: 404, msg: `Cannot find engagement with id ${engagementID}`})
        } else {
          let activityData = {}
          let modifiedUser = result.user.toObject()
          modifiedUser.name = modifiedUser.firstName ? modifiedUser.firstName + ' ' + modifiedUser.lastName : modifiedUser.email
          activityData.engagementID = result.engagement._id
          activityData.operation = 'DELETE'
          activityData.type = 'Engagement-user'
          activityData.original = modifiedUser
          activityData.updated = {}
          cbk(null, {engagement: result.engagement, activityData})
        }
      }
    })
  }

  const fetchAdditionalInfo = ({engagement, activityData}, cbk) => {
    engagement.fetchAdditionalInfo((err, data) => {
      if (err) {
        return cbk(err)
      }
      cbk(err, data, activityData)
    })
  }

  async.waterfall([removeUserFromEngagement, fetchAdditionalInfo], (err, engagement, activityData) => {
    if (err) {
      if (err.code && err.msg) {
        return callback(err)
      }
      return callback({code: 500, msg: 'Cannot add acl to engagement'})
    }

    callback({code: 0, result: {engagement, activityData}})
  })
}

module.exports = RemoveTeamMember
