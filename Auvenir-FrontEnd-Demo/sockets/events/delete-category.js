var Utility = require('../../plugins/utilities/utility')

var DeleteCategory = function (data, callback) {
  if (!data.engagementData.currentUserID) {
    callback({code: 1, msg: 'Current User is not defined.'})
    return
  }

  if (!data.engagementData.engagementID) {
    callback({code: 1, msg: 'Missing Engagement ID'})
    return
  }

  Utility.deleteCategories(data.categories, data.engagementData.currentUserID, data.engagementData.engagementID, function (err, result) {
    if (err) {
      callback(err)
    } else {
      callback({code: 0, result: result})
    }
  })
}

module.exports = DeleteCategory
