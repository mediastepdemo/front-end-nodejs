var Utility = require('../../plugins/utilities/utility')

var CreateTodo = function (data, callback) {
  if (!data.createdBy) {
    callback({code: 1, msg: 'Current User is not defined.'})
    return
  }

  if (!data.engagementID) {
    callback({code: 1, msg: 'Missing Engagement ID'})
    return
  }

  Utility.createTodo(data, data.createdBy, function (err, result) {
    if (err) {
      return callback(err)
    } else {
      let activityData = {}
      activityData.currentUserID = data.createdBy
      activityData.engagementID = data.engagementID
      activityData.operation = 'CREATE'
      activityData.type = 'Todo'
      activityData.original = {}
      activityData.updated = result.todos[result.todos.length - 1]
      result.activityData = activityData
      return callback({code: 0, result: result})
    }
  })
}

module.exports = CreateTodo
