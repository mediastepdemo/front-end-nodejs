var Utility = require('../../plugins/utilities/utility')

var UpdateFirm = function (data, callback) {
  if (!data.currentUserID) {
    callback({code: 1, msg: 'No Current User.'})
    return
  }

  if (!data.firmID) {
    callback({code: 1, msg: 'Missing Firm ID'})
    return
  }

  var currentUserID = data.currentUserID

  Utility.updateFirm(data, currentUserID, function (err, result) {
    if (err) {
      callback(err)
    } else {
      callback({code: 0, data: data, result: result})
    }
  })
}

module.exports = UpdateFirm
