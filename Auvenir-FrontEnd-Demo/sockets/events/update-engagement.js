var Utility = require('../../plugins/utilities/utility')

var UpdateEngagement = function (data, callback) {
  if (!data.currentUserID) {
    return callback({code: 1, msg: 'Missing Current User'})
  }

  if (!data.engagementID) {
    return callback({code: 1, msg: 'Missing Engagement ID.'})
  }

  Utility.updateEngagement(data, (err, response) => {
    if (err) {
      return callback(err)
    }

    callback(response)
  })
}
module.exports = UpdateEngagement
