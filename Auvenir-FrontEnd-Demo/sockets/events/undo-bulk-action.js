var Utility = require('../../plugins/utilities/utility')

var UndoBulkAction = function (data, callback) {
  if (!data.currentUserID) {
    callback({code: 1, msg: 'Current User is not defined.'})
    return
  }

  Utility.undoBulkAction(data.undoTodos, data.currentUserID, data.engagementID, function (err, result) {
    if (err) {
      callback(err)
    } else {
      callback({code: 0, result: result})
    }
  })
}

module.exports = UndoBulkAction
