/* eslint-disable handle-callback-err */
/**
 * Created by Son Pham on 7/25/2017.
 */

var Utility = require('../../plugins/utilities/utility')
var request = require('request')
const debug = require('debug')('auvenir:event:load-test-type')

var LoadWorkingPaperType = function (data, callback) {
  var options = {
    url: 'http://192.168.1.162:8080/api/wp-type',
    headers: {
      'Accept': 'application/json',
    }
  }

  request.get(options, function (error, response, body) {
    callback({code: 0, result: body, page: 'working-paper-step-1'})
  })
}

module.exports = LoadWorkingPaperType
