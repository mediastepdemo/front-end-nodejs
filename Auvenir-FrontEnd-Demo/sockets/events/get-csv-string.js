const Models = require('../../models')
const Utility = require('../../plugins/utilities/utility')
const async = require('async')
const debug = require('debug')('auvenir:event:get-csv-string')
const json2csv = require('json2csv')

var GetCSVString = function (data, callback) {
  debug('Executing get-csv-string event')
  const {dataList, fields} = data

  let resultCSV = ''
  if (dataList.length > 0) {
    resultCSV = json2csv({data: dataList, fields: fields})
  }
  return callback({code: 0, result: resultCSV})
}

module.exports = GetCSVString
