const logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = logger.init(__filename)
const socketIO = require('socket.io')
const cookie = require('cookie')
const cookieParser = require('cookie-parser')
const async = require('async')
const _ = require('lodash')
const moment = require('moment')
const debug = require('debug')('auvenir-core:socket')
const Events = require('./events')
const Models = require('../models')
const finicityErrorCodes = require(rootDirectory + '/finicity/plugins/errorCodes')

function lookUpUserBySession (redis, sessionId, cbk) {
  async.waterfall([
    function (cbk) {
      redis.get('sess:' + sessionId, function (err, result) {
        if (err || !result) {
          return cbk(err || new Error('Your session could not be found. Please refresh the page.'))
        }
        var r
        try {
          r = JSON.parse(result)
          cbk(null, r)
        } catch (err) {
          cbk(err)
        }
      })
    },
    function (result, cbk) {
      if (!result.passport || !result.passport.user || !result.passport.user.id) {
        return cbk(new Error('Invalid Session'))
      }
      Models.Auvenir.User.findById({_id: result.passport.user.id}, function (err, user) {
        if (err || !user) {
          return cbk(err || new Error('Cannot find user'))
        }
        cbk(null, user)
      })
    }
  ], cbk)
}

function PubSub (io, subscriber, publisher) {
  function getConnectedSocketByUserId (sockets, userId) {
    var userSocket = null
    _.forEach(_.keys(sockets), function (socketId) {
      var socket = sockets[socketId]
      if (socket.user && socket.user._id.toString() === userId.toString()) {
        userSocket = socket
      }
    })
    return userSocket
  }

  subscriber.on('unsubscribe', function (userId, count) {
    info(`redis client is unsubscribed on userId ${userId}. current online: ${count}`)
  })
  subscriber.on('subscribe', function (userId, count) {
    info(`redis client is subscribed on userId ${userId}. current online: ${count}`)
  })
  subscriber.on('message', function (userId, message) {
    var connectedSocket = getConnectedSocketByUserId(io.sockets.connected, userId)
    if (!connectedSocket) {
      return
    }
    var msgObj = JSON.parse(message)
    info(`redis subscriber receives event ${msgObj.eventName} for ${userId}`)
    connectedSocket.emit(msgObj.eventName, msgObj.payload)
  })

  this.pub = function (userId, payload) {
    info(`redis publisher publish event ${payload.eventName}`)
    publisher.publish(userId, JSON.stringify(payload))
  }
  this.sub = function (userId) {
    subscriber.subscribe(userId)
  }
  this.unSub = function (userId) {
    subscriber.unsubscribe(userId)
  }
}

let io = null
let pubSub = null
let finicity = null

module.exports = {
  init: function (httpsServer, redis, subscriber, publisher, appConfig, finicityArg) {
    io = socketIO(httpsServer)
    pubSub = new PubSub(io, subscriber, publisher)
    io.use(function (socket, next) {
      var handshake = socket.request
      if (!handshake || !handshake.headers || !handshake.headers.cookie) {
        return next(new Error('No cookie transmitted.'))
      }
      var cookies = cookie.parse(handshake.headers.cookie)
      var sessionID = cookieParser.signedCookie(cookies['connect.sid'], 'mySecretKey')

      lookUpUserBySession(redis, sessionID, function (err, user) {
        if (err) {
          warn('lookup user failed', {err})
          return next(err)
        }
        socket.user = user
        next()
      })
    })
    finicity = finicityArg

    function onConnection (socket) {
      function eventCallback (name, data) {
        if (name === 'login') {
          if (data.code !== 0) {
            return socket.emit('login-response', data)
          }

          return socket.emit('login-response', {code: 0, result: data.result})
        }
        var notify = data.result ? data.result.notify : null
        if (data.code === 0 && notify && notify.userId && notify.eventName && notify.payload) {
          pubSub.pub(notify.userId.toString(), {
            eventName: notify.eventName,
            payload: notify.payload
          })
        }
        data.name = name
        if (!data.code || data.code === finicityErrorCodes.api.OK.code) {
          info(`WebSocket event: ${name} response to ${socket.user.email}`)
        } else {
          warn(`WebSocket event: ${name} failed.`, data)
        }
        socket.emit('event-response', data)
      }
      socket.on('disconnect', function () {
        pubSub.unSub(socket.user._id.toString())
        info('User Disconnected id: ' + socket.id + ' userId: ' + socket.user._id)
      })
      socket.on('error', function (err) {
        error('WebSocket connection error', {err})
        socket.emit('server-error', {error: err.toString(), stack: err.stack})
      })
      pubSub.sub(socket.user._id.toString())
      info(`WebSocket user ${socket.user.email} connected`)
      Events['login']({email: socket.user.email}, function (result) {
        eventCallback('login', result)
      })
      // Handle all socket API calls for this Application.
      socket.on('event', function (payload) {
        let apiCallName = payload && payload.name
        let eventCall = Events[apiCallName]
        let finCall = finicity[apiCallName]
        let data = payload.data
        if ((!eventCall && !finCall) || !data) {
          warn('Invalid API call format', payload)
          return socket.emit('event-response', {
            name: 'unknown',
            code: 11,
            result: {msg: 'Invalid call format', tagName: apiCallName}
          })
        }
        info(`API call '${apiCallName}' triggered by ${socket.user.email}`)
        switch (apiCallName) {
          case 'send-engagement-invite':
          case 'verify-auditor':
          case 'onboard-user':
            const serverConfig = appConfig.publicServer
            data.port = serverConfig.port
            data.protocol = serverConfig.protocol
            data.domain = serverConfig.domain
            data.url = serverConfig.url
            break
        }
        data.currentUserID = socket.user._id
        if (socket && socket.customData) {
          data.socketData = {authID: socket.customData.authID}
        }
        if (eventCall) {
          eventCall(data, (result) => {
            result.payload = data
            eventCallback(result.name || apiCallName, result)
          })
        } else {
          data.code = 0
          finicity[apiCallName](data, (result) => {
            eventCallback(result.name || apiCallName, result)
          })
        }
      })
    }
    io.on('connection', onConnection)
    debug('socket.io server initialized')
  },
  getSockets: function () {
    return io.sockets
  }
}
