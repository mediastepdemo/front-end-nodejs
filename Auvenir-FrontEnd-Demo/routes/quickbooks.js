/* globals Logger */

// Lood all the required code.
var express = require('express')
var router = express.Router()
var request = require('request')
var qs = require('querystring')
var fs = require('fs')
var moment = require('moment')
var Quickbooks = require('node-quickbooks')
var Utility = require('../plugins/utilities/utility')
var Models = require('../models')
var Crypt = require('../plugins/utilities/cryptography')

// Information used to connect to QuickBooks.
var QUICKBOOKS_CURRENT_USER = 'https://appcenter.intuit.com/api/v1/user/current'
var consumerKey = 'qyprdmtKR7nlFRa1RMN4MoLNcza0hm'           // Production
var consumerSecret = '5pi3JyXswMw3AANmAsproTC6FXiKmCfEN04ncERU' // Production
var useSandbox = false
// var consumerKey    = 'qyprd3CxXhaOrqFiiZDOx9VJEAGTrK';           // Sandbox
// var consumerSecret = 'Qjjaspk3irYB9H9qaKas3zRejQkDMNiiF2e1M161'; // Sandbox
// var useSandbox     = true;
var qboDebug = false

// Name of this file to be included in all Logger logs.
var fileName = 'quickbooks'

// Path to where any files are to be written.
var dataFolderPath = '/server/data/'

// Hold information read from the Auvenir database.
var theBusinessInfo = null

// Hold information retrieved from QuickBooks Online.
var qboCompanyInfo = null
var qboBalanceSheet = null
var qboTrialBalance = null
var qboOpenBalance = null
var qboAccounts = null
var qboProfitLoss = null
var qboGeneralLedger = null
var qboPreferences = null

// Some other global variables.
var numGenLedgerCols = 0
var usingMultiCurrency = false
var headerIds = {}
var accountIds = {}
var linesToProcess = {}

var numberTxns = 0
var numberLines = 0
var numberHeaders = 0

var isAuthenticated = function (req, res, next) {
  Logger.log(fileName + ': IS CLIENT AUTHENTICATED')
  if (req.isAuthenticated()) {
    Logger.log(fileName + ': Basic Authentication')

        // var portalPermissions = req.user.accountPermissions;
        // for(var i=0; i<portalPermissions.length; i++) {
        //    if(portalPermissions[i].portal === 'CLIENT')
    return next()
        // }
  }
    // if the user is not authenticated then redirect him to the login page
  res.redirect('/')
}

/**
 * Save the account in the database.
 *
 * The object accountIds is used to track the account ObjectId, opening and
 * closing balances, and the account reference Id. When a line is saved in the
 * DB it needs the ObjectId of an account. It can get it from this object and
 * not have to read from the database.
 *
 * @param  {ObjectId} engagementId - the Id of the current engagement
 * @param  {JSON}     account      - an object containing the account information
 * @param  {Function} callback     - the callback function
 * @return {}                      - nothing
 */
var saveAccount = function (engagementId, account, callback) {
    // Get a new object and add the elements that are not optional from the QBO data.
  var dbAccount = new Models.Quickbooks.ChartAccount()
  dbAccount.engagementID = engagementId
  dbAccount.referenceId = account.Id
  dbAccount.name = account.Name
  dbAccount.nameFull = account.FullyQualifiedName
  dbAccount.classification = account.Classification
  dbAccount.currentBalance = account.CurrentBalance
  dbAccount.totalBalance = account.CurrentBalanceWithSubAccounts
  dbAccount.currency = account.CurrencyRef.value
  dbAccount.createTime = account.MetaData.CreateTime
  dbAccount.lastUpdateTime = account.MetaData.LastUpdatedTime
  dbAccount.active = account.Active

    // Add the optional elements from the QBO data.
  var key = ''
  if (account.AcctNum) {
    dbAccount.number = account.AcctNum
    key = account.AcctNum + ' '
  }
  if (account.AccountType) {
    dbAccount.category = account.AccountType
  }
  if (account.AccountSubType) {
    dbAccount.subCategory = account.AccountSubType
  }
  if (account.SubAccount) {
    dbAccount.parentReferenceId = account.ParentRef.value
  }

  key += account.FullyQualifiedName
  if (!(key in accountIds)) {
    accountIds[key] = {}
    accountIds[key].close = 0
    accountIds[key].open = 0
  }
  dbAccount.closeBalance = Number(accountIds[key].close)
  dbAccount.openBalance = Number(accountIds[key].open)

    // Save the document in the collection and then do the next account.
  dbAccount.save(function (err, save) {
    if (err) {
      var error = 'Error saving account in the Auvenir database!'
      Logger.error(fileName + ': ' + error)
      Logger.error(err)
      callback(error)
    } else {
      if (save) {
        key = ''
        if (save.number) {
          key = save.number + ' '
        }
        key += save.nameFull
        accountIds[key].id = save._id
      }
      callback(null)
    }
  })
}

/**
 * Process one account for saving to the database. For each account saved,
 * decrement the number or remaining accounts to be saved. This is used to
 * decide when to call the callback function.
 *
 * @param  {ObjectId} engagementId - the Id of the current engagement
 * @param  {JSON}     account      - an object containing the account information
 * @param  {JSON}     remaining    - an object that counts the number of transactions
 *                                   that are still in the process of being saved
 *                                   to the database. An object is being used
 *                                   because it is passed by reference.
 * @param  {Function} callback     - the callback function
 * @return {}                      - nothing
 */
var processOneAccount = function (engagementId, account, remaining, callback) {
  saveAccount(engagementId, account, function (err) {
    remaining.count--
    if (err) {
      if (!remaining.error) {
        remaining.error = true
        callback(err)
      }
    } else {
      if (remaining.count <= 0 && !remaining.error) {
        callback(null)
      }
    }
  })
}

/**
 * Process the Trial Balance report. The report contains a list of those
 * accounts that have a none zero closing balance.
 *
 * The object accountIds is used to track the account ObjectId, opening and
 * closing balances, and the account reference Id.
 *
 * @return {JSON} - an object containg a code and possibly a message.
 */
var processTrialBalance = function () {
    // Make sure the Column information is available.
  if (!qboTrialBalance.Columns || !qboTrialBalance.Columns.Column) {
    return {code: 1000, msg: 'Trial balance report missing column data!'}
  }

    // Get the index of the columns that we need.
  var index = {}
  var numberColumns = qboTrialBalance.Columns.Column.length
  for (var i = 0; i < numberColumns; i++) {
    switch (qboTrialBalance.Columns.Column[i].ColTitle) {
      case '':
        index.account = i
        break
      case 'Debit':
        index.debit = i
        break
      case 'Credit':
        index.credit = i
        break
    }
  }

    // Make sure the Rows information is available.
  if (!qboTrialBalance.Rows) {
    return {code: 1001, msg: 'Trial balance report missing row data!'}
  }

    // If there is no Row, then there are no accounts with a closing balance.
  if (!qboTrialBalance.Rows.Row) {
    return {code: 0}
  }

    // Go through each account to get the account name, id and debit or credit.
  var numberAccounts = qboTrialBalance.Rows.Row.length
  for (i = 0; i < numberAccounts; i++) {
    var account = qboTrialBalance.Rows.Row[i]
    if (!account.type && account.ColData) {
      var key = account.ColData[index.account].value
      if (!(key in accountIds)) {
        accountIds[key] = {}
        accountIds[key].open = 0
      }
      accountIds[key].refId = account.ColData[index.account].id
      if (account.ColData[index.debit].value !== '') {
        accountIds[key].close = account.ColData[index.debit].value
      } else {
        accountIds[key].close = account.ColData[index.credit].value
      }
    }
  }

  return {code: 0}
}

/**
 * Process one Opening Balance. It is possible that there can be sub accounts.
 *
 * @param  {JSON}   index      - an object that contains the array index of
 *                               each of the columns in the account array
 * @param  {String} key        - the name of the account
 * @param  {Array}  theAccount - an array that contains the account information
 * @return {JSON}              - an object containg a code and possibly a message.
 */
var processOneOpenBalance = function (index, key, theAccount) {
    // Loop through the account information looking for the Beginning Balance.
  var numberColumns = theAccount.length
  for (var i = 0; i < numberColumns; i++) {
        // Check if there are subaccounts.
    var accountInfo = theAccount[i]
    if (accountInfo.type && accountInfo.type === 'Section') {
            // Make sure there is information for the sub account.
      if (!accountInfo.Rows || !accountInfo.Rows.Row) {
        return {code: 2004, msg: 'Opening balance report missing sub account information!'}
      }

            // Recursively process the sub account.
      var result = processOneOpenBalance(index, accountInfo.Rows.Row)
      if (result.code !== 0) {
        return {code: result.code, msg: result.msg}
      }
      continue
    }

        // The Row type must be 'Data'.
    if (accountInfo.type !== 'Data') {
      continue
    }

        // Looking only for the Beginning Balance.
    if (accountInfo.ColData[index.account].value === 'Beginning Balance') {
      accountIds[key].open = accountInfo.ColData[index.balance].value
      break
    }
  }

  return {code: 0}
}

/**
 * Process the Opening Balance report. The report contains a list of those
 * accounts that have a none zero opening balance.
 *
 * The object accountIds is used to track the account ObjectId, opening and
 * closing balances, and the account reference Id.
 *
 * @return {JSON} - an object containg a code and possibly a message.
 */
var processOpenBalance = function () {
    // Make sure the Column information is available.
  if (!qboOpenBalance.Columns || !qboOpenBalance.Columns.Column) {
    return {code: 2000, msg: 'Opening balance report missing column data!'}
  }

    // Get the index of the columns that we need.
  var index = {}
  var numberColumns = qboOpenBalance.Columns.Column.length
  for (var i = 0; i < numberColumns; i++) {
    switch (qboOpenBalance.Columns.Column[i].ColTitle) {
      case 'Account':
        index.account = i
        break
      case 'Balance':
        index.balance = i
        break
    }
  }

    // Make sure there are Rows available.
  if (!qboOpenBalance.Rows) {
    return {code: 2001, msg: 'Opening balance report missing row data!'}
  }

    // If there is no Row, then there are no accounts with an opening balance.
  if (!qboOpenBalance.Rows.Row) {
    return {code: 0}
  }

    // Go through each account to get the account name, id and opening balance.
  var numberAccounts = qboOpenBalance.Rows.Row.length
  for (i = 0; i < numberAccounts; i++) {
        // For an account, the Row type must be 'Section'
    var accountInfo = qboOpenBalance.Rows.Row[i]
    if (accountInfo.type !== 'Section') {
      continue
    }

        // Must have a header. The header has the account name.
    if (!accountInfo.Header || !accountInfo.Header.ColData) {
      return {code: 2002, msg: 'Opening balance report missing account name!'}
    }

        // Make sure there is account.
    if (!accountInfo.Rows || !accountInfo.Rows.Row) {
      return {code: 2003, msg: 'Opening balance report missing account information!'}
    }

    var key = accountInfo.Header.ColData[index.account].value
    if (!(key in accountIds)) {
      accountIds[key] = {}
      accountIds[key].close = 0
    }

        // Now process the opening balance for this account.
    var result = processOneOpenBalance(index, key, accountInfo.Rows.Row)
    if (result.code !== 0) {
      return {code: result.code, msg: result.msg}
    }
  }

  return {code: 0}
}

/**
 * Save the accounts in the ChartAccount collection.
 *
 * The Trial Balance and Opening Balance reports must both be available.
 *
 * @param  {ObjectId} engagementId  - the Id of the current engagement
 * @param  {Function} callback      - the callback function
 * @return {}                       - nothing
 */
var saveTheAccounts = function (engagementId, callback) {
  Logger.log(fileName + ': Add accounts to ChartAccounts!')

    // Make sure there are accounts, trial balance and opening balances.
  if (!qboAccounts && !qboTrialBalance && !qboOpenBalance) {
    callback('Missing one or more of the QBO account, trial balance or open balance reports!')
    return
  }

    // Get the Account closing balances from the Trial Balace report.
  accountIds = {}
  var result = processTrialBalance()
  if (result.code !== 0) {
    callback(result.msg)
    return
  }

    // Get the Opening balances from the special General Ledger report.
  result = processOpenBalance()
  if (result.code !== 0) {
    callback(result.msg)
    return
  }

    // Now all the QBO accounts to add them to the database.
  var remaining = {'count': 0, 'error': false}
  var numberAccounts = qboAccounts.length
  for (var i = 0; i < numberAccounts && !remaining.error; i++) {
    remaining.count++
    processOneAccount(engagementId, qboAccounts[i], remaining, callback)
  }
}

/**
 * Save the transaction line in the database. A line can only be saved in the DB
 * if its header has already been saved. This is because the line has a reference
 * (ObectId) to the header. So if the header has not yet been saved, the line is
 * queued for later processing.
 *
 * @param  {ObjectId} engagementId   - the Id of the current engagement
 * @param  {JSON}     index          - an object that contains the array index of
 *                                     each of the columns in the transaction array
 *  @param {Array}    theTransaction - an array that contains the parts of the
 *                                     transaction
 * @param  {Function} callback       - the callback function
 * @return {}                        - nothing
 */
var saveLine = function (engagementId, index, theTransaction, callback) {
    // To proceed, must have the objectID of the header.
  if (!headerIds[theTransaction[index.txnType].id].id) {
    if (!linesToProcess[theTransaction[index.txnType].id]) {
      linesToProcess[theTransaction[index.txnType].id] = []
    }
    linesToProcess[theTransaction[index.txnType].id].push({'txn': theTransaction, 'callback': callback})
    return
  }

    // Deternine if the amount is a debit or credit.
  var amount, postType
  if (theTransaction[index.debit].value !== '') {
    amount = theTransaction[index.debit].value
    postType = 'Debit'
  } else {
    amount = theTransaction[index.credit].value
    postType = 'Credit'
  }

    // Create the new line object.
  var line = new Models.Quickbooks.Line()
  line.engagementID = engagementId
  line.accountID = accountIds[theTransaction[index.account].value].id
  line.lineNumber = headerIds[theTransaction[index.txnType].id].lineNum++
  line.details = theTransaction[index.memo].value
  line.amount = Number(amount)
  line.postType = postType

  line.headerID = headerIds[theTransaction[index.txnType].id].id
  numberLines++
  line.save(function (err, newLine) {
    var error = null
    if (err) {
      error = 'Error saving new line in the Auvenir database!'
      Logger.error(fileName + ': ' + error)
      Logger.error(err)
    }

    callback(error)
  })
}

/**
 * Process and lines that have been queued.
 *
 * @param  {ObjectId} engagementId - the Id of the current engagement
 * @param  {JSON}     index        - an object that contains the array index of
 *                                   each of the columns in the transaction array
 * @param  {JSON}     headerId     - an object to track the header OjbectId's
 * @return {}                      - nothing
 */
var processQueuedLines = function (engagementId, index, headerId) {
  var lines = linesToProcess[headerId]
  if (lines) {
    while (lines.length > 0) {
      var lineToSave = lines.shift()
      saveLine(engagementId, index, lineToSave.txn, lineToSave.callback)
    }
  }
}

/**
 * Take the transaction and save it in the database as a header and a line.
 *
 * An object is used to track the header number (the QBO txnType) and the
 * ObjectId once it is save in the DB. Using this object it is possible to know
 * if a header has been saved to the DB. And when a line is to be save, the
 * header ObjectId can be obtained from this object instead of read from the DB.
 *
 * @param  {ObjectId} engagementId    - the Id of the current engagement
 * @param  {ObjectId} ledgerId        - the Id of the Ledger for the current engagement
 * @param  {JSON}     index           - an object that contains the array index of
 *                                      each of the columns in the transaction array
 * @param  {Array}    theTransaction  - an array that contains the parts of the
 *                                      transaction
 * @param  {Function} callback        - the callback function
 * @return {}                         - nothing
 */
var saveHeaderLine = function (engagementId, ledgerId, index, theTransaction, callback) {
  var savedHeader = true
  var savedLine = false
  var executed = false
  var error = null

  var headerLineDone = function () {
    if (!savedHeader || !savedLine || executed) {
      return
    }

    executed = true
    callback(error)
  }

    // Check to see if the header has already been save to the database.
  if (!(theTransaction[index.txnType].id in headerIds)) {
    savedHeader = false

        /*
         * The QBO Tx date is only YYYY-MM-DD. Need to add a time and offset
         * before saving to database. Get the offset from the create date.
         */
    var effective = moment(theTransaction[index.createDate].value)
    var offset = effective.utcOffset()
    var posted = moment(theTransaction[index.txDate].value)
    posted = posted.hour(12).utcOffset(offset)

        /*
         * The header information only needs to be saved once. So track
         * the QBO Txn Type id. Each transaction that has multiple lines
         * will have the same Txn Type id.
         */
    headerIds[theTransaction[index.txnType].id] = {}
    headerIds[theTransaction[index.txnType].id].lineNum = 1

        // Create the new header object.
    var header = new Models.Quickbooks.Header()
    header.engagementID = engagementId
    header.ledgerID = ledgerId
    header.effectiveDate = theTransaction[index.createDate].value
    header.postedDate = posted
    header.headerNumber = theTransaction[index.txnType].id
    header.description = theTransaction[index.txnType].value
    header.createdBy = theTransaction[index.createBy].value
    header.created = theTransaction[index.createDate].value
    header.modifiedBy = theTransaction[index.modifiedBy].value
    header.modified = theTransaction[index.modified].value

        // Save the header in the collection.
    numberHeaders++
    header.save(function (err, newHeader) {
      if (err) {
        var msg = 'Error saving new header in the Auvenir database!'
        Logger.error(fileName + ': ' + msg)
        Logger.error(err)
        error += msg
      } else {
        headerIds[newHeader.headerNumber].id = newHeader._id

                // Now prcess any lines queued waiting for this header.
        processQueuedLines(engagementId, index, newHeader.headerNumber)
      }

      savedHeader = true
      headerLineDone()
    })
  }

    // Save the line in the collection.
  saveLine(engagementId, index, theTransaction, function (err, newLine) {
    if (err) {
      error += err
    }

    savedLine = true
    headerLineDone()
  })
}

/**
 * Process the given General Ledger Transaction. For each transaction the
 * header and the line are saved in the database.
 *
 * @param  {ObjectId} engagementId   - the Id of the current engagement
 * @param  {ObjectId} ledgerId       - the Id of the Ledger for the current engagement
 * @param  {JSON}     index          - an object that contains the array index of
 *                                     each of the columns in the transaction array
 * @param  {JSON}     remaining      - an object that counts the number of transactions
 *                                     that are still in the process of being saved
 *                                     to the database. An object is being used
 *                                     because it is passed by reference.
 * @param  {array}    theTransaction - an array that contains the parts of the
 *                                     transaction
 * @param  {Function} callback       - the callback function
 * @return {}                        - nothing
 */
var processGlAccountTxn = function (engagementId, ledgerId, index, remaining, theTransaction, callback) {
  saveHeaderLine(engagementId, ledgerId, index, theTransaction, function (err) {
    remaining.count--
    if (err) {
      if (!remaining.error) {
        remaining.error = true
        callback(err)
      }
    } else {
      if (remaining.count <= 0 && !remaining.error) {
        Logger.log(fileName + ': **************************** Finished saving all the headers and lines')
        Logger.log(fileName + ': Number of transactons = ' + numberTxns)
        Logger.log(fileName + ': Number of lines = ' + numberLines)
        Logger.log(fileName + ': Number of headers = ' + numberHeaders)

        callback(null)
      }
    }
  })
}

/**
 * Process the given General Ledger account. For the account each transaction
 * in the account is processed.
 *
 * @param  {ObjectId} engagementId - the Id of the current engagement
 * @param  {ObjectId} ledgerId     - the Id of the Ledger for the current engagement
 * @param  {JSON}     index        - an object that contains the array index of
 *                                   each of the columns in the account array
 * @param  {JSON}     remaining    - an object that counts the number of transactions
 *                                   that are still in the process of being saved
 *                                   to the database. An object is being used
 *                                   because it is passed by reference.
 * @param  {array}    theAccount   - an array of transactions for a given account
 * @param  {Function} callback     - the callback function
 * @return {}                      - nothing
 */
var processOneGlAccount = function (engagementId, ledgerId, index, remaining, theAccount, callback) {
    // Loop through each transaction in the account.
  var numberTransactions = theAccount.length
  for (var j = 0; j < numberTransactions && !remaining.error; j++) {
    var transaction = theAccount[j]

        // Check if there are subaccounts.
    if (transaction.type && transaction.type === 'Section') {
            // Make sure there are transactions.
      if (!transaction.Rows || !transaction.Rows.Row) {
        callback()
      }
      processOneGlAccount(engagementId, ledgerId, index, remaining, transaction.Rows.Row, callback)
      continue
    }

        // For a transaction, the Row type must be 'Data'.
    if (transaction.type !== 'Data') {
      continue
    }

        // The transaction date must be valid.
    if (!moment(new Date(transaction.ColData[index.txDate].value)).isValid()) {
      continue
    }
    remaining.count++
    numberTxns++

        // Now process the transaction.
    processGlAccountTxn(engagementId, ledgerId, index, remaining, transaction.ColData, callback)
  }
}

/**
 * Process each account that is found in the General Ledger. Wihin each account
 * there is a list of transactions.
 *
 * @param  {ObjectId} engagementId - the Id of the current engagement
 * @param  {ObjectId} ledgerId     - the Id of the Ledger for the current engagement
 * @param  {JSON}     index        - an object that contains the array index of
 *                                   each of the columns in the glAccounts array
 * @param  {Array}    glAccounts   - an array of the accounts in the GL
 * @param  {Function} callback     - the callback function
 * @return {}                      - nothing
 */
var processGeneralLedgerAccounts = function (engagementId, ledgerId, index, glAccounts, callback) {
  numberTxns = 0
  numberLines = 0
  numberHeaders = 0

  headerIds = {}
  var remaining = {'count': 0, 'error': false}

    // Loop through each account in the General Ledger.
  var numberAccounts = glAccounts.length
  Logger.log(fileName + ': The number of accounts (not including sub accounts) in the General Ledger is: ' + numberAccounts)
  for (var i = 0; i < numberAccounts && !remaining.error; i++) {
        // For an account, the Row type must be 'Section'
    var acctInformation = glAccounts[i]
    if (acctInformation.type !== 'Section') {
      continue
    }

        // Make sure there are transactions.
    if (!acctInformation.Rows || !acctInformation.Rows.Row) {
      continue
    }

        // Now process this General Ledger account.
    Logger.log(fileName + ': The number of transactions in account: ' + acctInformation.Header.ColData[0].value + ' is: ' + acctInformation.Rows.Row.length)
    processOneGlAccount(engagementId, ledgerId, index, remaining, acctInformation.Rows.Row, callback)
  }
}

/**
 * Process the General Ledger Detail report. This requires the QBO general ledger
 * report and the ChartAccounts and Ledger documents for the current engagement
 * must have already been created.
 *
 * @param  {ObjectId} engagementId - the Id of the current engagement
 * @param  {ObjectId}   ledgerId   - the Id of the Ledger for the current engagement
 * @param  {Function} callback     - the callback function
 * @return {}                      - nothing
 */
var processGeneralLedgerData = function (engagementId, ledgerId, callback) {
    // Make sure this is a General Ledger report.
  if (!qboGeneralLedger || !qboGeneralLedger.Header || qboGeneralLedger.Header.ReportName !== 'GeneralLedger'
    ) {
    callback('General ledger report is not a valid General Ledger!')
    return
  }

    // Make sure the column headers exist in the General Ledger data.
  if (!qboGeneralLedger.Columns || !qboGeneralLedger.Columns.Column) {
    callback('General ledger report is missing column data!')
    return
  }

    // Get the index of each column from which data will be taken.
  var numberColumns = qboGeneralLedger.Columns.Column.length
  if (numberColumns !== numGenLedgerCols) {
    callback('General ledger report does not have the correct number of columns!')
    return
  }
  var index = {}
  for (var i = 0; i < numberColumns; i++) {
    switch (qboGeneralLedger.Columns.Column[i].ColTitle) {
      case 'Date':
        index.txDate = i
        break
      case 'Transaction Type':
        index.txnType = i
        break
      case '#':
      case 'Num':
        index.docNum = i
        break
      case 'Create Date':
        index.createDate = i
        break
      case 'Created By':
        index.createBy = i
        break
      case 'Last Modified':
        index.modified = i
        break
      case 'Last Modified By':
        index.modifiedBy = i
        break
      case 'Memo/Description':
        index.memo = i
        break
      case 'Account':
        index.account = i
        break
      case 'Currency':
        index.currency = i
        break
      case 'Debit':
        index.debit = i
        break
      case 'Credit':
        index.credit = i
        break
      case 'Amount':
        index.amount = i
        break
    }
  }

    // Make sure the account data exists in the General Ledger.
  if (!qboGeneralLedger.Rows || !qboGeneralLedger.Rows.Row) {
    callback('General ledger report is missing account data!')
    return
  }

    // Now process the transactions within each of the accounts in the General Ledger.
  processGeneralLedgerAccounts(engagementId, ledgerId, index, qboGeneralLedger.Rows.Row, function (err) {
    callback(err)
  })
}

/**
 * Save the Ledger in the database. This requires the Profit and Loss and Balance
 * Sheet QBO reports.
 *
 * @param  {ObjectId} engagementId - the Id of the current engagement
 * @param  {Function} callback     - the callback function
 * @return {}                      - nothing
 */
var saveLedger = function (engagementId, callback) {
    // Make sure this is a Profit and Loss report.
  if (!qboProfitLoss || !qboProfitLoss.Header || qboProfitLoss.Header.ReportName !== 'ProfitAndLoss') {
    callback('Profit and Lost report is not a valid report!')
    return
  }

    // Make sure the column headers exist.
  if (!qboProfitLoss.Columns || !qboProfitLoss.Columns.Column) {
    callback('Profit and Loss report is missing column data!')
    return
  }

    // Make sure the account data exists in the General Ledger.
  if (!qboProfitLoss.Rows || !qboProfitLoss.Rows.Row) {
    callback('Profit and Loss report is missing account data!')
    return
  }

    // Make sure this is a Balance Sheet report.
  if (!qboBalanceSheet || !qboBalanceSheet.Header || qboBalanceSheet.Header.ReportName !== 'BalanceSheet') {
    callback('Balance Sheet report is not a valid report!')
    return
  }

    // Make sure the column headers exist.
  if (!qboBalanceSheet.Columns || !qboBalanceSheet.Columns.Column) {
    callback('Balance Sheet report is missing column data!')
    return
  }

    // Make sure the account data exists in the General Ledger.
  if (!qboBalanceSheet.Rows || !qboBalanceSheet.Rows.Row) {
    callback('Balance Sheet report is missing account data!')
    return
  }

    // Get the P&L column indexes.
  var numberColumns = qboProfitLoss.Columns.Column.length
  var index = {}
  for (var i = 0; i < numberColumns; i++) {
    switch (qboProfitLoss.Columns.Column[i].ColTitle) {
      case '':
        index.account = i
        break
      case 'Total':
        index.total = i
        break
    }
  }

    // Loop through the P&L accounts to get the totals.
  var theRow = {}
  var plTotals = {}
  for (i = 0; i < qboProfitLoss.Rows.Row.length; i++) {
    theRow = qboProfitLoss.Rows.Row[i]
    if (theRow.type !== 'Section') {
      continue
    }

        // Get the account total for the group.
    if (theRow.Summary && theRow.Summary.ColData[index.total]) {
      plTotals[theRow.group] = Number(theRow.Summary.ColData[index.total].value)
    } else {
      plTotals[theRow.group] = 0
    }
  }

    // Get the Balance sheet column indexes.
  numberColumns = qboBalanceSheet.Columns.Column.length
  index = {}
  for (i = 0; i < numberColumns; i++) {
    switch (qboBalanceSheet.Columns.Column[i].ColTitle) {
      case '':
        index.account = i
        break
      case 'Total':
        index.total = i
        break
    }
  }

    // This function gets account totals from the balance sheet.
  var getBalanceTotals = function (bsTotals, bsRows) {
        // Loop through each of the rows.
    for (var i = 0; i < bsRows.Rows.Row.length; i++) {
            // Get a row. If it has rows process them recursively.
      var theRow = bsRows.Rows.Row[i]
      if (theRow.Rows && theRow.Rows.Row) {
        getBalanceTotals(bsTotals, bsRows.Rows.Row[i])
      }

            // Ignore rows that are not of type 'Section'.
      if (theRow.type !== 'Section') {
        continue
      }

            // Get the account total for the group.
      if (theRow.Summary && theRow.Summary.ColData[index.total]) {
        bsTotals[theRow.group] = Number(theRow.Summary.ColData[index.total].value)
      } else {
        bsTotals[theRow.group] = 0
      }
    }
  }

    // Get the account totals from the balance sheet.
  var balanceTotals = {}
  getBalanceTotals(balanceTotals, qboBalanceSheet)

    // Create the new Ledger object with the new data from QBO then save to DB.
  Logger.log(fileName + ': Update the Ledger document.')

  var ledger = new Models.Quickbooks.Ledger()
  ledger.source = {type: 'quickbooks'}
  ledger.engagementID = engagementId
  ledger.startDate = qboBalanceSheet.Header.StartPeriod
  ledger.endDate = qboBalanceSheet.Header.EndPeriod

  ledger.income = (plTotals.Income) ? plTotals.Income : 0
  ledger.cogs = (plTotals.COGS) ? plTotals.COGS : 0
  ledger.grossProfit = (plTotals.GrossProfit) ? plTotals.GrossProfit : 0
  ledger.expenses = (plTotals.Expenses) ? plTotals.Expenses : 0
  ledger.netOperatingIncome = (plTotals.NetOperatingIncome) ? plTotals.NetOperatingIncome : 0
  ledger.otherIncome = (plTotals.OtherIncome) ? plTotals.OtherIncome : 0
  ledger.otherExpenses = (plTotals.OtherExpenses) ? plTotals.OtherExpenses : 0
  ledger.netOtherIncome = (plTotals.NetOtherIncome) ? plTotals.NetOtherIncome : 0
  ledger.netIncome = (plTotals.NetIncome) ? plTotals.NetIncome : 0

  ledger.totalAssets = (balanceTotals.TotalAssets) ? balanceTotals.TotalAssets : 0
  ledger.totalEquity = (balanceTotals.Equity) ? balanceTotals.Equity : 0
  ledger.totalLiabilities = (balanceTotals.TotalLiabilitiesAndEquity) ? balanceTotals.TotalLiabilitiesAndEquity : 0

  ledger.save(function (err, resSave) {
    if (err) {
      var error = 'Error saving ledger in the Auvenir database!'
      Logger.error(fileName + ': ' + error)
      Logger.error(err)
      callback(error)
    } else {
      callback(null, resSave)
    }
  })
}

/**
 * Write the JSON object to a file.
 *
 * @param  {String} theFilePath - the file path where the file is to be written
 * @param  {String} theFileName - the name of the file to be written
 * @param  {JSON} jsonData      - the JSON object containing the data to be written
 * @return {}                   - nothing
 */
var writeJsonToFile = function (theFilePath, theFileName, jsonData) {
  var fullFilePath = theFilePath + theFileName + '.json'
  fs.writeFile(fullFilePath, JSON.stringify(jsonData, null, '\t'), function (errFile) {
    if (errFile) {
      Logger.error(fileName + ': The ' + fullFilePath + ' file was NOT saved!')
      Logger.error(errFile)
    } else {
      Logger.log(fileName + ': The ' + fullFilePath + ' file was saved!')
    }
  })
}

/**
 * Calculates the fiscal year start and end dates. The calculation is done based
 * upon the fiscal start month.
 *
 * @param  {String} fiscalMonthStart - the fiscal start month
 * @return {JSON}                    - the fiscal year start and end dates
 */
var getFiscalPeriodFromMonth = function (fiscalMonthStart) {
    // Using the fiscal start month, calculate the fiscal year start and end.
  var calcFiscalStart = moment(fiscalMonthStart, 'MMM')
  var calcFiscalEnd = calcFiscalStart.clone().add(1, 'y').subtract(1, 'd')

    // The fiscal year end must be before today's date. If not subtract a year.
  if (calcFiscalEnd.isAfter(moment())) {
    Logger.log(fileName + ': Fiscal year end is after today, must be adjusted.')
    calcFiscalStart.subtract(1, 'y')
    calcFiscalEnd.subtract(1, 'y')
  }

    // Put the dates in the required format YYYY-MM-DD.
  var useFiscalStart = calcFiscalStart.format('YYYY-MM-DD')
  var useFiscalEnd = calcFiscalEnd.format('YYYY-MM-DD')
  Logger.log(fileName + ': Fiscal Year Start calculated: ' + useFiscalStart)
  Logger.log(fileName + ': Fiscal Year End calculated: ' + useFiscalEnd)

  return {code: 0, fiscalYear: {start: useFiscalStart, end: useFiscalEnd}}
}

/**
 * Get the BusinessInfo for the current engatement from the database.
 *
 * @param  {ObjectId} engagementId - the Id of the current engagement
 * @param  {Function} callback     - the callback function
 * @return {}                      - nothing
 */
var getBusinessInfo = function (engagementId, callback) {
    // Get the BusinessInfo document from the database for the engagement.
  Models.Auvenir.BusinessInfo.findOne({engagementID: engagementId}, function (err, theBusinessInfo) {
    var error = null
    if (err) {
      error = 'Error finding Business Info in Auvenir database.'
      Logger.error(fileName + ': ' + error)
      Logger.error(err)
    } else {
      Logger.log(fileName + ': Found Business Info in database.')
    }

    callback(error, theBusinessInfo)
  })
}

/**
 * Get the QBO CompanyInfo.
 *
 * This report is used as a source to update the BusinessInfo collection. It
 * also has the fiscal month start that is required when determining the fiscal
 * year start and end.
 *
 * @param  {Object}   qboData        - an object containing the QBO instance
 *                                     and the QBO token pieces.
 * @param  {String}   dataFolder     - the path to where files are saved
 * @param  {Function} callback       - the callback function
 * @return {}                        - nothing
 */
var getQboCompanyInfo = function (qboData, dataFolder, callback) {
    // Request the CompanyInfo from QBO.
  qboData.qbo.getCompanyInfo(qboData.qboToken.realmId, function (err, theCompanyInfo) {
    var error = null
    if (err) {
      error = 'Error requesting Company Info from QuickBooks.'
      Logger.error(fileName + ': ' + error)
      Logger.error(err)
    } else {
      Logger.log(fileName + ': Received Company Info from QuickBooks.')
      writeJsonToFile(dataFolder, 'CompanyInfo', theCompanyInfo)
    }

    callback(error, theCompanyInfo)
  })
}

/**
 * Get the QBO Perferences.
 *
 * This report is used as a source to update the BusinessInfo collection.
 *
 * @param  {object}   qbo            - the QBO instance
 * @param  {String}   dataFolder     - the path to where files are saved
 * @param  {Function} callback       - the callback function
 * @return {}                        - nothing
 */
var getQboPreferences = function (qbo, dataFolder, callback) {
    // Get the company preferences.
  qbo.getPreferences(function (err, thePreferences) {
    var error = null
    if (err) {
      error = 'Error requesting Preferences from QuickBooks.'
      Logger.error(fileName + ': ' + error)
      Logger.error(err)
    } else {
      Logger.log(fileName + ': Received Preferences from QuickBooks.')
      writeJsonToFile(dataFolder, 'Preferences', thePreferences)
    }

    callback(error, thePreferences)
  })
}

/**
 * Get the QBO Balance Sheet.
 *
 * This report is used as a source to create the Ledger in the database as well
 * as information to update the BusinessInfo collection.
 *
 * @param  {object}   qbo            - the QBO instance
 * @param  {object}   fiscalYear     - an object containing the fiscal year start and end
 * @param  {String}   dataFolder     - the path to where files are saved
 * @param  {Function} callback       - the callback function
 * @return {}                        - nothing
 */
var getQboBalanceSheet = function (qbo, fiscalYear, dataFolder, callback) {
    // Get the balance sheet for the fiscal period.
  var query = {
    start_date: fiscalYear.start,
    end_date: fiscalYear.end
  }
  qbo.reportBalanceSheet(query, function (err, theBalanceSheet) {
    var error = null
    if (err) {
      error = 'Error requesting Balance Sheet from QuickBooks.'
      Logger.error(fileName + ': ' + error)
      Logger.error(err)
    } else {
      Logger.log(fileName + ': Received Balance Sheet from QuickBooks.')
      writeJsonToFile(dataFolder, 'BalanceSheet', theBalanceSheet)
    }

    callback(error, theBalanceSheet)
  })
}

/**
 * Get the QBO Trial Balance.
 *
 * This report gets the trial balance for the year preceeding the fiscal period.
 * It returns those Accounts that have a closing balance at the end of the time
 * period. By default, any accounts not in the report have a closing balance of
 * $0.00.
 *
 * This report is used as a source to create the ChartAccount in the database.
 * It provides the closing balance of an account.
 *
 * @param  {object}   qbo            - the QBO instance
 * @param  {object}   fiscalYear     - an object containing the fiscal year start and end
 * @param  {String}   dataFolder     - the path to where files are saved
 * @param  {Function} callback       - the callback function
 * @return {}                        - nothing
 */
var getQboTrialBalanace = function (qbo, fiscalYear, yearOffset, dataFolder, callback) {
    /*
     * Adjust the fiscal year start and end by the yearOffset. This is Done
     * because the trial balance may need to be run for the year preceeding the
     * current fiscal year. That is the previous fiscal year.
     */
  var newStart = moment(fiscalYear.start)
  var newEnd = moment(fiscalYear.end)
  if (yearOffset > 0) {
    newStart = newStart.add(yearOffset, 'y')
    newEnd = newEnd.add(yearOffset, 'y')
  } else {
    newStart = newStart.subtract(Math.abs(yearOffset), 'y')
    newEnd = newEnd.subtract(Math.abs(yearOffset), 'y')
  }

    // Put the dates in the required format YYYY-MM-DD.
  newStart = newStart.format('YYYY-MM-DD')
  newEnd = newEnd.format('YYYY-MM-DD')

    // Get the trial balance for the required time period.
  var query = {
    start_date: newStart,
    end_date: newEnd
  }
  qbo.reportTrialBalance(query, function (err, theTrialBalance) {
    var error = null
    if (err) {
      error = 'Error requesting Trial Balance from QuickBooks.'
      Logger.error(fileName + ': ' + error)
      Logger.error(err)
    } else {
      Logger.log(fileName + ': Received Trial Balance from QuickBooks.')
      writeJsonToFile(dataFolder, 'TrialBalance', theTrialBalance)
    }

    callback(error, theTrialBalance)
  })
}

/**
 * Get the QBO Accounts.
 *
 * This report gets both the active and inactive accounts. The inactive accounts
 * are those that have been deleted. It is necessary to get these becauase it is
 * possible that transactons may reference a deleted account. That is, transactions
 * may have been made against an account and after the end of the fiscal year the
 * account was deleted.
 *
 * This report is used as a source to create the ChartAccount in the database.
 *
 * @param  {object}   qbo            - the QBO instance
 * @param  {object}   fiscalYear     - an object containing the fiscal year start and end
 * @param  {String}   dataFolder     - the path to where files are saved
 * @param  {Function} callback       - the callback function
 * @return {}                        - nothing
 */
var getQboAccounts = function (qbo, fiscalYear, dataFolder, callback) {
    /*
     * Get all the accounts. Need to get active and inative accounts. The
     * inactive accounts are ones that have been deleted.
     */
  var query = [
        {field: 'fetchAll', value: true},
        {field: 'Active', value: [true, false], operator: 'IN'}
  ]
  qbo.findAccounts(query, function (err, theAccounts) {
    var error = null
    if (err) {
      error = 'Error requesting Accounts from QuickBooks.'
      Logger.error(fileName + ': ' + error)
      Logger.error(JSON.stringify(err, null, 2))
    } else {
      Logger.log(fileName + ': Received Accounts from QuickBooks.')
      writeJsonToFile(dataFolder, 'ChartAccounts', theAccounts)
    }

        // Get the account data out of the query response received from QBO.
    if (theAccounts && theAccounts.QueryResponse) {
      theAccounts = theAccounts.QueryResponse.Account
    } else {
      theAccounts = undefined
    }

    callback(error, theAccounts)
  })
}

/**
 * Get the QBO Profit and Loss report.
 *
 * This report is used as a source to create the Ledger in the database.
 *
 * @param  {object}   qbo            - the QBO instance
 * @param  {object}   fiscalYear     - an object containing the fiscal year start and end
 * @param  {String}   dataFolder     - the path to where files are saved
 * @param  {Function} callback       - the callback function
 * @return {}                        - nothing
 */
var getQboProfitLoss = function (qbo, fiscalYear, dataFolder, callback) {
    // Get the profit and loss for the fiscal period.
  var query = {
    start_date: fiscalYear.start,
    end_date: fiscalYear.end
  }
  qbo.reportProfitAndLoss(query, function (err, theProfitLoss) {
    var error = null
    if (err) {
      error = 'Error requesting Profit and Loss from QuickBooks.'
      Logger.error(fileName + ': ' + error)
      Logger.error(err)
    } else {
      Logger.log(fileName + ': Received Profit and Loss from QuickBooks.')
      writeJsonToFile(dataFolder, 'ProfitAndLoss', theProfitLoss)
    }

    callback(error, theProfitLoss)
  })
}

/**
 * Get the QBO Opening Balance report.
 *
 * This report is a modified General Ledger Detail report. This is done by
 * setting the start and end dates to the first day of the fiscal period. This
 * runs the report for the one day and returns those Accounts that have an opening
 * balance at that point in time. By default, any accounts not in the report have
 * an opening balance of $0.00.
 *
 * This report is used as a source to create the ChartAccount in the database.
 * It provides the opening balance of an account.
 *
 * @param  {object}   qbo            - the QBO instance
 * @param  {object}   fiscalYear     - an object containing the fiscal year start and end
 * @param  {String}   dataFolder     - the path to where files are saved
 * @param  {Function} callback       - the callback function
 * @return {}                        - nothing
 */
var getQboOpenBalance = function (qbo, fiscalYear, dataFolder, callback) {
    // Different columns are required if Multi currency is enabled.
  var baseColumns = 'account_name'
  var addColumns
  if (usingMultiCurrency) {
    addColumns = ',rbal_nat_amount_home_nt'
  } else {
    addColumns = ',rbal_nat_amount'
  }
  var theColumns = baseColumns + addColumns

    // Get for the first day of the fiscal period as the opening balance.
  var query = {
    start_date: fiscalYear.start,
    end_date: fiscalYear.start,
    columns: theColumns,
    sort_by: 'tx_date',
    sort_order: 'ascned'
  }
  qbo.reportGeneralLedgerDetail(query, function (err, theOpenBalance) {
    var error = null
    if (err) {
      error = 'Error requesting account opening balances from QuickBooks.'
      Logger.error(fileName + ': ' + error)
      Logger.error(err)
    } else {
      Logger.log(fileName + ': Received Account opening balances from QuickBooks.')
      writeJsonToFile(dataFolder, 'OpenBalance', theOpenBalance)
    }

    callback(error, theOpenBalance)
  })
}

/**
 * Get the QBO General Ledger Detail report.
 *
 * Generally the report period starts one month before the fiscal year start and
 * ends one month after the fiscal year end. That gives a total of 14 months.
 * This report gives all the transactions over the given time period for each
 * account. The transactions are grouped by account.
 *
 * This report is used as the source to create the Header and Line entries in the
 * database.
 *
 * @param  {object}   qbo            - the QBO instance
 * @param  {object}   fiscalYear     - an object containing the fiscal year start and end
 * @param  {Number}   monthExtension - the number of months before and after
 * @param  {String}   dataFolder     - the path to where files are saved
 * @param  {Function} callback       - the callback function
 * @return {}                        - nothing
 */
var getQboLedgerDetail = function (qbo, fiscalYear, monthExtension, dataFolder, callback) {
    // Different columns are requested if multiCurrency is enabled.
  var baseColumns = 'tx_date,txn_type,doc_num,create_by,create_date,last_mod_by,last_mod_date,memo,account_name'
  if (usingMultiCurrency) {
    var addColumns = ',currency,debt_home_amt,credit_home_amt,subt_nat_amount_home_nt'
    numGenLedgerCols = 13
  } else {
    addColumns = ',debt_amt,credit_amt,subt_nat_amount'
    numGenLedgerCols = 12
  }
  var theColumns = baseColumns + addColumns

    /*
     * Adjust the fiscal year start and end by the monthExtension. This is Done
     * because the general ledger may need to be run for months preceeding the
     * fiscal year start and after the fiscal year end.
     */
  var newStart = moment(fiscalYear.start)
  var newEnd = moment(fiscalYear.end)
  if (monthExtension > 0) {
    newStart = newStart.subtract(monthExtension, 'M')
    newEnd = newEnd.add(monthExtension, 'M')
  } else {
    newStart = newStart.add(Math.abs(monthExtension), 'M')
    newEnd = newEnd.subtract(Math.abs(monthExtension), 'M')
  }

    // Put the dates in the required format YYYY-MM-DD.
  newStart = newStart.format('YYYY-MM-DD')
  newEnd = newEnd.format('YYYY-MM-DD')

    // Get the General Ledger for the fiscal period.
  var query = {
    start_date: newStart,
    end_date: newEnd,
    columns: theColumns,
    sort_by: 'tx_date',
    sort_order: 'ascned'
  }
  qbo.reportGeneralLedgerDetail(query, function (err, theLedgerDetail) {
    var error = null
    if (err) {
      error = 'Error requesting General Ledger Detail from QuickBooks.'
      Logger.error(fileName + ': ' + error)
      Logger.error(err)
    } else {
      Logger.log(fileName + ': Received General Ledger Detail from QuickBooks.')
      writeJsonToFile(dataFolder, 'GeneralLedgerDetail', theLedgerDetail)
    }

    callback(error, theLedgerDetail)
  })
}

/**
 * Update the BuisnessInfo document for the current engagement with the data
 * taken from the QBO CompanyInfo, Preferences and Balance Sheet reports.
 *
 * @param  {ObjectId} engagementId - the Id of the current engagement
 * @param  {Object}   qboToken     - an object conatining the QBO token pieces
 * @param  {Object}   fiscalYear   - an object containing the fiscal year start and end
 * @param  {Function} callback     - the callback function
 * @return {}                      - nothing
 */
var updateBusinessInfo = function (engagementId, qboToken, fiscalYear, callback) {
    // Object to hold the new updated BuisnessInfo data.
  var businessInfoUpdate = {}

    // Set the accounting software being used.
  businessInfoUpdate.accountingSoftware = 'qbo'

    // Initialize the integration obj and save the encrypted QBO token information.
  var token = {}
  if (qboToken) {
        // Encrypt the oauth information.
    var encryptedToken = Crypt.encrypt(qboToken.oauthToken, engagementId.toString())
    var encryptedSecret = Crypt.encrypt(qboToken.oauhTokenSecret, engagementId.toString())

    token = {
      oauthToken: encryptedToken,
      oauhTokenSecret: encryptedSecret,
      realmId: qboToken.realmId
    }
  }
  businessInfoUpdate.integration = {
    'quickbooks': {
      'token': token,
      'multiCurrency': false
    }
  }

    /*
     * Save the fiscal year end. It is being saved as a String since we do not
     * have to be or want to be concerned with time and time zones.
     */
  if (fiscalYear.end) {
    businessInfoUpdate.fiscalYearEnd = fiscalYear.end
  }

    /*
     * Get the required information from QBO CompanyInfo. Then update the
     * BusinessInfo document with this new information. Any existing values in
     * the document are overwritten.
     */
  if (qboCompanyInfo) {
    if (qboCompanyInfo.CompanyName) {
      businessInfoUpdate.name = qboCompanyInfo.CompanyName
    }
    if (qboCompanyInfo.LegalName) {
      businessInfoUpdate.nameLegal = qboCompanyInfo.LegalName
    }
    if (qboCompanyInfo.WebAddr && qboCompanyInfo.WebAddr.URI) {
      businessInfoUpdate.websiteURL = qboCompanyInfo.WebAddr.URI
    }
    if (qboCompanyInfo.PrimaryPhone.FreeFormNumber) {
      businessInfoUpdate.phone = qboCompanyInfo.PrimaryPhone.FreeFormNumber
    }
    if (qboCompanyInfo.Country) {
      businessInfoUpdate.country = qboCompanyInfo.Country
    }

    if (qboCompanyInfo.NameValue) {
      var compNameValues = qboCompanyInfo.NameValue
      for (var i = 0; i < compNameValues.length; i++) {
        if (compNameValues[i].Name === 'CompanyType') {
          switch (compNameValues[i].Value) {
            case 'Sole Proprietor':
              businessInfoUpdate.structure = 'SOLE'
              break
            case 'Partnership':
              businessInfoUpdate.structure = 'PARTNERSHIP'
              break
            case 'Other':
              businessInfoUpdate.structure = 'OTHER'
              break

            default:
              businessInfoUpdate.structure = compNameValues[i].Value
          }
        }

        if (compNameValues[i].Name === 'IndustryType') {
          businessInfoUpdate.industry = compNameValues[i].Value
        }
      }
    }

        // Save the addresses.
    var getQboAddress = function (source, destination, addressType) {
      var address = {}
      if (source.Line1 !== undefined) address.streetAddress = source.Line1
      if (source.Line2 !== undefined) address.unit = source.Line2
      if (source.City !== undefined) address.city = source.City
      if (source.CountrySubDivisionCode !== undefined) address.stateProvince = source.CountrySubDivisionCode
      if (source.PostalCode !== undefined) address.postalCode = source.PostalCode
      if (source.Country !== undefined) address.country = source.Country

      if (Object.keys(address).length > 0) {
        address.type = addressType
        destination.push(address)
      }
    }

    var addresses = []
    if (qboCompanyInfo.CompanyAddr) {
      getQboAddress(qboCompanyInfo.CompanyAddr, addresses, 'physical')
    }
    if (qboCompanyInfo.CustomerCommunicationAddr) {
      getQboAddress(qboCompanyInfo.CustomerCommunicationAddr, addresses, 'mailing')
    }
    if (qboCompanyInfo.LegalAddr) {
      getQboAddress(qboCompanyInfo.LegalAddr, addresses, 'legal')
    }
    if (addresses.length > 0) {
      businessInfoUpdate.addresses = addresses
    }
  }

    // Get the required information from the QBO Balance Sheet.
  if (qboBalanceSheet) {
    if (qboBalanceSheet.Header && qboBalanceSheet.Header.Option) {
      var options = qboBalanceSheet.Header.Option
      for (i = 0; i < options.length; i++) {
        if (options[i].Name === 'AccountingStandard') {
          businessInfoUpdate.accountingFramework = options[i].Value
          break
        }
      }
    }
  }

    // Get the required information from the QBO preferences.
  if (qboPreferences) {
    if (qboPreferences.CurrencyPrefs) {
      businessInfoUpdate.homeCurrency = qboPreferences.CurrencyPrefs.HomeCurrency.value
      businessInfoUpdate.integration.quickbooks.multiCurrency = qboPreferences.CurrencyPrefs.MultiCurrencyEnabled
    }
    if (qboPreferences.ReportPrefs) {
      businessInfoUpdate.reportBasis = qboPreferences.ReportPrefs.ReportBasis
    }
    if (qboPreferences.OtherPrefs && qboPreferences.OtherPrefs.NameValue) {
      var prefNameValues = qboPreferences.OtherPrefs.NameValue
      var numberValues = prefNameValues.length
      for (i = 0; i < numberValues; i++) {
        if (prefNameValues[i].Name === 'Language') {
          businessInfoUpdate.language = prefNameValues[i].Value
          continue
        }
        if (prefNameValues[i].Name === 'DateFormat') {
          switch (prefNameValues[i].Value) {
            case 'Year Month Date separated by a dash':
              businessInfoUpdate.dateFormat = 'YYYY-MM-DD'
              break
            case 'Year Month Date separated by a dot':
              businessInfoUpdate.dateFormat = 'YYYY.MM.DD'
              break
            case 'Year Month Date separated by a slash':
              businessInfoUpdate.dateFormat = 'YYYY/MM/DD'
              break

            case 'Date Month Year separated by a dash':
              businessInfoUpdate.dateFormat = 'DD-MM-YYYY'
              break
            case 'Date Month Year separated by a dot':
              businessInfoUpdate.dateFormat = 'DD.MM.YYYY'
              break
            case 'Date Month Year separated by a slash':
              businessInfoUpdate.dateFormat = 'DD/MM/YYYY'
              break

            case 'Month Date Year separated by a dash':
              businessInfoUpdate.dateFormat = 'MM-DD-YYYY'
              break
            case 'Month Date Year separated by a dot':
              businessInfoUpdate.dateFormat = 'MM.DD.YYYY'
              break
            case 'Month Date Year separated by a slash':
              businessInfoUpdate.dateFormat = 'MM.DD.YYYY'
              break

            default:
              businessInfoUpdate.dateFormat = ''
          }

          continue
        }
        if (prefNameValues[i].Name === 'NumberFormat') {
          switch (prefNameValues[i].Value) {
            case 'US Number Format':  // 123,456.00
              businessInfoUpdate.numberFormat = 'us'
              break
            case 'German Number Format':  // 123.456,00
              businessInfoUpdate.numberFormat = 'de'
              break
            case 'French Number Format':  // 123 456,00
              businessInfoUpdate.numberFormat = 'fr'
              break
            case 'Indian Number Format':  // 1,23,456,00
              businessInfoUpdate.numberFormat = 'in'
              break

            default:
              businessInfoUpdate.numberFormat = ''
          }

          continue
        }
      }
    }
  }

  // Update the document in the BusinessInfo collection.
  var query = {'engagementID': engagementId}
  if (theBusinessInfo._idD) {
    query._id = theBusinessInfo._id
  }
  Models.Auvenir.BusinessInfo.findOneAndUpdate(
        query,
        {$set: businessInfoUpdate},
        {new: true},
        function (err, updatedBusinessInfo) {
          var error = null
          if (err) {
            error = 'Error updating BusinessInfo in Auvenir database!'
            Logger.error(fileName + ': ' + error)
            Logger.error(err)
          }

          callback(error, updatedBusinessInfo)
        }
    )
}

/**
 * This function will proecess the data from the QBO reports received.
 *
 * - The BusinessInfo document is updated with data from QBO CompanyInfo, Balance
 * Sheet and Preferences.
 * - The QBO related data is removed from the Auvneir DB for the current engagement.
 * The data removed is the Chart of Accounts, Ledger and all the Headers and Lines.
 * - The Chart of Accounts is created with data from QBO Accounts, Opening Balance
 * and Trial Balance.
 * - The Ledger is created with data from QBO Profit and Loss and Balance Sheet
 * - The Header and Lines are created with data from the QBO General Ledger.
 *
 * @param  {ObjectId} engagementId - the Id of the current engagement
 * @param  {Object}   qboToken     - an object conatining the QBO token pieces
 * @param  {Object}   fiscalYear   - an object containing the fiscal year start and end
 * @param  {Function} callback     - the callback function
 * @return {}                      - nothing
 */
var processTheData = function (engagementId, qboToken, fiscalYear, callback) {
  var theLedger = null
  var haveLedger = false
  var haveChartAccounts = false

  var errorFound = null
  var processedGenLedger = false

    // Update BusinessInfo with the QBO data that's been received.
  Logger.log(fileName + ': Update the BusinessInfo document.')
  updateBusinessInfo(engagementId, qboToken, fiscalYear, function (err, result) {
    if (err) {
      Logger.error('ERROR: Updating BusinessInfo', err)
    }
    Logger.log(fileName + ': ' + 'BusinessInfo updated.')
    // Notify the client of the new business info.
  })

    // Remove accounting data for the engagement from the DB.
  Utility.removeAccountingData(engagementId, function (err, result) {
    if (err) {
      if (err.code !== 0) {
        callback(err.msg)
      } else {
        callback('Error removing data from Auvenir database!')
      }

      return
    }

        // Execute functions that are waiting for data removal.
    createChartAccount()
    createLedger()
  })

  var createChartAccount = function () {
    saveTheAccounts(engagementId, function (err) {
      if (err) {
        errorFound = (!errorFound) ? err : ' ' + err
      }

      haveChartAccounts = true
      processGeneralLedger()
    })
  }

  var createLedger = function () {
    saveLedger(engagementId, function (err, ledger) {
      if (err) {
        errorFound = (!errorFound) ? err : ' ' + err
      }

      theLedger = ledger
      haveLedger = true
      processGeneralLedger()
    })
  }

    /**
     * Process the QBO General Ledger report. It can only be done once both the
     * Chart of Accounts and the Ledger have been created.
     *
     * @return {} - nothing
     */
  var processGeneralLedger = function () {
        /*
         * Do not proceed if there are sill outstanding requests, or requests
         * had errors.
         */
    if (!haveChartAccounts || !haveLedger || processedGenLedger) {
      return
    }
    processedGenLedger = true

    if (errorFound) {
      callback(errorFound)
      return
    }

    processGeneralLedgerData(engagementId, theLedger._id, function (err) {
      callback(err)
    })
  }
}

/**
 * Get more data from QBO. Get the Balance Sheet, Opening Balances, Trial Balance,
 * Accounts, Profit and Loss reports. None of these reports is dependant upon the
 * other so they can all be requested asynchronously. Once all the reporst have
 * been received from QBO, they can be processed.
 *
 * @param  {ObjectId} engagementId - the Id of the current engagement
 * @param  {Object}   qboData      - an object containing the QBO instance and the
 *                                   QBO token pieces.
 * @param  {String}   dataFolder   - the path to where files are saved
 * @param  {Object}   fiscalYear   - an object containing the fiscal year start and end
 * @param  {Function} callback     - the callback function
 * @return {}                      - nothing
 */
var getAdditionalData = function (engagementId, qboData, dataFolder, fiscalYear, callback) {
  var haveGeneralLedger = false
  var haveBalanceSheet = false
  var haveTrialBalance = false
  var haveOpenBalance = false
  var haveAccounts = false
  var haveProfitLoss = false

  var errorFound = null
  var executed = false
  var qbo = qboData.qbo

  Logger.log(fileName + ': Request General Ledger Detail from QuickBooks.')
  getQboLedgerDetail(qbo, fiscalYear, 1, dataFolder, function (err, ledgerDetail) {
    qboGeneralLedger = ledgerDetail
    if (err) {
      errorFound = (!errorFound) ? err : ' ' + err
    }
    haveGeneralLedger = true

    allDataReceived()
  })

  Logger.log(fileName + ': Request Balance Sheet from QuickBooks.')
  qboBalanceSheet = null
  getQboBalanceSheet(qbo, fiscalYear, dataFolder, function (err, balanceSheet) {
    qboBalanceSheet = balanceSheet
    if (err) {
      errorFound = (!errorFound) ? err : ' ' + err
    }
    haveBalanceSheet = true

    allDataReceived()
  })

  Logger.log(fileName + ': Request Opening Balance from QuickBooks.')
  qboOpenBalance = null
  getQboOpenBalance(qbo, fiscalYear, dataFolder, function (err, openBalance) {
    qboOpenBalance = openBalance
    if (err) {
      errorFound = (!errorFound) ? err : ' ' + err
    }
    haveOpenBalance = true

    allDataReceived()
  })

  Logger.log(fileName + ': Request Trial Balance from QuickBooks.')
  qboTrialBalance = null
  getQboTrialBalanace(qbo, fiscalYear, -1, dataFolder, function (err, trialBalance) {
    qboTrialBalance = trialBalance
    if (err) {
      errorFound = (!errorFound) ? err : ' ' + err
    }
    haveTrialBalance = true

    allDataReceived()
  })

  Logger.log(fileName + ': Request Accounts from QuickBooks.')
  qboAccounts = null
  getQboAccounts(qbo, fiscalYear, dataFolder, function (err, accounts) {
    qboAccounts = accounts
    if (err) {
      errorFound = (!errorFound) ? err : ' ' + err
    }
    haveAccounts = true

    allDataReceived()
  })

  Logger.log(fileName + ': Request Profit and Loss from QuickBooks.')
  qboProfitLoss = null
  getQboProfitLoss(qbo, fiscalYear, dataFolder, function (err, profitLoss) {
    qboProfitLoss = profitLoss
    if (err) {
      errorFound = (!errorFound) ? err : ' ' + err
    }
    haveProfitLoss = true

    allDataReceived()
  })

    /**
     * Process the QBO reports that have been received.
     *
     * @return {} - nothing
     */
  var allDataReceived = function () {
        /*
         * Do not proceed if there are sill outstanding requests, or requests
         * had errors.
         */
    if (!haveBalanceSheet || !haveTrialBalance || !haveAccounts || !haveProfitLoss || !haveGeneralLedger || !haveOpenBalance || executed) {
      return
    }
    executed = true

    if (errorFound) {
      callback(errorFound)
      return
    }

        // Now process the data.
    processTheData(engagementId, qboData.qboToken, fiscalYear, function (err) {
      callback(err)
    })
  }
}

/**
 * Get the intial data. From QBO get the CompanyInfo and Preferences. From the
 * Auvneir DB get the BusinessInfo for the current engagement. Once these are
 * available they can be processed.
 *
 * @param  {Object}   engagement - the current engagement object
 * @param  {Object}   qboData    - an object containing the QBO instance and the
 *                                 QBO token pieces.
 * @param  {String}   dataFolder - the path to where files are saved
 * @param  {Function} callback   - the callback function
 * @return                       nothing
 */
var getInitialData = function (engagement, qboData, dataFolder, callback) {
    // Initialize
  var haveCompanyInfo = false
  var havePreferences = false
  var haveBusinessInfo = false

  var errorFound = null
  var executed = false
  var qbo = qboData.qbo

  Logger.log(fileName + ': Request Company Info from QuickBooks.')
  qboCompanyInfo = null
  getQboCompanyInfo(qboData, dataFolder, function (err, companyInfo) {
    qboCompanyInfo = companyInfo
    if (err) {
      errorFound = (!errorFound) ? err : ' ' + err
    }
    haveCompanyInfo = true

    processInitialData()
  })

  Logger.log(fileName + ': Request Preferences from QuickBooks.')
  qboPreferences = null
  getQboPreferences(qbo, dataFolder, function (err, preferences) {
    qboPreferences = preferences
    if (err) {
      errorFound = (!errorFound) ? err : ' ' + err
    }
    havePreferences = true

    processInitialData()
  })

  Logger.log(fileName + ': Query Business Info in the database.')
  theBusinessInfo = null
  getBusinessInfo(engagement._id, function (err, businessInfo) {
    theBusinessInfo = businessInfo
    if (err) {
      errorFound = (!errorFound) ? err : ' ' + err
    }
    haveBusinessInfo = true

    processInitialData()
  })

    /**
     * Process the initial data. The first step is to determine the fiscal
     * period since it is required when pulling further data from QBO. Once the
     * fiscal period has been determined and it is consistent between QBO and
     * BusinessInfo, the next set of data can be requested.
     *
     * @return {} - nothing
     */
  var processInitialData = function () {
        /*
         * Do not proceed if there are sill outstanding requests, or requests
         * had errors, or the requested data is empty.
         */
    if (!haveCompanyInfo || !havePreferences || !haveBusinessInfo || executed) {
      return
    }
    executed = true

    if (errorFound) {
      callback(errorFound)
      return
    }

    if (!qboCompanyInfo || !qboPreferences || !theBusinessInfo) {
      callback('Missing some initial required data!')
      return
    }

        /*
         * Use the database BusinessInfo fiscalYearEnd and the QBO CompanyInfo
         * FiscalYearStartMonth to determine the fiscal year start and end.
         */
    var businessFiscalEnd = theBusinessInfo.fiscalYearEnd
    var qboFiscalStartMonth = qboCompanyInfo.FiscalYearStartMonth

    if (businessFiscalEnd === null && qboFiscalStartMonth === undefined) {
      callback('Not able to determine the fiscal year!')
      return
    }

        // Take the month from QBO and calculate the start and end dates.
    var result = getFiscalPeriodFromMonth(qboFiscalStartMonth)
    if (result.code !== 0) {
      callback(result.msg)
      return
    }

        // Fiscal year end from BusinessInfo must be consistent with what was calculated.
    if (businessFiscalEnd) {
      if (!moment(businessFiscalEnd).isSame(result.fiscalYear.end, 'day')) {
        callback('BusinessInfo fiscal year end not consistent with QuickBooks!')
        return
      }
      Logger.log(fileName + ': Have the same fiscal year end.')
    }

        // Does the QBO company have multiCurrency enabled?
    if (!qboPreferences.CurrencyPrefs) {
      callback('Not able to get the currency preference from QuickBooks!')
      return
    }
    usingMultiCurrency = qboPreferences.CurrencyPrefs.MultiCurrencyEnabled

        // Have the fiscal period, so continue to get QBO data.
    getAdditionalData(engagement._id, qboData, dataFolder, result.fiscalYear, function (err) {
      if (err) {
        callback(err)
      } else {
        callback(null)
      }
    })
  }
}

module.exports = function (passport, config) {
    /**
     * Initial route that gets triggered when we start to initialize a quickbooks
     * connection. This route will request an access token from QuickBooks for
     * the clents account.
     */

  router.get('/requestToken', isAuthenticated, function (req, res) {
    Logger.log(fileName + ': Engagement Id: ' + req.query.engID)
    Logger.log(fileName + ': Route: Request Token for QuickBooks')
    Logger.log(fileName + ': Endpoint: ' + Quickbooks.V3_ENDPOINT_BASE_URL)

        // Get the current server to create the QBO callback address.
    var protocol = config.server.protocol
    var domain = config.server.domain
    var port = config.server.port
    var url = protocol + '://' + domain + ':' + port
    var callbackAddress = url + '/quickbooks/callback'
    Logger.log(fileName + ': Callback address: ' + url)

        // Create the QBO post and then do the post.
    var postBody = {
      url: Quickbooks.REQUEST_TOKEN_URL,
      oauth: {
        callback: callbackAddress,
        consumer_key: consumerKey,
        consumer_secret: consumerSecret
      }
    }
    Logger.log(postBody)

    request.post(postBody, function (e, r, data) {
      var requestToken = qs.parse(data)
      req.session.oauth_token_secret = requestToken.oauth_token_secret
      req.session.engID = req.query.engID
      Logger.log(fileName + ': QuickBooks token: ')
      Logger.log(requestToken)
      res.redirect(Quickbooks.APP_CENTER_URL + requestToken.oauth_token)
    })
  })

    /**
     * WHAT IS THIS?
     */
  router.get('/close_quickbooks', isAuthenticated, function (req, res) {
    Logger.log(fileName + ': Route: close_quickbooks')
    res.render('closeQuickbooks')
  })

    /**
     * This is the callback route executed when QuickBooks responds with the
     * access token that was requested. The access token is one piece of information
     * that is required to allow access to the clients QuickBooks account.
     */
  router.get('/callback', isAuthenticated, function (req, res) {
    var resultFailedStart = '<!DOCTYPE html><html lang="en"><head></head><body><script>window.opener.appCtrl.displayMessage("Quick Book Integration Failed! '
    var resultFailedEnd = '");  window.close();</script></body></html>'
    var quickbooksResultFail = resultFailedStart + resultFailedEnd

    Logger.log(fileName + ': Quick Books 4 **************** Route: Callback')

        // Check that the session has an engagement Id.
    if (!req.session.engID) {
            // There is no current engagement ID, so no need to load quickbook data.
      Logger.log(fileName + ': No Current Engagement.')
      res.send(quickbooksResultFail)
      return
    }

    var postBody = {
      url: Quickbooks.ACCESS_TOKEN_URL,
      oauth: {
        consumer_key: consumerKey,
        consumer_secret: consumerSecret,
        token: req.query.oauth_token,
        token_secret: req.session.oauth_token_secret,
        verifier: req.query.oauth_verifier,
        realmId: req.query.realmId
      }
    }

    request.post(postBody, function (e, r, data) {
      Logger.log(fileName + ': Route: Post body')

            // Check that the engagement Id exists in the database.
      Models.Auvenir.Engagement.findOne({_id: Utility.castToObjectId(req.session.engID)}, function (err, oneEngagement) {
        if (err) {
          Logger.error(fileName + ': Error getting the Engagement Information!')
          res.send(quickbooksResultFail)
          return
        }
        if (!oneEngagement) {
          Logger.log(fileName + ': Engagement does not exists!')
          res.send(quickbooksResultFail)
          return
        }

        Logger.log(fileName + ': Engagement Exists!')
        Logger.log(oneEngagement)

                // Get the access token and instantiate a QuickBooks object.
        var accessToken = qs.parse(data)
        var qbo = new Quickbooks(
                    consumerKey,
                    consumerSecret,
                    accessToken.oauth_token,
                    accessToken.oauth_token_secret,
                    postBody.oauth.realmId,
                    useSandbox,         // true = Sandbox, false = Production
                    qboDebug            // true = Debugging on
                )

                // Get the required QBO token pieces for saving later in the DB.
        var qboData = {
          qbo: qbo,
          qboToken: {
            oauthToken: accessToken.oauth_token,
            oauhTokenSecret: accessToken.oauth_token_secret,
            realmId: postBody.oauth.realmId
          }
        }

                // Now start to get informatin from QBO.
        var dataFolder = dataFolderPath + oneEngagement._id + '/'
        getInitialData(oneEngagement, qboData, dataFolder, function (err) {
          if (err) {
            Logger.error(fileName + ': QuickBooks integration failed!')
            Logger.error(err)
                        // update quickbooks' token value.
            res.send('<!DOCTYPE html>' +
                            '<html lang="en">' +
                            '   <head></head>' +
                            '   <body>' +
                            '   <script>' +
                            '       var ctrl = window.opener.appCtrl;' +
                            '       ctrl.displayMessage("Quick Book Integration Failed! ' + err + '");' +
                            '       if(!ctrl.currentAudit.businessInfo.integration) {' +
                            '           ctrl.currentAudit.businessInfo.integration = {' +
                            '               quickbooks: {' +
                            '                   token: true' +
                            '               }' +
                            '           };' +
                            '       } else {' +
                            '           ctrl.currentAudit.businessInfo.integration.quickbooks = {' +
                            '               token: true' +
                            '           };' +
                            '       }' +
                            '       window.close();' +
                            '   </script>' +
                            '   </body>' +
                            '</html>')
          } else {
            Logger.log(fileName + ': QuickBooks integration succeeded!')
                        // update quickbooks' token value.
            res.send('<!DOCTYPE html>' +
                            '<html lang="en">' +
                            '<head></head>' +
                            '<body>' +
                            '<script>' +
                            '       var ctrl = window.opener.appCtrl;' +
                            '       ctrl.displayMessage("Quick Book Integration Succeeded!");' +
                            '       if(!ctrl.currentAudit.businessInfo.integration) {' +
                            '           ctrl.currentAudit.businessInfo.integration = {' +
                            '               quickbooks: {' +
                            '                   token: true' +
                            '               }' +
                            '           };' +
                            '       } else {' +
                            '           ctrl.currentAudit.businessInfo.integration.quickbooks = {' +
                            '               token: true' +
                            '           };' +
                            '       }' +
                            '       window.close();' +
                            '</script>' +
                            '</body>' +
                            '</html>')
          }
        })
      })
    })
  })

    /**
     * This is the integration route executed when the businessInfo has QuickBooks token stored.
     * It follows the same behaviour as the callback route. The only difference is the token is
     * retrieved from businessInfo object
     */
  router.get('/integrate', isAuthenticated, function (req, res) {
    Logger.log(fileName + ': Quick Books 4 **************** Route: integrate')

        // Check that the session has an engagement Id.
    if (!req.query.engID) {
            // There is no current engagement ID, so no need to load quickbook data.
      Logger.log(fileName + ': No Current Engagement.')
      return res.status(404).json({
        message: 'EngagementID Not Found'
      })
    }

        // Check that the engagement Id exists in the database.
    Models.Auvenir.Engagement.findOne({_id: Utility.castToObjectId(req.query.engID)}, function (err, oneEngagement) {
      if (err) {
        Logger.error(fileName + ': Error getting the Engagement Information!')
        return res.status(500).json({
          message: 'Error Getting the engagement information'
        })
      }
      if (!oneEngagement) {
        Logger.log(fileName + ': Engagement does not exists!')
        return res.status(404).json({
          message: 'Engagement Not Found'
        })
      }
      Models.Auvenir.BusinessInfo.findOne({engagementID: oneEngagement._id}, function (err, theBusinessInfo) {
        if (err) {
          Logger.error(fileName + ': ' + err.message)
          Logger.error(err)
          return res.status(500).json({
            message: 'Error Getting the Business information'
          })
        }

        if (!theBusinessInfo) {
          Logger.log(fileName + ': businessInfo does not exists!')
          return res.status(404).json({
            message: 'Business information Not Found'
          })
        }

        if (!theBusinessInfo.integration ||
                    !theBusinessInfo.integration.quickbooks ||
                    !theBusinessInfo.integration.quickbooks.token) {
          return res.status(401).json({
            message: 'Token missing. Unable to authorized with QuickBooks. Please authorize again.'
          })
        }

        var token = theBusinessInfo.integration.quickbooks.token
        var tokenDecrypted = Crypt.decrypt(token.oauthToken, oneEngagement._id.toString())
        var tokenSecretDecrypted = Crypt.decrypt(token.oauhTokenSecret, oneEngagement._id.toString())
        var qbo = new Quickbooks(
                    consumerKey,
                    consumerSecret,
                    tokenDecrypted,
                    tokenSecretDecrypted,
                    token.realmId,
                    useSandbox,         // true = Sandbox, false = Production
                    qboDebug            // true = Debugging on
                )

                // validate the token is not expired
        request.post({
          url: QUICKBOOKS_CURRENT_USER,
          oauth: {
            consumer_key: consumerKey,
            consumer_secret: consumerSecret,
            token: tokenDecrypted,
            token_secret: tokenSecretDecrypted,
            realmId: token.realmId
          },
          json: true
        }, function (e, r, data) {
          if (e) {
            Logger.error(e)
            return res.status(500).json({
              message: 'System internal error. Unable to authorized with QuickBooks.'
            })
          }
          if (data && (~data.indexOf('<ErrorCode>22</ErrorCode>') || ~data.indexOf('This API requires Authorization.'))) {
            return Models.Auvenir.BusinessInfo.update({engagementID: oneEngagement._id}, {$set: {
              integration: {quickbooks: {}}
            }}, function () {
              return res.status(401).json({
                message: 'Token expired. Unable to authorized with QuickBooks. Please authorize again.'
              })
            })
          }

                    // Get the required QBO token pieces for saving later in the DB.
          var qboData = {
            qbo: qbo,
            qboToken: {
              oauthToken: tokenDecrypted,
              oauhTokenSecret: tokenSecretDecrypted,
              realmId: token.realmId
            }
          }
                    // Now start to get information from QBO.
          var dataFolder = dataFolderPath + oneEngagement._id + '/'
          getInitialData(oneEngagement, qboData, dataFolder, function (err) {
            if (err) {
              Logger.error(fileName + ': QuickBooks integration failed!')
              return res.status(403).json({
                message: err
              })
            }
            return res.status(200).json({
              message: 'QuickBooks integration succeeded!'
            })
          })
        })
      })
    })
  })
  return router
}
