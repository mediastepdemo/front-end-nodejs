const appConfig = require('../config')
const express = require('express')
const path = require('path')
const mongoose = require('mongoose')
const _ = require('lodash')
const __ = require(rootDirectory + '/plugins/utilities/_lodash')
const async = require('async')
const gridStream = require('gridfs-stream')
const fs = require('fs')
const debug = require('debug')('auvenir-core:router:index')
const router = express.Router()

const cookie = require('cookie')
const cookieParser = require('cookie-parser')
const redis = require('redis')
const formidable = require('formidable')
const archiver = require('archiver')
const sendRes = require('./routeHelper').sendResponse
const flash = require('./routeHelper').flash
const crypto = require('crypto')
const AWS = require('aws-sdk')
const nodemailer = require('nodemailer')

const Connections = require('../models/connections')
const Models = require('../models')
const Utility = require('../plugins/utilities/utility')
const Access = require('../plugins/utilities/accessControls')
const securityHelper = require('./securityHelper')
const FileManager = require('../plugins/file-manager')

const ONBOARDING = 'onboarding'
const WAITLIST = 'waitlist'
const PENDING = 'pending'
const ACTIVE = 'active'
const LOCKED = 'locked'

AWS.config.loadFromPath(appConfig.integrations.aws.config)
var ses = new AWS.SES({apiVersion: '2010-12-01'})
const FileUploader = FileManager.Uploader
const FileReader = FileManager.Reader
var auvenirGFS = null
var googleGFS = null

const encryptAlgorithm = appConfig.security.file_encryption.algorithm
const encryptPassword = appConfig.security.file_encryption.secret

Connections.auvenir.on('open', function () {
  auvenirGFS = gridStream(Connections.auvenir.db, mongoose.mongo)
})
Connections.gdrive.on('open', function () {
  googleGFS = gridStream(Connections.gdrive.db, mongoose.mongo)
})

/**
 * Middleware function that is responsible for checking with PassportJS to see wether the
 * the user is authenticated.
 *
 * @param: {function} req: the incoming http request
 * @param: {function} res: the outgoing response.
 * @param: {function} next: the next function if it is authenticated.
 */
var isAuthenticated = function (req, res, next) {
  req.log.info('Middleware: isAuthenticated')
  debug('authentication check', req.originalUrl, req.cookies)
  if (req.isAuthenticated()) {
    req.log.info('User is authenticated.')
    return next()
  } else {
    req.log.warn('Failed authentication check')
    // flash.write(req, 'Failed authentication check');
  }
  res.redirect('/')
}

const setAllowOriginHeaders = function (req, res) {
  let origin = req.headers['origin']
  if (!origin) return
  if (_.includes(appConfig.server.allowOrigin, origin.toLowerCase().trim())) {
    if (req.method.toLowerCase() === 'options') {
      res.header('Access-Control-Allow-Origin', origin)
      res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
      res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With')
      res.send(200)
    } else {
      res.header('Access-Control-Allow-Origin', origin)
      res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    }
  }
}

/**
 * Get user token for QA environment
 * TODO: move all QA endpoints to seperate file & mount routes cleanly
 *
 * @param: {function} req: the incoming http request
 * @param: {function} res: the outgoing response.
 * @return: user login token
 */
router.get('/getToken', function (req, res) {
  if (!req.query.email) {
    req.log.warn('Invalid user')
    return res.status(402).send({error: 'Invalid user'})
  } else if (appConfig.env === 'qa' || appConfig.env === 'local') {
    var email = req.query.email.toLowerCase()
    Utility.findUserAccount(email, function (err, user) {
      if (err || !user) {
        req.log.error('User not found', {err: err})
        return res.status(402).send({error: 'User not found'})
      }
      var expiryTime = Date.now() + Utility.SESSION_LENGTH
      var token = Utility.secureRandomString(64)

      debug(`user:${email} / token: ${token}`)

      user.auth.access.token = Utility.createHash(token)
      user.auth.access.expires = expiryTime
      user.save(function (err, result) {
        if (err) {
          req.log.error('ERROR: Updating Token for User.', {err: err})
          return res.status(500).send({error: 'Could not update user token'})
        } else {
          res.status(200).send({token: token})
        }
      })
    })
  } else {
    req.log.error('Incorrect environment')
    return res.status(403).send({error: 'Incorrect environment'})
  }
})

/**
 * Get engagement which user was invited by auditor for QA environment to onboard client
 * TODO: move all QA endpoints to seperate file & mount routes cleanly
 *
 * @param: {function} req: the incoming http request
 * @param: {function} res: the outgoing response.
 * @return: engagementID
 */
router.get('/getEID', function (req, res) {
  if (!req.query.email) {
    req.log.warn('Invalid user')
    return res.status(402).send({error: 'Invalid user'})
  } else if (appConfig.env === 'qa' || appConfig.env === 'local') {
    let email = req.query.email.toLowerCase()

    Utility.findEngagement(email, (err, engagements) => {
      if (err || !engagements) {
        req.log.error('Engagement not found', { err: err })
        return res.status(402).send({error: 'Engagement not found'})
      }

      debug('got engagement info from db')
      debug(engagements[0])
      let engagementID = engagements[0]._id.toString()
      debug(engagementID)
      return res.status(200).send({ eid: engagementID })
    })
  } else {
    req.log.error('Incorrect environment')
    return res.status(403).send({error: 'Incorrect environment'})
  }
})

/**
 * Custom middleware function that is responsible for checking url parameters for a 'token'
 * field that we use to grab a unique user. If the user is found, we login with that user in
 * order to tie to them to express' functionality. (Will pass the isAuthenticated() function check.
 *
 * @param: {function} req: the incoming http request
 * @param: {function} res: the outgoing response.
 * @param: {function} next: the next function if user's token is verified.
 */
var checkForToken = function (req, res, next) {
  req.log.info('MIDDLE: checkForToken.')
  if (req.query.token === undefined) {
    req.log.error('Undefined token found!')
    req.logout()
    return next()
  }
  if (req.query.email === undefined) {
    req.log.error('Undefined email!')
    req.logout()
    return next()
  }

  if (Utility.isEmailValid(req.query.email)) {
    req.log.info('Trying to get an user')
    Models.Auvenir.User.findOne({'email': req.query.email}, function (err, oneUser) {
      if (err) {
        req.log.error('ERROR: Error retrieving user.', {err: err})
        req.logout()
        return next()
      }
      if (oneUser) {
        var userID = oneUser.auth.id
        req.log.info('User Object is created.', {userId: userID})
        var valid_token = Utility.checkHash(req.query.token, oneUser.auth.access.token)
        req.log.warn(valid_token)
        req.log.warn(oneUser.auth.access.expires.getTime() > Date.now())
        if (valid_token && oneUser.auth.access.expires.getTime() > Date.now()) {
          req.log.warn('Token is valid, Expiring current token and setting user to session')
          // Update auth token
          if (oneUser.auth.access.passwordResetRequired) {
            oneUser.passwordResetRequired = true
            oneUser.auth.access.passwordResetRequired = false
          }
          oneUser.auth.access.expires = Date.now()
          oneUser.auth.access.token = undefined
          oneUser.save(function (err) {
            if (err) {
              req.log.error(err)
              req.logout()
              return next()
            } else {
              req.log.info('Token is verified.', {userId: userID})
              req.log.info('Logging in user.')
              req.logIn(oneUser, next)
            }
          })
        } else {
          req.log.warn('WARN: Token is Expired')
          req.logout()
          return next()
        }
      } else {
        req.log.warn('WARN: No user found.')
        req.logout()
        return next()
      }
    })
  } else {
    req.log.warn('WARN: Not a valid email address.')
    req.logout()
    return next()
  }
}

/*
 * // TODO - code cleanup after db model change
 *         - this should process Invitation by firm/business Admin to their business/firm.
 *         - not completed yet
 * Custom middleware function that is responsible for processing User Invite to our system.
 * If User is already in system as a member, call next() to process invite user to engagement.
 * If user is invited status, he now first time to login, so his information will be scrapped and he will be upgraded to "ONBOARDING" status and call next();
 *
 * @param: {function} req: the incoming http request
 * @param: {function} res: the outgoing response.
 * @param: {function} next: the next function if user is invited to this System.
 */

var processUserInvite = function (req, res, next) {
  if (!req.user) {
    req.log.warn('ERROR: Unable to process an invite when the user is not authenticated')
    req.logout()
    flash.write(req, 'Unable to process an invite when the user is not authenticated')
    return res.redirect('/')
  }
  if (!req.user.status) {
    req.log.warn('ERROR: User status is missing.')
    req.logout()
    flash.write(req, 'User status is missing.')
    return res.redirect('/')
  }

  if (req.user.status === 'INVITED') {
    Utility.scrapeUserData({email: req.user.email}, (err, result) => {
      if (err) {
        req.log.error({err: err})
        req.logout()
        flash.write(req, 'Error occured while checking intel')
        return res.redirect('/')
      }
      if (result) {
        var userData = result.user
        userData.status = 'ONBOARDING'
        Models.Auvenir.User.findOneAndUpdate({_id: req.user._id}, userData, {new: true}, (err, updatedUser) => {
          if (err) {
            req.log.error('ERROR: Error occurred while finding and updating the user.', {err: err})
            req.logout()
            flash.write(req, 'Error occurred while finding and updating the user.')
            return res.redirect('/')
          }
          if (!updatedUser) {
            req.log.warn('ERROR: User was authenticated, but we were unable to find the user in the db. Exiting.')
            req.logout()
            flash.write(req, 'User was authenticated, but we were unable to find the user in the db.')
            return res.redirect('/')
          }

          Models.Auvenir.Business.findOne({name: result.business.name}, (err, oneBusiness) => {
            if (err) {
              req.log.warn('ERROR: Error detected while finding a business', {err: err})
              req.logout()
              flash.write(req, 'Error detected while finding a business')
              return res.redirect('/')
            }
            if (oneBusiness) {
              var query = {_id: oneBusiness._id}
              Utility.addUserToACL(updatedUser._id, 'Business', { admin: false }, query, (err, updatedBusiness) => {
                if (err) {
                  req.log.error({err: err})
                  req.logout()
                  flash.write(req, 'Error occured while attempting to add the user to the business.')
                  return res.redirect('/')
                }
                return next()
              })
            }
            var jsonBiz = result.business
            jsonBiz.acl = [{ id: updatedUser._id, admin: false }]
            Utility.createBusiness(jsonBiz, function (err, newBusiness) {
              if (err) {
                req.log.error('ERROR: creating new business on db.', {err: err})
                req.logout()
                flash.write(req, 'Error occrured while creating a new business in the database')
                return res.redirect('/')
              }
              if (newBusiness) {
                return next()
              } else {
                req.log.warn('ERROR: Could not get new business created.')
                return next()
              }
            })
          })
        })
      } else {
        req.log.warn('WARN: User data was not found.')
        return next()
      }
    })
  } else {
    next()
  }
}

/**
 * Custom Middleware function for Invitation.
 * After User is invited our system, User clicked the email link.
 * If user was not invited to the engagement, stop process and redirect to home page.
 * Also, if user was invited status, he is now "member" status in engagement acl and call next();
 *
 * @param: {function} req: the incoming http request
 * @param: {function} res: the outgoing response.
 * @param: {function} next: the next function if user is invited to this Engagement.
 */
var processEngagementInvite = function (req, res, next) {
  if (!req.query.eid) {
    req.log.warn('ERROR: No engagement present, user was not invited to an engagement, moving on.', {req: req})
    return next()
  }
  var userID = req.user._id
  var engagementID = req.query.eid
  Models.Auvenir.User.findOne({_id: userID}, function (errFind, user) {
    if (errFind) {
      req.log.error('ERROR: Error while Finding the user.', {err: errFind})
      req.logout()
      flash.write(req, 'Error occured while attempting to find the user.')
      return res.redirect('/')
    } else {
      if (user) {
        user.status = 'ONBOARDING'
        user.lastOnboardedEID = engagementID
        user.save((errSave) => {
          if (errSave) {
            req.log.error('ERROR: Error while Updating the user.', {err: errSave})
            req.logout()
            flash.write(req, 'Error occured while attempting to update the user.')
            return res.redirect('/')
          } else {
            Models.Auvenir.Engagement.findOne({_id: engagementID}, function (err, engagement) {
              if (err) {
                req.log.error('ERROR: Error while finding the engagement.', {err: err})
                req.logout()
                flash.write(req, 'Error occured while attempting to find the engagement.')
                return res.redirect('/')
              }
              if (!engagement) {
                req.log.warn('ERROR: Engagement could not be found with that engagement ID', {req: req})
                return next()
              }
              engagement.acceptInvitation(userID, (err) => {
                debug('acceptInvitation result', err)
                if (err) {
                  if (err.code === 302) {
                    return next()
                  }
                  req.logout()
                  flash.write(req, 'Error occured while attempting to accept engagement invitation')
                  return res.redirect('/')
                }
                return next()
              })
            })
          }
        })
      } else {
        req.log.error('ERROR: Error while Finding the user.', {err: {code: 333, msg: 'No user Exists.'}})
        req.logout()
        flash.write(req, 'No User exists')
        return res.redirect('/')
      }
    }
  })
}

/**
 *
 *
 * @param passport
 * @param config
 * @returns
 */
module.exports = function (passport, config) {
  /** ** GET ROUTES ****/
  router.get('/landingAnalytics', function (req, res) {
    var analytics = {
      google_analytics: { id: appConfig.integrations.google_analytics.id },
      hot_jar: {
        id: appConfig.integrations.hot_jar.id,
        hjsv: appConfig.integrations.hot_jar.hjsv
      },
      intercom: { id: appConfig.integrations.intercom.id }
    }

    res.send(analytics)
  })

  router.get('/hc', function (req, res) {
    res.status(200)
    res.end()
  })

  router.get('/config', isAuthenticated, function (req, res) {
    var myConfig = {
      dependencies: [],
      resources: [],
      userType: req.user.type
    }

    var access = req.user.type
    if (!access) {
      req.log.warn('ERROR: Unknown Portal: ' + access)
      return res.send({code: 1, msg: 'Unknown portal: ' + access})
    }
    myConfig.type = access
    myConfig.environment = appConfig.env
    myConfig.connection = {
      protocol: appConfig.publicServer.protocol,
      domain: appConfig.publicServer.domain,
      port: appConfig.publicServer.port
    }
    myConfig.features = {
      googleAnalytics: { id: appConfig.integrations.google_analytics.id },
      hotJar: {
        id: appConfig.integrations.hot_jar.id,
        hjsv: appConfig.integrations.hot_jar.hjsv
      },
      intercom: { id: appConfig.integrations.intercom.id },
      gdrive: {
        appID: appConfig.integrations.gdrive.appID,
        clientID: appConfig.integrations.gdrive.CLIENT_ID
      }
    }
    myConfig.workingpaperapi = {
      baseUrl: appConfig.workingpaperapi.baseUrl
    }

    return res.send(myConfig)
  })

  /**
   * The link that is used from the invitation email. We use our custom middleware function
   * Check for token for login and process user invite and processEngagementInvite to make sure that he is invited to engagement by his Auditor.
   * If successful, it's a simple redirect to the homepage because his invitation worked successfully through middleware function.
   *
   * @param: {function} req: the incoming http request
   * @param: {function} res: the outgoing response.
   //processUserInvite
   */
  router.get('/acceptInvite', checkForToken, isAuthenticated, processEngagementInvite, function (req, res) {
    res.redirect('/home')
  })

  /**
   * The link that is used from the login email. We use our custom middleware function checkForToken() to
   * make sure that the token value matches up to a user. If successfull it's a simple redirect to the
   * homepage because manually log the user into the system.
   *
   * @param: {function} req: the incoming http request
   * @param: {function} res: the outgoing response.
   */
  router.get('/checkToken', checkForToken, isAuthenticated, function (req, res) {
    debug('checkToken success', req.query, req.user)
    res.redirect('/home')
  })

  /**
   * Return the favicon when the browser does a default lookup
   *
   * @param: {function} req: the incoming http request
   * @param: {function} res: the outgoing response.
   */
  router.get('/favicon.ico', function (req, res) {
    res.sendFile(path.join(global.fileServeRoot, '/images/favicons/faviconAuvenir.ico'))
  })

  /**
   * Currently points to the marketing landing page.
   *
   * @param: {function} req: the incoming http request
   * @param: {function} res: the outgoing response.
   */
  router.get('/', securityHelper.setFrameOption('DENY'), function (req, res) {
    if (appConfig.marketingServer.redirect) {
      return res.redirect(appConfig.marketingServer.url)
    }
    res.sendFile(path.join(global.fileServeRoot, 'auditors.html'))
  })

  /**
   * About Auvenir.
   *
   * @param: {function} req: the incoming http request
   * @param: {function} res: the outgoing response.
   */
  router.get('/about', securityHelper.setFrameOption('DENY'), function (req, res) {
    if (appConfig.marketingServer.redirect) {
      return res.redirect(appConfig.marketingServer.url + '/about')
    }
    res.sendFile(path.join(global.fileServeRoot, 'about.html'))
  })

  /**
   * This is where users can look for career opportunities.
   *
   * @param: {function} req: the incoming http request
   * @param: {function} res: the outgoing response.
   */
  router.get('/careers', securityHelper.setFrameOption('DENY'), function (req, res) {
    if (appConfig.marketingServer.redirect) {
      return res.redirect(appConfig.marketingServer.url + '/careers')
    }
    res.sendFile(path.join(global.fileServeRoot, 'careers.html'))
  })

  /**
   * This is the support page for users.
   *
   * @param: {function} req: the incoming http request
   * @param: {function} res: the outgoing response.
   */
  router.get('/support', securityHelper.setFrameOption('DENY'), function (req, res) {
    res.sendFile(path.join(global.fileServeRoot, 'support.html'))
  })

  /**
   * This is the terms and conditions from the home page.
   *
   * @param: {function} req: the incoming http request
   * @param: {function} res: the outgoing response.
   */
  router.get('/terms', securityHelper.setFrameOption('DENY'), function (req, res) {
    if (appConfig.marketingServer.redirect) {
      return res.redirect(appConfig.marketingServer.url + '/terms')
    }
    res.sendFile(path.join(global.fileServeRoot, 'terms.html'))
  })

  /**
   * This is the cookie notice from the home page.
   *
   * @param: {function} req: the incoming http request
   * @param: {function} res: the outgoing response.
   */
  router.get('/cookies', securityHelper.setFrameOption('DENY'), function (req, res) {
    if (appConfig.marketingServer.redirect) {
      return res.redirect(appConfig.marketingServer.url + '/cookies')
    }
    res.sendFile(path.join(global.fileServeRoot, 'cookies.html'))
  })

  /**
   * This is the privacy policy from the home page.
   *
   * @param: {function} req: the incoming http request
   * @param: {function} res: the outgoing response.
   */
  router.get('/privacy', securityHelper.setFrameOption('DENY'), function (req, res) {
    if (appConfig.marketingServer.redirect) {
      return res.redirect(appConfig.marketingServer.url + '/privacy')
    }
    res.sendFile(path.join(global.fileServeRoot, 'privacy.html'))
  })

  /**
   * Serves the files required for the displaying the homepage functionality
   *
   * @param: {function} req: the incoming http request
   * @param: {function} res: the outgoing response.
   */
  router.get('/home', securityHelper.setFrameOption('DENY'), isAuthenticated, function (req, res) {
    res.sendFile(path.join(global.fileServeRoot, 'home.html'))
  })

  /**
   * Triggers the logout functionality of the session, and then redirects
   * out onto the splash page.
   *
   * @param: {function} req: the incoming http request
   * @param: {function} res: the outgoing response.
   */
  router.get('/logout', securityHelper.setFrameOption('DENY'), function (req, res) {
    if (req.user) {
      var userID = req.user._id
      var type = req.user.type

      var cookies = cookie.parse(req.headers.cookie)
      var sessionID = cookieParser.signedCookie(cookies['connect.sid'], 'mySecretKey')

      var client
      if (appConfig.env === 'local') {
        client = redis.createClient({ host: appConfig.redis.domain, port: appConfig.redis.port })
      } else {
        client = redis.createClient(appConfig.redis.port, appConfig.redis.domain, {auth_pass: appConfig.redis.auth_pass, tls: {servername: appConfig.redis.servername}})
      }
      client.on('error', function (err) {
        req.log.error('Error (Redis) ', {err: err})
        client.quit()
      })

      client.get('sess:' + sessionID, function (err, oneSession) {
        if (err) {
          req.log.error('ERROR: getting session info.', {err: err})
          client.quit()
        } else {
          req.log.warn(oneSession)
          var x = JSON.parse(oneSession)
          x.previous = {user: {id: userID}}

          client.set('sess:' + sessionID, JSON.stringify(x), function (err, result) {
            if (err) {
              req.log.error('ERROR: setting session info.', {err: err})
              client.quit()
            } else {
              req.log.warn(result)
            }
          })
        }
      })

      req.logout()
      switch (type) {
        case 'ADMIN':
          res.redirect('/?logout')
          break
        case 'AUDITOR':
          res.redirect('/?logout')
          break
        case 'CLIENT':
          res.redirect('/?logout')
          break
        default:
          req.log.warn('ERROR: Unknown user type ' + type, {req: req})
          res.redirect('/')
      }
    } else {
      req.log.warn('WARN: No User Found.')
      flash.write(req, 'No user as found while logging out for this request.')
      res.redirect('/')
    }
  })

  /**
   * Return the logo picture of firm.
   *
   * @param: {function} req: the incoming http request
   * @param: {function} res: the outgoing response.
   * @return: the logo picturefile of Firm: File Object.
   */
  router.get('/:firmID/firmLogo', isAuthenticated, function (req, res) {
    if (req.params.firmID !== null && req.params.firmID !== undefined) {
      let objID = Utility.castToObjectId((req.params.firmID).valueOf())
      Models.Auvenir.Firm.findOne({ _id: objID }, function (err, oneFirm) {
        if (err) {
          req.log.error('ERROR: finding firm from db.', {err: err})
          return res.send('There was an error while getting your firm logo. Please contact Auvenir Team.')
        }
        if (!oneFirm) {
          req.log.warn('ERROR: No Firm Found.')
          return res.send('There was an error while getting your firm logo. Please contact Auvenir Team.')
        }
        var reader = new FileReader({query: {root: 'firmLogo', _id: oneFirm.logo.split('/')[1]}})
        reader.exists(function (err) {
          if (err) {
            req.log.error('ERROR: File Reader error at reader.exists', {err: err})
            return res.end('There was an error while getting your firm logo. Please contact Auvenir Team.')
          }
          reader.pipe(res, function (err) {
            if (err) {
              req.log.error('ERROR: File Reader Error at reader.pipe', {err: err})
              return res.end('There was an error while getting your firm logo. Please contact Auvenir Team.')
            }
          })
        })
      })
    } else {
      req.log.warn('ERROR: No UserID found.')
      res.end('There was an error while getting your firm logo. Please contact Auvenir Team.')
    }
  })

  /**
   * TODO - cleanup the flow & how validateFileID works.
   *  Return the file for parameter fileID for user to download.
   *  Using middleware function isAuthenicated and isAccessible, Only authenicated user can access file.
   *
   * @param: {function} req: the incoming http request
   * @param: {function} res: the outgoing response.
   * @return: file as a downloadable content if user has permission to access.
   */
  router.get('/downloadFile/:fileID/:engagementID/:filename', isAuthenticated, function (req, res) {
    let errorMsg = 'Error downloading the file. Please contact Auvenir Team.'
    let fileID = Utility.castToObjectId(req.params.fileID)
    let engagementID = Utility.castToObjectId(req.params.engagementID)
    let userID = Utility.castToObjectId(req.user._id)
    if (!fileID || !engagementID || !userID) {
      req.log.warn('Parameter is not defined...')
      return res.status(404).send(errorMsg)
    }

    const checkAccessToEngagement = (cbk) => {
      Access.engagement(engagementID, userID, (err, engagement) => {
        if (err) {
          req.log.warn(`Invalid access to engagement data ${engagementID} from ${userID}`)
          return cbk({code: 500, msg: errorMsg})
        }
        return cbk(null, engagement)
      })
    }

    const validateFileID = (engagement, cbk) => {
      async.parallel({
        file: function (cbk) {
          Models.Auvenir.File.findOne({ _id: fileID }, (err, file) => {
            if (err) {
              req.log.error(err)
              return cbk({ code: 500, msg: errorMsg})
            }
            if (!file) {
              return cbk({code: 500, msg: 'File is not available.'})
            }
            return cbk(null, file)
          })
        },
        validate: function (cbk) {
          let found = false
          engagement.files.forEach((fID, i) => {
            if (fID.toString() === fileID.toString()) {
              found = true
            }
            if (i === engagement.files.length - 1) {
              if (!found) {
                return cbk({code: 500, msg: errorMsg})
              } else {
                return cbk(null, fileID)
              }
            }
          })
        }
      }, (err, result) => {
        if (err) {
          return cbk(err)
        }
        return cbk(null, result.file)
      })
    }

    async.waterfall([checkAccessToEngagement, validateFileID], (err, file) => {
      if (err) {
        return res.status(err.code).send(err.msg)
      } else {
        if (file) {
          if (file.bucket) {
            var reader = new FileReader({query: {root: file.bucket.name, _id: file.bucket.id}})
            reader.exists(function (err, fsFile) {
              if (err) {
                req.log.error('ERROR: File Reader Error at reader.exists', {err: err})
                return res.status(500).send(errorMsg)
              }
              if (req.query.option && req.query.option === 'view') {
                res.set('Content-Type', fsFile.contentType)
              } else {
                res.attachment(file.name)
              }
              reader.pipe(res, function (err) {
                if (err) {
                  req.log.error('ERROR: File Reader Error at reader.pipe', {err: err})
                  return res.status(500).send(errorMsg)
                }
              })
            })
          } else {
            return res.status(404).send(errorMsg)
          }
        } else {
          return res.status(404).send(errorMsg)
        }
      }
    })
  })

  /**
   * Return the file for parameter fileID for user to open if web browser can open or download it.
   *
   * @param: {function} req: the incoming http request
   * @param: {function} res: the outgoing response.
   * @return: file as a openable content if user has permission to access.
   */
  router.get('/attachment/:fileID/:fileName', isAuthenticated, function (req, res) {
    if (req.params.fileID !== undefined && req.params.fileID !== null) {
      var reader = new FileReader({query: {root: 'messageAttachment', _id: req.params.fileID}})
      reader.exists(function (err) {
        if (err) {
          req.log.error('ERROR: File Reader Error', {err: err})
          return res.send('Error opening the file. Please contact Auvenir Team.')
        }

        var fileName = req.params.fileName || req.params.fileID
        res.attachment(fileName)
        reader.pipe(res, function (err) {
          if (err) {
            req.log.error('ERROR: File Reader Error', {err: err})
            return res.send('Error opening the file. Please contact Auvenir Team.')
          }
        })
      })
    } else {
      req.log.warn('ERROR: Invalid attachment request. Missing file name.')
      res.send('Error opening the file. Please contact Auvenir Team.')
    }
  })

  /**
   * TODO - cleanup the flow & how validateFileID works.
   * Return zip file of all the files for engagement to response.
   *
   * @param: {function} req: the incoming http request
   * @param: {function} res: the outgoing response.
   */
  router.get('/download-engagement-data', isAuthenticated, function (req, res) {
    var engagementID = req.query.engagementID

    if (!engagementID) {
      req.log.error('ERROR: Unknown engagement ID')
      return res.send('Error occured during download request. Contact the Auvenir team.')
    }

    engagementID = Utility.castToObjectId(engagementID)

    const checkAccessToEngagement = (cbk) => {
      Access.engagement(engagementID, req.user._id, (err, engagement) => {
        if (err) {
          req.log.warn(`Invalid access to engagement data ${engagementID} from ${req.user._id}`)
          return cbk({code: 500, msg: 'There was an error while downloading engagement data.'})
        }

        cbk(null, engagement)
      })
    }

    const getFileList = (engagement, cbk) => {
      Models.Auvenir.File.find({_id: {$in: engagement.files}}, (err, files) => {
        if (err) {
          return cbk({code: 500, msg: 'Error on retrieving Files'})
        }
        if (!files) {
          return cbk({code: 404, msg: 'Unable to find Files'})
        }
        cbk(null, engagement, files)
      })
    }

    const archiveFiles = (err, engagement, files) => {
      if (err) {
        if (err.code && err.msg) {
          return res.status(err.code).send(err.msg)
        }
      }
      var archive = archiver('zip', {})
      archive.on('error', (err) => {
        if (err) {
          req.log.error('Archiver Error', {err})
          return res.status(500).send('There was an error while archiving the files.')
        }
      })

      async.each(files, (file, cbk) => {
        if (file && file.bucket && file.bucket.name && file.bucket.id && file.status === 'ACTIVE') {
          // Local Files
          var query = {root: file.bucket.name, _id: file.bucket.id}

          auvenirGFS.findOne(query, (err, gfsFile) => {
            if (err) {
              // Pass Next File, Ignore this File.
              req.log.error('Error on retrieving GFS File Object', {err})
              cbk()
            } else {
              if (!gfsFile) {
                req.log.warn('No Exists for GFS File')
                cbk()
              } else {
                var encrypted = gfsFile.metadata ? gfsFile.metadata.encrypted : false
                var gridFSStream = auvenirGFS.createReadStream(query)
                gridFSStream.on('error', (err) => {
                  if (err) {
                    // Error on Streaming file Process next one.
                    req.log.error(`Error on File Stream for file ${file.name}`, {err})
                    cbk()
                  }
                })
                if (encrypted) {
                  var cryptoStream = crypto.createDecipher(encryptAlgorithm, encryptPassword)
                  gridFSStream.pipe(cryptoStream)
                  archive.append(cryptoStream, {name: file.path + file.name})
                } else {
                  archive.append(gridFSStream, {name: file.path + file.name})
                }
                cbk()
              }
            }
          })
        } else if (file && file.status === 'ACTIVE' && file.source && file.source.name === 'Google Drive' && file.source.uid && file.source.fid) {
          // Google Drive
          debug('Getting Google File object.')
          Models.Gdrive.Item.findOne({_id: file.source.fid}, function (err, gdrive) {
            if (err) {
              req.log.error('ERROR: Finding Google Drive File Object', {err: err})
              cbk()
            } else {
              if (!gdrive) {
                req.log.error('ERROR: Google Drive Object Not Exists')
                cbk()
              } else {
                var query = {root: 'fs', _id: gdrive.fileID}
                googleGFS.findOne(query, function (err, gdriveGFSObject) {
                  if (err) {
                    req.log.error('ERROR: Finding Google Drive File Chunks', {err})
                    cbk()
                  } else {
                    if (!gdriveGFSObject) {
                      req.log.warn('ERROR: Finding Google Drive File Chunk')
                      cbk()
                    } else {
                      var encrypted = gdriveGFSObject.metadata ? gdriveGFSObject.metadata.encrypted : false
                      var gridFSStream = googleGFS.createReadStream(query)
                      gridFSStream.on('error', function (err) {
                        if (err) {
                          req.log.error('ERROR: Grid FS Streaming issue', {err})
                        }
                        // Error on Streaming File, do next one.
                        cbk()
                      })
                      if (encrypted) {
                        var cryptoStream = crypto.createDecipher(encryptAlgorithm, encryptPassword)
                        gridFSStream.pipe(cryptoStream)
                        archive.append(cryptoStream, {name: file.path + file.name})
                      } else {
                        archive.append(gridFSStream, {name: file.path + file.name})
                      }
                      cbk()
                    }
                  }
                })// GoogleGFS Find One
              }
            }
          }) // Gdrive Object
        } else {
          req.log.warn('Real File Data does not exists! Proceed to next file.')
          cbk()
        }
      }, (err) => {
        if (err) {
          res.status(err.code).send(err.msg)
        } else {
          archive.finalize()
          res.attachment(engagement.name + '.zip')
          archive.pipe(res)
        }
      }) // Async Each
    } // ArchiveFiles
    async.waterfall([checkAccessToEngagement, getFileList], archiveFiles)
  })

  /**
   * TODO - cleanup the flow & how validateFileID works.
   * Return zip file of all the files for selected folder
   *
   * @param: {function} req: the incoming http request
   * @param: {function} res: the outgoing response.
   */
  router.post('/download-folder', isAuthenticated, function (req, res) {
    let route = '/download-folder'
    let defaultErrMsg = 'There was an error while downloading folder.'
    let userID = req.user._id
    let engagementID = req.query.eID
    let structure = req.query.structure
    let zipFile = auvenirGFS.createWriteStream({ filename: `${engagementID}.${userID}.zip`})

    debug('downloading selected folder')

    if (!engagementID) {
      req.log.error('ERROR: Unknown engagement ID')
      return res.status(500).json({ msg: defaultErrMsg })
    }

    engagementID = Utility.castToObjectId(engagementID)

    const parsingForm = (cbk) => {
      let selectedItemJSON = {}
      let form = new formidable.IncomingForm()
      form.parse(req, (err, field) => {
        if (err) {
          req.log.error(`${route} -- Error: parsing req with form info.`, { err })
          cbk(err)
        }
        if (!field) {
          req.log.error(`${route} -- ERROR: No fields passed through request.`)
          cbk('No Field was passed.')
        }
        if (!field.items || !_.isString(field.items)) {
          req.log.error(`${route} -- ERROR: Invalid folders were passed through request.`)
          cbk('Invalid folders passed.')
        }

        selectedItemJSON = JSON.parse(field.items)
        return cbk(null, selectedItemJSON)
      })
    }

    const checkAccessToEngagement = (selectedItemJSON, cbk) => {
      Access.engagement(engagementID, req.user._id, (err, engagement) => {
        if (err) {
          req.log.warn(`Invalid access to engagement data ${engagementID} from ${req.user._id}`)
          return cbk('There was an error while downloading engagement data.')
        }

        cbk(null, selectedItemJSON, engagement)
      })
    }

    const findChildrenItems = (selectedItemJSON, engagement, cbk) => {
      req.log.warn('FIND CHILDREN ITEMS...')
      let selectedItemArray = []
      let filesOnlyArray = []
      if (structure === 'Flat' || structure === 'Folder' || !structure) {
        for (let item in selectedItemJSON) {
          selectedItemArray.push(selectedItemJSON[item])
        }
        const getChildren = (childrenArray) => {
          childrenArray.forEach((item, i) => {
            if (item.fileType === 'folder' && item.children.length > 0) {
              getChildren(item.children)
            } else {
              filesOnlyArray.push(item)
            }
          })
        }

        selectedItemArray.forEach((item, i) => {
          // if the item is folder.
          if (item.fileType === 'folder' && item.children.length > 0) {
            getChildren(item.children)
          } else { // if the item is file.
            filesOnlyArray.push(item)
          }

          if (i === selectedItemArray.length - 1) {
            if (filesOnlyArray.length > 0) {
              req.log.warn(filesOnlyArray)
              cbk(null, filesOnlyArray, engagement)
            } else {
              cbk('No files found.')
            }
          }
        })
      } else if (structure === 'Todo') {
        _.forEach(selectedItemJSON, (value, key) => { // 'key' is todo name.
          _.forEach(selectedItemJSON[key], (fileItem, fileIDKey) => {
            fileItem.todo = key
            filesOnlyArray.push(fileItem)
          })
        })
        cbk(null, filesOnlyArray, engagement)
      }
    }

    const archiveFiles = (err, files, engagement) => {
      if (err) {
        return res.status(500).json({ msg: defaultErrMsg })
      }

      let archive = archiver('zip', {})
      archive.on('error', (err) => {
        if (err) {
          req.log.error('Archiver Error', { err })
          return res.status(500).json({ msg: defaultErrMsg })
        }
      })
      async.each(files, (file, cbk) => {
        let cbkCalled = false
        if (file && file.bucket && file.bucket.name && file.bucket.id && file.status === 'ACTIVE') {
          // Local Files
          var query = { root: file.bucket.name, _id: file.bucket.id }

          auvenirGFS.findOne(query, (err, gfsFile) => {
            if (err) {
              // Pass Next File, Ignore this File.
              req.log.error('Error on retrieving GFS File Object', { err })
              if (!cbkCalled) {
                cbkCalled = true
                cbk()
              }
              return
            }
            if (!gfsFile) {
              req.log.warn('No Exists for GFS File')
              if (!cbkCalled) {
                cbkCalled = true
                cbk()
              }
              return
            }
            var encrypted = gfsFile.metadata ? gfsFile.metadata.encrypted : false
            var gridFSStream = auvenirGFS.createReadStream(query)
            gridFSStream.on('error', (err) => {
              if (err) {
                // Error on Streaming file Process next one.
                req.log.error(`Error on File Stream for file ${file.name}`, { err })
                if (!cbkCalled) {
                  cbkCalled = true
                  cbk()
                }
                return
              }
            })
            let filePath = ''
            switch (structure) {
              case 'Flat': // filePath stays ''
                break
              case 'Todo':
                filePath = file.todo + '/'
                break
              case 'Folder':
              default:
                filePath = file.path
                break
            }
            if (encrypted) {
              var cryptoStream = crypto.createDecipher(encryptAlgorithm, encryptPassword)
              gridFSStream.pipe(cryptoStream)
              archive.append(cryptoStream, { name: filePath + file.name })
            } else {
              archive.append(gridFSStream, { name: filePath + file.name })
            }
            if (!cbkCalled) {
              cbkCalled = true
              cbk()
            }
            return
          })
        } else if (file && file.status === 'ACTIVE' && file.source && file.source.name === 'Google Drive' && file.source.uid && file.source.fid) {
          // Google Drive
          debug('Getting Google File object.')
          Models.Gdrive.Item.findOne({ _id: file.source.fid }, function (err, gdrive) {
            if (err) {
              req.log.error('ERROR: Finding Google Drive File Object', { err: err })
              if (!cbkCalled) {
                cbkCalled = true
                cbk()
              }
              return
            }
            if (!gdrive) {
              req.log.error('ERROR: Google Drive Object Not Exists')
              if (!cbkCalled) {
                cbkCalled = true
                cbk()
              }
              return
            }
            var query = { root: 'fs', _id: gdrive.fileID }
            googleGFS.findOne(query, function (err, gdriveGFSObject) {
              if (err) {
                req.log.error('ERROR: Finding Google Drive File Chunks', { err })
                if (!cbkCalled) {
                  cbkCalled = true
                  cbk()
                }
                return
              }
              if (!gdriveGFSObject) {
                req.log.warn('ERROR: Finding Google Drive File Chunk')
                cbk()
              } else {
                var encrypted = gdriveGFSObject.metadata ? gdriveGFSObject.metadata.encrypted : false
                var gridFSStream = googleGFS.createReadStream(query)
                gridFSStream.on('error', function (err) {
                // Error on Streaming File, do next one.
                  if (err) {
                    req.log.error('ERROR: Grid FS Streaming issue', { err })
                    if (!cbkCalled) {
                      cbkCalled = true
                      cbk()
                    }
                    return
                  }
                })
                let filePath = ''
                switch (structure) {
                  case 'Flat': // filePath stays ''
                    break
                  case 'Todo':
                    filePath = file.todo + '/'
                    break
                  case 'Folder':
                  default:
                    filePath = file.path
                    break
                }
                if (encrypted) {
                  var cryptoStream = crypto.createDecipher(encryptAlgorithm, encryptPassword)
                  gridFSStream.pipe(cryptoStream)
                  archive.append(cryptoStream, { name: filePath + file.name })
                } else {
                  archive.append(gridFSStream, { name: filePath + file.name })
                }
                if (!cbkCalled) {
                  cbkCalled = true
                  cbk()
                }
                return
              }
            })// GoogleGFS Find One
          }) // Gdrive Object
        } else {
          req.log.warn('Real File Data does not exists! Proceed to next file.')
          if (!cbkCalled) {
            cbkCalled = true
            cbk()
          }
          return
        }
      }, (err) => {
        if (err) {
          return res.status(500).json({ msg: defaultErrMsg })
        }
        archive.finalize()
        archive.pipe(zipFile)
        zipFile.on('close', (file) => {
          let zipUrl = `download-files/${engagementID}.${userID}.zip`
          return res.status(200).json({ url: zipUrl })
        })
        zipFile.on('error', (err) => {
          req.log.error('Error writing file: ' + zipFile.fileName, { err })
          return res.status(500).json({ msg: `Something went wrong while saving zip file on mongo`})
        })
      }) // Async Each
    }

    async.waterfall([parsingForm, checkAccessToEngagement, findChildrenItems], archiveFiles)
  })

  /**
   * This route will send zip file that user archived & delete them after the file is sent.
   */
  router.get('/download-files/:zipFile/:eName', function (req, res) {
    req.log.info('download-files triggered...')
    let userID = req.user._id
    let query = { root: 'fs', filename: req.params.zipFile }

    const checkUserAccess = (cbk) => {
      let engagementID = (req.params.zipFile).split('.')[0]
      Access.engagement(engagementID, userID, (err, engagement) => {
        if (err) {
          req.log.warn(`Invalid access to engagement data ${engagementID} from ${req.user._id}`)
          return cbk('There was an error while downloading files.')
        }

        return cbk(null)
      })
    }

    const downloadZipFile = (cbk) => {
      auvenirGFS.findOne(query, (err, gfsFile) => {
        if (err) {
          return cbk(err)
        }
        if (!gfsFile) {
          return cbk('No zip file found under the same name.')
        }

        var gridFSStream = auvenirGFS.createReadStream(query)
        gridFSStream.on('error', (err) => {
          if (err) {
            return cbk(err)
          }
        })
        res.attachment(req.params.eName + '.zip')
        gridFSStream.pipe(res)
        return cbk(null, gfsFile)
      })
    }

    async.waterfall([checkUserAccess, downloadZipFile], (err, gfsFile) => {
      if (err) {
        req.log.error(err)
        return res.status(500).json({ msg: 'There was an error while downloading your files.' })
      }

      req.on('end', () => {
        if (res.finished) {
          auvenirGFS.remove(query, (err) => {
            if (err) {
              req.log.error(`${gfsFile.filename} could not be removed successfully.`)
              req.log.error({err})
            } else {
              req.log.info(`${gfsFile.filename} deleted successfully!`)
            }
          })
        }
      })
    })
  })

  /**
   * Querying session info for the user.
   *
   * @param: {function} req: the incoming http request.
   * @param: {function} res: the outgoing response.
   */
  router.get('/session-info', function (req, res) {
    var cookies = cookie.parse(req.headers.cookie)
    var sessionID = cookieParser.signedCookie(cookies['connect.sid'], 'mySecretKey')

    var client
    if (appConfig.env === 'local') {
      client = redis.createClient({ host: appConfig.redis.domain, port: appConfig.redis.port })
    } else {
      client = redis.createClient(appConfig.redis.port, appConfig.redis.domain, {auth_pass: appConfig.redis.auth_pass, tls: {servername: appConfig.redis.servername}})
    }
    client.on('error', function (err) {
      req.log.error('ERROR: Error (Redis): ', {err: err})
      return sendRes(res, 500, 'Error occurred while working with Redis')
    })

    client.get('sess:' + sessionID, function (err, result) {
      client.quit()
      if (err) {
        req.log.error('ERROR: getting redis session.', {err: err})
        return sendRes(res, 500, 'Error occurred in redis session retrieval')
      }
      if (!result) {
        return sendRes(res, 404, 'No session found in database')
      }

      var r = JSON.parse(result)
      if (r.flash && r.flash !== '') {
        flash.write(req, '')
      }

      if (r && r.passport && r.passport.user && r.passport.user.id) {
        let userID = Utility.castToObjectId(r.passport.user.id)
        Models.Auvenir.User.findOne({_id: userID}, {
          firstName: 1,
          lastName: 1,
          email: 1,
          type: 1
        }, (err, result) => {
          if (err) {
            req.log.error('ERROR: retriving user session info from db.', {err: err})
            return sendRes(res, 500, 'Error occurred while retrieving Session User')
          }
          if (result) {
            result.status = 'current'
            var json = result.toObject()
            if (r.flash) {
              json.flash = r.flash
            }
            return sendRes(res, 200, json)
          } else {
            return sendRes(res, 404, 'No result found!')
          }
        })
      } else if (r && r.previous && r.previous.user && r.previous.user.id) {
        let userID = Utility.castToObjectId(r.previous.user.id)
        Models.Auvenir.User.findOne({_id: userID}, {
          firstName: 1,
          lastName: 1,
          email: 1,
          type: 1
        }, function (err, result) {
          if (err) {
            req.log.error('ERROR: retriving user session info from db.', {err: err})
            return sendRes(res, 500, 'Error occurred while retrieving Session User')
          }
          if (result) {
            req.log.info('Displaying user session info: ' + result)
            result.status = 'previous'
            var json = result.toObject()
            if (r.flash) {
              json.flash = r.flash
            }
            return sendRes(res, 200, json)
          } else {
            return sendRes(res, 404, 'No result found!')
          }
        })
      } else {
        return sendRes(res, 404, {flash: r.flash})
      }
    })
  })

  /** ** POST ROUTES ****/
  /**
   * Login a user if the email exists, register them otherwise.
   * If user is already in our system, send login link to that mail.
   * If user is not in our system, create account as PENDING status and send email that he is now in our sytem as a PENDING status.
   *
   * @param: {function} req: the incoming http request
   * @param: {function} res: the outgoing response.
   */
  router.post('/login', function (req, res) {
    // Login Flow
    validateFields(req)
      .then(findOrCreateUser)
      .then(checkUserStatus)
      .then(createLoginToken)
      .then(sendEmail)
      .catch((err) => {
        if (err.code && err.msg) {
          return res.json(err)
        } else {
          req.log.error('LOGIN: Uncaught error', {err})
          return res.json({code: 500, msg: 'There was an issue logging in. Please contact support@auvenir.com.'})
        }
      })

    // Promise Functions
    function validateFields (req) {
      return new Promise(function (resolve, reject) {
        var form = new formidable.IncomingForm()
        form.maxFieldsSize = 10 * 1024 * 1024
        form.keepExtensions = true
        form.parse(req, (err, fields) => {
          if (err) return reject(err)
          if (!fields || !fields.email) return reject({code: 400, msg: 'Invalid Fields'})
          fields.email = fields.email.toLowerCase().trim()
          resolve(fields)
        })
      })
    }

    // TODO - for Admin, we could create account right away but otherwise client & auditor should already been created.
    function findOrCreateUser ({email, password, type}) {
      return new Promise(function (resolve, reject) {
        Utility.findUserAccount(email, (err, user) => {
          if (err) {
            reject({code: 400, msg: 'There was an issue finding your account'})
          } else if (user) {
            if (password) return reject({code: 400, msg: 'Invalid Fields'})
            // Return the registered user
            resolve({user, newUser: false})
          } else {
            // Create the user account
            const json = {email}
            var suffix = '@auvenir.com'
            var index = email.indexOf(suffix)
            if ((index !== -1) && ((email.length - index) === suffix.length)) {
              json.type = 'ADMIN'
            } else if (type === 'AUDITOR') {
              json.type = type
            } else if (type === 'CLIENT') {
              reject({code: 500, msg: 'Client must be invited by an Auditor.'})
            } else {
              reject({code: 500, msg: 'Invalid user type'})
            }
            Utility.createUserAccount(json, (err, result) => {
              if (err) {
                req.log.error(err.msg)
                reject({code: 400, msg: 'Error creating user'})
              }
              const user = result.user
              if (!user) {
                reject({code: 500, msg: 'Cannot create a user'})
              }
              resolve({ user })
            })
          }
        })
      })
    }

    function checkUserStatus ({user}) {
      return new Promise(function (resolve, reject) {
        let template
        switch (user.status) {
          case 'ACTIVE':
            template = {
              code: 202,
              msg: 'Check your Email!',
              msg2: `We sent a login link to ${user.email}`,
              msg3: 'images/illustrations/login.svg',
              email: user.email,
              firstName: user.firstName,
              lastName: user.lastName
            }
            resolve({user, status: ACTIVE, template})
            break
          case 'ONBOARDING':
            template = {
              code: 202,
              msg: 'Check your Email!',
              msg2: `We sent a login link to ${user.email}`,
              msg3: 'images/illustrations/login.svg',
              email: user.email,
              firstName: user.firstName,
              lastName: user.lastName
            }
            resolve({user, status: ONBOARDING, template})
            break
          case 'PENDING':
            template = {
              code: 200,
              email: user.email,
              msg: 'Check your Email!',
              msg2: 'Please login from the email invitation sent by your auditor.  If you did not receive one, please contact your auditor.',
              msg3: 'images/illustrations/waitlist.svg'
            }
            resolve({user, status: PENDING, template})
            break
          case 'WAIT-LIST':
            template = {
              code: 200,
              email: user.email,
              msg: 'Awaiting approval!',
              msg2: 'We are currently reviewing your credentials and will let you know when you\'ve been approved.',
              msg3: 'images/illustrations/waitlist.svg'
            }
            resolve({user, status: WAITLIST, template})
            break
          case 'LOCKED':
            reject({
              code: 200,
              email: user.email,
              msg: 'Your account is locked!',
              msg2: 'Please contact our Customer Service at info@auvenir.com.',
              msg3: 'images/illustrations/lock.svg'
            })
            break
          default:
            reject({
              code: 400,
              msg: 'Account deactivated.',
              msg2: 'Sorry but your account has been deactivated. Please contact support@auvenir.com.'
            })
            break
        }
      })
    }

    function createLoginToken ({user, status, template}) {
      return new Promise(function (resolve, reject) {
        if (status === ACTIVE || status === ONBOARDING) {
          var token = Utility.secureRandomString(64)
          user.auth.access.token = Utility.createHash(token)
          // Extending expiry date of user authentication if local environment for testing convenience
          if (appConfig.env === 'local') {
            var extendedTime = new Date()
            extendedTime.setFullYear(extendedTime.getFullYear() + 1)
            user.auth.access.expires = extendedTime.getTime() + Utility.SESSION_LENGTH
          } else {
            user.auth.access.expires = Date.now() + Utility.SESSION_LENGTH
          }
          user.save((err) => {
            if (err) {
              reject({code: 400, msg: 'Error creating login token'})
            } else {
              resolve({user, status, template, token})
            }
          })
        } else {
          resolve({user, status, template, token})
        }
      })
    }

    function sendEmail ({user, status, template, token}) {
      return new Promise(function (resolve, reject) {
        const {email, firstName, lastName} = user
        const customValues = {
          url: appConfig.publicServer.url,
          token: token,
          email: email
        }
        if (status !== PENDING) {
          Utility.sendEmail({
            fromEmail: 'andi@auvenir.com',
            toEmail: email,
            toName: firstName + ' ' + lastName,
            status,
            customValues
          }, (err) => {
            if (err) {
              req.log.error('ERROR: sending email.', {err: err})
              reject({code: 400, msg: 'Error sending email'})
            } else {
              req.log.info('Email Sent Successfully.')
              reject(template)
            }
          })
        } else {
          reject(template)
        }
      })
    }
  })

  /**
   * Login a user if the email exists, register them otherwise.
   * If user is already in our system, send login link to that mail.
   * If user is not in our system, create account as PENDING status and send email that he is now in our sytem as a PENDING status.
   *
   * @param: {function} req: the incoming http request
   * @param: {function} res: the outgoing response.
   */
  router.options('/weblogin', setAllowOriginHeaders)

  router.post('/weblogin', function (req, res) {
    setAllowOriginHeaders(req, res)
    // Login Flow
    validateFields(req)
      .then(authenticateUser)
      .then(checkUserStatus)
      .then(createLoginToken)
      .then(sendRedirect)
      .catch((err) => {
        if (err.code && err.msg) {
          return res.json(err)
        } else {
          req.log.error('WEBLOGIN: Uncaught error', {err})
          return res.json({code: 500, msg: 'There was an issue logging in. Please contact support@auvenir.com.'})
        }
      })

    // Promise Functions
    function validateFields (req) {
      return new Promise(function (resolve, reject) {
        if (!req.body || !req.body.email || (!req.body.password && !req.body.type)) {
          return reject({code: 400, msg: 'Invalid Fields'})
        }
        req.body.email = req.body.email.toLowerCase().trim()
        resolve(req.body)
      })
    }

    function authenticateUser ({email, password, type}) {
      return new Promise(function (resolve, reject) {
        Utility.findUserAccount(email, (err, user) => {
          if (err || !user) {
            return reject({code: 400, msg: 'There was an issue finding your account'})
          }
          Utility.validateUserPassword(user, password, (err) => {
            if (err) return reject(err)
            resolve({user, newUser: false})
          })
        })
      })
    }

    function checkUserStatus ({user}) {
      return new Promise(function (resolve, reject) {
        let template
        switch (user.status) {
          case 'ACTIVE':
            template = {
              code: 202,
              msg: 'Check your Email!',
              msg2: `We sent a login link to ${user.email}`,
              msg3: 'images/illustrations/login.svg',
              email: user.email,
              firstName: user.firstName,
              lastName: user.lastName
            }
            resolve({user, status: ACTIVE, template})
            break
          case 'ONBOARDING':
            template = {
              code: 202,
              msg: 'Check your Email!',
              msg2: `We sent a login link to ${user.email}`,
              msg3: 'images/illustrations/login.svg',
              email: user.email,
              firstName: user.firstName,
              lastName: user.lastName
            }
            resolve({user, status: ONBOARDING, template})
            break
          case 'PENDING':
            template = {
              code: 200,
              email: user.email,
              msg: 'Check your Email!',
              msg2: 'Please login from the email invitation sent by your auditor.  If you did not receive one, please contact your auditor.',
              msg3: 'images/illustrations/waitlist.svg'
            }
            resolve({user, status: PENDING, template})
            break
          case 'WAIT-LIST':
            template = {
              code: 200,
              email: user.email,
              msg: 'Awaiting approval!',
              msg2: 'We are currently reviewing your credentials and will let you know when you\'ve been approved.',
              msg3: 'images/illustrations/waitlist.svg'
            }
            resolve({user, status: WAITLIST, template})
            break
          case 'LOCKED':
            reject({
              code: 200,
              email: user.email,
              msg: 'Your account is locked!',
              msg2: 'Please contact our Customer Service at info@auvenir.com.',
              msg3: 'images/illustrations/lock.svg'
            })
            break
          default:
            reject({
              code: 400,
              msg: 'Account deactivated.',
              msg2: 'Sorry but your account has been deactivated. Please contact support@auvenir.com.'
            })
            break
        }
      })
    }

    function createLoginToken ({user, status, template}) {
      return new Promise(function (resolve, reject) {
        if (status === ACTIVE || status === ONBOARDING) {
          var token = Utility.secureRandomString(64)
          user.auth.access.token = Utility.createHash(token)
          // Extending expiry date of user authentication if local environment for testing convenience
          if (appConfig.env === 'local') {
            var extendedTime = new Date()
            extendedTime.setFullYear(extendedTime.getFullYear() + 1)
            user.auth.access.expires = extendedTime.getTime() + Utility.SESSION_LENGTH
          } else {
            user.auth.access.expires = Date.now() + Utility.SESSION_LENGTH
          }
          user.save((err) => {
            if (err) {
              reject({code: 400, msg: 'Error creating login token'})
            } else {
              resolve({user, status, template, token})
            }
          })
        } else {
          resolve({user, status, template, token})
        }
      })
    }

    function sendRedirect ({user, status, template, token}) {
      return new Promise(function (resolve, reject) {
        res.json({code: 200, email: user.email, token: token})
        res.end()
        resolve()
      })
    }
  })

  /**
   * Login a user if the email exists, register them otherwise.
   * If user is already in our system, send login link to that mail.
   * If user is not in our system, create account as PENDING status and send email that he is now in our sytem as a PENDING status.
   *
   * @param: {function} req: the incoming http request
   * @param: {function} res: the outgoing response.
   */
  router.options('/websignup', setAllowOriginHeaders)

  router.post('/websignup', function (req, res) {
    setAllowOriginHeaders(req, res)
    validateFields(req)
      .then(createUser)
      .then(checkUserStatus)
      .then(createLoginToken)
      .then(sendEmail)
      .catch((err) => {
        if (err.code && err.msg) {
          const code = err.code < 200 ? 400 : err.code
          return res.status(code).json({data: {message: err.msg}})
        } else {
          req.log.error(err)
          return res.status(500).json({data: {message: 'There was an issue logging in. Please contact support@auvenir.com.'}})
        }
      })

    // Promise Functions
    function validateFields (req) {
      return new Promise(function (resolve, reject) {
        if (!req.body) return reject({code: 400, msg: 'Invalid fields'})
        const member = req.body.member
        if (!member || !_.isObject(member) || !_.isString(member.email)) return reject({code: 400, msg: 'Invalid fields'})
        member.email = member.email.trim().toLowerCase()
        let mes = member.email.split('@')
        if (mes.length === 2 && mes[1] === 'auvenir.com') {
          member.type = 'ADMIN'
        } else {
          member.type = 'AUDITOR'
          member.password = ''
        }
        member.jobTitle = member.roleInFirm
        member.referral = member.hearFrom
        member.skipOnboard = true
        let firm = req.body.firm
        if (firm) {
          if (!_.isObject(firm)) return reject({code: 400, msg: 'Invalid fields'})
          let address = firm.address
          if (address) {
            if (!_.isObject(address)) return reject({code: 400, msg: 'Invalid fields'})
            address = {
              unit: address.suitNumber,
              streetAddress: address.streetAddress,
              city: address.city,
              stateProvince: address.stateProvince,
              postalCode: address.postalCode,
              country: address.country
            }
          } else {
            address = {}
          }
          firm = {
            name: firm.name,
            logo: firm.logo,
            website: firm.website,
            size: firm.numberOfEmployee,
            phone: firm.phone.replace('-', ''),
            affiliatedFirmName: firm.affiliatedFirmName,
            logoDisplayAgreed: firm.logoDisplayAgreed,
            previousName: firm.previousName,
            memberID: firm.memberID,
            address: address
          }
          if (_.isString(firm.affiliatedFirmName) && firm.affiliatedFirmName !== '') firm.affiliated = true
          if (_.isString(firm.previousName) && firm.previousName !== '') firm.nameChange = true
          if (_.isString(firm.address.postalCode)) firm.address.postalCode = firm.address.postalCode.toUpperCase()
        }
        resolve({member, firm})
      })
    }

    function createUser ({member, firm}) {
      return new Promise(function (resolve, reject) {
        Utility.createUserAccount(member, (err, result) => {
          if (err) return reject(err)
          if (result.firm) {
            if (firm) {
              firm.firmID = result.firm._id
              return Utility.updateFirm(firm, result.user._id, (err) => {
                if (err) return reject(err)
                firm = _.defaultsDeep(firm, result.firm)
                resolve({user: result.user, firm: firm})
              })
            }
            return resolve({user: result.user, firm: result.firm})
          }
          firm.acl = [{id: result.user._id, admin: true}]
          Utility.createFirm(firm, (err, firm) => {
            if (err) return reject(err)
            return resolve({user: result.user, firm: firm})
          })
        })
      })
    }

    function checkUserStatus ({user}) {
      return new Promise(function (resolve, reject) {
        let template
        switch (user.status) {
          case 'ACTIVE':
            template = {
              code: 200,
              msg: 'Check your Email!',
              msg2: `We sent a login link to ${user.email}`,
              msg3: 'images/illustrations/login.svg',
              email: user.email,
              firstName: user.firstName,
              lastName: user.lastName
            }
            resolve({user, status: ACTIVE, template})
            break
          case 'ONBOARDING':
            template = {
              code: 200,
              msg: 'Check your Email!',
              msg2: `We sent a login link to ${user.email}`,
              msg3: 'images/illustrations/login.svg',
              email: user.email,
              firstName: user.firstName,
              lastName: user.lastName
            }
            resolve({user, status: ONBOARDING, template})
            break
          case 'PENDING':
            template = {
              code: 200,
              email: user.email,
              msg: 'Check your Email!',
              msg2: 'Please login from the email invitation sent by your auditor.  If you did not receive one, please contact your auditor.',
              msg3: 'images/illustrations/waitlist.svg'
            }
            resolve({user, status: PENDING, template})
            break
          case 'WAIT-LIST':
            template = {
              code: 200,
              email: user.email,
              msg: 'Awaiting approval!',
              msg2: 'We are currently reviewing your credentials and will let you know when you\'ve been approved.',
              msg3: 'images/illustrations/waitlist.svg'
            }
            resolve({user, status: WAITLIST, template})
            break
          case 'LOCKED':
            reject({
              code: 400,
              email: user.email,
              msg: 'Your account is locked!',
              msg2: 'Please contact our Customer Service at info@auvenir.com.',
              msg3: 'images/illustrations/lock.svg'
            })
            break
          default:
            reject({
              code: 400,
              msg: 'Account deactivated.',
              msg2: 'Sorry but your account has been deactivated. Please contact support@auvenir.com.'
            })
            break
        }
      })
    }

    function createLoginToken ({user, status, template}) {
      return new Promise(function (resolve, reject) {
        if (status === ACTIVE || status === ONBOARDING) {
          var token = Utility.secureRandomString(64)
          user.auth.access.token = Utility.createHash(token)
          // Extending expiry date of user authentication if local environment for testing convenience
          if (appConfig.env === 'local') {
            var extendedTime = new Date()
            extendedTime.setFullYear(extendedTime.getFullYear() + 1)
            user.auth.access.expires = extendedTime.getTime() + Utility.SESSION_LENGTH
          } else {
            user.auth.access.expires = Date.now() + Utility.SESSION_LENGTH
          }
          user.save((err) => {
            if (err) {
              reject({code: 400, msg: 'Error creating login token'})
            } else {
              resolve({user, status, template, token})
            }
          })
        } else {
          resolve({user, status, template, token})
        }
      })
    }

    function sendEmail ({user, status, template, token}) {
      return new Promise(function (resolve, reject) {
        const {email, firstName, lastName} = user
        const customValues = {
          url: appConfig.publicServer.url,
          token: token,
          email: email
        }
        if (status !== PENDING) {
          Utility.sendEmail({
            fromEmail: 'andi@auvenir.com',
            toEmail: email,
            toName: firstName + ' ' + lastName,
            status,
            customValues
          }, (err) => {
            if (err) {
              req.log.error('ERROR: sending email.', {err: err})
              return reject({code: 400, msg: 'Error sending email'})
            }
            req.log.info('Email Sent Successfully.')
            return reject(template)
          })
        } else {
          return reject(template)
        }
      })
    }
  })

  router.options('/webexists', setAllowOriginHeaders)

  router.get('/webexists', function (req, res) {
    setAllowOriginHeaders(req, res)
    if (!req.query || !_.isString(req.query.email)) return res.status(400).send({code: 400, message: 'Invalid fields'})
    const email = req.query.email.trim().toLowerCase()
    Utility.findUserAccount(email, (err, user) => {
      if (err) return res.status(400).send({code: 400, message: 'Error looking up user'})
      res.status(200).send({code: 200, exists: user != null})
    })
  })

  router.options('/webresetpassword', setAllowOriginHeaders)

  router.post('/webresetpassword', function (req, res) {
    setAllowOriginHeaders(req, res)
    validateFields(req)
      .then(findUser)
      .then(checkUserStatus)
      .then(createLoginToken)
      .then(sendEmail)
      .catch((err) => {
        if (err.code && err.msg) {
          const code = err.code < 200 ? 400 : err.code
          return res.status(code).json({data: {message: err.msg}})
        } else {
          req.log.error(err)
          return res.status(500).json({data: {message: 'There was an issue attempting to reset your password. Please contact support@auvenir.com.'}})
        }
      })

    // Promise Functions
    function validateFields (req) {
      return new Promise(function (resolve, reject) {
        if (!req.body || !req.body.email) {
          return reject({code: 400, msg: 'Invalid Fields'})
        }
        req.body.email = req.body.email.toLowerCase().trim()
        resolve(req.body)
      })
    }

    function findUser ({email}) {
      return new Promise(function (resolve, reject) {
        Utility.findUserAccount(email, (err, user) => {
          if (err || !user) {
            return reject({code: 400, msg: 'There was an issue finding your account'})
          }
          resolve({user})
        })
      })
    }

    function checkUserStatus ({user}) {
      return new Promise(function (resolve, reject) {
        let template
        switch (user.status) {
          case 'ACTIVE':
            template = {
              code: 200,
              msg: 'Check your Email!',
              msg2: `We sent a password reset link to ${user.email}`,
              msg3: 'images/illustrations/login.svg',
              email: user.email,
              firstName: user.firstName,
              lastName: user.lastName
            }
            resolve({user, status: ACTIVE, template})
            break
          case 'ONBOARDING':
            template = {
              code: 200,
              msg: 'Check your Email!',
              msg2: `We sent a password reset link to ${user.email}`,
              msg3: 'images/illustrations/login.svg',
              email: user.email,
              firstName: user.firstName,
              lastName: user.lastName
            }
            resolve({user, status: ONBOARDING, template})
            break
          case 'PENDING':
            reject({
              code: 400,
              email: user.email,
              msg: 'Check your Email!',
              msg2: 'Please login from the email invitation sent by your auditor.  If you did not receive one, please contact your auditor.',
              msg3: 'images/illustrations/waitlist.svg'
            })
            break
          case 'WAIT-LIST':
            reject({
              code: 400,
              email: user.email,
              msg: 'Awaiting approval!',
              msg2: 'We are currently reviewing your credentials and will let you know when you\'ve been approved.',
              msg3: 'images/illustrations/waitlist.svg'
            })
            break
          case 'LOCKED':
            reject({
              code: 400,
              email: user.email,
              msg: 'Your account is locked!',
              msg2: 'Please contact our Customer Service at info@auvenir.com.',
              msg3: 'images/illustrations/lock.svg'
            })
            break
          default:
            reject({
              code: 400,
              msg: 'Account deactivated.',
              msg2: 'Sorry but your account has been deactivated. Please contact support@auvenir.com.'
            })
            break
        }
      })
    }

    function createLoginToken ({user, status, template}) {
      return new Promise(function (resolve, reject) {
        var token = Utility.secureRandomString(64)
        user.auth.access.token = Utility.createHash(token)
        user.auth.access.passwordResetRequired = true
        // Extending expiry date of user authentication if local environment for testing convenience
        if (appConfig.env === 'local') {
          var extendedTime = new Date()
          extendedTime.setFullYear(extendedTime.getFullYear() + 1)
          user.auth.access.expires = extendedTime.getTime() + Utility.SESSION_LENGTH
        } else {
          user.auth.access.expires = Date.now() + Utility.SESSION_LENGTH
        }
        user.save((err) => {
          if (err) {
            reject({code: 400, msg: 'Error creating login token'})
          } else {
            resolve({user, status, template, token})
          }
        })
      })
    }

    function sendEmail ({user, status, template, token}) {
      return new Promise(function (resolve, reject) {
        const {email, firstName, lastName} = user
        const customValues = {
          url: appConfig.publicServer.url,
          token: token,
          email: email
        }
        Utility.sendEmail({
          fromEmail: 'andi@auvenir.com',
          toEmail: email,
          toName: firstName + ' ' + lastName,
          status: 'passwordReset',
          customValues
        }, (err) => {
          if (err) {
            req.log.error('ERROR: sending email.', {err: err})
            return reject({code: 400, msg: 'Error sending email'})
          }
          req.log.info('Email Sent Successfully.')
          return reject(template)
        })
      })
    }
  })

  router.options('/webcontact', setAllowOriginHeaders)

  router.post('/webcontact', function (req, res) {
    setAllowOriginHeaders(req, res)
    validateFields(req)
      .then(sendEmail)
      .catch((err) => {
        if (err.code && err.msg) {
          const code = err.code < 200 ? 400 : err.code
          return res.status(code).json({data: {message: err.msg}})
        } else {
          req.log.error(err)
          return res.status(500).json({data: {message: 'There was an issue attempting to send your message from the Contact page. Please contact us at support@auvenir.com.'}})
        }
      })

    // Promise Functions
    function validateFields (req) {
      return new Promise(function (resolve, reject) {
        const body = req.body
        if (!body) {
          return reject({code: 400, msg: 'Invalid Fields'})
        }
        const customValues = {
          email: __.toSuperTrimmedString(body.email),
          name: __.toSuperTrimmedString(body.name),
          type: __.toSuperTrimmedString(body.type),
          otherType: __.toSuperTrimmedString(body.otherType),
          message: __.toSuperTrimmedString(body.message)
        }
        if (customValues.email === '' || customValues.name === '' || customValues.type === '' || customValues.message === '') {
          return reject({code: 400, msg: 'Invalid Fields'})
        }
        resolve(customValues)
      })
    }

    function sendEmail (customValues) {
      return new Promise(function (resolve, reject) {
        Utility.sendEmail({
          fromEmail: 'andi@auvenir.com',
          toEmail: appConfig.integrations.nodemailer.contactMailbox,
          toName: 'Auvenir',
          status: 'contactPage',
          customValues
        }, (err) => {
          if (err) {
            req.log.error('ERROR: sending email.', {err: err})
            return reject({code: 400, msg: 'Error sending email'})
          }
          req.log.info('Email Sent Successfully.')
          return reject({code: 200, msg: 'Contact data sent successfully'})
        })
      })
    }
  })

  /**
   * Handling sending user message.
   *
   * @param: {function} req: the incoming http request
   * @param: {function} res: the outgoing response.
   */
  router.post('/userMessage', function (req, res) {
    var form = new formidable.IncomingForm()
    form.maxFieldsSize = 10 * 1024 * 1024
    form.keepExtensions = true

    form.parse(req, function (err, fields) {
      if (err) {
        req.log.error('ERROR: error parsing request with form info', {err: err})
        res.json({code: 2, msg: 'Error processing request.'})
        res.end()
        return
      }
      if (!fields) {
        req.log.warn('ERROR: Invalid Fields')
        res.json({code: 1, msg: 'Invalid Fields'})
        res.end()
        return
      }
      if (fields.email === undefined) {
        req.log.warn('ERROR: Email Field not found')
        res.json({code: 2, msg: 'Email Field not found'})
        res.end()
        return
      }
      if (fields.name === undefined) {
        req.log.warn('ERROR: Name Field not found')
        res.json({code: 2, msg: 'Name Field not found'})
        res.end()
        return
      }
      if (fields.message === undefined) {
        req.log.warn('ERROR: Message Field not found')
        res.json({code: 2, msg: 'Message Field not found'})
        res.end()
        return
      }
      if (fields.email) {
        var email = fields.email.toLowerCase()
        var name = fields.name
        var message = fields.message

        sendContactEmail(email, name, message, function (result) {
          if (!result) {
            req.log.warn('ERROR: No result returned from sending contact email')
            res.json({code: 2, msg: 'Message Field not found'})
          }
          if (typeof result.code === 'number') {
            if (result.code === 1) {
              req.log.error('ERROR: Email was not sent successfully. \nError message: ' + result.msg)
              res.json(result)
            } else {
              req.log.info('Email was sent successfully.')
              res.json(result)
            }
          } else {
            req.log.error('ERROR: Unkown result code.')
            res.json({code: 2, msg: 'Unknown result code'})
          }
        })
      }
    })
  })

  router.post('/sendConfirmationEmail', function (req, res) {
    var form = new formidable.IncomingForm()
    form.maxFieldsSize = 10 * 1024 * 1024
    form.keepExtensions = true

    form.parse(req, function (err, fields) {
      if (err) {
        req.log.error('ERROR: parsing req with form info.', {err: err})
        res.json(err)
        res.end()
        return
      }
      if (!fields) {
        req.log.warn('ERROR: Invalid Fields')
        res.json({code: 1, msg: 'Invalid Fields'})
        res.end()
        return
      }
      if (fields.email === undefined) {
        req.log.warn('ERROR: Email Field not found')
        res.json({code: 2, msg: 'Email Field not found'})
        res.end()
        return
      }
      if (!fields.email) {
        req.log.warn('ERROR: Email Field is empty')
        res.json({code: 2, msg: 'Email Field is empty'})
        res.end()
        return
      }
      var email = fields.email.toLowerCase()
      Utility.findUserAccount(email, function (err, user) {
        if (err) {
          req.log.error('ERROR: finding user account from db.', {err: err})
          res.json(err)
          res.end()
          return
        }
        if (!user) {
          res.send({code: 25, msg: 'No user found.'})
          res.end()
        } else {
          Utility.hasApplicationAccess(user.status, function (analysis) {
            if (analysis.code === Utility.USER_STATUS.ACTIVE || analysis.code === Utility.USER_STATUS.ONBOARDING) {
              // If User is authenicated from Auvenir Team, we generate new token.
              var expiryTime = Date.now() + Utility.SESSION_LENGTH
              user.auth.access.token = Utility.secureRandomString(64)
              user.auth.access.expires = expiryTime
              req.log.info('Setup New Temporary Token for User For ' + user.email + '.')
              user.save(function (err, result) {
                if (err) {
                  req.log.error('ERROR: Updating Token for User.', {err: err})
                  res.json(err)
                  return res.end()
                }
                var customValues = {}
                var status = ''
                customValues['url'] = appConfig.publicServer.url

                if (analysis.code === Utility.USER_STATUS.ACTIVE) {
                  status = ACTIVE
                } else {
                  status = ONBOARDING
                }

                customValues.token = result.auth.access.token
                customValues.email = email

                var emailINFO = {
                  fromEmail: 'andi@auvenir.com',
                  toEmail: user.email,
                  toName: user.firstName + ' ' + user.lastName,
                  status: status,
                  customValues: customValues
                }

                // check if the user is registered as an auditor
                if (user.type === 'AUDITOR' || user.type === 'ADMIN') {
                  Utility.sendEmail(emailINFO, function (err, emailResult) {
                    if (err) {
                      req.log.error('ERROR: sending email.', {err: err})
                      res.json(err)
                      res.end()
                    } else {
                      req.log.info('Email sent successfully')
                      // ACTIVE AND ONBOARDING SAME MESSAGE.
                      res.json({
                        code: 200,
                        msg: 'Check your Email!',
                        msg2: 'We sent a login link to ',
                        msg3: 'images/illustrations/login.svg',
                        email: user.email,
                        // profilePicture: user.profilePicture,
                        firstName: user.firstName,
                        lastName: user.lastName
                      })
                      res.end()
                    }
                  })
                } else {
                  req.log.warn('User is not an auditor')
                  // ACTIVE AND ONBOARDING SAME MESSAGE.
                  res.json({
                    code: 206,
                    msg: 'You aren\'t registered',
                    msg2: 'Try logging in as a client',
                    msg3: 'images/illustrations/login.svg'
                  })
                  res.end()
                }
              })
            } else if (analysis.code === Utility.USER_STATUS.WAITLIST || analysis.code === Utility.USER_STATUS.PENDING) {
              req.log.info('User is WAITLIST or PENDING')
              // User's account is on waitlist
              res.json({
                code: 0,
                msg: 'Awaiting approval!',
                msg2: 'We are currently reviewing your credentials and will let you know when you\'ve been approved.',
                msg3: 'images/illustrations/waitlist.svg'
              })
              res.end()
              // Since We've already sent a email that indicating user is on waitlist, we don't need to send it again.
              // But show that we will be in touch soon.
            } else if (analysis.code === Utility.USER_STATUS.LOCKED) {
              req.log.warn('User is locked status and try to login.')
              // User's account is locked
              res.json({
                code: 0,
                msg: 'Your account is locked!',
                msg2: 'Please contact our Customer Service at info@auvenir.com.',
                msg3: 'images/illustrations/lock.svg'
              })
              res.end()
            } else {
              req.log.warn('User is Unknown status')
              res.json(analysis)
              res.end()
            }
          })
        }
      })
    })
  })

  /**
   * TODO - file upload will be fixed when working on file-manager page
   * @param: {function} req: the incoming http request
   * @param: {function} res: the outgoing response.
   */
  router.post('/file-upload', isAuthenticated, function (req, res) {
    const engagementID = Utility.castToObjectId(req.query.eID)
    const requestID = Utility.castToObjectId(req.query.rID)
    const todoID = Utility.castToObjectId(req.query.tID)
    const userID = Utility.castToObjectId(req.user._id)
    const fileName = req.query.fname
    const filePath = req.query.fpath
    const fileSize = req.query.fsize

    if (_.isEmpty(engagementID)) {
      req.log.error('No engagementID was passed.')
      return res.status(500).json({ msg: 'No engagementID passed.' })
    }

    req.log.info(`File is being uploaded on ${engagementID}`)

    const checkAccess = (cbk) => {
      req.log.info(`Access controls check on engagement ${engagementID}`)

      if (todoID && requestID) {
        Access.request(requestID, todoID, engagementID, userID, (err, engagement) => {
          if (err) {
            return cbk(err)
          }
          return cbk(null, { engagement })
        })
      } else { // when only engagementID was passed.
        Access.engagement(engagementID, userID, (err, engagement) => {
          if (err) {
            return cbk(err)
          }
          return cbk(null, { engagement })
        })
      }
    }

    const createFile = (data, cbk) => {
      req.log.info('Create a file object in the database.')

      var file = new Models.Auvenir.File()
      file.name = fileName || 'unknown'
      file.size = fileSize || 'unknown'
      file.path = filePath
      file.status = 'UPLOADING'
      file.owner = userID
      file.save((err) => {
        if (err) {
          cbk(err, { file })
        } else {
          data.file = file
          cbk(null, data)
        }
      })
    }

    const saveFSFileInDB = (data, cbk) => {
      req.log.info('Saving the file in DB.')
      let { engagement, file } = data
      var uploader = new FileUploader({
        filename: (formFields, filename) => {
          debug(`Defining file name. name: ${filename}, formFields: ${JSON.stringify(formFields)}`)
          return fileName // TODO - check what this is doing
        },
        validate: (formFields, filename, mimeType) => {
          debug(`Validating file. mimeType ${mimeType}, name: ${filename}, formFields: ${JSON.stringify(formFields)}`)
          return true // TODO - always..true???
        },
        encrypt: true
      })
      uploader.parse(req, (err, fields, files) => {
        if (err) {
          file.status = 'ERROR'
          file.save((fileError) => {
            if (fileError) {
              req.log.error('ERROR: while saving the file.status.', {fileError})
            } else {
              req.log.info('ERROR: while parsing form. Changing file.status to \'ERROR\'')
            }
            return cbk(err, {file})
          })
        } else {
          cbk(null, engagement, file, files.myFile)
        }
      })
    }

    const modifyFileInfo = (engagement, file, fsFile, cbk) => {
      req.log.info('use the fields uploaded in the form data to update the file data.')
      file.name = fsFile.name
      file.size = fsFile.size.toString()
      file.path = filePath || '/'
      file.bucket = {
        name: 'fs',
        id: fsFile.id
      }

      file.save((err) => {
        if (err) {
          return cbk({ error: err, filename: file.name }, { file })
        }

        Utility.createActivity(userID, engagementID, 'UPLOAD', 'File', {}, file.toObject(), (err) => {
          if (err) {
            req.log.error('Error on creating Activity', {err})
          }
        })
        return cbk(null, engagement, file)
      })
    }

    const addFileID = (engagement, file, cbk) => {
      req.log.info('Update the files array in engagement.')

      const addToEngagement = (cbk) => {
        // save in engagement.files
        engagement.files.push(file._id)
        engagement.save((err) => {
          return cbk(err, { file, engagement })
        })
      }

      const addToRequest = (cbk) => {
        // save in request.currentFiles[]
        engagement.addFileToRequest(requestID, todoID, file._id, (err) => {
          return cbk(err, { file, engagement })
        })
        // engagement.todos.forEach((todo) => {
        //   if (todo.requests.length > 0) {
        //     todo.requests.forEach((request) => {
        //       if (request._id.toString() === requestID.toString()) {
        //         request.previousFiles.push(request.currentfile)
        //         request.currentFile = file._id
        //         engagement.save((err) => {
        //           return cbk(err, { file, engagement })
        //         })
        //       }
        //     })
        //   }
        // })
      }

      if (requestID) {
        async.parallel([addToEngagement, addToRequest], (err, result) => {
          cbk(err, result[1])
        })
      } else {
        addToEngagement(cbk)
      }
    }

    async.waterfall([checkAccess, createFile, saveFSFileInDB, modifyFileInfo, addFileID], (err, result) => {
      const sendErrResponse = () => {
        if (err.filename) {
          return res.status(500).json({msg: 'There was an error while uploading your file: ' + err.filename})
        } else {
          return res.status(500).json({msg: 'There was an error while uploading your file.'})
        }
      }

      let { file, engagement } = result

      if (err) {
        req.log.error('ERROR: File Uploading Error.', { err })

        if (file) {
          file.status = 'ERROR'
          file.save((saveErr) => {
            if (saveErr) {
              req.log.error('ERROR: saving file status to ERROR.', { saveErr })
            }
            sendErrResponse()
          })
        } else {
          sendErrResponse()
        }
      } else {
        req.log.warn(`File ${file.name} Save Successful`)
        file.status = 'ACTIVE'
        file.save((saveErr) => {
          if (saveErr) {
            req.log.error('ERROR: while saving file.status "ACTIVE".', {err: saveErr})
            res.status(500).json({msg: 'There was an error while updating the file status.'})
          } else {
            engagement.fetchAdditionalInfo((fetchErr, updatedEngagement) => {
              if (fetchErr) {
                req.log.error('There was a fetch engagement info error.', {fetchErr})
              }
              return res.status(200).json({file, engagement: updatedEngagement || engagement })
            })
          }
        })
      }
    })
  })

  /**
   *  Upload Firm logo
   *
   * @param: {function} req: the incoming http request
   * @param: {function} res: the outgoing response.
   */
  router.post('/firm-logo-upload', isAuthenticated, function (req, res) {
    var uploader = new FileUploader({
      root: 'firmLogo',
      filename: function (fileInfo, fileName) {
        return fileInfo.firmID + '/' + fileName // filename format firmID/fileName
      },
      validate: function (fileInfo) {
        return fileInfo.firmID !== ''
      },
      encrypt: false
    })

    uploader.parse(req, function (err, fields, files) {
      if (err) {
        req.log.error('ERROR: File Uploading Error', {err: err})
        return res.end(err.message)
      }
      var file = files.file

      done(file)

      function done (file) {
        var firmID = file.path.split('/')[0]
        res.json({ logo: firmID + '/' + file.id.toString()})
      }
    })
  })

  /**
   * Function that sends email to email using name and message.
   *
   * @param: email: String Reciever's Email
   * @param: name: String Reciever's name
   * @param: message: String The content of Email.
   */
  function sendContactEmail (email, name, message, callback) {
    if (appConfig.env === 'local') {
      let fromAddress = 'andi@auvenir.com'
      let toAddress = 'info@auvenir.com'
      let htmlText = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' +
        '<html xmlns="http://www.w3.org/1999/xhtml">' +
        ' ' +
        '<head>' +
        '  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' +
        '  <title>A Simple Responsive HTML Email</title>' +
        '  <style type="text/css">' +
        '  </style>' +
        '</head>' +
        '' +
        '<body yahoo bgcolor="#fff">' +
        '<p>You have received a new message from a user of auvenir.com' +
        '<p>NAME: ' +
        name +
        '</p>' +
        '<p>EMAIL: ' +
        email +
        '</p>' +
        '<p>MESSAGE: ' +
        message +
        '</p>' +
        '</body>' +
        '</html>'

      var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'andi@auvenir.com',
          pass: '(Try2Run2)'
        }
      }, {
        // default values for sendMail method
        from: fromAddress,
        headers: {
          'My-Awesome-Header': '123'
        }
      })
      transporter.sendMail({
        to: toAddress,
        subject: 'New message from auvenir.com',
        html: htmlText
      }, function (err, emailInfo) {
        if (err) {
          // error log will be printed where this function is being used.
          callback({code: 1, msg: err.toString()})
        } else {
          callback({code: 0})
        }
      })
    } else {
      let NO_REPLY_BSL = 'andi@auvenir.com'
      let toAddresses = 'info@auvenir.com'
      let subject = 'New message from user'
      let fromAddresses = [NO_REPLY_BSL]
      let htmlText = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' +
        '<html xmlns="http://www.w3.org/1999/xhtml">' +
        ' ' +
        '<head>' +
        '  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' +
        '  <title>A Simple Responsive HTML Email</title>' +
        '  <style type="text/css">' +
        '  </style>' +
        '</head>' +
        '' +
        '<body yahoo bgcolor="#fff">' +
        '<p>You have received a new message from a user of auvenir.com' +
        '<p>NAME: ' +
        name +
        '</p>' +
        '<p>EMAIL: ' +
        email +
        '</p>' +
        '<p>MESSAGE: ' +
        message +
        '</p>' +
        '</body>' +
        '</html>'

      var params = {
        Destination: {BccAddresses: [], CcAddresses: [], ToAddresses: [toAddresses]},
        Message: {
          Body: {Html: {Data: htmlText, Charset: 'UTF-8'}},
          Subject: {Data: subject, Charset: 'UTF-8'}
        },
        Source: fromAddresses[0],
        ReplyToAddresses: fromAddresses
      }

      ses.sendEmail(params, function (err, data) {
        if (err) {
          callback({code: 1, msg: err.toString()})
        } else {
          callback({code: 0})
        }
      })
    }
  }

  return router
}
