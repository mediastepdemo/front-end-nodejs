const router = require('express').Router()
const Utility = require('../plugins/utilities/utility')
const Code = require('../plugins/utilities/code')
const async = require('async')
const Models = require('../models/index')
const initLogger = require('../plugins/logger/logger').init
const { error, info } = initLogger(__filename)

/**
 * Marketing page will call to this api when its new user is created
 * to make sure Marketing page database and Auvenir platform database synced
 */
module.exports = (passport, config) => {
  const createNewUser = (user, callback) => {
    if (!user.email) {
      return callback({code: Code.api.MISSING_EMAIL, msg: 'Registration email not found'})
    }

    if (!Utility.isEmailValid(user.email)) {
      return callback({code: Code.api.INVALID_EMAIL, msg: 'Email address is invalid'})
    }

    Models.Auvenir.User.findOne({'email': user.email}, (err, foundUser) => {
      if (err) {
        error(err)
        return callback({code: 23, msg: 'Error finding user'})
      }

      if (foundUser) {
        info('User existed. Return user to marketing page to update status')
        return callback(null, foundUser)
      }

      Utility.createUser(user, (err, newUser) => {
        if (err) {
          error(err)
          return callback({code: Code.ERROR_CREATE_USER, msg: 'Error creating new user in database'})
        }

        if (user.firm) {
          user.firm.acl = [{
            id: newUser._id,
            admin: true
          }]
          Utility.createFirm(user.firm, (err, newFirm) => {
            if (err) {
              error(err)
            }
            info('Firm is created for user ' + newUser._id)
          })
        }

        callback(null, newUser)
      })
    })
  }

  router.post('/sync-users', (req, res, next) => {
    let users = req.body.users

    async.map(users, createNewUser, (err, results) => {
      if (err) {
        req.log.error({err})
        res.json(err)
        res.end()
        return
      }

      res.json({code: Code.api.OK, users: results})
      res.end()
    })
  })

  return router
}
