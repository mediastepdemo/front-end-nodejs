var express = require('express')
var Utility = require('../plugins/utilities/utility')
var Code = require('../plugins/utilities/code')
var Models = require('../models')
var IDQ = require('../plugins/idq/idq')
var appConfig = require('../config')
var sockets = require('../sockets')
const async = require('async')
const _ = require('lodash')

var API_SESSION_LENGTH = 60 * 60000

/**
 * Middleware function that can be used with the API router functions.
 * Checks to see if the server request contains a valid dev key.
 * If authenticated, it will trigger the next function and proceed to either further middleware, or the callback function.
 * If failed authentication, we return JSON in the response
 * @memberof MiddlewareFunctions
 * @param {Function} req  - the incoming http request
 * @param {Function} res  - the outgoing response
 * @param {Function} next - the next function if developer is authnticated.
 * @example https://domain.com/api/path?key=32bitalphanumeric
 */
var isDevAuthenticated = function (req, res, next) {
  req.log.info('API: checking if dev is authenticated')
  var devAuthID
  var apiKey
  if (req.method === 'GET') {
    devAuthID = req.query.devAuthID
    apiKey = req.query.apiKey
  } else if (req.method === 'POST') {
    devAuthID = req.body.devAuthID
    apiKey = req.body.apiKey
  }

  if (devAuthID === undefined) {
    req.log.warn('Dev Authentication ID not found')
    res.json({code: Code.api.MISSING_DEV_AUTHID, msg: 'Dev Authentication ID not found'})
    res.end()
    return
  }
  if (apiKey === undefined) {
    req.log.warn('API key not found')
    res.json({code: Code.api.MISSING_DEV_APIKEY, msg: 'API key not found'})
    res.end()
    return
  }

  Models.Auvenir.User.findOne({ 'auth.id': devAuthID }, function (err, oneDev) {
    if (err) {
      req.log.error({err})
      res.json({code: Code.db.ERROR_OP_FIND, msg: 'Error during developer authentication'})
      res.end()
    } else {
      if (oneDev) {
        if (oneDev.auth.developer.apiKey === apiKey) {
          req.log.info('Developer Validated: ' + oneDev.email)
          req.dev = oneDev
          next()
        } else {
          req.log.warn('Developer denied API access')
          res.json({code: Code.api.ACCESS_DENIED, msg: 'Developer denied API access'})
          res.end()
        }
      } else {
        req.log.warn('Developer does not exist')
        res.json({code: Code.db.DNE_USER, msg: 'Developer does not exist'})
        res.end()
      }
    }
  })
}

/**
 * Middleware function that can be used with the API router functions.
 * Check to see if the developer that was authenticated has a valid Authentication ID
 * @memberof MiddlewareFunctions
 * @param {Function} req  - the incoming http request
 * @param {Function} res  - the outgoing response
 * @param {Function} next - the next function if user is authnticated.
 */
var isValidAuthenticationID = function (req, res, next) {
  var userAuthID
  if (req.method === 'GET') {
    userAuthID = req.query.userAuthID
  } else if (req.method === 'POST') {
    userAuthID = req.body.userAuthID
  }

  if (userAuthID === undefined) {
    req.log.warn('User Authentication ID not found')
    res.json({code: Code.api.MISSING_USER_AUTHID, msg: 'User Authentication ID not found'})
    res.end()
    return
  }

  Models.Auvenir.User.findOne({'auth.id': userAuthID}, function (err, oneUser) {
    if (err) {
      req.log.error({err})
      res.json({code: Code.db.ERROR_OP_FIND, msg: 'Error during user authentication'})
      res.end()
    } else {
      if (oneUser) {
        req.log.info('Valid user authentication id')
        next()
      } else {
        req.log.warn('User does not exist')
        res.json({code: Code.db.DNE_USER, msg: 'User does not exist'})
        res.end()
      }
    }
  })
}

/**
 * Middleware function that can be used with the API router functions.
 * Checks to see if the developer has a valid access token and is not expired
 * If expired or invalid, it will return JSON in response
 * @memberof MiddlewareFunctions
 * @param {Function} req  - the incoming http request
 * @param {Function} res  - the outgoing response
 * @param {Function} next - the next function if access token is valid.
 */
var isValidAccessToken = function (req, res, next) {
  req.log.info('API: Verifying user access token')
  var userAuthID
  var accessToken
  var deviceID

  if (req.method === 'GET') {
    userAuthID = req.query.userAuthID
    accessToken = req.query.access_token
    deviceID = req.query.deviceInfo
  } else if (req.method === 'POST') {
    userAuthID = req.body.userAuthID
    accessToken = req.body.access_token
    deviceID = req.body.deviceInfo
  }

  if (accessToken === undefined) {
    req.log.warn('User Access Token not found')
    res.json({code: Code.api.MISSING_ACCESS_TOKEN, msg: 'User Access Token not found'})
    return res.end()
  }

  if (deviceID === undefined) {
    req.log.warn('Device ID not found')
    res.json({code: Code.api.MISSING_DEVICE_INFO, msg: 'Device ID not found'})
    return res.end()
  }

  var query = { 'auth.id': userAuthID, 'auth.devices.uniqueID': deviceID }
  var setJSON = { 'auth.devices.$.expires': Date.now() + API_SESSION_LENGTH }
  var options = { new: true }
  Models.Auvenir.User.findOneAndUpdate(query, setJSON, options, function (err, updatedUser) {
    if (err) {
      req.log.error({err})
      res.json({code: Code.db.ERROR_OP_FIND, msg: 'Error during access token check'})
      res.end()
    } else {
      if (updatedUser) {
        req.log.info('Access token renewed')
        next()
      } else {
        req.log.warn('User does not exist')
        res.json({code: Code.db.DNE_USER, msg: 'User does not exist'})
        res.end()
      }
    }
  })
}

/**
 * This is the API Router
 * @class Router
 * @param {Object} passport - The PassportJS reference.
 * @param {Object} config   - The Auvenir configuration that has been laoded.
 */
var router = express.Router()

module.exports = function (passport, config) {
/**
 * Prints out a mapping of all the current socket connections and their
 * socketID to userID mappings.
 * @name api/v1/connnections
 * @memberof Router
 * @param {String}   path     - the incoming http request
 * @param {Function} callback - the outgoing response.
 */
  router.get('/v1/connections', isDevAuthenticated, function (req, res) {
    if (!sockets.getSockets()) {
      req.log.warn('IO has not been set yet.')
      res.json({ code: Code.core.IO_NOT_SETUP, msg: 'IO has not been set yet.' })
    } else {
      var connections = sockets.getSockets().connected
      var keys = Object.keys(connections)
      var output = []
      for (var i = 0; i < keys.length; i++) {
        if (connections[keys[i]].customData) {
          output.push({ socketID: connections[keys[i]].id, data: connections[keys[i]].customData })
        } else {
          output.push({ socketID: connections[keys[i]].id})
        }
      }
      res.json({ code: Code.api.OK, result: output })
    }
    res.end()
  })

/**
 * Just here to protect the route
 * @example https://domain.com/api
 * @name api/
 * @memberof Router
 * @param {String}   path     - the route used in the get request
 * @param {Function} callback - Handles successfull naviationg of middleware functions.
 */
  router.get('/', isDevAuthenticated, function (req, res) {
    res.json({ code: Code.api.OK, message: 'Will eventually return a list of all versions of our API' })
    res.end()
  })

/**
 * API's GET and POST requests
 * @name api/v1
 * @memberof Router
 * @param {String}   path     - the route used in the get and post request
 * @param {Function} callback - the outgoing response.
 */
  router.get('/v1', isDevAuthenticated, function (req, res) {
    var str = ''
    str += 'GET /v1/connections\r\n'
    str += 'Returns an array of all active connections that map socket IDs to user authentication IDs\r\n\r\n'

    str += 'POST /v1/user/create\r\n'
    str += 'Content-Type: applicaton/x-www-form-urlencoded\r\n'
    str += 'Fields: email, devAuthID, apiKey\r\n'
    str += 'Response: code\r\n\r\n'

    str += 'POST /v1/user/delete\r\n'
    str += 'Content-Type: applicaton/x-www-form-urlencoded\r\n'
    str += 'Fields: email, devAuthID, apiKey\r\n'
    str += 'Response: code\r\n\r\n'
    str += 'Note: Only available in staging and local development environments'

    str += 'POST /v1/register\r\n'
    str += 'Content-Type: applicaton/x-www-form-urlencoded\r\n'
    str += 'Fields: email, devAuthID, apiKey\r\n'
    str += 'Response: userAuthID\r\n\r\n'

    str += 'POST /v1/send-confirmation\r\n'
    str += 'Content-Type: applicaton/x-www-form-urlencoded\r\n'
    str += 'Fields: userAuthID, devAuthID, apiKey, deviceBrand, deviceModel, deviceType, deviceColor, deviceUDID\r\n'
    str += 'Response: code\r\n\r\n'

    str += 'POST /v1/poll-registration\r\n'
    str += 'Content-Type: applicaton/x-www-form-urlencoded\r\n'
    str += 'Fields: userAuthID, devAuthID, apiKey\r\n'
    str += 'Response: confirmationCode\r\n\r\n'

    str += 'POST /v1/register-device\r\n'
    str += 'Content-Type: applicaton/x-www-form-urlencoded\r\n'
    str += 'Fields: userAuthID, devAuthID, apiKey, name, brand, model, type, color, uniqueID, serialNum, os, lastUsed, confirmationCode\r\n'
    str += 'Response: token\r\n\r\n'

    str += 'POST /v1/registration-complete\r\n'
    str += 'Content-Type: applicaton/x-www-form-urlencoded\r\n'
    str += 'Fields: userAuthID, devAuthID, apiKey\r\n'
    str += 'Response: code\r\n\r\n'

    str += 'POST /v1/lock-account\r\n'
    str += 'Content-Type: applicaton/x-www-form-urlencoded\r\n'
    str += 'Fields: userAuthID, devAuthID, apiKey\r\n'
    str += 'Response: success/fail\r\n\r\n'

    str += 'POST /v1/login\r\n'
    str += 'Content-Type: applicaton/x-www-form-urlencoded\r\n'
    str += 'Fields: userAuthID, devAuthID, apiKey\r\n'
    str += 'Response: success/fail\r\n\r\n'

    str += 'GET /v1/user/'
    str += 'GET /v1/engagements'

    str += 'POST /v1/deregister-device\r\n'
    str += 'Content-Type: applicaton/x-www-form-urlencoded\r\n'
    str += 'Fields: userAuthID, devAuthID, apiKey, accessToken, uniqueID\r\n'
    str += 'Response: code\r\n\r\n'

    str += 'POST /v1/check-authentication\r\n'
    str += 'Content-Type: applicaton/x-www-form-urlencoded\r\n'
    str += 'Fields: userAuthID, devAuthID, apiKey, uniqueID, pushMessage\r\n'
    str += 'Response: code\r\n\r\n'

    str += 'POST /v1/deregister-user\r\n'
    str += 'Content-Type: applicaton/x-www-form-urlencoded\r\n'
    str += 'Fields: userAuthID, devAuthID, apiKey\r\n'
    str += 'Response: code\r\n\r\n'

    str += 'GET /v1/check-device/'
    str += 'Fields: userAuthID, devAuthID, apiKey, uniqueID\r\n'
    str += 'Response: code\r\n\r\n'

    res.end(str)
  })

/* POST ROUTES */

/**
 * Handles the process of creating a new user and sets the status as 'ACTIVE'
 * Checks if the users has business. If yes, then it updates the business
 * If not, it creates new business for that user
 * @name api/v1/user/create
 * @memberof Router
 * @param {String}   path                - the incoming http request
 * @param {Function} isDevAuthenticated  - Middleware function call
 * @param {Function} callback            - the outgoing response.
 */
  router.post('/v1/user/create', isDevAuthenticated, function (req, res) {
    var email = req.body.email
    var firstName = (req.body.firstName !== undefined) ? req.body.firstName : null
    var lastName = (req.body.lastName !== undefined) ? req.body.lastName : null

    if (email === undefined) {
      req.log.warn('Registration email not found')
      res.json({code: Code.api.MISSING_EMAIL, msg: 'Registration email not found'})
      res.end()
      return
    }

    if (!Utility.isEmailValid(email)) {
      req.log.warn('Email address is invalid')
      res.json({code: Code.api.INVALID_EMAIL, msg: 'Email address is invalid'})
      res.end()
      return
    }

    Models.Auvenir.User.findOne({ email: email }, function (err, oneUser) {
      if (err) {
        req.log.error({err})
        res.json({ code: Code.db.ERROR_OP_FIND, msg: 'Error detected while trying to find a user.'})
        res.end()
        return
      }

      if (oneUser) {
        req.log.warn('Unable to create user, user already exists with that email.')
        res.json({ code: Code.db.EXISTS_USER, msg: 'User already exists with that email'})
        res.end()
      } else {
        Utility.scrapeUserData({email: email}, function (err, scrapings) {
          if (err) {
            req.log.error('Error occured while scraping user data', {err})
            res.json({ code: Code.core.ERROR_INSPECTOR, msg: 'Error occured while scraping user data'})
            res.end()
            return
          }

          var jsonUser = scrapings.user
          var jsonBusiness = scrapings.business
          Utility.generateUniqueAuthID(function (err, result) {
            if (err) {
              req.log.error('Unable to generate a unique user authentication ID', {err})
              res.json({ code: Code.core.ERROR_GENERATE_AUTHID, msg: 'Unable to genrate a unique user authentication ID'})
              res.end()
              return
            }

            var apiKey = result.apiKey
            jsonUser.auth = { id: result.authID }
            jsonUser.status = 'ACTIVE'

            if (firstName != null) jsonUser.firstName = firstName
            if (lastName != null) jsonUser.lastName = lastName

            var newUser = new Models.Auvenir.User(jsonUser)
            newUser.save(function (err) {
              if (err) {
                req.log.error({err})
                res.json({ code: Code.db.ERROR_OP_SAVE, msg: 'Error creating new user in database'})
                res.end()
              } else {
                var businessName = (scrapings.business) ? scrapings.business.name : null
                Models.Auvenir.Business.findOne({name: businessName}, function (err, oneBusiness) {
                  if (err) {
                    req.log.error({err})
                    res.json({ code: Code.db.ERROR_OP_FIND, msg: 'Error finding new business in database'})
                    res.end()
                  } else {
                    if (oneBusiness) {
                      req.log.info('Add to business step')
                      var query = {_id: oneBusiness._id}
                      Utility.addUserToACL(newUser._id, 'Business', 'member', query, function (err, updatedBusiness) {
                        if (err) {
                          req.log.error({err})
                          res.json({ code: Code.db.ERROR_OP_SAVE, msg: 'Error updating business acl in database'})
                          res.end()
                        } else {
                          if (updatedBusiness) {
                            res.json({ code: Code.api.OK, msg: 'New user created, and Business Updated'})
                            res.end()
                          } else {
                            req.log.error({err})
                            res.json({ code: Code.db.ERROR, msg: 'Missing updated business object'})
                            res.end()
                          }
                        }
                      })
                    } else {
                      req.log.info('Creating Business step')
                      jsonBusiness.acl = [{ role: 'member', id: newUser._id}]
                      Utility.createBusiness(jsonBusiness, function (err, newBusiness) {
                        if (err) {
                          req.log.error({err})
                          res.json({ code: Code.db.ERROR_OP_SAVE, msg: 'Error creating new business in database'})
                          res.end()
                        } else {
                          if (newBusiness) {
                            res.json({ code: Code.api.OK, msg: 'New User created, and new Business created'})
                            res.end()
                          } else {
                            req.log.error({err})
                            res.json({ code: Code.db.ERROR, msg: 'Missing new business object'})
                            res.end()
                          }
                        }
                      })
                    }
                  }
                })
              }
            })
          })
        })
      }
    })
  })

/**
 * Handles the process of deleting the user from database
 * @name api/v1/user/delete
 * @memberof Router
 * @param {String}   path                - the incoming http request
 * @param {Function} isDevAuthenticated  - Middleware function call
 * @param {Function} callback            - the outgoing response.
 */
  router.post('/v1/user/delete', isDevAuthenticated, function (req, res) {
    if (config.env !== config.STAGING && config.env !== config.LOCAL && config.env !== config.QA && config.env !== config.DEV) {
      res.json({code: Code.api.ERROR, msg: 'Invalid environment for API user delete'})
      res.end()
      return
    }

    var email = req.body.email

    if (email === undefined) {
      req.log.warn('Email not found')
      res.json({code: Code.api.MISSING_EMAIL, msg: 'Email not found'})
      res.end()
      return
    }

    if (!Utility.isEmailValid(email)) {
      req.log.warn('Email address is invalid')
      res.json({code: Code.api.INVALID_EMAIL, msg: 'Email address is invalid'})
      res.end()
      return
    }

    Models.Auvenir.User.findOneAndRemove({ email: email }, function (err, result) {
      if (err) {
        req.log.error('Error deleting user', {err})
        res.json({ code: Code.api.ERROR, msg: 'Error deleting user'})
        res.end()
      } else {
        if (result) {
          req.log.info('User ' + email + ' was deleted')
          res.json({ code: Code.api.OK, msg: 'User ' + email + ' was deleted'})
          res.end()
        } else {
          req.log.warn('unable to delete user')
          res.json({ code: Code.api.ERROR, msg: 'unable to delete user'})
          res.end()
        }
      }
    })
  })

/**
 * Handles the process for registering the mobile device for that user and returns the users authID
 * @name api/v1/register
 * @memberof Router
 * @param {String}   path                - the incoming http request
 * @param {Function} isDevAuthenticated  - Middleware function call
 * @param {Function} callback            - the outgoing response.
 */
  router.post('/v1/register', isDevAuthenticated, function (req, res) {
    var email = req.body.email

    if (email === undefined) {
      req.log.warn('Registration email not found')
      res.json({code: Code.api.MISSING_EMAIL, msg: 'Registration email not found'})
      res.end()
      return
    }

    if (!Utility.isEmailValid(email)) {
      req.log.warn('Email address is invalid')
      res.json({code: Code.api.INVALID_EMAIL, msg: 'Email address is invalid'})
      res.end()
      return
    }

    Models.Auvenir.User.findOne({ email: email }, function (err, oneUser) {
      if (err) {
        req.log.error({err})
        res.json({ code: Code.db.ERROR_OP_FIND, msg: 'Error retrieving user from database'})
        res.end()
      } else {
        if (oneUser) {
          if (!(oneUser.status === 'ACTIVE' || oneUser.status === 'ONBOARDING')) {
            req.log.warn('User is unable to register a mobile app at this time. Needs to be active')
            res.json({code: Code.core.INACTIVE_USER, msg: 'User is unable to register a mobile app at this time. Needs to be active'})
            res.end()
            return
          }

          res.json({code: Code.api.OK, authID: oneUser.auth.id})
          res.end()
        } else {
          req.log.warn('User does not exist')
          res.json({code: Code.db.DNE_USER, msg: 'User does not exist'})
          res.end()
        }
      }
    })
  })

/**
 * Handles the process of confirming the device of the particular user and updates the device information
 * Send the request for registration confirmation of a new device.
 * @name api/v1/send-confirmation
 * @memberof Router
 * @param {String}   path                    - the incoming http request
 * @param {Function} isDevAuthenticated      - Middleware function call
 * @param {Function} isValidAuthenticationID - Middleware function call
 * @param {Function} callback                - the outgoing response
 */
  router.post('/v1/send-confirmation', isDevAuthenticated, isValidAuthenticationID, function (req, res) {
    var userAuthID = req.body.userAuthID
    var brand = req.body.deviceBrand
    var model = req.body.deviceModel
    var type = req.body.deviceType
    var color = req.body.deviceColor
    var uniqueID = req.body.deviceInfo
    var customName = req.body.deviceCustomName

    if (brand === undefined) {
      req.log.warn('Device Brand needs to be provided')
      res.json({code: Code.api.MISSING_DEVICE_INFO, msg: 'Device Brand needs to be provided'})
      res.end()
      return
    }
    if (model === undefined) {
      req.log.warn('Device Model Number needs to be provided')
      res.json({code: Code.api.MISSING_DEVICE_INFO, msg: 'Device Model Number needs to be provided'})
      res.end()
      return
    }
    if (type === undefined) {
      req.log.warn('Device Type needs to be provided')
      res.json({code: Code.api.MISSING_DEVICE_INFO, msg: 'Device Type needs to be provided'})
      res.end()
      return
    }
    if (uniqueID === undefined) {
      req.log.warn('Device UDID needs to be provided')
      res.json({code: Code.api.MISSING_DEVICE_INFO, msg: 'Device UDID needs to be provided'})
      res.end()
      return
    }
    if (customName === undefined) {
      req.log.warn('Device Custom Name needs to be provided')
      res.json({code: Code.api.MISSING_DEVICE_INFO, msg: 'Device Custom Name needs to be provided'})
      res.end()
      return
    }

    Models.Auvenir.User.findOne({ 'auth.id': userAuthID }, function (err, oneUser) {
      if (err) {
        req.log.error({err})
        res.json({ code: Code.db.ERROR_OP_FIND, msg: 'Error retrieving user from database'})
        res.end()
      } else {
        if (oneUser) {
          if (!(oneUser.status === 'ACTIVE' || oneUser.status === 'ONBOARDING')) {
            req.log.warn('User is unable to register a mobile app at this time. Needs to be active')
            res.json({code: Code.core.INACTIVE_USER, msg: 'User is unable to register a mobile app at this time. Needs to be active'})
            res.end()
            return
          }
          var devices = oneUser.auth.devices
          if (devices.indexOf(uniqueID) !== -1) {
            req.log.warn('User has already register this uniqueID with this user.')
            res.json({code: Code.api.DEVICE_ALREADY_REGISTERED, msg: 'User has already register this udid with this user.'})
            res.end()
            return
          }

          var connections = sockets.getSockets().connected
          var keys = Object.keys(connections)

          var socketID = null
          for (var i = 0; i < keys.length; i++) {
            if (connections[keys[i]].customData && connections[keys[i]].customData.authID === oneUser.auth.id) {
              socketID = connections[keys[i]].id
              break
            }
          }

          if (socketID == null) {
            req.log.warn('Unable to find an active socket connection for that user')
            res.json({code: Code.core.NO_SOCKET_CONNECTION, msg: 'Unable to find an active socket connection for that user'})
            res.end()
          } else {
            req.log.info('Socket ID found: ' + socketID)

            var deviceData = {
              type: 'MOBILE',
              uniqueID: uniqueID,
              name: customName,
              brand: brand,
              model: model,
              modelType: type,
              color: color
            }

            connections[keys[i]].customData.pairConfirmed = -1 // REG_PENDING
            sockets.getSockets().to(socketID).emit('event-response', {name: 'notification-mobile-registration', code: 0, result: { authID: oneUser.auth.id, device: deviceData, msg: 'Notification Mobile Registration' }})
            res.json({code: Code.api.OK, msg: 'Sent a request for registration confirmation'})
            res.end()
          }
        } else {
          req.log.warn('User does not exist')
          res.json({code: Code.db.DNE_USER, msg: 'User does not exist'})
          res.end()
        }
      }
    })
  })

/**
 * Handles the process of registration. Checks if the user has accepted the registration or rejected it.
 * If accepted, then it returns the confirmation code for registration.
 * @name api/v1/poll-registration
 * @memberof Router
 * @param {String}   path                     - the incoming http request
 * @param {Function} isDevAuthenticated       - Middleware function call
 * @param {Function} isValidAuthenticationID  - Middleware function call
 * @param {Function} callback                 - the outgoing response.
 */
  router.post('/v1/poll-registration', isDevAuthenticated, isValidAuthenticationID, function (req, res) {
    var userAuthID = req.body.userAuthID

    var connections = sockets.getSockets().connected
    var keys = Object.keys(connections)

    var socketID = null
    var pairConfirmed = null

    for (var i = 0; i < keys.length; i++) {
      if (connections[keys[i]].customData && connections[keys[i]].customData.authID === userAuthID) {
        socketID = connections[keys[i]].id
        pairConfirmed = connections[keys[i]].customData.pairConfirmed
        break
      }
    }

    if (socketID == null) {
      req.log.warn('No active socket to poll for that user authentication ID')
      res.json({code: Code.core.NO_SOCKET_CONNECTION, msg: 'No active socket to poll for that user authentication ID'})
      res.end()
      return
    }

    if (pairConfirmed === undefined || pairConfirmed === null) {
      req.log.warn('Confirmation Request has never been sent')
      res.json({code: Code.api.ERROR, msg: 'Confirmation Request has never been sent'})
      res.end()
    } else {
      var REG_PENDING = -1
      var REG_CONFIRMED = 0
      var REG_REJECTED = 1
      var REG_USER_DNE = 2
      var REG_USER_DB_ERROR = 3
      var REG_MISSING_CONFIRMATION = 4
      var REG_MISSING_AUTH_ID = 5
      var REG_AUTH_VALID = 6

      switch (pairConfirmed) {
        case REG_PENDING:
          req.log.info('Registration pending')
          res.json({code: Code.api.REGISTRATION_PENDING, msg: 'Still waiting for registration notification response'})
          break

        case REG_CONFIRMED:
          req.log.info('Registration confirmed')
          var confirmationCode = Utility.randomString(8, Utility.ALPHANUMERIC) + '-' + Utility.randomString(8, Utility.ALPHANUMERIC)
          connections[keys[i]].customData.confirmationCode = confirmationCode
          res.json({code: Code.api.REGISTRATION_APPROVED, msg: 'Registration confirmed', confirmationCode: confirmationCode})
          break

        case REG_REJECTED:
          req.log.info('Registration Rejected')
          res.json({code: Code.api.REGISTRATION_REJECTED, msg: 'Registration Rejected'})
          break

        default:
          req.log.warn('Error occured during registration. Code: ' + pairConfirmed)
          res.json({code: Code.api.REGISTRATION_ERROR, msg: 'Error occured during registration'})
      }
      res.end()
    }
  })

/**
 * Handles the process of registering the device finally. Checks if the User has IDQ ID
 * If yes, it will add the device in IDQ device database and returns the token for login.
 * If no IDQ ID, then it will create user's idq id, add the device to idq device database and returns the token.
 * Currently, only uniqueID and brand (Apple, Samsung and etc.) are mandatory to register a device.
 * @name api/v1/register-device
 * @memberof Router
 * @param {String}   path                    - the incoming http request
 * @param {Function} isDevAuthenticated      - Middleware function call
 * @param {Function} isValidAuthenticationID - Middleware function call
 * @param {Function} callback                - the outgoing response
 */
  router.post('/v1/register-device', isDevAuthenticated, isValidAuthenticationID, function (req, res) {
    var uniqueID = req.body.deviceInfo
    var confirmCode = req.body.confirmationCode
    var name = req.body.deviceCustomName
    var brand = req.body.deviceBrand
    var model = req.body.deviceModel
    var type = req.body.deviceType
    var os = req.body.deviceOS
    var color = req.body.deviceColor
    var serialNum = req.body.deviceSerialNum

    if (uniqueID === undefined || uniqueID == null) {
      req.log.warn('Device UniqueID not found')
      sockets.getSockets().to(socketID).emit('mobile-registration-response', {code: 1, msg: 'Device UniqueID not found' })
      res.json({code: Code.api.MISSING_DEVICE_INFO, msg: 'Device UniqueID not found'})
      res.end()
      return
    }
    if (typeof uniqueID !== 'string' || uniqueID === '') {
      req.log.warn('Device UniqueID is invalid')
      sockets.getSockets().to(socketID).emit('mobile-registration-response', {code: 1, msg: 'Device UniqueID is invalid' })
      res.json({code: Code.api.INVALID_DEVICE_INFO, msg: 'Device UniqueID is invalid'})
      res.end()
      return
    }

    if (brand === undefined || brand === null) {
      req.log.warn('Device Brand not found')
      sockets.getSockets().to(socketID).emit('mobile-registration-response', {code: 1, msg: 'Device Brand not found' })
      res.json({code: Code.api.MISSING_DEVICE_INFO, msg: 'Device Brand not found'})
      res.end()
      return
    } else if (typeof brand !== 'string' || brand === '') {
      req.log.warn('Device Brand is invalid')
      sockets.getSockets().to(socketID).emit('mobile-registration-response', {code: 1, msg: 'Device Brand is invalid' })
      res.json({code: Code.api.INVALID_DEVICE_INFO, msg: 'Device Brand is invalid'})
      res.end()
      return
    }

    if (confirmCode === undefined) {
      req.log.warn('Registration Confirmation Code not found')
      sockets.getSockets().to(socketID).emit('mobile-registration-response', {code: 1, msg: 'Registration Confirmation Code not found' })
      res.json({code: Code.api.MISSING_CONFIRMATION_CODE, msg: 'Registration Confirmation Code not found'})
      res.end()
      return
    }

    var userAuthID = req.body.userAuthID
    var connections = sockets.getSockets().connected
    var keys = Object.keys(connections)

    var socketID = null
    var confirmationCode = null
    for (var i = 0; i < keys.length; i++) {
      if (connections[keys[i]].customData && connections[keys[i]].customData.authID === userAuthID) {
        socketID = connections[keys[i]].id
        confirmationCode = connections[keys[i]].customData.confirmationCode
        break
      }
    }

    if (socketID == null) {
      req.log.warn('No active socket connection')
      sockets.getSockets().to(socketID).emit('mobile-registration-response', {code: 1, msg: 'No active socket connection' })
      res.json({code: Code.core.NO_SOCKET_CONNECTION, msg: 'No active socket connection'})
      res.end()
      return
    }

    if (confirmationCode !== confirmCode) {
      req.log.warn('Confirmation code is invalid')
      sockets.getSockets().to(socketID).emit('mobile-registration-response', {code: 1, msg: 'Confirmation code is invalid' })
      res.json({code: Code.api.INVALID_CONFIRMATION_CODE, msg: 'Confirmation code is invalid'})
      res.end()
      return
    }

    Models.Auvenir.User.findOne({ 'auth.id': userAuthID }, function (err, oneUser) {
      if (err) {
        req.log.error('Error retrieving user from database', {err})
        sockets.getSockets().to(socketID).emit('mobile-registration-response', {code: 1, msg: 'Error retrieving user from database' })
        res.json({code: Code.db.ERROR_OP_FIND, msg: 'Error retrieving user from database'})
        res.end()
        return
      }

      if (!oneUser) {
        req.log.warn('Unable to find user for user authentication ID')
        sockets.getSockets().to(socketID).emit('mobile-registration-response', {code: 1, msg: 'Unable to find user for user authentication ID' })
        res.json({code: Code.db.DNE_USER, msg: 'Unable to find user for user authentication ID'})
        res.end()
        return
      } else {
        req.log.info('User found: ' + oneUser.email)
        if (!oneUser.auth.mobile) {
          req.log.warn('No auth.mobile.id available on database.')
          sockets.getSockets().to(socketID).emit('mobile-registration-response', {code: 1, msg: 'Not a valid database structure' })
          res.end()
          return
        }
        if (oneUser.auth.mobile.id) {
          req.log.info('User has an Mobile ID')

          var devices = oneUser.auth.devices
          for (var i = 0; i < devices.length; i++) {
            var device = devices[i]
            if (device.uniqueID === uniqueID) {
              req.log.warn('Device is already registered for that user')
              sockets.getSockets().to(socketID).emit('mobile-registration-response', {code: 2, msg: 'Device is already registered for that user' })
              res.json({code: Code.api.DEVICE_ALREADY_REGISTERED, msg: 'Device is already registered for that user'})
              res.end()
              return
            }
          }

          IDQ.createInvitationToken(oneUser.auth.mobile.id, function (err, invResult) {
            if (err) {
              req.log.error({err})
              sockets.getSockets().to(socketID).emit('mobile-registration-response', {code: 1, msg: err })
              res.json(err)
              res.end()
              return
            } else {
              var newDevice = {
                type: 'MOBILE',
                name: name,
                brand: brand,
                model: model,
                deviceType: type,
                color: color,
                serialNum: serialNum,
                os: os,
                uniqueID: uniqueID
              }

              Models.Auvenir.User.findOneAndUpdate({'auth.id': userAuthID, 'auth.mobile.id': oneUser.auth.mobile.id}, {$push: {'auth.devices': newDevice}, $set: {status: 'ACTIVE'}}, {new: true}, function (err, updatedUser) {
                if (err) {
                  req.log.error('Error mapping IDQ to Auvenir', {err})
                  sockets.getSockets().to(socketID).emit('mobile-registration-response', {code: 1, msg: 'Error mapping IDQ to Auvenir' })
                  res.json({code: Code.db.ERROR_OP_UPDATE, msg: 'Error mapping IDQ to Auvenir'})
                  res.end()
                  return
                } else {
                  if (updatedUser) {
                    req.log.info('IDQ - Auvenir mapping complete. user: ' + updatedUser.email + ' mobileID: ' + oneUser.auth.mobile.id)
                    sockets.getSockets().to(socketID).emit('mobile-registration-response', {code: 0, token: invResult.token, device: newDevice })
                    res.json({code: Code.api.OK, token: invResult.token})
                    res.end()
                    return
                  } else {
                    req.log.warn('Unable to find a user to complete mapping.')
                    sockets.getSockets().to(socketID).emit('mobile-registration-response', {code: 1, msg: 'Unable to find a user to complete mapping' })
                    res.json({code: Code.db.DNE_USER, msg: 'Unable to find a user to complete mapping.'})
                    res.end()
                    return
                  }
                }
              })
            }
          })
        } else {
          var userData = {
            email: oneUser.email,
            firstName: oneUser.firstName,
            lastName: oneUser.lastName,
            maxDevices: 3
          }
          IDQ.createUser(userData, function (err, userResult) {
            if (err) {
              req.log.error({err})
              sockets.getSockets().to(socketID).emit('mobile-registration-response', {code: 1, msg: err })
              res.json(err)
              res.end()
              return
            } else {
              IDQ.createInvitationToken(userResult.idqID, function (err, invResult) {
                if (err) {
                  req.log.error({err})
                  sockets.getSockets().to(socketID).emit('mobile-registration-response', {code: 1, msg: err })
                  res.json(err)
                  res.end()
                  return
                } else {
                  var newDevice = {
                    type: 'MOBILE',
                    name: name,
                    brand: brand,
                    model: model,
                    modelType: type,
                    color: color,
                    serialNum: serialNum,
                    os: os,
                    uniqueID: uniqueID
                  }

                  Models.Auvenir.User.findOneAndUpdate({'auth.id': userAuthID}, {$set: {'auth.mobile.id': userResult.idqID, 'status': 'ACTIVE'}, $push: {'auth.devices': newDevice}}, {new: true}, function (err, updatedUser) {
                    if (err) {
                      req.log.error('Error mapping IDQ to Auvenir', {err})
                      sockets.getSockets().to(socketID).emit('mobile-registration-response', {code: 1, msg: 'Error mapping IDQ to Auvenir' })
                      res.json({code: Code.db.ERROR_OP_UPDATE, msg: 'Error mapping IDQ to Auvenir'})
                      res.end()
                      return
                    } else {
                      if (updatedUser) {
                        req.log.info('IDQ - Auvenir mapping complete. user: ' + updatedUser.email + ' idq: ' + userResult.idqID)
                        sockets.getSockets().to(socketID).emit('mobile-registration-response', {code: 0, token: invResult.token})
                        res.json({code: Code.api.OK, token: invResult.token})
                        res.end()
                        return
                      } else {
                        req.log.warn('Unable to find a user to complete mapping.')
                        sockets.getSockets().to(socketID).emit('mobile-registration-response', {code: 1, msg: 'Unable to find a user to complete mapping' })
                        res.json({code: Code.db.DNE_USER, msg: 'Unable to find a user to complete mapping.'})
                        res.end()
                        return
                      }
                    }
                  })
                }
              })
            }
          })
        }
      }
    })
  })

/**
 * Handles the process of check-authentication to see if the access token is assigned.
 * @name api/v1/check-authentication
 * @memberof Router
 * @param {String}   path                    - the incoming http request
 * @param {Function} isDevAuthenticated      - Middleware function call
 * @param {Function} isValidAuthenticationID - Middleware function call
 * @param {Function} callback                - the outgoing response
 */
  router.post('/v1/check-authentication', isDevAuthenticated, isValidAuthenticationID, function (req, res) {
    var authID = req.body.userAuthID
    var uniqueID = req.body.deviceInfo
    var pushMessage = req.body.pushMessage

    if (uniqueID === undefined || uniqueID === null) {
      req.log.warn('Device Info not found or undefined')
      res.json({code: Code.api.MISSING_DEVICE_INFO, msg: 'Device Info not found or undefined'})
      res.end()
      return
    }

    if (pushMessage === undefined || pushMessage === null) {
      req.log.warn('Push Message Token was not found or undefined')
      res.json({code: Code.api.IDQ_PUSHMESSAGE_TOKEN, msg: 'Push Message Token was not found or undefined'})
      res.end()
      return
    }

    var counter = 0
    var INTERVAL = 500
    var TIMEOUT = 10000
    poll()

    function poll () {
      Models.Auvenir.User.findOne({'auth.id': authID}, function (err, oneUser) {
        if (err) {
          return pollCallback({code: 1, msg: 'Poll Error'})
        }
        if (!oneUser) {
          pollCallback({code: 1, msg: 'USER DNE'})
        } else {
          if (oneUser.auth.mobile && oneUser.auth.mobile.flag) {
            req.log.info('Flag was set')
            pollCallback(null, oneUser)
          } else {
            if (counter > TIMEOUT) {
              pollCallback({ code: 333, msg: 'Timed Out or Done Polling'})
            } else {
              counter += INTERVAL
              setTimeout(poll, INTERVAL)
            }
          }
        }
      })
    }

    function pollCallback (err, oneUser) {
      if (err) {
        req.log.error({err})
        res.json(err)
        res.end()
      } else {
        if (oneUser.auth.mobile && oneUser.auth.mobile.id) {
          if (!oneUser.auth.mobile.id) {
            req.log.warn('IDQ ID does not exist')
            res.json({code: Code.db.DNE_IDQID, msg: 'IDQ ID does not exist'})
            res.end()
            return
          }

          if (oneUser.auth.mobile.push.token !== pushMessage) {
            req.log.warn(' PushMessage Token DOES NOT match with Database ' + pushMessage)
            res.json({code: Code.api.IDQ_PUSHMESSAGE_TOKEN, msg: 'PushMessage Token DOES NOT match with Database'})
            res.end()
            return
          } else {
            var devices = oneUser.auth.devices
            var isFound = -1
            for (var i = 0; i < devices.length; i++) {
              var device = devices[i]
              if (device.uniqueID === uniqueID) {
                req.log.info('Device ' + uniqueID + ' was registered in Auvenir Database.')
                isFound = i
                break
              }
            }

            if (isFound !== -1) {
              var token = Utility.secureRandomString(64)
              oneUser.auth.devices[isFound].access.token = Utility.createHash(token)
              oneUser.auth.devices[isFound].access.expires = Date.now() + API_SESSION_LENGTH
              oneUser.auth.mobile.push.token = null
              oneUser.auth.mobile.flag = false
              oneUser.save(function (err) {
                if (err) {
                  req.log.error('Error occured while assigning api access token', {err})
                  res.json({ code: Code.db.ERROR_OP_SAVE, msg: 'Error occured while assigning access token'})
                  res.end()
                } else {
                  res.json({ code: Code.api.OK, access_token: token})
                  res.end()
                }
              })
            } else {
              req.log.warn('Device was not found')
              res.json({code: Code.db.DNE_DEVICE, msg: 'No Device was found'})
              res.end()
            }
          }
        } else {
          req.log.warn('No IDQ for these userId registered')
          res.json({code: Code.db.MISSING_IDQID, msg: 'No IDQ for these userId registered'})
          res.end()
        }
      }
    }
  })

/**
 * Handles the process of completing the registration by assigning the dev access token to user.
 * @name api/v1/registration-complete
 * @memberof Router
 * @param {String}   path                    - the incoming http request
 * @param {Function} isDevAuthenticated      - Middleware function call
 * @param {Function} isValidAuthenticationID - Middleware function call
 * @param {Function} callback                - the outgoing response
 */
  router.post('/v1/registration-complete', isDevAuthenticated, isValidAuthenticationID, function (req, res) {
    var authID = req.body.userAuthID
    var uniqueID = req.body.deviceInfo

    Models.Auvenir.User.findOne({ 'auth.id': authID}, function (err, oneUser) {
      if (err) {
        req.log.error({err})
        res.json({ code: Code.db.ERROR_OP_FIND, msg: 'Error retrieving user from database'})
        res.end()
      } else {
        if (oneUser) {
          if (oneUser.auth.mobile && oneUser.auth.mobile.id) {
            // TODO - This is hacky for determining user registration
            IDQ.getDevices(oneUser.auth.mobile.id, function (err, result) {
              if (err) {
                req.log.error({err})
                res.json(err)
                res.end()
              } else {
                if (typeof result === 'string') {
                  result = JSON.parse(result)
                }

                if (result.length > 0) {
                  req.log.info('User registration completion detected (' + uniqueID + ')')
                  var isFound = -1
                  var devices = oneUser.auth.devices

                  for (var i = 0; i < devices.length; i++) {
                    if (devices[i].uniqueID === uniqueID) {
                      isFound = i
                      break
                    }
                  }

                  if (isFound > -1) {
                    var token = Utility.secureRandomString(64)
                    oneUser.auth.devices[isFound].access.token = Utility.createHash(token)
                    oneUser.auth.devices[isFound].access.expires = Date.now() + API_SESSION_LENGTH
                    oneUser.save(function (err) {
                      if (err) {
                        req.log.warn('Error occured while assigning api access token')
                        res.json({ code: Code.db.ERROR_OP_SAVE, msg: 'Error occured while assigning access token'})
                        res.end()
                      } else {
                        res.json({ code: Code.api.OK, access_token: token})
                        res.end()
                      }
                    })
                  } else {
                    req.log.warn('Device was not found')
                    res.json({code: Code.db.DNE_DEVICE, msg: 'No Device was found'})
                    res.end()
                  }
                } else {
                  req.log.warn('Device registration was never completed')
                  res.json({ code: Code.api.ERROR, msg: 'Device registration was never completed'})
                  res.end()
                }
              }
            })
          } else {
            req.log.warn('User is missing an security ID')
            res.json({code: Code.db.MISSING_IDQID, msg: 'User is missing a security ID'})
            res.end()
          }
        } else {
          req.log.warn('User does not exist')
          res.json({code: Code.db.DNE_USER, msg: 'User does not exist'})
          res.end()
        }
      }
    })
  })

/**
 * Handles the process of locking the user account.
 * @name api/v1/lock-account
 * @memberof Router
 * @param {String}   path                    - the incoming http request
 * @param {Function} isDevAuthenticated      - Middleware function call
 * @param {Function} isValidAuthenticationID - Middleware function call
 * @param {Function} callback                - the outgoing response
 */
  router.post('/v1/lock-account', isDevAuthenticated, isValidAuthenticationID, function (req, res) {
    var authID = req.body.userAuthID

    Models.Auvenir.User.findOneAndUpdate({ 'auth.id': authID}, {$set: { status: 'LOCKED'}}, {new: true}, function (err, updatedUser) {
      if (err) {
        req.log.error({err})
        res.json({ code: Code.db.ERROR_OP_UPDATE, msg: 'Error updating user account'})
        res.end()
      } else {
        req.log.info('Account ' + updatedUser.email + ' has been locked')
        res.json({ code: Code.api.OK, msg: 'Account Locked' })
        res.end()
      }
    })
  })

/**
 * Handles the process of returning the login link by assigning auth local token to user
 * @name api/v1/login-link
 * @memberof Router
 *  @param {String}   path                    - the incoming http request
 * @param {Function} isDevAuthenticated      - Middleware function call
 * @param {Function} isValidAuthenticationID - Middleware function call
 * @param {Function} callback                - the outgoing response
 */
  router.post('/v1/login-link', isDevAuthenticated, isValidAuthenticationID, function (req, res) {
    if (config.env !== config.STAGING && config.env !== config.LOCAL && config.env !== config.QA && config.env !== config.DEV) {
      req.log.warn('Invalid environment for API user delete')
      res.json({code: Code.api.ERROR, msg: 'Invalid environment for API user delete'})
      res.end()
      return
    }

    var authID = req.body.userAuthID

    Models.Auvenir.User.findOne({'auth.id': authID}, function (err, oneUser) {
      if (err) {
        req.log.error({err})
        res.json({ code: Code.db.ERROR_OP_FIND, msg: 'Error retrieving user from database'})
        res.end()
      } else {
        if (!oneUser) {
          req.log.warn('User does not exist')
          res.json({code: Code.db.DNE_USER, msg: 'User does not exist'})
          res.end()
        } else {
          if (oneUser) {
            var token = Utility.secureRandomString(64)
            oneUser.auth.access = {
              token: Utility.createHash(token),
              expires: Date.now() + API_SESSION_LENGTH
            }
            oneUser.save(function (err) {
              if (err) {
                req.log.error('Error occured while assigning api local token', {err})
                res.json({ code: Code.db.ERROR_OP_SAVE, msg: 'Error occured while assigning local token'})
                res.end()
              } else {
                var protocol = config.server.protocol
                var domain = config.server.domain
                var url = `${protocol}://${domain}/checkToken?token=${token}&email=${oneUser.email}`

                res.json({ code: Code.api.OK, link: url})
                res.end()
              }
            })
          } else {
            req.log.warn('Unable to find user')
            res.json({code: Code.api.DNE_USER, msg: 'Unable to find user'})
            res.end()
          }
        }
      }
    })
  })

/**
 * Will retrieve a user token assuming that they have a valid dev token and have submitted
 * an email address that is in the system as a user.
 * @name api/v1/login
 * @memberof Router
 * @param {String}   path                    - the incoming http request
 * @param {Function} isDevAuthenticated      - Middleware function call
 * @param {Function} isValidAuthenticationID - Middleware function call
 * @param {Function} callback                - the outgoing response
 * TODO: Still need user authentication component.
 */
  router.post('/v1/login', isDevAuthenticated, isValidAuthenticationID, function (req, res) {
    var authID = req.body.userAuthID
    var uniqueID = req.body.uniqueID

    Models.Auvenir.User.findOne({'auth.id': authID}, function (err, oneUser) {
      if (err) {
        req.log.error({err})
        res.json({ code: Code.db.ERROR_OP_FIND, msg: 'Error retrieving user from database'})
        res.end()
      } else {
        if (!oneUser) {
          req.log.warn('User does not exist')
          res.json({code: Code.db.DNE_USER, msg: 'User does not exist'})
          res.end()
        } else {
          if (oneUser.auth.mobile !== undefined && oneUser.auth.mobile.id !== undefined && user.auth.devices !== undefined && user.auth.devices.length > 0) {
            Utility.idqVerification(oneUser, function (err, result) {
              if (err) {
                req.log.error({err})
                res.json(err)
                res.end()
              } else {
                res.json({ code: Code.api.OK })
                res.end()
              }
            })
          } else {
            req.log.warn('Unable to login via API without IDQ Registration')
            res.json({code: Code.api.MISSING_IDQID, msg: 'Unable to login via API without IDQ Registration'})
            res.end()
          }
        }
      }
    })
  })

/**
 * Sends back the user and business information in a JSON object after
 * validating the dev and the access to the user account.
 * @name api/v1/user
 * @memberof Router
 * @param {String}   path                    - the incoming http request
 * @param {Function} isDevAuthenticated      - Middleware function call
 * @param {Function} isValidAuthenticationID - Middleware function call
 * @param {Function} isValidAccessToken      - Middleware function call
 * @param {Function} callback                - the outgoing response
 */
  router.get('/v1/user', isDevAuthenticated, isValidAuthenticationID, isValidAccessToken, function (req, res) {
    var userAuthID = req.query.userAuthID
    Models.Auvenir.User.findOne({'auth.id': userAuthID}, function (err, oneUser) {
      if (err) {
        req.log.error({err})
        res.json({code: Code.db.ERROR_OP_FIND, msg: 'Error detected during user retrieval'})
        res.end()
        return
      }

      if (!oneUser) {
        req.log.warn('No user with that email')
        res.json({code: Code.db.DNE_USER, msg: 'No user with that email'})
        res.end()
      } else {
        req.log.info('User retrieved.')
        Models.Auvenir.Business.findOne({acl: {$elemMatch: { id: oneUser._id }}}, function (err, oneBusiness) {
          if (err) {
            req.log.error({err})
            res.json({code: Code.db.ERROR_OP_FIND, msg: 'Error detected during business retrieval'})
            res.end()
          }

          var jsonData = {
            code: Code.api.OK,
            status: false,   // status determines whether the engagement status is displayed and whether engagement polling is done.
            user: {
              firstName: oneUser.firstName,
              lastName: oneUser.lastName,
              email: oneUser.email,
              jobTitle: oneUser.jobTitle,
              phone: oneUser.phone
              // profilePicture: oneUser.profilePicture
            }
          }

          if (oneBusiness) {
            jsonData.business = {
              name: oneBusiness.name,
              logo: oneBusiness.logo,
              phone: oneBusiness.phone,
              addresses: oneBusiness.addresses,
              websiteURL: oneBusiness.websiteURL
            }
          }

          res.json(jsonData)
          res.end()
        })
      }
    })
  })

/**
 * Handles the process of retriving engagements whenever the user login
 * @name api/v1/engagements
 * @memberof Router
 * @param {String}   path                    - the incoming http request
 * @param {Function} isDevAuthenticated      - Middleware function call
 * @param {Function} isValidAuthenticationID - Middleware function call
 * @param {Function} isValidAccessToken      - Middleware function call
 * @param {Function} callback                - the outgoing response
 */
  router.get('/v1/engagements', isDevAuthenticated, isValidAuthenticationID, isValidAccessToken, function (req, res) {
    var userAuthID = req.query.userAuthID

    Models.Auvenir.User.findOne({'auth.id': userAuthID}, function (err, oneUser) {
      if (err) {
        req.log.error({err})
        res.json({code: Code.db.ERROR_OP_FIND, msg: 'Error detected during user retrieval'})
        res.end()
        return
      }

      if (!oneUser) {
        req.log.warn('User does not exist')
        res.json({code: Code.db.DNE_USER, msg: 'User does not exist'})
        res.end()
      } else {
        var engs = [
          {
            'name': 'Engagement Name 1',
            'label': 'Action Required',
            'image': 'https://staging-1337.auvenir.com/img/illustrations/auditStatus/mobile-1-1x.png',
            'Text': 'You are now ready to complete, sign and upload your Audit Opinion from the desktop'
          },
          {
            'name': 'Engagement Name 2',
            'label': 'In Progress',
            'image': 'https://staging-1337.auvenir.com/img/illustrations/auditStatus/mobile-2-1x.png',
            'Text': 'We will email you once your client completes the schedule'
          },
          {
            'name': 'Engagement Name 3',
            'label': 'In Progress',
            'image': 'https://staging-1337.auvenir.com/img/illustrations/auditStatus/mobile-3-1x.png',
            'Text': 'An invitation has been sent to your client.  Your audit will begin shortly'
          },
          {
            'name': 'Engagement Name 4',
            'label': 'Action Required',
            'image': 'https://staging-1337.auvenir.com/img/illustrations/auditStatus/mobile-4-1x.png',
            'Text': 'Great, your client is done! Please review the schedule from your desktop'
          }
        ]
        res.json({code: Code.api.OK, engagements: engs})
        res.end()
      }
    })
  })

/**
 * Handles the process of deregestring the device from the idq database of the user
 * @name api/v1/deregister-device
 * @memberof Router
 * @param {String}   path                    - the incoming http request
 * @param {Function} isDevAuthenticated      - Middleware function call
 * @param {Function} isValidAuthenticationID - Middleware function call
 * @param {Function} isValidAccessToken      - Middleware function call
 * @param {Function} callback                - the outgoing response
 */
  router.post('/v1/deregister-device', isDevAuthenticated, isValidAuthenticationID, isValidAccessToken, function (req, res) {
    var userAuthID = req.body.userAuthID
    var uniqueID = req.body.deviceInfo

    if (uniqueID === undefined || uniqueID === null) {
      req.log.warn('Device serial number is not found')
      res.json({code: Code.api.MISSING_DEVICE_INFO, msg: 'Device serial number is not found'})
      res.end()
      return
    } else {
      if (typeof uniqueID !== String || uniqueID === '') {
        req.log.warn('Device serial number is invalid')
        res.json({code: Code.api.INVALID_DEVICE_INFO, msg: 'Device serial number is invalid'})
        res.end()
        return
      }
    }

    Models.Auvenir.User.findOne({ 'auth.id': userAuthID }, function (err, oneUser) {
      if (err) {
        req.log.error('Error retrieving user from database', {err})
        res.json({code: Code.db.ERROR_OP_FIND, msg: 'Error retriveing user from database'})
        res.end()
        return
      }

      if (!oneUser) {
        req.log.warn('Unable to find user for authentication ')
        res.json({code: Code.db.DNE_USER, msg: 'Unable to find user for authentication'})
        res.end()
      } else {
        if (oneUser.auth.mobile && oneUser.auth.devices.length > 0) {
          var devices = oneUser.auth.devices
          var isFound = false
          for (var i = 0; i < devices.length; i++) {
            var device = devices[i]
            if (device.uniqueID === uniqueID) {
              isFound = true
              break
            }
          }

          if (isFound) {
            Models.Auvenir.User.update({'auth.id': userAuthID}, {$pull: {'auth.devices': {'uniqueID': uniqueID}}}, function (err, result) {
              if (err) {
                req.log.error({err})
                res.json({code: Code.db.ERROR_OP_UPDATE, msg: 'Error occured during deleting devices'})
                res.end()
              } else {
                if (result) {
                  res.json({code: Code.api.OK, msg: 'Device: ' + uniqueID + ' was Deregistered'})
                  res.end()
                } else {
                  req.log.warn('unable to delete device')
                  res.json({code: Code.api.ERROR, msg: 'unable to delete device'})
                  res.end()
                }
              }
            })
          } else {
            req.log.info('Device was not found')
            res.json({code: Code.db.DNE_DEVICE, msg: 'No Device was found'})
            res.end()
          }
        } else {
          req.log.warn('No device for these userId registered')
          res.json({code: Code.db.MISSING_IDQID, msg: 'No device for these userId registered'})
          res.end()
        }
      }
    })
  })

/**
 * Check access for devices to see whether the device is on the list of device supported for the user
 * @name api/v1/check-device
 * @memberof Router
 * @param {String}   path                    - the incoming http request
 * @param {Function} isDevAuthenticated      - Middleware function call
 * @param {Function} isValidAuthenticationID - Middleware function call
 * @param {Function} isValidAccessToken      - Middleware function call
 * @param {Function} callback                - the outgoing response
 */
  router.get('/v1/check-device', isDevAuthenticated, isValidAuthenticationID, function (req, res) {
    var userAuthID = req.query.userAuthID
    var uniqueID = req.query.deviceInfo

    Models.Auvenir.User.findOne({ 'auth.id': userAuthID}, function (err, oneUser) {
      if (err) {
        req.log.error('Error retrieving user from database', {err})
        res.json({code: Code.db.ERROR_OP_FIND, msg: 'Error retriveing user from database'})
        res.end()
        return
      }

      if (!oneUser) {
        req.log.warn('Unable to find user for authentication')
        res.json({code: Code.db.DNE_USER, msg: 'Unable to find user for authentication'})
        res.end()
      } else {
        req.log.info('User exists')

        if (oneUser.auth && oneUser.auth.mobile) {
          if (!oneUser.auth.mobile.id) {
            req.log.warn('IDQ ID does not exists')
            res.json({code: Code.db.DNE_IDQID, msg: 'IDQ ID does not exist'})
            res.end()
            return
          }

          var devices = oneUser.auth.devices
          if (!devices) {
            req.log.warn('Device does not exist')
            res.json({code: Code.db.DNE_DEVICE, msg: 'Device does not exist'})
            res.end()
            return
          }
          for (var i = 0; i < devices.length; i++) {
            if (devices[i].uniqueID === uniqueID) {
              req.log.info('Device ' + uniqueID + ' was registered in Auvenir Database.')
              res.json({code: Code.api.OK, msg: 'Device ' + uniqueID + ' was registered in Auvenir Database.'})
              res.end()
              return
            }
          }

          res.json({code: Code.db.ERROR, msg: 'Device does not exist'})
          res.end()
        } else {
          req.log.warn('DB authorization for IDQ does not exist')
          res.json({code: Code.db.ERROR, msg: 'DB authorization for IDQ does not exist'})
          res.end()
        }
      }
    })
  })

/**
 * This function will De-register the IDQ user ID and its devices from Auvenir Database.
 * TODO: This function can/should be moved application side to Utility.removeDevice
 *       However, there is a dependancy on the mobile application.
 * @name api/v1/de-register
 * @memberof Router
 * @param {String}   path                    - the incoming http request
 * @param {Function} isDevAuthenticated      - Middleware function call
 * @param {Function} isValidAuthenticationID - Middleware function call
 * @param {Function} callback                - the outgoing response
 */
  router.post('/v1/deregister-user', isDevAuthenticated, isValidAuthenticationID, function (req, res) {
    var userAuthID = req.body.userAuthID

    Models.Auvenir.User.findOne({ 'auth.id': userAuthID}, function (err, oneUser) {
      if (err) {
        req.log.error({err})
        res.json({ code: Code.db.ERROR_OP_FIND, msg: 'Error retrieving user from database'})
        res.end()
        return
      }

      if (!oneUser) {
        req.log.warn('Unable to find user for authentication ')
        res.json({code: Code.db.DNE_USER, msg: 'Unable to find user'})
        res.end()
      } else {
        req.log.info('User found: ' + oneUser.email)
        if (oneUser.auth.mobile.id && oneUser.auth.devices) {
          IDQ.deleteUser(oneUser.auth.mobile.id, function (err, result) {
            if (err) {
              req.log.error({err})
              res.json(err)
              res.end()
            } else {
              Models.Auvenir.User.update({'auth.id': userAuthID}, {$unset: {'auth.mobile': '' }}, function (err, updatedUser) {
                if (err) {
                  req.log.error({err})
                  res.json({code: Code.db.ERROR_OP_UPDATE, msg: 'Error occured during update of idq id data'})
                  res.end()
                } else {
                  if (updatedUser) {
                    Models.Auvenir.User.update({'auth.id': userAuthID}, {$pull: {'auth.devices': {'type': 'MOBILE'} }}, function (err, results) {
                      if (err) {
                        req.log.error({err})
                        res.json({code: Code.db.ERROR_OP_UPDATE, msg: 'Error occured during update of idq devices data'})
                        res.end()
                      } else {
                        req.log.info('IDQ ID and Devices was Deregistered')
                        res.json({code: Code.api.OK, msg: 'IDQ ID and Devices was Deregistered'})
                        res.end()
                      }
                    })
                  } else {
                    req.log.warn('Unable to Deregistered IDQ ID and Devices')
                    res.json({code: Code.api.ERROR, msg: 'Unable to Deregistered IDQ IDand Devices'})
                    res.end()
                  }
                }
              })
            }
          })
        } else {
          req.log.warn('No IDQ ID found with that user')
          res.json({code: Code.db.MISSING_IDQID, msg: 'No IDQ ID found with that user'})
          res.end()
        }
      }
    })
  })

  router.post('/v1/online-users', isDevAuthenticated, function (req, res) {
    const onlineUsers = []
    if (sockets.getSockets()) {
      const connections = sockets.getSockets().connected
      Object.keys(connections).forEach((socketId) => {
        const connection = connections[socketId]
        if (connection.customData && connection.customData.authID) {
          const authID = connection.customData.authID
          if (!~onlineUsers.indexOf(authID)) {
            onlineUsers.push(authID)
          }
        }
      })
    }

    Models.Auvenir.User.find({'auth.id': {$in: onlineUsers}}, (err, users) => {
      if (err) {
        return res.json({code: 500, err})
      }
      const onlineUsers = users.map(({_id, email, type, firstName, lastName}) => {
        return {_id, email, type, firstName, lastName}
      })
      res.json({code: 200, onlineUsers})
    })
  })
  router.post('/v1/admin/query', isDevAuthenticated, (req, res) => {
    const {query = {}, model = 'User', method = 'find'} = req.body
    const validModels = ['User']
    const validMethods = ['find']

    if (!~validModels.indexOf(model)) {
      return res.json({
        code: 400,
        err: {
          message: 'Invalid model',
          validModels
        }
      })
    }
    if (!~validMethods.indexOf(method)) {
      return res.json({
        code: 400,
        err: {
          message: 'Invalid method',
          validMethods
        }
      })
    }
    if (!_.isObject(query)) {
      return res.json({
        code: 400,
        err: {
          message: 'Invalid query. Should be json object'
        }
      })
    }

    const modelObj = Models.Auvenir[model]
    const cbk = (err, results) => {
      if (err) {
        return res.json({code: 500, err})
      }

      if (model === 'User') {
        results.forEach((user) => {
          if (user.devices) {
            user.devices.forEach((device) => {
              if (device.access && device.access.token) {
                delete device.access.token
              }
            })
          }
        })
      }
      res.json({code: 200, results})
    }
    const params = [query]
    if (model === 'User') {
      params.push({
        'auth.access.token': 0,
        'auth.mobile.push.token': 0,
        'auth.developer.apiKey': 0
      })
    }
    params.push(cbk)

    modelObj[method].apply(modelObj, params)
  })
  router.post('/v1/admin/send-login-email', isDevAuthenticated, (req, res) => {
    const {email} = req.body

    const findUserAccount = (cbk) => {
      Utility.findUserAccount(email, (err, user) => {
        if (err || !user) {
          return cbk({code: 400, msg: 'There was an issue finding the account'})
        }
        cbk(null, {user})
      })
    }

    const checkUserStatus = ({user}, cbk) => {
      switch (user.status) {
        case 'ACTIVE':
          cbk(null, {user, status: 'active'})
          break
        case 'ONBOARDING':
          cbk(null, {user, status: 'onboarding'})
          break
        case 'PENDING':
          cbk({code: 403, msg: `A login email cannot be sent due to the user status is pending.`})
          break
        case 'WAIT-LIST':
          cbk({code: 403, msg: `A login email cannot be sent due to the user status is wait-list.`})
          break
        case 'LOCKED':
          cbk({code: 403, msg: `A login email cannot be sent due to the user status is locked.`})
          break
        default:
          cbk({code: 403, msg: `A login email cannot be sent due to the user status is deactived.`})
          break
      }
    }

    const createLoginToken = ({user, status}, cbk) => {
      if (status === 'active' || status === 'onboarding') {
        user.auth.access.token = Utility.randomString(64, Utility.ALPHANUMERIC)
        user.auth.access.expires = Date.now() + Utility.SESSION_LENGTH
        user.save((err) => {
          if (err) {
            return cbk({code: 400, msg: 'Error creating login token'})
          }
          cbk(null, {user, status})
        })
      }
    }

    const sendEmail = ({user, status}, cbk) => {
      const {email, firstName, lastName, auth} = user
      const customValues = {
        url: appConfig.publicServer.url,
        token: auth.access.token,
        email: email
      }
      Utility.sendEmail({
        fromEmail: 'andi@auvenir.com',
        toEmail: email,
        toName: firstName + ' ' + lastName,
        status,
        customValues
      }, (err) => {
        if (err) {
          return cbk({code: 400, msg: 'Error sending email'})
        }
        cbk()
      })
    }

    async.waterfall([findUserAccount, checkUserStatus, createLoginToken, sendEmail], (err) => {
      if (err) {
        if (err.code) {
          res.status(err.code).json(err)
        } else {
          res.status(500).json(err)
        }
      } else {
        res.status(200).json({
          msg: `Login email sent to ${email}`
        })
      }
    })
  })

  /**
   * These apis are for dev/testing purpose.
   */
  if (~['local', 'dev', 'qa'].indexOf(appConfig.env)) {
    router.get('/user/:email/delete', ({params: {email}}, res) => {
      if (!email) {
        return res.json({error: 'Email was not provided'})
      }

      const findUser = (cbk) => {
        Models.Auvenir.User.findOne({email: email}, (err, user) => {
          if (!user) {
            return cbk({msg: `Cannot find the user with ${email}`, code: 404})
          }
          cbk(err, {user})
        })
      }

      const clearUser = ({user}, cbk) => {
        const aclQuery = {acl: {$elemMatch: {id: user._id}}}
        const removeUser = (cbk) => {
          user.remove((err) => {
            cbk(err)
          })
        }
        const removeFromBusiness = (cbk) => {
          Models.Auvenir.Business.findOneAndUpdate(aclQuery, {$pull: {acl: {id: user._id}}}, (err) => {
            cbk(err)
          })
        }
        const removeFromEngagement = (cbk) => {
          Models.Auvenir.Engagement.findOneAndUpdate(aclQuery, {$pull: {acl: {id: user._id}}}, (err) => {
            cbk(err)
          })
        }
        const removeFromFirm = (cbk) => {
          Models.Auvenir.Firm.findOneAndUpdate(aclQuery, {$pull: {acl: {id: user._id}}}, (err) => {
            cbk(err)
          })
        }

        const removeFromIDQ = (cbk) => {
          IDQ.deleteUser(user.auth.mobile.id, cbk)
        }

        const tasks = [removeUser, removeFromBusiness, removeFromFirm, removeFromEngagement]
        if (user.auth && user.auth.mobile && user.auth.mobile.id) {
          tasks.push(removeFromIDQ)
        }
        async.parallel(tasks, cbk)
      }

      async.waterfall([findUser, clearUser], (err) => {
        if (err) {
          if (err.code) {
            return res.json(err)
          }
          return res.json({code: 500, err})
        }

        res.json({
          deleted: email,
          code: 200
        })
      })
    })
    router.get('/user/:email/update', ({params: {email}, query: {status}}, res) => {
      const validStatus = ['WAIT-LIST', 'ONBOARDING', 'ACTIVE', 'LOCKED', 'INACTIVE', 'PENDING']
      if (!~validStatus.indexOf(status)) {
        return res.json({code: 400, err: 'Invalid Status'})
      }
      Models.Auvenir.User.findOneAndUpdate({email}, {$set: {status}}, {new: true}, (err) => {
        if (err) {
          return res.json({code: 500, err})
        }

        res.json({code: 200})
      })
    })
  }
  return router
}
