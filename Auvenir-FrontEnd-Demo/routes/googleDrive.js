/**
 * Google Drive Module
 *
 * @author Lucy Kang & Justin
 * @lastUpdated 2017-05-11
 */

// Load all the required modules
const express = require('express')
const router = express.Router()
const request = require('request')
const formidable = require('formidable')
const mongoose = require('mongoose')
const async = require('async')
const _ = require('lodash')
const Utility = require('../plugins/utilities/utility')
const Models = require('../models')
const Access = require('../plugins/utilities/accessControls')
const appConfig = require('../config')
const debug = require('debug')('googleDrive')
const crypto = require('crypto')
const gridStream = require('gridfs-stream')
const encryptAlgorithm = appConfig.security.file_encryption.algorithm
const encryptPassword = appConfig.security.file_encryption.secret
const url = appConfig.publicServer.url
const ObjectId = require('mongodb').ObjectID
const initLogger = require('../plugins/logger/logger').init
const {info, warn, error} = initLogger(__filename)

const connections = require('../models/connections')

var gfs = null
connections.gdrive.on('open', function () {
  gfs = gridStream(connections.gdrive.db, mongoose.mongo)
})

const google = require('googleapis')
const googleDrive = google.drive('v3')
const CLIENT_ID = appConfig.integrations.gdrive.CLIENT_ID
const CLIENT_SECRET = appConfig.integrations.gdrive.CLIENT_SECRET

var moduleName = '[Google Drive] '
var defaultErrMsg = 'There was an error while integrating with Google Drive.'
var defaultFileErrMsg = 'There was an error while uploading your file from Google Drive.'

/* -------------------------------------------------------------*
 *                    middleware functions                       *
 * -------------------------------------------------------------- */

/**
 * This function checks if user is authenticated
 *
 * @param {any} req
 * @param {any} res
 * @param {any} next
 * @returns
 */
var isAuthenticated = function (req, res, next) {
  debug(`${moduleName} Client Authentication Check.`)
  if (req.isAuthenticated()) {
    debug(`${moduleName} Basic Authentication`)

    // var portalPermissions = req.user.accountPermissions
    // for(var i=0; i<portalPermissions.length; i++) {
    //    if(portalPermissions[i].portal === 'CLIENT')
    return next()
    // }
  }

  debug(`${moduleName} NOT Authenticated.`)
  res.redirect('/')
}

/* -------------------------------------------------------------*
 *                          functions                            *
 * -------------------------------------------------------------- */

/**
 * function to request GD : getChildList(gdriveID, accessToken)
 *
 * @param {string} gdriveID
 * @return {JSON}  err / body
 */
var getChildList = function (gdriveID, accessToken, callback) {
  request.get('https://www.googleapis.com/drive/v2/files/' + gdriveID + '/children?access_token=' + accessToken, (err, resp, body) => {
    if (err) {
      return callback(err)
    }
    if (body) {
      if (body.error) {
        return callback({ msg: body.error.message, code: body.error.code })
      } else {
        body = JSON.parse(body)
        return callback(null, body)
      }
    }
  })
}

/**
 * function to request GD : getFileInfo(gdriveID, accessToken)
 *
 * @param {string} gdriveID
 * @param {string} accessToken
 * @param {any} callback
 */
var getFileInfo = function (gdriveID, accessToken, callback) {
  request.get('https://www.googleapis.com/drive/v2/files/' + gdriveID + '?access_token=' + accessToken, (err, resp, body) => {
    if (err) {
      return callback(err)
    }
    if (body) {
      if (body.error) {
        return callback({ msg: body.error.message, code: body.error.code })
      } else {
        body = JSON.parse(body)
        return callback(null, body)
      }
    }
  })
}

/**
 * Revoke refresh token (or access_token).
 * By revoking the access_token, it will revoke associated refresh_token as well.
 *
 * @param {any} token
 * @param {any} callback
 */
var revokeTokenRequest = function (req, token, callback) {
  let options = {
    url: 'https://accounts.google.com/o/oauth2/revoke?token=' + token,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  }
  request.post(options, (err, resp, body) => {
    if (err) {
      return callback({ status: 'error', msg: err })
    }
    if (JSON.parse(body).error) {
      req.log.warn(JSON.stringify(body))
      return callback(null, {
        status: 'fail',
        msg: { error: JSON.parse(body).error, description: JSON.parse(body).error_description }
      })
    }

    req.log.warn(`revokeToken result : ${JSON.stringify(body)}`)
    return callback(null, { status: 'success' })
  })
}

var revokeToken = function (oneConsumer, req, res, errMsg) {
  revokeTokenRequest(req, oneConsumer.refreshToken, (err, result) => {
    if (err || !result) {
      return res.status(500).json({ msg: errMsg })
    }

    if (result.status === 'success') {
      oneConsumer.status = 'INACTIVE'
      oneConsumer.refreshToken = 'REVOKED'
      oneConsumer.selectedFolder = []
      oneConsumer.idToken = 'REVOKED'
      oneConsumer.accessToken = 'REVOKED'
      oneConsumer.save((err) => {
        if (err) {
          return res.status(500).json({ msg: errMsg })
        } else {
          debug(`${moduleName} Consumer data with REVOKED status is saved successfully.`)
          return res.status(200).json({ msg: 'Your Google Drive account is disconnected successfully.' })
        }
      })
    } else if (result.status === 'fail') {
      // TODO - if we have queue system in place, we will try a few more times.
      return res.status(500).json({ msg: errMsg })
    } else {
      return res.status(500).json({ msg: errMsg })
    }
  })
}

/**
 * request new access_token with refresh token in case the access_token is
 * expired in certain time (usually in 3920ms)
 *
 * @param {string} refreshToken
 * @param {any} callback
 */
var renewToken = function (oauthClient, callback) {
  debug(`${moduleName} Getting new access_token from Google api`)
  oauthClient.refreshAccessToken((err, tokens) => {
    if (err) {
      return callback({ msg: err })
    }

    if (tokens) {
      debug(`TOKEN RECEVIED.`)
      debug(`${JSON.stringify(tokens)}`)
      return callback(null, tokens)
    }
  })
}

/* -------------------------------------------------------------*
 *                           Routers                            *
 * ------------------------------------------------------------- */
module.exports = function (passport, config) {
  /**
   * Creating user on db.
   */
  router.post('/createUser', isAuthenticated, function (req, res) {
    debug(`${moduleName} /createUser is triggered.`)

    try {
      let form = new formidable.IncomingForm()
      form.parse(req, (err, field) => {
        if (err) {
          req.log.error(moduleName + 'ERROR: parsing req with form info.', { err })
          return res.status(500).json({ msg: defaultErrMsg })
        }
        if (_.isEmpty(field)) {
          req.log.warn(moduleName + 'ERROR: No Fields passed through request.')
          return res.status(500).json({ msg: defaultErrMsg })
        }
        if (_.isEmpty(field.userID)) {
          req.log.warn(moduleName + 'ERROR: No userID was passed through request.' + field.userID)
          return res.status(500).json({ msg: defaultErrMsg })
        }

        let auvUserID = (_.isString(field.userID)) ? ObjectId(field.userID) : field.userID
        let uid = Utility.randomString(12, Utility.ALPHANUMERIC)
        let gdriveUser = new Models.Gdrive.User()
        gdriveUser.uid = uid
        gdriveUser.userID = auvUserID
        gdriveUser.status = 'SETUP'

        // find the user by userID, if not, create one.
        Models.Gdrive.User.findOne({ userID: auvUserID }, (err, oneUser) => {
          if (err) {
            req.log.error(moduleName + 'ERROR finding user from db.', { err })
            return res.status(500).json({ msg: defaultErrMsg })
          }
          if (oneUser) {
            debug(`${moduleName} User already has another Google Drive account. Not creating new user.`)
            return res.status(200).json({ uid: oneUser.uid })
          }

          debug(`${moduleName} No User found with the same userID. Creating User.`)
          gdriveUser.save((err) => {
            if (err) {
              req.log.error(moduleName + 'There was an error while saving User.', { err })
              return res.status(500).json({ msg: defaultErrMsg })
            }

            debug(`${moduleName} User is saved successfully.`)
            return res.status(200).json({ userID: auvUserID, uid: gdriveUser.uid })
          })
        })
      })
    } catch (err) {
      req.log.error(moduleName + '@createUser - Unknown error occured.', { err })
      return res.status(500).json({ msg: defaultErrMsg + ' If problem persists, please remove all file integrations you have chosen and contact Auvenir Team.' })
    }
  })

  /**
   * Upon offline access grant, make a request to google api server to retrieve permanent token.
   * Here we create Account.
   */
  router.post('/offlineAccess', isAuthenticated, function (req, res) {
    debug(`${moduleName} /offlineAccess is triggered. `)

    try {
      let form = new formidable.IncomingForm()
      form.parse(req, (err, field) => {
        if (err) {
          req.log.error(moduleName + '@offlineAccess - ERROR: parsing req with form info.', { err })
          return res.status(500).json({ msg: defaultErrMsg })
        }
        if (!field) {
          req.log.error(moduleName + '@offlineAccess - ERROR: No valid fields passed through request.')
          return res.status(500).json({ msg: defaultErrMsg })
        }
        if (!field.code || !_.isString(field.code)) {
          req.log.error(moduleName + '@offlineAccess - ERROR: No code was passed through request.', field)
          return res.status(500).json({ msg: defaultErrMsg })
        }
        if (!field.authID || !_.isString(field.authID)) {
          req.log.error(moduleName + '@offlineAccess - ERROR: No authID was passed through request.', field)
          return res.status(500).json({ msg: defaultErrMsg })
        }
        if (!field.uid || !_.isString(field.uid)) {
          req.log.error(moduleName + '@offlineAccess - ERROR: No uid was passed through request.', field)
          return res.status(500).json({ msg: defaultErrMsg })
        }
        if (!field.userID || !_.isString(field.userID)) {
          req.log.error(moduleName + '@offlineAccess - ERROR: invalid userID passed through request.', field)
          return res.status(500).json({ msg: defaultErrMsg })
        }

        let { code, authID, userID, uid } = field

        // for security, check uid against db
        Models.Gdrive.User.findOne({ userID, uid }, (err, oneUser) => {
          if (err) {
            req.log.error(moduleName + '@offlineAccess - ERROR: finding user from db.', { err })
            return res.status(500).json({ msg: defaultErrMsg })
          }
          if (!oneUser) {
            // TODO - malicious attempt. Send security notification.
            req.log.error(`${moduleName} @offlineAccess - No user was found with the userID: ${userID} & uid: ${uid} `)
            return res.status(500).json({ msg: defaultErrMsg })
          }

          let params = {
            code,
            client_id: CLIENT_ID,
            client_secret: CLIENT_SECRET,
            grant_type: 'authorization_code',
            redirect_uri: url
          }

          request.post({ url: 'https://www.googleapis.com/oauth2/v4/token', form: params }, (err, resp, body) => {
            if (err) {
              req.log.error(moduleName + '@offlineAccess - ERROR while requesting tokens from google api', { err })
              return res.status(500).json({ msg: defaultErrMsg })
            }
            if (!resp) {
              req.log.error(moduleName + '@offlineAccess - No response received from google api server.')
              return res.status(500).json({ msg: defaultErrMsg })
            }

            debug(`Token Request Response Body: ${JSON.stringify(body)}`)
            if (body.error) {
              req.log.error(moduleName + '@offlineAccess - ERROR Msg: ' + body.error + 'ERROR Desc: ' + body.error_description)
              return res.status(500).json({ msg: 'There was an error while connecting to your account in Google. Please try again.' })
            }

            let expiryDate = new Date().getTime() + (JSON.parse(body)).expires_in
            let idToken = (JSON.parse(body)).id_token
            let userEmail
            let accessToken = (JSON.parse(body)).access_token
            let refreshToken = (JSON.parse(body)).refresh_token

            async.waterfall([
              function requestUserEmail (cbk) {
                // send request to get user info from idToken.
                request.post('https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=' + idToken, (err, resp, body) => {
                  if (err) {
                    req.log.error(moduleName + '@offlineAccess - ERROR while requesting user info from google api.', { err })
                    // revoke token if there is an error before creating the consumer on gdriveDB.
                    revokeTokenRequest(req, accessToken, (err, result) => {
                      if (err || !result) {
                        req.log.error(moduleName + '@offlineAccess - Error from request to Google api to revoke token.', { err })
                        return cbk({ msg: defaultErrMsg })
                      }
                      if (result.status === 'success') {
                        debug(`${moduleName} @offlineAccess - Token revoked successfully.`)
                        return cbk({ msg: defaultErrMsg + ' Please try again.' })
                      } else if (result.status === 'fail') {
                        // TODO - if we have queue system in place, we will try a few more times.
                        req.log.error(moduleName + 'Bad Request 400 is returned from Google api.', result)
                        return cbk({ msg: defaultErrMsg })
                      } else {
                        req.log.error(moduleName + 'Unknown Status: ' + result.status)
                        req.log.error(result)
                        return cbk({ msg: defaultErrMsg })
                      }
                    })
                  } else if (!resp) {
                    req.log.error(moduleName + '@offlineAccess - No response received from google api server.')
                    revokeTokenRequest(req, accessToken, (err, result) => {
                      if (err || !result) {
                        req.log.error(moduleName + '@offlineAccess - Error from request to Google api to revoke token.', { err })
                        return cbk({ msg: defaultErrMsg })
                      }
                      if (result.status === 'success') {
                        debug(`${moduleName} @offlineAccess - Token revoked successfully.`)
                        return cbk({ msg: defaultErrMsg + ' Please try again.' })
                      } else if (result.status === 'fail') {
                        // TODO - if we have queue system in place, we will try a few more times.
                        req.log.error(moduleName + 'Bad Request 400 is returned from Google api.', result)
                        return cbk({ msg: defaultErrMsg })
                      } else {
                        req.log.error(moduleName + 'Unknown Status: ' + result.status)
                        req.log.error(result)
                        return cbk({ msg: defaultErrMsg })
                      }
                    })
                  } else {
                    debug(`${moduleName} @offlineAccess - User Info from id_token: ${JSON.stringify(body)}`)
                    let intendedClient = JSON.parse(body).aud

                    if (intendedClient !== CLIENT_ID) {
                      // TODO - security issue. send slack notification to the security team.
                      revokeTokenRequest(req, accessToken, (err, result) => {
                        if (err || !result) {
                          req.log.error(moduleName + '@offlineAccess - Error from request to Google api to revoke token.', { err })
                          return cbk({ msg: defaultErrMsg })
                        }
                        if (result.status === 'success') {
                          debug(`${moduleName} @offlineAccess - Token revoked successfully.`)
                          return cbk({ msg: defaultErrMsg + ' Please try again.' })
                        } else if (result.status === 'fail') {
                          // TODO - if we have queue system in place, we will try a few more times.
                          req.log.error(moduleName + 'Bad Request 400 is returned from Google api.', result)
                          return cbk({ msg: defaultErrMsg })
                        } else {
                          req.log.error(moduleName + 'Unknown Status: ' + result.status)
                          req.log.error(result)
                          return cbk({ msg: defaultErrMsg })
                        }
                      })
                    } else {
                      debug(`${moduleName} @offlineAccess - Both id_token & intended_client are verified.`)
                      userEmail = JSON.parse(body).email
                      cbk(null, refreshToken, userEmail)
                    }
                  }
                })
              }, function checkConsumer (refreshToken, userEmail, cbk) {
                // if no refresh_token, revoke using access token & send res so user can try offlineAccess again.
                if (!refreshToken) {
                  // check if user has account under same email & uid & authID (This means this account was connected with this user. )
                  Models.Gdrive.Consumer.findOne({ userEmail, uid, createdBy: authID }, (err, oneConsumer) => {
                    if (err) {
                      req.log.error(moduleName + '@offlineAccess - ERROR finding account from db.', { err })
                      return cbk({ msg: defaultErrMsg })
                    }
                    if (!oneConsumer) {
                      req.log.error(moduleName + '@offlineAccess - No consumer found on our db but the user have Auvenir as connected App with their Google Account.')

                      revokeTokenRequest(req, accessToken, (err, result) => {
                        if (err || !result) {
                          req.log.error(moduleName + '@offlineAccess - Error from request to Google api to revoke token.', { err })
                          return cbk({ msg: defaultErrMsg })
                        }
                        if (result.status === 'success') {
                          debug(`${moduleName} @offlineAccess - Token revoked successfully.`)
                          return cbk({ msg: defaultErrMsg + ' Please try again.' })
                        } else if (result.status === 'fail') {
                          // TODO - if we have queue system in place, we will try a few more times.
                          req.log.error(moduleName + 'Bad Request 400 is returned from Google api.', result)
                          return cbk({ msg: defaultErrMsg })
                        } else {
                          req.log.error(moduleName + 'Unknown Status: ' + result.status)
                          req.log.error(result)
                          return cbk({ msg: defaultErrMsg })
                        }
                      })
                    } else {
                      debug(`${moduleName} @offlineAccess - User already has the same account. Not creating new account.`)
                      if (oneConsumer.refreshToken === 'REVOKED') {
                        revokeTokenRequest(req, accessToken, (err, result) => {
                          if (err || !result) {
                            req.log.error(moduleName + '@offlineAccess - Error from request to Google api to revoke token.', { err })
                            return cbk({ msg: defaultErrMsg })
                          }
                          if (result.status === 'success') {
                            debug(`${moduleName} @offlineAccess - Token revoked successfully.`)
                            return cbk({ msg: defaultErrMsg + ' Please try again.' })
                          } else if (result.status === 'fail') {
                            // TODO - if we have queue system in place, we will try a few more times.
                            req.log.error(moduleName + 'Bad Request 400 is returned from Google api.')
                            req.log.error(result.msg)
                            return cbk({ msg: defaultErrMsg })
                          } else {
                            req.log.error(moduleName + 'Unknown Status: ' + result.status)
                            req.log.error(result)
                            return cbk({ msg: defaultErrMsg })
                          }
                        })
                      } else {
                        oneConsumer.accessToken = accessToken
                        oneConsumer.tokenExpiry = expiryDate
                        oneConsumer.status = 'SETUP'

                        oneConsumer.save((err) => {
                          if (err) {
                            req.log.error(moduleName + '@offlineAccess - ERROR: new accessToken could not be saved.', { err })
                          }
                        })
                        return cbk(null, { 'access_token': accessToken, userEmail })
                      }
                    }
                  })
                } else {
                  debug(`ExpiryDate: ${expiryDate} `)
                  debug(`Expire in: ${(JSON.parse(body)).expires_in}`)

                  // check if the new tokens need to be updated to existing consumer or added to a new consumer.
                  Models.Gdrive.Consumer.findOne({
                    userEmail,
                    uid,
                    createdBy: authID
                  }, (err, existingConsumer) => {
                    if (err) {
                      req.log.error(moduleName + '@offlineAccess - ERROR finding account from db.', { err })

                      revokeTokenRequest(req, accessToken, (err, result) => {
                        if (err || !result) {
                          req.log.error(moduleName + '@offlineAccess - Error from request to Google api to revoke token.', { err })
                          return cbk({ msg: defaultErrMsg })
                        }
                        if (result.status === 'success') {
                          debug(`${moduleName} @offlineAccess - Token revoked successfully.`)
                          return cbk({ msg: defaultErrMsg + ' Please try again.' })
                        } else if (result.status === 'fail') {
                          // TODO - if we have queue system in place, we will try a few more times.
                          req.log.error(moduleName + 'Bad Request 400 is returned from Google api.')
                          req.log.error(result.msg)
                          return cbk({ msg: defaultErrMsg })
                        } else {
                          req.log.error(moduleName + 'Unknown Status: ' + result.status)
                          req.log.error(result)
                          return cbk({ msg: defaultErrMsg })
                        }
                      })
                    }
                    if (!existingConsumer) {
                      let gdriveConsumer = new Models.Gdrive.Consumer()
                      gdriveConsumer.uid = uid
                      gdriveConsumer.userEmail = userEmail
                      gdriveConsumer.refreshToken = refreshToken
                      gdriveConsumer.tokenLastUsed = new Date() // for refreshToken
                      gdriveConsumer.accessToken = accessToken
                      gdriveConsumer.tokenExpiry = expiryDate // for accessToken
                      gdriveConsumer.idToken = idToken
                      gdriveConsumer.status = 'SETUP'
                      gdriveConsumer.createdBy = authID

                      gdriveConsumer.save((err) => {
                        if (err) {
                          req.log.error(moduleName + '@offlineAccess - There was an error while saving Account.', { err })
                          revokeTokenRequest(req, refreshToken, (err, result) => {
                            if (err || !result) {
                              req.log.error(moduleName + '@offlineAccess - Error from request to Google api to revoke token.', { err })
                              return cbk({ msg: defaultErrMsg })
                            }
                            if (result.status === 'success') {
                              debug(`${moduleName} @offlineAccess - Token revoked successfully.`)
                              return cbk({ msg: defaultErrMsg + ' Please try again.' })
                            } else if (result.status === 'fail') {
                              // TODO - if we have queue system in place, we will try a few more times.
                              req.log.error(moduleName + 'Bad Request 400 is returned from Google api.')
                              req.log.error(result.msg)
                              return cbk({ msg: defaultErrMsg })
                            } else {
                              req.log.error(moduleName + 'Unknown Status: ' + result.status)
                              req.log.error(result)
                              return cbk({ msg: defaultErrMsg })
                            }
                          })
                        }
                        debug(`${moduleName} @offlineAccess - Account is saved successfully.`)
                      })
                    } else {
                      if (existingConsumer.status === 'INACTIVE') {
                        debug(`${moduleName} @offlineAccess - User once had the same consumer with uid, email and authID with status INACTIVE. Not creating new account.`)
                      } else {
                        debug(`${moduleName} @offlineAccess - User once had the same consumer with uid, email and authID with status ${existingConsumer.status}. The user might have disconnected on their end (at the Google - user settings).`)
                      }

                      existingConsumer.refreshToken = refreshToken
                      existingConsumer.tokenLastUsed = new Date()
                      existingConsumer.accessToken = accessToken
                      existingConsumer.tokenExpiry = expiryDate
                      existingConsumer.idToken = idToken
                      existingConsumer.status = 'SETUP'
                      existingConsumer.save((err) => {
                        if (err) {
                          req.log.error(moduleName + '@offlineAccess - ERROR: new accessToken could not be saved.', { err })
                          return cbk({ msg: defaultErrMsg })
                        }
                      })
                    }
                  })

                  // change the status for user.
                  oneUser.status = 'ACTIVE'
                  oneUser.save((err) => {
                    if (err) {
                      req.log.error(moduleName + '@offlineAccess - ERROR: updating user status to ACTIVE', { err })
                      return cbk({ msg: defaultErrMsg })
                      // TODO - send slacknotifier or error email.
                    }
                    debug(`${moduleName} @offlineAccess - Updated user status to ACTIVE successfully.`)
                    return cbk(null, { 'access_token': accessToken, userEmail })
                  })
                }
              }
            ], function (err, offlineAccResult) {
              if (err) {
                req.log.error(err)
                return res.status(500).json({ msg: defaultErrMsg })
              }
              debug(`offlineAccess was successful.`)
              return res.status(200).json(offlineAccResult)
            })
          })
        })
      })
    } catch (err) {
      req.log.error(moduleName + '@offlineAccess - Unknown error occured.', { err })
      return res.status(500).json({ msg: defaultErrMsg + ' If problem persists, please remove all file integrations you have chosen and skip this section. ' })
    }
  })

  /**
   * Saving selected folder into the db & find
   */
  router.post('/saveSelectedFolders', isAuthenticated, function (req, res) {
    debug(`${moduleName} /saveSelectedFolders is triggered `)

    try {
      let form = new formidable.IncomingForm()
      form.parse(req, (err, field) => {
        if (err) {
          req.log.error(moduleName + '@saveSelectedFolders - ERROR: parsing req with form info.', { err })
          return res.status(500).json({ msg: defaultErrMsg })
        }
        if (!field) {
          req.log.error(moduleName + '@saveSelectedFolders - ERROR: No Fields passed through request.')
          return res.status(500).json({ msg: defaultErrMsg })
        }
        if (!field.folder || !_.isString(field.folder)) {
          req.log.error(moduleName + '@saveSelectedFolders - ERROR: Invalid folders were passed through request.')
          req.log.error(field.folder)
          return res.status(500).json({ msg: defaultErrMsg })
        }
        if (!field.authID || !_.isString(field.authID)) {
          req.log.error(moduleName + '@saveSelectedFolders - ERROR: Invalid authID passed through request.')
          req.log.error(field.authID)
          return res.status(500).json({ msg: defaultErrMsg })
        }
        if (!field.userID || !_.isString(field.userID)) {
          req.log.error(moduleName + '@saveSelectedFolders - ERROR: Invalid userID passed through request.')
          req.log.error(field.userID)
          return res.status(500).json({ msg: defaultErrMsg })
        }
        if (!field.accessToken || !_.isString(field.accessToken)) {
          req.log.error(moduleName + '@saveSelectedFolders - ERROR: Invalid Access Token passed through request.')
          req.log.error(field.accessToken)
          return res.status(500).json({ msg: defaultErrMsg })
        }
        if (!field.uid || !_.isString(field.uid)) {
          req.log.error(moduleName + '@saveSelectedFolders - ERROR: Invalid uid passed through request.')
          req.log.error(field.uid)
          return res.status(500).json({ msg: defaultErrMsg })
        }
        if (!field.email || !_.isString(field.email)) {
          req.log.error(moduleName + '@saveSelectedFolders - ERROR: Invalid email passed through request.')
          req.log.error(field.email)
          return res.status(500).json({ msg: defaultErrMsg })
        }

        let auvUserID = field.userID
        let auvAuthID = field.authID
        let selectedFolder = JSON.parse(field.folder)
        let selectedFolderArray = []
        let gDriveEmail = field.email
        let uid = field.uid
        let counter = 0

        for (let folder in selectedFolder) {
          if (selectedFolderArray.indexOf(selectedFolder[folder]) === -1) {
            selectedFolderArray.push(selectedFolder[folder])
          }
        }

        let totalSelectedFolder = selectedFolderArray.length
        Models.Gdrive.User.findOne({ userID: auvUserID, uid: uid }, (err, oneUser) => {
          if (err) {
            req.log.error(moduleName + '@saveSelectedFolders - ERROR finding user from db.', { err })
            return res.status(500).json({ msg: defaultErrMsg })
          }
          if (!oneUser) {
            req.log.error(moduleName + '@saveSelectedFolders - No User found with the same userID. Could not integrate folders.')
            return res.status(500).json({ msg: defaultErrMsg })
          } else {
            debug(`${moduleName} @saveSelectedFolders - User found from /gdrive/users.`)

            // find the account by the user.uid & authID
            Models.Gdrive.Consumer.findOne({
              uid: oneUser.uid,
              createdBy: auvAuthID,
              userEmail: gDriveEmail
            }, (err, oneConsumer) => {
              if (err) {
                req.log.error(moduleName + '@saveSelectedFolders - ERROR finding consumer from db.', { err })
                return res.status(500).json({ msg: defaultErrMsg })
              }
              if (!oneConsumer) {
                req.log.error(moduleName + '@saveSelectedFolders - No Consumer was found for the user.')
                return res.status(500).json({ msg: defaultErrMsg })
              }

              if (oneConsumer.status === 'ACTIVE' || oneConsumer.status === 'SETUP') {
                // first save selectedFolder on gdriveFile and then retrieve that _id to save in the Consumer.selectedFolders[].
                for (let i = 0; i < selectedFolderArray.length; i++) {
                  let folderID = selectedFolderArray[i.toString()].id

                  // check if there's any same folder that's been selected before.
                  // if not, start saving the files.
                  Models.Gdrive.Item.findOne({
                    gdriveID: folderID,
                    consumerID: Utility.castToObjectId(oneConsumer._id)
                  }, (err, oldFolder) => {
                    if (err) {
                      req.log.error(moduleName + '@saveSelectedFolders - ERROR finding same folder.', { err })
                      return res.status(500).json({ msg: 'There was an error while interating with Google Drive.' })
                    }
                    if (oldFolder) {
                      counter++
                      debug(`${moduleName} @saveSelectedFolders - This folder has already been integrated before. FolderID: ${oldFolder.gdriveID}`)
                      if (oneConsumer.selectedFolder.indexOf(oldFolder._id) === -1) {
                        debug(`Saving ${oldFolder._id} to the selectedFolder array`)
                        oneConsumer.selectedFolder.push(Utility.castToObjectId(oldFolder._id))
                      }
                      // re-activate the consumer.
                      oneConsumer.status = 'ACTIVE'
                      oneConsumer.save((err) => {
                        if (err) {
                          req.log.error(moduleName + '@saveSelectedFolders - ERROR saving updated selectedFolder info into db.', { err })
                          return res.status(500).json({ msg: defaultErrMsg })
                        }
                        debug(`${moduleName} @saveSelectedFolders - Saving selectedFolder into db was successful`)
                      })

                      if (counter === totalSelectedFolder) {
                        return res.status(200).json({ msg: 'Success' })
                      }
                    } else {
                      let gdriveFolder = new Models.Gdrive.Item()
                      gdriveFolder.consumerID = Utility.castToObjectId(oneConsumer._id)
                      gdriveFolder.gdriveID = folderID
                      gdriveFolder.parent = null
                      gdriveFolder.type = 'FOLDER'

                      gdriveFolder.save((err) => {
                        if (err) {
                          req.log.error(moduleName + '@saveSelectedFolders - ERROR saving selected folders', { err })
                          return res.status(500).json({ msg: defaultErrMsg })
                        }
                        debug(`${moduleName} @saveSelectedFolders - Selected Folders are saved successfully.`)

                        // find the file to get the objectID created by MongoDB
                        Models.Gdrive.Item.findOne({
                          gdriveID: folderID,
                          consumerID: Utility.castToObjectId(oneConsumer._id)
                        }, (err, oneFile) => {
                          if (err) {
                            req.log.error(moduleName + '@saveSelectedFolders - ERROR finding folder info from db.', { err })
                            return res.status(500).json({ msg: defaultErrMsg })
                          }
                          if (oneFile) {
                            debug(`${moduleName} adding file._id into the selectedFolder. oneFile._id: ${oneFile._id}`)
                            if (oneConsumer.selectedFolder.indexOf(oneFile._id) === -1) {
                              oneConsumer.selectedFolder.push(Utility.castToObjectId(oneFile._id))
                            }

                            // save addeds selectedFolder info into oneConsumer.
                            oneConsumer.status = 'ACTIVE'
                            oneConsumer.save((err) => {
                              if (err) {
                                req.log.error(moduleName + '@saveSelectedFolders - ERROR saving updated selectedFolder info into db.', { err })
                                return res.status(500).json({ msg: defaultErrMsg })
                              }
                              debug(`${moduleName} @saveSelectedFolders - Saving selectedFolder into db was successful`)
                              counter++
                              if (counter === totalSelectedFolder) {
                                return res.status(200).json({ msg: 'Success' })
                              }
                            })
                          } else {
                            req.log.error(moduleName + 'COULD NOT FIND oneFile that was supposed to be created before.')
                            return res.status(500).json({ msg: defaultErrMsg })
                          }
                        })
                      })
                    }
                  })
                }
              } else {
                req.log.error(moduleName + "@saveSelectedFolders - User's consumer was found but the status is not ACTIVE or SETUP.")
                return res.status(500).json({ msg: defaultErrMsg })
              }
            })
          }
        })
      })
    } catch (err) {
      req.log.error(moduleName + '@saveSelectedFolders - Unknown error occured.', { err })
      return res.status(500).json({ msg: defaultErrMsg + ' Please try again or contact Auvenir Team.' })
    }
  })

  router.post('/removeFolder', isAuthenticated, function (req, res) {
    debug(`${moduleName} Remove Folder is triggered. `)

    try {
      let form = new formidable.IncomingForm()

      form.parse(req, (err, field) => {
        if (err) {
          req.log.error(moduleName + '@removeFolder - ERROR: parsing req with form info.', { err })
          return res.status(500).json({ msg: defaultErrMsg })
        }
        if (!field) {
          req.log.error(moduleName + '@removeFolder - ERROR: No Fields passed through request.')
          return res.status(500).json({ msg: defaultErrMsg })
        }
        if (!field.uid || !_.isString(field.uid)) {
          req.log.error(moduleName + '@removeFolder - ERROR: Invalid uid passed through request.')
          req.log.error(field.uid)
          return res.status(500).json({ msg: defaultErrMsg })
        }
        if (!field.authID || !_.isString(field.authID)) {
          req.log.error(moduleName + '@removeFolder - ERROR: Invalid authID passed through request.')
          req.log.error(field.authID)
          return res.status(500).json({ msg: defaultErrMsg })
        }
        if (!field.userID || !_.isString(field.userID)) {
          req.log.error(moduleName + '@removeFolder - ERROR: Invalid userID passed through request.')
          req.log.error(field.userID)
          return res.status(500).json({ msg: defaultErrMsg })
        }
        if (!field.email || !_.isString(field.email)) {
          req.log.error(moduleName + '@removeFolder - ERROR: Invalid email passed through request.')
          req.log.error(field.email)
          return res.status(500).json({ msg: defaultErrMsg })
        }
        if (!field.folderID || !_.isString(field.folderID)) {
          req.log.error(moduleName + '@removeFolder - ERROR: Invalid folderID passed through request.')
          req.log.error(field.folderID)
          return res.status(500).json({ msg: defaultErrMsg })
        }

        let { uid, userID, authID, email, folderID } = field

        Models.Gdrive.User.findOne({ userID, uid }, (err, oneUser) => {
          if (err) {
            req.log.error(moduleName + '@removeFolder - ERROR finding user from db.', { err })
            return res.status(500).json({ msg: defaultErrMsg })
          }
          if (!oneUser) {
            req.log.error(moduleName + '@removeFolder - No User found with the same userID. Could not integrate folders.')
            return res.status(500).json({ msg: defaultErrMsg })
          }

          debug(`${moduleName} @removeFolder - User found from /gdrive/users.`)

          if (oneUser.status !== 'ACTIVE') {
            req.log.error(moduleName + '@removeFolder - User has not been activated. Current User status: ' + oneUser.status)
            return res.status(500).json({ msg: defaultErrMsg })
          }

          // find the account by the user.uid & authID
          Models.Gdrive.Consumer.findOne({
            uid: oneUser.uid,
            createdBy: authID,
            userEmail: email
          }, (err, oneConsumer) => {
            if (err) {
              req.log.error(moduleName + '@removeFolder - ERROR finding consumer from db.', { err })
              return res.status(500).json({ msg: defaultErrMsg })
            }
            if (!oneConsumer) {
              req.log.error(moduleName + '@removeFolder - No Consumer was found for the user.')
              return res.status(500).json({ msg: defaultErrMsg })
            }

            for (let i = 0; i < oneConsumer.selectedFolder.length; i++) {
              if (oneConsumer.selectedFolder[i] === folderID) {
                oneConsumer.selectedFolder.splice(i, 1)
              }
            }

            oneConsumer.save((err) => {
              if (err) {
                req.log.error(`${moduleName} @removeFolder - Consumer ${email}'s removing folder ${folderID} was not successful on db.`, { err })
                return res.status(500).json({ msg: defaultErrMsg })
              } else {
                debug('Consumer\'s removing folder save was successful.')
                debug(oneConsumer.selectedFolder)
                return res.status(200).json({ msg: 'success' })
              }
            })
          })
        })
      })
    } catch (err) {
      req.log.error(moduleName + '@removeFolder - Unknown error occured.', { err })
      return res.status(500).json({ msg: defaultErrMsg + ' Please try again or contact Auvenir Team.' })
    }
  })

  /**
   * retrieving files by selected folderID and download them.
   */
  router.post('/folderIntegration', isAuthenticated, function (req, res) {
    debug(`${moduleName} Folder Integration is triggered. `)
    let oauth2 = new google.auth.OAuth2(CLIENT_ID, CLIENT_SECRET, 'postmessage')
    let integratedFolders = {}
    let totalNumFiles = 0
    let failedFilesCounter = 0
    let successFilesCounter = 0
    let selectedGoogleDriveFolderIDs = []

    try {
      let form = new formidable.IncomingForm()

      form.parse(req, (err, fields) => {
        if (err) {
          req.log.error(moduleName + '@folderIntegration - ERROR: parsing req with form info.', { err })
          return res.status(500).json({ msg: defaultErrMsg })
        }
        if (!fields) {
          req.log.error(moduleName + '@folderIntegration - ERROR: No Fields passed through request.')
          return res.status(500).json({ msg: defaultErrMsg })
        }

        let { userID, authID, accessToken, email, uid, engagementID } = fields

        const validateFields = (cbk) => {
          req.log.info(`${moduleName} @fileUpload - validate fields.`)
          let expectedFields = ['userID', 'authID', 'engagementID', 'email', 'uid', 'accessToken']
          let receivedFileds = Object.keys(fields)
          let invalid = false
          expectedFields.forEach((field, i) => {
            function findMatch (element) {
              return element === field
            }

            if (!~(receivedFileds.findIndex(findMatch))) {
              invalid = true
            }
            if (i === expectedFields.length - 1) {
              if (invalid) {
                return cbk(`${moduleName} @fileUpload - ERROR: Invalid fields were passed.`)
              } else {
                return cbk(null)
              }
            }
          })
        }

        const checkGDUserExists = (cbk) => {
          req.log.info(`${moduleName} @fileUpload - checking GDUser & setting the oauth token.`)
          Models.Gdrive.User.findOne({ userID, uid }, (err, oneUser) => {
            if (err) {
              return cbk(`${moduleName} + '@folderIntegration - ERROR finding user from db. ${{err}}`)
            }
            if (!oneUser) {
              return cbk(`${moduleName} + '@folderIntegration - No User ${userID} found with the same userID.`)
            }

            req.log.info(`${moduleName} @folderIntegration - User found from /gdrive/users.`)

            if (oneUser.status !== 'ACTIVE') {
              return cbk(`${moduleName} + '@folderIntegration - User has not been activated. Current User status: ${oneUser.status}`)
            }

            // find the account by the user.uid & authID
            Models.Gdrive.Consumer.findOne({ uid: oneUser.uid, createdBy: authID, userEmail: email }, (err, oneConsumer) => {
              if (err) {
                return cbk(`${moduleName} '@folderIntegration - ERROR finding consumer from db. ${{err}}`)
              }
              if (!oneConsumer) {
                return cbk(`${moduleName} '@folderIntegration - No Consumer ${email} was found for the user.`)
              }

              if (oneConsumer.status !== 'ACTIVE') {
                return cbk(`${moduleName} '@folderIntegration - Consumer has not been activated. Current Consumer status: ${oneConsumer.status}`)
              }

              // set oauth client for each request
              oauth2.setCredentials({
                access_token: accessToken,
                refresh_token: oneConsumer.refreshToken
              })

              if (oneConsumer.tokenExpiry < new Date().getTime()) {
                debug(`TOKEN IS EXPIRED.`)
                renewToken(oauth2, (err, tokenResp) => {
                  if (err) {
                    return cbk(err)
                  }

                  oauth2.setCredentials(tokenResp)

                  oneConsumer.accessToken = tokenResp.access_token
                  oneConsumer.tokenExpiry = tokenResp.expiry_date
                  oneConsumer.id_token = tokenResp.id_token
                  oneConsumer.save((err) => {
                    if (err) {
                      req.log.error(`ERROR: while saving new access token. ${{err}}`)
                    }
                    return cbk(null, oneUser, oneConsumer)
                  })
                })
              }
            })
          })
        }

        const validateAccessToEngagement = (GDUser, GDConsumer, cbk) => {
          req.log.info(`${moduleName} @folderIntegration - validate access to Engagement.`)
          Access.engagement(engagementID, userID, (err, engagement) => {
            if (err) {
              return cbk(err)
            }
            return cbk(null, GDUser, GDConsumer, engagement)
          })
        }

        const importFilesInFolders = (GDUser, GDConsumer, engagement, cbk) => {
          for (let i = 0; i < GDConsumer.selectedFolder.length; i++) {
            selectedGoogleDriveFolderIDs.push(Utility.castToObjectId(GDConsumer.selectedFolder[i]))
          }
          Models.Gdrive.Item.find({ _id: { $in: selectedGoogleDriveFolderIDs } }, (err, folders) => {
            if (err) {
              return cbk(`${moduleName} @folderIntegration - ERROR finding folder by id.', {${err}}`)
            }
            if (!folders) {
              return cbk(`${moduleName} @folderIntegration - Parent Folders have not been saved in db.`)
            }

            let taskArray = []
            for (let i = 0; i < folders.length; i++) {
              taskArray.push({ gdriveID: folders[i].gdriveID, path: '/' })
            }

            // createate file path for all files
            async.whilst(
              function checkTaskArray () {
                return taskArray.length > 0
              },
              function getFilesFromGDrive (next) {
                // retrieve item information from google drive api
                const getFileInfomation = (callback) => {
                  getFileInfo(taskArray[0].gdriveID, GDConsumer.accessToken, (err, fileInfo) => {
                    if (err) {
                      return callback(err)
                    }
                    if (fileInfo) {
                      return callback(null, fileInfo, GDConsumer.accessToken)
                    }
                  })
                }

                // this function will check the type of item and either add it to the task array (for folder) or download the file.
                const mainFunctionToDownload = (fileInfo, accessToken, callback) => {
                  if (fileInfo.mimeType === 'application/vnd.google-apps.folder') {
                    getChildList(taskArray[0].gdriveID, accessToken, (err, childList) => {
                      if (err) {
                        req.log.error(moduleName + '@folderIntegration - ERROR retrieving childlist.', { err })
                        return callback({ msg: 'There was an error while retrieving information from Google Drive.' }, null)
                      }

                      for (let j = 0; j < childList.items.length; j++) {
                        let currentChildID = childList.items[j].id
                        let taskArrayObj = {
                          gdriveID: currentChildID,
                          path: taskArray[0].path + fileInfo.title + '/'
                        }
                        taskArray.push(taskArrayObj)
                      }
                      return callback(null, null)
                    })
                  } else {
                    totalNumFiles++

                    const validateFileType = (cbk) => {
                      debug(`Checking file type: ${fileInfo.mimeType}`)
                      Utility.validateMimeType(fileInfo.mimeType)
                        .then((type) => { // on resolve
                          var coreFile = new Models.Auvenir.File({
                            name: fileInfo.title,
                            size: fileInfo.fileSize ? fileInfo.fileSize : 'unknown',
                            path: '/',
                            owner: ObjectId(userID),
                            sortType: 'AUTO',
                            status: 'UPLOADING',
                            source: {
                              name: 'Google Drive',
                              uid: GDConsumer.uid
                            }
                          })

                          coreFile.save((err) => {
                            if (err) {
                              req.log.error(moduleName + ' ERROR: saving coreFile object.')
                              return cbk(err, coreFile)
                            }
                            return cbk(null, coreFile, type)
                          })
                        }, (reject) => { // on reject
                          return cbk(`Unsupported File Type: ${fileInfo.mimeType}`, null)
                        })
                    }

                    const receiveNewToken = (coreFile, type, cbk) => {
                      if (GDConsumer.tokenExpiry < new Date().getTime()) {
                        debug(`TOKEN IS EXPIRED.`)
                        renewToken(oauth2, (err, tokenResp) => {
                          if (err) {
                            return cbk(err, coreFile)
                          }

                          oauth2.setCredentials(tokenResp)

                          GDConsumer.accessToken = tokenResp.access_token
                          GDConsumer.tokenExpiry = tokenResp.expiry_date
                          GDConsumer.id_token = tokenResp.id_token
                          GDConsumer.save((err) => {
                            if (err) {
                              req.log.error('ERROR: while saving new access token.', { err })
                            }
                          })
                          return cbk(null, coreFile, type)
                        })
                      } else {
                        debug(`TOKEN IS NOT EXPIRED`)
                        return cbk(null, coreFile, type)
                      }
                    }

                    const downloadFile = (coreFile, type, cbk) => {
                      debug(`Downloading starts for file: ${fileInfo.id}`)
                      googleDrive.files.get({ fileId: fileInfo.id, auth: oauth2 }, (err, metadata) => {
                        if (err) {
                          debug(`There was an error while getting the metadata.`)
                          return cbk(err, coreFile)
                        }

                        req.log.info(`Downloading ${metadata.name}...`)

                        if (type === 'original') {
                          let option = {
                            mode: 'w',
                            content_type: metadata.mimeType,
                            filename: metadata.name
                          }

                          let dest = gfs.createWriteStream(option)
                          googleDrive.files.get({
                            auth: oauth2,
                            alt: 'media',
                            fileId: fileInfo.id
                          }).on('end', () => {
                            debug('Download from Google Drive Done')
                          }).on('error', (err) => {
                            req.log.error('THERE WAS AN ERROR WHILE GETTING FILE', { err })
                            return cbk(err, coreFile)
                          }).pipe(dest)

                          dest.on('close', (file) => {
                            debug(`Uploading to db is done.`)
                            return cbk(null, coreFile, file)
                          })
                          dest.on('error', (err) => {
                            req.log.error('Error writing file: ' + fileInfo.id, { err })
                            return cbk(err, coreFile)
                          })
                        } else {
                          let exportFileType = ''
                          let tempFileName = metadata.name
                          let tempNameArr = tempFileName.split('.')

                          if (metadata.mimeType === 'application/vnd.google-apps.document') {
                            var docExt = tempNameArr[tempNameArr.length - 1]
                            if (docExt === 'pdf') {
                              exportFileType = 'application/pdf'
                            } else if (docExt === 'txt') {
                              exportFileType = 'text/plain'
                            } else if (docExt === 'docx' || docExt === 'doc') {
                              exportFileType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                            } else if (docExt === 'xls' || docExt === 'xlsx') {
                              exportFileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                            } else if (docExt === 'csv') {
                              exportFileType = 'text/csv'
                            } else if (docExt === 'ppt' || docExt === 'pptx') {
                              exportFileType = 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
                            } else {
                              debug(`THIS FILE IS exported into PDF`)
                              tempFileName += '.pdf'
                              exportFileType = 'application/pdf'
                            }
                          } else if (metadata.mimeType === 'application/vnd.google-apps.presentation') {
                            if (!tempFileName.endsWith('.pptx')) {
                              tempFileName += '.pptx'
                            }
                            exportFileType = 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
                          } else if (metadata.mimeType === 'application/vnd.google-apps.spreadsheet') {
                            if (!tempFileName.endsWith('.xlsx')) {
                              tempFileName += '.xlsx'
                            }
                            exportFileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                          } else if (metadata.mimeType === 'application/vnd.google-apps.drawing') {
                            // currently just converting all drawings into pdf.
                            tempFileName += '.pdf'
                            exportFileType = 'application/pdf'
                          } else if (metadata.mimeType === 'application/vnd.google-apps.photo') {
                            var picExt = tempNameArr[tempNameArr.length - 1]
                            if (picExt === 'jpeg' || picExt === 'jpg') {
                              exportFileType = 'image/jpeg'
                            } else if (picExt === 'png') {
                              exportFileType = 'image/png'
                            } else {
                              debug(`INVALID Image extension: ${picExt} / mimeType: ${metadata.mimeType}`)
                              return cbk('INVALID Image extension: ' + picExt + ' / mimeType: ' + metadata.mimeType, coreFile)
                            }
                          } else {
                            debug(`INVALID FILE TYPE: ${metadata.mimeType}`)
                            return cbk('Not a valid file type: ' + metadata.mimeType, coreFile)
                          }

                          debug(`EXPORT FILE TYPE: ${exportFileType}`)

                          let option = {
                            mode: 'w',
                            content_type: exportFileType,
                            filename: tempFileName // metadata.name
                          }
                          let dest = gfs.createWriteStream(option)

                          googleDrive.files.export({
                            auth: oauth2,
                            fileId: fileInfo.id,
                            mimeType: exportFileType
                          }).on('error', (err) => {
                            req.log.error('ERROR: exporting file.')
                            return cbk(err, coreFile)
                          }).pipe(dest)

                          dest.on('close', (file) => {
                            return cbk(null, coreFile, file)
                          }).on('error', (err) => {
                            req.log.error('Error writing file: ' + fileInfo.id, { err })
                            return cbk(err, coreFile)
                          })
                        }
                      })
                    }

                    const saveGDriveObj = (coreFile, savedFile, cbk) => {
                      let gdriveFile = new Models.Gdrive.Item()
                      gdriveFile.consumerID = Utility.castToObjectId(GDConsumer._id)
                      gdriveFile.gdriveID = fileInfo.id
                      gdriveFile.parent = taskArray[0].gdriveID
                      gdriveFile.type = 'FILE'
                      gdriveFile.fileID = Utility.castToObjectId(savedFile._id)

                      gdriveFile.save((err) => {
                        if (err) {
                          req.log.error(moduleName + '@folderIntergration - There was an error while saving the file into gdriveFile: ' + JSON.parse(gdriveFile), { err })

                          gfs.remove({ _id: savedFile._id }, (err) => {
                            if (err) {
                              req.log.error(moduleName + '@folderIntegration - ERROR deleting gfs file that had error while creating gdriveObj. gfs file: ' + JSON.parse(savedFile), { err })
                              return cbk(err, coreFile)
                            }
                            debug(`removed gfs file: ${JSON.parse(savedFile)}`)
                          })

                          return cbk(err, coreFile)
                        } else {
                          debug(`GDRIVE object is saved.`)
                          return cbk(null, coreFile, gdriveFile, savedFile)
                        }
                      })
                    }

                    const saveCoreFile = (coreFile, gdriveFile, savedFile, cbk) => {
                      debug(`Saving Corefile info.`)
                      coreFile.size = savedFile.chunkSize
                      coreFile.path = taskArray[0].path
                      coreFile.status = 'ACTIVE'
                      coreFile.source.fid = Utility.castToObjectId(gdriveFile._id)
                      coreFile.save((err) => {
                        return cbk(err, coreFile)
                      })
                    }

                    const saveInCoreDB = (coreFile, cbk) => {
                      engagement.files.push(coreFile._id)
                      engagement.save((err) => {
                        if (err) {
                          req.log.error(`${moduleName} @folderIntegration - ERROR saving file id(${coreFile._id}) into engagement(${engagementID}).files.`, { err })
                          return cbk(err, coreFile)
                        }
                        return cbk(null, coreFile)
                      })
                    }

                    async.waterfall([
                      validateFileType,
                      receiveNewToken,
                      downloadFile,
                      saveGDriveObj,
                      saveCoreFile,
                      saveInCoreDB
                    ], (err, coreFile) => {
                      if (err) {
                        req.log.error({ err })
                        return callback(err, coreFile)
                      } else {
                        debug(`SAVED fs.files : ${JSON.stringify(coreFile.name)}`)
                        callback(null, coreFile)
                      }
                    })
                  } // else if (type === file)
                } // end of mainFunctionToDownload

                async.waterfall([getFileInfomation, mainFunctionToDownload], (asyncERR, coreFile) => {
                  if (asyncERR) {
                    failedFilesCounter++
                    req.log.error(moduleName + '@folderIntegration - ERROR retrieving file info & saving the file.')
                    if (coreFile) {
                      coreFile.status = 'ERROR'
                      coreFile.save((err) => {
                        if (err) {
                          req.log.error(moduleName + '@folderIntegration - ERROR saving coreFile.status to ERROR. ', { err })
                        }
                      })
                    }
                  } else {
                    // The only time the coreFile is null is when the gdriveItem is folder.
                    if (coreFile) {
                      successFilesCounter++
                      coreFile.save((err) => {
                        if (err) {
                          req.log.error(moduleName + '@folderIntegration - ERROR saving coreFile.status to ACTIVE', { err })
                        } else {
                          var rootFolderName = coreFile.path.split('/')[1]
                          if (integratedFolders[rootFolderName]) {
                            integratedFolders[rootFolderName].push(coreFile._id)
                          } else {
                            integratedFolders[rootFolderName] = [coreFile._id]
                          }

                          Utility.createActivity(userID, engagementID, 'UPLOAD', 'File', {}, coreFile.toObject(), (err) => {
                            if (err) {
                              req.log.error('Error on creating Activity', { err })
                            }
                          })
                        }
                      })
                    }
                  }

                  taskArray.splice(0, 1)
                  next()
                })
              }, function whilstResultHandler (err) {
                if (err) {
                  return cbk(err)
                }
                req.log.info(`Successful Files: ${successFilesCounter}`)
                req.log.info(`Failed Files: ${failedFilesCounter}`)
                req.log.info(`Total Number of Files: ${totalNumFiles}`)
                req.log.info(`GOOGLE INTEGRATION SETUP WAS SUCCESSFUL`)

                let folderNames = Object.keys(integratedFolders)
                folderNames.forEach((folder, i) => {
                  let updated = {
                    folderName: folder,
                    files: integratedFolders[folder],
                    integrationType: 'GDRIVE'
                  }
                  Utility.createActivity(userID, engagementID, 'CREATE', 'Integration', {}, updated, (err) => {
                    if (err) {
                      req.log.error('Error on creating Activity', { err })
                    }
                  })
                })

                if (successFilesCounter === totalNumFiles) {
                  return cbk(null, { code: 0, msg: 'All files in ' + email + ' Google Drive were successfully uploaded.' })
                } else {
                  return cbk(null, { code: 1, msg: 'File upload for ' + email + ' account completed. Unable to upload ' + failedFilesCounter + ' of the ' + totalNumFiles + ' files in Google Drive.' })
                }
              })
          })
        }
        async.waterfall([validateFields, checkGDUserExists, validateAccessToEngagement, importFilesInFolders], (err, result) => {
          if (err) {
            return res.status(500).json({ msg: 'There was an error while retrieving files from Google Drive. Please try again.' })
          }
          return res.status(200).json(result)
        })
      })
    } catch (err) {
      req.log.error(moduleName + '@folderIntegration - Unknown error occured.', { err })
      return res.status(500).json({ msg: defaultErrMsg + ' Please try again or contact Auvenir Team.' })
    }
  })

  /**
   *  This function is for uploading single file to an engagement.
   */
  router.post('/fileUpload', isAuthenticated, function (req, res) {
    debug(`${moduleName} /fileUpload is triggered.`)
    let oauth2 = new google.auth.OAuth2(CLIENT_ID, CLIENT_SECRET, 'postmessage')

    try {
      let form = new formidable.IncomingForm()
      form.parse(req, (err, fields) => {
        if (err) {
          req.log.error(moduleName + '@fileUpload - ERROR: parsing req with form info.')
          req.log.error(err)
          return res.status(500).json({ msg: defaultFileErrMsg })
        }
        if (!fields) {
          req.log.error(moduleName + '@fileUpload - ERROR: No Fields passed through request.')
          return res.status(500).json({ msg: defaultFileErrMsg })
        }

        let { gdriveID, userID, authID, email, uid, engagementID, requestID, todoID } = fields

        const validateFields = (cbk) => {
          req.log.info(`${moduleName} @fileUpload - validate fields.`)
          let expectedFields = ['gdriveID', 'userID', 'authID', 'engagementID', 'email', 'uid', 'accessToken'] // 'requestID', 'todoID'
          let receivedFileds = Object.keys(fields)
          let invalid = false
          expectedFields.forEach((field, i) => {
            if (!~_.findIndex(receivedFileds, (receivedField) => { return field === receivedField })) {
              invalid = true
            }
            if (i === expectedFields.length - 1) {
              if (invalid) {
                return cbk(`${moduleName} @fileUpload - ERROR: Invalid fields were passed.`)
              } else {
                return cbk(null)
              }
            }
          })
        }

        const checkGDUserExists = (cbk) => {
          req.log.info(`${moduleName} @fileUpload - check Google Drive access.`)
          Models.Gdrive.User.findOne({ userID, uid }, (err, oneUser) => {
            if (err) {
              return cbk(`${moduleName} + '@fileUpload - ERROR finding user from db. ${{err}}`)
            }
            if (!oneUser) {
              return cbk(`${moduleName} + '@fileUpload - No User ${userID} found with the same userID.`)
            }

            req.log.info(`${moduleName} @fileUpload - User found from /gdrive/users.`)
            Models.Gdrive.Consumer.findOne({ uid: oneUser.uid, createdBy: authID, userEmail: email }, (err, oneConsumer) => {
              if (err) {
                return cbk(`${moduleName} '@fileUpload - ERROR finding consumer from db. ${{err}}`)
              }
              if (!oneConsumer) {
                return cbk(`${moduleName} '@fileUpload - No Consumer ${email} was found for the user.`)
              }

              if (oneConsumer.status === 'ACTIVE' || oneConsumer.status === 'SETUP') {
                // set oauth client for each request
                oauth2.setCredentials({
                  access_token: oneConsumer.accessToken,
                  refresh_token: oneConsumer.refreshToken
                })

                if (oneConsumer.tokenExpiry < new Date().getTime()) {
                  debug(`TOKEN IS EXPIRED.`)
                  renewToken(oauth2, (err, tokenResp) => {
                    if (err) {
                      return cbk(err)
                    }

                    oauth2.setCredentials(tokenResp)

                    oneConsumer.accessToken = tokenResp.access_token
                    oneConsumer.tokenExpiry = tokenResp.expiry_date
                    oneConsumer.id_token = tokenResp.id_token
                    oneConsumer.save((err) => {
                      if (err) {
                        req.log.error(`ERROR: while saving new access token. ${{err}}`)
                      }
                      return cbk(null, oneUser, oneConsumer)
                    })
                  })
                }
              } else {
                return cbk(`${moduleName} '@fileUpload - This consumer(${oneConsumer})'s status is INACTIVE.`)
              }
            })
          })
        }

        const validateAccessToEngagement = (GDUser, GDConsumer, cbk) => {
          req.log.info(`${moduleName} @fileUpload - validate access to Engagement.`)
          if (todoID && requestID) {
            Access.request(requestID, todoID, engagementID, userID, (err, engagement) => {
              if (err) {
                return cbk(err)
              }
              return cbk(null, GDUser, GDConsumer, engagement)
            })
          } else { // when only engagementID was passed.
            Access.engagement(engagementID, userID, (err, engagement) => {
              if (err) {
                return cbk(err)
              }
              return cbk(null, GDUser, GDConsumer, engagement)
            })
          }
        }

        const getFileInformation = (GDUser, GDConsumer, engagement, cbk) => {
          getFileInfo(gdriveID, GDConsumer.accessToken, (err, fileInfo) => {
            if (err) {
              return cbk(err)
            }
            return cbk(null, fileInfo, GDConsumer, engagement)
          })
        }

        const checkFileType = (fileInfo, GDConsumer, engagement, cbk) => {
          debug(`Checking file type: ${fileInfo.mimeType}`)
          Utility.validateMimeType(fileInfo.mimeType).then((type) => { // on resolve
            // type: 'original' or 'google'
            let coreFile = new Models.Auvenir.File({
              name: fileInfo.title,
              size: fileInfo.fileSize ? fileInfo.fileSize : 'unknown',
              path: '/',
              sortType: 'AUTO',
              status: 'UPLOADING',
              owner: ObjectId(userID),
              source: {
                name: 'Google Drive',
                uid: GDConsumer.uid
              }
            })

            coreFile.save((err) => {
              if (err) {
                req.log.error(moduleName + ' ERROR: saving coreFile object.')
              }
              return cbk(err, coreFile, fileInfo, GDConsumer, engagement, type)
            })
          }, (reject) => { // on reject
            return cbk(`Unsupported File Type: ${fileInfo.mimeType}`)
          })
        }

        const downloadGDFile = (coreFile, fileInfo, GDConsumer, engagement, type, cbk) => {
          debug(`Downloading file from Google Drive. File: ${fileInfo.title}`)
          googleDrive.files.get({ fileId: fileInfo.id, auth: oauth2 }, (err, metadata) => {
            if (err) {
              debug(`There was an error while getting the metadata.`)
              return cbk(err, { coreFile })
            }

            if (type === 'original') {
              let option = {
                mode: 'w',
                content_type: metadata.mimeType,
                filename: metadata.name
              }

              let dest = gfs.createWriteStream(option)
              googleDrive.files.get({
                auth: oauth2,
                alt: 'media',
                fileId: fileInfo.id
              }).on('end', () => {
                debug('Download from Google Drive Done')
              }).on('error', (err) => {
                req.log.error('THERE WAS AN ERROR WHILE GETTING FILE', { err })
                return cbk(err, { coreFile })
              }).pipe(dest)

              dest.on('close', (file) => {
                req.log.info(`Uploading to db is done.`)
                return cbk(null, coreFile, file, GDConsumer, engagement)
              })
              dest.on('error', (err) => {
                req.log.error('Error writing file: ' + fileInfo.id, { err })
                return cbk(err, { coreFile })
              })
            } else if (type === 'google') {
              let exportFileType = ''
              let tempFileName = metadata.name
              let tempNameArr = tempFileName.split('.')

              if (metadata.mimeType === 'application/vnd.google-apps.document') {
                var docExt = tempNameArr[tempNameArr.length - 1]
                if (docExt === 'pdf') {
                  exportFileType = 'application/pdf'
                } else if (docExt === 'txt') {
                  exportFileType = 'text/plain'
                } else if (docExt === 'docx' || docExt === 'doc') {
                  exportFileType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                } else if (docExt === 'xls' || docExt === 'xlsx') {
                  exportFileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                } else if (docExt === 'csv') {
                  exportFileType = 'text/csv'
                } else if (docExt === 'ppt' || docExt === 'pptx') {
                  exportFileType = 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
                } else {
                  debug(`THIS FILE IS exported into PDF`)
                  tempFileName += '.pdf'
                  exportFileType = 'application/pdf'
                }
              } else if (metadata.mimeType === 'application/vnd.google-apps.presentation') {
                exportFileType = 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
                tempFileName += '.pptx'
              } else if (metadata.mimeType === 'application/vnd.google-apps.spreadsheet') {
                tempFileName += '.xlsx'
                exportFileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
              } else if (metadata.mimeType === 'application/vnd.google-apps.drawing') {
                // currently just converting all drawings into pdf.
                exportFileType = 'application/pdf'
              } else if (metadata.mimeType === 'application/vnd.google-apps.photo') {
                var picExt = tempNameArr[tempNameArr.length - 1]
                if (picExt === 'jpeg' || picExt === 'jpg') {
                  exportFileType = 'image/jpeg'
                } else if (picExt === 'png') {
                  exportFileType = 'image/png'
                } else {
                  debug(`INVALID Image extension: ${picExt} / mimeType: ${metadata.mimeType}`)
                  return cbk('INVALID Image extension: ' + picExt + ' / mimeType: ' + metadata.mimeType, coreFile)
                }
              } else {
                debug(`INVALID FILE TYPE: ${metadata.mimeType}`)
                return cbk('Not a valid file type: ' + metadata.mimeType, { coreFile })
              }

              debug(`EXPORT FILE TYPE: ${exportFileType} >>> FILE NAME: ${tempFileName}`)

              let option = {
                mode: 'w',
                content_type: exportFileType,
                filename: tempFileName
              }
              let dest = gfs.createWriteStream(option)

              googleDrive.files.export({
                auth: oauth2,
                fileId: fileInfo.id,
                mimeType: exportFileType
              }).on('error', (err) => {
                req.log.error('ERROR: exporting file.')
                return cbk(err, { coreFile })
              }).pipe(dest)

              dest.on('close', (file) => {
                return cbk(null, coreFile, file, GDConsumer, engagement)
              }).on('error', (err) => {
                req.log.error('Error writing file: ' + fileInfo.id, { err })
                return cbk(err, { coreFile })
              })
            }
          })
        }

        const saveGDriveItem = (coreFile, savedFile, GDConsumer, engagement, cbk) => {
          req.log.info(`Saving Google Drive Item in GD database.`)
          let gdriveFile = new Models.Gdrive.Item()
          gdriveFile.consumerID = Utility.castToObjectId(GDConsumer._id)
          gdriveFile.gdriveID = gdriveID
          gdriveFile.parent = null
          gdriveFile.type = 'FILE'
          gdriveFile.fileID = Utility.castToObjectId(savedFile._id)

          gdriveFile.save((err) => {
            if (err) {
              req.log.error(moduleName + '@fileUpload - There was an error while saving the file into gdriveFile: ' + JSON.parse(gdriveFile), { err })

              gfs.remove({ _id: savedFile._id }, (err) => {
                if (err) {
                  req.log.error(moduleName + '@fileUpload - ERROR deleting gfs file that had error while creating gdriveObj. gfs file: ' + JSON.parse(savedFile), { err })
                  return cbk(err, { coreFile })
                }
                debug(`removed gfs file: ${JSON.parse(savedFile)}`)
              })
              return cbk(err, { coreFile })
            } else {
              debug(`GDrive Item ${gdriveFile._id} is saved.`)
              return cbk(null, coreFile, savedFile, gdriveFile, engagement)
            }
          })
        }

        const updateCoreFile = (coreFile, savedFile, gdriveFile, engagement, cbk) => {
          req.log.info(`Updating CoreFile info.`)
          coreFile.size = savedFile.chunkSize
          coreFile.name = savedFile.filename
          coreFile.status = 'ACTIVE'
          coreFile.source.fid = Utility.castToObjectId(gdriveFile._id)
          coreFile.save((err) => {
            if (err) {
              return cbk(err, { coreFile })
            } else {
              return cbk(null, coreFile, engagement)
            }
          })
        }

        const savingInEngagement = (coreFile, engagement, cbk) => {
          req.log.info('Update the files array in engagement.')

          const addToEngagement = (cbk) => {
            // save in engagement.files
            engagement.files.push(coreFile._id)
            engagement.save((err) => {
              if (err) {
                return cbk(err, {coreFile})
              } else {
                return cbk(null, {coreFile, engagement})
              }
            })
          }

          const addToRequest = (cbk) => {
            // save in request.currentFiles[]
            engagement.todos.forEach((todo) => {
              if (todo.requests.length > 0) {
                todo.requests.forEach((request) => {
                  if (request._id.toString() === requestID.toString()) {
                    request.previousFiles.push(request.currentfile)
                    request.currentFile = coreFile._id
                    engagement.save((err) => {
                      if (err) {
                        return cbk(err, {coreFile})
                      } else {
                        return cbk(null, {coreFile, engagement})
                      }
                    })
                  }
                })
              }
            })
          }

          if (requestID) {
            async.parallel([addToEngagement, addToRequest], (err, result) => {
              error(result[1])
              cbk(err, result[1])
            })
          } else {
            addToEngagement(cbk)
          }
        }

        async.waterfall([
          validateFields,
          checkGDUserExists,
          validateAccessToEngagement,
          getFileInformation,
          checkFileType,
          downloadGDFile,
          saveGDriveItem,
          updateCoreFile,
          savingInEngagement
        ], (asyncERR, result) => {
          let { coreFile, engagement } = result
          if (asyncERR) {
            req.log.error(asyncERR)
            if (coreFile) {
              coreFile.status = 'ERROR'
              coreFile.save((err) => {
                if (err) {
                  req.log.error(moduleName + '@fileUpload - ERROR saving coreFile.status to ERROR. ', { err })
                }
                res.status(500).json({ msg: defaultFileErrMsg })
              })
            }
          } else {
            engagement.fetchAdditionalInfo((fetchErr, updatedEngagement) => {
              if (fetchErr) {
                req.log.error('There was a fetch engagement info error.', {fetchErr})
              }
              return res.status(200).json({file: coreFile, requestID: requestID, engagement: updatedEngagement || engagement, msg: `Your file ${coreFile.name} was successfully uploaded.`})
            })
          }
        })
      })
    } catch (err) {
      req.log.error(moduleName + '@fileUpload - Unknown error occured.')
      req.log.error(err)
      res.status(500).json({msg: 'There was an error while uploading your file.'})
    }
  })

  /**
   * TODO - Validation for files & engagement is needed.
   * Download File for Client (triggered by File page)
   */
  router.get('/downloadFile/:fileID/:fileName', isAuthenticated, function (req, res) {
    if (req.params.fileID) {
      var fileID = Utility.castToObjectId(req.params.fileID)
    } else {
      res.status(500).json({ msg: 'No File was found.' })
    }

    var errorMsg = 'Error downloading the file. Please contact Auvenir Team.'
    debug(fileID)

    function getGoogleFileObject (cb) {
      debug('Getting Google File object.')
      Models.Gdrive.Item.findOne({ _id: fileID }, function (err, gdrive) {
        if (err) {
          req.log.error('ERROR: Finding Google Drive File Object', { err: err })
          cb({ code: 500, msg: errorMsg })
        } else {
          if (!gdrive) {
            req.log.error('ERROR: Finding Google Drive File Object')
            cb({ code: 500, msg: errorMsg })
          } else {
            cb(null, gdrive.fileID)
          }
        }
      })
    }

    function checkFileExists (fid, cb) {
      debug('Checking file Chunk exists.')
      var query = { root: 'fs', _id: fid }
      gfs.findOne(query, function (err, gdriveChunk) {
        if (err) {
          req.log.error('ERROR: Finding Google Drive File Chunks', { err })
          cb({ code: 500, msg: errorMsg })
        } else {
          if (!gdriveChunk) {
            req.log.error('ERROR: Finding Google Drive File Chunk')
            cb({ code: 500, msg: errorMsg })
          } else {
            var encrypted = gdriveChunk.metadata ? gdriveChunk.metadata.encrypted : false
            cb(null, gdriveChunk, encrypted, query)
          }
        }
      })
    }

    function downloadFile (err, file, encrypted, query) {
      if (err) {
        return res.status(err.code).json({ msg: err.msg })
      } else {
        if (req.query.option && req.query.option === 'view') {
          res.set('Content-Type', file.contentType)
        } else {
          res.attachment(file.filename)
        }
        var gridFSStream = gfs.createReadStream(query)
        gridFSStream.on('error', function (err) {
          var callback
          if (callback) {
            callback(err)
          }
        })

        if (encrypted) {
          return gridFSStream.pipe(crypto.createDecipher(encryptAlgorithm, encryptPassword)).pipe(res)
        } else {
          return gridFSStream.pipe(res)
        }
      }
    }

    async.waterfall([getGoogleFileObject, checkFileExists], downloadFile)
  })

  /**
   * disconnect the consumer
   * This function triggered when user click trash icon.
   */
  router.post('/disconnect', isAuthenticated, function (req, res) {
    debug(`${moduleName} Disconnect the consumer is triggered.`)
    let disconnectErrMsg = 'There was an error while removing your Google Drive integration.'
    try {
      let form = new formidable.IncomingForm()
      form.parse(req, (err, field) => {
        if (err) {
          req.log.error(moduleName + '@disconnect - ERROR: parsing fields in the req.', { err })
          return res.status(500).json({ code: 1, msg: disconnectErrMsg })
        }
        if (!field) {
          req.log.error(moduleName + '@disconnect - ERROR: No Fields passed through request.')
          return res.status(500).json({ code: 1, msg: disconnectErrMsg })
        }
        if (!field.authID || !_.isString(field.authID)) {
          req.log.error(moduleName + '@disconnect - ERROR: Invalid authID: ' + field.authID)
          return res.status(500).json({ code: 1, msg: disconnectErrMsg })
        }
        if (!field.uid || !_.isString(field.uid)) {
          req.log.error(moduleName + '@disconnect - ERROR: Invalid uid: ' + field.uid)
          return res.status(500).json({ code: 1, msg: disconnectErrMsg })
        }
        if (!field.email || !_.isString(field.email)) {
          req.log.error(moduleName + '@disconnect - ERROR: Invalid email: ' + field.email)
          return res.status(500).json({ code: 1, msg: disconnectErrMsg })
        }

        Models.Gdrive.Consumer.findOne({
          uid: field.uid,
          createdBy: field.authID,
          userEmail: field.email
        }, (err, oneConsumer) => {
          if (err) {
            req.log.error(moduleName + '@disconnect - ERROR: while finding the consumer from db.', { err })
            return res.status(500).json({ code: 1, msg: disconnectErrMsg })
          }
          if (!oneConsumer) {
            req.log.error(moduleName + '@disconnect - Could not find the consumer from db.')
            return res.status(500).json({ code: 1, msg: disconnectErrMsg })
          }

          debug('Found consumer. Now looking for files and inactivating them.')
          Models.Gdrive.Item.find({
            consumerID: Utility.castToObjectId(oneConsumer._id),
            type: 'FILE'
          }, (err, files) => {
            if (err) {
              req.log.error(`${moduleName} @disconnect - ERROR while getting files to disconnect in db.`, { err })
              return res.status(500).json({ msg: 'There was an error while removing your integration for ' + oneConsumer.userEmail })
            }
            if (!files) {
              debug(`${moduleName} @disconnect - There was no gdrive files uploaded by this user account.`)
              debug('Revoking the token.')

              revokeToken(oneConsumer, req, res)
              return
            }

            debug(`${files.length} files found.`)
            debug(files)

            let numOfGDriveFiles = files.length
            let i = 0

            if (numOfGDriveFiles > 0) {
              async.whilst(
                function checkFiles () {
                  return (i < numOfGDriveFiles)
                },
                function modifyFiles (next) {
                  var query = { 'source.fid': Utility.castToObjectId(files[i]._id) }
                  Models.Auvenir.File.findOne(query, (err, fileObj) => {
                    if (err) {
                      req.log.error(`${moduleName} @disconnect - ERROR while finding fileObj from Auvenir Core DB.`, { err })
                      i = numOfGDriveFiles
                      next()
                    }
                    if (!fileObj) {
                      req.log.error(`${moduleName} @disconnect - gdriveObj { _id: ${files[i]._id}} exists but the actual file doesn't exist. The file is either deleted from Auvenir Core DB or there must have an error while downloading this file before.`)
                    } else {
                      debug(`${fileObj.name}`)
                      fileObj.status = 'INACTIVE'
                      fileObj.save((err) => {
                        if (err) {
                          req.log.error(`${moduleName} @disconnect - There was an error while saving fileObj { _id: ${files[i]._id}} to INACTIVE`, { err })
                          i = numOfGDriveFiles
                          next()
                        } else {
                          debug(`file { _id: ${files[i]._id}} set to INACTIVE successfully`)
                          i++
                          next()
                        }
                      })
                    }
                  })
                },
                function whilstFilesHandler (err) {
                  if (err) {
                    return res.status(500).json({ msg: 'There was an error while removing your integration for ' + oneConsumer.userEmail })
                  }
                  debug(oneConsumer.refreshToken)
                  debug(oneConsumer.accessToken)
                  revokeToken(oneConsumer, req, res)
                }
              )
            } else {
              revokeToken(oneConsumer, req, res)
            }
          })
        })
      })
    } catch (err) {
      req.log.error(moduleName + '@disconnect - Unknown error occured.', { err })
      return res.status(500).json({ msg: 'There was an error while disconnecting with Google Drive.' })
    }
  })

  router.post('/getAllConsumers', isAuthenticated, function (req, res) {
    debug(`${moduleName} getAllConsumers are triggered.`)
    let defaultErrMsg = 'There was an error while getting your integration data. '

    try {
      let form = new formidable.IncomingForm()
      form.parse(req, (err, field) => {
        if (err) {
          req.log.error(moduleName + '@getAllConsumers - ERROR: parsing fields in the req.', { err })
          return res.status(500).json({ code: 1, msg: defaultErrMsg })
        }
        if (!field) {
          req.log.error(moduleName + '@getAllConsumers - ERROR: No Fields passed through request.')
          return res.status(500).json({ code: 1, msg: defaultErrMsg })
        }
        if (!field.authID || !_.isString(field.authID)) {
          req.log.error(moduleName + '@getAllConsumers - ERROR: Invalid authID: ' + field.authID)
          return res.status(500).json({ code: 1, msg: defaultErrMsg })
        }
        if (!field.uid || !_.isString(field.uid)) {
          req.log.error(moduleName + '@getAllConsumers - ERROR: Invalid uid: ' + field.uid)
          return res.status(500).json({ code: 1, msg: defaultErrMsg })
        }
        if (!field.userID || !_.isString(field.userID)) {
          req.log.error(moduleName + '@getAllConsumers - ERROR: Invalid userID: ' + field.userID)
          return res.status(500).json({ code: 1, msg: defaultErrMsg })
        }

        let { uid, userID, authID } = field

        Models.Gdrive.User.findOne({ uid, userID }, (err, oneUser) => {
          if (err) {
            req.log.error(`${moduleName} @getAllConsumers - ERROR while finding user.`, { err })
            return res.status(500).json({ msg: defaultErrMsg })
          }
          if (!oneUser) {
            req.log.error(`${moduleName} @getAllConsumers - No User was found.`)
            return res.status(500).json({ msg: defaultErrMsg })
          }

          Models.Gdrive.Consumer.find({
            uid,
            createdBy: authID,
            status: { $in: ['ACTIVE', 'SETUP'] }
          }, (err, consumers) => {
            if (err) {
              req.log.error(`${moduleName} @getAllConsumers - ERROR while finding consumer.`, { err })
              return res.status(500).json({ msg: defaultErrMsg })
            }
            if (!consumers || consumers.length === 0) {
              req.log.error(`${moduleName} @getAllConsumers - No Consumer was found.`)
              return res.status(200).json({ msg: 'NOTFOUND' })
            }

            debug('consumers found!')

            let consumerStr = ''

            if (consumers.length > 1) {
              for (let i = 0; i < consumers.length; i++) {
                consumerStr += consumers[i].userEmail
                if (i !== consumers.length - 1) {
                  consumerStr += ','
                }
              }
            } else {
              consumerStr = consumers[0].userEmail
            }

            debug(consumerStr)
            return res.status(200).json({ consumers: consumerStr })
          })
        })
      })
    } catch (err) {
      req.log.error(moduleName + '@getAllConsumers - Unknown error occured.', { err })
      return res.status(500).json({ msg: defaultErrMsg })
    }
  })

  router.post('/getFolderNames', isAuthenticated, function (req, res) {
    debug(`${moduleName} getFolderNames are triggered.`)
    let defaultErrMsg = 'There was an error while getting your integration data. '
    let oauth2 = new google.auth.OAuth2(CLIENT_ID, CLIENT_SECRET, 'postmessage')

    try {
      let form = new formidable.IncomingForm()
      form.parse(req, (err, field) => {
        if (err) {
          req.log.error(moduleName + '@getFolderNames - ERROR: parsing fields in the req.', { err })
          return res.status(500).json({ code: 1, msg: defaultErrMsg })
        }
        if (!field) {
          req.log.error(moduleName + '@getFolderNames - ERROR: No Fields passed through request.')
          return res.status(500).json({ code: 1, msg: defaultErrMsg })
        }
        if (!field.authID || !_.isString(field.authID)) {
          req.log.error(moduleName + '@getFolderNames - ERROR: Invalid authID: ' + field.authID)
          return res.status(500).json({ code: 1, msg: defaultErrMsg })
        }
        if (!field.uid || !_.isString(field.uid)) {
          req.log.error(moduleName + '@getFolderNames - ERROR: Invalid uid: ' + field.uid)
          return res.status(500).json({ code: 1, msg: defaultErrMsg })
        }
        if (!field.userID || !_.isString(field.userID)) {
          req.log.error(moduleName + '@getFolderNames - ERROR: Invalid userID: ' + field.userID)
          return res.status(500).json({ code: 1, msg: defaultErrMsg })
        }
        if (!field.email || !_.isString(field.email)) {
          req.log.error(moduleName + '@getFolderNames - ERROR: Invalid email: ' + field.email)
          return res.status(500).json({ code: 1, msg: defaultErrMsg })
        }

        let { uid, userID, authID, email } = field

        Models.Gdrive.User.findOne({ uid, userID }, (err, oneUser) => {
          if (err) {
            req.log.error(`${moduleName} @getFolderNames - ERROR while finding user.`, { err })
            return res.status(500).json({ msg: defaultErrMsg })
          }
          if (!oneUser) {
            req.log.error(`${moduleName} @getFolderNames - No User was found.`)
            return res.status(500).json({ msg: defaultErrMsg })
          }

          Models.Gdrive.Consumer.findOne({
            uid,
            createdBy: authID,
            userEmail: email,
            status: { $in: ['ACTIVE', 'SETUP'] }
          }, (err, consumer) => {
            if (err) {
              req.log.error(`${moduleName} @getFolderNames - ERROR while finding consumer.`, { err })
              return res.status(500).json({ msg: defaultErrMsg })
            }
            if (!consumer) {
              req.log.error(`${moduleName} @getFolderNames - No Consumer was found.`)
              return res.status(500).json({ msg: defaultErrMsg })
            }

            debug(`consumer ${email} found!`)
            debug(`Folders: ${consumer.selectedFolder}`)

            oauth2.setCredentials({
              access_token: consumer.accessToken,
              refresh_token: consumer.refreshToken
            })

            var folderStr = ''
            var folderIDStr = ''
            var i = 0 // counter

            function checkToken (cbk) {
              if (consumer.tokenExpiry < new Date().getTime()) {
                renewToken(oauth2, (err, tokenResp) => {
                  if (err) {
                    return cbk(err)
                  }

                  oauth2.setCredentials(tokenResp)

                  consumer.accessToken = tokenResp.access_token
                  consumer.tokenExpiry = tokenResp.expiry_date
                  consumer.save((err) => {
                    if (err) {
                      req.log.error('ERROR: while saving new access token.', { err })
                    }
                    return cbk(null, tokenResp.access_token)
                  })
                })
              } else {
                return cbk(null, consumer.accessToken)
              }
            }

            function getFolderInfo (accessToken, cbk) {
              Models.Gdrive.Item.findOne({ _id: Utility.castToObjectId(consumer.selectedFolder[i]) }, (err, folder) => {
                if (err) {
                  req.log.error(`${moduleName} @getFolderNames - ERROR getting gdriveObj of the folder ${consumer.selectedFolder[i]}`, { err })
                  return cbk(err)
                }
                if (!folder) {
                  req.log.error(`${moduleName} @getFolderNames - No gdriveObj was found with _id of ${consumer.selectedFolder[i]}.`)
                  return cbk(`No gdriveObj was found with _id of ${consumer.selectedFolder[i]}`)
                }
                getFileInfo(folder.gdriveID, accessToken, (err, fileInfo) => {
                  if (err) {
                    return cbk(err)
                  }
                  if (!fileInfo) {
                    return cbk(`No file/folder was found for gdriveID: ${folder.gdriveID}`)
                  }

                  return cbk(null, fileInfo)
                })
              })
            }

            async.whilst(
              function checkFolderArray () {
                return (consumer.selectedFolder.length > 0) && (i < consumer.selectedFolder.length)
              },
              function getFilesFromGDrive (next) {
                async.waterfall([checkToken, getFolderInfo], function waterfallResultHandler (err, folderInfo) {
                  if (err) {
                    req.log.error({ err })
                  }
                  if (folderInfo) {
                    debug('handling folderInfo.')
                    debug(folderInfo)

                    folderStr += folderInfo.title
                    folderIDStr += folderInfo.id
                    if (i !== consumer.selectedFolder.length - 1) {
                      folderStr += ','
                      folderIDStr += ','
                    }
                    debug(folderStr)
                    debug(folderIDStr)
                  }

                  i++
                  next()
                })
              },
              function whilstResultHandler (err) {
                if (err) {
                  req.log.error({ err })
                  return res.status(500).json({ msg: defaultErrMsg })
                }

                debug(`Successful getting folder info: ${folderStr} ${folderIDStr}`)
                if (folderStr === '') {
                  return res.status(200).json({ code: 1, msg: 'No folders' })
                } else {
                  return res.status(200).json({ code: 0, folderStr: folderStr, folderIDStr: folderIDStr })
                }
              }
            )
          })
        })
      })
    } catch (err) {
      req.log.error(moduleName + '@getFolderNames - Unknown error occured.', { err })
      return res.status(500).json({ msg: defaultErrMsg })
    }
  })

  return router
}
