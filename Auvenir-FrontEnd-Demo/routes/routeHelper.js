const _ = require('lodash')
const appConfig = require(rootDirectory + '/config')
const cookie = require('cookie')
const cookieParser = require('cookie-parser')
const redis = require('redis')
var initLogger = require(rootDirectory + '/plugins/logger/logger').init
const {info, warn, error} = initLogger(__filename)

/**
 * standardize http response.
 * A success response in json format will be: {status: 200, data: Object}
 * A failure response in json format will be: {status: String(http status code), msg: String}
 * @param {Object} res - express response obj
 * @param {Number} status - If the request succeed, status should be 200. For other situation, follow http status standard
 * @param {Object|String} data - If the request succeed, the data should be an object. For other failed situation, data should be a string representing the error message
 */
const sendResponse = (res, status, data) => {
  if (_.isNull(res) || _.isUndefined(res)) {
    warn('Invalid response object.')
  }

  if (!_.isNumber(status)) {
    warn('Invalid status code. Must be a number')
  }

  if (!_.isString(data)) {
    res.json({ status, data })
  } else {
    res.json({
      status,
      data: {
        msg: data
      }
    })
  }
}

/**
 * Writes a flash message into the session object. This is so that when the user refreshes the screen
 * we are able to display some form of custom message.
 * @param {Object}   req      - The express request object
 * @param {String}   message  - The message to be added to the session
 * @param {Function} callback - (optional) The function to be triggered on callback.
 */
const writeFlash = (req, message, callback) => {
  if (typeof message !== 'string') {
    return callback({code: 1, msg: 'Invalid message type. Must be a string'})
  }

  if (req === null || req === undefined) {
    return callback({code: 1, msg: 'Invalid request object.'})
  }

  if (typeof callback !== 'function') {
    callback = function () {}
  }

  var cookies = cookie.parse(req.headers.cookie)
  var sessionID = cookieParser.signedCookie(cookies['connect.sid'], 'mySecretKey')

  var client
  if (appConfig.env === 'local') {
    client = redis.createClient({ host: appConfig.redis.domain, port: appConfig.redis.port })
  } else {
    client = redis.createClient(appConfig.redis.port, appConfig.redis.domain, {auth_pass: appConfig.redis.auth_pass, tls: {servername: appConfig.redis.servername}})
  }
  client.on('error', function (err) {
    error('Error (Redis) ', {err: err})
    client.quit()
    return callback({code: 1, msg: 'Error occured within Redis.'})
  })

  client.get('sess:' + sessionID, function (err, oneSession) {
    if (err) {
      error('ERROR: getting session info.', {err: err})
      client.quit()
      return callback({code: 1, msg: 'Error occured retrieving session'})
    }

    if (!oneSession) {
      error('Unable to find session for the given user')
      client.quit()
      return callback({code: 1, msg: 'Unable to find session for the given user'})
    } else {
      var newObj = JSON.parse(oneSession)
      newObj.flash = message

      client.set('sess:' + sessionID, JSON.stringify(newObj), function (err, result) {
        if (err) {
          error('ERROR: setting session info.', {err: err})
          client.quit()
          callback({code: 1, msg: 'Error occured setting session'})
        } else {
          client.quit()
          callback(null)
        }
      })
    }
  })
}

/**
 * Is responsible for reading the current session's flash value and then returning the result.
 * It is also required that the value be removed from the object before returning the value, because
 * we don't want to be continually sending the same flash message.
 * @param {Object}   req      - The express request object
 * @param {Function} callback - The function to be triggered on callback.
 */
const readFlash = (req, callback) => {
  if (typeof callback !== 'function') {
    return callback({code: 1, msg: 'Invalid callback type. Must be a function.'})
  }

  if (req === null || req === undefined) {
    return callback({code: 1, msg: 'Invalid request object.'})
  }

  var returnValue
  var cookies = cookie.parse(req.headers.cookie)
  var sessionID = cookieParser.signedCookie(cookies['connect.sid'], 'mySecretKey')

  var client
  if (appConfig.env === 'local') {
    client = redis.createClient({ host: appConfig.redis.domain, port: appConfig.redis.port })
  } else {
    client = redis.createClient(appConfig.redis.port, appConfig.redis.domain, {auth_pass: appConfig.redis.auth_pass, tls: {servername: appConfig.redis.servername}})
  }
  client.on('error', function (err) {
    error('Error (Redis) ', {err: err})
    client.quit()
    return callback({code: 1, msg: 'Error occured within Redis.'})
  })

  client.get('sess:' + sessionID, function (err, oneSession) {
    if (err) {
      error('ERROR: getting session info.', {err: err})
      client.quit()
      return callback({code: 1, msg: 'Error occured retrieving session'})
    }

    if (!oneSession) {
      error('Unable to find session for the given user')
      client.quit()
      return callback({code: 1, msg: 'Unable to find session for the given user'})
    } else {
      var newObj = JSON.parse(oneSession)
      returnValue = newObj.flash
      newObj.flash = ''

      client.set('sess:' + sessionID, JSON.stringify(newObj), function (err, result) {
        if (err) {
          error('ERROR: setting session info.', {err: err})
          client.quit()
          callback({code: 1, msg: 'Error occured updating session'})
        } else {
          client.quit()
          callback(null, returnValue)
        }
      })
    }
  })
}

module.exports = {
  sendResponse,
  flash: {
    write: writeFlash,
    read: readFlash
  }
}
