var appConfig = require('../config')
var uri = require('uri-js')

// for now just support these two values
var supportFrameOption = ['DENY', 'SAMEORIGIN']

var allowedRootDomain = [
  'auvenir.com' // allow control from *.auvenir.com
]
if (appConfig.env === 'development') {
  allowedRootDomain.push('localhost')
}

/**
 * reference --> https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options
 * @param option
 * @returns {Function}
 */
exports.setFrameOption = function (option) {
  if (!~supportFrameOption.indexOf(option)) {
    throw new Error(option + ' is not a valid option')
  }

  return function (req, res, next) {
    res.set('X-Frame-Options', option)
    next()
  }
}

exports.setAllowControl = function (options) {
  var allowCredentials = options.allowCredentials || false
  var methods = options.methods || 'GET, POST'
  var originHeaderFieldName = options.originHeaderFieldName || 'origin' // the proxy may modify the field name
  var allowedHeaders = options.allowedHeaders || 'X-Requested-With, Content-Type'

  return function (req, res, next) {
    var origin = req.get(originHeaderFieldName)

    // by default, http allow control from the same origin
    if (origin) {
      var uriComponents = uri.parse(origin)
      var temp = uriComponents.host.split('.').reverse()
      var rootDomain = temp[1] + '.' + temp[0]
      if (uriComponents.scheme === 'https' && ~allowedRootDomain.indexOf(rootDomain)) {
        res.set('Access-Control-Allow-Origin', origin)
      }
    }

    res.set('Access-Control-Allow-Credentials', allowCredentials)
    res.set('Access-Control-Allow-Methods', methods)
    res.set('Access-Control-Allow-Headers', allowedHeaders)

    next()
  }
}

/**
 * reference --> https://developer.mozilla.org/en-US/docs/Web/Security/HTTP_strict_transport_security
 * @param maxAge
 */
exports.enableSTS = function (maxAge) {
  maxAge = maxAge || 31536000
  return function (req, res, next) {
    res.set('Strict-Transport-Security', 'max-age=' + maxAge + '; includeSubDomains')
    next()
  }
}
