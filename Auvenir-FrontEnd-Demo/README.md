[![Docker Repository on Quay](https://quay.io/repository/auvenir/webapp/status?token=d40fb99f-1bef-42ca-a490-a3ae2ee1c9d6 "Docker Repository on Quay")](https://quay.io/repository/auvenir/webapp)

# AUVENIR
Software used by clients and auditors in order to perform an audit. The client is able to connect their accounting software, uploading bank statements, resolve outliers, provided user and business data and other files in order to complete the audit requirements before they are sent on to their auditor.

## Installing a Development Environment (case-sensitive file system on OSX)

(If you're using Linux for your development environment please see [here](/docker/#local-installs-linux)).

Make sure you have configured ssh keys for your github account, see [here](https://help.github.com/articles/adding-a-new-ssh-key-to-your-github-account/).

To create a new development environment, follow these steps:

1. Download the initialization script:<br>
  ``` 
  $ curl https://s3.amazonaws.com/auvenir-bootstrap/init.sh > init.sh 
  ```
2. Run the init script, with sudo:<br>
  ```
  $ sudo bash init.sh
  ```
3. The application will start automatically, and be placed inside a 'workspace' folder in your home directory.<br>
  ```
  $ cd ${HOME}/workspace/Auvenir
  ```
4. For subsequent starts:<br>
  ```
  $ ./docker/local/build.sh
  ```

## Pulling down a new branch

When you update your repo from a new branch, the fastest way to ensure you don't have conflicting npm dependencies or app state is by running the following command

```
./docker/local/build.sh fast
```
This should be run the first time after anytime you change your branch to ensure the correct code is being run.

## Accessing configuration/environment variables
1. Require the config into your file as appConfig i.e appConfig = require('./config')
2. Access properties with appConfig.```<your property here>```

## Updating configuration/environment variables
The config folder is what is used to ensure that the environment variables are loaded correctly on all environments.
If you need to add an environment variable, first add a line to /config/schema.js, i.e.
```
// config/schema.js
new_variable: { default: null, format: String}
```
Note #1: For type validation (the format field), all javascript built in types are available, as well as custom types (see the link in the file for details). Although the config will throw a slack notification if there is a missing config in another environment, it's always a good idea to give a heads up to DevOps when adding a new variable because they will need to update the other environment specific config files.

Note #2: If you want the variable the same on all environments, set the default field to that value. Otherwise, for environment specific values, please leave this to null and have DevOps update their <env>.json files as required.

Lastly, add the actual variable inside /config/local.json, i.e.
```
// config/local.js
new_variable: 'My new variable'
```

## Other Information

For more details, please refer to:

https://baystreetlabs.atlassian.net/wiki/spaces/DEV or

https://github.com/Auvenir/Auvenir/wiki (outdated)


Copyright © 2017 Auvenir
