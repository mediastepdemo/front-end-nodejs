
module.exports = function () {
  // Log to the window
  if (typeof window !== 'undefined' && window.log) {
    // Log to window
    return window.log
  } else {
    // Log to console
    var loggingOn = true
    var args = []

    for (var i = 0; i < arguments.length; i++) {
      if (typeof arguments[i] === 'string') {
        if (arguments[i].length) {
          args.splice(0, 0, 'color:#424242;font-weight:bold;background:#C5E1A5')
          args.splice(0, 0, '%c \t' + arguments[i] + '\t')
        }
      } else {
        args.push(arguments[i])
      }
    }

    if (!args.length) {
      return console.log // eslint-disable-line
    }

    if (loggingOn) {
      console.log.apply(null, args) // eslint-disable-line
    }
  }
}
