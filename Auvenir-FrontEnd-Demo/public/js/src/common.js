const _ = require('lodash')

var COMMON = {
  TODOS_BULK_UPDATE_TYPE: {
    DELETE: 1,
    MARK_COMPLETE: 2,
    AUDITOR_ASSIGN: 3,
    CLIENT_ASSIGN: 4
  },

  FILTER_ENUM: {
    COMPLETE: 1,
    ASSIGNEE: 2,
    DUEDATE: 3,
    COMMENT: 4,
    REQUEST: 5,
    OUTSTANDING: 6
  },

  TODO_DELETE_STATUS_ENUM: {
    ACTIVE: 'ACTIVE',
    INACTIVE: 'INACTIVE'
  },

  removeInactiveTodos: function (todosData) {
    var todos = []
    _.map(todosData, function (todo) {
      if (todo.status !== COMMON.TODO_DELETE_STATUS_ENUM.INACTIVE) {
        todos.push(todo)
      }
    })
    return todos
  },

  checkEngagementData: function (data) {
    var length = data.length
    for (var i = 0; i < length; i++) {
      data[i].todos = COMMON.removeInactiveTodos(data[i].todos)
    }

    return data
  },

  addSearchDataToTodos: function (todos, categories, acl) {
    var todoArr = []
    _.map(todos, function (todo) {
      _.map(categories, function (category) {
        if (todo.category === category._id) {
          todo.category = category
        }
      })
      _.map(acl, function (user) {
        if (todo.auditorAssignee.toString() === user.id.toString()) {
          todo.auditorAssignee = {
            _id: user.id,
            firstName: user.userInfo.firstName,
            lastName: user.userInfo.lastName
          }
        }
        if (todo.clientAssignee && todo.clientAssignee.toString() === user.id.toString()) {
          todo.clientAssignee = {
            _id: user.id,
            firstName: user.userInfo.firstName,
            lastName: user.userInfo.lastName
          }
        }
      })
      if (todo.dueDate !== '') {
        let date = new Date(todo.dueDate)
        todo.dueDateString = date.getTime()
      }
      todoArr.push(todo)
    })
    return todoArr
  },

  isEngagementStatusCompleted: function (todosData) {
    var isCompleted = true
    _.map(todosData, function (todo) {
      if (todo.status === COMMON.TODO_DELETE_STATUS_ENUM.ACTIVE && !todo.completed) {
        isCompleted = false
      }
    })
    return isCompleted
  }
}

module.exports = COMMON
