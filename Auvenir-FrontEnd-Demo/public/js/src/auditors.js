// AuditorScriptsLanding.js
$.belowthefold = function (element, settings) {
  var fold = $(window).height() + $(window).scrollTop()
  return fold <= $(element).offset().top - settings.threshold
}
$.abovethetop = function (element, settings) {
  var top = $(window).scrollTop()
  return top >= $(element).offset().top + $(element).height() - settings.threshold
}
$.rightofscreen = function (element, settings) {
  var fold = $(window).width() + $(window).scrollLeft()
  return fold <= $(element).offset().left - settings.threshold
}
$.leftofscreen = function (element, settings) {
  var left = $(window).scrollLeft()
  return left >= $(element).offset().left + $(element).width() - settings.threshold
}
$.inviewport = function (element, settings) {
  return !$.rightofscreen(element, settings) && !$.leftofscreen(element, settings) && !$.belowthefold(element, settings) && !$.abovethetop(element, settings)
}
$.extend($.expr[':'], {
  'below-the-fold': function (a, i, m) {
    return $.belowthefold(a, {
      threshold: 0
    })
  },
  'above-the-top': function (a, i, m) {
    return $.abovethetop(a, {
      threshold: 0
    })
  },
  'left-of-screen': function (a, i, m) {
    return $.leftofscreen(a, {
      threshold: 0
    })
  },
  'right-of-screen': function (a, i, m) {
    return $.rightofscreen(a, {
      threshold: 0
    })
  },
  'in-viewport': function (a, i, m) {
    return $.inviewport(a, {
      threshold: 0
    })
  }
})

// FORM VALIDATION

$('.subscribe-form input').jqBootstrapValidation({
  preventSubmit: true,
  submitSuccess: function ($form, event) {
    event.preventDefault() // prevent default submit behaviour
    $.ajax({
      success: function () {
        $('#subscribe-success').html("<div class='alert alert-success'>")
        $('#subscribe-success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times")
          .append('</button>')
        $('#subscribe-success > .alert-success')
          .append('<strong>Your message has been sent. </strong>')
        $('#subscribe-success > .alert-success')
          .append('</div>')
      }
    })
  }
})

// HEADER PARTICLES EFFECT

if ($(window).width() > 960) {
  particlesJS('particles-js', {
    'particles': {
      'number': {
        'value': 120,
        'density': {
          'enable': true,
          'value_area': 1800
        }
      },
      'color': {
        'value': '#ffffff'
      },
      'shape': {
        'type': 'circle',
        'stroke': {
          'width': 0,
          'color': '#000000'
        },
        'polygon': {
          'nb_sides': 3
        },
        'image': {
          'src': 'img/github.svg',
          'width': 100,
          'height': 100
        }
      },
      'opacity': {
        'value': 0.5,
        'random': false,
        'anim': {
          'enable': false,
          'speed': 1,
          'opacity_min': 0.2,
          'sync': false
        }
      },
      'size': {
        'value': 3,
        'random': true,
        'anim': {
          'enable': false,
          'speed': 20,
          'size_min': 0.1,
          'sync': false
        }
      },
      'line_linked': {
        'enable': true,
        'distance': 250,
        'color': '#ffffff',
        'opacity': 0.2,
        'width': 1
      },
      'move': {
        'enable': true,
        'speed': 1,
        'direction': 'none',
        'random': false,
        'straight': false,
        'out_mode': 'out',
        'bounce': false,
        'attract': {
          'enable': false,
          'rotateX': 600,
          'rotateY': 1200
        }
      }
    },
    'interactivity': {
      'detect_on': 'window',
      'events': {
        'onhover': {
          'enable': false,
          'mode': 'grab'
        },
        'onclick': {
          'enable': false,
          'mode': 'push'
        },
        'resize': true
      },
      'modes': {
        'grab': {
          'distance': 180,
          'line_linked': {
            'opacity': 1
          }
        },
        'bubble': {
          'distance': 400,
          'size': 40,
          'duration': 2,
          'opacity': 8,
          'speed': 3
        },
        'repulse': {
          'distance': 200,
          'duration': 0.4
        },
        'push': {
          'particles_nb': 4
        },
        'remove': {
          'particles_nb': 2
        }
      }
    },
    'retina_detect': true
  })
}

// PRELOADER

$(window).on('load', function () {
  $('#preloader').fadeOut('slow', function () {
    $(this).remove()
  })
})

// PIE CHARTS

// AUTO PLAY YOUTUBE VIDEO

function autoPlayYouTubeModal () {
  var trigger = $('body').find('[data-toggle="modal"]')
  trigger.click(function () {
    var theModal = $(this).data('target')
    var videoSRC = $('#video-modal iframe').attr('src')
    var videoSRCauto = videoSRC + '?autoplay=1'
    $(theModal + ' iframe').attr('src', videoSRCauto)
    $(theModal + ' button.close').on('click', function () {
      $(theModal + ' iframe').attr('src', videoSRC)
    })
    $('.modal').on('click', function () {
      $(theModal + ' iframe').attr('src', videoSRC)
    })
  })
}
autoPlayYouTubeModal()

// TESTIMONIALS SLIDER

$('#testimonials .slider').owlCarousel({
  navigation: true,
  slideSpeed: 300,
  paginationSpeed: 400,
  singleItem: true
})

// CLIENTS SLIDER

$('#clients .slider').owlCarousel({
  navigation: true,
  pagination: false,
  autoPlay: 5000, // Set AutoPlay to 3 seconds
  items: 5
})

// MAIN MENU TOGGLE AND SMOOTH SCROLL

$('.navbar-collapse ul li a').on('click', function () {
  $('.navbar-toggle:visible').click()
})

$(function () {
  $('a.page-scroll').on('click', function (event) {
    var $anchor = $(this)
    $('html, body').stop().animate({
      scrollTop: $($anchor.attr('href')).offset().top - 64
    }, 1500, 'easeInOutExpo')
    event.preventDefault()
  })
})

$('body').scrollspy({
  offset: 64,
  target: '.navbar-fixed-top'
})

// ANIMATED MENU

var cbpAnimatedHeader = (function () {
  var docElem = document.documentElement
  var header = document.querySelector('.navbar-default')
  var didScroll = false
  var changeHeaderOn = 50

  function init () {
    window.addEventListener('scroll', function (event) {
      if (!didScroll) {
        didScroll = true
        setTimeout(scrollPage, 100)
      }
    }, false)
    window.addEventListener('load', function (event) {
      if (!didScroll) {
        didScroll = true
        setTimeout(scrollPage, 100)
      }
    }, false)
  }

  function scrollPage () {
    var sy = scrollY()
    if (sy >= changeHeaderOn) {
      $('.navbar-default').addClass('navbar-shrink')
      document.getElementById('headerLogo').src = 'img/logo_green.svg'
      header.style.backgroundColor = '#fff'
    } else {
      document.getElementById('headerLogo').src = 'img/logo.svg'
    }
    didScroll = false
  }

  function scrollY () {
    return window.pageYOffset || docElem.scrollTop
  }
})()

// Auditor Page Script
var currentUser = null
var userFullName = ''
var cookieEmail = ''
var urlParameters = window.location.search
var analytics = {}

var xhr = new XMLHttpRequest()
xhr.onreadystatechange = function () {
  if (xhr.readyState !== 4) {
    return
  }
  var response = JSON.parse(xhr.responseText)
  if (response.data && response.data.flash && response.data.flash !== '') {
    // Currently we are only outputting to an alert until design/UX can come
    // up with a way of doing it. One way is to update the text of the popup for id=landing-welcomeBack
    togglePopUp(response.data.flash)
  }
  if (response.status === 200) {
    currentUser = response.data
    updateUser()
    updateDom()
    if (currentUser.status.toLowerCase() === 'current') {
      window.location = '/home'
    } else if (currentUser.status.toLowerCase() === 'previous') {
      cookieEmail = currentUser.email
      recognizeUser('top')
    }
  }
}

xhr.open('GET', '/session-info', true)
xhr.send();

(function () {
  var xhr = new XMLHttpRequest()
  xhr.open('GET', '/landingAnalytics')
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        try {
          analytics = JSON.parse(xhr.responseText)

          // GOOGLE ANALYTICS INTEGRATION
          var addGA = function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r
            i[r] = i[r] || function () {
              (i[r].q = i[r].q || []).push(arguments)
            }
            i[r].l = 1 * new Date()
            a = s.createElement(o)
            m = s.getElementsByTagName(o)[0]
            a.async = 1
            a.src = g
            m.parentNode.insertBefore(a, m)
          }
          addGA(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga')
          ga('create', analytics.google_analytics.id, 'auto')
          ga('send', 'pageview')

          var addHotjar = function (h, o, t, j, a, r) {
            h.hj = h.hj || function () {
              (h.hj.q = h.hj.q || []).push(arguments)
            }
            h._hjSettings = {
              hjid: analytics.hot_jar.id,
              hjsv: analytics.hot_jar.hjsv
            }
            a = o.getElementsByTagName('head')[0]
            r = o.createElement('script')
            r.async = 1
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv
            a.appendChild(r)
          }
          addHotjar(window, document, '//static.hotjar.com/c/hotjar-', '.js?sv=')

          window.intercomSettings = { app_id: analytics.intercom.id }
          var addIntercom = function () {
            var w = window
            var ic = w.Intercom
            if (typeof ic === 'function') {
              ic('reattach_activator')
              ic('update', intercomSettings)
            } else {
              var d = document
              var i = function () {
                i.c(arguments)
              }
              i.q = []
              i.c = function (args) {
                i.q.push(args)
              }
              w.Intercom = i

              var s = d.createElement('script')
              s.type = 'text/javascript'
              s.async = true
              s.src = 'https://widget.intercom.io/widget/' + analytics.intercom.id
              var x = d.getElementsByTagName('script')[0]
              x.parentNode.insertBefore(s, x)
            }
          }
          addIntercom()
        } catch (err) {
          return
        }
      }
    }
  }
  xhr.send()
})()

function updateUser () {
  var json = currentUser
  var userFirstName = ''
  var userLastName = ''
  if (json.firstName === '' && json.lastName !== '') {
    userFirstName = json.lastName
    userFullName = json.lastName
  } else if (json.firstName !== '' && json.lastName === '') {
    userFirstName = json.firstName
    userFullName = json.firstName
  } else if (json.firstName === '' && json.lastName === '') {
    userFirstName = json.email
    userFullName = json.email
  } else {
    userFirstName = json.firstName
    userLastName = json.lastName
    userFullName = json.firstName + ' ' + userLastName
  }

  document.getElementById('audLanding-NameBtn').innerHTML = userFirstName
  document.getElementById('audLandProf-fullName').innerHTML = userFullName
  document.getElementById('audLandProf-email').innerHTML = json.email
  document.getElementById('audLand-dd-fullName').innerHTML = userFullName
  document.getElementById('audLand-dd-email').innerHTML = json.email
}

function submitEmail () {
  if (document.getElementById('emailField').value !== '') {
    var email = document.getElementById('emailField')
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
    if (!filter.test(email.value)) {
      document.getElementById('mainErrorDiv').classList.add('audLand-errorAfter')
      return false
    }
    document.getElementById('mainErrorDiv').classList.remove('audLand-errorAfter')
    var cpaEmail = document.getElementById('emailField').value
    document.getElementById('audLandMainBtn').setAttribute('data-toggle', 'modal')
  } else if (document.getElementById('audLandProf-email') !== '') {
    var cpaEmail = document.getElementById('audLandProf-email').innerHTML
    document.getElementById('audLandProf-loginBtn').setAttribute('data-toggle', 'modal')
  }

  var uri = '/login'
  var xhr = new XMLHttpRequest()
  var fd = new FormData()

  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      var json = JSON.parse(xhr.responseText)
      if (json.redirect) {
        window.location = json.redirect
      } else if (json.code === 200) {
        document.getElementById('audLand-preloaderModal').style.display = 'none'
        document.getElementById('audLand-modal-loginImg').src = json.msg3 || 'images/icons/warning.svg'
        document.getElementById('audLand-modal-loginHeader').innerHTML = json.msg
        document.getElementById('audLand-modal-loginSubHeader').innerHTML = json.msg2 || ''
        document.getElementById('audLandModalDoneBtn').className = 'btn landing-modal-button'
      } else if (json.code === 202) {
        document.getElementById('audLand-preloaderModal').style.display = 'none'
        document.getElementById('audLand-modal-loginImg').src = json.msg3 || 'images/icons/warning.svg'
        document.getElementById('audLand-modal-loginHeader').innerHTML = json.msg
        document.getElementById('audLand-modal-loginSubHeader').innerHTML = json.msg2 || ''
        document.getElementById('audLandModalDoneBtn').className = 'btn landing-modal-button'
      } else {
        document.getElementById('audLand-preloaderModal').style.display = 'none'
        document.getElementById('audLand-modal-loginImg').src = json.msg3 || 'images/icons/warning.svg'
        document.getElementById('audLand-modal-loginHeader').innerHTML = json.msg
        document.getElementById('audLand-modal-loginSubHeader').innerHTML = json.msg2 || ''
        document.getElementById('audLandModalDoneBtn').className = 'btn landing-modal-button'
      }
    }
  }

  xhr.open('POST', uri, true)
  fd.append('email', cpaEmail)
  fd.append('type', 'AUDITOR')
  xhr.send(fd)
}

// add elipsers if name too long for minimal font-size of 12px
var addElipses = function (name, elem) {
  var longName = name
  longName = longName.substring(0, 25) + '...'
  document.getElementById(elem).innerHTML = longName
}

// check for elipses
var elipsesCheck = function (elem) {
  var fontSize = document.getElementById(elem).style.fontSize
  var fontSizeNoPx = parseInt(fontSize, 10)
  if (fontSizeNoPx <= 12) {
    document.getElementById(elem).style.fontSize = '12px'
    addElipses(document.getElementById(elem).innerHTML, elem)
  }
}

/* SETTING FONT SIZE OF NAME BASED ON WIDTH */
var adjustFontSize = function (elem) {
  var fontstep = 2
  if ($(elem).height() > $(elem).parent().height() || $(elem).width() > $(elem).parent().width()) {
    $(elem).css('font-size', (($(elem).css('font-size').substr(0, 2) - fontstep)) + 'px').css('line-height', (($(elem).css('font-size').substr(0, 2))) + 'px')
    adjustFontSize(elem)
  }
}

function togglePopUp (text) {
  if (text) {
    $('#audLanding-welcomeBack').html(text)
  }
  var main = $('main')
  var val = !main.hasClass('nav-is-visible')
  main.toggleClass('nav-is-visible', val)
  $('.cd-3d-nav-container').toggleClass('nav-is-visible', val)
}

/**
 * Responsible for when a success link has been triggered (Email)
 * Triggered whenever an email is sent out.
 */
var updateDom = function () {
  if (currentUser) {
    // set display none to log in buttons and display the new button
    document.getElementById('aud-loginBtnHolder').style.display = 'none'
    document.getElementById('audLand-login-dropdown').style.display = 'none'
    document.getElementById('audLand-profile').style.display = 'block'

    if (document.getElementById('audLand-secondaryLogin').classList.contains('land-loginDropdown-after')) {
      openEmailInput()
    }

    document.getElementById('audLand-login-errorDiv').style.display = 'none'
    document.getElementById('audLand-profile-dropdown').style.display = 'block'
    document.getElementById('audLand-loginDropdown-sec').style.display = 'none'

    document.getElementById('audLand-preloaderBtn').style.display = 'none'
    document.getElementById('audLand-preloaderBtnSecondary').style.display = 'none'
  }

  if (document.getElementById('audLand-profile-dropdown-container').classList.contains('land-profileDropContainer-after')) {
    document.getElementById('audLand-profile-dropdown-container').classList.toggle('land-profileDropContainer-after')
  }
}

/**
 * Triggered whenever we have an email. Sends a message to the server and will return user info and may
 * also send out an email.
 */
var recognizeUser = function (trig, isMobile) {
  var uri = '/login'

  var xhr = new XMLHttpRequest()
  var fd = new FormData()
  var text = isMobile ? 'Welcome! Please check your mobile device for a push notification.' : false

  function popUp () {
    if ($('main').hasClass('nav-is-visible')) {
      togglePopUp(text)
      setTimeout(function () {
        togglePopUp(text)
      }, 800)
    } else {
      setTimeout(function () {
        togglePopUp(text)
      }, 200)
    }
  }
  if (text) {
    popUp()
  }

  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      if (xhr.responseURL.indexOf('home') !== -1) {
        window.location = xhr.responseURL
      } else {
        var json = JSON.parse(xhr.responseText)
        if (json.redirect) {
          window.location = json.redirect
        }
        // If initial login, basic functionality
        if (trig === 'top') {
          if (json.code === 202) {
            currentUser = json
            popUp()
            loginDropdownToggle()
            clearFields()
          }
          if (json.code === 200 || json.code === 201) {
            document.getElementById('audLand-login-notVerified').style.display = 'block'
            clearFields()
          }
          document.getElementById('audLand-preloaderBtn').style.display = 'none'
        // If already remembered from cookie, then more detailed functionality
        } else if (trig === 'sec') {
          if (json.code === 202) {
            currentUser = json
            updateUser()
            updateDom()
            popUp()
          } else if (json.code === 200 || json.code === 201) {
            document.getElementById('audLand-preloaderBtnSecondary').style.display = 'none'
            document.getElementById('audLand-login-notVerifiedSecondary').style.display = 'block'
          } else {
            document.getElementById('audLand-login-secondaryErrorDiv').style.display = 'block'
            document.getElementById('audLand-preloaderBtnSecondary').style.display = 'none'
          }
        }
        if (json.redirect) {
          window.location = json.redirect
        }
      }
    }
  }

  xhr.open('POST', uri, true)
  fd.append('email', cookieEmail)
  fd.append('type', 'AUDITOR')
  xhr.send(fd)
}

// This is the first input field when you want to log in
var recognizeUser_Top = function () {
  var email = document.getElementById('audLand-emailField-top')
  var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
  if (!filter.test(email.value)) {
    document.getElementById('audLand-login-notVerified').style.display = 'none'
    document.getElementById('audLand-login-errorDiv').style.display = 'block'
    return false
  }
  cookieEmail = document.getElementById('audLand-emailField-top').value
  document.getElementById('audLand-login-errorDiv').style.display = 'none'
  document.getElementById('audLand-login-notVerified').style.display = 'none'
  document.getElementById('audLand-preloaderBtn').style.display = 'block'
  recognizeUser('top')
}

// this is the input field when you are trying to switch users
var recognizeUser_Secondary = function () {
  var email = document.getElementById('audLand-emailField-secondaryTop')
  var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
  if (!filter.test(email.value)) {
    document.getElementById('audLand-login-notVerifiedSecondary').style.display = 'none'
    document.getElementById('audLand-login-secondaryErrorDiv').style.display = 'block'
    return false
  }
  cookieEmail = document.getElementById('audLand-emailField-secondaryTop').value
  document.getElementById('audLand-login-secondaryErrorDiv').style.display = 'none'
  document.getElementById('audLand-login-notVerifiedSecondary').style.display = 'none'
  document.getElementById('audLand-preloaderBtnSecondary').style.display = 'block'
  recognizeUser('sec')
}

// submit on enter press
$(document).ready(function () {
  $('#audLand-emailField-top').keypress(function (e) {
    if (e.keyCode === 13) {
      $('#audLand-dd-loginBtn').click()
    }
  })
})
$(document).ready(function () {
  $('#audLand-emailField-secondaryTop').keypress(function (e) {
    if (e.keyCode === 13) {
      $('#audLand-loginBtn-secondaryTop').click()
    }
  })
})

$('#audLandEmailSent').on('hidden.bs.modal', function () {
  clearFields()
})
 /*
 * Replace all SVG images with inline SVG
 */
jQuery('img.svg').each(function () {
  var $img = jQuery(this)
  var imgID = $img.attr('id')
  var imgClass = $img.attr('class')
  var imgURL = $img.attr('src')

  jQuery.get(imgURL, function (data) {
        // Get the SVG tag, ignore the rest
    var $svg = jQuery(data).find('svg')

        // Add replaced image's ID to the new SVG
    if (typeof imgID !== 'undefined') {
      $svg = $svg.attr('id', imgID)
    }
        // Add replaced image's classes to the new SVG
    if (typeof imgClass !== 'undefined') {
      $svg = $svg.attr('class', imgClass + ' replaced-svg')
    }

        // Remove any invalid XML tags as per http://validator.w3.org
    $svg = $svg.removeAttr('xmlns:a')

        // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
    if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
      $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
    }

        // Replace image with new SVG
    $img.replaceWith($svg)
  }, 'xml')
})

function clearFields () {
  document.getElementById('audLand-emailField-top').value = ''
  document.getElementById('emailField').value = ''
  document.getElementById('audLand-preloaderModal').style.display = 'inherit'
  document.getElementById('audLand-modal-loginImg').src = ''
  document.getElementById('audLand-modal-loginHeader').innerHTML = ''
  document.getElementById('audLand-modal-loginSubHeader').innerHTML = ''
  document.getElementById('audLandModalDoneBtn').className = 'landing-displayNone'
  document.getElementById('audLandMainBtn').removeAttribute('data-toggle')
  document.getElementById('audLand-preloaderBtn').style.display = 'none'
}

var loginDropdownToggle = function () {
  document.getElementById('aud-loginBtnHolder').classList.toggle('audLand-loginBtnHolder-after')
  document.getElementById('audLand-login-dropdown').classList.toggle('land-loginDropdown-after')
  document.getElementById('audLand-loginBtn').classList.toggle('audLand-login-btn-after')
  document.getElementById('aud-loginChevron').classList.toggle('audLand-login-btn-after')
}

var profileDropdown = function () {
  document.getElementById('audLand-profile-dropdown-container').classList.toggle('land-profileDropContainer-after')
  adjustFontSize('#audLandProf-fullName')
  adjustFontSize('#audLandProf-email')
  elipsesCheck('audLandProf-fullName')
  elipsesCheck('audLandProf-email')
}

var openEmailInput = function () {
  document.getElementById('audLand-loginDiffHolder').classList.toggle('audLand-ld-diffEmailHolder-after')
  document.getElementById('audLand-secondaryLogin').classList.toggle('land-loginDropdown-after')
}

var openSwitchAccount = function () {
  document.getElementById('audLand-profile-dropdown').style.display = 'none'
  document.getElementById('audLand-loginDropdown-sec').style.display = 'block'
  adjustFontSize('#audLand-dd-fullName')
  adjustFontSize('#audLand-dd-email')
  elipsesCheck('audLand-dd-fullName')
  elipsesCheck('audLand-dd-email')
}

// Clicking outside of dropdowns will close them.
// For Login Dropdown
$(document).mouseup(function (e) {
  var containerLogin = $('#audLand-login-dropdown')
  var loginButton = $('#audLand-loginBtn')
  var containerProfile = $('#audLand-profile-dropdown-container')
  var profileButton = $('#audLanding-NameBtn')
  if (!containerLogin.is(e.target) && containerLogin.has(e.target).length === 0 && document.getElementById('audLand-login-dropdown').classList.contains('land-loginDropdown-after') && !loginButton.is(e.target)) {
    loginDropdownToggle()
  }
  if (!containerProfile.is(e.target) && containerProfile.has(e.target).length === 0 && document.getElementById('audLand-profile-dropdown-container').classList.contains('land-profileDropContainer-after') && !profileButton.is(e.target)) {
    profileDropdown()
    if (document.getElementById('audLand-loginDropdown-sec').style.display === 'block') {
      document.getElementById('audLand-loginDropdown-sec').style.display = 'none'
      document.getElementById('audLand-profile-dropdown').style.display = 'block'
    }
  }
})

$(document).ready(function () {
  var loginBoxCheck = (document.URL.indexOf('#login') > 0)
  if (loginBoxCheck) { document.getElementById('audLand-loginBtn').click() }
})
