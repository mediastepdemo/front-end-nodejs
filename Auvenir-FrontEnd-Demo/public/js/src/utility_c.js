
import log from './log'
/**
 * This is the utility object
 * Extends [`Control`](#Control).
 * @class Utility
 * @param {Object} options
 * @param {boolean} [options.failIfMajorPerformanceCaveat=false] If `true`, map creation will fail if it is determined
 * @param {Object|string} [options.style] The map's Mapbox style. This must be an a JSON object conforming to
 * the schema described in the [Mapbox Style Specification](https://mapbox.com/mapbox-gl-style-spec/), or a URL to
 * such JSON.
 *
 * To load a style from the Mapbox API, you can use a URL of the form `mapbox://styles/:owner/:style`,
 * where `:owner` is your Mapbox account name and `:style` is the style ID. Or you can use one of the following
 * [the predefined Mapbox styles](https://www.mapbox.com/maps/):
 *
 *  * `mapbox://styles/mapbox/streets-v9`
 *  * `mapbox://styles/mapbox/outdoors-v9`
 *  * `mapbox://styles/mapbox/light-v9`
 *  * `mapbox://styles/mapbox/dark-v9`
 *  * `mapbox://styles/mapbox/satellite-v9`
 *  * `mapbox://styles/mapbox/satellite-streets-v9`
 * @example
  * var map = new mapboxgl.Map({
 *   container: 'map',
 *   center: [-122.420679, 37.772537],
 *   zoom: 13,
 *   style: style_object,
 *   hash: true
 * });
*/
var Utility = {

  /**
   * Responsible for generically filtering an array of json objects and returning
   * a new array based on the filter json object passed in.  If the field in the filter
   * object doesn't match an JSON object field, then it is skipped!
   * @name Utility.findMatches
   * @param {Array} arr an array of stuff
   * @param {Object} filter the filter aksljldhaskjdh askdjbas kjdh askjdh aksjdh aksjdh aksjdh akjshdaksjdhakjsd akjsd kja sdjasd kj
   * @return {Array} results an array
   */

  ALPHANUMERIC: '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
  REGEX: {
    email: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,
    crud: /^[cud]+$/i,
    phone: /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/,
    unitNumber: /^\d+$/,
    streetAddress: /^\s*\S+(?:\s+\S+)*$/,
    postalCode: /^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$/,
    zipCode: /^\d{5}([ \-]\d{4})?$/,
    country: /^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/,
    city: /^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/,
    stateProvince: /^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/,
    noSelect: /^(?!Select).*$/,
    alphanumeric: /^[A-Za-z0-9]*$/,
    imageFormat: /.(?:jpe?g|png|gif)$/,
    name: /^([A-Za-z]+[,.]?[ ]?|[a-z]+['-]?)+$/i,
    industry: /^([a-z0-9\/]+[,.]?[ ]?|[a-z0-9]+['-]?)+$/i,
    businessName: /^([a-z0-9&/-]+[,.]?[ ]?|[a-z0-9]+['-]?)/i,
    alphanumericName: /^[A-Za-z0-9 ,.'"-_]*$/,
    date: /([0-9]{2}[/]?[0-9]{2}[/]?[0-9]{4})/,
    url: /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=+$,\w]+@)?[A-Za-z0-9.\-]+|(?:www\.|[\-;:&=+$,\w]+@)[A-Za-z0-9.\-]+)((?:\/[+~%\/.\w\-_]*)?\??(?:[\-+=&;%@.\w_]*)#?(?:[.!\/\\\w]*))?)/,
    urlNoHTTPWWW: /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([-.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/
  },

  /**
   * Client-side validation, return true/false if errors exist
   * Side-effects: marks/unmarks fields if errors exists
   * @class Utility_C
   * @memberof Utility
   * @name Utility.isFieldValid
   * @param {Object} data
   * @returns whether the field is valid or not
   */
  isFieldValid: function (data) {
    log('Checking if data passed is valid')
    var updateDOM = document.getElementById(data.id)
    var valid = true
    // Sets valid if field is valid
    if (data.validation) {
      if (data.validation.required) {
        valid = valid && updateDOM.value.length > 0
      }

      if (data.validation.min) {
        valid = valid && updateDOM.value.length >= data.validation.min
      }

      if (data.validation.max) {
        valid = valid && updateDOM.value.length <= data.validation.max
      }

      if (data.validation.regex) {
        var regex = new RegExp(data.validation.regex)
        valid = valid && regex.test(updateDOM.value)
      }
    }
    if (valid) {
      Utility.unmarkAsError(data.id)
    } else {
      Utility.markAsError(data.id)
    }
    return valid
  },

  /**
   * Regular Expression test for email validation
   * @class Utility_C
   * @memberof Utility
   * @name Utility.isDateInFuture
   * @param {rawDateString} string Date string in dd/mm/yyyy format
   * @param {includePresentDay} boolean Determines whether to include the present day in positive cases or not
   * @return {Utility}
   */
  isDateInFuture: function (rawDateString, includePresentDay) {
    var val = new Date(rawDateString.split('/')[2], rawDateString.split('/')[0] - 1, rawDateString.split('/')[1])
    var cur = new Date(Date.now())
    if (!val || (val.getTime() < cur.getTime() &&
        !(includePresentDay && (cur.getDate() === val.getDate() && cur.getMonth() === val.getMonth() && val.getFullYear() === cur.getFullYear())))) {
      return false
    }
    return true
  },

  /**
   * Regular Expression test for email validation
   * @class Utility_C
   * @memberof Utility
   * @name Utility.isEmailValid
   * @param {String} email The character set to be used
   * @return {Utility}
   */
  isEmailValid: function (email) {
    log('Verifying email')
    if (typeof email !== 'string') {
      return false
    }
    if (Utility.REGEX.email.test(email)) {
      log('Email verified')
      return true
    } else {
      return false
    }
  },

  /**
   * Regular Expression test for phone number validation
   * @class Utility_C
   * @memberof Utility
   * @name Utility.isPhoneValid
   * @param {String} phone The phone number to be tested
   * @return {Utility}
   */
  isPhoneValid: function (phone) {
    log('Verifying Phone')
    if (typeof phone !== 'string' && phone !== '') {
      return false
    }
    if (Utility.REGEX.phone.test(phone)) {
      log('Phone verified')
      return true
    } else {
      return false
    }
  },

  /**
   * Returns a random string generated of length 'length'
   * @class Utility_C
   * @memberof Utility
   * @name Utility.randomString
   * @param {Number} length
   * @param {string} chars
   * @returns a random string
   */
  randomString: function (length, chars) {
    var result = ''
    for (var i = length; i > 0; --i) {
      result += chars[Math.round(Math.random() * (chars.length - 1))]
    }
    return result
  },

  /**
   * Update Mongo DB if there is any changes.
   * @class Utility_C
   * @memberof Utility
   * @name Utility.updateToDB
   * @param {Object} data
   * @returns {}
   */
  updateToDB: function (data) {
    log('Update new information based on what user typed.')
    var updateDOM = document.getElementById(data.field.id)

    if (!(this.isFieldValid(data.field))) {
      // Nothing changes if not valid.
      return
    }

    if (data.field.value === updateDOM.value) {
      // Nothing Changes We don't need to update the database
      return
    }

    var modelName = data.field.model.name
    var paramName = data.field.model.param
    var json = {}
    var paramKey = paramName.split('.')

    if (modelName === 'User') { // not being accessed from CardAccount. Different format of data passed
      json[paramName] = updateDOM.value
      json.userID = data.appCtrl.currentUser._id
      data.appCtrl.mySocket.emit('update-user', json)
    } else if (modelName === 'Business') { // not being accessed from CardAccount. Different format of data passed
      json[paramName] = updateDOM.value
      json.businessID = data.appCtrl.myBusinesses[0]._id
      data.appCtrl.mySocket.emit('update-business', json)
    } else if (modelName === 'Firm') { // not being accessed from CardAccount. Different format of data passed
      json[paramName] = updateDOM.value
      json.firmID = data.appCtrl.myFirms[0]._id
      data.appCtrl.mySocket.emit('update-firm', json)
    } else {
      return
    }
  },

  /**
   * Responsible for generically filtering an array of json objects and returning
   * a new array based on the filter json object passed in. If the field in the filter
   * object doesn't match any JSON object field, then it is skipped!
   * @class Utility_C
   * @memberof Utility
   * @name Utility.findMatches
   * @param {Array}  arr
   * @param {Object} filter
   * @return array of keys of filtered values
   */
  findMatches: function (arr, filter) {
    log('Lookup based on filters')

    if (!(arr instanceof Array)) {
      return null
    }

    if (!(filter instanceof Object)) {
      return null
    }

    var results = []
    for (var i = 0; i < arr.length; i++) {
      var keys = Object.keys(filter)
      for (var j = 0; j < keys.length; j++) {
        if (arr[i][keys[j]] === filter[keys[j]]) {
          results.push(arr[i])
        }
      }
    }
    return results
  },

  getUserInitials: function (user) {
    var picUserInitials = ''
    var picUserInitialsCaps = ''
    var picUserFirstName = user.firstName || ''
    var picUserLastName = user.lastName || ''

    if (!picUserFirstName && !picUserLastName) {
      picUserInitials = user.email.charAt(0)
    } else {
      picUserInitials = picUserFirstName.charAt(0) + picUserLastName.charAt(0)
    }

    picUserInitialsCaps = picUserInitials.toUpperCase()
    return picUserInitialsCaps
  },
  /**
   * Responsible for returing real user's profile picture address.
   * @class Utility_C
   * @memberof Utility
   * @name Utility.getProfilePicture
   * @param {string} picsrc
   * @return string of real picture source.
   */
  getProfilePicture: function (picsrc) {
    if (!picsrc || picsrc === 'images/profilePhotos/default.png') {
      return 'images/profilePhotos/default.png'
    } else if (Utility.REGEX.url.test(picsrc)) {
      return picsrc
    } else {
      var userID = picsrc.split('/')[0]
      return userID + '/profilePicture?' + new Date().getTime()
    }
  },
  /**
   * Return Real picture source of buisness Logo
   * @class Utility_C
   * @memberof Utility
   * @name Utility.getLogo
   * @param {string} picsrc
   * @return real logo picture source.
   */
  getLogo: function (picsrc) {
    if (picsrc === undefined || picsrc === '') {
      log('Updating Logo')
      log('Logo is not defined, return default picture')
      return 'images/profilePhotos/default.png'
    }
    if (Utility.REGEX.url.test(picsrc)) {
      log("It's from external source, just return picsrc")
      return picsrc
    } else {
      // It's retrieving logo from business DB.
      var businessID = picsrc.split('/')[0]
      return businessID + '/businessLogo?' + new Date().getTime()
    }
  },

  /**
   * Return Real picture source of firm Logo
   * @class Utility_C
   * @memberof Utility
   * @name Utility.getLogoFirm
   * @param {string} picsrc
   * @return real logo picture source.
   */
  getLogoFirm: function (picsrc) {
    if (picsrc === undefined || picsrc === '') {
      log('Updating Logo')
      log('Logo is not defined, return default picture')
      return 'images/profilePhotos/default.png'
    }
    if (Utility.REGEX.url.test(picsrc)) {
      log("It's from external source, just return picsrc")
      return picsrc
    } else {
      // It's retrieving logo from business DB.
      var firmID = picsrc.split('/')[0]
      return firmID + '/firmLogo?' + new Date().getTime()
    }
  },

  /**
   * Helper function that returns the name of the month depending on the length passed
   * @class Utility_C
   * @memberof Utility
   * @name Utility.monthProcess
   * @param {String} length - required length of the month's name
   * @return {Utility}
   */
  monthProcess: function (length, month) {
    if (typeof length !== 'string') {
      return null
    }

    if (typeof month !== 'number') {
      return null
    }

    var m_string = ''
    switch (month) {
      case 1: m_string = 'January'; break
      case 2: m_string = 'February'; break
      case 3: m_string = 'March'; break
      case 4: m_string = 'April'; break
      case 5: m_string = 'May'; break
      case 6: m_string = 'June'; break
      case 7: m_string = 'July'; break
      case 8: m_string = 'August'; break
      case 9: m_string = 'September'; break
      case 10: m_string = 'October'; break
      case 11: m_string = 'November'; break
      case 12: m_string = 'December'; break
      default: return 'Invalid Month'
    }
    if (length === 'long') {
      return m_string
    } else if (length === 'short') {
      return ((month === 5) ? 'May' : m_string.substring(0, 3))
    }
  },

  /**
   * Helper function that returns true if the format passed for output is valid
   * @class Utility_C
   * @memberof Utility
   * @name Utility.validFormatPassed
   * @param {String} format - desired format of the output
   * @return {Utility}
   */
  validFormatPassed: function (format) {
    // check for Date repetitions
    var myFormat = format.output
    var d = (myFormat.indexOf('DD') || (myFormat.indexOf('dd')))
    if (d > -1) {
      if ((myFormat.substring(d + 3).includes('DD')) ||
         (myFormat.substring(d + 3).includes('dd')) ||
         (myFormat.substring(d + 4).includes('D'))) {
        return false
      }
    }
    // check for Month repetitions
    var m = (myFormat.indexOf('MM') || myFormat.indexOf('mm'))

    if (m > -1) {
      if ((myFormat.substring(m + 1).indexOf('MM') > -1) ||
         (myFormat.substring(m + 2).indexOf('M') > -1)) return false
    }
    // check for Year repetitions
    var y = myFormat.indexOf('YYYY') || myFormat.indexOf('yyyy') ||
        myFormat.indexOf('YY') || myFormat.indexOf('yy')

    if (y > -1) {
      if ((myFormat.substring(y + 1).indexOf('YYYY') > -1) ||
         (myFormat.substring(y + 1).indexOf('yyyy') > -1) ||
         (myFormat.substring(y + 3).indexOf('YY') > -1) ||
         (myFormat.substring(y + 3).indexOf('yy') > -1)) return false
    }
    // check for Hours repetitions
    var h = (myFormat.indexOf('hh'))
    if (h > -1) {
      if ((myFormat.substring(h + 1).indexOf('hh') > -1) ||
        (myFormat.substring(h + 2).indexOf('h') > -1)) return false
    }
    // check for Minutes repetitions
    var m = (myFormat.indexOf('mm'))
    if (m > -1) {
      if ((myFormat.substring(m + 1).indexOf('mm') > -1) ||
        (myFormat.substring(m + 2).indexOf('m') > -1)) return false
    }
    // check for Seconds repetitions
    var s = (myFormat.indexOf('ss'))
    if (s > -1) {
      if ((myFormat.substring(s + 1).indexOf('ss') > -1) ||
        (myFormat.substring(s + 2).indexOf('s') > -1)) return false
    }
    if (format.military && h === -1) return false

    return true
  },

  /**
   * Returns the date as desired, based on the parameters passed
   * @class Utility_C
   * @memberof Utility
   * @name Utility.dateProcess
   * @param {DateObject} param
   * @param {Object}     format
   * @returns the desired date or null
   * @example
   * {   military:   true/false
   *    month:     {  type:     string/integer
   *            length:   long/short }
   *    day:     {  append:   true/false}
   *    output:   string with DD/D MM YYYY/YY hh mm ss with any separator/format
   * }
   */
  dateProcess: function (param, format) {
    var myDate = null
    var day_append = ''
    function UserException (name, message) {
      this.name = name
      this.message = message
    }
    if (param === '' || param === null) {
      myDate = new Date()
    } else if (typeof param === 'string') {
      try {
        myDate = new Date(param)
        if (!myDate) throw new UserException('Date Missing', 'Error processing date parameter')
      } catch (e) {
        log(e)
      }
    } else if (param instanceof Date) {
      myDate = param
      try {
        if (!myDate) throw new UserException('Date Missing', 'Error processing date parameter')
      } catch (e) {
        return
      }
    }
    if (!this.validFormatPassed(format)) {
      return
    }
    var myDay = myDate.getDate()
    var myMonth = myDate.getMonth() + 1
    var myYear = myDate.getFullYear()
    var myHours = myDate.getHours()
    var myMin = myDate.getMinutes()
    var mySec = myDate.getSeconds()
    var myMilliSec = myDate.getMilliseconds()
    var return_date = format.output
    if (format.day !== undefined && format.day.append) {
      if (myDay === 1 || myDay === 21 || myDay === 31) day_append = 'st'
      else if (myDay === 2 || myDay === 22) day_append = 'nd'
      else if (myDay === 3 || myDay === 23) day_append = 'rd'
      else day_append = 'th'
    }
    // day process
    var DDPos = format.output.indexOf('DD')
    if (DDPos > -1) {  // there is a DD field
      return_date = return_date.replace('DD', (myDay < 10 && (!format.day)) ? '0' + myDay + day_append : myDay + day_append)
    } else {
      var DPos = format.output.indexOf('D')
      if (DPos > -1) {
        return_date = return_date.replace('D', myDay)
      }
    }

    // month process
    var MMPos = format.output.indexOf('MM')
    if (MMPos > -1) {
      if (format.month.type === 'integer') {
        return_date = return_date.replace('MM', (myMonth < 10) ? '0' + myMonth : myMonth)
      } else if (format.month.type === 'string') {
        return_date = return_date.replace('MM', this.monthProcess(format.month.length, myMonth))
      } else {
        log('Unexpected month format passed')
        return
      }
    }

    // year process
    var YYYYPos = format.output.indexOf('YYYY')

    if (YYYYPos > -1) {
      return_date = return_date.replace('YYYY', myYear)
    } else {
      var YYPos = format.output.indexOf('YY')
      if (YYPos > -1) {
        return_date = return_date.replace('YY', myYear.toString().substring(2))
      }
    }

    if (format.military) {
      var hrs = myHours < 10 ? '0' + myHours : myHours
      var min = myMin < 10 ? '0' + myMin : myMin
      var str_to_replace = return_date.substring(return_date.indexOf('hh'), return_date.lastIndexOf('s') + 1)
      return_date = return_date.replace(str_to_replace, (hrs + '' + min))
    } else if (!format.military) {
      var hhPos = format.output.indexOf('hh')
      var mmPos = format.output.indexOf('mm')
      var ssPos = format.output.indexOf('ss')
      var msPos = format.output.indexOf('ms')
      if (hhPos > -1) {
        return_date = return_date.replace('hh', (myHours > 12) ? ((myHours - 12) < 10 ? ('0' + myHours - 12) : (myHours - 12)) : myHours)
        return_date += (myHours > 12) ? ' PM' : ' AM'
      }
      if (mmPos > -1) {
        return_date = return_date.replace('mm', (myMin < 10) ? ('0' + myMin) : myMin)
      }
      if (ssPos > -1) {
        return_date = return_date.replace('ss', (mySec < 10) ? ('0' + mySec) : mySec)
      }
      if (msPos > -1) {
        return_date = return_date.replace('ms', (myMilliSec < 10) ? ('0' + myMilliSec) : myMilliSec)
      }
    }
    return return_date
  },

  /**
   * Function to display Auvenir mail with proper whitespace
   * @class Utility_C
   * @memberof Utility
   * @name Utility.decodeEmail
   * @param {Object} json
   * @returns the decoded URI
   */
  decodeEmail: function (json) {
    var replaceCR = json.replace(/%0A/g, '</br>')
    var keepSpaces = replaceCR.replace(/%20/g, ' ')
    var decodeCopy = decodeURI(keepSpaces)

    return decodeCopy
  },

  /**
   * Function to format the amount
   * @class Utility_C
   * @memberof Utility
   * @name Utility.formatAmount
   * @param {String} str
   * @returns the formatted amount value
   */
  formatAmount: function (str) {
    var amount = str

    while (amount.indexOf('-') !== -1) {
      amount = amount.replace('-', '')
    }

    while (amount.indexOf(',') !== -1) {
      amount = amount.replace(',', '')
    }

    return parseFloat(amount.trim())
  },

  /**
   * Returns a MM-DD-YYYY string that is based on the date string that is
   * passed. The parameter needs to be a string formatted date object.
   * @class Utility_C
   * @memberof Utility
   * @name Utility.getFormattedDate
   * @param {String} str
   * @param {String} char
   * @returns a JSON object of the count of both the auditor and the client's tasks
   */
  getFormattedDate: function (str, char) {
    var d = new Date(str)
    var year = d.getFullYear()
    var month = d.getMonth() + 1
    month = (month < 10) ? ('0' + month) : month
    var day = d.getDate()
    day = (day < 10) ? ('0' + day) : day

    var c = '-'
    if (char !== null) {
      c = char
    }

    return (month + c + day + c + year)
  },

  /**
   * Returns a MM/DD/YYYY string that is based on the date string that is
   * passed. The parameter needs to be a string formatted date object.
   * @class Utility_C
   * @memberof Utility
   * @name Utility.getFormattedDatePicker
   * @param {String} str: The date
   * @returns a date string
   */
  getFormattedDatePicker: function (str) {
    var month = str.substring(5, 7)
    var day = str.substring(8, 10)
    var year = str.substring(0, 4)
    var c = '/'
    return (month + c + day + c + year)
  },

  /**
   * Prevents a field's property from bubbling
   * @class Utility_C
   * @memberof Utility
   * @name Utility.stopBubbling
   * @param {Object} event
   * @returns {}
   */
  stopBubbling: function (event) {
    event = event || window.event // cross-browser event
    if (event.stopPropagation) {
      event.stopPropagation()
    } else {
      event.cancelBubble = true
    }
  },

  /**
   * Marks a field if error is found
   * @class Utility_C
   * @memberof Utility
   * @name Utility.markAsError
   * @param {ObjectID} id
   * @returns {}
   */
  markAsError: function (id) {
    document.getElementById(id + '-error').classList.add('block-errorExclamation')
    document.getElementById(id).classList.add('block-errorBorder')
  },

  /**
   * Unmarks a field if error is found
   * @class Utility_C
   * @memberof Utility
   * @name Utility.unmarkAsError
   * @param {ObjectID} id
   * @returns {}
   */
  unmarkAsError: function (id) {
    var errorDom = document.getElementById(id + '-error')
    if (errorDom) errorDom.classList.remove('block-errorExclamation')
    document.getElementById(id).classList.remove('block-errorBorder')
  },

  /**
   * Returns formatted full name otherwise returns email
   * @name Utility.GetFullNameFromUser
   *
   * @param {Object} user - The key for the conversation
   * @returns {String}
   */
  GetFullNameFromUser: function (user) {
    if (!user) {
      return
    }

    var firstName = user.firstName.trim()
    var lastName = user.lastName.trim()

    if (firstName && lastName) {
      return firstName + ' ' + lastName
    } else if (firstName) {
      return firstName
    } else if (lastName) {
      return lastName
    } else {
      return user.email
    }
  },

  /**
   * Retrieve JS files on the fly, instead of Configuration Mnaager
   * @class Utility_C
   * @memberof Utility
   * @name Utility.retrieveJS
   * @param {Object} scr
   * @param {Function} callback
   * @returns a callback to the server with a code and a message
   */
  retrieveJS: function (scr, callback) {
    log('Adding script: ' + scr.path)
    var script = document.createElement('script')
    script.type = 'text/javascript'
    script.src = scr.path
    script.onload = function () {
      log('Loaded script successfully')
      callback(null, {})
    }
    script.onerror = function (err) {
      if (err) {
        log(err)
      }
      callback({code: 1, msg: 'Unable to load script successfully'})
    }

    document.head.appendChild(script)
  },

  /**
     * This function create file path when file object is passed.
     * @class Utility_C
     * @memberof Utility
     * @name Utility.createFilePath
     * @param {File} file
     * @returns {string || boolean}
     */
  createFilePath: function (file) {
    if (!file) {
      return { err: 'No file is given.' }
    }
    try {
      var pathString = file.webkitRelativePath
      var pathArray = pathString.split('/')
      var pathToSave = ''
      for (var i = 0; i < pathArray.length - 1; i++) {
        pathToSave += '/' + pathArray[i]
      }
      return pathToSave + '/'
    } catch (err) {
      return { err: 'Error occured while creating file path.' }
    }
  },

  /**
   * This function validates if it's a right mime type for our service.
   * list of mimeType : https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types
   * @class Utility_C
   * @memberof Utility
   * @name Utility.validateMimeType
   * @param {string} mime
   * @returns {boolean}
   */
  checkMimeType: function (mime) {
    log('Validating Mime Type')
    switch (mime) {
      case 'application/pdf':
      case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
      case 'application/msword':
      case 'application/vnd.ms-word.document.macroEnabled.12':
      case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
      case 'application/vnd.ms-powerpoint':
      case 'application/vnd.oasis.opendocument.spreadsheet':
      case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
      case 'application/vnd.ms-excel':
      case 'image/png':
      case 'image/bmp':
      case 'image/jpeg':
      case 'text/plain':
      case 'text/csv':
      case 'application/rtf':
      case 'text/sgml':
        return true
      default:
        return false
    }
  },

  convertEngagement: function (engagement) {
    var todoData = {
      completed: function () {
        if (!engagement.todos) {
          return 0
        }
        var rs = engagement.todos.filter(function (todo) {
          return todo.completed
        })
        return rs.length
      },
      total: function () {
        return engagement.todos ? engagement.todos.length : 0
      },
      percent: function () {
        if (engagement.todos && (this.completed() !== 0 & this.total() !== 0)) {
          return Math.round((this.completed() / this.total()) * 100) + '% (' +
          this.completed() + '/' + this.total() + ')'
        } else {
          return '0% (0/' + this.total() + ')'
        }
      },
      outstanding: function () {
        if (!engagement.todos) {
          return 0
        }
        var completed = todoData.completed()
        var overdue = todoData.overdue()
        var total = todoData.total()
        return (total - completed - overdue)
      },
      overdue: function () {
        if (!engagement.todos) {
          return 0
        }
        var rs = engagement.todos.filter(function (todo) {
          if (!todo.completed) {
            if (todo.dueDate === null) return
            return new Date(todo.dueDate) - new Date() <= 0
          }
        })
        return rs.length
      }
    }

    var docsData = {
      calculateCount: function () {
        let completeCount = 0
        let overDueCount = 0
        let totalCount = 0
        let outstandingCount = 0
        if (!engagement.todos) {
          return {overDueCount, outstandingCount, totalCount, completeCount}
        }
        for (let i = 0; i < engagement.todos.length; i++) {
          let todo = engagement.todos[i]
          totalCount = totalCount + todo.requests.length
          if (!todo.completed) {
            for (let j = 0; j < todo.requests.length; j++) {
              let request = todo.requests[j]
              if (request.currentFile) {
                completeCount++
              } else if (todo.dueDate !== null && (new Date(todo.dueDate) - new Date()) <= 0) {
                overDueCount++
              }
            }
          } else {
            completeCount = completeCount + todo.requests.length
          }
        }
        outstandingCount = totalCount - completeCount - overDueCount
        return {overDueCount, outstandingCount, totalCount, completeCount}
      },
      percent: function (complete, total) {
        if (engagement.todos && (complete !== 0 & total !== 0)) {
          return Math.round((complete / total) * 100) + '% (' +
          complete + '/' + total + ')'
        } else {
          return '0% (0/' + total + ')'
        }
      }
    }

    let engagementInfo = {}
    engagementInfo.business = engagement.business
    engagementInfo._id = engagement._id
    engagementInfo.name = engagement.name
    engagementInfo.type = engagement.type
    engagementInfo.firm = engagement.firm.name
    engagementInfo.status = engagement.status
    const auditorIndex = engagement.acl.findIndex((acl) => {
      return acl.role === 'AUDITOR' && acl.lead === true
    })

    let auditorName
    if (auditorIndex !== -1) {
      let firsName = engagement.acl[auditorIndex].userInfo.firstName
      let lastName = engagement.acl[auditorIndex].userInfo.lastName
      auditorName = `${firsName} ${lastName}`
    } else {
      auditorName = 'None Assigned'
    }
    engagementInfo.leadAuditor = auditorName
    engagementInfo.completedToDo = todoData.percent()
    engagementInfo.completedToDoNumber = todoData.completed()
    engagementInfo.outstandingToDo = todoData.outstanding()
    engagementInfo.totalToDo = todoData.total()
    engagementInfo.overdueToDo = todoData.overdue()

    const clientIndex = engagement.acl.findIndex((acl) => {
      return acl.role === 'CLIENT' && acl.lead === true
    })

    let clientName
    if (clientIndex !== -1) {
      let firsName = engagement.acl[clientIndex].userInfo.firstName
      let lastName = engagement.acl[clientIndex].userInfo.lastName
      clientName = `${firsName} ${lastName}`
    } else {
      clientName = 'None Assigned'
    }

    engagementInfo.leadClient = clientName

    let docCount = docsData.calculateCount()
    engagementInfo.outstandingDoc = docCount.outstandingCount
    engagementInfo.totalDoc = docCount.totalCount
    engagementInfo.overdueDoc = docCount.overDueCount
    engagementInfo.completedDocs = docsData.percent(docCount.completeCount, docCount.totalCount)
    engagementInfo.completedDocsNumber = docCount.completeCount

    if (engagement.lastUpdated) {
      engagementInfo.lastUpdated = new Date(engagement.lastUpdated)
    }

    if (engagement.dueDate) {
      engagementInfo.dueDate = new Date(engagement.dueDate)
    }

    return engagementInfo
  },

  validateStartEndDate: function (dateRangeStart, dateRangeEnd) {
    var startDate = new Date(dateRangeStart)
    var endDate = new Date(dateRangeEnd)

    if (startDate < endDate) {
      return false
    } else {
      return true
    }
  },

  sendAjax: function(url, callback){
    $.ajax({
      type: "GET",
      url: url,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function (data) {
        callback(data);
      }
    })
  }
}

module.exports = Utility
