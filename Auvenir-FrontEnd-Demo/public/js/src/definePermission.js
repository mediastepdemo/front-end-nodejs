const EngagementPermissions = {
  AUDITOR_R_TEAM: 'AUDITOR_R_TEAM',
  CLIENT_R_TEAM: 'CLIENT_R_TEAM',
  TODO_W_ADD: 'TODO_W_ADD',
  INVITE_CLIENT: 'INVITE_CLIENT'
}
const TodoPermissions = {
  GENERAL_W: 'GENERAL_W',
  STATUS_W: 'STATUS_W',
  STATUS_W_COMPLETE: 'STATUS_W_COMPLETE',
  AUDITOR_ASSIGNEE_W: 'AUDITOR_ASSIGNEE_W',
  CLIENT_ASSIGNEE_W_LEAD: 'CLIENT_ASSIGNEE_W_LEAD',
  CLIENT_ASSIGNEE_W_GENERAL: 'CLIENT_ASSIGNEE_W_GENERAL',
  REQUEST_W: 'REQUEST_W',
  FILE_W: 'FILE_W',
  COMMENT_W: 'COMMENT_W',
  BULK_W: 'BULK_W'
}
const Roles = {
  ADMIN_CLIENT: [EngagementPermissions.CLIENT_R_TEAM],
  LEAD_CLIENT: [
    EngagementPermissions.CLIENT_R_TEAM,
    TodoPermissions.CLIENT_ASSIGNEE_W_GENERAL,
    TodoPermissions.FILE_W,
    TodoPermissions.COMMENT_W
  ],
  CLIENT: [
    TodoPermissions.FILE_W,
    TodoPermissions.COMMENT_W
  ],
  ADMIN_AUDITOR: [
    EngagementPermissions.AUDITOR_R_TEAM
  ],
  LEAD_AUDITOR: [
    EngagementPermissions.INVITE_CLIENT,
    EngagementPermissions.AUDITOR_R_TEAM,
    TodoPermissions.GENERAL_W,
    TodoPermissions.STATUS_W_COMPLETE,
    TodoPermissions.AUDITOR_ASSIGNEE_W,
    TodoPermissions.CLIENT_ASSIGNEE_W_LEAD,
    TodoPermissions.REQUEST_W,
    TodoPermissions.FILE_W,
    TodoPermissions.COMMENT_W,
    EngagementPermissions.TODO_W_ADD,
    TodoPermissions.BULK_W
  ],
  AUDITOR: [
    TodoPermissions.GENERAL_W,
    TodoPermissions.STATUS_W_COMPLETE,
    // TodoPermissions.AUDITOR_ASSIGNEE_W,
    TodoPermissions.CLIENT_ASSIGNEE_W_LEAD,
    TodoPermissions.REQUEST_W,
    TodoPermissions.FILE_W,
    TodoPermissions.COMMENT_W,
    EngagementPermissions.TODO_W_ADD,
    TodoPermissions.BULK_W
  ]
}

module.exports = {
  EngagementPermissions: EngagementPermissions,
  TodoPermissions: TodoPermissions,
  Roles: Roles
}
