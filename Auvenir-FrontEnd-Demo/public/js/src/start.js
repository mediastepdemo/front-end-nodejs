import Controller from '../../core/Controller'

startApp()

function startApp () {
  var appCtrl = new Controller()
  appCtrl.runApplication()
}

module.exports = startApp
