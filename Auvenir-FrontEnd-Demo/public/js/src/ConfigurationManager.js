/* globals XMLHttpRequest, ga */

/**
 * The Configuration class is responsible for retrieving a JSON configuration object and loading the
 * dependencies and resources into the DOM. This class has zero dependencies on third party libraries,
 * and is a pure JS implementation that can be used in multiple projects.
 * @class ConfigurationManager
 */
var ConfigurationManager = function () {
  var self = this
  var name = 'CM'

  var currentConfiguration = null
  var currentEnvironment = null
  var currentSettings = {
    protocol: '',
    domain: '',
    port: '',
    path: ''
  }

  var googleAdded = false
  var hotJarAdded = false

  /**
   * Initializes the configuration manager
   * Also attaches a visual representation of the loading of resources
   * @memberof ConfigurationManager
   */
  var initialize = function () {
    var pBarDiv = document.createElement('div')
    pBarDiv.setAttribute('id', 'config-loader')
    pBarDiv.setAttribute('class', 'config-container')

    var barDiv = document.createElement('div')
    barDiv.setAttribute('id', 'config-bar')
    barDiv.setAttribute('class', 'config-bar')
    pBarDiv.appendChild(barDiv)
    document.body.appendChild(pBarDiv)
  }

  /**
   * @memberof ConfigurationManager
   */
  this.getSettings = function () {
    return currentSettings
  }

  /**
   * @memberof ConfigurationManager
   */
  this.getEnvironment = function () {
    return currentEnvironment
  }

  /**
   * @memberof ConfigurationManager
   */
  this.getConfiguration = function () {
    return currentConfiguration
  }
  /**
   * Adds Google Analytics functionality to the project.
   * @memberof ConfigurationManager
   * @param {String} accountID
   */
  var addGoogleAnalytics = function (accountID) {
    if (googleAdded) {
      return
    } else {
      (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r
        i[r] = i[r] || function () {
          (i[r].q = i[r].q || []).push(arguments)
        }
        i[r].l = 1 * new Date()
        a = s.createElement(o)
        m = s.getElementsByTagName(o)[0]
        a.async = 1
        a.src = g
        m.parentNode.insertBefore(a, m)
      })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga')
      ga('create', accountID, 'auto')
      ga('send', 'pageview')
      googleAdded = true
    }
  }

  /**
   * Adds HotJara functionality to the project for tracking user utilization.
   * @memberof ConfigurationManager
   * @param {String} accountID
   */
  var addHotJar = function (accountID) {
    if (hotJarAdded) {
      return
    } else {
      (function (h, o, t, j, a, r) {
        h.hj = h.hj || function () {
          (h.hj.q = h.hj.q || []).push(arguments)
        }
        h._hjSettings = {
          hjid: 49781,
          hjsv: 5
        }
        a = o.getElementsByTagName('head')[0]
        r = o.createElement('script')
        r.async = 1
        r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv
        a.appendChild(r)
      })(window, document, '//static.hotjar.com/c/hotjar-', '.js?sv=')
      hotJarAdded = true
    }
  }

  /**
   * Sends and XMLHttpRequest to the server to retrieve the configuration for the currently
   * authorized user.
   * @memberof ConfigurationManager
   * @param {Function} callback
   */
  this.retrieveConfiguration = function (callback) {
    var xhr = new XMLHttpRequest()
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          var json = JSON.parse(xhr.responseText)
          loadConfiguration(json, callback)
        } else {
          callback({code: 1, msg: xhr.statusText})
        }
      }
    }
    xhr.open('GET', '/config', true)
    xhr.send()
  }

  /**
   * Triggered when a configuration is pulled down from the server. It is responsible for loading the
   * passed in configuration variables into memory, and also downloading any dependencies before doing
   * the same thing for any resources.
   * @memberof ConfigurationManager
   * @param {Object}   config
   * @param {Function} callback
   */
  var loadConfiguration = function (config, callback) {
    if (!config) {
      callback({code: 1, msg: 'Missing Configuration Object'})
      return
    }

    if (!config.environment || !config.connection) {
      callback({code: 1, msg: 'Unable to load configuration, invalid environment setup'})
      return
    }

    currentConfiguration = config
    currentEnvironment = config.environment
    currentSettings.protocol = config.connection.protocol
    currentSettings.domain = config.connection.domain
    currentSettings.port = config.connection.port
    currentSettings.path = '/'
    currentSettings.apiWorkingPagerBaseUrl = config.workingpaperapi.baseUrl

    if (config.features && config.features.googleAnalytics && config.features.googleAnalytics.id) {
      addGoogleAnalytics(config.features.googleAnalytics.id)
    }

    if (config.features && config.features.hotJar && config.features.hotJar.id) {
      addHotJar(config.features.hotJar.id)
    }
    // Return user type
    callback({code: 0, msg: 'Loaded.', userType: config.type})
  }

  initialize()

  return this
}

module.exports = ConfigurationManager
