// This code is executed immediately and is responsible for retrieving basic session information about the
// user which can be used to determine wether we want to automatically trigger the sending of an email.
var urlParameters = window.location.search
var currentUser = null
var userFullName = ''
var analytics = {}
var cookieEmail = ''

var xhr = new XMLHttpRequest()
xhr.onreadystatechange = function () {
  if (xhr.readyState !== 4) {
    return
  }

  var response = JSON.parse(xhr.responseText)
  if (response.data && response.data.flash && response.data.flash !== '') {
    // Currently we are only outputting to an alert until design/UX can come
    // up with a way of doing it. One way is to update the text of the popup for id=landing-welcomeBack
  }

  if (response.status === 200) {
    currentUser = response.data
    updateUser()
    updateDom()
    if (currentUser.status.toLowerCase() === 'current') {
      setPopUp('user')
      setTimeout(function () {
        togglePopUp(isMobile)
      }, 1200)
    } else if (currentUser.status.toLowerCase() === 'previous') {
      cookieEmail = currentUser.email
      recognizeUser('top', currentUser.mobile)
    }
  } else {
    setTimeout(function () {
      setPopUp('auditor')
      togglePopUp(isMobile)
    }, 800)
  }
}

xhr.open('GET', '/session-info', true)
xhr.send();

(function () {
  var xhr = new XMLHttpRequest()
  xhr.open('GET', '/landingAnalytics')
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        try {
          analytics = JSON.parse(xhr.responseText)

          // GOOGLE ANALYTICS INTEGRATION
          var addGA = function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r
            i[r] = i[r] || function () {
              (i[r].q = i[r].q || []).push(arguments)
            }
            i[r].l = 1 * new Date()
            a = s.createElement(o)
            m = s.getElementsByTagName(o)[0]
            a.async = 1
            a.src = g
            m.parentNode.insertBefore(a, m)
          }
          addGA(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga')
          ga('create', analytics.google_analytics.id, 'auto')
          ga('send', 'pageview')

          var addHotjar = function (h, o, t, j, a, r) {
            h.hj = h.hj || function () {
              (h.hj.q = h.hj.q || []).push(arguments)
            }
            h._hjSettings = {
              hjid: analytics.hot_jar.id,
              hjsv: analytics.hot_jar.hjsv
            }
            a = o.getElementsByTagName('head')[0]
            r = o.createElement('script')
            r.async = 1
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv
            a.appendChild(r)
          }
          addHotjar(window, document, '//static.hotjar.com/c/hotjar-', '.js?sv=')

          window.intercomSettings = { app_id: analytics.intercom.id }
          var addIntercom = function () {
            var w = window
            var ic = w.Intercom
            if (typeof ic === 'function') {
              ic('reattach_activator')
              ic('update', intercomSettings)
            } else {
              var d = document
              var i = function () {
                i.c(arguments)
              }
              i.q = []
              i.c = function (args) {
                i.q.push(args)
              }
              w.Intercom = i

              var s = d.createElement('script')
              s.type = 'text/javascript'
              s.async = true
              s.src = 'https://widget.intercom.io/widget/' + analytics.intercom.id
              var x = d.getElementsByTagName('script')[0]
              x.parentNode.insertBefore(s, x)
            }
          }
          addIntercom()
        } catch (err) {
          return
        }
      }
    }
  }
  xhr.send()
})()

/**
 * Responsible for updating user information that is being tracked and displayed on the screen.
 */
function updateUser () {
  var userFirstName
  var json = currentUser

  if (json.firstName === '' && json.lastName !== '') {
    userFirstName = json.lastName
    userFullName = json.lastName
  } else if (json.firstName !== '' && json.lastName === '') {
    userFirstName = json.firstName
    userFullName = json.firstName
  } else if (json.firstName === '' && json.lastName === '') {
    userFirstName = json.email
    userFullName = json.email
  } else {
    userFirstName = json.firstName
    userFullName = json.firstName + ' ' + json.lastName
  }

  document.getElementById('landing-NameBtn').innerHTML = userFirstName
  document.getElementById('landProf-fullName').innerHTML = userFullName
  document.getElementById('landProf-email').innerHTML = json.email
  document.getElementById('aud-dd-fullName').innerHTML = userFullName
  document.getElementById('aud-dd-email').innerHTML = json.email

  setTimeout(function () {
    if (!userFirstName) {
      document.getElementById('land-dropDown-firstName').innerHTML = userFirstName
    }

    if (!json.email) {
      document.getElementById('land-dropDown-email').innerHTML = json.email
    }
  }, 250)
}

// add elipsers if name too long for minimal font-size of 12px
function addElipses (name, elem) {
  var longName = name
  longName = longName.substring(0, 25) + '...'
  document.getElementById(elem).innerHTML = longName
}

// check for elipses
function elipsesCheck (elem) {
  if (!elem) {
    var fontSize = document.getElementById(elem).style.fontSize
    var fontSizeNoPx = parseInt(fontSize, 10)
    if (fontSizeNoPx <= 12) {
      document.getElementById(elem).style.fontSize = '12px'
      addElipses(document.getElementById(elem).innerHTML, elem)
    }
  }
}

/* SETTING FONT SIZE OF NAME BASED ON WIDTH */

function adjustFontSize (elem) {
  var fontstep = 2
  if ($(elem).height() > $(elem).parent().height() || $(elem).width() > $(elem).parent().width()) {
    $(elem).css('font-size', (($(elem).css('font-size').substr(0, 2) - fontstep)) + 'px').css('line-height', (($(elem).css('font-size').substr(0, 2))) + 'px')
    adjustFontSize(elem)
  }
}

/**
 * Sets the pop up (animation from top of screen) to a certain DOM mode of either
 * USER or AUDITOR
 * @param {String} type
 */
function setPopUp (type) {
  switch (type) {
    case 'user':
      document.getElementById('landing-popUp-auditor').style.display = 'none'
      document.getElementById('landing-popUp-user').style.display = 'block'
      break
    case 'auditor':
      document.getElementById('landing-popUp-auditor').style.display = 'block'
      document.getElementById('landing-popUp-user').style.display = 'none'
      break
    default:
  }
}

/**
 * Will either display or hide the pop up, regardless of the mode it is in.
 */
function togglePopUp (text) {
  if (text) {
    $('#audLanding-welcomeBack').html(text)
  } else {
    $('#audLanding-welcomeBack').html('Welcome! Please check your email for a login link.')
  }
  var main = $('main')
  var val = !main.hasClass('nav-is-visible')
  main.toggleClass('nav-is-visible', val)
  $('.cd-3d-nav-container').toggleClass('nav-is-visible', val)
}

/**
 * Responsible for when a success link has been triggered (Email)
 * Triggered whenever an email is sent out.
 */
function updateDom () {
  if (currentUser) {
    // set display none to log in buttons and display the new button
    document.getElementById('land-login-btnContainer').style.display = 'none'
    document.getElementById('land-busBtnHolder').style.display = 'none'
    document.getElementById('land-login-dropdown').style.display = 'none'
    document.getElementById('land-profile').style.display = 'block'
    if (document.getElementById('land-profile-dropdown-container').classList.contains('land-profileDropContainer-after')) {
      document.getElementById('land-profile-dropdown-container').classList.toggle('land-profileDropContainer-after')
    }
    if (document.getElementById('land-secondaryLogin').classList.contains('land-loginDropdown-after')) {
      openEmailInput()
    }
  }
  document.getElementById('land-login-errorDiv').style.display = 'none'
  document.getElementById('land-profile-dropdown').style.display = 'block'
  document.getElementById('land-loginDropdownSecondary').style.display = 'none'
  document.getElementById('preloaderBtn').style.display = 'none'
  document.getElementById('preloaderBtnSecondary').style.display = 'none'
}

/**
 * this is the first input field when you want to log in
 */
function recognizeUser_Top () {
  var email = document.getElementById('emailField-top')
  var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
  if (!filter.test(email.value)) {
    document.getElementById('land-login-notVerified').style.display = 'none'
    document.getElementById('land-login-errorDiv').style.display = 'block'
    return false
  }
  cookieEmail = document.getElementById('emailField-top').value
  // document.getElementById('land-login-errorDiv').style.display = 'none'
  document.getElementById('land-login-notVerified').style.display = 'none'
  document.getElementById('preloaderBtn').style.display = 'block'
  recognizeUser('top')
}

/**
 * This is the input field when you are trying to switch users
 */
function recognizeUser_Secondary () {
  var email = document.getElementById('land-emailField-secondaryTop')
  var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
  if (!filter.test(email.value)) {
    document.getElementById('land-login-notVerifiedSecondary').style.display = 'none'
    document.getElementById('land-login-secondaryErrorDiv').style.display = 'block'
    return false
  }
  cookieEmail = document.getElementById('land-emailField-secondaryTop').value
  document.getElementById('land-login-secondaryErrorDiv').style.display = 'none'
  document.getElementById('land-login-notVerifiedSecondary').style.display = 'none'
  document.getElementById('preloaderBtnSecondary').style.display = 'block'
  recognizeUser('sec')
}

/**
 * Triggered whenever we have an email. Sends a message to the server and will return user info and may
 * also send out an email.
 * @param {String} trig - The trigger string that is used.
 */
function recognizeUser (trig, isMobile) {
  var uri = '/login'

  var xhr = new XMLHttpRequest()
  var fd = new FormData()

  xhr.onreadystatechange = function () {
    if (xhr.readyState !== 4) {
      return
    }
    if (~xhr.responseURL.indexOf('home')) {
      window.location = xhr.responseURL
      return
    }

    var json = JSON.parse(xhr.responseText)
    if (json.redirect) {
      window.location = json.redirect
      return
    }

    var preLoadBtn = document.getElementById('preloaderBtn')
    var preLoadSecBtn = document.getElementById('preloaderBtnSecondary')
    var landLoginErr = document.getElementById('land-login-errorDiv')
    var landLoginSecErr = document.getElementById('land-login-secondaryErrorDiv')

    // If initial login, basic functionality
    if (trig === 'top') {
      if (json.code === 200) {
        currentUser = json
      }
      if ($('main').hasClass('nav-is-visible')) {
        togglePopUp(isMobile)
        setTimeout(function () {
          setPopUp('user')
        }, 200)
        setTimeout(function () {
          togglePopUp(isMobile)
        }, 800)
      } else {
        setPopUp('user')
        setTimeout(function () {
          togglePopUp(isMobile)
        }, 200)
      }
      preLoadBtn.style.display = 'none'
    // If already remembered from cookie, then more detailed functionality
    } else if (trig === 'sec') {
      switch (json.code) {
        case 202:
          updateDom()
          break
        case 200:
          currentUser = json
          updateUser()
          updateDom()
          if ($('main').hasClass('nav-is-visible')) {
            togglePopUp(isMobile)
            setTimeout(function () {
              setPopUp('user')
            }, 200)
            setTimeout(function () {
              togglePopUp(isMobile)
            }, 800)
          } else {
            setPopUp('user')
            setTimeout(function () {
              togglePopUp(isMobile)
            }, 200)
          }
          preLoadBtn.style.display = 'none'
          break
        case 0:
          currentUser = json
          updateUser()
          updateDom()
          if ($('main').hasClass('nav-is-visible')) {
            togglePopUp(isMobile)
            setTimeout(function () {
              setPopUp('user')
            }, 200)
            setTimeout(function () {
              togglePopUp(isMobile)
            }, 800)
          } else {
            setPopUp('user')
            setTimeout(function () {
              togglePopUp(isMobile)
            }, 200)
          }
          preLoadSecBtn.style.display = 'none'
          landLoginSecErr.style.display = 'block'
          break
        case 206:
          landLoginSecErr.style.display = 'block'
          preLoadSecBtn.style.display = 'none'
          if (trig !== 'top' && trig !== 'sec') {
            document.getElementById('landing-popUp-auditor').style.display = 'block'
            document.getElementById('landing-popUp-user').style.display = 'none'
            setTimeout(function () {
              togglePopUp(isMobile)
            }, 1200)
          }
          break
        default:
          updateDom()
          landLoginSecErr.style.display = 'block'
          preLoadSecBtn.style.display = 'none'
          break
      }
      if (json.redirect) {
        window.location = json.redirect
      }
    }
    if (document.getElementById('land-login-dropdown').classList.contains('land-loginDropdown-after')) {
      loginDropdownToggle()
      clearFields()
    }
  }
  xhr.open('POST', uri, true)
  fd.append('email', cookieEmail)
  fd.append('status', 'registrationCheck')
  fd.append('type', 'CLIENT')
  xhr.send(fd)
}

/**
 * Triggers email signup with the modal popup. Based on a button click
 */
function submitEmail () {
  // Email sumission
  // Check where the request is coming from and set clientsemail accordingly
  var clientsEmail = null
  var emailField = document.getElementById('emailField')
  if (emailField.value !== '') {
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
    if (!filter.test(emailField.value)) {
      emailField.focus()
      return false
    }
    clientsEmail = emailField.value
    document.getElementById('landing-modal-LoginBtn').setAttribute('data-toggle', 'modal')
  } else if (document.getElementById('landProf-email').value !== '') {
    clientsEmail = document.getElementById('landProf-email').innerHTML
    document.getElementById('landProf-loginBtn').setAttribute('data-toggle', 'modal')
  }

  var uri = '/login'

  var xhr = new XMLHttpRequest()
  var fd = new FormData()

  xhr.onreadystatechange = function () {
    if (xhr.readyState !== 4) {
      return
    }
    if (~xhr.responseURL.indexOf('home')) {
      window.location = xhr.responseURL
      return window.location
    }

    $('#emailField').blur()
    try {
      var json = JSON.parse(xhr.responseText)
      if (json.redirect) {
        window.location = json.redirect
      }
      document.getElementById('preloaderModal').style.display = 'none'
      var loginImg = document.getElementById('landing-modal-loginImg')
      var loginHeader = document.getElementById('landing-modal-loginHeader')
      var loginSubHeader = document.getElementById('landing-modal-loginSubHeader')
      var doneBtn = document.getElementById('landingModalDoneBtn')
      switch (json.code) {
        case 0:
          loginImg.src = '../images/illustrations/Waitlist.svg'
          loginHeader.innerHTML = json.msg
          loginSubHeader.innerHTML = json.msg2
          doneBtn.className = 'btn landing-modal-button'
          break
        case 200:
          loginImg.src = json.msg3
          loginHeader.innerHTML = json.msg
          loginSubHeader.innerHTML = json.msg2 + ' ' + clientsEmail
          doneBtn.className = 'btn landing-modal-button'
          break
        case 201:
          loginImg.src = json.msg3
          loginHeader.innerHTML = json.msg
          loginSubHeader.innerHTML = json.msg2
          doneBtn.className = 'btn landing-modal-button'
          break
        case 24:
          loginImg.src = '../images/illustrations/Waitlist.svg'
          loginHeader.innerHTML = json.msg
          loginSubHeader.innerHTML = 'Refresh your page and try again.'
          doneBtn.className = 'btn landing-modal-button'
          break
        case 206:
          loginImg.src = '../images/illustrations/Waitlist.svg'
          loginHeader.innerHTML = json.msg
          loginSubHeader.innerHTML = json.msg2
          doneBtn.className = 'btn landing-modal-button'
          break
        default:
          loginHeader.innerHTML = json.msg
      }
    } catch (err) {
      log(err)
    }
  }

  xhr.open('POST', uri, true)
  fd.append('email', clientsEmail)
  fd.append('type', 'CLIENT')
  xhr.send(fd)
}

/**
 * Triggered when the user clicks the "Send Message" for the Get In Touch Form
 */
function submitUserMessage () {
  var name = document.getElementById('landing-userName').value
  var email = document.getElementById('landing-userEmail').value
  var message = document.getElementById('landing-userMessage').value
  var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/

  if (!filter.test(email)) {
    return false
  } else if (name === '' || email === '' || message === '') {
    return false
  } else {
    document.getElementById('landing-modal-msgBtn').setAttribute('data-toggle', 'modal') // modal triggered here?

    var uri = '/userMessage'
    var xhr = new XMLHttpRequest()
    var fd = new FormData()

    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        var json = JSON.parse(xhr.responseText)

        if (json && json.code === 0) {
          $('#landingUserMessageSent').modal('toggle')
        }
      }
    }

    xhr.open('POST', uri, true)
    fd.append('email', email)
    fd.append('name', name)
    fd.append('message', message)
    xhr.send(fd)
  }
}

// this is for the mobile security flows that will be accessed at a later date (almccoo 08/12/2016)
/**
 * Not current being used (MOBILE LOGIN)
 */
function submitEmailModal () {
  // Email sumission
  var newEmail = ''
  var email = null
  var filter = null
  // Check where the request is coming from and set clientsemail accordingly
  if (document.getElementById('modal-emailInputThree').value !== '') {
    email = document.getElementById('modal-emailInputThree')
    filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
    if (!filter.test(email.value)) {
      email.focus()
      if (document.getElementById('modal-emailInputThree').value !== '') {
        document.getElementById('modal-alert-containerThree').style.display = 'inline'
      }
      return false
    }
    newEmail = document.getElementById('modal-emailInputThree').value
    document.getElementById('modal-emailBtn').setAttribute('data-toggle', 'modal')
  } else if (document.getElementById('modal-emailInputOne').value !== '') {
    email = document.getElementById('modal-emailInputOne')
    filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
    if (!filter.test(email.value)) {
      email.focus()
      if (document.getElementById('modal-emailInputOne').value !== '') {
        document.getElementById('modal-alert-containerOne').style.display = 'block'
      }
      return false
    }
  }
  var uri = '/login'
  var xhr = new XMLHttpRequest()
  var fd = new FormData()
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      if (xhr.responseURL.indexOf('home') !== -1) {
        window.location = xhr.responseURL
      } else {
        var json = JSON.parse(xhr.responseText)
        if (json.code === 200) {
          if (document.getElementById('modal-emailInputOne').value !== '') {
            document.getElementById('modal-success-containerOne').style.display = 'inline'
          }
          if (document.getElementById('modal-emailInputThree').value !== '') {
            document.getElementById('modal-success-containerThree').style.display = 'inline'
          }
        } else {
          if (document.getElementById('modal-emailInputOne').value !== '') {
            document.getElementById('modal-alert-containerOne').style.display = 'block'
          }
          if (document.getElementById('modal-emailInputThree').value !== '') {
            document.getElementById('modal-alert-containerThree').style.display = 'inline'
          }
        }
      }
    }
  }
  xhr.open('POST', uri, true)
  fd.append('email', newEmail)
  fd.append('status', 'registrationCheck')
  fd.append('type', 'CLIENT')
  xhr.send(fd)
}

// Triggers on an ENTER key
document.getElementById('land-emailField-secondaryTop').onkeydown = function (event) {
  if (event.keyCode === 13) {  // ENTER
    recognizeUser_Secondary()
  }
}

$('#landingEmailSent').on('hidden.bs.modal', function () {
  clearFields()
})

$('#landingUserMessageSent').on('hidden.bs.modal', function () {
  clearFields()
})

/**
 * Replace all SVG images with inline SVG
 */
jQuery('img.svg').each(function () {
  var $img = jQuery(this)
  var imgID = $img.attr('id')
  var imgClass = $img.attr('class')
  var imgURL = $img.attr('src')

  jQuery.get(imgURL, function (data) {
    // Get the SVG tag, ignore the rest
    var $svg = jQuery(data).find('svg')

    // Add replaced image's ID to the new SVG
    if (typeof imgID !== 'undefined') {
      $svg = $svg.attr('id', imgID)
    }
    // Add replaced image's classes to the new SVG
    if (typeof imgClass !== 'undefined') {
      $svg = $svg.attr('class', imgClass + ' replaced-svg')
    }

    // Remove any invalid XML tags as per http://validator.w3.org
    $svg = $svg.removeAttr('xmlns:a')

    // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
    if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
      $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
    }

    // Replace image with new SVG
    $img.replaceWith($svg)
  }, 'xml')
})

function clearFields () {
  document.getElementById('emailField').value = ''
  document.getElementById('emailField-top').value = ''
  document.getElementById('landing-userEmail').value = ''
  document.getElementById('landing-userName').value = ''
  document.getElementById('landing-userMessage').value = ''
  document.getElementById('preloaderModal').style.display = 'inherit'
  document.getElementById('landing-modal-loginImg').src = ''
  document.getElementById('landing-modal-loginHeader').innerHTML = ''
  document.getElementById('landing-modal-loginSubHeader').innerHTML = ''
  document.getElementById('landingModalDoneBtn').className = 'landing-displayNone'
  document.getElementById('landing-modal-messageHead').innerHTML = 'Success!'
  document.getElementById('landing-modal-messageSub').innerHTML = 'We\'ll be in touch shortly.'
  document.getElementById('landing-modal-msgBtn').removeAttribute('data-toggle')
  document.getElementById('landing-modal-LoginBtn').removeAttribute('data-toggle')
}

function loginDropdownToggle () {
  document.getElementById('landing-loginBtn').classList.toggle('landing-login-btn-after')
  document.getElementById('landing-loginChevron').classList.toggle('landing-login-btn-after')
  document.getElementById('land-login-btnContainer').classList.toggle('land-loginBtnContainer-after')
  document.getElementById('land-login-dropdown').classList.toggle('land-loginDropdown-after')
}

function profileDropdown () {
  document.getElementById('land-profile-dropdown-container').classList.toggle('land-profileDropContainer-after')
  adjustFontSize('#landProf-fullName')
  adjustFontSize('#landProf-email')
  elipsesCheck('landProf-fullName')
  elipsesCheck('landProf-email')
}

// Clicking 'login with a different email'
function openEmailInput () {
  document.getElementById('land-loginDiffHolder').classList.toggle('audLand-ld-diffEmailHolder-after')
  document.getElementById('land-secondaryLogin').classList.toggle('land-loginDropdown-after')
}

// Clicking 'switch account'
function openSwitchAccount () {
  document.getElementById('land-profile-dropdown').style.display = 'none'
  document.getElementById('land-loginDropdownSecondary').style.display = 'block'
  adjustFontSize('#aud-dd-fullName')
  adjustFontSize('#audLand-ld-email')
  elipsesCheck('aud-dd-fullName')
  elipsesCheck('audLand-ld-email')
}

function removeLearnMore () {
  document.getElementById('landing-fixedLearnMore').style.display = 'none'
}

// submit on enter press
$(document).ready(function () {
  $('#emailField-top').keyup(function (e) {
    if (e.keyCode === 13) { // ENTER
      $('#land-loginBtn').click()
    }
  })
})
// If body is at top make learn more button appear
if (document.body.scrollTop === 0) {
  document.getElementById('landing-fixedLearnMore').style.display = 'block'
}
// Remove learn More button on scroll
$(document).scroll(function () {
  removeLearnMore()
})

// Clicking outside of dropdowns will close them.
// For Login Dropdown
$(document).mouseup(function (e) {
  var containerLogin = $('#land-login-dropdown')
  var loginButton = $('#landing-loginBtn')
  var containerProfile = $('#land-profile-dropdown-container')
  var profileButton = $('#landing-NameBtn')
  if (!containerLogin.is(e.target) && containerLogin.has(e.target).length === 0 && document.getElementById('land-login-dropdown').classList.contains('land-loginDropdown-after') && !loginButton.is(e.target)) {
    loginDropdownToggle()
  }
  if (!containerProfile.is(e.target) && containerProfile.has(e.target).length === 0 && document.getElementById('land-profile-dropdown-container').classList.contains('land-profileDropContainer-after') && !profileButton.is(e.target)) {
    profileDropdown()
    if (document.getElementById('land-loginDropdownSecondary').style.display === 'block') {
      document.getElementById('land-loginDropdownSecondary').style.display = 'none'
      document.getElementById('land-profile-dropdown').style.display = 'block'
    }
  }
})

// add script to hit url and grab config analytics options and set them
