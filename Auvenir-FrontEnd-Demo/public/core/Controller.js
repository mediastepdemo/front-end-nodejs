'use strict'

import Utility from '../js/src/utility_c'
import ConfigurationManager from '../js/src/ConfigurationManager'
import log from '../js/src/log'
import Socket from './Socket'
import FlashAlert from './widgets/flash-alert/flash-alert'
import GDriveManager from './widgets/gdrive-manager/gdrive-manager'
import COMMON from '../js/src/common'
import _ from 'lodash'

// Views
import Header from './views/header/Header'
import ClientSettings from './views/settings/ClientSettings'
import AdminSettings from './views/settings/AdminSettings'
import AuditorSettings from './views/settings/AuditorSettings'
import Engagement from './views/engagement/Engagement'
import Onboarding from './views/onboarding/Onboarding'
import Admin from './views/admin/Admin'
import Auditor from './views/auditor/Auditor'
import Client from './views/client/Client'
import EngagementPreview from './views/engagement-preview/engagementPreview'
import ClientList from './views/client-list/ClientList'
import Permission from './Permission'

// Modals
import Modal_Custom from './modals/custom/Modal_Custom'
import Modal_Message from './modals/message/Modal_Message'
import Modal_DeleteRequest from './modals/delete-request/Modal_DeleteRequest'
import Modal_Deactivate from './modals/deactivate/Modal_Deactivate'
import Modal_PrivacyPolicy from './modals/agreements/Modal_PrivacyPolicy'
import Modal_TermsConditions from './modals/agreements/Modal_TermsConditions'
import Modal_Archiving from './modals/archiving/Modal_Archiving'
import Modal_Offline from './modals/offline/Modal_Offline'
import Modal_AddNewClient from './modals/add-new-client/Modal_AddNewClient'
import Modal_InviteClient from './modals/invite-client/Modal_InviteClient'
import Modal_RemoveClient from './modals/modal-removeClient/Modal_RemoveClient'
import Modal_ArchiveEngagement from './modals/modal-archiveEngagement/Modal_ArchiveEngagement'
import Modal_EditClient from './modals/edit-client/Modal_EditClient'
import Modal_DeleteRequestFile from './modals/delete-request-file/Modal_DeleteRequestFile'
import Modal_CreateEngagement from './modals/create-engagement/Modal_CreateEngagement'
import Modal_FirmAffiliation from './modals/firm-affiliation/Modal_FirmAffiliation'
import Modal_InviteNewMember from './modals/invite-new-member/Modal_InviteNewMember'
import Modal_LetUsKnow from './modals/let-us-know/Modal_LetUsKnow'
import Modal_AddCategory from './modals/toDoList/Modal_AddCategory'
import Modal_EditCategory from './modals/toDoList/Modal_EditCategory'
import Modal_DeleteToDo from './modals/toDoList/Modal_DeleteToDo'
import Modal_MarkCompleteToDo from './modals/toDoList/Modal_MarkComplete'
import Modal_FileManager from './modals/file-manager/Modal_FileManager'
import Modal_BankIntegration from './modals/bank-integration/Modal_BankIntegration'
import Modal_CreateWorkingPaper from './modals/working-paper/Modal_CreateWorkingPaper'
import Modal_ViewPreview from './modals/preview/Modal_ViewPreview'
import Modal_GenerateWPConfirm from './modals/toDoList/Modal_GenerateWPConfirm'
import Modal_GenerateWPWaiting from './modals/toDoList/Modal_GenerateWPWaiting'
import en from '../language/english/en.js'

/**  CONSTANTS  **/
Controller.LOCAL = 'local'
Controller.DEVELOPMENT = 'dev'
Controller.TEST = 'test'
Controller.PRODUCTION = 'prod'

function Controller () {
  var self = this

  // Configuration manager
  var cm = new ConfigurationManager()

  // Cookie for user e-mail logged in
  var cookies = { environment: '', email: '' }

  // Features
  var socketApp = this.socketApp = true
  var preventWindowDragDrop = true
  this.mySocket = null

  // Modules
  var modules = {}
  var headerModule = null
  var adminModule = null
  var clientModule = null
  var auditorModule = null
  var previewModule = null
  var engagementModule = null
  var clientListModule = null // will be Contacts module
  var onboardingModule = null
  var settingsModule = null
  var currentModule = null
  var previousModule = null

  // Modals
  var customModal = null
  var msgModal = null
  var deactivateModal = null
  var termsModal = null
  var privacyModal = null
  var archiverModal = null
  var offlineModal = null
  var flashAlert = null
  var addClientModal = null
  var inviteClientModal = null
  var editClientModal = null
  var deleteRequestModal = null
  // var deleteFileModal = null
  var removeClientModal = null
  var archiveEngagementModal = null
  var deleteRequestFileModal = null
  var createEngagementModal = null
  var firmAffiliationModal = null
  var inviteNewMemberModal = null
  var letUsKnowModal = null
  var addCategoryModal = null
  var editCategoryModal = null
  var deleteToDoModal = null
  var markCompleteToDoModal = null
  var fileManagerModal = null
  var bankIntegrationModal = null
  // var changeTemplateModal = null
  // var changeDueDateModal = null
  // var changeAllDatesModal = null
  var createWorkingPaperModal = null
  var viewPreviewModal = null;
  var modalGenerateWpConfirm = null
  var modalGenerateWpWaiting = null
  // Permission
  this.permission = null
  // Controller's accessable properties
  this.onboardingPassword = null
  this.currentUser = null
  this.currentBusiness = null
  this.currentFirm = null
  this.currentEngagement = null
  this.currentTeam = []
  this.myEngagements = []
  this.myContacts = []
  this.canceledFiles = []
  this.myPendingUsers = []
  this.myPendingBusiness = []
  this.myLatestActivities = []
  this.undoTodos = []
  this.bankName = ''
  this.workingPaperInfo = {
    workingPaperType: [],
    testType: null,
    techniqueType: []
  }
  this.selectedWorkingPaperInfo = null
  // customData is for connection btw idq mobile & socket
  // this.customData = {}

  // GDrive plugin related
  this.gdriveManager = null
  var gdriveConfig = {}
  this.currentIntegrations = {
    gdrive: {
      uid: ''
    }
  }

  // Will eventually be a switch statement with different languages
  this.lang = en

  /**
   * Start running the application.
   * Main Entry point.
   */
  this.runApplication = function () {
    log('Controller: Starting Application ...')

    if (!navigator.cookieEnabled) {
      document.getElementById('progressOverlay').style.display = 'none'
      self.trigger('flash', { content: 'Auvenir requires cookies to be enabled', type: 'ERROR' })
      return
    }

    retrieveCookieInformation()

    cm.retrieveConfiguration(function (result) {
      if (!result) {
        self.trigger('flash', { content: 'Invalid configuration', type: 'ERROR' })
      } else {
        if (result.code === 0) {
          log('Controller: Configuration loaded ...')
          log('Controller: Initializing modules ...')
          headerModule = new Header(self)
          headerModule.on('settings-click', function () {
            settingsModule.viewAccount()
            self.viewPage('SETTINGS')
          })
          headerModule.on('client-list-link-click', function (evt) {
            self.viewPage('CLIENTLIST')
            headerModule.setCurrentLink(evt.linkID)
          })
          headerModule.on('engagements-link-click', function (evt) {
            self.registerForEvent('get-engagements', 'GET-ENGAGEMENT-FOR-RELOAD-000', function (tagName, data) {
              self.deregisterForEvent('get-engagements', 'GET-ENGAGEMENT-FOR-RELOAD-000')

              if (self.currentUser.type === 'AUDITOR') {
                self.viewPage('AUDITOR')
                previewModule.loadDashboard()
              } else if (self.currentUser.type === 'CLIENT') {
                self.viewPage('CLIENT')
                clientModule.loadDashboard(self.myEngagements)
              }
              headerModule.setCurrentLink(evt.linkID)
            })
            self.getEngagements(self.currentUser._id, self.currentUser.type)
          })
          headerModule.on('header-logo-click', function (evt) {
            if (self.currentUser.type === 'CLIENT') {
              self.viewPage('CLIENT')
              // headerModule.setCurrentLink('h-dashboardLink')
              headerModule.setCurrentLink('h-engagementsLink')
              window.scrollTo(0, 0)
            } else if (self.currentUser.type === 'AUDITOR') {
              self.viewPage('AUDITOR')
              headerModule.setCurrentLink('h-engagementsLink')
              window.scrollTo(0, 0)
            } else if (self.currentUser.type === 'ADMIN') {
              self.viewPage('ADMIN')
              window.scrollTo(0, 0)
            }
          })

          if (result.userType === 'CLIENT') {
            settingsModule = new ClientSettings(self, { display: 'none', scroll: true })
          } else if (result.userType === 'AUDITOR') {
            settingsModule = new AuditorSettings(self, {display: 'none', scroll: true})
          } else if (result.userType === 'ADMIN') {
            settingsModule = new AdminSettings(self, {display: 'none', scroll: true})
          }
          settingsModule.on('flash', (data) => {
            flashAlert.flash(data)
          })

          // Add a class to the body to be able to style based on user type
          $('body').addClass(`user-type-${result.userType.toLowerCase()}`)

          modules = {
            'HEADER': headerModule,
            'ADMIN': adminModule,
            'CLIENT': clientModule,
            'AUDITOR': auditorModule,
            'ONBOARDING': onboardingModule,
            'ENGAGEMENT': engagementModule,
            'CLIENTLIST': clientListModule,
            'SETTINGS': settingsModule,
            'PREVIEW': previewModule
          }

          log('Initializing Modals ...')
          self.permission = new Permission(self)
          customModal = new Modal_Custom(self)
          msgModal = new Modal_Message(self)
          deactivateModal = new Modal_Deactivate(self)
          termsModal = new Modal_TermsConditions(self)
          privacyModal = new Modal_PrivacyPolicy(self)
          archiverModal = new Modal_Archiving(self)
          offlineModal = new Modal_Offline(self)
          inviteClientModal = new Modal_InviteClient(self)
          addClientModal = new Modal_AddNewClient(self)
          removeClientModal = new Modal_RemoveClient(self)
          archiveEngagementModal = new Modal_ArchiveEngagement(self)
          editClientModal = new Modal_EditClient(self)
          deleteRequestFileModal = new Modal_DeleteRequestFile(self)
          deleteRequestModal = new Modal_DeleteRequest(self)
          createEngagementModal = new Modal_CreateEngagement(self)
          firmAffiliationModal = new Modal_FirmAffiliation(self)
          inviteNewMemberModal = new Modal_InviteNewMember(self)
          letUsKnowModal = new Modal_LetUsKnow(self)
          addCategoryModal = new Modal_AddCategory(self)
          editCategoryModal = new Modal_EditCategory(self)
          deleteToDoModal = new Modal_DeleteToDo(self)
          markCompleteToDoModal = new Modal_MarkCompleteToDo(self)
          fileManagerModal = new Modal_FileManager(self)
          bankIntegrationModal = new Modal_BankIntegration(self)
          createWorkingPaperModal = new Modal_CreateWorkingPaper(self)
          viewPreviewModal = new Modal_ViewPreview(self)
          modalGenerateWpConfirm = new Modal_GenerateWPConfirm(self)
          modalGenerateWpWaiting = new Modal_GenerateWPWaiting(self)
          archiveEngagementModal.setupListeners()
          archiveEngagementModal.on('done', function (e) {
            self.loadEngagement(e.engagementID, e.page)
          })
          archiverModal.on('done', function () {
            self.viewPage('AUDITOR')
          })
          inviteClientModal.on('engagementInvite', function (evt) {
            self.sendEngagementInvite(evt.id, evt.email)
          })
          deactivateModal.on('deactive', function () {
            self.deactivateAccount()
          })
          deleteRequestFileModal.on('delete-file', function (data) {
            self.sendEvent({name: 'delete-file', data: data})
          })

          flashAlert = new FlashAlert()
          initializeFeatures()
        }
      }
      var isChrome = !!window.chrome && !!window.chrome.webstore
      if (!isChrome) {
        self.displayMessage('<img src="images/logos/auvenir/logo_big.svg" height="50px" style="padding-bottom:30px"/><br/><br/><b style="color:#518d9a">We currently only support the latest version of Chrome<br/> Please try using <a href="http://www.google.com/chrome/browser/desktop/" style="color:#599ba1"> Chrome </a> in order to experience the full Auvenir immersion.</b>')
      }
    })
  }

  /*****************************************************************************
   *                         General Private Functions
   *****************************************************************************/
  /**
   * Retrieves the cookie information and sets the appropriate variables in the
   * application. This cookie information should NOT be sensitive information.
   */
  var retrieveCookieInformation = function () {
    log('Controller: Retrieving Cookie Information ...')

    cookies.environment = 'notset'
    cookies.email = 'notset'
    var myCookies = document.cookie

    var myCookieParams = myCookies.split(';')
    for (var i = 0; i < myCookieParams.length; i++) {
      var fields = myCookieParams[i].trim().split('=')
      if (fields[0] === 'env') {
        cookies.environment = fields[1]
      }

      if (fields[0] === 'useremail') {
        cookies.email = fields[1].replace(/%40/g, '@')
      }
    }
  }

  /**
   * Initializes features and sets them according to the environment set as needed. Currently we
   * are managing the following features.
   *  - drag n drop
   *  - socket
   */
  var initializeFeatures = function () {
    log('Controller: Initializing Features ...')

    if (preventWindowDragDrop) {
      window.addEventListener('dragover', function (e) {
        e = e || event
        e.preventDefault()
      }, false)
      window.addEventListener('drop', function (e) {
        e = e || event
        e.preventDefault()
      }, false)
    }

    if (socketApp) {
      self.mySocket = new Socket(cm.getSettings(), socketConnected, socketFailed, socketError)
    }
  }

  /**
   * Get the environment settings.
   */
  this.getSettings = function () {
    return cm.getSettings()
  }

  /**
   * Call from the Socket Object when a connection is established with the server
   */
  var socketConnected = function () {
    log('Application: Socket connected')

    self.setupListeners()
  }

  /**
   * Called when we are unable to connect to the server via Socket
   */
  var socketFailed = function () {
    log('Application: Socket failed to connect')
    self.displayOfflineModal()
  }

  /**
   * Occures when there is an error detected when attempting to login.
   */
  var socketError = function (err) {
    log('Application: Socket Error')

    if (!offlineModal.isOpen()) {
      document.getElementById('progressOverlay').style.display = 'none'
      self.displayMessage(err, {color: 'red'})
    }
  }

  /*****************************************************************************
   *                      Generic Public Functions
   *****************************************************************************/
  /**
   * Trigger Server Login event.
   * @param {json} data: json object of login data
   */
  this.login = function (data) {
    self.mySocket.emit('login', data)
  }

  /**
   * If the message passed in is a string it prepend or just sets, if the
   * message is an object of type DIV then we don't apply string styling
   */
  this.displayMessage = function (msg, options) {
    log('Controller: display message modal')
    if (typeof msg === 'object') {
      msgModal.setObject(msg, options)
    } else {
      msgModal.setText(msg, options)
    }

    msgModal.open()
  }
  this.closeMessage = function () {
    msgModal.close()
  }

  /**
   * Display Custom Modal
   * @param {String/Object} obj
   * @param {JSON}          settings
   */
  this.displayModal = function (obj, settings) {
    if (typeof obj === 'object') {
      customModal.setObject(obj, settings)
    } else if (typeof obj === 'string') {
      customModal.setText(obj, settings)
    }
    customModal.open()
  }
  this.closeModal = function () {
    customModal.close()
  }

  /**
   * Display Terms & Privacy Modal
   */
  this.displayTermsModal = function () {
    termsModal.open()
  }
  this.displayPrivacyModal = function () {
    privacyModal.open()
  }

  /**
   * Display Offline Modal
   */
  this.displayOfflineModal = function () {
    offlineModal.showWait()
    offlineModal.open()
  }
  this.closeOfflineModal = function () {
    offlineModal.close()
  }

  /**
   * Display Deactivate Modal (user account deactivation)
   */
  this.displayDeactivateModal = function (id) {
    deactivateModal.open()
  }

  this.displayFirmAffiliationModal = function () {
    firmAffiliationModal.open()
  }
  this.closeFirmAffiliationModal = function () {
    firmAffiliationModal.close()
  }

  /*****************************************************************************
   *                   Engagement Related Public Functions
   *****************************************************************************/
  this.displayCreateEngagementModal = function () {
    createEngagementModal.open()
  }
  this.displayInviteClientModal = function () {
    let myClients = []
    self.myEngagements.forEach((engagement) => {
      if (engagement.business.keyContact) {
        myClients.unshift(engagement.business)
      }
    })
    inviteClientModal.loadClients(myClients)
    inviteClientModal.buildDropdown()
    inviteClientModal.open()
  }
  this.displayAddClientModal = function (name) {
    addClientModal.setTitle(name)
    addClientModal.open()
  }
  this.displayArchiveEngagementModal = function (unArchive) {
    archiveEngagementModal.refresh(unArchive)
    archiveEngagementModal.open()
  }
  this.closeArchiveEngagementModal = function () {
    archiveEngagementModal.close()
  }
  this.displayRemoveClientModal = function () {
    removeClientModal.open()
  }
  this.closeRemoveClientModal = function () {
    removeClientModal.close()
  }
  this.displayEditClientModal = function (clientInfo) {
    editClientModal.loadData(clientInfo)
    editClientModal.open()
  }
  this.displayDeleteRequestFileModal = function (data) {
    var json = data
    if (self.currentFiles) {
      for (var i = 0; i < self.currentFiles.length; i++) {
        if (self.currentFiles[i]._id.toString() === data.fileID.toString()) {
          json.fileName = self.currentFiles[i].name
        }
      }
    }
    deleteRequestFileModal.data = json
    deleteRequestFileModal.loadData()
    deleteRequestFileModal.open()
  }
  this.closeDeleteRequestFileModal = function () {
    deleteRequestFileModal.close()
  }
  this.displayDeleteRequestModal = function (data) {
    deleteRequestModal.request = data
    deleteRequestModal.loadData()
    deleteRequestModal.open()
  }
  // this.displayDeleteFileModal = function () {
  //   deleteFileModal.open()
  // }
  this.displayCreateEngagementModal = function () {
    createEngagementModal.open()
  }
  this.displayFirmAffiliationModal = function () {
    firmAffiliationModal.open()
  }
  this.closeFirmAffiliationModal = function () {
    firmAffiliationModal.close()
  }
  this.displayInviteNewMemberModal = function () {
    if (self.currentEngagement) {
      inviteNewMemberModal.setTitle(self.currentEngagement.name)
    }
    inviteNewMemberModal.open()
  }
  this.closeInviteNewMemberModal = function () {
    inviteNewMemberModal.reset()
    inviteNewMemberModal.close()
  }
  this.displayletUsKnowModal = function () {
    letUsKnowModal.open()
  }
  this.closeletUsKnowModal = function () {
    letUsKnowModal.close()
  }
  this.displayAddCategoryModal = function (todoId) {
    addCategoryModal.loadData(todoId)
    addCategoryModal.open()
  }
  this.displayEditCategoryModal = function () {
    editCategoryModal.loadData()
    editCategoryModal.open()
  }
  this.displayDeleteToDoModal = function () {
    deleteToDoModal.loadData()
    deleteToDoModal.open()
  }
  this.displayMarkCompleteToDoModal = function () {
    markCompleteToDoModal.loadData()
    markCompleteToDoModal.open()
  }
  this.displayFileManagerModal = function (request, todo) {
    fileManagerModal.loadData(request._id, todo._id)
    fileManagerModal.open()
  }
  this.displayBankIntegrationModal = function () {
    bankIntegrationModal.reset()
    bankIntegrationModal.open()
    bankIntegrationModal.eventListeners()
    bankIntegrationModal.finicityConnect()
  }

  this.displayCreateWorkingPaperModal = function () {
    createWorkingPaperModal.open()
  }

  this.displayViewPreviewModal = function () {
    viewPreviewModal.open()
  }

  this.closeCreateWorkingPaperModal = function () {
    createWorkingPaperModal.reset()
    createWorkingPaperModal.close()
  }
  this.displayModalGenerateWPConfirm = function () {
    modalGenerateWpConfirm.open()
  }
  this.displayModalGenerateWPWaiting = function () {
    modalGenerateWpWaiting.open()
  }

  /*****************************************************************************
   *                       Event related functions
   *****************************************************************************/
  // EVENT REGISTRATIONS FOR HANDLING RESPONSE
  var registrations = {
    'unknown': [], // triggered when event is not in right format
    'dev-signup': [],
    'get-admin-mapping': [],
    'login': [],
    'onboarding-complete': [],
    'load-engagement': [],
    'create-engagement': [],
    'create-workingpaper': [],
    'view-preview': [],
    'create-activity': [],
    'update-user': [],
    'update-user-password': [],
    'update-business': [],
    'update-engagement': [],
    'add-existing-file': [],
    'delete-file': [],
    'verify-auditor': [],
    'onboard-user': [],
    'update-user-status': [],
    'deactivate-account': [],
    'send-engagement-invite': [],
    'update-file': [],
    'update-multiple-files': [],
    'update-firm': [],
    'get-activities': [],
    'get-contact-list': [],
    'select-client': [],
    'add-integration': [],
    'add-team-member': [],
    'send-team-member-invite': [],
    'create-todo': [],
    'update-todo': [],
    'get-user-list': [],
    'create-category': [],
    'update-category': [],
    'delete-category': [],
    'bulk-todos-update': [],
    'undo-bulk-action': [],
    'add-multiple-team-members': [],
    'send-multiple-team-member-invites': [],
    'delete-engagement': [],
    'send-suggestion': [],
    'get-engagements': [],
    'find-contacts': [],
    'set-notification': [],
    'set-user-permission': [],
    'remove-team-member': [],
    'fn-connect': [],
    'fn-serverError': [],
    'fn-loginFormRequest': [],
    'fn-loginSubmit': [],
    'fn-accountList': [],
    'fn-loginFailure': [],
    'fn-loginSecurity': [],
    'fn-mfaTimeout': [],
    'fn-accountListSelectedResponse': [],
    'fn-accountListSelectedRequest': [],
    'fn-selectionSecurityFailure': [],
    'fn-selectionSecurity': [],
    'get-csv-string': [],
    'fn-loginComplete': [],
	  'get-samples': [],
    'load-workingpaper-type': [],
    'load-test-type': [],
    'load-technique-type': []
  }

  /**
   * Registers a callback function to a specific model by adding it to an array
   * called 'registrations'. We also allow registrations to specify a unique ID
   * that allows for registrations on a specific element versus all of them. We
   * also check for a type of operation following the CRUD methodology, except we
   * do not include READ as it isn't required at this time.
   *
   * @param tag:    String - The name of the event
   * @param uniqueID: String - A unique ID for source identification
   * @param callback: Function - What action to take
   * @return status: Boolean
   */
  this.registerForEvent = function (tag, uniqueID, callback) {
    if (typeof tag !== 'string' || typeof uniqueID !== 'string' || typeof callback !== 'function') {
      return false
    }

    if (!registrations[tag]) {
      return false
    }

    registrations[tag].push({ id: uniqueID, cb: callback })
    return true
  }

  /**
   * Searches the appropriate model registrations and removes the event listener.
   *
   * @param tag: String
   * @param uniqueID: String
   * @return status: Boolean
   */
  this.deregisterForEvent = function (tag, uniqueID) {
    if (typeof tag !== 'string' || typeof uniqueID !== 'string') {
      return false
    }

    if (!registrations[tag]) {
      return false
    }

    log('Remove update registration request: ' + tag + ', ' + uniqueID)
    var eventReg = registrations[tag]
    for (var i = 0; i < eventReg.length; i++) {
      if (eventReg[i].id === uniqueID) {
        eventReg.splice(i, 1)
        log('Removed registration for ' + tag + ', ' + uniqueID)
        return true
      }
    }

    return false
  }

  /**
   * Trigger Server event through socket if event is verified Format.
   * @param {json} event: Json object of event data.
   */
  this.sendEvent = function (payload) {
    if (verifyEventPayload(payload)) {
      self.mySocket.emit('event', payload)
    }
  }

  /**
   *  bulkTodosUpdate function
   */
  this.bulkTodosUpdate = function (data) {
    log('Updating bulk of todos...')
    data.currentUserID = self.currentUser._id
    data.engagementID = self.currentEngagement._id
    let dataList = {}
    dataList.engagementID = self.currentEngagement._id
    dataList.data = data
    self.sendEvent({name: 'bulk-todos-update', data: dataList})
  }

  /**
   * Triggers the array of callback functions that are currently associated with
   * the given model. We pass it the model, the id affected, the operation that occured,
   * and the fields that changed.
   *
   * @param tag: String
   * @param result: JSON
   */
  this.triggerEventListener = function (tag, result) {
    if (typeof tag !== 'string') {
      return
    }

    if (!registrations[tag]) {
      return
    }

    for (var i = 0; i < registrations[tag].length; i++) {
      var obj = registrations[tag][i]
      log('Firing Event: ' + tag + ', ' + obj.id)
      obj.cb(tag, result)
    }
  }

  /**
   * Verify event payloadbefore it emit message through socket.
   * @param {JSON} evt JSON object of event
   */
  var verifyEventPayload = function (payload) {
    if (!payload.name) {
      return false
    } else if (!payload.data) {
      // evt.data =""  which is fine because there is a case that does not have parameter but we need to be defined as a "".
      return false
    } else {
      return true
    }
  }
  /**
   * A scrubber for socket messaging responses. It will look at the error code and decide what
   * we need to do with the response.
   */
  var verifyEventResponse = function (tag, payload) {
    document.getElementById('progressOverlay').style.display = 'none'
    if (payload.code !== 0) {
      if (payload.msg) {
        flashAlert.flash({ content: payload.msg, type: 'ERROR' })
      } else {
        flashAlert.flash({ content: 'Code: ' + payload.code + ' - Error message unavailable', type: 'ERROR' })
      }
      return false
    } else {
      return true
    }
  }

  /**
   * Socket has been Connected.
   * Setup all of the application listeners.
   */
  this.setupListeners = function () {
    log('Application: Registering listeners to the connected socket')

    self.registerForEvent('dev-signup', 'EVT-MAC-000', handle_devSignupResponse)
    self.registerForEvent('load-engagement', 'EVT-MAC-001', handle_loadEngagementResponse)
    self.registerForEvent('create-engagement', 'EVT-MAC-004', handle_createEngagementResponse)
    self.registerForEvent('send-engagement-invite', 'EVT-MAC-006', handle_sendEngagementInviteResponse)
    self.registerForEvent('update-user', 'EVT-MAC-012', handle_updateUserResponse)
    self.registerForEvent('update-engagement', 'EVT-MAC-013', handle_updateEngagementResponse)
    self.registerForEvent('delete-file', 'EVT-MAC-016', handle_deleteFileResponse)
    self.registerForEvent('get-admin-mapping', 'EVT-MAC-017', handle_viewAdminResponse)
    self.registerForEvent('update-business', 'EVT-MAC-026', handle_updateBusinessResponse)
    self.registerForEvent('update-firm', 'EVT-MAC-027', handle_updateFirmResponse)
    self.registerForEvent('update-file', 'EVT-MAC-028', handle_updateFileResponse)
    self.registerForEvent('get-activities', 'EVT-MAC-029', handle_getActivitiesResponse)
    self.registerForEvent('get-contact-list', 'EVT-MAC-030', handle_getContactListResponse)
    self.registerForEvent('select-client', 'EVT-MAC-034', handle_selectClientResponse)
    self.registerForEvent('create-activity', 'EVT-MAC-036', handle_createActivityResponse)
    self.registerForEvent('deactivate-account', 'EVT-MAC-037', handle_deactivateAccResponse)
    self.registerForEvent('add-team-member', 'EVT-MAC-038', handle_addTeamMemberResponse)
    self.registerForEvent('send-team-member-invite', 'EVT-MAC-039', handle_sendTeamMemberInviteResponse)
    self.registerForEvent('create-todo', 'EVT-MAC-TODO-040', handle_createTodoResponse)
    self.registerForEvent('update-todo', 'EVT-MAC-TODO-041', handle_updateTodoResponse)
    self.registerForEvent('get-user-list', 'EVT-MAC-TODO-042', handle_getUserListResponse)
    self.registerForEvent('create-category', 'EVT-MAC-TODO-043', handle_createCategoryResponse)
    self.registerForEvent('update-category', 'EVT-MAC-TODO-044', handle_updateCategoryResponse)
    self.registerForEvent('delete-category', 'EVT-MAC-TODO-045', handle_deleteCategoryResponse)
    self.registerForEvent('bulk-todos-update', 'EVT-MAC-TODO-046', handle_bulkTodosUpdateResponse)
    self.registerForEvent('undo-bulk-action', 'EVT-MAC-TODO-047', handle_undoBulkActionResponse)
    self.registerForEvent('add-multiple-team-members', 'EVT-MAC-040', handle_addMultipleTeamMembersResponse)
    self.registerForEvent('send-multiple-team-member-invites', 'EVT-MAC-041', handle_sendMultipleTeamMemberInvitesResponse)
    self.registerForEvent('delete-engagement', 'EVT-MAC-042', handle_deleteEngagementResponse)
    self.registerForEvent('send-suggestion', 'EVT-MAC-043', handle_sendSuggestionResponse)
    self.registerForEvent('get-engagements', 'EVT-MAC-044', handle_getEngagementsResponse)
    self.registerForEvent('find-contacts', 'EVT-MAC-045', handle_findContactsResponse)
    self.registerForEvent('remove-team-member', 'EVT-MAC-046', handle_removeTeamMemberResponse)
    self.registerForEvent('set-notification', 'EVT-MAC-050', handle_setNotificationResponse)
    self.registerForEvent('set-user-permission', 'EVT-MAC-051', handle_setUserPermissionResponse)
    self.registerForEvent('fn-connect', 'EVT-MAC-052', handle_fnConnectResponse)
    self.registerForEvent('fn-loginFormRequest', 'EVT-MAC-053', handle_fnLoginFormRequestResponse)
    self.registerForEvent('fn-accountList', 'EVT-MAC-054', handle_fnAccountListResponse)
    self.registerForEvent('get-csv-string', 'EVT-MAC-055', handle_getCSVStringResponse)
    self.registerForEvent('fn-accountListSelectedResponse', 'EVT-MAC-056', handle_fnAccountListSelectedResponse)
    self.registerForEvent('fn-loginComplete', 'EVT-MAC-057', handle_fnLoginCompleteResponse)
    self.registerForEvent('fn-serverError', 'EVT-MAC-058', handle_fnServerErrorResponse)
    self.registerForEvent('update-user-password', 'EVT-MAC-059', handle_updateUserResponse)
    self.registerForEvent('load-workingpaper-type', 'EVT-MAC-WORKINGPAPER-001', handle_loadWorkingPaperTypeResponse)
    self.registerForEvent('load-test-type', 'EVT-MAC-WORKINGPAPER-002', handle_loadWorkingPaperTestTypeResponse)
    self.registerForEvent('load-technique-type', 'EVT-MAC-WORKINGPAPER-003', handle_loadWorkingPaperTechiniqueTypeResponse)
    self.registerForEvent('get-samples', 'EVT-MAC-WORKINGPAPER-004', handle_getSamplesResponse)

    this.mySocket.listen('event-response', function (payload) {
      log('Verifying event:' + payload.name)
      log('Payload: ' + JSON.stringify(payload))
      if (verifyEventResponse(payload.name, payload)) {
        self.triggerEventListener(payload.name, payload)
      }
      // Finicity Response Listeners
      if (payload.name === 'fn-connect') {
        handle_fnConnectResponse(payload.name, payload)
      }
      if (payload.name === 'fn-loginFormResponse') {
        handle_fnLoginFormRequestResponse(payload.name, payload)
      }
      if (payload.name === 'fn-accountList') {
        handle_fnAccountListResponse(payload.name, payload)
      }
      if (payload.name === 'fn-accountListSelectedResponse') {
        handle_fnAccountListSelectedResponse(payload.name, payload)
      }
      if (payload.name === 'fn-loginComplete') {
        handle_fnLoginCompleteResponse(payload.name, payload)
      }
      if (payload.name === 'fn-serverError') {
        handle_fnServerErrorResponse(payload.name, payload)
      }
    })

    this.mySocket.listen('login-response', function (payload) {
      if (verifyEventResponse('login-response', payload) && !self.mySocket.reconnected) {
        handle_loginResponse(payload)
      }
    })

    // broadcasting to users online.
    this.mySocket.listen('broadcasting', function (data) {
      self.displayMessage(data.msg)
    })
  }

  /*****************************************************************************
   *                   Event-Response Handler Functions
   *****************************************************************************/
  /**
   * Login Response handler.
   * Loading appropriate page based on user type and status.
   * @param {String} data
   */
  var handle_loginResponse = function (data) {
    log('Login-response: Status Code ' + data.code, data)

    // const footer = $('footer.footer')
    // footer.show()

    if (data.code === 0 && data.result) {
      // load data to controller's properties (ie. c.currentUser)
      loadUserData(data.result)
      settingsModule.loadDashboard()

      if (data.result.user) {
        displayProperPage()
      } else {
        self.displayMessage('No User Information Available')
      }
    } else {
      if (data.result) {
        switch (data.result.user.status) {
          case 'PENDING':
            self.displayMessage("<b>Your account is pending approval.<br/>For additional information, contact Auvenir.</b><br/><br/><a style='color:blue' href='contact@baystreetlabs.com'>contact@baystreetlabs.com</a>")
            break
          case 'WAIT-LIST':
            self.displayMessage("<b>Your account is wait listed.<br/>Contact the Auvenir support team for access.</b><br/><br/><a style='color:blue' href='contact@baystreetlabs.com'>contact@baystreetlabs.com</a>")
            break
          case 'INACTIVE':
            self.displayMessage("<b>Your account is deactivated.<br/>Contact the Auvenir support team for help.</b><br/><br/><a style='color:blue' href='contact@baystreetlabs.com'>contact@baystreetlabs.com</a>")
            break
          case 'LOCKED':
            self.displayMessage("<b>Your account has been locked.<br/>Contact the Auvenir support team.</b><br/><br/><a style='color:blue' href='contact@baystreetlabs.com'>contact@baystreetlabs.com</a>")
            break
          default:
            self.displayMessage('Unknown User Status')
        }
      }
    }
  }
  /**
   *  Load Engagement when load engagement-response comes to Socket.
   * @param tag: {String} Tag Name
   * @param Data: {JSON} The Result of load-engagement Event.
   */
  var handle_loadEngagementResponse = function (tag, data) {
    log('load-engagement-response: Status Code ' + data.code)
    log(data)
    document.getElementById('progressOverlay').style.display = 'none'
    if (data.code !== 0) {
      self.displayMessage('Unable to load engagement')
    } else {
      // TODO verification of engagement access - see flow of load-engagement-response
      loadEngagementData(data.result).then(() => {
        self.viewPage(data.page || 'ENGAGEMENT')
      })
    }
  }

  var handle_loadWorkingPaperTypeResponse = function (tag, data) {
    log('load-working-paper-type-response: Status Code ' + data.code)
    log(data)
  }

  var handle_loadWorkingPaperTestTypeResponse = function (tag, data) {
    log('load-working-paper-test-type-response: Status Code ' + data.code)
    log(data)
  }

  var handle_loadWorkingPaperTechiniqueTypeResponse = function (tag, data) {
    log('load-working-paper-techinique-type-response: Status Code ' + data.code)
    log(data)
  }

  var handle_getSamplesResponse = function (tag, data) {
    log('> Event: Get Samples response: Status Code ' + data.code)
    log(data)
  }

  /**
   * Handler for create engagement response.
   * @param tag: {String} Tag Name
   * @param Data: {JSON} The Result of createEngagement Event.
   */
  var handle_createEngagementResponse = function (tag, data) {
    log('> Event: create-engagement-response')
    log(data)
    if (data.code !== 0) {
      document.getElementById('progressOverlay').style.display = 'none'
      self.displayMessage(data.msg)
    } else {
      self.myEngagements.push(data.engagement)
      self.currentEngagement = data.engagement
      if (data.isNewBusiness) {
        self.myPendingBusiness = data.business
      }
    }
  }

  /**
   * Handle for Send Engagement response.
   * @param tag: {String} Tag Name
   * @param Data: {JSON} The Result of send-engagement-invite Event.
   */
  var handle_sendEngagementInviteResponse = function (tag, data) {
    document.getElementById('progressOverlay').style.display = 'none'
    if (data.code) {
      self.displayMessage(data.msg)
    } else {
      let { engagementId, email, activityData } = data
      const eng = self.myEngagements.find((eng) => {
        return eng._id === engagementId
      })
      eng.acl.forEach((user) => {
        if (user.role === 'CLIENT' && user.userInfo.email === email) {
          user.status = 'INVITED'
          engagementModule.clientInvited(user)
        }
      })
      eng.status = 'ACTIVE'
      flashAlert.flash({
        content: '<div class="send-message-success-alert"><div class="icon-check"></div><span>Your engagement invitation has been sent.</span></div>',
        type: 'SUCCESS'
      })

      if (self.currentEngagement._id.toString() === engagementId.toString()) {
        self.currentEngagement = eng
        engagementModule.updateOverview()
      }

      if (activityData) {
        self.createActivity(activityData)
      }
    }
  }

  /**
   * Handle for update user response from socket.
   * @param tag: {String} Tag Name
   * @param Data: {JSON} The Result of update-user Event.
   */
  var handle_updateUserResponse = function (tagName, data) {
    if (verifyEventResponse('update-user-response', data)) {
      log(data)
      if (data.result.fields) {
        if (data.result.fields.firstName !== undefined) self.currentUser.firstName = data.result.fields.firstName
        if (data.result.fields.lastName !== undefined) self.currentUser.lastName = data.result.fields.lastName
        if (data.result.fields.phone !== undefined) self.currentUser.phone = data.result.fields.phone
        if (data.result.fields.email !== undefined) self.currentUser.email = data.result.fields.email
        if (data.result.fields.jobTitle !== undefined) self.currentUser.jobTitle = data.result.fields.jobTitle
        if (data.result.fields.agreements !== undefined) self.currentUser.agreements = data.result.fields.agreements
        if (data.result.fields.referral !== undefined) self.currentUser.referral = data.result.fields.referral
        if (data.result.fields.passwordResetRequired !== undefined) self.currentUser.passwordResetRequired = data.result.fields.passwordResetRequired
        if (data.result.fields.lastOnboardedEID !== undefined) self.currentUser.lastOnboardedEID = data.result.fields.lastOnboardedEID
      } else {
        log('No user info to update.')
      }
    }
  }

  /**
   * Handle for update engagement response from socket.
   *
   * @param tag: {String} Tag Name
   * @param Data: {JSON} The Result of update-engagement Event.
   */
  var handle_updateEngagementResponse = function (tagName, data) {
    if (verifyEventResponse('update-engagement-response', data)) {
      const engagement = data.result
      const engagementIndex = self.myEngagements.findIndex((eng) => {
        return eng._id === engagement._id
      })
      self.myEngagements.splice(engagementIndex, 1, engagement)
      if (self.currentEngagement._id === engagement._id) {
        self.currentEngagement = engagement
      }
    }
  }

  /**
   * Handle for delete-file response from socket.
   *
   * @param tag: {String} Tag Name
   * @param Data: {JSON} The Result of delete-file Event.
   */
  var handle_deleteFileResponse = function (tagName, data) {
    document.getElementById('progressOverlay').style.display = 'none'
    if (verifyEventResponse('delete-file', data)) {
      let { engagement, successFiles } = data.result

      self.myEngagements.forEach((oneEngagement, i) => {
        if (engagement._id.toString() === oneEngagement._id.toString()) {
          for (let j = 0; j < successFiles.length; j++) {
            oneEngagement.files.forEach((file, i) => {
              if (file._id.toString() === successFiles[j]._id.toString()) {
                oneEngagement.files[i].status = 'INACTIVE'
              }
            })
          }
        }
      })

      for (let activity in data.result.actvityList) {
        self.createActivity(activity)
      }
    }
  }

  /**
   * Handle for get-admin-mapping response from socket.
   * @param tag: {String} Tag Name
   * @param Data: {JSON} The Result of get-admin-mapping Event.
   */
  var handle_viewAdminResponse = function (tagName, data) {
    document.getElementById('progressOverlay').style.display = 'none'
    log('get-admin-mapping-Response: Status Code ' + data.code)

    if (data.code !== 0) {
      self.displayMessage(data.msg, {color: 'red'})
    } else {
      if (self.currentUser.type === 'ADMIN') {
        log(data.result)
        adminModule.loadData(data.result)
        self.viewPage('ADMIN')
      } else {
        self.displayMessage('Access denied to the admin view', {color: 'red'})
      }
    }
  }

  var handle_getContactListResponse = function (tagName, data) {
    log('\nResponse: ' + tagName, data)
  }

  var handle_devSignupResponse = function (tagName, data) {
    log('\nResponse: ' + tagName)
    log(data)

    adminModule.credentialsSet(true)
    self.currentUser = data.result
    self.displayMessage('<b>Auth ID:</b> ' + self.currentUser.auth.id + '<br/><b>API Key:</b> ' + self.currentUser.auth.developer.apiKey)
  }

  var handle_updateBusinessResponse = function (tagName, data) {
    if (verifyEventResponse('update-business-response', data)) {
      if (data.result && data.result.fields) {
        if (data.result.fields.name) {
          self.currentBusiness.name = data.result.fields.name
        }
        if (data.result.fields.websiteURL) {
          self.currentBusiness.websiteURL = data.result.fields.websiteURL
        }
        if (data.result.fields.phone) {
          self.currentBusiness.phone = data.result.fields.phone
        }
        if (data.result.fields.logo) {
          self.currentBusiness.logo = data.result.fields.logo
        }
        if (data.result.fields.addresses) {
          self.currentBusiness.addresses = data.result.fields.addresses
        }
        if (data.result.fields.structure) {
          self.currentBusiness.structure = data.result.fields.structure
        }
        if (data.result.fields.industry) {
          self.currentBusiness.industry = data.result.fields.industry
        }
        if (data.result.fields.accountingFramework) {
          self.currentBusiness.accountingFramework = data.result.fields.accountingFramework
        }
        if (data.result.fields.accountingSoftware) {
          self.currentBusiness.accountingSoftware = data.result.fields.accountingSoftware
        }
        if (data.result.fields.fiscalYearEnd) {
          self.currentBusiness.fiscalYearEnd = data.result.fields.fiscalYearEnd
        }
        if (onboardingModule) {
          onboardingModule.loadData()
        }
      }
    }
  }

  var handle_updateFirmResponse = function (tagName, data) {
    if (verifyEventResponse('update-firm-response', data)) {
      log(data)
      if (data.result && data.result.fields) {
        if (data.result.fields.name) {
          self.currentFirm.name = data.result.fields.name
        }
        if (data.result.fields.address) {
          self.currentFirm.address = data.result.fields.address
        }
        if (data.result.fields.phone) {
          self.currentFirm.phone = data.result.fields.phone
        }
        if (data.result.fields.affiliated) {
          self.currentFirm.affiliated = data.result.fields.affiliated
        }
        if (data.result.fields.logo) {
          self.currentFirm.logo = data.result.fields.logo
        }
        if (data.result.fields.website) {
          self.currentFirm.website = data.result.fields.website
        }
        if (data.result.fields.size) {
          self.currentFirm.size = data.result.fields.size
        }
        if (data.result.fields.logoDisplayAgreed) {
          self.currentFirm.logoDisplayAgreed = data.result.fields.logoDisplayAgreed
        }
        if (onboardingModule !== null) {
          onboardingModule.loadData()
        }
      } else {
        log('No user info to update.')
      }
    }
  }

  /**
   * This function is triggered after user sorted the file(or uploaded a file) to a existing other request.
   * @param {String} tagName
   * @param {JSON} data
   * TODO - code cleanup after db model change
   *        this function will be used when TODO page is built.
   */
  var handle_updateFileResponse = function (tagName, data) {
    if (verifyEventResponse('update-file-response', data)) {
      // if (data.result && data.result.newRequestID !== data.result.previousRequestID) {
      //   for (let i = 0; i < self.currentRequests.length; i++) {
      //     if (self.currentRequests[i]._id.toString() === data.result.newRequestID.toString()) {
      //       self.currentRequests[i].items.push(data.result.file)
      //     }
      //   }
      //   if (data.result.previousRequestID) {
      //     var deleteIndex = -1
      //     var requests = self.currentRequests
      //     for (let i = 0; i < requests.length; i++) {
      //       if (requests[i]._id.toString() === data.result.previousRequestID.toString()) {
      //         var previousRequest = requests[i]
      //         for (let j = 0; j < previousRequest.items.length; j++) {
      //           if (previousRequest.items[j]._id.toString() === data.result.fileID.toString()) {
      //             deleteIndex = j
      //           }
      //         }
      //         if (deleteIndex >= 0) {
      //           previousRequest.items.splice(deleteIndex, 1)
      //         }
      //       }
      //     }
      //   }
      // }
      if (data.result.activityData) {
        self.createActivity(data.result.activityData)
      }
    } else {
      log('No File info to update.')
    }
  }

  var handle_getActivitiesResponse = function (tagName, data) {
    log('Got' + data.activities.length + ' Activities!')
  }

  var handle_createActivityResponse = function (tagName, data) {
    if (data.result.activity && data.result.activity.engagementID) {
      let newActivity = {...data.result.activity, ...data}
      newActivity.userID = {_id: self.currentUser._id, firstName: self.currentUser.firstName, lastName: self.currentUser.lastName, email: self.currentUser.lastName}
      self.currentActivities.unshift(newActivity)
    }
  }

  // TODO - code cleanup needed. Are we still using this?
  var handle_selectClientResponse = function (tagName, data) {
    let { client, business, engagement } = data.result

    if (!client || !engagement) {
      flashAlert.flash({ type: 'ERROR', content: 'There was an error while registering client.'})
      return
    } else {
      let engagementIndex

      self.myContacts.push(client)
      inviteClientModal.buildDropdown()
      clientListModule.loadData()

      engagementIndex = self.myEngagements.findIndex((eng) => {
        return eng._id === engagement._id
      })
      self.myEngagements.splice(engagementIndex, 1, engagement)
      self.sendEngagementInvite(engagement._id, client.email)
    }
  }

  var handle_deactivateAccResponse = function (tagName, data) {
    log('EVT> Deactivate-account response.')

    if (data) {
      if (data.code) {
        flashAlert.flash({ content: 'Something went wrong while deactivating your account.', type: 'ERROR' })
      } else {
        location.href = '/logout'
      }
    }
  }

  /**
   * Handle for add team member response from socket.
   *
   * @param tag: {String} Tag Name
   * @param data: {JSON} The Result of add-team-member Event.
   */
  var handle_addTeamMemberResponse = function (tagName, data) {
    log('EVT> Add-Team-Member response.')
    if (verifyEventResponse('add-team-member-response', data)) {
      const engagement = data.result.engagement
      const engagementIndex = self.myEngagements.findIndex((eng) => {
        return eng._id === engagement._id
      })
      self.myEngagements.splice(engagementIndex, 1, engagement)
      if (self.currentEngagement._id === engagement._id) {
        self.currentEngagement = engagement
        log('Controller currentEngagement updated', self.currentEngagement)
      }

      flashAlert.flash({
        content: '<div class="send-message-success-alert"><div class="icon-check"></div><span>Your team member has been added and notified.</span></div>',
        type: 'SUCCESS'
      })

      let newMember = {
        id: data.payload.userID,
        name: data.payload.firstName + ' ' + data.payload.lastName,
        role: data.payload.role,
        permission: 'General',
        isNewUser: false
      }
      engagementModule.loadNewMember(newMember)
      if (data.result.activityData) {
        self.createActivity(data.result.activityData)
      }

      /**
        * Set the added user's lastOnboardedEID to this current engagement
        * Setting that user as currentUser to forgo the ADMIN check in Utility.UpdateUser
        **/
      var json = {
        currentUserID: self.currentUser._id,
        userID: data.payload.userID,
        lastOnboardedEID: self.currentEngagement._id
      }
      self.updateUser(json)
    }
  }

  var handle_sendTeamMemberInviteResponse = function (tag, data) {
    log('EVT> Send-Team-Member-Invite response.')
    document.getElementById('progressOverlay').style.display = 'none'
    if (data.code) {
      self.displayMessage(data.msg)
    } else {
      const engagement = data.engagement
      const engagementIndex = self.myEngagements.findIndex((eng) => {
        return eng._id === engagement._id
      })
      self.myEngagements.splice(engagementIndex, 1, engagement)
      if (self.currentEngagement._id === engagement._id) {
        self.currentEngagement = engagement
        log('Controller currentEngagement updated', self.currentEngagement)
      }

      flashAlert.flash({
        content: '<div class="send-message-success-alert"><div class="icon-check"></div><span>Your team member invitation has been sent.</span></div>',
        type: 'SUCCESS'
      })

      /* Add the new member to the team list */
      var engagementUser = self.currentEngagement.acl
      var newMemberPermission = 'General'
      let newMember = {
        id: data.newMember._id,
        name: data.newMember.firstName + ' ' + data.newMember.lastName,
        role: data.newMember.jobTitle,
        permission: newMemberPermission,
        isNewUser: true
      }
      engagementModule.loadNewMember(newMember)
      engagementModule.updateOverviewTeam()

      let newContact = data.newMember
      self.myContacts.push(newContact)
      clientListModule.loadData()

      if (data.activityData) {
        self.createActivity(data.activityData)
      }
    }
  }

  var handle_addMultipleTeamMembersResponse = function (tagName, data) {
    log('EVT> Add-Multiple-Team-Members response.')
    if (verifyEventResponse('add-multiple-team-members-response', data)) {
      if (data.code) {
        self.displayMessage(data.msg)
      } else {
        if (!data.isNewUser) {
          const engagement = data.engagement
          const engagementIndex = self.myEngagements.findIndex((eng) => {
            return eng._id === engagement._id
          })
          self.myEngagements.splice(engagementIndex, 1, engagement)
          if (self.currentEngagement._id === engagement._id) {
            self.currentEngagement = engagement
            log('Controller currentEngagement updated', self.currentEngagement)
          }
        }

        self.myPendingUsers.push({user: data.user, isNewUser: data.isNewUser})
      }
    }
  }

  var handle_sendMultipleTeamMemberInvitesResponse = function (tagName, data) {
    log('EVT> Send-Multiple-Team-Member-Invites response.')
    if (verifyEventResponse('send-multiple-team-member-invites-response', data)) {
      if (!data.code) {
        if (data.engagement) {
          const engagement = data.engagement
          const engagementIndex = self.myEngagements.findIndex((eng) => {
            return eng._id === engagement._id
          })
          self.myEngagements.splice(engagementIndex, 1, engagement)
          if (self.currentEngagement._id === engagement._id) {
            self.currentEngagement = engagement
            for (var i = 0; i < data.payload.users.length; i++) {
              var json = {
                currentUserID: self.currentUser._id,
                userID: data.payload.users[i].user._id,
                lastOnboardedEID: self.currentEngagement._id
              }
              self.updateUser(json)
            }
            log('Controller currentEngagement updated', self.currentEngagement)
          }

          if (data.activityDataList) {
            for (let i = 0; i < data.activityDataList.length; i++) {
              self.createActivity(data.activityDataList[i])
            }
          }
        }
      }
      self.myPendingBusiness = null
      self.myPendingUsers.length = 0
    }
  }

  var handle_deleteEngagementResponse = function (tagName, data) {
    log('EVT> Delete-Engagement response.')
    if (verifyEventResponse('delete-engagement-response', data)) {
      if (!data.code) {
        self.myPendingBusiness = null
        self.currentEngagement = null
        self.myPendingUsers.length = 0

        const engagementIndex = self.myEngagements.findIndex((eng) => {
          return eng._id === data.engagementID
        })

        if (engagementIndex > -1) {
          self.myEngagements.splice(engagementIndex, 1)
        }
      }
    }
  }

  var handle_sendSuggestionResponse = function (tagName, data) {
    log('EVT> send-suggestion response')
    if (verifyEventResponse('send-suggestion', data)) {
      if (!data.code) {
        flashAlert.flash({ type: 'SUCCESS', content: 'Your suggestion was sent successfully.' })
      }
    }
  }
  /**
   *  Add User to contacts when User is retrieved.
   * @param tag: {String} Tag Name
   * @param Data: {JSON} The Result of load-engagement Event.
   */
  var handle_findContactsResponse = function (tag, data) {
    log('findContacts: Status Code ' + data.code)
    log(data)
    if (data.code !== 0) {
      self.displayMessage('Unable to add users')
    } else {
      self.myContacts = _.uniqBy(data.users, 'email')
      if (data.payload.length === data.users.length) {
        clientListModule.loadData()
      }
    }
  }

  var handle_getEngagementsResponse = function (tagName, data) {
    log('EVT> get-engagements response')
    if (verifyEventResponse('get-engagements', data)) {
      if (!data.code) {
        self.myEngagements = COMMON.checkEngagementData(data.engagements)
        const engagementIndex = self.myEngagements.findIndex((eng) => {
          return eng._id === self.currentEngagement._id
        })
        if (engagementIndex !== -1) {
          self.currentEngagement = self.myEngagements[engagementIndex]
        } else {
          self.currentEngagement = {}
        }
      }
    }
  }
  /**
   * @param {*} tag
   * @param {*} data
   */
  var handle_setNotificationResponse = function (tagName, data) {
    log('EVT> Set-Notification response.')
    if (verifyEventResponse('set-notification-response', data)) {
      const user = data.user
      if (user) {
        self.currentUser = user
      }
    }
  }

  var handle_removeTeamMemberResponse = function (tagName, data) {
    log('EVT> Remove-Team-Member response.')
    if (verifyEventResponse('remove-team-member-response', data)) {
      const engagement = data.result.engagement
      const engagementIndex = self.myEngagements.findIndex((eng) => {
        return eng._id === engagement._id
      })
      self.myEngagements.splice(engagementIndex, 1, engagement)
      if (self.currentEngagement._id === engagement._id) {
        self.currentEngagement = engagement
        log('Controller currentEngagement updated', self.currentEngagement)
      }

      flashAlert.flash({
        content: '<div class="send-message-success-alert"><div class="icon-check"></div><span>Your team member has been removed.</span></div>',
        type: 'SUCCESS'
      })
      self.createActivity(data.result.activityData)
      engagementModule.removeTeamMember(data.payload.userID)
    }
  }

  var handle_setUserPermissionResponse = function (tagName, data) {
    log('EVT> Set-User-Permission response.')
    if (verifyEventResponse('set-user-permission-response', data)) {
      if (data.code) {
        self.displayMessage(data.msg)
      } else {
        const engagement = data.engagement
        const engagementIndex = self.myEngagements.findIndex((eng) => {
          return eng._id === engagement._id
        })
        self.myEngagements.splice(engagementIndex, 1, engagement)
        if (self.currentEngagement._id === engagement._id) {
          self.currentEngagement = engagement
          log('Controller currentEngagement updated', self.currentEngagement)
        }
      }
    }
  }

  var handle_getCSVStringResponse = function (tagName, data) {
    log('EVT> Get-CSV-String response.')
    if (verifyEventResponse('get-csv-string-response', data)) {
      if (data.code) {
        self.displayMessage(data.msg)
      } else {
        let csvContent = 'data:text/csv;charset=utf-8,'
        csvContent += data.result
        let encodedUri = encodeURI(csvContent)
        window.open(encodedUri)
      }
    }
  }

  /*****************************************************************************
   *                            Other functions
   *****************************************************************************/
  /**
   * Handler for create todos response.
   *
   * @param tag: {String} Tag Name
   * @param Data: {JSON} The Result of createTodo Event.
   */
  var handle_createTodoResponse = function (tag, data) {
    log('> Event: create-todo-response......')
    log(data)
    if (data.code !== 0) {
      flashAlert.flash({ type: 'ERROR', content: 'There was an error while creating Todo.'})
    } else {
      log(JSON.stringify(data.result))
      var todos = COMMON.removeInactiveTodos(data.result.todos)
      self.currentEngagement.status = data.result.engagementStatus
      todos = COMMON.addSearchDataToTodos(todos, self.currentEngagement.categories, self.currentEngagement.acl)
      self.currentTodos = todos.reverse()

      self.currentEngagement.todos = data.result.todos
      engagementModule.updateOverview()

      self.createActivity(data.result.activityData)
    }
  }

  /**
   * Handler for mark todos as complete response.
   *
   * @param tag: {String} Tag Name
   * @param Data: {JSON} The Result of markTodoComplete Event.
   */
  var handle_bulkTodosUpdateResponse = function (tag, data) {
    log('> Event: bulk-todos-update-response......')
    log(data)
    if (data.code !== 0) {
      flashAlert.flash({ type: 'ERROR', content: 'There was an error while updating Todo.'})
    } else {
      log(JSON.stringify(data.result))
      var todos = COMMON.removeInactiveTodos(data.result.todos)
      todos = COMMON.addSearchDataToTodos(todos, self.currentEngagement.categories, self.currentEngagement.acl)
      self.currentTodos = todos.reverse()
      self.currentEngagement.todos = data.result.todos
      self.undoTodos = data.result.undoTodos
      self.currentEngagement.status = data.result.engagementStatus

      if (data.result.activityDataList) {
        for (let i = 0; i < data.result.activityDataList.length; i++) {
          self.createActivity(data.result.activityDataList[i])
        }
      }
    }
  }

  /**
   * Handler for undo bulk action response.
   *
   * @param tag: {String} Tag Name
   * @param Data: {JSON} The Result of undoBulkAction Event.
   */
  var handle_undoBulkActionResponse = function (tag, data) {
    log('> Event: undo-bulk-action-response......')
    log(data)
    if (data.code !== 0) {
      flashAlert.flash({ type: 'ERROR', content: 'There was an error while undoing bulk action.'})
    } else {
      log(JSON.stringify(data.result))
      var todos = COMMON.removeInactiveTodos(data.result.todos)
      todos = COMMON.addSearchDataToTodos(todos, self.currentEngagement.categories, self.currentEngagement.acl)
      self.currentTodos = todos.reverse()
      self.undoTodos = []
      self.currentEngagement.status = data.result.engagementStatus
    }
  }

  /**
   * Handler for update Todos response.
   *
   * @param tag: {String} Tag Name
   * @param Data: {JSON} The Result of updateTodo Event.
   */
  var handle_updateTodoResponse = function (tag, data) {
    log('> Event: update-todo-response......')
    log(data)

    if (data.code !== 0) {
      document.getElementById('progressOverlay').style.display = 'none'
      self.displayMessage(data.msg)
    } else {
      log(JSON.stringify(data.result))
      var todos = COMMON.removeInactiveTodos(data.result.todos)
      self.currentEngagement.status = data.result.engagementStatus
      todos = COMMON.addSearchDataToTodos(todos, self.currentEngagement.categories, self.currentEngagement.acl)
      self.currentTodos = todos.reverse()

      self.currentEngagement.todos = self.currentTodos
      engagementModule.refresh()

      self.createActivity(data.result.activityData)
    }
  }

  /**
   * Handler for update Todos response.
   *
   * @param tag: {String} Tag Name
   * @param Data: {JSON} The Result of updateTodo Event.
   */
  var handle_getUserListResponse = function (tag, data) {
    log('> Event: get-user-list-response......')
    log(data)

    if (data.code !== 0) {
      document.getElementById('progressOverlay').style.display = 'none'
      self.displayMessage(data.msg)
    } else {
      log(JSON.stringify(data.result))
    }
  }

  /**
   * Handler for create category response.
   *
   * @param tag: {String} Tag Name
   * @param Data: {JSON} The Result of createCategory Event.
   */
  var handle_createCategoryResponse = function (tag, data) {
    log('> Event: create-category-response......')
    log(data)
    if (data.code !== 0) {
      flashAlert.flash({ type: 'ERROR', content: 'There was an error while creating category.'})
      return
    } else {
      log(JSON.stringify(data.result))
      self.currentEngagement.categories = data.result.category.reverse()

      var todos = COMMON.removeInactiveTodos(data.result.todos)
      todos = COMMON.addSearchDataToTodos(todos, self.currentEngagement.categories, self.currentEngagement.acl)
      self.currentTodos = todos.reverse()
      self.currentEngagement.todos = self.currentTodos
    }
  }

  /**
   * Handler for update category response.
   *
   * @param tag: {String} Tag Name
   * @param Data: {JSON} The Result of updateCategory Event.
   */
  var handle_updateCategoryResponse = function (tag, data) {
    log('> Event: update-category-response......')
    log(data)
    if (data.code !== 0) {
      flashAlert.flash({ type: 'ERROR', content: 'There was an error while updating categories.'})
      return
    } else {
      log(JSON.stringify(data.result))
      self.currentEngagement.categories = data.result.reverse()
      self.currentTodos = COMMON.addSearchDataToTodos(self.currentTodos, self.currentEngagement.categories, self.currentEngagement.acl)
    }
  }

  /**
   * Handler for delete category response.
   *
   * @param tag: {String} Tag Name
   * @param Data: {JSON} The Result of deleteCategory Event.
   */
  var handle_deleteCategoryResponse = function (tag, data) {
    log('> Event: delete-category-response......')
    log(data)
    if (data.code !== 0) {
      flashAlert.flash({ type: 'ERROR', content: 'There was an error while deleting categories.'})
    } else {
      log(JSON.stringify(data.result))
      self.currentEngagement.categories = data.result.categories.reverse()

      // update todo list with changes of category list
      self.currentTodos = data.result.todos
      self.currentTodos = COMMON.addSearchDataToTodos(self.currentTodos, self.currentEngagement.categories, self.currentEngagement.acl)
      self.currentEngagement.todos = self.currentTodos
    }
  }

  /**
   * Loads the required module depending on the access control list of the user.
   */
  var loadRequiredModules = function () {
    log('Loading the required dashboards')

    function setupClientList () {
      clientListModule = new ClientList(self, {display: 'none', scroll: true, contacts: true})
    }

    function setupEngagement () {
      engagementModule = new Engagement(self, {display: 'none', scroll: true})
      engagementModule.setupListeners()
      engagementModule.loadDashboard()

      let type = self.currentUser.type

      if (type === 'AUDITOR') { // TODO - fix this. not only auditor but client can update the file
        engagementModule.on('update-file', (data) => {
          document.getElementById('progressOverlay').style.display = 'inherit'
          self.sendEvent({name: 'update-file', data: data})
        })
        engagementModule.on('update-selected-files', (data) => {
          document.getElementById('progressOverlay').style.display = 'inherit'
          self.sendEvent({name: 'update-multiple-files', data: data})
        })
      }
      engagementModule.on('flash', (options) => {
        flashAlert.flash(options)
      })
    }

    switch (self.currentUser.status) {
      case 'PENDING':
        self.displayMessage('Account is pending. Unable to load modules')
        break
      case 'LOCKED':
        self.displayMessage('Account is locked. Unable to load modules')
        break
      case 'WAIT-LIST':
        self.displayMessage('Account is on the wait list.  Unable to load modules')
        break
      case 'ONBOARDING':
        log('Loading Onboarding Process')
        onboardingModule = new Onboarding(self, {display: 'none'})
        onboardingModule.setupListeners()
        onboardingModule.on('display-privacy', function () {
          self.displayPrivacyModal()
        })
        onboardingModule.on('display-term', function () {
          self.displayTermsModal()
        })
        onboardingModule.on('accept-agreement', function () {
          self.acceptSecurityAgreement()
        })
        onboardingModule.on('onboarding-complete', function (evt, data) {
          self.onboardingComplete(data)
          headerModule.exitOnboardingMode()
        })
        onboardingModule.on('flash', function (option) {
          flashAlert.flash(option)
        })
        break
      case 'INACTIVE':
        self.displayMessage('Account is deactivated. Unable to load modules')
        break
      case 'ACTIVE':
        var userType = self.currentUser.type
        if (userType === 'ADMIN') {
          adminModule = new Admin(self, {display: 'none', scroll: true})
          if (self.currentUser && self.currentUser.auth && self.currentUser.auth.developer && self.currentUser.auth.developer.apiKey) {
            adminModule.credentialsSet(true)
          } else {
            adminModule.credentialsSet(false)
          }
          adminModule.on('dev-signup', function (evt, data) {
            document.getElementById('progressOverlay').style.display = 'inherit'
            self.mySocket.emit('event', {name: 'dev-signup', data: {}})
          })
          adminModule.on('view-dev-creds', function (evt, data) {
            if (self.currentUser && self.currentUser.auth && self.currentUser.auth.developer && self.currentUser.auth.developer.apiKey) {
              self.displayMessage('<b>Auth ID:</b> ' + self.currentUser.auth.id + '<br/><b>API Key:</b> ' + self.currentUser.auth.developer.apiKey)
            } else {
              self.displayMessage('Developer Credentials do not exist for this user.', {color: 'red'})
            }
          })

          if (adminModule) {
            adminModule.loadDashboard()
          } else {
            self.displayMessage('Unable to build Admin Module.')
          }
          headerModule.setAdminHeader()
        } else if (userType === 'CLIENT') {
          clientModule = new Client(self, {display: 'none', scroll: true})
          clientModule.loadDashboard(self.myEngagements)

          setupEngagement()
          setupClientList()
          headerModule.setClientHeader()
          headerModule.setCurrentLink('h-engagementsLink')
        } else if (userType === 'AUDITOR') {
          previewModule = new EngagementPreview(self, { display: 'none', scroll: true })
          previewModule.loadDashboard()
          // auditorModule = new Auditor(self, {display: 'none', scroll: true})
          // auditorModule.loadDashboard()

          setupEngagement()
          setupClientList()
          headerModule.setAuditorHeader()
          headerModule.setCurrentLink('h-engagementsLink')
          break
        }
        break
    }
  }

  /**
   * Checks the access control list for the user and then takes the appropriate
   * action that leads towards loading the page.
   */
  var displayProperPage = function () {
    log('Displaying Proper Initial Page for ', self.currentUser.email)

    loadRequiredModules()
    let userType = self.currentUser.type
    let userStatus = self.currentUser.status
    let userID = self.currentUser._id
    let onboardedID = self.currentUser.lastOnboardedEID

    /* Checks of there's record of a last Onboarded ID and if there is, enters that engagement then resets that engagement to empty. */
    var checkOnboardedEID = function () {
      for (var i = 0; i < self.myEngagements.length; i++) {
        if (self.myEngagements[i]._id === onboardedID) {
          self.loadEngagement(onboardedID)
          var data = {
            currentUserID: userID,
            userID: userID,
            lastOnboardedEID: ''
          }
          self.updateUser(data)
        }
      }
    }

    if (userStatus === 'ONBOARDING') {
      self.viewPage('ONBOARDING')
    } else if (userStatus === 'ACTIVE') {
      switch (userType) {
        case 'ADMIN':
          document.getElementById('progressOverlay').style.display = 'inherit'
          self.sendEvent({name: 'get-admin-mapping', data: {userID: self.currentUser._id}})
          break
        case 'CLIENT':
          if (onboardedID !== '') {
            checkOnboardedEID()
          } else {
            self.viewPage('CLIENT')
          }
          break
        case 'AUDITOR':
          if (onboardedID !== '') {
            checkOnboardedEID()
          } else {
            self.viewPage('AUDITOR')
          }
          break
        default:
          log(`User is active but user does not have right type: ${self.currentUser.type}`)
          flashAlert.flash({ type: 'ERROR', content: 'There was an error while finding loading your page.' })
      }
    } else {
      log(`The user does not have appropriate status : ${self.currentUser.status}`)
      flashAlert.flash({ type: 'ERROR', content: 'There was an error while finding loading your page.' })
    }
  }

  /**
   * Checks to see if the current user has access to the dashboard / page.
   * TODO What about feature access?
   */
  var hasPageAccess = function (type) {
    var access = false
    switch (type) {
      case 'ADMIN':
        access = (adminModule !== null)
        break
      case 'AUDITOR':
        // access = (auditorModule !== null)
        access = (previewModule !== null)
        break
      case 'CLIENT':
        access = (clientModule !== null)
        break
      case 'ENGAGEMENT':
        access = (engagementModule !== null)
        break
      case 'ONBOARDING':
        access = (onboardingModule !== null)
        break
      case 'SETTINGS':
        access = (settingsModule !== null)
        break
      case 'CLIENTLIST':
        access = (clientListModule !== null)
        break
      default:
    }

    return access
  }

  /**
   * Responsible for tracking the previous page that the user was viewing and to display it.
   */
  this.goToPreviousPage = function () {
    log('Event: Go back to previous page')

    if (Auditor && previousModule instanceof Auditor) {
      document.body.style.background = '#f5f5f5'
      headerModule.viewFixedMode()
    } else {
      document.body.style.background = '#f5f5f5'
      headerModule.viewFixedMode()
    }

    currentModule.hide()
    var tempState = currentModule
    currentModule = previousModule
    previousModule = tempState
    currentModule.show()
  }

  /**
   * Responsible for tracking the previous page that the user was viewing and to display it.
   */
  this.goToPreviousEngagementPage = function () {
    self.registerForEvent('get-engagements', 'GET-ENGAGEMENT-FOR-RELOAD-000', function (tagName, data) {
      self.deregisterForEvent('get-engagements', 'GET-ENGAGEMENT-FOR-RELOAD-000')

      if (self.currentUser.type === 'AUDITOR') {
        self.viewPage('AUDITOR')
        previewModule.loadDashboard()
      } else if (self.currentUser.type === 'CLIENT') {
        self.viewPage('CLIENT')
        clientModule.loadDashboard(self.myEngagements)
        headerModule.removeAuditorLogo()
      }
    })
    self.getEngagements(self.currentUser._id, self.currentUser.type)
  }

  /**
   * Responsible for hiding the current page and displaying a new page based on the page
   * type request. It also updates the currentModule parameter in order to track which module is active.
   */
  this.viewPage = function (type) {
    if (currentModule && modules[type] === currentModule) {
      return
    }

    if (currentModule) {
      currentModule.hide()
      previousModule = currentModule
    }

    headerModule.setUsername({firstName: self.currentUser.firstName, lastName: self.currentUser.lastName, email: self.currentUser.email})
    if (type === 'ADMIN') {
      if (hasPageAccess(type)) {
        document.body.style.background = '#f5f5f5'
        document.body.style.overflowY = 'auto'

        headerModule.viewFixedMode()
        headerModule.setAdminHeader()
        currentModule = adminModule
        currentModule.show()
      } else {
        self.displayMessage('User does not have access to the Admin Dashboard', {color: 'red'})
      }
    } else if (type === 'CLIENT') {
      if (hasPageAccess(type)) {
        document.body.style.background = '#f5f5f5'
        document.body.style.overflowY = 'auto'

        headerModule.viewFixedMode()
        headerModule.setClientHeader()
        currentModule = clientModule
        currentModule.show()
      } else {
        self.displayMessage('User does not have access to the Client Dashboard', {color: 'red'})
      }
    } else if (type === 'ENGAGEMENT') {
      if (hasPageAccess(type)) {
        document.body.style.background = '#f5f5f5'
        document.body.style.overflowY = 'auto'
        headerModule.viewFixedMode()
        engagementModule.refresh()
        engagementModule.hideToDoComponents()

        if (self.currentUser.type === 'CLIENT') {
          engagementModule.setBorder()
        }
        previousModule = currentModule
        currentModule = engagementModule
        currentModule.show()
      } else {
        self.displayMessage('User does not have access to the Client Dashboard', {color: 'red'})
      }
    } else if (type === 'CLIENTLIST') {
      if (hasPageAccess(type)) {
        document.body.style.background = '#f5f5f5'
        document.body.style.overflowY = 'auto'
        headerModule.viewFixedMode()
        // headerModule.setAuditorHeader()
        currentModule = clientListModule
        currentModule.show()
      } else {
        self.displayMessage('User does not have access to the client list page', {color: 'red'})
      }
    } else if (type === 'AUDITOR') {
      if (hasPageAccess(type)) {
        document.body.style.background = '#f5f5f5'
        document.body.style.overflowY = 'auto'
        headerModule.viewFixedMode()
        headerModule.setAuditorHeader()
        // currentModule = auditorModule
        currentModule = previewModule
        currentModule.show()
      } else {
        self.displayMessage('User does not have access to the AUDITOR Dashboard', {color: 'red'})
      }
    } else if (type === 'ONBOARDING') {
      if (self.currentUser.status === 'ONBOARDING') {
        document.body.style.background = '#f5f5f5'
        if ($(window).height() > 768) {
          document.body.style.overflowY = 'auto'
        }
        headerModule.showColorBackgroundHeader()
        headerModule.hideNav()
        headerModule.viewOnboardingMode()

        currentModule = onboardingModule
        currentModule.show()
      } else {
        self.displayMessage('Only users in the onboarding state can access onboarding', {color: 'red'})
      }
    } else if (type === 'SETTINGS') {
      if (hasPageAccess(type)) {
        document.body.style.background = '#f5f5f5'
        document.body.style.overflowY = 'auto'
        headerModule.viewFixedMode()
        headerModule.showNav()
        currentModule = settingsModule
        currentModule.show()
      } else {
        self.displayMessage('User does not have access to the Settings Page', {color: 'red'})
      }
    } else {
      if (currentModule) {
        currentModule.show()
      }
      self.displayMessage('Unknown page type', {color: 'red'})
    }
  }

  /**
   * Called once we have a valid user connecting via socket server. This doesn't load the engagement
   * specific data, but instead focuses on the data that is required regardless of the user.
   */
  var loadUserData = function (serverData) {
    log('Loading related data into Controller ...', serverData)
    if (serverData.user) {
      self.currentUser = serverData.user
    }

    if (serverData.business) {
      self.currentBusiness = serverData.business
    }

    if (serverData.firm) {
      self.currentFirm = serverData.firm
    }

    if (serverData.engagements) {
      self.currentEngagement = {}
      self.myEngagements = COMMON.checkEngagementData(serverData.engagements)
    }

    if (self.currentUser.type === 'CLIENT') {
      let path = '/config'
      let configReq = new XMLHttpRequest()
      configReq.onreadystatechange = function () {
        if (configReq.readyState === 4 && configReq.status === 200) {
          let json = JSON.parse(configReq.responseText)
          gdriveConfig = json.features.gdrive
          let integrations = self.currentUser.integrations || []
          if (integrations.length > 0) {
            integrations.forEach((integration) => {
              if (integration.name === 'Google Drive') {
                self.currentIntegrations.gdrive.uid = integration.uid

                self.gdriveManager = new GDriveManager(gdriveConfig, self)

                self.gdriveManager.on('add-integration', (data) => {
                  self.sendEvent({ name: 'add-integration', data: data })
                  self.currentIntegrations.gdrive.uid = data.uid
                })
              }
            })
          } else {
            self.gdriveManager = new GDriveManager(gdriveConfig, self)

            self.gdriveManager.on('add-integration', (data) => {
              self.sendEvent({ name: 'add-integration', data: data })
            })
          }
        }
      }
      configReq.open('GET', path, true)
      configReq.send()
    }
  }

  /**
   * Responsible for resetting all of the engagement data fields and then reloading the data based
   * on the server response data that is passed in.
   * TODO Validation checks on the data.
   */
  var loadEngagementData = function (serverData) {
    log('Loading the Server Engagement Data ...')
    log(serverData)

    return new Promise((resolve, reject) => {
      self.currentEngagement = null
      // TODO - code cleanup
      // self.clientsList = []
      // self.auditorsList = []
      self.currentTodos = []
      self.currentFiles = []
      self.currentActivities = []

      // TODO - code cleanup
      var { engagementInfo, activities } = serverData.result
      // if (engagementInfo) {
      //   self.currentEngagement = engagementInfo
      //   loadTeam(engagementInfo.acl)
      //   self.auditorsList = self.getUsersByRole('AUDITOR')
      //   self.clientsList = self.getUsersByRole('CLIENT')

      if (!engagementInfo) {
        return reject()
      }

      self.currentEngagement = engagementInfo
      loadTeam(engagementInfo.acl)
      if (engagementInfo.todos) {
        var todos = COMMON.removeInactiveTodos(engagementInfo.todos)
        // TODO - code cleanup
        // todos = COMMON.addSearchDataToTodos(engagementInfo.todos, engagementInfo.categories, self.auditorsList, self.clientsList)
        todos = COMMON.addSearchDataToTodos(todos, engagementInfo.categories, engagementInfo.acl)
        engagementInfo.todos = todos
        self.currentTodos = engagementInfo.todos.reverse()
        // TODO - update currentEngagement.todos > currentTodos in the code base
      }

      if (engagementInfo.categories) {
        self.currentEngagement.categories = engagementInfo.categories.reverse()
      }

      if (engagementInfo.files) {
        self.currentFiles = engagementInfo.files
        // TODO - engagement.files: just fileIDs, currentFiles: fileDictionary
      }

      if (activities) {
        self.currentActivities = activities
      }
      resolve()
    })
  }

  this.getUsersByRole = function (queryRole, isNullObjectAdded = false, isAdminIncluded = false) {
    var users = self.currentEngagement.acl
    var returnedUsers = []
    if (isNullObjectAdded === true) {
      let nullObject = {}
      nullObject.id = ''
      returnedUsers.push(nullObject)
    }
    _.map(users, function (user) {
      if (isAdminIncluded === true) {
        if (self.currentUser.type.toUpperCase() === 'CLIENT') {
          if (user.role.toUpperCase() === queryRole && user.status.toUpperCase() === 'ACTIVE') {
            returnedUsers.push(user)
          }
        } else if (self.currentUser.type.toUpperCase() === 'AUDITOR') {
          if (user.role.toUpperCase() === queryRole && user.lead === true && user.status.toUpperCase() === 'ACTIVE') {
            returnedUsers.push(user)
          }
        }
      } else {
        if (user.role.toUpperCase() === queryRole && user.status.toUpperCase() === 'ACTIVE') {
          returnedUsers.push(user)
        }
      }
    })
    return returnedUsers
  }

  /**
   * Responsible for resetting loading the team members based on the engagement acl
   * TODO Validation checks on the data.
   */
  var loadTeam = function (acl) {
    self.currentTeam = []
    for (var i = 0; i < acl.length; i++) {
      if (acl[i].role === self.currentUser.type && acl[i].id !== self.currentUser._id) {
        // self.currentTeam.forEach(function (currentMember) {
        //   console.error(currentMember)
        //   if (currentMember.id === acl[i].id) {
        //     return
        //   }
        // })
        var memberPermission
        if (acl[i].lead) {
          memberPermission = 'Lead'
        } else {
          memberPermission = 'General'
        }
        let teamMember = {
          id: acl[i].id,
          name: acl[i].userInfo.firstName + ' ' + acl[i].userInfo.lastName,
          role: acl[i].userInfo.jobTitle,
          permission: memberPermission
        }
        self.currentTeam.push(teamMember)
      }
    }
    engagementModule.addTeamList()
  }
  /**
   * Triggers the functionality that occurs when the onboarding process for a user is completed. It
   * will check the user status to verify that they are now an ACTIVE user. If they are, then we set
   * all of the relevant data and then grant them access to their homepage.
   */
  this.onboardingComplete = function (data) {
    log('Onboarding process complete, MAC has been notified')
    log(data)

    log('Loading onboarding update data')
    this.currentUser = data.result.user
    // Update potentially associated Engagement if new Auditor user
    if (this.currentUser.role === 'AUDITOR') {
      Utility.findEngagement(this.currentUser.email, function (err, engagements) {
        if (err) {
          // do nothing
        } else if (engagements && engagements[0]) {
          self.updateEngagement(this.currentUser._id, engagements[0]._id)
        }
      })
    }

    if (this.currentUser.status !== 'ACTIVE') {
      flashAlert.flash({ type: 'ERROR', content: 'There was an error while onboarding you. Refresh the page and if problem persist, contact Auvenir.' })
    } else {
      displayProperPage()
    }
  }

  /**
   * Triggers the functionality of hitting the 'Skip Security' button on the skip security modal
   * It reads the onboarding data provided by the user and makes calls to update the database
   */
  this.acceptSecurityAgreement = function () {
    document.getElementById('progressOverlay').style.display = 'inherit'
    self.sendEvent({ name: 'onboarding-complete', data: { password: self.onboardingPassword, currentUserID: self.currentUser.currentUserID, agreementV: self.currentUser.agreements[0].agreementID, acceptedOn: Date() } })
  }

  this.submitNewEngagementRequest = function (firmID, auditorID, engagementInfo, companyName) {
    log('App: Request new engagement creation')

    document.getElementById('progressOverlay').style.display = 'inherit'
    this.sendEvent({name: 'create-engagement', data: {firmID, auditorID, currentUserID: self.currentUser._id, engagementInfo, businessName: companyName}})
  }

  /**
   * select a client for an engagement
   * @param {String} action enum SELECT, ADD
   * @param {String} engagementId
   * @param {String} clientId
   * @param {Object} user accepted fields: firstName, lastName, email, phone, profilePicture
   * @param {Object} business
   */
  this.selectClient = function (action, engagementId, clientId, user, business) {
    if (['SELECT', 'ADD', 'UPDATE'].indexOf(action) === -1) {
      return // Invalid action value
    }
    var payload = {
      action: action,
      engagementId: engagementId
    }

    if (['SELECT', 'UPDATE'].indexOf(action) > -1) {
      payload.clientId = clientId
    }
    if (['ADD', 'UPDATE'].indexOf(action) > -1) {
      payload.user = user
      payload.business = business
    }
    self.sendEvent({name: 'select-client', data: payload})
  }

  /**
   * Responsible for sending a request to the server to invite a user to join an engagement.
   *
   * @param engagementID String
   * @param email        String
   * @param subject      String
   * @param content      String
   */
  this.sendEngagementInvite = function (engagementID, email) {
    if (engagementID === undefined) {
      self.displayMessage('Need to provided engagement ID', {color: 'red'})
      return
    }
    if (email === undefined || !Utility.isEmailValid(email)) {
      self.displayMessage('Need to provide a valid email', {color: 'red'})
      return
    }

    document.getElementById('progressOverlay').style.display = 'inherit'
    var json = {
      engagementId: engagementID,
      auditor: self.currentUser,
      email: email
    }
    self.sendEvent({name: 'send-engagement-invite', data: json})
  }

  /**
   * Triggers a request to the server to download all of the engagement data for a specific
   * engagement ID. If the engagement ID exists for that specific user OR they are an admin,
   * then we allow them to submit a request to download the related engagement data.
   */
  this.loadEngagement = function (engagementID, page) {
    log('Load engagement ' + engagementID)
    if (engagementID) {
      var matches = Utility.findMatches(self.myEngagements, { '_id': engagementID })
      if ((matches && matches.length > 0)) { // || self.currentUser.type === 'ADMIN') {
        self.currentEngagement = matches[0]
        var currentFirm = self.currentEngagement.firm
        if (currentFirm.logoDisplayAgreed && currentFirm.logo && self.currentUser.type === 'CLIENT') {
          headerModule.addAuditorLogo(currentFirm)
        }
        self.sendEvent({name: 'load-engagement', data: { engagementID, page }})

        self.sendEvent({name: 'load-workingpaper-type', data: { engagementID, page }})

        self.sendEvent({name: 'load-test-type', data: { engagementID, page }})

        self.sendEvent({name: 'load-technique-type', data: { engagementID, page }})

        self.sendEvent({name: 'get-samples', data: { engagementID, page }})

        document.getElementById('progressOverlay').style.display = 'inherit'
      } else {
        this.displayMessage('Unable to find Engagement Object for ' + engagementID)
      }
    } else {
      this.displayMessage('Invalid EngagementID')
    }
  }

  this.getSamples = function (engagementID, data) {
    self.sendEvent({name: 'get-samples', data: data})
  }

  this.loadContacts = function () {
    log('Load contacts for ' + self.currentUser)
    var contactUsers = []

    if (self.currentUser.type === 'AUDITOR') {
      /* Load Lead Clients for engagements */
      for (var i = 0; i < self.myEngagements.length; i++) {
        let engagementUser = self.myEngagements[i].acl
        engagementUser.forEach(function (user) {
          if (user.role === 'CLIENT' && user.lead === true) {
            contactUsers.push(user)
          }
        })
      }
      /* Load other firm members */
      var firmAcl = self.currentFirm.acl
      firmAcl.forEach(function (member) {
        if (member.id !== self.currentUser._id) {
          contactUsers.push(member)
        }
      })
    } else if (self.currentUser.type === 'CLIENT') {
      /* Load Lead Auditors for engagements */
      for (var j = 0; j < self.myEngagements.length; j++) {
        let engagementUser = self.myEngagements[j].acl
        engagementUser.forEach(function (user) {
          if (user.role === 'AUDITOR') {
            contactUsers.push(user)
          }
        })
      }
      /* Load other business members */
      var businessAcl = self.currentBusiness.acl
      businessAcl.forEach(function (member) {
        if (member.id !== self.currentUser._id) {
          contactUsers.push(member)
        }
      })
    }
    self.sendEvent({name: 'find-contacts', data: contactUsers})
  }

  this.createActivity = function (data) {
    log('\nTriggering Socket Message: Create Activity')
    log(data)
    self.sendEvent({name: 'create-activity', data: data})
  }

  this.getActivities = function (data) {
    log('Fetching messages from server.')
    data.currentUserID = self.currentUser._id
    data.engagementID = data.engagementID
    self.sendEvent({name: 'get-activities', data: data})
  }

  this.getContactList = function (data) {
    log('Getting contact list')

    self.sendEvent({name: 'get-contact-list', data: data})
  }

  this.updateUser = function (data) {
    log('Triggering Socket Message: Update User')
    log(data)
    self.sendEvent({name: 'update-user', data: data})
  }

  this.updateUserPassword = function (data) {
    log('Triggering Socket Message: Update User Password')
    log(data)
    self.sendEvent({name: 'update-user-password', data: data})
  }

  this.updateBusiness = function (data) {
    log('Triggering Socket Message: Update Business')
    log(data)
    self.sendEvent({name: 'update-business', data: data})
  }

  this.updateFirm = function (data) {
    log('Triggering Socket Message: Update Firm')
    log(data)
    self.sendEvent({name: 'update-firm', data: data})
  }

  this.updateEngagement = function (data) {
    log('Triggering Socket Message: Update Engagement')
    log(data)
    self.sendEvent({ name: 'update-engagement', data: data })
  }

  this.deactivateAccount = function () {
    log('Triggering Socket Message: Deactivate Account')
    self.sendEvent({name: 'deactivate-account', data: {}})
  }

  this.addTeamMember = function (data) {
    log('\nTriggering Socket Message: Add Team Member')
    data.currentUser = self.currentUser
    log(data)
    // data contains userID, currentUser, and engagementID
    self.sendEvent({name: 'add-team-member', data: data})
  }

  this.sendTeamMemberInvite = function (engagementID, userInfo) {
    if (engagementID === undefined) {
      self.displayMessage('Need to provided engagement ID', {color: 'red'})
      return
    }
    if (userInfo.email === undefined || !Utility.isEmailValid(userInfo.email)) {
      self.displayMessage('Need to provide a valid email', {color: 'red'})
      return
    }

    document.getElementById('progressOverlay').style.display = 'inherit'
    let json = {
      engagementId: engagementID,
      currentUser: self.currentUser,
      teamMember: userInfo // this contains the same properties as user schema object
    }
    self.sendEvent({name: 'send-team-member-invite', data: json})
  }

  this.addMultipleTeamMembers = function (userInfo, engagementID) {
    log('\nTriggering Socket Message: Add Multiple Team Members')
    // Data consists of User {user schema object} & engagement id
    self.sendEvent({name: 'add-multiple-team-members', data: {userInfo, engagementID}})
  }

  this.sendMultipleTeamMemberInvites = function (engagementID) {
    log('\nTriggering Socket Message: Send Multiple Team Member Invites')

    if (self.myPendingUsers.length > 0) {
      let json = {
        engagementID: engagementID,
        currentUser: self.currentUser,
        users: self.myPendingUsers
      }
      self.sendEvent({name: 'send-multiple-team-member-invites', data: json})
    }
  }

  this.deleteEngagement = function (businessID, engagementID) {
    log('\nTriggering Socket Message: Delete Engagement')
    self.sendEvent({name: 'delete-engagement', data: {businessID, engagementID}})
  }

  this.getEngagements = function (userId, userRole) {
    log('\nTriggering Socket Message: Get Engagements')
    self.sendEvent({name: 'get-engagements', data: {userId, userRole}})
  }

  this.setUserPermission = function (engagementID, userID, role) {
    log('Triggering Socket Message: Set User Permission')
    self.sendEvent({name: 'set-user-permission', data: {engagementID, userID, role}})
  }

  this.removeTeamMember = function (userID, engagementID) {
    log('\nTriggering Socket Message: Remove Team Member')
    // data contains userID and engagementID
    self.sendEvent({name: 'remove-team-member', data: {userID, engagementID}})
  }

  this.getCSVString = function (dataList, fields) {
    log('\nTriggering Socket Message: Get CSV String')
    self.sendEvent({name: 'get-csv-string', data: {dataList, fields}})
  }

  /**
   * This function is called when user decides to cancel the file upload in the middle.
   * The fileID will be added to the ctrl.canceledFiles array and when the file upload is
   * completed, it will look for fileID in the array and call ctrl.cancelUploadFile
   */
  this.addToCanceledFiles = function (data) {
    log('Controller: Canceling File...')
    if (this.canceledFiles) {
      this.canceledFiles.push(data.uniqueID)
    } else {
      this.canceledFiles = [data.uniqueID]
    }
  }
  this.cancelUploadFile = function (data) {
    log('Triggering Cancel Upload File...')
    self.sendEvent({name: 'cancel-upload', data: data})
  }

  /**
   * This function is being used to uploadFile.
   */
  this.uploadFile = function (data) {
    log('Controller: Uploading a file ...')
    let { uniqueID, file, engagementID, requestID, todoID, progress, oldRequest, callback } = data
    let progressFunc = progress
    let callbackFunc = callback

    if (!file) {
      return
    }
    if (!callbackFunc || typeof callbackFunc !== 'function') {
      return
    }
    if (!engagementID) {
      return callbackFunc({ msg: 'No Engagement to upload the file to.'}, { uniqueID })
    }

    // URL Contruction
    var filePath = Utility.createFilePath(file)
    if (filePath.err) {
      filePath = '/'
    }

    let payload = { fname: file.name, fsize: file.size, fpath: filePath }
    let params = 'fname=' + file.name + '&fsize=' + file.size + '&fpath=' + filePath

    payload.eID = engagementID
    params += '&eID=' + engagementID

    if (requestID) {
      payload.rID = requestID
      params += '&rID=' + requestID
    }
    if (todoID) {
      payload.tID = todoID
      params += '&tID=' + todoID
    }

    var uri = '/file-upload?' + params
    var xhr = new XMLHttpRequest()
    var fd = new FormData()

    xhr.onload = function () {
      log('Uploaded file ' + file.name)
    }

    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        // SUCCESS
        if (xhr.status === 200) {
          log(`${xhr.status} OK - Upload Complete`)
          let result = JSON.parse(xhr.responseText)
          let { file, engagement } = result
          if (~self.canceledFiles.indexOf(uniqueID)) {
            self.cancelUploadFile({fileID: file._id})
          } else {
            file.owner = { _id: file.owner, firstName: self.currentUser.firstName, lastName: self.currentUser.lastName, email: self.currentUser.email}
            if (engagement) { // only when fetching engagement info was successful
              engagement.todos = COMMON.removeInactiveTodos(engagement.todos)
              if (self.currentEngagement) {
                if (self.currentEngagement._id.toString() === engagement._id.toString()) {
                  self.currentEngagement = engagement
                }
              }
              self.myEngagements.forEach((oneEngagement) => {
                if (oneEngagement._id.toString() === engagement._id.toString()) {
                  oneEngagement = engagement
                }
              })
            }
            engagement.todos = COMMON.addSearchDataToTodos(engagement.todos, self.currentEngagement.categories, self.currentEngagement.acl)
            var resultjson = {
              file,
              engagement,
              oldRequest,
              todoID,
              requestID,
              uniqueID
            }
            callbackFunc(null, resultjson)
          }
        // REQUEST ERROR
        } else if (xhr.status === 400) {
        // SERVER ERROR
        } else if (xhr.status === 500) {
          var errorjson = JSON.parse(xhr.responseText)
          callbackFunc(errorjson.msg, { uniqueID })
        } else {
          log('Unhandled server http status code (' + xhr.status + ')')
        }
      } else {
        // do nothing
      }
    }

    xhr.upload.onprogress = function (evt) {
      log(evt)
      if (evt.lengthComputable) {
        var rounded = Math.round(((evt.loaded / evt.total) * 100) * 10) / 10
        var percentComplete = Math.min(rounded, 100.0)
        if (progressFunc) {
          progressFunc(null, { uniqueID: uniqueID, value: percentComplete })
        }
      } else {
        if (progressFunc) {
          progressFunc('Error: Upload length is not computable')
        }
      }
    }

    xhr.open('POST', uri, true)
    fd.append('myFile', file)
    // Initiate a multipart/form-data upload
    xhr.send(fd)
  }

  /**
   * finicityConnect
   * @param data.currentUserID
   * @param data.currentBusinessID
   * @param data.institutionID  see ./finicity/plugins/institutionInfo.js
   * @event
   */

  this.finicityConnect = function finicityConnect (data) {
    log('Triggering Socket Message: Finicity Connect Request')
    log(data)
    self.sendEvent({name: 'fn-connect', data: data})
  }

  this.finicityLoginFormRequest = function finicityLoginFormRequest (data) {
    log('Triggering Socket Message: Finicity Login Form Request')
    log(data)
    self.sendEvent({name: 'fn-loginFormRequest', data: data})
  }

  this.finicityLoginSubmit = function finicityLoginSubmit (data) {
    log('Triggering Socket Message: Finicity Login Submit')
    log(data)
    self.sendEvent({name: 'fn-loginSubmit', data: data})
  }

  this.finicityLoginSecuritySubmit = function finicityLoginSecuritySubmit (data) {
    log('Triggering Socket Message: Finicity Login Security Submit')
    log(data)
    self.sendEvent({name: 'fn-loginSecuritySubmit', data: data})
  }

  this.finicityAccountListSelectedRequest = function finicityAccountListSelectedRequest (data) {
    log('Triggering Socket Message: Finicity Account List Selected Request')
    log(data)
    self.sendEvent({name: 'fn-accountListSelectedRequest', data: data})
    onboardingModule.addAccounts(data, self.bankName)
  }

  this.finicitySelectionSecuritySubmit = function finicitySelectionSecuritySubmit (data) {
    log('Triggering Socket Message: Finicity Selection Security Submit')
    log(data)
    self.sendEvent({name: 'fn-selectionSecuritySubmit', data: data})
  }

  this.finicityLoginExit = function finicityLoginExit (data) {
    log('Triggering Socket Message: Finicity Login Exit')
    log(data)
    self.sendEvent({name: 'fn-loginExit', data: data})
  }

  this.finicityLoginErrorExit = function finicityLoginErrorExit (data) {
    log('Triggering Socket Message: Finicity Login Error Exit')
    log(data)
    self.sendEvent({name: 'fn-loginErrorExit', data: data})
  }

  this.finicityLoginComplete = function finicityLoginComplete (data) {
    log('Triggering Socket Message: Finicity Login Complete')
    log(data)
    self.sendEvent({name: 'fn-loginComplete', data: data})
  }

  var handle_fnConnectResponse = function (tagName, data) {
    log('EVT> fn-connect response.')
    bankIntegrationModal.setSessionID(data.sessionID)
    self.finicityLoginFormRequest(data)
  }

  var handle_fnLoginFormRequestResponse = function (tagName, data) {
    log('EVT> fn-login-form-request response.')
    if (data) {
      bankIntegrationModal.finicityLoginFormResponse(data)
    } else {
      bankIntegrationModal.cantConnect()
    }
  }

  var handle_fnAccountListResponse = function (tagName, data) {
    log('EVT> fn-account-list-request response.')
    bankIntegrationModal.finicityAccountList(data)
  }

  var handle_fnAccountListSelectedResponse = function (tagName, data) {
    log('EVT> fn-account-list-selected-request response.')
    if (data.error) {
      bankIntegrationModal.cantConnect()
    } else {
      bankIntegrationModal.finicityLoginComplete(data)
      onboardingModule.showAccounts()
    }
  }

  var handle_fnLoginCompleteResponse = function (tagName, data) {
    log('EVT> fn-login-complete response.')
    log(data)
  }

  var handle_fnServerErrorResponse = function (tagName, data) {
    bankIntegrationModal.cantConnect()
  }

  this.moveToNextOnboarding = function () {
    onboardingModule.viewNextComponent()
  }

  /* This retrieves the relevant information for the clients welcome screen */
  this.auditInfoOnboarding = function () {
    var auditorName
    var clientName
    var bussinessName
    var auditorFirm
    var engagementName

    if (self.myEngagements && self.myEngagements.length === 1) {
      var engagement = self.myEngagements[0]
      for (var i = 0; i < engagement.acl.length; i++) {
        if (engagement.acl[i].role === 'AUDITOR' && engagement.acl[i].lead) {
          auditorName = engagement.acl[i].userInfo.firstName + ' ' + engagement.acl[i].userInfo.lastName
        }
        if (engagement.acl[i].role === 'CLIENT' && engagement.acl[i].lead && engagement.acl[i].id !== self.currentUser._id) {
          clientName = engagement.acl[i].userInfo.firstName + ' ' + engagement.acl[i].userInfo.lastName
          bussinessName = engagement.business.name
        }
        log(engagement.acl[i].id, self.currentUser._id)
      }
      log(engagement.business)
      auditorFirm = (engagement.firm.name) ? engagement.firm.name : 'a firm'
      engagementName = (engagement.name) ? engagement.name : ''
    }
    var auditInfo = {
      auditor: auditorName,
      client: clientName,
      bussiness: bussinessName,
      firm: auditorFirm,
      engagement: engagementName
    }
    log(auditInfo)
    return auditInfo
  }

  this.footer =
    `<footer class="footer" style="width:100%; position: fixed; bottom: 0; left: 0">
      <div class="container">
        <div class="lower-footer">
          <div class="pull-left">
            <span>© 2017 Auvenir Inc. All rights reserved. </span>
          </div>
          <div class="pull-right">
            <a href="/terms" target="_blank"> Terms of Service </a>
            <a class="footer-dot">.</span>
            <a href="/privacy" target="_blank"> Privacy Statement </a>
            <a class="footer-dot">.</span>
            <a href="/cookies" target="_blank"> Cookie Notice </a>
          </div>
        </div>
      </div>
    </footer>`

  return this
}

module.exports = Controller
