'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import styles from './Modal_CreateEngagement.css'
import Utility from '../../../js/src/utility_c'

import Component_SetUp from '../../components/setup/setup'
import Component_Team from '../../components/team/team'
import Component_Customize from '../../components/customize/customize'

var Modal_CreateEngagement = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  var c = ctrl
  var self = this
  var text = c.lang.createEngagementModal
  var currentStep = 0
  let nextStep
  var currentComponent = null

  /**
   * This function is responsible for initializing the creation of a new Client
   * @memberof Modal_CreateEngagement
   */
  var initialize = function () {
    log('Modal_CreateEngagement: initialize')
    self.setModalName('engagementModel')
    self.setClose({'window': true, 'image': 'images/icons/x-small-white.svg'})
    self.setFullScreen(true)
    self.setHeader({'show': false})
    self.setPosition({'bottom': '0px'})
    self.setPadding('0px')
    self.build()
    self.setupListeners()
    firstComponent()
  }

  var components = {
    setUp: { linkID: 'link-ce-setup', obj: null, displayID: 'ce-setup-container', title: text.setupTitle, step: 0, complete: false },
    team: { linkID: 'link-ce-team', obj: null, displayID: 'ce-team-container', title: text.teamTitle, step: 1, complete: false },
    customize: { linkID: 'link-ce-customize', obj: null, displayID: 'ce-customize-container', title: text.customizeTitle, step: 2, complete: false }
  }
  var componentArray = [
    components.setUp,
    components.team,
    components.customize
  ]

  var componentContainer = 'create-engagement-component-container'

  var firstComponent = function () {
    currentComponent = components.setUp
    document.getElementById(currentComponent.displayID).style.display = 'inherit'
    document.getElementById(currentComponent.linkID + 'circle').classList.toggle('ce-numberCircle-active')
  }

  var viewNextComponent = function () {
    nextStep = currentStep + 1
    // if (nextStep === 3) {
    //    self.close()
    // } else {
    //   viewComponent(nextStep)
    // }

    // The following is an example of the engagement flow backend function calls
    if (nextStep === 1) {
      // Set-up

      // Test if the Setup page is filled out and return out if not.
      if (checkSetup()) {
        return
      }

      let engName = document.getElementById('engagement-name').value
      let engType = document.getElementById('engagement-type').value
      let deadlineUnformated = document.getElementById('engagement-deadline').value.split('/')
      let dateStartUnformated = document.getElementById('engagement-date-range-start').value.split('/')
      let dateEndUnformated = document.getElementById('engagement-date-range-end').value.split('/')
      let deadline = new Date(Number(deadlineUnformated[2]), Number(deadlineUnformated[0]) - 1, Number(deadlineUnformated[1]))
      let dateStart = new Date(Number(dateStartUnformated[2]), Number(dateStartUnformated[0]) - 1, Number(dateStartUnformated[1]))
      let dateEnd = new Date(Number(dateEndUnformated[2]), Number(dateEndUnformated[0]) - 1, Number(dateEndUnformated[1]))
      let engagementInfo = {name: engName, type: engType, dueDate: deadline, bankStatementDateRange: {startDate: dateStart, endDate: dateEnd}}
      let businessName = document.getElementById('engagement-company').value
      document.getElementById('m-ce-modalTitle').innerHTML = engName
      c.submitNewEngagementRequest(c.currentFirm._id, c.currentUser._id, engagementInfo, businessName)

      document.getElementById('m-ce-addBtn').style.display = 'none'
      document.getElementById('m-ce-cancelBtn').style.display = 'none'
    }
    if (nextStep === 2) {
      // Team
      document.getElementById('m-ce-addBtn').style.display = 'none'
      document.getElementById('m-ce-cancelBtn').style.display = 'none'
    }

    if (nextStep === 3) {
      // Customize - Create Your team
      // WE ARE CURRRENTLY ASSUMING THAT CONTINUE IS CREATE TO DO
      components.customize.complete = true
      c.sendMultipleTeamMemberInvites(c.currentEngagement._id)
      clearNewEngModal()
    }

    if (nextStep === 4) {
      // Customize - Rollover
      // DeleteEngagement should be called when user cancels the creation of a new engagement
      // c.deleteEngagement(c.myPendingBusiness._id, c.currentEngagement._id)
      components.customize.complete = true
      c.sendMultipleTeamMemberInvites(c.currentEngagement._id)
      clearNewEngModal()
    }
    if (nextStep < 3) {
      viewComponent(nextStep)
    }
  }

  const checkSetup = () => {
    let name = document.getElementById('engagement-name').value
    let type = document.getElementById('engagement-type').value
    let company = document.getElementById('engagement-company').value
    let deadline = document.getElementById('engagement-deadline').value
    let dateRangeStart = document.getElementById('engagement-date-range-start').value
    let dateRangeEnd = document.getElementById('engagement-date-range-end').value
    let hasFailed = false

    if (name === '' || !Utility.REGEX.alphanumericName.test(name)) {
      document.getElementById('engagement-name').classList.add('auv-error')
      hasFailed = true
    } else {
      document.getElementById('engagement-name').classList.remove('auv-error')
    }
    if (type === '' || !Utility.REGEX.businessName.test(type)) {
      document.getElementById('engagement-type').classList.add('auv-error')
      hasFailed = true
    } else {
      document.getElementById('engagement-type').classList.remove('auv-error')
    }
    if (company === '' || !Utility.REGEX.businessName.test(company)) {
      document.getElementById('engagement-company').classList.add('auv-error')
      hasFailed = true
    } else {
      document.getElementById('engagement-company').classList.remove('auv-error')
    }
    if (deadline === '' || !Utility.REGEX.date.test(deadline) || !Utility.isDateInFuture(deadline, true)) {
      document.getElementById('engagement-deadline').classList.add('auv-error')
      hasFailed = true
    } else {
      document.getElementById('engagement-deadline').classList.remove('auv-error')
    }
    if (dateRangeStart === '' || !Utility.REGEX.date.test(dateRangeStart) || Utility.validateStartEndDate(dateRangeStart, dateRangeEnd)) {
      document.getElementById('engagement-date-range-start').classList.add('auv-error')
      hasFailed = true
    } else {
      document.getElementById('engagement-date-range-start').classList.remove('auv-error')
    }
    if (dateRangeEnd === '' || !Utility.REGEX.date.test(dateRangeEnd) || Utility.validateStartEndDate(dateRangeStart, dateRangeEnd)) {
      document.getElementById('engagement-date-range-end').classList.add('auv-error')
      hasFailed = true
    } else {
      document.getElementById('engagement-date-range-end').classList.remove('auv-error')
    }

    if (hasFailed) {
      return true
    }
  }

  /**
   * Clears the New Engagement Modal to be used again
   */
  let clearNewEngModal = () => {
    self.close()
    currentStep = 0
    nextStep = 0
    viewComponent(0)

    let clearContent = (contentID) => {
      document.getElementById(contentID).value = ''
      document.getElementById(contentID).classList.remove('auv-error')
    }
    clearContent('engagement-name')
    clearContent('engagement-type')
    clearContent('engagement-deadline')
    clearContent('engagement-date-range-start')
    clearContent('engagement-date-range-end')
    clearContent('engagement-company')
    document.getElementById('m-ce-modalTitle').innerHTML = 'New Engagement'
    document.getElementById('m-ce-addBtn').style.display = 'block'
    document.getElementById('m-ce-cancelBtn').style.display = 'block'
    if (document.getElementById('m-ne-teamMemberName')) {
      clearContent('m-ne-teamMemberName')
      clearContent('m-ne-teamMamberEmail')
      clearContent('m-ne-teamMamberEmailSecond')
      clearContent('m-ne-teamMemberRole')
    }
    components.team.obj.viewTeamCreation()
    if (document.getElementById('team-addMemberBtn')) {
      document.getElementById('team-addMemberBtn').style.display = 'none'
    }
    document.getElementById('c-team-options').style.display = 'inherit'

    if (document.getElementById('team-invitedList')) {
      const removeInvitedList = document.getElementById('team-invitedList')
      while (removeInvitedList.firstChild) {
        removeInvitedList.removeChild(removeInvitedList.firstChild)
      }
    }
    if (document.getElementById('team-selectMember')) {
      const removeSelectMember = document.getElementById('team-selectMember')
      while (removeSelectMember.firstChild) {
        removeSelectMember.removeChild(removeSelectMember.firstChild)
      }
    }
  }

  /**
   * Changes the front end UI for the navigation system, and hides and shows the appropriate component.
   * If its toggling past the security page, it skips to the Dashboard.
   *
   * @param {object} name - The name of the component to show.
   */

  var viewComponent = function (n) {
    if (n < currentComponent.step) {
      for (let i = n; i < componentArray.length; i++) {
        componentArray[i].complete = false
        document.getElementById(componentArray[i].linkID + 'num').innerText = componentArray[i].step + 1
        document.getElementById(componentArray[i].linkID + 'num').classList.remove('auvicon-checkmark', 'ce-nav-checkmark')
        document.getElementById(componentArray[i].linkID + 'circle').classList.remove('ce-numberCircle-active')
        document.getElementById(componentArray[i].linkID + 'circle').style.cursor = 'default'
      }
    } else {
      document.getElementById(currentComponent.linkID + 'num').innerText = ''
      document.getElementById(currentComponent.linkID + 'num').classList.add('auvicon-checkmark', 'ce-nav-checkmark')
      document.getElementById(currentComponent.linkID + 'circle').classList.add('ce-numberCircle-active')
      document.getElementById(currentComponent.linkID + 'circle').style.cursor = 'pointer'
    }

    document.getElementById(currentComponent.displayID).style.display = 'none'
    currentStep = n
    currentComponent = componentArray[n]
    document.getElementById(currentComponent.displayID).style.display = 'inherit'
    document.getElementById(currentComponent.linkID + 'circle').classList.toggle('ce-numberCircle-active')
    document.getElementById(currentComponent.linkID + 'title').classList.toggle('ce-navLink-active')
  }

  /**
   * This function is responsible for building the page only when called.
   * @memberof Modal_CreateEngagement
   */
  this.buildDom = function () {
    log('Building CreateEnagement Modal')

    var parentContainer = Forge.build({'dom': 'div', 'id': 'CreateEngagementParent', class: 'ce-parent'})
    var header = Forge.build({'dom': 'div', 'class': 'ce-header'})
    var headerTitle = Forge.build({'dom': 'h2', id: 'm-ce-modalTitle', 'class': 'ce-headerTitle', 'text': text.headerTitle})
    header.appendChild(headerTitle)

    var systemContainer = Forge.build({'dom': 'div', 'class': 'ce-systemContainer', 'id': 'm-ce-systemContainer'})
    var navigationSystem = Forge.build({'dom': 'div', 'id': 'm-ce-navSystem', 'class': 'ce-navSystem'})
    var componentSystem = Forge.build({'dom': 'div', 'id': componentContainer, 'class': 'ce-componentContainer'})

    var setUpComponent = Forge.build({'dom': 'div', 'id': components.setUp.displayID, 'class': 'ce-individualContainer'})
    var teamComponent = Forge.build({'dom': 'div', 'id': components.team.displayID, 'class': 'ce-individualContainer'})
    var customizeComponent = Forge.build({'dom': 'div', 'id': components.customize.displayID, 'class': 'ce-individualContainer'})

    components.setUp.obj = new Component_SetUp(c, setUpComponent, this)
    components.team.obj = new Component_Team(c, teamComponent, this)
    components.customize.obj = new Component_Customize(c, customizeComponent, this)

    componentSystem.appendChild(setUpComponent)
    componentSystem.appendChild(teamComponent)
    componentSystem.appendChild(customizeComponent)

    var navSystemLinkHolderOne = Forge.build({'dom': 'div', 'id': components.setUp.linkID, 'class': 'ce-navLinkHolder'})
    var navSystemLinkOne = Forge.build({'dom': 'span', 'id': components.setUp.linkID + 'title', 'text': components.setUp.title, 'class': 'ce-navLinkTxt'})
    var navNumberCircleOne = Forge.build({'dom': 'div', 'id': components.setUp.linkID + 'circle', 'class': 'ce-numberCircle'})
    var navNumberOne = Forge.build({'dom': 'span', 'text': 1, 'class': 'ce-number', 'id': components.setUp.linkID + 'num'})

    var navSystemLinkHolderTwo = Forge.build({'dom': 'div', 'id': components.team.linkID, 'class': 'ce-navLinkHolder'})
    var navSystemLinkTwo = Forge.build({'dom': 'span', 'id': components.team.linkID + 'title', 'text': components.team.title, 'class': 'ce-navLinkTxt'})
    var navNumberCircleTwo = Forge.build({'dom': 'div', 'id': components.team.linkID + 'circle', 'class': 'ce-numberCircle'})
    var navNumberTwo = Forge.build({'dom': 'span', 'text': 2, 'class': 'ce-number', 'id': components.team.linkID + 'num'})
    var navLinkHrOne = Forge.build({'dom': 'hr', 'class': 'ce-linkHr'})
    navSystemLinkHolderTwo.appendChild(navLinkHrOne)

    var navSystemLinkHolderThree = Forge.build({'dom': 'div', 'id': components.customize.linkID, 'class': 'ce-navLinkHolder'})
    var navSystemLinkThree = Forge.build({'dom': 'span', 'id': components.customize.linkID + 'title', 'text': components.customize.title, 'class': 'ce-navLinkTxt'})
    var navNumberCircleThree = Forge.build({'dom': 'div', 'id': components.customize.linkID + 'circle', 'class': 'ce-numberCircle'})
    var navNumberThree = Forge.build({'dom': 'span', 'text': 3, 'class': 'ce-number', 'id': components.customize.linkID + 'num'})
    var navLinkHrTwo = Forge.build({'dom': 'hr', 'class': 'ce-linkHr'})
    navSystemLinkHolderThree.appendChild(navLinkHrTwo)

    navNumberCircleOne.appendChild(navNumberOne)
    navSystemLinkHolderOne.appendChild(navSystemLinkOne)
    navSystemLinkHolderOne.appendChild(navNumberCircleOne)

    navNumberCircleTwo.appendChild(navNumberTwo)
    navSystemLinkHolderTwo.appendChild(navSystemLinkTwo)
    navSystemLinkHolderTwo.appendChild(navNumberCircleTwo)

    navNumberCircleThree.appendChild(navNumberThree)
    navSystemLinkHolderThree.appendChild(navSystemLinkThree)
    navSystemLinkHolderThree.appendChild(navNumberCircleThree)

    navigationSystem.appendChild(navSystemLinkHolderOne)
    navigationSystem.appendChild(navSystemLinkHolderTwo)
    navigationSystem.appendChild(navSystemLinkHolderThree)

    var footer = Forge.build({'dom': 'div', 'class': 'ce-footer'})
    var footerBtnHolder = Forge.build({'dom': 'div', 'class': 'ce-footerBtnHolder'})
    var footerCancelBtn = Forge.buildBtn({
      'text': c.lang.cancel,
      'id': 'm-ce-cancelBtn',
      'type': 'secondary',
      'margin': '22px 0px 22px 11px',
      'float': 'right'
    }).obj
    var footerContinueBtn = Forge.buildBtn({
      'text': c.lang.continue,
      'id': 'm-ce-addBtn',
      'type': 'primary',
      'margin': '22px 83px 22px 11px',
      'float': 'right'
    }).obj

    footerContinueBtn.addEventListener('click', function () {
      currentComponent.complete = true
      viewNextComponent()
    })

    footerCancelBtn.addEventListener('click', function () {
      clearNewEngModal()
    })

    footerBtnHolder.appendChild(footerContinueBtn)
    footerBtnHolder.appendChild(footerCancelBtn)
    footer.appendChild(footerBtnHolder)

    systemContainer.appendChild(header)
    header.appendChild(navigationSystem)
    systemContainer.appendChild(componentSystem)
    systemContainer.appendChild(footer)
    parentContainer.appendChild(systemContainer)

    return parentContainer
  }

  this.onClose = () => {
    let modal = document.getElementById(self._modalID)
    modal.parentNode.removeChild(modal)
  }

  this.setupListeners = function () {
    // let modal = document.getElementById(self._modalID)
    // this.addEventListener('modal-close', (e) => {
    //   e.target.parentNode.removeChild(e.target)
    // })

    components.team.obj.on('continue', function () {
      components.team.complete = true
      viewNextComponent()
    })

    components.customize.obj.on('close', function () {
      components.customize.complete = true
      c.deleteEngagement(c.myPendingBusiness._id, c.currentEngagement._id)
      clearNewEngModal()
    })

    $('#engagement-type').prop('readonly', true)
    $('#engagement-type').focusout(function () {
      if ($(this).val() === c.lang.setupComponent.other) {
        $(this).val(c.lang.pleaseType)
        $(this).prop('readonly', false)
      } else {
        $('#engagement-type').prop('readonly', true)
      }
      if ($(this).val() === c.lang.pleaseType) {
        $(this).focus()
        $(this).select()
      }
    })

    $('#engagement-type').click(function () {
      if ($(this).val() === c.lang.pleaseType) {
        $(this).val(null)
      }
    })

    components.customize.obj.on('create', function () {
      viewNextComponent()
      if (c.currentEngagement._id) {
        c.loadEngagement(c.currentEngagement._id)
      }
    })
  }

  this.onClose = function () {
    clearNewEngModal()
  }

  initialize()
}

Modal_CreateEngagement.prototype = ModalTemplate.prototype
Modal_CreateEngagement.prototype.constructor = Modal_CreateEngagement

module.exports = Modal_CreateEngagement
