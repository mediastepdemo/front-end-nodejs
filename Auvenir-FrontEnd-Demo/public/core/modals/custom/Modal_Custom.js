'use strict'

import ModalTemplate from '../ModalTemplate'
import Utility from '../../../js/src/utility_c'
import log from '../../../js/src/log'
import Forge from '../../Forge'

/**
   * Displays a custom modal
   * @class Modal_Custom
   * @param {Object} ctrl           - A reference to the current instance of the Main Application Controller (Controller.js)
   * @param {Object} initialSettings - The custom settings that are used to generate the instance of this modal.
   */
var Modal_Custom = function (ctrl, initialSettings) {
  ModalTemplate.call(this, ctrl, initialSettings)
  var c = ctrl
  var self = this
  var contentId = ''
  var randomStr = ''

  do {
    randomStr = Utility.randomString(8, Utility.ALPHANUMERIC)
    contentId = 'm-content-' + randomStr
    this.contentID = contentId
  } while (document.getElementById(contentId))

  var defaultSettings = {
    color: 'black',
    borderWidth: 0,
    modalName: 'defaultCustomModal',
    close: { window: true, global: false, keystroke: 27 },
    width: '460px',
    height: 'auto',
    background: '#ffffff',
    header: { background: '', show: false, color: '', icon: '' },
    position: { top: '0px', bottom: '0px', left: '0px', right: '0px' },
    title: null,
    footer: false,
    fullscreen: false
  }

  /**
   * Responsible for building the DOM structure for the custom modal.
   * @memberof Modal_Custom
   * @return {DOM} content
   */
  this.buildDom = function () {
    var content = Forge.build({ 'dom': 'div', 'id': contentId, 'text': 'Not Available' })
    return content
  }

  /**
   * Sets the content of the modal to be a string with styling.
   * @memberof Modal_Message
   *
   * @param {String} str   - String text for the message modal
   * @param {Object} settings - A json representation of the styling on the modal
   */
  this.setText = function (str, settings) {
    if (settings) {
      if (typeof settings === 'object') {
        if (settings.color) {
          if (typeof settings.color === 'string') {
            defaultSettings.color = settings.color
          }
        }
        if (settings.border) {
          if (typeof settings.border === 'number') { defaultSettings.borderWidth = settings.border }
        }
      }
    }
    // please don't change order of the codes from here to below, otherwise, it will break.
    self.setSettings(settings)

    $('#' + contentId).html('')
    if (this.isOpen()) {
      // When Message Modal is opened, then we have to add this string to our message modal.
      $('#' + contentId).html('<div style="padding: 15px; border: ' + defaultSettings.borderWidth + 'px solid ' + defaultSettings.color + ';color:' + defaultSettings.color + '">' + str + '</div><br/>' + $('#' + contentId).html())
    } else {
      // When Message Modal is not opened, User already closed previous error messages, so we should delete these messages.
      $('#' + contentId).html('<div style="padding: 15px; border: ' + defaultSettings.borderWidth + 'px solid ' + defaultSettings.color + ';color:' + defaultSettings.color + '">' + str + '</div>')
    }
  }

  /**
   * Sets the content of the modal to be a specific DOM object structure.
   * @memberof Modal_Message
   *
   * @param {String} obj      - Dom for the modal
   * @param {JSON}   settings - A json representation of the styling on the dom elements & modal settings
   */
  this.setObject = function (obj, settings) {
    if (settings) {
      if (typeof settings === 'object') {
        if (settings.color) {
          if (typeof settings.color === 'string') {
            defaultSettings.color = settings.color
          }
        }
        if (settings.border) {
          if (typeof settings.border === 'number') { defaultSettings.borderWidth = settings.border }
        }
      }
      // please don't change order of the codes from here to below, otherwise, it will break.
      self.setSettings(settings)

      $('#' + contentId).css('color', defaultSettings.color)
      document.getElementById(this.contentID).innerHTML = ''
      document.getElementById(this.contentID).appendChild(obj)
    }
  }

  if (initialSettings) {
    self.setSettings(initialSettings)
  } else {
    self.setSettings(defaultSettings)
  }

  self.build()
}

Modal_Custom.prototype = ModalTemplate.prototype
Modal_Custom.prototype.constructor = Modal_Custom

module.exports = Modal_Custom
