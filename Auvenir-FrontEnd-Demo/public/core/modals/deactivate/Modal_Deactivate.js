'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import styles from './Modal_Deactivate.css'
/**
 * The Deactivate Modal is responsible for deactivating the user account.
 * @class Modal_Deactivate
 *
 * @param {Object} ctrl      - A reference to the current instance of the Main Application Controller (Controller.js)
 * @param {Object} settings - The custom settings that are used to generate the instance of this modal.
 */
var Modal_Deactivate = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  var self = this

  /**
   * Responsible for building the DOM elements that are associated with the Deactivate Account Modal,
   * and attaching them to the parent modal container.
   * @memberof Modal_Deactivate
   */
  this.buildDom = function () {
    log('Building Deactivate Modal')

    var submitContainer = Forge.build({'dom': 'div'})
    var submitText = Forge.build({'dom': 'div', 'id': 'deactivateAccountText', 'class': 'skip-text', 'text': 'You are about to deactivate your account. Once deactivated, all of the information associated with this account will no longer be accessible to you.'})
    var cancelBtn = Forge.build({'dom': 'input', 'type': 'button', 'class': 'btn m-dm-cancelBtn', 'value': 'Cancel'})
    var deactivateBtn = Forge.build({'dom': 'input', 'type': 'button', 'class': 'btn m-dm-deactivateBtn', 'value': 'Deactivate'})

    submitContainer.appendChild(submitText)
    submitContainer.appendChild(cancelBtn)
    submitContainer.appendChild(deactivateBtn)

    deactivateBtn.addEventListener('click', function () {
      self.trigger('deactive')
    })

    cancelBtn.addEventListener('click', function () {
      self.close()
    })

    return submitContainer
  }

  self.setModalName('deactivateModal')
  self.setClose({'window': true, 'global': false})
  self.setWidth('480px')
  self.setHeader({'show': true, 'color': '#f3672f', 'icon': 'warning', 'background': '#f6f6f6'})
  self.setPosition({ 'bottom': '0px'})
  self.setTitle('Deactivate Account')
  self.setHeight('395px')
  self.build()
}

Modal_Deactivate.prototype = ModalTemplate.prototype
Modal_Deactivate.prototype.constructor = Modal_Deactivate

module.exports = Modal_Deactivate
