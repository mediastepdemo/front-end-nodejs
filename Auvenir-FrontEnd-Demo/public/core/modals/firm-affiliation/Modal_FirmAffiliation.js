'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import styles from './Modal_FirmAffiliation.css'

/**
 * Displays an email input modal
 * @class Modal_Email
 *
 * @param {Object} ctrl      - A reference to the current instance of the Main Application Controller (Controller.js)
 * @param {Object} settings - The custom settings that are used to generate the instance of this modal.
 */
var Modal_FirmAffiliation = function (ctrl, settings) {
  var c = ctrl
  var text = c.lang.firmAffiliationModal

  ModalTemplate.call(this, ctrl, settings)
  this.configure = function (receivers, subject, content) {

  }

  this.buildDom = function () {
    function buildHeader () {
      var header = Forge.build({'dom': 'div', 'class': 'm-fa-header'})
      var headerText = Forge.build({'dom': 'div', 'class': 'm-fa-headerText', 'text': text.header})

      header.appendChild(headerText)

      return header
    }
    var contentBody = Forge.build({ 'dom': 'div', 'class': 'm-fa-bodyText'})
    for (var i = 0; i < text.body.length; i++) {
      var contentLine = Forge.build({ 'dom': 'div', 'class': 'm-fa-bodyLine', 'text': text.body[i]})
      contentBody.appendChild(contentLine)
      if ((i + 1) === text.body.length) {
        contentLine.style.marginBottom = '5px'
      }
    }

    var closeBtn = Forge.buildBtn({'text': c.lang.close, 'type': 'secondary', 'margin': '15px auto 32px'}).obj

    closeBtn.addEventListener('click', function () {
      ctrl.closeFirmAffiliationModal()
    })

    var firmAffiliatedContainer = Forge.build({dom: 'div', class: 'm-fa-container', style: 'margin:-15px; font-family: Lato;'})
    firmAffiliatedContainer.appendChild(buildHeader())
    firmAffiliatedContainer.appendChild(contentBody)
    firmAffiliatedContainer.appendChild(closeBtn)

    return firmAffiliatedContainer
  }

  this.setModalName('firmAffiliation')
  this.setClose({'window': true, 'global': true})
  this.setWidth('609px')
  this.setHeight('634px')
  this.setPosition({'top': '0%'})
  this.build()
}

module.exports = Modal_FirmAffiliation
