
'use strict'

import ModalTemplate from '../ModalTemplate'
import Utility from '../../../js/src/utility_c'
import Forge from '../../Forge'
import log from '../../../js/src/log'

import styles from './Modal_AddNewClient.css'

var Modal_AddNewClient = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  var c = ctrl
  var self = this
  var text = c.lang.newClientModal

  /**
   * This function is responsible for initializing the creation of a new Client
   * @memberof Modal_AddNewClient
   */
  var initialize = function () {
    log('Modal_AddNewClient: initialize')

    self.setModalName('dashboardModel')
    self.setClose({'window': true, 'image': 'images/icons/x-small-white.svg'})
    self.setFullScreen(true)
    self.setHeader({'show': false})
    self.setPosition({'bottom': '0px'})
    self.setZIndex('1015')
    self.setOverflow('auto')
    self.setPadding('0px')
    self.build()
    self.onClose = function () {
      refreshDom()
    }
  }

  const fields = {
    name: {id: 'm-ac-name', value: ''},
    firstName: {value: ''},
    lastName: {value: ''},
    email: {id: 'm-ac-email', value: ''},
    emailVerify: {id: 'm-ac-emailVerify'},
    role: {id: 'm-ac-role', value: ''}
  }

  this.setTitle = (title) => {
    document.getElementById('m-addClient-title').innerHTML = title
  }

  /**
   * This function is responsible for building the dom
   * @memberof Modal_AddNewClient
   */
  this.buildDom = function () {
    log('Modal_AddNewClient: building DOM')

    var superContainer = Forge.build({'dom': 'div', 'id': 'add-client-super-container', 'style': 'height:100%'})
    var header = Forge.build({'dom': 'div', 'class': 'inm-header'})
    var headerTitle = Forge.build({'dom': 'h2', 'class': 'inm-headerTitle', 'id': 'm-addClient-title'})
    header.appendChild(headerTitle)
    superContainer.appendChild(header)

    var title = Forge.build({'dom': 'p', 'class': 'addClient-header', 'text': 'Invite New Client'})
    superContainer.appendChild(title)

    var mainContainter = Forge.build({'dom': 'form', 'style': ' width: 458px; margin: 32px auto; text-align: left;'})

    var contactNameInput = Forge.buildInput({
      'label': text.name,
      'errorText': text.nameError,
      'id': fields.name.id,
      'width': '378px',
      'errorLeft': '272px'
    })
    var contactEmailInput = Forge.buildInput({
      'label': text.email,
      'errorText': text.emailError,
      'id': fields.email.id,
      'width': '378px',
      'errorLeft': '382px'
    })
    var contactVerifyEmailInput = Forge.buildInput({
      'label': text.verifyEmail,
      'errorText': text.verifyEmailError,
      'id': fields.emailVerify.id,
      'width': '378px',
      'errorLeft': '382px'
    })
    var contactRoleInput = Forge.buildInput({
      'label': text.role,
      'errorText': text.roleError,
      'id': fields.role.id,
      'width': '378px',
      'errorLeft': '272px'
    })

    mainContainter.appendChild(contactNameInput)
    mainContainter.appendChild(contactEmailInput)
    mainContainter.appendChild(contactVerifyEmailInput)
    mainContainter.appendChild(contactRoleInput)

    var footer = Forge.build({'dom': 'div', 'class': 'addClient-footer'})
    var footerCancelBtn = Forge.buildBtn({
      'text': text.cancel,
      'id': 'm-ac-cancelBtn',
      'type': 'secondary',
      'width': '131px',
      'margin': '22px 11px'
    }).obj
    var footerAddBtn = Forge.buildBtn({
      'text': text.invite,
      'id': 'm-ac-addBtn',
      'type': 'primary',
      'width': '131px',
      'margin': '22px 11px'
    }).obj
    footerAddBtn.disabled = true

    footer.appendChild(footerCancelBtn)
    footer.appendChild(footerAddBtn)

    superContainer.appendChild(mainContainter)
    superContainer.appendChild(footer)

    // Validation on input fields.
    /**
     * Checks for < or > and fails if they are detected.
     */
    const testCharacters = (value) => {
      let CheckSum = {REGEX: {notAllowed: /[<>[\]{}()'"]/}}
      if (CheckSum.REGEX.notAllowed.test(value)) {
        return false
      } else {
        return true
      }
    }

    /**
     * Each Input has its validation is passed here instead of having this function for each input
     * Vallidation shows error on blur, while on each keyup it will only attach a pass/fail class
     * that gives no user feedback. There is propbably a better way to do this though.
     */
    const inputCheck = (element, testType, array) => {
      element.addEventListener('blur', () => {
        if (testType()) {
          array = element.value
          element.classList.remove('auv-error')
          element.classList.remove('auv-validationFailed')
        } else {
          element.classList.add('auv-error')
        }
      })
      element.addEventListener('keyup', () => {
        if (testType()) {
          element.classList.remove('auv-validationFailed')
          element.classList.remove('auv-error')
        } else {
          element.classList.add('auv-validationFailed')
        }
        self.checkInputValidation()
      })
    }

    /*
     * Functions containing the validation to run for each field.
     */
    const nameValidation = () => { return (testCharacters(contactNameInput.children[1].value)) }
    const emailValidation = () => { return (Utility.REGEX.email.test(contactEmailInput.children[1].value)) }
    const emailVerifyValidation = () => { return (contactEmailInput.children[1].value === contactVerifyEmailInput.children[1].value) }
    const roleValidation = () => { return (testCharacters(contactRoleInput.children[1].value)) }

    /*
     * Function that will add validation to fields.
     * 1st param: Element that validation is to be added to.
     * 2nd param: Function with validaion from above.
     * 3rd param: JSON to be updated if value is correct.
     */
    inputCheck(contactNameInput.children[1], nameValidation, fields.name.value)
    inputCheck(contactEmailInput.children[1], emailValidation, fields.email.value)
    inputCheck(contactVerifyEmailInput.children[1], emailVerifyValidation, fields.email.value)
    inputCheck(contactRoleInput.children[1], roleValidation, fields.role.value)

    footerCancelBtn.addEventListener('click', function () {
      self.close()
      refreshDom()
    })

    footerAddBtn.addEventListener('click', () => {
      updateFieldValues()
      addNewClient()
      refreshDom()
    })

    return superContainer
  }

  /**
   * checking the checkbox and input fields validation and enable / disable the continue button.
   */
  this.checkInputValidation = function () {
    const continueBtn = document.getElementById('m-ac-addBtn')
    const keyContactName = document.getElementById(fields.name.id)
    const contactEmailInput = document.getElementById(fields.email.id)
    const contactEmailInputCheck = document.getElementById(fields.emailVerify.id)
    const keyContactRole = document.getElementById(fields.role.id)

    continueBtn.disabled = false

    if (keyContactName.value === '' || keyContactName.classList.contains('auv-validationFailed') || keyContactName.classList.contains('auv-error')) {
      continueBtn.disabled = true
    }
    if (contactEmailInput.value === '' || contactEmailInputCheck.classList.contains('auv-validationFailed') || contactEmailInputCheck.classList.contains('auv-error')) {
      continueBtn.disabled = true
    }
    if (contactEmailInputCheck.value !== contactEmailInput.value || contactEmailInputCheck.classList.contains('auv-validationFailed') || contactEmailInputCheck.classList.contains('auv-error')) {
      continueBtn.disabled = true
    }
    if (keyContactRole.value === '' || keyContactRole.classList.contains('auv-validationFailed') || keyContactRole.classList.contains('auv-error')) {
      continueBtn.disabled = true
    }
  }

  const refreshDom = () => {
    var modelContent = document.getElementById(`modal-content-${this._uniqueID}`)
    if (modelContent) {
      modelContent.scrollTop = 0
    }
    self.close()
    $('#add-client-super-container').replaceWith(this.buildDom())
  }

  var updateFieldValues = function () {
    fields.name.value = document.getElementById(fields.name.id).value
    var nameField = document.getElementById(fields.name.id)
    if (nameField.value.indexOf(' ') > 0) {
      var twoNames = nameField.value.split(' ')
      fields.firstName.value = twoNames[0]
      fields.lastName.value = twoNames[1]
    } else {
      fields.firstName.value = nameField.value
      fields.lastName.value = ''
    }

    fields.email.value = document.getElementById(fields.email.id).value
    fields.role.value = document.getElementById(fields.role.id).value
  }

  const addNewClient = function () {
    const user = {
      firstName: fields.firstName.value,
      lastName: fields.lastName.value,
      email: fields.email.value,
      jobTitle: fields.role.value
    }

    c.selectClient('ADD', c.currentEngagement._id, null, user)
  }

  initialize()
}

Modal_AddNewClient.prototype = ModalTemplate.prototype
Modal_AddNewClient.prototype.constructor = Modal_AddNewClient

module.exports = Modal_AddNewClient
