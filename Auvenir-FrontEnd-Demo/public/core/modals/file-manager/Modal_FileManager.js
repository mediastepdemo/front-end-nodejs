'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import Component_FileManager from '../../components/file-manager/file-manager'
import styles from './Modal_FileManager.css'

/**
 * The modal is responsible for deleting the request for that user.
 * It is modal that prompts the user about deletion of request.
 * @class Modal_FileManager
 * @param: {Object} ctrl     - appController for the current user
 * @param: {Object} settings - settings of the current user
 */
var Modal_FileManager = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  var self = this
  var c = ctrl
  var text = c.lang
  var fmComponent = null
  var requestID = null
  var todoID = null

  /**
   * This function is responsible for building the page only when called
   * @memberof Modal_FileManager
   */
  this.buildDom = function () {
    var container = Forge.build({ dom: 'div', id: 'm-fm-modal-container' })
    var tableDiv = Forge.build({ dom: 'div', id: 'm-fm-table-div', class: 'm-fm-table-div' })
    var buttonGroup = Forge.build({ 'dom': 'div', 'id': 'm-fm-buttonGroup', class: 'm-fm-buttonGroup' })
    var cancelBtn = Forge.buildBtn({ 'text': text.cancel, 'type': 'light', 'id': 'm-fm-cancelBtn', width: '128px', height: '40px', 'margin': 'auto' }).obj
    var addBtn = Forge.buildBtn({ 'text': text.add, 'type': 'primary', 'id': 'm-fm-addBtn', width: '128px', height: '40px', 'margin': 'auto 0px auto 8px' }).obj
    buttonGroup.appendChild(cancelBtn)
    buttonGroup.appendChild(addBtn)

    cancelBtn.addEventListener('click', function (event) {
      event.preventDefault()
      self.close()
    })

    addBtn.addEventListener('click', function () {
      fmComponent.addExistingFileToRequest(requestID, todoID)
      self.close()
    })

    container.appendChild(tableDiv)
    container.appendChild(buttonGroup)

    return container
  }

  self.setModalName('File Manager')
  self.setClose({ window: true, global: false })
  self.setWidth('970px')
  self.setHeight('925px')
  self.setTitle(text.fileManagerModal.title)
  self.setHeader({ show: true, color: '#363A3C' })
  self.setPosition({ top: '0px' })
  self.build()

  /**
   * This function is responsible for loading the text with current request name.
   * @memberof Modal_FileManager
   */
  this.loadData = function (rID, tID) {
    requestID = rID
    todoID = tID
    if (!fmComponent) {
      fmComponent = new Component_FileManager(c, document.getElementById('m-fm-table-div'), this, c.currentEngagement)
    }
    fmComponent.loadData(c.currentEngagement.files, c.currentUser.type, false)
    fmComponent.on('flash', (option) => {
      // TODO - display error message on the modal.
    })
  }
}

Modal_FileManager.prototype = ModalTemplate.prototype
Modal_FileManager.prototype.constructor = Modal_FileManager

module.exports = Modal_FileManager
