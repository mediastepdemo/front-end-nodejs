'use strict'

import ModalTemplate from '../ModalTemplate'
import styles from './Modal_ArchiveEngagement.css'

/**
 * Displays an email input modal
 * @class Modal_Email
 * @param {Object} ctrl      - A reference to the current instance of the Main Application Controller (Controller.js)
 * @param {Object} settings - The custom settings that are used to generate the instance of this modal.
 */
module.exports = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)

  const yesBtnId = 'modal-archive-engagement-confirm-btn'
  const cancelBtnId = 'modal-archive-engagement-cancel-btn'
  const closeIconId = 'modal-archive-engagement-close-icon'

  const headerTextClass = 'archive-engagement-headerText'
  const bodyTextClass = 'archive-engagement-bodyText'

  let unArchivedMode = false

  this.buildDom = () => {
    return $(`<div class='archive-engagement-Container' style='margin:-15px; font-family: Lato;'>
      <div class='archive-engagement-header'>
        <div class='${headerTextClass}'>Archive Engagement?</div>
      </div>
      <div class="archive-engagement-fileContainer-icon">
        <img src='images/illustrations/illustration-archive.svg' />
      </div>
      <div class='${bodyTextClass}'>Are you sure you're ready to archive your engagement?</div>
      <button class='btn archive-engagement-cancel-btn' id='${cancelBtnId}'>Cancel</button>
      <button class='btn archive-engagement-yes-btn' id='${yesBtnId}'>Archive</button>
    </div>`)[0]
  }

  this.setupListeners = () => {
    $(`#${yesBtnId}`).on('click', () => {
      ctrl.updateEngagement({
        engagementID: ctrl.currentEngagement._id,
        status: unArchivedMode ? 'UNARCHIVED' : 'ARCHIVED'
      })
      this.trigger('done', {
        engagementID: ctrl.currentEngagement._id,
        page: 'ENGAGEMENT'
      })
      this.close()
    })
    $(`#${cancelBtnId}`).on('click', () => {
      this.close()
    })
    $(`#${closeIconId}`).on('click', () => {
      this.close()
    })
  }

  this.refresh = (unArchived) => {
    const header = $(`.${headerTextClass}`)
    const body = $(`.${bodyTextClass}`)
    const yesBtn = $(`#${yesBtnId}`)
    if (unArchived) {
      yesBtn.text('Unarchive')
      header.text('UnArchive Engagement?')
      body.text('You will able to access this portal')
      unArchivedMode = true
    } else {
      yesBtn.text('Archive')
      header.text('Archive Engagement?')
      body.text('Are you sure you\'re ready to archive your engagement?')
      unArchivedMode = false
    }
  }

  this.setModalName('archiveEngagement')
  this.setClose({'window': true, 'global': true})
  this.setWidth('483px')
  this.setHeight('534px')
  this.setPosition({'top': '22%'})
  this.build()
}
