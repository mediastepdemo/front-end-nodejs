'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import Utility from '../../../js/src/utility_c'
import log from '../../../js/src/log'
import styles from './Modal_InviteNewMember.css'

var Modal_InviteNewMember = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  var c = ctrl
  var self = this
  var text = c.lang.inviteMemberModal

  /* Pull in the engagement title */

  // var permissionList = [ {text: c.lang.admin}, {text: c.lang.lead}, {text: c.lang.general} ]

  /*
   * Local instances of the field values
   */
  var fields = {
    name: { id: 'm-inm-name', type: 'text', value: ''},
    firstName: { id: 'm-inm-firstName', type: 'text', value: ''},
    lastName: { id: 'm-inm-lastName', type: 'text', value: ''},
    email: { id: 'm-inm-email', type: 'text', value: ''},
    jobTitle: { id: 'm-inm-jobTitle', type: 'text', value: ''}
    // permission: { id: 'm-inm-permission', type: 'text', value: ''}
  }

  /**
   * This function is responsible for initializing the creation of a new Client
   * @memberof Modal_InviteNewMember
   */
  var initialize = function () {
    log('Modal_InviteNewMember: initialize')
    self.setModalName('engagementModel')
    self.setClose({'window': true, 'image': 'images/icons/x-small-white.svg'})
    self.setFullScreen(true)
    self.setHeader({'show': false})
    self.setPosition({ 'bottom': '0px'})
    self.setPadding('0px')
    self.build()
    self.setupListeners()
  }

  this.setTitle = function (engagementName) {
    document.getElementById('m-inm-engagementName').innerHTML = engagementName
  }

  /**
   * This function is responsible for building the page only when called
   * @memberof Modal_InviteNewMember
   */
  this.buildDom = function () {
    log('Building CreateEnagement Modal')

    var parentContainer = Forge.build({'dom': 'div', 'id': 'inviteMemberParent', class: 'inm-parent'})
    var header = Forge.build({'dom': 'div', 'class': 'inm-header'})
    var headerTitle = Forge.build({'dom': 'h2', id: 'm-inm-engagementName', 'class': 'inm-headerTitle'})
    header.appendChild(headerTitle)

    var body = Forge.build({'dom': 'div', 'class': 'inm-creationContainer'})
    var subTitle = Forge.build({dom: 'h3', text: text.subTitle, class: 'inm-subTitle'})
    var nameInput = Forge.buildInput({'label': text.enterName, 'errorText': text.fullNameError, 'id': fields.name.id, 'placeholder': ''})
    var emailInput = Forge.buildInput({'label': text.emailLabel, 'errorText': text.emailError, 'id': fields.email.id, 'placeholder': '', 'maxlength': '100'})
    var reEmailInput = Forge.buildInput({'label': text.reEmailLabel, 'errorText': text.reEmailError, id: 'm-inm-reEmail', 'placeholder': '', 'maxlength': '100'})
    var roleInput = Forge.buildInput({'label': text.role, 'errorText': text.roleError, 'id': fields.jobTitle.id, 'placeholder': ''})
    // var selectPermissionInput = Forge.buildInputDropdown({'label': text.permission, 'id': fields.permission.id, 'list': permissionList, 'placeholder': '', width: '349px'})
    body.appendChild(subTitle)
    body.appendChild(nameInput)
    body.appendChild(emailInput)
    body.appendChild(reEmailInput)
    body.appendChild(roleInput)
    // body.appendChild(selectPermissionInput)

    var footer = Forge.build({'dom': 'div', 'class': 'inm-footer'})
    var footerBtnHolder = Forge.build({'dom': 'div', 'class': 'inm-footerBtnHolder'})
    var footerCancelBtn = Forge.buildBtn({
      'text': c.lang.cancel,
      'id': 'm-inm-cancelBtn',
      'type': 'secondary',
      'margin': '22px 0px'
    }).obj
    var footerInviteBtn = Forge.buildBtn({
      'text': c.lang.invite,
      'id': 'm-inm-addBtn',
      'type': 'primary',
      'margin': '22px 0px 22px 11px'
    }).obj
    footerInviteBtn.disabled = true

    footerInviteBtn.addEventListener('click', function () {
      // Below is an example of the parameters used for calling sendTeamMemberInvite
      let data = {
        firstName: fields.firstName.value,
        lastName: fields.lastName.value,
        email: fields.email.value,
        jobTitle: fields.jobTitle.value}

      c.sendTeamMemberInvite(c.currentEngagement._id, data)
      self.close()
      self.reset()
    })

    footerCancelBtn.addEventListener('click', function () {
      self.close()
      self.reset()
    })

    var checkInputValidation = function () {
      var checkName = document.getElementById(fields.name.id)
      var checkEmail = document.getElementById(fields.email.id)
      var checkRole = document.getElementById(fields.jobTitle.id)
      // check for input fields validation
      if (checkName.classList.contains('auv-error') || checkName.classList.contains('auv-validationFailed') || checkName.value === '') {
        footerInviteBtn.disabled = true
      } else if (checkEmail.classList.contains('auv-error') || checkEmail.classList.contains('auv-validationFailed') || checkEmail.value === '') {
        footerInviteBtn.disabled = true
      } else if (checkRole.classList.contains('auv-error') || checkRole.classList.contains('auv-validationFailed') || checkRole.value === '') {
        footerInviteBtn.disabled = true
      } else {
        footerInviteBtn.disabled = false
      }
    }

    var timeName = null
    nameInput.children[1].onblur = nameInput.children[1].onkeydown = function () {
      clearTimeout(timeName)
      var context = this
      timeName = setTimeout(function () {
        fields.name.value = nameInput.children[1].value
        if (fields.name.value.trim() === '') {
          context.classList.add('auv-error')
        } else {
          context.classList.remove('auv-error')
        }
        checkInputValidation()
      }, 100)
    }

    // Everytime it's out of focus, it will check that field's value and update DOM's value.
    emailInput.children[1].addEventListener('blur', function () {
      var emailField = document.getElementById(fields.email.id)
      if (Utility.REGEX.email.test(emailField.value)) {
        emailField.classList.remove('auv-error')
      } else {
        emailField.classList.add('auv-error')
      }
      checkInputValidation()
    })

    reEmailInput.children[1].addEventListener('blur', function () {
      var emailField = document.getElementById(fields.email.id)
      var reEmailField = document.getElementById('m-inm-reEmail')
      if (emailField.value === reEmailField.value) {
        reEmailField.classList.remove('auv-error')
        fields.email.value = emailField.value
      } else {
        reEmailField.classList.add('auv-error')
      }
      checkInputValidation()
    })

    var timeRole = null
    roleInput.children[1].onblur = roleInput.children[1].onkeydown = function () {
      clearTimeout(timeRole)
      var context = this
      timeRole = setTimeout(function () {
        fields.jobTitle.value = roleInput.children[1].value
        if (fields.jobTitle.value.trim() === '' || !Utility.REGEX.alphanumericName.test(fields.jobTitle.value)) {
          context.classList.add('auv-error')
        } else {
          context.classList.remove('auv-error')
        }
        checkInputValidation()
      }, 100)
    }

    // selectPermissionInput.children[1].addEventListener('blur', function () {
    //   var permissionField = document.getElementById(fields.permission.id)
    //   if (Utility.REGEX.name.test(permissionField.value)) {
    //     selectPermissionInput.children[1].classList.remove('auv-validationFailed')
    //     permissionField.classList.remove('auv-error')
    //   } else {
    //     selectPermissionInput.children[1].classList.add('auv-validationFailed')
    //   }
    //   checkInputValidation()
    // })

    footerBtnHolder.appendChild(footerCancelBtn)
    footerBtnHolder.appendChild(footerInviteBtn)
    footer.appendChild(footerBtnHolder)

    parentContainer.appendChild(header)
    parentContainer.appendChild(body)
    parentContainer.appendChild(footer)

    return parentContainer
  }

  this.setupListeners = function () {

  }

  this.reset = function () {
    $('#inviteMemberParent :input').val('')
  }

  initialize()
}

Modal_InviteNewMember.prototype = ModalTemplate.prototype
Modal_InviteNewMember.prototype.constructor = Modal_InviteNewMember

module.exports = Modal_InviteNewMember
