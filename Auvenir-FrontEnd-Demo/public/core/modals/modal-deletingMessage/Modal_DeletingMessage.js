'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import styles from './Modal_DeletingMessage.css'

/**
 * Displays an email input modal
 * @class Modal_Email
 *
 * @param {Object} ctrl      - A reference to the current instance of the Main Application Controller (Controller.js)
 * @param {Object} settings - The custom settings that are used to generate the instance of this modal.
 */
var Modal_DeletingMessage = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  // ModalTemplate.setSettings(settings);
  var counter = 0
  var length = 0
  var lastText = ''
  var mainText = ''
  var nextText = ''

  this.configure = function (list) {
    length = list.length

    if (counter < length) {
      showMessage()
    }
    function showMessage () {
      lastText = mainText
      if (lastText !== '') {
        document.getElementById('deleting-message-select-icon').style.display = 'block'
      } else {
        document.getElementById('deleting-message-select-icon').style.display = 'none'
      }

      mainText = list[counter]
      nextText = list[counter + 1]
      if (counter > 0) {
        document.getElementById('deleting-message-bodyContainerOne').classList.toggle('message-bodyContainerOne')
        document.getElementById('deleting-message-bodyContainerThree').classList.toggle('message-bodyContainerThree')
      }

      document.getElementById('text-ContainerOne').innerHTML = lastText
      document.getElementById('text-ContainerTwo').innerHTML = mainText
      if ((counter + 1) !== length) {
        document.getElementById('text-ContainerThree').innerHTML = nextText
      } else {
        document.getElementById('text-ContainerThree').innerHTML = ''
      }

      counter++
      if (counter < length) {
        setTimeout(function () {
          showMessage()
        }, 3000)
      }
    }
  }

  this.buildDom = function () {
    var firstContent = Forge.build({'dom': 'div', 'class': 'deleting-message-bodyContainerOne', 'id': 'deleting-message-bodyContainerOne'})
    var selectIcon = Forge.build({
      'dom': 'img',
      'class': 'deleting-message-select-icon',
      'id': 'deleting-message-select-icon',
      'src': 'images/icons/tick.png',
      'style': 'float:left'

    })

    var contentBodyOne = Forge.build({
      'dom': 'div',
      'class': 'deleting-message-bodyTextOne',
      'id': 'text-ContainerOne',
      'text': ''

    })
    firstContent.appendChild(selectIcon)
    firstContent.appendChild(contentBodyOne)
    var secondContent = Forge.build({'dom': 'div', 'class': 'deleting-message-bodyContainerTwo', 'id': 'deleting-message-bodyContainerTwo'})
    var selectIconTwo = Forge.build({
      'dom': 'img',
      'class': 'deleting-message-select-iconTwo',
      'src': 'images/icons/tick.png',
      'style': 'float:left'

    })

    var contentBodyTwo = Forge.build({
      'dom': 'div',
      'class': 'deleting-message-bodyTextTwo',
      'id': 'text-ContainerTwo',
      'text': ''

    })
    secondContent.appendChild(selectIconTwo)
    secondContent.appendChild(contentBodyTwo)

    var thirdContent = Forge.build({'dom': 'div', 'class': 'deleting-message-bodyContainerThree', 'id': 'deleting-message-bodyContainerThree'})

    var contentBodyThree = Forge.build({
      'dom': 'div',
      'class': 'deleting-message-bodyTextThree',
      'id': 'text-ContainerThree',
      'text': ''

    })

    thirdContent.appendChild(contentBodyThree)
    var deletingMessageContainer = Forge.build({dom: 'div', class: 'deleting-message-Container', style: 'margin:-15px; font-family: Lato;'})
    deletingMessageContainer.appendChild(firstContent)
    deletingMessageContainer.appendChild(secondContent)
    deletingMessageContainer.appendChild(thirdContent)
    return deletingMessageContainer
  }

  this.setModalName('deletingMessage')
  this.setClose({'window': true})
  this.setFullScreen(true)
  this.setHeader({'show': false})
  this.setPosition({ 'bottom': '0px'})
  this.build()
}

module.exports = Modal_DeletingMessage
