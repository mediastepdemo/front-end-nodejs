'use strict'

import Utility from '../../js/src/utility_c'
import log from '../../js/src/log'
import Forge from '../Forge'
import styles from './ModalTemplate.css'
/**
 * The Modal Template is responsible for generating all modals
 * @class ModalTemplate
 *
 * @param: {Object} ctrl     - appController for the current user
 * @param: {Object} settings - settings for the current user
 */
var ModalTemplate = function (ctrl, settings) {
  var self = this
  var c = ctrl

  this._modalID = ''
  this.contentID = ''
  var myModalBodyID = ''
  var myModalDialogID = ''
  var modalBackdropID = ''
  var myModalContentID = ''
  var titleContainerDomID = ''
  var titleDomID = ''

  // Create a unique ID for a particular modal
  do {
    this._uniqueID = Utility.randomString(32, Utility.ALPHANUMERIC)
  } while (document.getElementById('modal-header-' + this._uniqueID))

  myModalDialogID = 'modal-container-' + this._uniqueID
  myModalBodyID = 'modal-body-' + this._uniqueID
  myModalContentID = 'modal-content-' + this._uniqueID
  modalBackdropID = 'modal-backdrop-' + this._uniqueID

  var isOpen = false
  var currentSettings = {
    modalName: '',
    rounded: false,
    close: {
      window: false,
      global: false,
      keystroke: null,
      image: 'images/icons/x-large.svg'
    },
    height: 'auto',
    // width: 'auto',
    title: null,
    background: '#ffffff',
    header: {
      background: '#F6F6F6',
      show: false,
      color: '#858585',
      icon: ''
    },
    position: {
      top: '0px',
      bottom: '0px',
      left: '0px',
      right: '0px'
    },
    footer: {
      show: false
    },
    fullscreen: false,
    zIndex: null,
    overflow: null,
    padding: null
  }

  // Event Object
  this.evts = {}

  /**
   * Sets up a listener on any specific tag
   * @param {String}   tag - The name of the event
   * @param {Function} cb  - The callback function
   */
  this.on = function (tag, cb) {
    if (typeof tag !== 'string' || typeof cb !== 'function') {
      return
    }

    if (!self.evts[tag]) {
      self.evts[tag] = cb
    } else {
      self.evts[tag] = cb
    }
  }

  /**
   * Trigger all Listening Objects Callbacks
   * @param {String} - The tag being triggered
   * @param {Object} - The event that occured (if available)
   * @param {Object} - The custom event data
   */
  this.trigger = function (tag, evt, data) {
    if (this.evts[tag]) {
      this.evts[tag](evt, data)
    }
  }

  /**
   * This function is responsible for setting the modal Name as is passed in settings.
   * @memberof ModalTemplate
   * @param: {string} m - modal name
   */
  var setModalName = this.setModalName = function (name) {
    if (name !== undefined && name !== null) {
      if (typeof name === 'string') {
        currentSettings.modalName = name
        self._modalID = currentSettings.modalName + '-' + self._uniqueID
      }
    }
  }

  /**
   * This function is responsible for setting the flag for closing the modal as is passed in settings.
   * @memberof ModalTemplate
   * @param: {Object} close - key-value pairs for ways to close modal
   */
  var setClose = this.setClose = function (close) {
    var isSet = false
    if (close !== undefined && close !== null) {
      if (close['window'] !== undefined && (typeof close['window'] === 'boolean')) {
        currentSettings.close.window = close['window']
        isSet = true
      }
      if (close['global'] !== undefined && (typeof close['global'] === 'boolean')) {
        currentSettings.close.global = close['global']
        isSet = true
      }
      if (close['keystroke'] && (typeof close['keystroke'] === 'number')) {
        currentSettings.close.keystroke = close['keystroke']
        isSet = true
      }
      if (close['image']) {
        currentSettings.close.image = close['image']
        isSet = true
      }
    }
  }

  /**
   * This function is responsible for setting the height of the modal as is passed in settings.
   * @memberof ModalTemplate
   * @param: {number} height - height of the modal
   */
  var setHeight = this.setHeight = function (height) {
    if (height) {
      if (typeof height === 'string') {
        currentSettings.height = height
      }
    }
  }

  /**
   * This function is responsible for setting the width of the modal as is passed in settings.
   * @memberof ModalTemplate
   * @param: {number} m - width of the modal
   */
  var setWidth = this.setWidth = function (width) {
    if (width) {
      if (typeof width === 'string') {
        currentSettings.width = width
      }
    }
  }

  /**
   * This function is responsible for setting the z-index of the modal as is passed in settings.
   * @memberof ModalTemplate
   * @param: {number} m - z-index of the modal
   */
  var setZIndex = this.setZIndex = function (zIndex) {
    if (zIndex) {
      if (typeof zIndex === 'string') {
        currentSettings.zIndex = zIndex
      }
    }
  }

  /**
   * This function is responsible for setting the overflow of the modal as is passed in settings.
   * @memberof ModalTemplate
   * @param: {string} m - overflow of the modal
   */
  var setOverflow = this.setOverflow = function (overflow) {
    if (overflow) {
      if (typeof overflow === 'string') {
        currentSettings.overflow = overflow
      }
    }
  }

  /**
   * This function is responsible for setting the padding of the modal as is passed in settings.
   * @memberof ModalTemplate
   * @param: {string} m - padding of the modal
   */
  var setPadding = this.setPadding = function (padding) {
    if (padding) {
      if (typeof padding === 'string') {
        currentSettings.padding = padding
      }
    }
  }

  /**
   * This function is responsible for setting the title of the modal as is passed in settings.
   * @memberof ModalTemplate
   * @param: {string} title - title of the modal
   */
  var setTitle = this.setTitle = function (title) {
    if (title !== undefined && title !== null) {
      if (typeof title === 'string') {
        currentSettings.title = title
        if (document.getElementById('m-' + self._modalID + '-title-text')) {
          document.getElementById('m-' + self._modalID + '-title-text').innerHTML = title
        }
      }
    }
  }

  /**
   * This function is responsible for setting the flag for showing the header of the modal as is passed in settings.
   * @memberof ModalTemplate
   * @param: {Object} header - header properties of the modal
   */
  var setHeader = this.setHeader = function (header) {
    var isSet = false
    if (header !== undefined && header !== null) {
      if (('background' in header) && (typeof header['background'] === 'string')) {
        currentSettings.header.background = header['background']
        if (document.getElementById('modal-header-' + self._uniqueID)) {
          document.getElementById('modal-header-' + self._uniqueID).style.background = header['background']
        }
        isSet = true
      }
      if (('show' in header) && (typeof header['show'] === 'boolean')) {
        currentSettings.header.show = header['show']
        isSet = true
      }
      if (('color' in header) && (typeof header['color'] === 'string')) {
        currentSettings.header.color = header['color']
        isSet = true
      }
      if (('icon' in header) && (typeof header['icon'] === 'string')) {
        // path to image source file
        currentSettings.header.icon = header['icon']
        isSet = true
      }
    }
  }

  /**
   * This function is responsible for positioning the modal
   * @memberof ModalTemplate
   * @param: {Object} pos - positioning of the modal
   */
  var setPosition = this.setPosition = function (pos) {
    var isSet = false
    if (pos !== undefined && pos !== null) {
      if (('top' in pos) && (pos['top'])) {
        currentSettings.position.top = pos['top']
        isSet = true
      }
      if (('bottom' in pos) && (pos['bottom'])) {
        currentSettings.position.bottom = pos['bottom']
        isSet = true
      }
      if (('left' in pos) && (pos['left'])) {
        currentSettings.position.left = pos['left']
        isSet = true
      }
      if (('right' in pos) && (pos['right'])) {
        currentSettings.position.right = pos['right']
        isSet = true
      }
    }
  }

  /**
   * This function is responsible for setting the flag for footer display preferences of the modal
   * @memberof ModalTemplate
   * @param: {boolean} footer - footer flag of the modal
   */
  var setFooter = this.setFooter = function (footer) {
    if (footer !== undefined && footer !== null) {
      if (typeof footer === 'boolean') {
        currentSettings.footer.show = footer
      }
    }
  }

  /**
   * This function is responsible for setting the flag for fullScreen display of the modal as is passed in settings.
   * @memberof ModalTemplate
   * @param: {boolean} fs - fullscreen flag of the modal
   */
  var setFullScreen = this.setFullScreen = function (fs) {
    if (fs !== undefined && fs !== null) {
      if (typeof fs === 'boolean') {
        currentSettings.fullscreen = fs
      }
    }
  }

  /**
   * This function is responsible for setting the appropriate close-icon as per the size of the modal
   * @memberof ModalTemplate
   */
  var setIcon = this.setIcon = function () {
    var d = document.getElementById('modal-close-' + self._modalID)
    if (d) {
      d.setAttribute('src', 'images/icons/x-large-white.svg')
    }
  }

  /**
   * This function is responsible for setting the background of the modal
   * @memberof ModalTemplate
   * @param: {string} clr - color of the background of the modal
   */
  var setBackground = this.setBackground = function (clr) {
    if (clr !== undefined && clr !== null) {
      if (typeof clr === 'string') {
        currentSettings.background = clr
        var elem = document.getElementById(myModalDialogID)

        if (elem) {
          elem.style.background = clr
        }
      }
    }
  }

  this.setSettings = function (settings) {
    setModalName(settings.modalName)
    setClose(settings.close)
    setWidth(settings.width)
    setHeight(settings.height)
    setHeader(settings.header)
    setPosition(settings.position)
    setBackground(settings.background)
    setFooter(settings.footer)
    setFullScreen(settings.fullscreen)
    setTitle(settings.title)
    setZIndex(settings.zIndex)
    setOverflow(settings.overflow)
    setPadding(settings.padding)
  }

  /**
   * This function is responsible for setting the modal Name as is passed in settings.
   * @memberof ModalTemplate
   * @param: {string} m - modal name
   */
  this.build = function () {
    var containerType = 'au-modal-container'
    var styleStr = 'top:' + currentSettings.position.top + '; ' + 'right:' + currentSettings.position.right + '; ' + 'left:' + currentSettings.position.left + '; ' + 'bottom:' + currentSettings.position.bottom + '; '
    var myModal = Forge.build({'dom': 'div', 'id': self._modalID, 'class': 'au-modal', 'style': styleStr})
    var myModalDialog = Forge.build({'dom': 'div', 'class': containerType, 'id': myModalDialogID, 'style': 'width:' + currentSettings.width + ';' + 'height:' + currentSettings.height + ';background:' + currentSettings.background})
    var myModalContent = Forge.build({'dom': 'div', 'id': myModalContentID, 'class': 'au-modal-content' })

    if (currentSettings.fullscreen) {
      if (currentSettings.zIndex) {
        myModal.style.cssText = 'z-index:' + currentSettings.zIndex + '; '
      }
      containerType = 'au-modal-container au-modal-fullScreen'
      myModalDialog.className = containerType
      myModalDialog.style.cssText = 'width:' + currentSettings.width + ';background:' + currentSettings.background
    }

    var myModalBody = Forge.build({'dom': 'div', 'class': 'au-modal-body', 'id': myModalBodyID})

    var closeId = 'modal-close-' + self._modalID
    var myModalCloseButton = Forge.build({'dom': 'img', 'style': 'fill:red', 'id': closeId, 'src': 'images/icons/x-small.svg', 'class': 'au-modal-closeBtn'})
    var myModalFooter

    $(document).on('keydown', function (e) {
      var code = e.keyCode ? e.keyCode : e.which
      if (code === 27) {
        self.close()
      }
    })

    if (currentSettings.fullscreen) {
      if (currentSettings.overflow) {
        myModalContent.style.cssText = 'overflow:' + currentSettings.overflow + '; height: 100%'
      } else {
        myModalContent.setAttribute('style', 'height:100%')
      }
      myModalBody.setAttribute('style', 'height:100%')
      myModalCloseButton.setAttribute('style', 'top:32px; right:32px')
      myModalCloseButton.setAttribute('src', currentSettings.close.image)
    }

    if (currentSettings.padding) {
      myModalBody.style.cssText = 'padding:' + currentSettings.padding + '; height: 100%'
    }

    if (currentSettings.rounded) {
      myModalDialog.classList.add('rounded')
    }

    if (currentSettings.header.show) {
      var background = (currentSettings.header.background) ? currentSettings.header.background : '#f3f3f3'
      var myModalHeader = Forge.build({'dom': 'div', 'class': 'au-modal-header', 'style': 'background:' + background, 'text': currentSettings.header.title, 'id': 'modal-header-' + self._uniqueID})
      myModalDialog.appendChild(myModalHeader)
      myModalBody.classList.add('down')
    }

    if (currentSettings.title !== null) {
      var title = currentSettings.title
      var titleContainerDom = Forge.build({'dom': 'div', 'id': 'm-' + self._modalID + '-title', 'class': 'au-modal-title'})

      var titleDom = Forge.build({'dom': 'label', 'id': 'm-' + self._modalID + '-title-text', 'text': title, 'class': 'au-modal-title-text', 'style': 'color:' + currentSettings.header.color + ';'})

      if (currentSettings.header.icon !== '') {
        switch (currentSettings.header.icon) {
          case 'warning':
            var iconDom = Forge.build({'dom': 'img', 'id': 'm-' + self._modalID + '-icon', 'src': 'images/icons/warning.svg', 'class': 'au-modal-title-icon'})
            titleContainerDom.appendChild(iconDom)
            break
          default:
        }
      }
      titleContainerDom.appendChild(titleDom)
      myModalDialog.appendChild(titleContainerDom)
    }

    if (currentSettings.close.window) {
      myModalDialog.appendChild(myModalCloseButton)
    }

    myModalContent.appendChild(myModalBody)

    var bodyDom = this.buildDom()
    if (bodyDom) {
      myModalBody.appendChild(bodyDom)
    }

    if (currentSettings.footer.show) {
      myModalFooter = Forge.build({'dom': 'div', 'class': 'modal-footer', 'id': 'modal-footer-' + self._uniqueID })
      myModalContent.appendChild(myModalFooter)
    }

    myModalDialog.appendChild(myModalContent)
    myModal.appendChild(myModalDialog)

    document.body.appendChild(myModal)
    if (currentSettings.close.window) {
      document.getElementById(closeId).addEventListener('click', () => {
        if (self.onClose) {
          self.onClose()
        }
        self.close()
      })
    }

    if (currentSettings.close.global) {
      document.getElementById(self._modalID).addEventListener('click', function (evt) {
        if (!$.contains($(`#${myModalContentID}`)[0], evt.target)) {
          self.close()
        }
      })
    }
  }

   /**
    * Interface function that needs to be implemented by each Modal.
    */
  this.buildDom = function () {
    return null
  }

    /**
     * This function is responsible for creating animations when the modal is opened
     * @memberof ModalTemplate
     */
  var openAnimation = function () {
    if (currentSettings.fullscreen) {
       // add classes for fullscreen
      document.getElementById(myModalDialogID).classList.add('modalTransition-fp-container')
      document.getElementById(myModalContentID).classList.add('modalTransition-fp-sub')

       // Run transition for fullscreen
      setTimeout(function () {
        document.getElementById(myModalDialogID).style.opacity = '1'
        document.getElementById(myModalContentID).style.opacity = '1'
        document.getElementById(myModalContentID).style.transform = 'rotateX(0deg)'
        document.getElementById(myModalContentID).style.webkitTransform = 'rotateX(0deg)'
      }, 100)

      if (currentSettings.overflow === 'auto' || currentSettings.overflow === 'hidden') {
        document.body.style.overflowY = 'hidden'
      }
    } else if (!currentSettings.fullscreen) {
       // Reset classes for pop-up (runs animation in css)
      document.getElementById(myModalDialogID).classList.add('modalTransition-popUp-container')

       // Build backdrop
      var fade = Forge.build({'dom': 'div', 'id': modalBackdropID, 'class': ''})
      document.body.appendChild(fade)
      document.body.classList.add('au-modal-display')

       // Run transition for backdrop
      setTimeout(function () {
        document.getElementById(modalBackdropID).classList.add('au-modal-backdrop')
        document.getElementById(modalBackdropID).classList.add('au-modal-backdrop-display')
      }, 100)
    }
  }

   /**
    * This function is responsible for creating animations when the modal is closed
    * @memberof ModalTemplate
    */
  var closeAnimation = function () {
    if (currentSettings.fullscreen) {
       // Run transition for fullscreen

      document.getElementById(myModalDialogID).classList.add('modalTransition-fp-containerOut')
      document.getElementById(myModalContentID).classList.add('modalTransition-fp-subOut')
      document.getElementById(myModalContentID).style.transform = 'translateY(25%) rotateX(-35deg)'
      document.getElementById(myModalContentID).style.webkitTransform = 'translateY(25%) rotateX(-35deg)'
      document.getElementById(myModalDialogID).style.opacity = '0'
      document.getElementById(myModalContentID).style.opacity = '0.4'

       // Reset properties and classes
      setTimeout(function () {
        document.getElementById(myModalContentID).style.transform = 'translateY(-50%) rotateX(70deg)'
        document.getElementById(myModalContentID).style.webkitTransform = 'translateY(-50%) rotateX(70deg)'
        document.getElementById(myModalDialogID).classList.remove('modalTransition-fp-container')
        document.getElementById(myModalContentID).classList.remove('modalTransition-fp-sub')
        document.getElementById(self._modalID).style.display = 'none'
        document.getElementById(self._modalID).classList.remove('au-display')
        document.getElementById(myModalDialogID).classList.remove('modalTransition-fp-containerOut')
        document.getElementById(myModalContentID).classList.remove('modalTransition-fp-subOut')

        self.removeListeners()
      }, 500)

      if (currentSettings.overflow === 'auto' || currentSettings.overflow === 'hidden') {
        document.body.style.overflowY = 'auto'
      }
    } else if (!currentSettings.fullscreen) {
       // Run animation for pop-up
      document.getElementById(myModalDialogID).classList.remove('modalTransition-popUp-container')
      document.getElementById(myModalDialogID).classList.add('modalTransition-popUp-containerOut')
      document.getElementById(modalBackdropID).classList.add('au-modal-backdrop-out')
      document.getElementById(modalBackdropID).style.opacity = '0'

       // Reset properties and classes
      setTimeout(function () {
        document.getElementById(modalBackdropID).classList.remove('.au-modal-backdrop-out')
        document.getElementById(myModalDialogID).classList.remove('modalTransition-popUp-containerOut')
        document.getElementById(modalBackdropID).remove()
        document.getElementById(self._modalID).style.display = 'none'
        document.getElementById(self._modalID).classList.remove('au-display')
        document.body.classList.remove('au-modal-display')
        self.removeListeners()
      }, 500)
    }
  }

   /**
    * Will check to see if the modal is closed, and if it is it will open it, as
    * well as create the required DOM element for the backdrop.
    * @memberof ModalTemplate
    */
  this.open = function () {
    log('Showing Modal: ' + self._modalID)

    if (!isOpen) {
      if (!document.getElementById(self._modalID).classList.contains('display')) {
        document.getElementById(self._modalID).style.display = 'block'
        document.getElementById(self._modalID).classList.add('au-display')
        openAnimation()
        $('#' + myModalContentID).scrollTop(0)
        $('body').css('overflow', 'hidden')

         // click out of modal to close
        if (currentSettings.close.window) {
          if (!currentSettings.fullscreen && currentSettings.modalName !== 'skipSecurity' && currentSettings.modalName !== 'viewDevice') {
            $('#' + self._modalID).click(self.close)
          }

          $('#' + myModalDialogID).click(function (e) {
            e.stopPropagation()
          })
        }
        isOpen = true
        self.addListeners()
      }
    }
  }

   /**
    * If the modal is open, it will be closed, and the associated backdrop removed.
    * @memberof ModalTemplate
    */
  this.close = function () {
    log('Closing Modal: ' + self._modalID)
    if (isOpen) {
      closeAnimation()
      $('body').css('overflow', 'inherit')

      isOpen = false
    }
  }

   /**
    * Returns true/false flag on wether the modal is open or not
    * @memberof ModalTemplate
    */
  this.isOpen = function () {
    return isOpen
  }

  this.addListeners = function () {}
  this.removeListeners = function () {}
}

module.exports = ModalTemplate
