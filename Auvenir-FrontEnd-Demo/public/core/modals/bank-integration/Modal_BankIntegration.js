'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import styles from './Modal_BankIntegration.css'
/**
 * The modal is responsible for integrating with bank and finicity.
 * @class Modal_BankIntegration
 * @param: {Object} ctrl     - appController for the current user
 * @param: {Object} settings - settings of the current user
 */
var Modal_BankIntegration = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  var self = this
  var c = ctrl
  var text = c.lang.bankIntegrationModal
  var content = ``
  var accountsContent = ``
  var currentSessionID = ''

  var data = {
    bankName: 'RBC Royal Bank',
    bankAccounts: [
      'Chequing Account 19023, $458.00',
      'Savings Account 19024, $1458.00'
    ],
    success: true,
    securityQuestion: 'What is your father\'s middle name?'
  }

  var currentSection = 'm-bi-connectingDiv'
  var previousSections = []
  var currentButton = 'm-bi-buttons'
  var previousButtons = []

  var loginInfo = []
  var accountList = []
  var bankName = ''
  var userNameObject = {}
  var passwordObject = {}

  var setHeight = function (section) {
    if (section === 'm-bi-securityQuestion') {
      $('#m-bi-modal-container').parents(':eq(2)').height('336px')
    } else {
      $('#m-bi-modal-container').parents(':eq(2)').height('534px')
    }
  }

  var viewSection = function (section) {
    setHeight(section)
    document.getElementById(currentSection).style.display = 'none'
    document.getElementById(section).style.display = 'block'
    previousSections.push(currentSection)
    currentSection = section
  }

  var viewButton = function (button) {
    if (button === 'none') {
      document.getElementById(currentButton).style.display = 'none'
    } else {
      document.getElementById(currentButton).style.display = 'none'
      document.getElementById(button).style.display = 'block'
      previousButtons.push(currentButton)
      currentButton = button
    }
  }

  var goToPrevious = function () {
    var lastSection = previousSections.length - 1
    var lastButton = previousButtons.length - 1
    document.getElementById(currentSection).style.display = 'none'
    document.getElementById(currentButton).style.display = 'none'
    if (currentSection === previousSections[lastSection]) {
      lastSection = previousSections.length - 2
      lastButton = previousButtons.length - 2
    }
    setHeight(previousSections[lastSection])
    document.getElementById(previousSections[lastSection]).style.display = 'block'
    document.getElementById(previousButtons[lastButton]).style.display = 'block'
    currentSection = previousSections[lastSection]
    currentButton = previousButtons[lastButton]
  }

  /* Sets the account balance with a dollar sign */
  var accountBalance = function (balance) {
    var balanceString = balance.toString()
    var currentBalance = ''
    if (balanceString.charAt(0) === '-') {
      currentBalance = balanceString.slice(0, 1) + '$' + balanceString.slice(1)
    } else {
      currentBalance = '$' + balanceString
    }
    return currentBalance
  }

  /* Creates the list of accounts */
  var showAccountList = function () {
    accountsContent = ''
    for (var i = 0; i < accountList.length; i++) {
      accountsContent = accountsContent + `
      <div class="ui fitted checkbox bankInt-checkDiv" style= "margin-top: 0px;">
        <input type="checkbox" id="m-bi-accountCheck-${i}" class="bankInt-checkInput" style="left: 38px !important">
        <label class="bankInt-accountLabel" style="padding-left: 60px !important">${accountList[i].type} ${accountList[i].number}, ${accountBalance(accountList[i].balance)}</label>
      </div>`
    }
    document.getElementById('m-bi-accounts').innerHTML = accountsContent
    viewSection('m-bi-accounts')
    viewButton('m-bi-accountBtnHolder')
  }

  // var connectingSection = Forge.build({dom: 'div', id: 'm-bi-connectingDiv'})
  // connectingSection.innerHTML = `
  //   <div id="m-bi-preloaderBtn" class="bankInt-preloader">
  //     <img alt="progress spinner" src="images/loading/spinner.svg" width="200px" height="200px" style="position:relative; top:-5px">
  //   </div>
  //   <p class="bankInt-connecting" id="m-bi-loaderText">${text.connecting}...</p>
  // `
  // var loginSection = Forge.build({dom: 'div', id: 'm-bi-login', class: 'bankInt-loginDiv'})

  /**
   * This function is responsible for building the page only when called
   * @memberof Modal_BankIntegration
   */
  this.buildDom = function () {
    var container = Forge.build({ dom: 'div', id: 'm-bi-modal-container', style: 'height: 404px;'})

    content = `
      <span id="m-bi-exitIcon" class="auvicon-ex bankInt-exitIcon"></span>
      <div id="m-bi-connectingDiv">
        <div class="bankInt-preloader">
          <img alt="progress spinner" src="images/loading/spinner.svg" width="200px" height="200px" style="position:relative; top:-5px">
        </div>
        <p class="bankInt-subText" id="m-bi-loaderText">${text.connecting}...</p>
      </div>

      <div id="m-bi-login" class="bankInt-loginDiv" style="display: none">
        <p id="bankInt-login-label" class="bankInt-label">${text.userName}</p>
        <div class="ui input bankInt-input">
          <input type="text" placeholder="" id="m-bi-userName">
        </div>
        <p id="bankInt-password-label" class="bankInt-label">${c.lang.password}</p>
        <div class="ui input bankInt-input" style="margin-bottom: 8px">
          <input type="password" placeholder="" id="m-bi-password">
        </div>
        <a id="m-bi-forgot-password" class="bankInt-forgot" style="display: none">${text.forgot}</a>
      </div>

      <div id="m-bi-securityQuestion" class="bankInt-securitySection" style="display: none">
        <h3 class="bankInt-securityTxt">${text.securityQuestion}</h3>
        <p class="bankInt-label bankInt-securityLabel">${data.securityQuestion}</p>
        <div class="ui input bankInt-input">
          <input type="text" placeholder="">
        </div>
      </div>

      <div id="m-bi-authorizingDiv" style="display: none">
        <div class="bankInt-preloader">
          <img alt="progress spinner" src="images/loading/spinner.svg" width="200px" height="200px" style="position:relative; top:-5px">
        </div>
        <p class="bankInt-subText" id="m-bi-loaderText">${text.authorizing}...</p>
      </div>

      <div id="m-bi-loadingDiv" style="display: none">
        <div class="bankInt-preloader">
          <img alt="progress spinner" src="images/loading/spinner.svg" width="200px" height="200px" style="position:relative; top:-5px">
        </div>
        <p class="bankInt-subText" id="m-bi-loaderText">${c.lang.loading}...</p>
      </div>

      <div id="m-bi-success" style="display: none">
        <img src="images/illustrations/bankSuccess.png" class="bankInt-successImg">
        <p class="bankInt-successTxt">${text.success} <span id="m-bi-successBankTitle">your bank.</span></p>
      </div>

      <div id="m-bi-fail" style="display: none">
        <img src="images/illustrations/fail.png" class="bankInt-failImg">
        <p class="bankInt-failTxt">${text.unsuccessful}</p>
        <p class="bankInt-failTxtTwo">${text.fail}</p>
      </div>

      <div id="m-bi-accounts" class="bankInt-accountSection" style="display: none">
      </div>

      <div id="m-bi-exitBank" class="bankInt-exitSection" style="display: none">
        <img src="images/illustrations/exitbank.png" class="bankInt-exitImg">
        <p class="bankInt-subText bankInt-exitText">${text.exitBankOne}</p>
        <p class="bankInt-subText bankInt-exitText">${text.exitBankTwo}</p>
      </div>
      `
    container.innerHTML = content

    var btnHolder = Forge.build({dom: 'div', id: 'm-bi-buttons', style: 'display: none'})
    var sendBtn = Forge.buildBtn({text: c.lang.send, id: 'm-bi-sendBtn', type: 'primary', width: '128px', margin: '0px 4px'}).obj
    var cancelBtn = Forge.buildBtn({text: c.lang.cancel, id: 'm-bi-cancelBtn', type: 'secondary', width: '128px', margin: '0px 4px'}).obj
    var continueBtn = Forge.buildBtn({text: c.lang.continue, id: 'm-bi-continueBtn', type: 'primary', width: '128px', margin: '55px auto 0px'}).obj
    var retryBtn = Forge.buildBtn({text: text.retry, id: 'm-bi-retryBtn', type: 'primary', width: '128px', margin: '29px auto 0px'}).obj
    var submitBtn = Forge.buildBtn({text: c.lang.submit, id: 'm-bi-submitBtn', type: 'primary', width: '128px', margin: '28px auto 0px'}).obj
    var secondHolder = Forge.build({dom: 'div', id: 'm-bi-accountBtnHolder', class: 'bankInt-secondBtnHolder', style: 'display: none; width: 404px'})
    // var backBtn = Forge.build({dom: 'a', text: c.lang.back, id: 'm-bi-backBtn', class: 'bankInt-secondBtns bankInt-back'})
    var nextBtn = Forge.build({dom: 'a', text: c.lang.next, id: 'm-bi-nextBtn', class: 'bankInt-secondBtns bankInt-next'})
    var exitHolder = Forge.build({dom: 'div', id: 'm-bi-exitBtnHolder', style: 'display: none'})
    var exitBtn = Forge.buildBtn({text: c.lang.exit, id: 'm-bi-exitBtn', type: 'warning', width: '128px', margin: '32px 4px 0px'}).obj
    // var backBtnFull = Forge.buildBtn({text: c.lang.back, id: 'm-bi-backBtnFull', type: 'secondary', width: '128px', margin: '32px 4px 0px'}).obj

    btnHolder.appendChild(cancelBtn)
    btnHolder.appendChild(sendBtn)
    // secondHolder.appendChild(backBtn)
    secondHolder.appendChild(nextBtn)
    // exitHolder.appendChild(backBtnFull)
    exitHolder.appendChild(exitBtn)
    container.appendChild(btnHolder)
    container.appendChild(continueBtn)
    container.appendChild(retryBtn)
    container.appendChild(submitBtn)
    container.appendChild(secondHolder)
    container.appendChild(exitHolder)
    continueBtn.style.display = 'none'
    retryBtn.style.display = 'none'
    submitBtn.style.display = 'none'
    sendBtn.disabled = true

    return container
  }

  self.setModalName('Bank-Integration')
  self.setClose({window: false, global: false})
  self.setWidth('483px')
  self.setHeight('534px')
  self.setTitle(text.connecting)
  self.setHeader({show: true, color: '#363A3C'})
  self.setPosition({top: '60px'})
  self.build()

  /**
   * Responsible for getting data from usrl and store it in json format
   * @return {object} -url details in json formate
   */
  function getJsonFromUrl () {
    var query = location.search.substr(1)
    var result = {}
    query.split('&').forEach(function (part) {
      var item = part.split('=')
      result[item[0]] = decodeURIComponent(item[1])
    })
    return result
  }

  /**
   * This function is responsible for loading the finicity calls
   * @memberof Modal_BankIntegration
   */
  this.eventListeners = function () {
    /* Submit username and password */
    $('#m-bi-sendBtn').off('click').on('click', function () {
      finicitySubmitLogin()
    })

    var inputFieldChecks = function () {
      if ($('#m-bi-userName').val() !== '' && $('#m-bi-password').val() !== '') {
        document.getElementById('m-bi-sendBtn').disabled = false
      } else {
        document.getElementById('m-bi-sendBtn').disabled = true
      }
    }

    /* Enable continue button when input fields aren't empty. */
    document.getElementById('m-bi-userName').addEventListener('keyup', function () {
      inputFieldChecks()
    })
    document.getElementById('m-bi-password').addEventListener('keyup', function () {
      inputFieldChecks()
    })

    /* Enter Button functionality */
    $('#m-bi-userName').off('keyup').on('keyup', function (event) {
      if (event.keyCode === 13) {
        $('#m-bi-password').focus()
      }
    })
    $('#m-bi-password').off('keyup').on('keyup', function (event) {
      if (document.getElementById('m-bi-sendBtn').disabled === false) {
        if (event.keyCode === 13) {
          $('#m-bi-sendBtn').click()
        }
      }
    })

    /* Now that we've synced lets see the accounts */
    $('#m-bi-continueBtn').off('click').on('click', function () {
      completeIntegration()
    })

    /* Forgot password, so receive security questions from finicity */
    $('#m-bi-forgot-password').off('click').on('click', function () {
      // TODO: Function to retrieve the security Question
      viewSection('m-bi-securityQuestion')
      viewButton('m-bi-submitBtn')
    })

    /* Make sure the user wants to exit the integration */
    $('#m-bi-exitIcon').off('click').on('click', function () {
      viewSection('m-bi-exitBank')
      viewButton('m-bi-exitBtnHolder')
    })
    $('#m-bi-cancelBtn').off('click').on('click', function () {
      viewSection('m-bi-exitBank')
      viewButton('m-bi-exitBtnHolder')
    })

    /* Exit is confirmed */
    $('#m-bi-exitBtn').off('click').on('click', function () {
      self.close()
    })

    /* Go Back to previous view */
    // $('#m-bi-backBtn').off('click').on('click', function () {
    //   goToPrevious()
    // })
    // $('#m-bi-backBtnFull').off('click').on('click', function () {
    //   goToPrevious()
    // })

    /* Finished integrating */
    $('#m-bi-nextBtn').off('click').on('click', function () {
      var chosenAccounts = []
      for (var i = 0; i < accountList.length; i++) {
        var checkboxID = 'm-bi-accountCheck-' + i
        const account = accountList[i]
        if (document.getElementById(checkboxID).checked) {
          chosenAccounts.push(accountList[i])
          account.answer = true
        } else {
          account.answer = false
        }
      }
      var selectedAccountData = {
        code: 0,
        sessionID: currentSessionID,
        accountsSelected: chosenAccounts
      }
      c.finicityAccountListSelectedRequest(selectedAccountData)
      viewSection('m-bi-loadingDiv')
      viewButton('none')
      // TODO: SHOW THE BANK ACCOUNTS ON THE BANK PAGE
      // self.close()
    })

    /* Start the integration again */
    $('#m-bi-retryBtn').off('click').on('click', function () {
      viewSection('m-bi-connectingDiv')
      viewButton('none')
      self.finicityConnect()
    })

    /* Submit the answer to the security question */
    // document.getElementById('m-bi-submitBtn').addEventListener('click', function () {
    //   /* if there are no errors */
    //   viewButton('none')
    //   viewSection('m-bi-authorizingDiv')
    //   /* Once the authorization is complete set the loader (currently setTimeout timer is just for effect). */
    //   setTimeout(function () {
    //     viewSection('m-bi-loadingDiv')
    //     /* Once the loader is complete set the next div (currently setTimeout imer is just for effect). */
    //     setTimeout(function () {
    //       /* If there was success show success div */
    //       if (data.success === true) {
    //         viewButton('m-bi-continueBtn')
    //         viewSection('m-bi-success')
    //       } else {
    //         /* If the login failed show failed div */
    //         viewButton('m-bi-retryBtn')
    //         viewSection('m-bi-fail')
    //       }
    //     }, 1000)
    //   }, 1000)
    // })
  }

  this.finicityConnect = function () {
    var connectData = {
      currentBusinessID: c.currentBusiness._id,
      institutionID: 101732,
      name: 'fn-connect'
    }
    c.finicityConnect(connectData)
  }

  this.setSessionID = function (data) {
    currentSessionID = data
  }

  /* Information about the banks login screens. Sets the login screen. */
  this.finicityLoginFormResponse = function (data) {
    if (data) {
      bankName = data.institutionRecord.name
      c.bankName = bankName
      self.setTitle(bankName)
      if (data.loginInfo) {
        document.getElementById('bankInt-login-label').innerHTML = data.loginInfo[0].description
        document.getElementById('bankInt-password-label').innerHTML = data.loginInfo[1].description
      }
      if (data.loginInfo.length > 1) {
        userNameObject = data.loginInfo[0]
        passwordObject = data.loginInfo[1]
      }
      viewButton('m-bi-buttons')
      viewSection('m-bi-login')
    } else {
      self.cantConnect()
    }
  }

  /**
   * Responsilbe for getting login input details and emit fn-loginSubmit
   * @param  {object} event -mouse event data
   */
  var finicitySubmitLogin = function () {
    var userName = $('#m-bi-userName').val()
    userNameObject.value = userName
    var password = $('#m-bi-password').val()
    passwordObject.value = password
    loginInfo.push(userNameObject)
    loginInfo.push(passwordObject)

    var loginData = {
      code: 0,
      loginInfo: loginInfo,
      sessionID: currentSessionID
    }
    c.finicityLoginSubmit(loginData)

    viewSection('m-bi-authorizingDiv')
    viewButton('none')
    $('#m-bi-userName').val('')
    $('#m-bi-password').val('')
    return
  }

  this.finicityAccountList = function (accountsData) {
    if (accountsData.result) {
      accountList = accountsData.result
      document.getElementById('m-bi-successBankTitle').innerHTML = bankName
      showAccountList()
    } else {
      /* If the login failed show failed div */
      viewButton('m-bi-retryBtn')
      viewSection('m-bi-fail')
    }
  }

  this.cantConnect = function () {
    self.reset()
    viewButton('m-bi-retryBtn')
    viewSection('m-bi-fail')
  }

  this.finicityLoginComplete = function (completeData) {
    viewButton('m-bi-continueBtn')
    viewSection('m-bi-success')
  }

  var completeIntegration = function () {
    var completeData = {
      code: 0,
      sessionID: currentSessionID,
      engagementID: c.myEngagements[0]._id
    }
    c.finicityLoginComplete(completeData)
    self.close()
  }

  this.reset = function () {
    $('#m-bi-userName').val('')
    $('#m-bi-password').val('')
    content = ``
    accountsContent = ``
    currentSessionID = ''
    loginInfo = []
    accountList = []
    bankName = ''
    userNameObject = {}
    passwordObject = {}
    viewSection('m-bi-connectingDiv')
    viewButton('none')
  }
}

module.exports = Modal_BankIntegration
