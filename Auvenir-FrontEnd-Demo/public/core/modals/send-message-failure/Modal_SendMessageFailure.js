'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import styles from './Modal_SendMessageFailure.css'

var Modal_SendMessageFailure = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  var self = this

  this.buildDom = function () {
    var container = Forge.build({dom: 'div', id: 'send-message-failure-modal-container'})
    container.appendChild(Forge.build({
      dom: 'img',
      src: '/images/illustrations/illustration-envelope.svg',
      style: ''
    }))
    container.appendChild(Forge.build({
      dom: 'div',
      text: 'We were unable to send you message.',
      id: 'send-message-failure-modal-tip'
    }))

    var cancelBtn = Forge.build({
      dom: 'button',
      text: 'Cancel',
      class: 'btn',
      id: 'send-message-failure-cancel-btn'
    })
    cancelBtn.addEventListener('click', function (event) {
      event.preventDefault()
      self.close()
    })
    container.appendChild(cancelBtn)

    var retryBtn = Forge.build({
      dom: 'button',
      text: 'Try Again',
      class: 'btn',
      id: 'send-message-failure-retry-btn'
    })
    retryBtn.addEventListener('click', function (event) {
      event.preventDefault()
      self.close()
    })

    container.appendChild(retryBtn)
    return container
  }

  self.setModalName('Unable to send message')
  self.setClose({window: true, global: false})
  self.setWidth('483px')
  self.setTitle('Unable to send message')
  self.setHeader({show: true, color: '#363a3c', icon: 'warning'})
  self.setPosition({bottom: '0px'})
  self.build()
}

module.exports = Modal_SendMessageFailure
