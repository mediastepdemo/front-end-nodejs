'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import styles from './Modal_DeleteRequestFile.css'
/**
 * The modal is responsible for deleting the request for that user.
 * It is modal that prompts the user about deletion of request.
 * @class Modal_DeleteRequestFile
 * @param: {Object} ctrl     - appController for the current user
 * @param: {Object} settings - settings of the current user
 */
var Modal_DeleteRequestFile = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  var self = this
  var selectedFile = null

/**
  * This function is responsible for building the page only when called
  * @memberof Modal_DeleteRequestFile
  */
  this.buildDom = function () {
    var container = Forge.build({dom: 'div', id: 'del-req-file-container', class: 'del-req-file-container'})

    container.appendChild(Forge.build({dom: 'div', text: 'Once you delete this file you will no longer be able to access it. Are you sure you would like to perminently remove the following file?', class: 'del-req-file-tip'}))
    container.appendChild(Forge.build({dom: 'div', text: '', class: 'del-req-file-tip', id: 'del-req-file-tip'}))

    var cancelBtn = Forge.buildBtn({text: 'Cancel', type: 'light', width: '128px', margin: '4px'}).obj
    cancelBtn.addEventListener('click', function (event) {
      event.preventDefault()
      self.close()
    })

    container.appendChild(cancelBtn)

    var confirmBtn = Forge.buildBtn({text: 'Delete', type: 'warning', width: '128px', margin: '4px'}).obj

    confirmBtn.addEventListener('click', function (event) {
      event.preventDefault()
      self.trigger('delete-file', selectedFile)
      self.close()
    })

    container.appendChild(confirmBtn)
    return container
  }

  self.setModalName('Delete Request File')
  self.setClose({window: true, global: false})
  self.setWidth('480px')
  self.setHeight('440px')
  self.setTitle('Delete File?')
  self.setHeader({show: true, color: '#363a3c', icon: 'warning'})
  self.setPosition({top: '100px'})
  self.build()

/**
  * This function is responsible for loading the text with current request name.
  * @memberof Modal_DeleteRequestFile
  */
  this.loadData = function () {
    selectedFile = self.data
    document.getElementById('del-req-file-tip').innerHTML = selectedFile.fileName
  }
}

module.exports = Modal_DeleteRequestFile
