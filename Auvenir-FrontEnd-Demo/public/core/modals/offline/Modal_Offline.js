'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import styles from './Modal_Offline.css'
/**
   * The Offline Modal controls the logic when the client of server goes offline
   * @class Modal_Offline
 *
   * @param: {Object} ctrl   - appController for the current user
   * @param: {Object} settings - settings of the current user
  */
var Modal_Offline = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  var c = ctrl

  var self = this
  var connectId = 'offline-connect-container'
  var waitId = 'offline-wait-container'
  var timeoutId = 'offline-timeout-container'

  var waitState = null
  var connectState = null
  var timeOutState = null

  self.setBackground('#131f4f')

  // Attributes for wait
  var state = this
  var attempt = 0
  var counter = 0
  var timeInterval = null

  // Constants
  var MAX_ATTEMPTS = 5
  var MAX_WAIT = 7

  /**
     * Prebuilds the modal doms and sets them to hidden
     * @memberof Modal_Offline
    */
  this.buildDom = function () {
    var domContainer = Forge.build({'dom': 'div', 'class': 'offline-parent'})

    // Wait Container
    var waitContainer = Forge.build({'dom': 'div', 'id': waitId})
    var serverWaitText = Forge.build({'dom': 'p', 'class': 'offline-body-wait offline-icon', 'id': 'offline-server-wait-text', 'text': 'Disconnected from the server.' })
    var timerText = Forge.build({'dom': 'p', 'class': 'offline-body-wait', 'id': 'offline-timer-text', 'text': 'Trying to reconnect in ' + (MAX_WAIT - counter) + 's...'})
    var reconnectBtn = Forge.build({'dom': 'a', 'class': 'offline-reconnect-now-btn', 'text': 'Reconnect Now'})

    var warningSvg = Forge.build({'dom': 'img', 'src': 'images/icons/warning-white-filled.svg'})
    reconnectBtn.addEventListener('click', function () {
      clearInterval(timeInterval)
      showConnect()
    })

    waitContainer.appendChild(serverWaitText)
    serverWaitText.appendChild(timerText)
    waitContainer.appendChild(reconnectBtn)
    domContainer.appendChild(waitContainer)
    domContainer.appendChild(warningSvg)
    warningSvg.style.display = 'none'

    // Connect Container
    var connectContainer = Forge.build({'dom': 'div', 'id': connectId})
    var serverConnectText = Forge.build({'dom': 'p', 'class': 'offline-body-connect offline-icon', 'id': 'server-connect-text', 'text': 'There seemed to be a problem connecting to the server.'})
    var connectText = Forge.build({'dom': 'p', 'class': 'offline-reconnect-now-text', 'text': 'Connecting...'})

    connectContainer.appendChild(serverConnectText)
    connectContainer.appendChild(connectText)
    domContainer.appendChild(connectContainer)

    // Timeout Container
    var timeoutContainer = Forge.build({'dom': 'div', 'id': timeoutId})
    var submitText = Forge.build({'dom': 'p', 'class': 'offline-body-timeout offline-icon', 'text': 'There was a problem reconnecting to the server. Please check your internet connection then log back in to Auvenir. '})

    timeoutContainer.appendChild(submitText)
    domContainer.appendChild(timeoutContainer)

    return domContainer
  }

  /**
   * Displays the div for the Countdown until reconnect state, enables the logic for the div
   * @memberof Modal_Offline
    */
  this.showWait = function () {
    showWait()
  }
  var showWait = function () {
    log('Modal_Offline: Display Wait')
    var waitContainer = document.getElementById(waitId)
    var timerText = document.getElementById('offline-timer-text')
    var serverText = document.getElementById('offline-server-wait-text')
    // Makes sure there are no other intervals running when we run this
    if (timeInterval) {
      clearInterval(timeInterval)
      timeInterval = null
    }

    document.getElementById(waitId).style.display = 'inherit'
    document.getElementById(connectId).style.display = 'none'
    document.getElementById(timeoutId).style.display = 'none'

    attempt++
    counter = 1

    serverText.removeChild(timerText)
    timerText.innerHTML = 'Trying to reconnect in ' + (MAX_WAIT - counter) + 's...'
    serverText.appendChild(timerText)

    // Counting script
    timeInterval = setInterval(function () {
      if (self.isOpen()) {
        if (counter < MAX_WAIT) {
          counter++
          serverText.removeChild(timerText)
          timerText.innerHTML = 'Trying to reconnect in ' + (MAX_WAIT - counter) + 's...'
          serverText.appendChild(timerText)
        } else {
          clearInterval(timeInterval)
          showConnect()
        }
      }
    }, 1000)
  }

  /**
     * Displays the div for the connecting state, enables the logic for the div
     * @memberof Modal_Offline
    */
  var showConnect = function () {
    log('Modal_Offline: Display Connect')
    document.getElementById(waitId).style.display = 'none'
    document.getElementById(connectId).style.display = 'inherit'
    document.getElementById(timeoutId).style.display = 'none'
    c.mySocket.tryConnect(function (err) {
      if (err) {
        if (attempt >= MAX_ATTEMPTS) {
          showTimeout()
          return
        } else {
          showWait()
        }
      } else {
        self.close()
        counter = 0
        attempt = 0
      }
    })
  }

  /**
     * Display the special timeout message div
     * @memberof Modal_Offline
    */
  var showTimeout = function () {
    log('Modal_Offline: Display Timeout')
    document.getElementById(waitId).style.display = 'none'
    document.getElementById(connectId).style.display = 'none'
    document.getElementById(timeoutId).style.display = 'inherit'
  }

  var init = function () {
    self.setModalName('offlineModal')
    self.setClose({'window': true, 'global': false})
    self.setWidth('412px')
    self.setHeight('165px')
    self.setHeader({'show': false})
    self.setPosition({'top': 'inherit', 'right': 'inherit'})
    self.build()
  }

  init()
}

Modal_Offline.prototype = ModalTemplate.prototype
Modal_Offline.prototype.constructor = Modal_Offline

module.exports = Modal_Offline
