'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import styles from './Modal_RemoveClient.css'
/**
 * Displays an email input modal
 * @class Modal_Email
 *
 * @param {Object} ctrl      - A reference to the current instance of the Main Application Controller (Controller.js)
 * @param {Object} settings - The custom settings that are used to generate the instance of this modal.
 */
var Modal_RemoveClient = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  this.configure = function (receivers, subject, content) {

  }

  this.buildDom = function () {
    function buildHeader () {
      var header = Forge.build({'dom': 'div', 'class': 'remove-client-header'})
      var caution = Forge.build({
        'dom': 'img',
        'class': 'remove-client-caution-icon',
        'src': 'images/icons/warning.svg',
        'style': ''
      })
      var headerText = Forge.build({'dom': 'div', 'class': 'remove-client-headerText', 'style': 'float:left', 'text': 'Remove Client ?'})
      var close = Forge.build({
        'dom': 'img',
        'class': 'remove-client-close-icon',
        'src': 'images/icons/x-small.svg'

      })
      close.addEventListener('click', function () {
        ctrl.closeRemoveClientModal()
      })

      header.appendChild(close)
      header.appendChild(caution)
      header.appendChild(headerText)

      return header
    }

    var contentBody = Forge.build({
      'dom': 'div',
      'class': 'remove-client-bodyText',
      'text': 'You are about to remove your client from this engagement. Once removed you will have the option to add another client.'

    })
    var cancel = Forge.build({'dom': 'button', 'text': 'Cancel', 'class': 'btn remove-client-cancel-btn', 'id': 'cancel-btn'})

    cancel.addEventListener('click', function () {
      ctrl.closeRemoveClientModal()
    })

    var remove = Forge.build({'dom': 'button', 'text': 'Remove', 'class': 'btn remove-client-remove-btn', 'id': 'remove-btn'})
    //
    remove.addEventListener('click', function () {
      // TODO : integrate with backend
      // ctrl.removeClient();
    })

    var removeClientContainer = Forge.build({dom: 'div', class: 'remove-client-Container', style: 'margin:-15px; font-family: Lato;'})
    removeClientContainer.appendChild(buildHeader())
    removeClientContainer.appendChild(contentBody)
    removeClientContainer.appendChild(cancel)
    removeClientContainer.appendChild(remove)

    return removeClientContainer
  }

  this.setModalName('removeClient')
  this.setClose({'window': true, 'global': true})
  this.setWidth('480px')
  this.setHeight('349px')
  // this.setTittle('Remove Client');
  this.setPosition({'top': '22%'})
  this.build()
}

module.exports = Modal_RemoveClient
