'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import styles from './Modal_InviteClient.css'

var Modal_InviteClient = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  var c = ctrl
  var self = this
  var text = c.lang.inviteClientModal

  /**
   * This function is responsible for initializing the creation of a new Client
   * @memberof Modal_InviteClient
   */
  var initialize = function () {
    log('Modal_InviteClient: initialize')
    self.setModalName('dashboardModel')
    self.setClose({'window': true, 'image': 'images/icons/x-small-white.svg'})
    self.setFullScreen(true)
    self.setHeader({'show': false})
    self.setPosition({'bottom': '0px'})
    self.setPadding('0px')
    self.build()
  }

  var clientList = [{ text: text.newClient }]
  var selectedClient
  /**
   * This function is responsible for building the page only when called
   * @memberof Modal_InviteClient
   */
  this.buildDom = function () {
    log('Building Invite Client Modal')
    var superContainer = Forge.build({'dom': 'div', class: 'inm-parent'})
    var header = Forge.build({'dom': 'div', 'class': 'inm-header'})
    var headerTitle = Forge.build({'dom': 'h2', 'class': 'inm-headerTitle', id: 'm-ic-engName'})
    header.appendChild(headerTitle)
    superContainer.appendChild(header)

    var submitContainer = Forge.build({'dom': 'div', 'id': 'm-ic-inputContainer-' + self._uniqueID, 'class': 'm-ic-container', 'style': 'display:block'})
    const invite = Forge.build({dom: 'p', class: 'm-ic-subTitle', text: text.subTitle})
    submitContainer.appendChild(invite)

    var step1 = Forge.build({'dom': 'div', 'id': 'm-ic-step1-' + self._uniqueID, 'style': 'display: block; margin: 32px auto 250px; width: 351px; text-align: left;'})
    var step1Head = Forge.build({'dom': 'p', 'class': 'm-ic-headText', 'text': text.inputTitle})
    var step1InputHolder = Forge.build({'dom': 'div', 'id': 'm-ic-inputContainer', 'class': 'm-ic-inputHolder'})
    step1.appendChild(step1Head)
    step1.appendChild(step1InputHolder)

    submitContainer.appendChild(step1)

    var btnDiv = Forge.build({'dom': 'div', 'id': 'm-ic-footerMap', 'style': 'display:block', 'class': 'm-ic-btnContainer'})
    var cancelBtn = Forge.buildBtn({'text': c.lang.cancel, 'id': 'm-ic-cancelBtn', 'type': 'light', 'width': '128px', 'margin': '0px 8px 0px 0px'}).obj
    var continueBtn = Forge.buildBtn({'text': text.invite, 'id': 'm-ic-continueBtn', 'type': 'primary', 'width': '140px'}).obj
    btnDiv.appendChild(cancelBtn)
    btnDiv.appendChild(continueBtn)

    submitContainer.appendChild(btnDiv)
    superContainer.appendChild(submitContainer)

    continueBtn.setAttribute('disabled', 'true')
    continueBtn.blur()

    cancelBtn.addEventListener('click', function () {
      self.close()
    })

    continueBtn.addEventListener('click', function (evt) {
      let engagementID = c.currentEngagement._id
      let email
      let client
      let input = document.getElementById('m-ci-step-one-input')
      if (input.value === text.newClient) {
        c.displayAddClientModal(c.currentEngagement.name)
        setTimeout(function () {
          self.close()
        }, 150)
      } else {
        for (let i = 0; i < clientList.length; i++) {
          if (clientList[i].text === selectedClient) {
            email = clientList[i].email
            client = clientList[i]
          }
        }
        if (email && engagementID) {
          let oldEngagement = c.myEngagements.find((engagement) => {
            return engagement.business.keyContact.email === email
          })
          let oldClient = oldEngagement.business.keyContact
          if (oldClient) {
            c.selectClient('SELECT', engagementID, oldClient._id)
            self.close()
          }
        } else if (email) {
          c.selectClient('SELECT', engagementID, client._id)
          self.close()
        }
      }
    })

    return superContainer
  }

  this.buildDropdown = function () {
    if (document.getElementById('m-ci-step-one-input')) {
      var oldDrop = document.getElementById('m-ci-step-one-input')
      oldDrop.parentNode.removeChild(oldDrop)
    }
    var btn = document.getElementById('m-ic-continueBtn')
    var step1Input = Forge.buildInputDropdown({
      list: clientList,
      id: 'm-ci-step-one-input',
      placeholder: 'Select',
      width: '351px',
      style: 'height: 69px; width: 351px; padding-left: 14px; font-size: 18px',
      chevronStyle: 'top: 15px; left: 322px; font-size: 16px;',
      onSelect: (item) => {
        if (item.text === text.newClient) {
          self.close()
          c.displayAddClientModal(c.currentEngagement.name)
        } else {
          selectedClient = item.text
        }
      }
    })
    document.getElementById('m-ic-inputContainer').appendChild(step1Input)

    step1Input.children[2].addEventListener('blur', function () {
      if (step1Input.children[2].value === 'Select') {
        btn.setAttribute('disabled', 'true')
        btn.blur()
      } else {
        btn.removeAttribute('disabled')
      }
    })
  }

  this.loadClients = function (myClients) {
    document.getElementById('m-ic-engName').innerHTML = c.currentEngagement.name
    if (myClients) {
      clientList = [{ text: text.newClient }]
      for (let i = 0; i < myClients.length; i++) {
        if (myClients[i].keyContact !== '') {
          let clientName = ''
          let email = myClients[i].keyContact.email
          let id = myClients[i].keyContact._id
          let fname = myClients[i].keyContact.firstName || ''
          let lname = myClients[i].keyContact.lastName || ''
          let firm = myClients[i].name
          if (fname.trim() === '' && lname.trim() === '') {
            clientName = myClients[i].keyContact.email
          } else {
            clientName = `${fname} ${lname} (${firm})`
          }
          clientList.push({'text': clientName, email, firm, _id: id})
        } else {
          log('No keycontact to add')
        }
      }
    } else {
      log('No client registered with this engagement')
    }
  }

  initialize()
}

Modal_InviteClient.prototype = ModalTemplate.prototype
Modal_InviteClient.prototype.constructor = Modal_InviteClient

module.exports = Modal_InviteClient
