'use strict'

import ModalTemplate from '../ModalTemplate'
import log from '../../../js/src/log'
import Forge from '../../Forge'
import TermsOfUse from '../../../data/terms'
import styles from './Modal_Agreements.css'
/**
 * This is the Terms and Conditions Full Screen Modal
 * accessed from the onboarding page.
 *
 * @class Modal_TermsConditions
 * @param {Object} settings The specified settings for this modal
 * @param {Function} ctrl The Controller
 */

var Modal_TermsConditions = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)

  var self = this

  /**
   * This function initializes the buildDom function,
   * Runs the background function to set the color,
   * Runs the icon function to set the close button.
   *
   * @memberof Modal_TermsConditions
  */
  var initialize = function () {
    log('Modal_TermsConditions: initialize')

    self.setModalName('terms')
    self.setClose({'window': true, 'image': 'images/icons/x-large-white.svg'})
    self.setFullScreen(true)
    self.setHeader({'show': false})
    self.setPosition({ 'bottom': '0px'})
    // self.setTitle('Terms and Conditions');
    self.setBackground('#50baa8')
    self.setIcon()
    self.setOverflow('auto')
    self.build()

    // This is accessing the Terms & Conditions modal so that it
    // appears over top of the Skip Security Modal.
    var absoluteParent = $('#' + 'modal-body-' + self._uniqueID).parents(':nth(2)')
    absoluteParent[0].style.zIndex = '1113'
    document.getElementById('modal-body-' + self._uniqueID).style.overflowY = 'scroll'
  }
  this.buildDom = function () {
    var container = Forge.build({'dom': 'div'})
    var header = Forge.build({'dom': 'h3', 'text': 'Auvenir', 'class': 'terms-header'})
    var title = Forge.build({'dom': 'h2', 'text': TermsOfUse.title, 'class': 'terms-title'})
    var languages = Forge.build({'dom': 'div', 'class': 'terms-languages'})
    var english = Forge.build({'dom': 'a', 'text': 'English', 'style': 'cursor:pointer;'})
    var split = Forge.build({'dom': 'span', 'text': ' | '})
    var french = Forge.build({'dom': 'a', 'text': 'French', 'style': 'cursor:pointer;'})
    var agreement = Forge.build({'dom': 'div', 'class': 'terms-content'})
    container.appendChild(header)
    container.appendChild(title)
    container.appendChild(languages)
    languages.appendChild(english)
    languages.appendChild(split)
    languages.appendChild(french)
    container.appendChild(languages)
    agreement.innerHTML = TermsOfUse.EN
    container.appendChild(agreement)
    english.addEventListener('click', function () {
      agreement.innerHTML = TermsOfUse.EN
    })
    french.addEventListener('click', function () {
      agreement.innerHTML = TermsOfUse.FR
    })
    return container
  }

  /**
   * Builds Dom elements of the modal.
   * Adds a scroll cover and a fader feature to the bottom of the text.
   *
   * @memberof Modal_TermsConditions
   */

  initialize()
}

Modal_TermsConditions.prototype = ModalTemplate.prototype
Modal_TermsConditions.prototype.constructor = Modal_TermsConditions

module.exports = Modal_TermsConditions
