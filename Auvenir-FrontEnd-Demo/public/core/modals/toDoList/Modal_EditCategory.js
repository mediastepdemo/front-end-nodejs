'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import styles from './Modal_Category.css'

var Modal_EditCategory = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  var c = ctrl
  var self = this
  var text = c.lang.categoryModal

  /**
   * This function is responsible for initializing the creation of a new Client
   * @memberof Modal_EditCategory
   */
  var initialize = function () {
    log('Modal_EditCategory: initialize')
    self.setModalName('Category Edit Modal')
    self.setClose({'window': true, 'image': 'images/icons/x-large.svg'})
    self.setFullScreen(false)
    self.setWidth('480px')
    self.setHeight('auto')
    self.setOverflow('auto')
    self.setHeader({ 'show': true, color: '#363A3C' })
    self.setTitle(text.editHeaderTitle)
    self.setPosition({ 'bottom': '0px'})
    self.setPadding('0px')
    self.build()
  }

  this.loadData = function () {
    generateListCategory(c.currentEngagement.categories)
  }

  /**
   * validate function
   */
  var isFieldValid = function (updateDOM) {
    // We use updateDOM to get the updated value (data has old values).
    var valid = true
    // Sets valid if field is valid
    if (fields.name.validation) {
      if (fields.name.validation.required) {
        valid = valid && updateDOM.value.length > 0
      }

      if (fields.name.validation.min) {
        valid = valid && updateDOM.value.length >= fields.name.validation.min
      }

      if (fields.name.validation.max) {
        valid = valid && updateDOM.value.length <= fields.name.validation.max
      }

      if (fields.name.validation.regex) {
        log(fields.name.validation.regex)
        var regex = new RegExp(fields.name.validation.regex)
        valid = valid && !regex.test(updateDOM.value)
      }
    } else {
      valid = true
    }

    if (valid) {
      updateDOM.classList.remove('auv-error')
      updateDOM.closest('div').classList.toggle('auv-cat-error', false)
    } else {
      updateDOM.className += ' auv-error'
      updateDOM.closest('div').classList.toggle('auv-cat-error', true)
    }
    return valid
  }

  /*
   * Local instances of the field values
   */
  var fields = {
    name: { id: 'category-name', type: 'text', value: '', model: {name: 'engagement', field: 'category'}, validation: {min: 1, max: 100, regex: /[~!@#$%^&*+?><,.]/ }},
    color: { id: 'category-color', type: 'text', value: '', model: {name: 'engagement', field: 'category'}}
  }

  /**
   * This function is responsible for building the page only when called.
   * @memberof Modal_EditCategory
   */
  this.buildDom = function () {
    log('Building Category Modal')

    var parentContainer = Forge.build({'dom': 'div', class: 'ce-parent todo-modal-container'})
    var systemContainer = Forge.build({'dom': 'div', 'class': 'ce-systemContainer'})
    var componentSystem = Forge.build({'dom': 'div'})
    var componentBody = Forge.build({'dom': 'p', 'id': 'setup-component-body', 'class': 'modal-body'})
    var inputContainer = Forge.build({'dom': 'div', 'class': 'setup-inputContainer edit-category-container todo-input-container'})

    componentBody.appendChild(inputContainer)
    componentSystem.appendChild(componentBody)

    var footer = Forge.build({'dom': 'div', 'class': 'ce-footer'})
    var footerBtnHolder = Forge.build({'dom': 'div', 'class': 'ce-footerBtnHolder category-footer'})
    var footerCancelBtn = Forge.buildBtn({
      'text': c.lang.cancel,
      'id': 'm-ce-cancelBtn',
      'type': 'secondary',
      'margin': '22px 10px 0px 0px'
    }).obj
    var footerContinueBtn = Forge.buildBtn({
      'text': 'Save',
      'id': 'category-updateBtn',
      'type': 'primary'
    }).obj
    footerContinueBtn.disabled = true

    footerCancelBtn.addEventListener('click', function () {
      self.close()
    })

    footerContinueBtn.addEventListener('click', function () {
      log('Event: click update button')
      var inputs = document.getElementsByClassName('edit-category-container')[0].querySelectorAll('input')
      var data = {delete: [], update: []}
      inputs.forEach(function (item) {
        if (item.getAttribute('data-del') === '1') {
          data.delete.push({
            categoryId: item.getAttribute('data-id'),
            name: item.value
          })
        } else {
          if (item.getAttribute('data-dbdata') !== item.value) {
            data.update.push({
              categoryId: item.getAttribute('data-id'),
              name: item.value
            })
          }
        }
      })
      log(data)
      if (data.delete.length > 0) {
        deleteCategories(data.delete)
      }
      if (data.update.length > 0) {
        updateCategories(data.update)
      }
      self.close()
    })

    footerBtnHolder.appendChild(footerCancelBtn)
    footerBtnHolder.appendChild(footerContinueBtn)
    footer.appendChild(footerBtnHolder)

    systemContainer.appendChild(componentSystem)
    systemContainer.appendChild(footer)
    parentContainer.appendChild(systemContainer)

    return parentContainer
  }

  /*
   * generate list category
   */
  var generateListCategory = function (data) {
    document.getElementById('category-updateBtn').disabled = true
    const listContainer = document.getElementsByClassName('edit-category-container')[0]
    listContainer.innerHTML = ''
    var desDiv = Forge.build({'dom': 'div', 'text': text.desEditModal, 'class': 'des-edit-modal'})
    listContainer.appendChild(desDiv)
    var categoryUpdateBtn = document.getElementById('category-updateBtn')
    categoryUpdateBtn.innerText = 'Save'
    categoryUpdateBtn.classList.remove('warning')

    data.forEach(function (item) {
      if (item._id !== 'uncategorized') {
        var rowDiv = Forge.build({'dom': 'div', 'class': 'item'})
        var nameInput = Forge.buildInput({'label': '', 'errorText': text.nameError, 'placeholder': ''})
        var textColor = (item.color === null ? 'black' : 'white')
        nameInput.children[1].classList.add('edit-category')
        nameInput.children[1].value = item.name
        nameInput.children[1].style = 'background: ' + item.color + '; color: ' + textColor
        nameInput.children[1].setAttribute('data-id', item._id)
        nameInput.children[1].setAttribute('data-del', '0')
        nameInput.children[1].setAttribute('data-dbdata', item.name)
        nameInput.children[1].setAttribute('maxlength', 100)

        nameInput.children[1].disabled = true
        var editBtn = Forge.build({'dom': 'div', 'id': 'cat-edit-btn', 'class': 'auvicon-edit cat-edit-btn'})
        var trashBtn = Forge.build({'dom': 'div', 'id': 'cat-trash-btn', 'class': 'auvicon-trash cat-trash-btn'})
        var iconTrash = Forge.build({'dom': 'i', 'class': 'fa fa-times'})
        iconTrash.style.display = 'none'

        rowDiv.appendChild(trashBtn)
        rowDiv.appendChild(editBtn)
        rowDiv.appendChild(iconTrash)
        rowDiv.appendChild(nameInput)
        listContainer.appendChild(rowDiv)

        // validate form
        nameInput.children[1].addEventListener('blur', function () {
          isFieldValid(this)
          checkInputValidation()
        })
        var timeName = null
        nameInput.children[1].onkeydown = function () {
          var context = this
          clearTimeout(timeName)
          timeName = setTimeout(function () {
            isFieldValid(context)
            checkInputValidation()
          }, 400)
        }

        editBtn.addEventListener('click', function () {
          var icon = this.closest('div.item').querySelector('.fa-times')
          var elem = this.closest('div.item').querySelector('input')
          setTimeout(function () {
            icon.style.display = 'none'
          }, 100)
          elem.setAttribute('data-del', '0')
          elem.disabled = false
          elem.focus()
        })

        trashBtn.addEventListener('click', function () {
          var elem = this.closest('div.item').querySelector('input')
          var icon = this.closest('div.item').querySelector('.fa-times')
          icon.style.display = (icon.style.display === 'none' ? 'block' : 'none')
          elem.disabled = true
          elem.setAttribute('data-del', (icon.style.display === 'none' ? '0' : '1'))
          if (icon.style.display === 'block') {
            document.getElementById('category-updateBtn').disabled = false
            checkInputValidation()
          }
        })

        rowDiv.appendChild(nameInput)
        listContainer.appendChild(rowDiv)

        // validate form
        nameInput.children[1].addEventListener('blur', function () {
          isFieldValid(this)
          checkInputValidation()
        })
        var timeName = null
        nameInput.children[1].onkeydown = function () {
          var context = this
          clearTimeout(timeName)
          timeName = setTimeout(function () {
            isFieldValid(context)
            checkInputValidation()
          }, 400)
        }
      }
    })

    var checkInputValidation = function () {
      // check for input fields validation
      var err = false
      var inputs = document.getElementsByClassName('edit-category-container')[0].querySelectorAll('input')
      for (var i = 0; i < inputs.length; i++) {
        var item = inputs[i]
        if (item.classList.contains('auv-error') || item.value === '') {
          err = true
          break
        }
      }
      document.getElementById('category-updateBtn').disabled = err
      return err
    }
  }

  var updateCategories = function (data) {
    let dataList = {}
    dataList.engagementID = c.currentEngagement._id
    dataList.categories = data
    c.sendEvent({name: 'update-category', data: dataList})
  }

  var deleteCategories = function (data) {
    let dataList = {}
    dataList.engagementData = {currentUserID: c.currentUser._id, engagementID: c.currentEngagement._id}
    dataList.categories = data
    c.sendEvent({name: 'delete-category', data: dataList})
  }

  initialize()
}

Modal_EditCategory.prototype = ModalTemplate.prototype
Modal_EditCategory.prototype.constructor = Modal_EditCategory

module.exports = Modal_EditCategory
