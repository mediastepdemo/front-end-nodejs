'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import styles from './Modal_Category.css'

var Modal_AddCategory = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  var c = ctrl
  var self = this
  var text = c.lang.categoryModal
  var todoId = null

  /**
   * This function is responsible for initializing the creation of a new Client
   * @memberof Modal_Category
   */
  var initialize = function () {
    log('Modal_AddCategory: initialize')
    self.setModalName('categoryModel')
    self.setClose({'window': true, 'image': 'images/icons/x-large.svg'})
    self.setFullScreen(false)
    self.setWidth('480px')
    self.setOverflow('auto')
    self.setHeader({ 'show': true, color: '#363A3C' })
    self.setTitle(text.addHeaderTitle)
    self.setPosition({ 'bottom': '0px'})
    self.setPadding('0px')
    self.build()
  }

  /*
   * Reset form
   */
  this.loadData = function (id) {
    todoId = id
    var nameInput = document.getElementById(fields.name.id)
    var colorInput = document.getElementById(fields.color.id)
    document.getElementById('category-addBtn').disabled = true
    nameInput.value = ''
    colorInput.value = ''

    nameInput.classList.remove('auv-error')
    colorInput.classList.remove('auv-error')
    colorInput.style = 'width: 253px;'
    setTimeout(function () {
      nameInput.focus()
    }, 400)
  }

  /*
   * Bringing in the localization language
   */
  var colorList = [
    {text: '#317f85'}, {text: '#5c9a9e'}, {text: '#426f8a'}, {text: '#2b4875'}, {text: '#16315c'},
    {text: '#161a5c'}, {text: '#d65c33'}, {text: '#a33517'}, {text: '#575a5c'}, {text: '#000000'}
  ]

  /*
   * Local instances of the field values
   */
  var fields = {
    name: { id: 'category-name', type: 'text', value: '', model: {name: 'engagement', field: 'category'}, validation: {min: 1, max: 100, regex: /[~!@#$%^&*+?><,.]/ }},
    color: { id: 'category-color', type: 'text', value: '', model: {name: 'engagement', field: 'category'}, validation: {min: 1, max: 7 }}
  }

  /**
   * validate function
   */
  var isFieldValid = function (data) {
    // We use updateDOM to get the updated value (data has old values).
    var updateDOM = document.getElementById(data.id)
    var valid = true
    // Sets valid if field is valid
    if (data.validation) {
      if (data.validation.required) {
        valid = valid && updateDOM.value.length > 0
      }

      if (data.validation.min) {
        valid = valid && updateDOM.value.length >= data.validation.min
      }

      if (data.validation.max) {
        valid = valid && updateDOM.value.length <= data.validation.max
      }

      if (data.validation.regex) {
        var regex = new RegExp(data.validation.regex)
        valid = valid && !regex.test(updateDOM.value)
      }
    } else {
      valid = true
    }

    if (valid) {
      document.getElementById(data.id).classList.remove('auv-error')
    } else {
      document.getElementById(data.id).className += ' auv-error'
    }
    return valid
  }

  /**
   * This function is responsible for building the page only when called.
   * @memberof Modal_AddCategory
   */
  this.buildDom = function () {
    log('Building Category Modal')

    var parentContainer = Forge.build({'dom': 'div', class: 'ce-parent todo-modal-container'})
    var systemContainer = Forge.build({'dom': 'div', 'class': 'ce-systemContainer'})
    var componentSystem = Forge.build({'dom': 'div', 'class': 'todo-modal-component'})
    var componentBody = Forge.build({'dom': 'p', 'id': 'setup-component-body', 'class': 'modal-body'})

    var inputContainer = Forge.build({'dom': 'div', 'class': 'setup-inputContainer todo-input-container'})
    var nameInput = Forge.buildInput({'label': text.nameLabel, 'errorText': text.nameError, 'id': fields.name.id, 'placeholder': '', 'width': '258px'})
    var colorInput = Forge.buildInputDropdown({'label': text.colorLabel, 'list': colorList, 'id': fields.color.id, 'placeholder': '', 'width': '258px'})

    nameInput.querySelector('.auv-inputError').style = 'text-align:left'
    // validate name
    nameInput.children[1].setAttribute('maxlength', 100)
    nameInput.children[1].addEventListener('blur', function () {
      isFieldValid(fields.name)
      checkInputValidation()
    })
    var timeName = null
    nameInput.children[1].onkeydown = function () {
      clearTimeout(timeName)
      timeName = setTimeout(function () {
        isFieldValid(fields.name)
        checkInputValidation()
      }, 400)
    }

    // validate color
    var coloItem = colorInput.querySelectorAll('ul li')
    coloItem.forEach(function (item) {
      var color = item.children[0].innerHTML
      item.style = item.children[0].style = 'background: ' + color + '; color: ' + color
    })
    colorInput.children[2].onblur = colorInput.children[2].onchange = function () {
      var context = this
      if (context.value !== '') {
        context.style = 'background: ' + context.value + '; color: ' + context.value + '; width: 253px'
        setTimeout(function () {
          context.style = 'background: ' + context.value + '; color: ' + context.value + '; width: 253px'
        }, 200)
      }
      isFieldValid(fields.color)
      checkInputValidation()
    }
    colorInput.children[2].onkeypress = function () {
      return false
    }

    inputContainer.appendChild(nameInput)
    inputContainer.appendChild(colorInput)

    componentBody.appendChild(inputContainer)

    componentSystem.appendChild(componentBody)

    var footer = Forge.build({'dom': 'div', 'class': 'ce-footer'})
    var footerBtnHolder = Forge.build({'dom': 'div', 'class': 'ce-footerBtnHolder', style: 'margin:auto;'})
    var footerCancelBtn = Forge.buildBtn({
      'text': c.lang.cancel,
      'id': 'm-ce-cancelBtn',
      'type': 'secondary',
      'margin': '22px 10px 0px 0px'
    }).obj
    var footerContinueBtn = Forge.buildBtn({
      'text': text.btnCreate,
      'id': 'category-addBtn',
      'type': 'primary'
    }).obj
    footerContinueBtn.disabled = true

    footerCancelBtn.addEventListener('click', function () {
      self.close()
    })

    footerContinueBtn.addEventListener('click', function () {
      log('Event: click category-addBtn')
      var data = {
        name: nameInput.children[1].value,
        color: colorInput.children[2].value
      }
      if (todoId) data.todoId = todoId
      createCategory(data)
      self.close()
    })

    var checkInputValidation = function () {
      // check for input fields validation
      if (nameInput.children[1].value === '' || nameInput.children[1].classList.contains('auv-error')) {
        footerContinueBtn.disabled = true
        return
      }
      if (colorInput.children[2].value === '' || colorInput.children[2].classList.contains('auv-error')) {
        footerContinueBtn.disabled = true
        return
      }
      footerContinueBtn.disabled = false
    }

    footerBtnHolder.appendChild(footerCancelBtn)
    footerBtnHolder.appendChild(footerContinueBtn)
    footer.appendChild(footerBtnHolder)

    systemContainer.appendChild(componentSystem)
    systemContainer.appendChild(footer)
    parentContainer.appendChild(systemContainer)

    return parentContainer
  }

  var createCategory = function (data) {
    data.createdBy = c.currentUser._id
    data.engagementID = c.currentEngagement._id
    c.sendEvent({name: 'create-category', data: data})
  }

  initialize()
}

Modal_AddCategory.prototype = ModalTemplate.prototype
Modal_AddCategory.prototype.constructor = Modal_AddCategory

module.exports = Modal_AddCategory
