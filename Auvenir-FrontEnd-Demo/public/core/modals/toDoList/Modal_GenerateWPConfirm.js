﻿'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import styles from './Modal_Category.css'

var Modal_GenerateWPConfirm = function (ctrl, settings) {
	ModalTemplate.call(this, ctrl, settings)
	var c = ctrl
	var self = this
	var text = c.lang.categoryModal

	/**
	 * This function is responsible for initializing the creation of a new Client
	 * @memberof Modal_EditCategory
	 */
	var initialize = function () {
		log('Modal_GenerateWPConfirm: initialize')
		self.setModalName('generatewpconfirm')
		self.setClose({'window': true, 'image': 'images/icons/x-large.svg'})
		self.setFullScreen(false)
		self.setWidth('600px')
		self.setHeight('auto')
		self.setOverflow('auto')
		self.setHeader({ 'show': false, color: '#363A3C' })
		self.setTitle('')
		self.setPosition({ 'bottom': '0px'})
		self.setPadding('60px')
		self.build()
	}

	/**
	 * This function is responsible for building the page only when called.
	 * @memberof Modal_EditCategory
	 */
	this.buildDom = function () {
		log('Building Category Modal')

		var parentContainer = Forge.build({'dom': 'div', class: 'ce-parent todo-modal-container'})
		var systemContainer = Forge.build({'dom': 'div', 'class': 'ce-systemContainer'})

		var image = Forge.build({ 'dom': 'img', 'src': '../../images/wpconfirm.png', 'style': 'margin-bottom: 20px' });
		var description = Forge.build({ 'dom': 'div', 'text': "Are you sure you want to generate working papers for Flower Shop 2016? You've only collected 1 of 5 confirmation requests." });
		var question = Forge.build({ 'dom': 'div', 'text': 'Are you sure?', 'style': 'font-weight: bold; margin-bottom: 5px' });
		parentContainer.appendChild(image);
		parentContainer.appendChild(question);
		parentContainer.appendChild(description);

		var footer = Forge.build({'dom': 'div', 'class': 'ce-footer'})
		var footerBtnHolder = Forge.build({'dom': 'div', 'class': 'ce-footerBtnHolder category-footer'})
		var footerCancelBtn = Forge.buildBtn({
			'text': c.lang.cancel,
			'id': 'm-ce-cancelBtn',
			'type': 'secondary',
			'margin': '22px 10px 0px 0px'
		}).obj
		var footerContinueBtn = Forge.buildBtn({
			'text': 'Continue',
			'id': 'category-updateBtn',
			'type': 'primary'
		}).obj
		footerContinueBtn.disabled = false

		footerCancelBtn.addEventListener('click', function () {
			self.close()
		})

		footerContinueBtn.addEventListener('click', function () {
			log('Event: click update button')
            self.close();
            c.displayModalGenerateWPWaiting();
		})

		footerBtnHolder.appendChild(footerCancelBtn)
		footerBtnHolder.appendChild(footerContinueBtn)
		footer.appendChild(footerBtnHolder)

		systemContainer.appendChild(footer)
		parentContainer.appendChild(systemContainer)

		return parentContainer
	}
	initialize()
}

Modal_GenerateWPConfirm.prototype = ModalTemplate.prototype
Modal_GenerateWPConfirm.prototype.constructor = Modal_GenerateWPConfirm

module.exports = Modal_GenerateWPConfirm
