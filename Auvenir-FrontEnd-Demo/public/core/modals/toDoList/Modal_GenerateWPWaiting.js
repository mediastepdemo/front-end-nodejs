﻿'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import styles from './Modal_Category.css'
const COMMON = require('../../../js/src/common')

var Modal_GenerateWPWaiting = function (ctrl, settings) {
    ModalTemplate.call(this, ctrl, settings)
    var c = ctrl
    var self = this
    var text = c.lang.categoryModal

    /**
     * This function is responsible for initializing the creation of a new Client
     * @memberof Modal_Category
     */
    var initialize = function () {
        log('Modal_GenerateWPWaiting: initialize')
        self.setModalName('Modal_GenerateWPWaiting')
        self.setClose({ 'window': true, 'show': false, 'image': 'images/icons/x-large.svg' })
        self.setFullScreen(false)
        self.setWidth('600px')
        self.setHeight('auto')
        self.setOverflow('auto')
        self.setHeader({ 'show': false, color: '#363A3C' })
        self.setTitle('')
        self.setPosition({ 'bottom': '0px' })
        //self.setPadding('60px')
        self.build()
    }

    /*
     * Reset form
     */
    this.loadData = function () {
        log('ModalMarkComplete-loadData')
        var findCheckbox = document.getElementById('todo-table').querySelectorAll('tbody input[type="checkbox"]')
        var length = findCheckbox.length
        var name = []
        for (var i = 0; i < length; i++) {
            if (findCheckbox[i].checked) {
                var requests = findCheckbox[i].closest('tr').getAttribute('data-requests')
                if (requests === '0') {
                    name.push("'" + findCheckbox[i].closest('tr').querySelector('input.newTodoInput').value + "'")
                }
            }
        }
        var descriptionMark = text.markCompleteDes.replace('ITEM', name.join(', '))
        document.getElementById('des-mark-complete').innerText = descriptionMark
        log(descriptionMark)
    }

    /**
	 * This function is responsible for building the page only when called.
	 * @memberof Modal_EditCategory
	 */
    this.buildDom = function () {
        log('Building Category Modal')

        var parentContainer = Forge.build({ 'dom': 'div', class: 'ce-parent todo-modal-container' })
        var systemContainer = Forge.build({ 'dom': 'div', 'class': 'ce-systemContainer' })

        var image = Forge.build({ 'dom': 'img', 'src': '../../images/widgets/checklist.png', 'style': 'margin-bottom: 20px' });
        var description = Forge.build({ 'dom': 'div', 'text': "Just a moment while we generate your working papers.", 'style': 'margin-bottom: 20px' });
        var question = Forge.build({ 'dom': 'div', 'text': 'Generating working paper', 'style': 'font-weight: bold; margin-bottom: 5px' });
        var image2 = Forge.build({ 'dom': 'img', 'src': '../../images/loading/spinner.svg', 'style': 'margin-bottom: 20px' });
        parentContainer.appendChild(image);
        parentContainer.appendChild(question);
        parentContainer.appendChild(description);
        parentContainer.appendChild(image2);

        parentContainer.appendChild(systemContainer)

        return parentContainer
    }
    initialize()
}

Modal_GenerateWPWaiting.prototype = ModalTemplate.prototype
Modal_GenerateWPWaiting.prototype.constructor = Modal_GenerateWPWaiting

module.exports = Modal_GenerateWPWaiting
