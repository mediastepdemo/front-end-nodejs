'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import styles from './Modal_Category.css'
const COMMON = require('../../../js/src/common')

var Modal_DeleteToDo = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  var c = ctrl
  var self = this
  var text = c.lang.categoryModal

  /**
   * This function is responsible for initializing the creation of a new Client
   * @memberof Modal_Category
   */
  var initialize = function () {
    log('Modal_DeleteToDo: initialize')
    self.setModalName('Delete Todo Modal')
    self.setClose({'window': true, 'image': 'images/icons/x-large.svg'})
    self.setFullScreen(false)
    self.setWidth('480px')
    self.setHeight('auto')
    self.setOverflow('auto')
    self.setHeader({ 'show': true, color: '#363A3C' })
    self.setTitle(text.deleteToDo)
    self.setPosition({ 'bottom': '0px'})
    self.setPadding('0px')
    self.build()
  }

  /*
   * Reset form
   */
  this.loadData = function () {

  }

  /**
   * get data of all checkbox is checked
   */
  var getDataAllCheckbox = function () {
    var findCheckbox = document.getElementById('todo-table').querySelectorAll('tbody input[type="checkbox"]')
    var length = findCheckbox.length
    var data = []
    for (var i = 0; i < length; i++) {
      if (findCheckbox[i].checked) {
        data.push(findCheckbox[i].closest('tr').getAttribute('data-id'))
      }
    }
    log(data)
    return data
  }

  /**
   * This function is responsible for building the page only when called.
   * @memberof Modal_DeleteToDo
   */
  this.buildDom = function () {
    log('Building Category Modal')

    var parentContainer = Forge.build({'dom': 'div', 'id': 'DeleteTodoModal', class: 'ce-parent todo-modal-container'})
    var systemContainer = Forge.build({'dom': 'div', 'class': 'ce-systemContainer', 'id': 'm-ce-systemContainer'})
    var componentSystem = Forge.build({'dom': 'div', 'id': 'create-engagement-component-container', 'class': 'category-componentContainer'})

    var componentBody = Forge.build({ 'dom': 'p', 'id': 'setup-component-body', 'class': 'modal-body' })
    componentSystem.appendChild(componentBody)
    var divContainer = Forge.build({'dom': 'div', 'class': 'center'})
    componentBody.appendChild(divContainer)
    var img = Forge.build({'dom': 'img', 'src': '../../images/icons/clipboard-empty.png'})
    divContainer.appendChild(img)
    var divDes = Forge.build({'dom': 'div', 'text': text.deleteToDoDes, 'class': 'des-delete-modal'})
    divContainer.appendChild(divDes)

    var footer = Forge.build({'dom': 'div', 'class': 'ce-footer'})
    var footerBtnHolder = Forge.build({'dom': 'div'})
    var footerCancelBtn = Forge.buildBtn({
      'text': c.lang.cancel,
      'type': 'secondary',
      'margin': '22px 10px 0px 0px'
    }).obj
    var footerContinueBtn = Forge.buildBtn({
      'text': text.btnDelete,
      'type': 'warning'
    }).obj

    footerCancelBtn.addEventListener('click', function () {
      self.close()
    })

    footerContinueBtn.addEventListener('click', function () {
      log('Event: click category-addBtn')
      var data = getDataAllCheckbox()
      if (data.length > 0) {
        var json = {}
        json.type = COMMON.TODOS_BULK_UPDATE_TYPE.DELETE
        json.updateList = []
        for (var i = 0; i < data.length; i++) {
          json.updateList.push({todoId: data[i], value: COMMON.TODO_DELETE_STATUS_ENUM.INACTIVE})
        }
        log(json)
        c.bulkTodosUpdate(json)
      }
      self.close()
    })

    footerBtnHolder.appendChild(footerCancelBtn)
    footerBtnHolder.appendChild(footerContinueBtn)
    footer.appendChild(footerBtnHolder)
    componentBody.appendChild(footer)

    systemContainer.appendChild(componentSystem)
    parentContainer.appendChild(systemContainer)

    return parentContainer
  }

  initialize()
}

Modal_DeleteToDo.prototype = ModalTemplate.prototype
Modal_DeleteToDo.prototype.constructor = Modal_DeleteToDo

module.exports = Modal_DeleteToDo
