'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import styles from './Modal_Category.css'
const COMMON = require('../../../js/src/common')

var Modal_MarkComplete = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  var c = ctrl
  var self = this
  var text = c.lang.categoryModal

  /**
   * This function is responsible for initializing the creation of a new Client
   * @memberof Modal_Category
   */
  var initialize = function () {
    log('Modal_MarkComplete: initialize')
    self.setModalName('Mark As Complete')
    self.setClose({'window': true, 'image': 'images/icons/x-large.svg'})
    self.setFullScreen(false)
    self.setWidth('480px')
    self.setHeight('492px')
    self.setOverflow('auto')
    self.setHeader({ 'show': true, color: '#363A3C' })
    self.setTitle(text.markComplete)
    self.setPosition({ 'bottom': '0px'})
    self.setPadding('0px')
    self.build()
  }

  /*
   * Reset form
   */
  this.loadData = function () {
    log('ModalMarkComplete-loadData')
    var findCheckbox = document.getElementById('todo-table').querySelectorAll('tbody input[type="checkbox"]')
    var length = findCheckbox.length
    var name = []
    for (var i = 0; i < length; i++) {
      if (findCheckbox[i].checked) {
        var requests = findCheckbox[i].closest('tr').getAttribute('data-requests')
        if (requests === '0') {
          name.push("'" + findCheckbox[i].closest('tr').querySelector('input.newTodoInput').value + "'")
        }
      }
    }
    var descriptionMark = text.markCompleteDes.replace('ITEM', name.join(', '))
    document.getElementById('des-mark-complete').innerText = descriptionMark
    log(descriptionMark)
  }

  /**
   * get data of all checkbox is checked
   */
  var getDataAllCheckbox = function () {
    var findCheckbox = document.getElementById('todo-table').querySelectorAll('tbody input[type="checkbox"]')
    var length = findCheckbox.length
    var data = []
    for (var i = 0; i < length; i++) {
      if (findCheckbox[i].checked) {
        data.push(findCheckbox[i].closest('tr').getAttribute('data-id'))
      }
    }
    log(data)
    return data
  }

  /**
   * This function is responsible for building the page only when called.
   * @memberof Modal_MarkComplete
   */
  this.buildDom = function () {
    log('Building Category Modal')

    var parentContainer = Forge.build({'dom': 'div', class: 'ce-parent todo-modal-container'})
    var systemContainer = Forge.build({'dom': 'div', 'class': 'ce-systemContainer', 'id': 'm-ce-systemContainer'})
    var componentSystem = Forge.build({'dom': 'div', 'class': 'category-componentContainer'})

    var componentBody = Forge.build({'dom': 'p', 'class': 'modal-body'})
    componentSystem.appendChild(componentBody)

    var divContainer = Forge.build({'dom': 'div', 'class': 'center'})
    componentBody.appendChild(divContainer)

    var img = Forge.build({'dom': 'img', 'src': '../../images/icons/clipboard-yellow.png'})
    divContainer.appendChild(img)

    var divDes = Forge.build({'dom': 'div', 'id': 'des-mark-complete', 'text': text.markCompleteDes, 'class': 'des-delete-modal'})
    divContainer.appendChild(divDes)

    var footer = Forge.build({'dom': 'div', 'class': 'ce-footer'})
    var footerBtnHolder = Forge.build({'dom': 'div', 'class': 'ce-footerBtnHolder'})
    var footerCancelBtn = Forge.buildBtn({
      'text': c.lang.cancel,
      'type': 'light',
      'margin': 'auto 0 auto 8px'
    }).obj
    footerCancelBtn.style.display = 'inline-block'
    var footerContinueBtn = Forge.buildBtn({
      'text': text.btnArchive,
      'type': 'primary',
      'margin': 'auto'
    }).obj
    footerContinueBtn.style.display = 'inline-block'

    footerCancelBtn.addEventListener('click', function () {
      self.close()
    })

    footerContinueBtn.addEventListener('click', function () {
      log('Event: click mark-as-complete-addBtn')
      var data = getDataAllCheckbox()
      if (data.length > 0) {
        var json = {}
        json.type = COMMON.TODOS_BULK_UPDATE_TYPE.MARK_COMPLETE
        json.updateList = []
        for (var i = 0; i < data.length; i++) {
          let todoIndex = c.currentEngagement.todos.findIndex((todo) => {
            return todo._id.toString() === data[i].toString()
          })
          if (todoIndex !== -1) {
            let complete = c.currentEngagement.todos[todoIndex].complete
            if (complete) {
              continue
            }
            json.updateList.push({todoId: data[i], value: true})
          }
        }
        log(json)
        c.bulkTodosUpdate(json)
      }
      self.close()
    })

    footerBtnHolder.appendChild(footerContinueBtn)
    footerBtnHolder.appendChild(footerCancelBtn)
    footer.appendChild(footerBtnHolder)

    systemContainer.appendChild(componentSystem)
    systemContainer.appendChild(footer)
    parentContainer.appendChild(systemContainer)

    return parentContainer
  }

  initialize()
}

Modal_MarkComplete.prototype = ModalTemplate.prototype
Modal_MarkComplete.prototype.constructor = Modal_MarkComplete

module.exports = Modal_MarkComplete
