'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import styles from './Modal_Mobile.css'

var Modal_Mobile = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  var c = ctrl
  var self = this
  var firstName = ''
  var lastName = ''

  var initialize = function () {
    log('Modal_Mobile: initialize')

    self.setModalName('mob')
    self.setClose({'window': true})
    self.setHeader({'show': false})
    self.setPosition({ 'bottom': '0px'})
    self.setFullScreen(false)
    self.setBackground('#ffffff')
    self.build()
    self.setName('Julianna', 'Slocum')
    self.setDevice({ type: 'Samsung 5', id: '9823759352385923u5923u592'})
  }

  this.buildDom = function () {
    var logo = Forge.build({'dom': 'img', 'src': 'images/logos/auvenir/logo_auvenir.svg', 'alt': 'Auvenir', 'style': 'position:absolute; top:42px; left:64px'})
    var chevron = Forge.build({'dom': 'img', 'src': 'images/icons/chevron.svg', 'style': 'position:absolute; top:50px; right:82px'})
    var name = Forge.build({'dom': 'p', 'id': 'm-mob-' + self._uniqueID + '-name', 'class': 'modal-mobile-name'})

    var container = Forge.build({'dom': 'div', 'class': 'modal-mobile-content'})

    var phoneContainer = Forge.build({'dom': 'div', 'id': 'm-mob-' + self._uniqueID + '-phone-container', 'style': 'position:absolute;top:0px;right:0px;display:none'})
    var phoneImg = Forge.build({'dom': 'img', 'src': 'images/illustrations/phone.svg'})
    var phoneFullname = Forge.build({'dom': 'p', 'id': 'm-mob-' + self._uniqueID + '-phone-fullname', 'class': 'modal-mobile-phone-title'})
    var phoneDevice = Forge.build({'dom': 'p', 'id': 'm-mob-' + self._uniqueID + '-phone-device', 'class': 'modal-mobile-phone-device'})
    var phoneIcon = Forge.build({'dom': 'img', 'id': 'm-mob-' + self._uniqueID + '-phone-icon', 'class': 'modal-mobile-phone-icon'})
    var phoneID = Forge.build({'dom': 'p', 'id': 'm-mob-' + self._uniqueID + '-phone-uid', 'class': 'modal-mobile-phone-uid'})
    phoneContainer.appendChild(phoneImg)
    phoneContainer.appendChild(phoneFullname)
    phoneContainer.appendChild(phoneDevice)
    phoneContainer.appendChild(phoneIcon)
    phoneContainer.appendChild(phoneID)

    // if in login
    var phoneContainerLogin = Forge.build({'dom': 'div', 'id': 'm-mob-' + self._uniqueID + '-phone-containerLogin', 'style': 'position:absolute;top:0px;right:0px;display:none'})
    var phoneImgLogin = Forge.build({'dom': 'img', 'src': 'images/modal_mobile/20-layers.png'})
    phoneContainerLogin.appendChild(phoneImgLogin)

    var peopleContainer = Forge.build({'dom': 'div', 'id': 'm-mob-' + self._uniqueID + '-people-container', 'style': 'position:absolute;top:0px;right:0px;display:none'})
    var peopleImg = Forge.build({'dom': 'img', 'src': 'images/modal_mobile/people.png'})
    peopleContainer.appendChild(peopleImg)

    var cardsContainer = Forge.build({'dom': 'div', 'style': 'position:absolute;top:147px;left:0px'})

    /** CHECK DEVICE **/
    var CHECKDEVICE_card = Forge.build({'dom': 'div', 'id': 'm-mob-' + self._uniqueID + '-card-checkdevice', 'style': 'text-align:left;width:488px; display:none'})
    var CHECKDEVICE_title = Forge.build({'dom': 'p', 'text': 'Check your device', 'class': 'modal-mobile-card-title'})
    var CHECKDEVICE_desc = Forge.build({'dom': 'p', 'class': 'modal-mobile-card-description', 'text': 'We just sent a push notification to your device.<br>Please swipe the notification to login to Auvenir on your desktop.'})
    var CHECKDEVICE_btnDiv = Forge.build({'dom': 'div'})
    var CHECKDEVICE_troubleshootBtn = Forge.build({'dom': 'input', 'type': 'button', 'class': 'btn modal-mobile-grayBtn', 'value': 'Troubleshoot'})
    CHECKDEVICE_btnDiv.appendChild(CHECKDEVICE_troubleshootBtn)
    CHECKDEVICE_card.appendChild(CHECKDEVICE_title)
    CHECKDEVICE_card.appendChild(CHECKDEVICE_desc)
    // CHECKDEVICE_card.appendChild(CHECKDEVICE_desc2);
    CHECKDEVICE_card.appendChild(CHECKDEVICE_btnDiv)
    CHECKDEVICE_troubleshootBtn.addEventListener('click', function () { self.setState('login_trouble') })

    /** LOGIN TROUBLE **/
    var LOGINTROUBLE_card = Forge.build({'dom': 'div', 'id': 'm-mob-' + self._uniqueID + '-card-logintrouble', 'style': 'text-align:left;width:488px; display:none'})
    var LOGINTROUBLE_title = Forge.build({'dom': 'p', 'text': 'Having trouble logging in?', 'class': 'modal-mobile-card-title-login', 'style': 'width:271px; height:29px; color:black; font-family:Lato; font-size:24px; font-weight:400px; line-height:29px;'})

    var LOGINTROUBLE_descContainer = Forge.build({'dom': 'ul', 'id': 'container-mehul', 'style': 'width:100%, list-style-type: none, padding-left:20px, margin:0px'})

    var troubleshoot1LI = Forge.build({'dom': 'li', 'class': 'a-mobile-tr', 'style': 'list-style-type: none;'})
    var troubleshoot1UL = Forge.build({'dom': 'ul', 'class': 'request-toggleTable request-subTable', 'style': 'width:100%'})
    var troubleshoot2LI = Forge.build({'dom': 'li', 'class': 'a-mobile-tr', 'style': 'list-style-type: none;'})
    var troubleshoot2UL = Forge.build({'dom': 'ul', 'class': 'request-toggleTable request-subTable', 'style': 'width:100%'})
    var troubleshoot3LI = Forge.build({'dom': 'li', 'class': 'a-mobile-tr', 'style': 'list-style-type: none;'})
    var troubleshoot3UL = Forge.build({'dom': 'ul', 'class': 'request-toggleTable request-subTable', 'style': 'width:100%'})

    var LOGINTROUBLE_choice1Div = Forge.build({'dom': 'div', 'class': 'modal-mobile-card-description-login', 'style': 'width:300px; height:35px; padding-bottom:5px;'})
    var LOGINTROUBLE_choice1Radio = Forge.build({'dom': 'input', 'id': 'm-mob-logintrouble-choice-1', 'type': 'radio', 'name': 'radioResp', 'style': 'display:inline-block;'})

    var LOGINTROUBLE_choice1Content = Forge.build({'dom': 'div', 'class': 'modal-mobile-card-description-in', 'text': 'I don\'t have access to my device.', 'style': 'display:inline-block; padding:12px;' })
    var LOGINTROUBLE_choice1ErrorContainer = Forge.build({'dom': 'div', 'id': 'modal-mobile-choice-chosen', 'style': 'padding-left:20px' })// 'style':'display: none'
    var LOGINTROUBLE_choice1ErrorContent = Forge.build({'dom': 'p', 'class': 'modal-mobile-card-description-in', 'text': 'To gain access without your device, please enter the email address you use to login to Auvenir. We will send you a login token to that email address.', 'style': '' })
    var LOGINTROUBLE_choice1ErrorEmailContainer = Forge.build({'dom': 'div', 'class': '', 'text': 'Email Address', 'style': 'width:240px' })
    var LOGINTROUBLE_choice1ErrorEmailInput = Forge.build({'dom': 'input', 'type': 'text', 'placeholder': 'Type your email', 'style': 'display: inline-block'})
    var LOGINTROUBLE_choice1ErrorEmailSubmit = Forge.build({'dom': 'input', 'type': 'button', 'value': 'Send', 'style': 'display: inline-block'})
    LOGINTROUBLE_choice1Div.appendChild(LOGINTROUBLE_choice1Radio)
    LOGINTROUBLE_choice1Div.appendChild(LOGINTROUBLE_choice1Content)
    LOGINTROUBLE_choice1Div.appendChild(LOGINTROUBLE_choice1ErrorContainer)
    LOGINTROUBLE_choice1ErrorEmailContainer.appendChild(LOGINTROUBLE_choice1ErrorEmailInput)
    LOGINTROUBLE_choice1ErrorEmailContainer.appendChild(LOGINTROUBLE_choice1ErrorEmailSubmit)
    LOGINTROUBLE_choice1ErrorContainer.appendChild(LOGINTROUBLE_choice1ErrorContent)
    LOGINTROUBLE_choice1ErrorContainer.appendChild(LOGINTROUBLE_choice1ErrorEmailContainer)
    LOGINTROUBLE_choice1Radio.addEventListener('click', function () {
      $(troubleshoot1UL).toggleClass('request-toggleTable')
      if (!($(troubleshoot2UL).hasClass('request-toggleTable'))) {
        $(troubleshoot2UL).toggleClass('request-toggleTable')
      }
      if (!($(troubleshoot3UL).hasClass('request-toggleTable'))) {
        $(troubleshoot3UL).toggleClass('request-toggleTable')
      }
    })

    var LOGINTROUBLE_choice2Div = Forge.build({'dom': 'div', 'class': 'modal-mobile-card-description-login', 'style': 'width:300px; height:35px;padding-bottom:5px;'})
    var LOGINTROUBLE_choice2Radio = Forge.build({'dom': 'input', 'id': 'm-mob-logintrouble-choice-2', 'type': 'radio', 'name': 'radioResp', 'style': 'display:inline-block;'})
    var LOGINTROUBLE_choice2Content = Forge.build({'dom': 'div', 'class': 'modal-mobile-card-description-in', 'text': 'I replaced my old device with a new one.', 'style': 'display:inline-block;  padding:12px;' })
    var LOGINTROUBLE_choice2ErrorContainer = Forge.build({'dom': 'div', 'id': 'modal-mobile-choice-chosen', 'style': 'padding-left:20px' })// 'style':'display: none'
    var LOGINTROUBLE_choice2ErrorContent = Forge.build({'dom': 'p', 'class': 'modal-mobile-card-description-in', 'text': 'To register your new device with Auvenir, please start by downloading the mobile application. Type your mobile number below and we can text you a download link.', 'style': '' })
    // Link Texting SMS Integration
    var LTWidgetContainer = Forge.build({'dom': 'div', 'class': 'linkTextingWidget', 'text': 'New Device Phone Number', 'style': 'margin: 0px; display:inline-block;'})
    var LTWidgetInnerContainer = Forge.build({'dom': 'div', 'class': 'linkTextingInner', 'style': 'display:inline-block; position:absolute; left:0px'})
    var LTWidgetInputContainer = Forge.build({'dom': 'input', 'class': 'linkID', 'type': 'hidden', 'value': '0258cde2-3a86-4d28-95bb-d14994314fe4'})
    var LTWidgetInputWrapper = Forge.build({'dom': 'div', 'class': 'linkTextingInputWrapper'})
    var LTWidgetInput = Forge.build({'dom': 'input', 'id': 'numberToText_linkTexting', 'class': 'linkTextingInput', 'type': 'tel', 'value': ''})
    var LTWidgetButton = Forge.build({'dom': 'button', 'id': 'sendButton_linkTexting', 'class': 'linkTextingButton', 'type': 'button', 'text': 'Text me a Link'})
    var LTWidgetError = Forge.build({'dom': 'div', 'id': 'linkTextingError', 'class': 'linkTextingError' })// 'style':'display: none;'
    var LTSmsSuccessContainer = Forge.build({'dom': 'div', 'style': 'display: inline-block; width: 200px; position:absolute; top:250px;'})
    var LTSmsSuccessCheckmark = Forge.build({'dom': 'img', 'id': '', 'src': 'images/icons/checkActive.svg', 'class': '', 'style': 'display: inline-block; margin: 10px; margin-left:20px;'})
    var LTSmsSuccessText = Forge.build({'dom': 'div', 'class': '', 'id': '', 'text': 'Sent to your device', 'style': 'display: inline-block; color: #599ba1; width:134px; height:19px;'})
    var linkContainer = Forge.build({'dom': 'div', 'class': 'onboard-appLink'})
    var linkMac = Forge.build({'dom': 'img', 'src': 'images/components/macApp.png', 'style': ''})
    var linkGoogle = Forge.build({'dom': 'img', 'src': 'images/components/googleApp.png', 'style': 'float:right'})
    LTWidgetContainer.appendChild(LTWidgetInnerContainer)
    LTWidgetInnerContainer.appendChild(LTWidgetInputContainer)
    LTWidgetInnerContainer.appendChild(LTWidgetInputWrapper)
    LTWidgetInputWrapper.appendChild(LTWidgetButton)
    LTWidgetInnerContainer.appendChild(LTWidgetError)
    LTWidgetInputWrapper.appendChild(LTWidgetInput)
    LTSmsSuccessContainer.appendChild(LTSmsSuccessCheckmark)
    LTSmsSuccessContainer.appendChild(LTSmsSuccessText)
    linkContainer.appendChild(linkMac)
    linkContainer.appendChild(linkGoogle)

    LOGINTROUBLE_choice2Div.appendChild(LOGINTROUBLE_choice2Radio)
    LOGINTROUBLE_choice2Div.appendChild(LOGINTROUBLE_choice2Content)
    LOGINTROUBLE_choice2Div.appendChild(LOGINTROUBLE_choice2ErrorContainer)
    LOGINTROUBLE_choice2ErrorContainer.appendChild(LOGINTROUBLE_choice2ErrorContent)
    LOGINTROUBLE_choice2ErrorContainer.appendChild(LTWidgetContainer)
    LOGINTROUBLE_choice2ErrorContainer.appendChild(linkContainer)
    LOGINTROUBLE_choice2Radio.addEventListener('click', function () {
      $(troubleshoot2UL).toggleClass('request-toggleTable')
      if (!($(troubleshoot1UL).hasClass('request-toggleTable'))) {
        $(troubleshoot1UL).toggleClass('request-toggleTable')
      }
      if (!($(troubleshoot3UL).hasClass('request-toggleTable'))) {
        $(troubleshoot3UL).toggleClass('request-toggleTable')
      }
    })
    // TO DO - check if the text is actually sent successfully.
    // if yes, then append
    // LTWidgetContainer.appendChild(LTSmsSuccessContainer);

    var LOGINTROUBLE_choice3Div = Forge.build({'dom': 'div', 'class': 'modal-mobile-card-description-login', 'style': 'width:300px; height:35px;padding-bottom:5px;'})
    var LOGINTROUBLE_choice3Radio = Forge.build({'dom': 'input', 'id': 'm-mob-logintrouble-choice-3', 'type': 'radio', 'name': 'radioResp', 'style': 'display:inline-block;'})
    var LOGINTROUBLE_choice3Content = Forge.build({'dom': 'div', 'class': 'modal-mobile-card-description-in', 'text': 'I need to switch accounts.', 'style': 'display:inline-block; padding:12px;' })
    var LOGINTROUBLE_choice3ErrorContainer = Forge.build({'dom': 'div', 'id': 'modal-mobile-choice-chosen', 'style': 'padding-left:20px; padding-top:20px;' }) // 'style':'display: none'
    var LOGINTROUBLE_choice3ErrorContent = Forge.build({'dom': 'p', 'class': 'modal-mobile-card-description-in', 'text': 'To gain access as a different account holder, please enter your registered email address below. We will send you a login token to the email address provided.', 'style': '' })
    var LOGINTROUBLE_choice3ErrorEmailContainer = Forge.build({'dom': 'div', 'class': '', 'text': 'Email Address', 'style': 'width:240px' })
    var LOGINTROUBLE_choice3ErrorEmailInput = Forge.build({'dom': 'input', 'type': 'text', 'placeholder': 'Type your email', 'style': 'display: inline-block'})
    var LOGINTROUBLE_choice3ErrorEmailSubmit = Forge.build({'dom': 'input', 'type': 'button', 'value': 'Send', 'style': 'display: inline-block'})
    var LOGINTROUBLE_choice3ErrorRetry = Forge.build({'dom': 'input', 'type': 'button', 'value': 'Send', 'style': 'display: inline-block'})
    var LOGINTROUBLE_choice3ErrorGetHelp = Forge.build({'dom': 'input', 'type': 'button', 'value': 'Send', 'style': 'display: inline-block'})
    LOGINTROUBLE_choice3Div.appendChild(LOGINTROUBLE_choice3Radio)
    LOGINTROUBLE_choice3Div.appendChild(LOGINTROUBLE_choice3Content)
    LOGINTROUBLE_choice3Div.appendChild(LOGINTROUBLE_choice3ErrorContainer)
    LOGINTROUBLE_choice3ErrorEmailContainer.appendChild(LOGINTROUBLE_choice3ErrorEmailInput)
    LOGINTROUBLE_choice3ErrorEmailContainer.appendChild(LOGINTROUBLE_choice3ErrorEmailSubmit)
    LOGINTROUBLE_choice3ErrorContainer.appendChild(LOGINTROUBLE_choice3ErrorContent)
    LOGINTROUBLE_choice3ErrorContainer.appendChild(LOGINTROUBLE_choice3ErrorEmailContainer)
    LOGINTROUBLE_choice3Radio.addEventListener('click', function () {
      $(troubleshoot3UL).toggleClass('request-toggleTable')
      if (!($(troubleshoot1UL).hasClass('request-toggleTable'))) {
        $(troubleshoot1UL).toggleClass('request-toggleTable')
      }
      if (!($(troubleshoot2UL).hasClass('request-toggleTable'))) {
        $(troubleshoot2UL).toggleClass('request-toggleTable')
      }
    })

    var LOGINTROUBLE_btnDiv = Forge.build({'dom': 'div'})
    var LOGINTROUBLE_cancelBtn = Forge.build({'dom': 'input', 'type': 'button', 'class': 'btn modal-mobile-grayBtn btn modal-mobile-grayBtn-login ', 'value': 'Cancel', 'style': 'margin-top:20px; height:40px; width:122px; padding:0px'})
    LOGINTROUBLE_choice3Div.appendChild(LOGINTROUBLE_choice3Radio)
    LOGINTROUBLE_choice3Div.appendChild(LOGINTROUBLE_choice3Content)

    troubleshoot1LI.appendChild(LOGINTROUBLE_choice1Div)
    troubleshoot2LI.appendChild(LOGINTROUBLE_choice2Div)
    troubleshoot3LI.appendChild(LOGINTROUBLE_choice3Div)

    troubleshoot1UL.appendChild(LOGINTROUBLE_choice1ErrorContainer)
    troubleshoot2UL.appendChild(LOGINTROUBLE_choice2ErrorContainer)
    troubleshoot3UL.appendChild(LOGINTROUBLE_choice3ErrorContainer)

    LOGINTROUBLE_descContainer.appendChild(troubleshoot1LI)
    LOGINTROUBLE_descContainer.appendChild(troubleshoot1UL)
    LOGINTROUBLE_descContainer.appendChild(troubleshoot2LI)
    LOGINTROUBLE_descContainer.appendChild(troubleshoot2UL)
    LOGINTROUBLE_descContainer.appendChild(troubleshoot3LI)
    LOGINTROUBLE_descContainer.appendChild(troubleshoot3UL)
    LOGINTROUBLE_btnDiv.appendChild(LOGINTROUBLE_cancelBtn)
    LOGINTROUBLE_card.appendChild(LOGINTROUBLE_title)
    LOGINTROUBLE_card.appendChild(LOGINTROUBLE_descContainer)
    LOGINTROUBLE_card.appendChild(LOGINTROUBLE_btnDiv)
    LOGINTROUBLE_cancelBtn.addEventListener('click', function () { self.setState('login_trouble') })

    /** CONFIRM **/
    var CONFIRM_card = Forge.build({'dom': 'div', 'id': 'm-mob-' + self._uniqueID + '-card-confirm', 'style': 'text-align:left;width:488px; display:none'})
    var CONFIRM_title = Forge.build({'dom': 'p', 'text': 'Confirm your device', 'class': 'modal-mobile-card-title'})
    var CONFIRM_desc = Forge.build({'dom': 'p', 'class': 'modal-mobile-card-description', 'text': 'Please confirm that the device name to the right is yours.  You can register more than one device in settings once you are logged in.'})
    var CONFIRM_btnDiv = Forge.build({'dom': 'div'})
    var CONFIRM_nBtn = Forge.build({'dom': 'input', 'type': 'button', 'class': 'btn modal-mobile-grayBtn', 'value': 'Not My Phone'})
    var CONFIRM_yBtn = Forge.build({'dom': 'input', 'type': 'button', 'class': 'btn modal-mobile-greenBtn', 'value': 'Yes, That\'s Mine'})
    CONFIRM_btnDiv.appendChild(CONFIRM_nBtn)
    CONFIRM_btnDiv.appendChild(CONFIRM_yBtn)
    CONFIRM_card.appendChild(CONFIRM_title)
    CONFIRM_card.appendChild(CONFIRM_desc)
    CONFIRM_card.appendChild(CONFIRM_btnDiv)
    CONFIRM_nBtn.addEventListener('click', function () { self.setState('notify') })
    CONFIRM_yBtn.addEventListener('click', function () { self.setState('register') })

    /** NOTIFY **/
    var NOTIFY_card = Forge.build({'dom': 'div', 'id': 'm-mob-' + self._uniqueID + '-card-notify', 'style': 'text-align:left;width:488px; display:none'})
    var NOTIFY_title = Forge.build({'dom': 'p', 'text': 'Notify Auvenir', 'class': 'modal-mobile-card-title'})
    var NOTIFY_desc = Forge.build({'dom': 'p', 'class': 'modal-mobile-card-description', 'text': 'Would you like to notify Auvenir of a potential security threat?'})
    var NOTIFY_btnDiv = Forge.build({'dom': 'div'})
    var NOTIFY_nBtn = Forge.build({'dom': 'input', 'type': 'button', 'class': 'btn modal-mobile-grayBtn', 'value': 'Cancel'})
    var NOTIFY_yBtn = Forge.build({'dom': 'input', 'type': 'button', 'class': 'btn modal-mobile-greenBtn', 'value': 'Notify'})
    NOTIFY_btnDiv.appendChild(NOTIFY_nBtn)
    NOTIFY_btnDiv.appendChild(NOTIFY_yBtn)
    NOTIFY_card.appendChild(NOTIFY_title)
    NOTIFY_card.appendChild(NOTIFY_desc)
    NOTIFY_card.appendChild(NOTIFY_btnDiv)
    NOTIFY_nBtn.addEventListener('click', function () { self.setState('confirm') })
    NOTIFY_yBtn.addEventListener('click', function () { self.setState('notify-confirm') })

    /** NOTIFY CONFIRMED **/
    var NOTIFY_C_card = Forge.build({'dom': 'div', 'id': 'm-mob-' + self._uniqueID + '-card-notify-confirm', 'style': 'text-align:left;width:488px; display:none'})
    var NOTIFY_C_title = Forge.build({'dom': 'p', 'text': 'Thanks!', 'class': 'modal-mobile-card-title'})
    var NOTIFY_C_desc = Forge.build({'dom': 'p', 'class': 'modal-mobile-card-description', 'text': 'We sent a live report to our security team. Eugene, Jon, and Matt are on it! We\'ll be in touch soon.'})
    var NOTIFY_C_btnDiv = Forge.build({'dom': 'div'})
    var NOTIFY_C_Btn = Forge.build({'dom': 'input', 'type': 'button', 'class': 'btn modal-mobile-greenBtn', 'value': 'Return to Auvenir'})
    NOTIFY_C_btnDiv.appendChild(NOTIFY_C_Btn)
    NOTIFY_C_card.appendChild(NOTIFY_C_title)
    NOTIFY_C_card.appendChild(NOTIFY_C_desc)
    NOTIFY_C_card.appendChild(NOTIFY_C_btnDiv)
    NOTIFY_C_Btn.addEventListener('click', function () { self.close() })

    /** REGISTERING **/
    var REGISTER_card = Forge.build({'dom': 'div', 'id': 'm-mob-' + self._uniqueID + '-card-register', 'style': 'text-align:left;width:488px; display:none'})
    var REGISTER_title = Forge.build({'dom': 'p', 'text': 'Registering ...', 'class': 'modal-mobile-card-title'})
    REGISTER_card.appendChild(REGISTER_title)

    /** TIMEOUT **/
    var TIMEOUT_card = Forge.build({'dom': 'div', 'id': 'm-mob-' + self._uniqueID + '-card-timeout', 'style': 'text-align:left;width:488px; display:none'})
    var TIMEOUT_title = Forge.build({'dom': 'p', 'text': 'We\'re sorry, the server has timed out.', 'class': 'modal-mobile-card-title'})
    var TIMEOUT_btnDiv = Forge.build({'dom': 'div', 'style': 'margin-bottom:24px;'})
    var TIMEOUT_Btn = Forge.build({'dom': 'input', 'type': 'button', 'class': 'btn modal-mobile-greenBtn', 'value': 'Try Again'})
    var TIMEOUT_subContainer = Forge.build({'dom': 'div', 'style': 'margin-top: 16px; margin-left:4px;width:488px'})
    var TIMEOUT_sub1 = Forge.build({'dom': 'label', 'text': 'If this persists, please'})
    var TIMEOUT_sutroubleshoot2UL = Forge.build({'dom': 'label', 'text': 'notify Auvenir', 'class': 'modal-mobile-sub-link'})
    TIMEOUT_btnDiv.appendChild(TIMEOUT_Btn)
    TIMEOUT_card.appendChild(TIMEOUT_title)
    TIMEOUT_card.appendChild(TIMEOUT_btnDiv)
    TIMEOUT_subContainer.appendChild(TIMEOUT_sub1)
    TIMEOUT_subContainer.appendChild(TIMEOUT_sutroubleshoot2UL)
    TIMEOUT_card.appendChild(TIMEOUT_subContainer)
    TIMEOUT_Btn.addEventListener('click', function () { self.setState('register') })
    TIMEOUT_sutroubleshoot2UL.addEventListener('click', function () { self.setState('register') })

    var COMPLETE_card = Forge.build({'dom': 'div', 'id': 'm-mob-' + self._uniqueID + '-card-complete', 'style': 'text-align:left;width:488px: display:none'})

    cardsContainer.appendChild(CHECKDEVICE_card)
    cardsContainer.appendChild(LOGINTROUBLE_card)
    cardsContainer.appendChild(CONFIRM_card)
    cardsContainer.appendChild(NOTIFY_card)
    cardsContainer.appendChild(NOTIFY_C_card)
    cardsContainer.appendChild(REGISTER_card)
    cardsContainer.appendChild(TIMEOUT_card)
    cardsContainer.appendChild(COMPLETE_card)

    // var content = Forge.build({'dom':'div', 'class':'terms-content'});
    // var p1 = Forge.build({'dom':'p', 'class':'terms-paragraph', 'text': "Hella narwhal Cosby sweater McSweeney's, salvia kitsch before they sold out High Life. Umami tattooed sriracha meggings pickled Marfa Blue Bottle High Life next level four loko PBR. Keytar pickled next level keffiyeh drinking vinegar street art."});
    // var p2 = Forge.build({'dom':'p', 'class':'terms-paragraph', 'text': "Kogi Cosby sweater ethical squid irony disrupt, organic tote bag gluten-free XOXO wolf typewriter mixtape small batch. DIY pickled four loko McSweeney's, Odd Future dreamcatcher plaid. PBR&B single-origin coffee gluten-free McSweeney's banjo, bicycle rights food truck gastropub vinyl four loko umami +1 narwhal chia. Fashion axe Banksy chia umami artisan, bitters 90's fanny pack. Single-origin coffee drinking vinegar blog semiotics. Fap mumblecore church-key PBR&B, direct trade 8-bit swag asymmetrical slow-carb Marfa ethical. Mustache retro four loko, church-key Thundercats Portland Helvetica Cosby sweater Echo Park sartorial 8-bit ugh bicycle rights iPhone tote bag."});
    // var p3 = Forge.build({'dom':'p', 'class':'terms-paragraph', 'text': "Hella narwhal Cosby sweater McSweeney's, salvia kitsch before they sold out High Life. Umami tattooed sriracha meggings pickled Marfa Blue Bottle High Life next level four loko PBR. Keytar pickled next level keffiyeh drinking vinegar street art."});
    // var p4 = Forge.build({'dom':'p', 'class':'terms-paragraph', 'text': "Hella narwhal Cosby sweater McSweeney's, salvia kitsch before they sold out High Life. Umami tattooed sriracha meggings pickled Marfa Blue Bottle High Life next level four loko PBR. Keytar pickled next level keffiyeh drinking vinegar street art."});

    // content.appendChild(p1);
    // content.appendChild(p2);
    // content.appendChild(p3);
    // content.appendChild(p4);

    container.appendChild(logo)
    container.appendChild(name)
    container.appendChild(chevron)
    container.appendChild(phoneContainer)
    container.appendChild(phoneContainerLogin)
    container.appendChild(peopleContainer)
    container.appendChild(cardsContainer)
    return container
  }

  this.setName = function (firstName, lastName) {
    document.getElementById('m-mob-' + self._uniqueID + '-name').innerHTML = firstName
    document.getElementById('m-mob-' + self._uniqueID + '-phone-fullname').innerHTML = firstName + ' ' + lastName + '\'s'
  }

  /**
   * Type, ID
   */
  this.setDevice = function (json) {
    document.getElementById('m-mob-' + self._uniqueID + '-phone-device').innerHTML = json.type
    document.getElementById('m-mob-' + self._uniqueID + '-phone-uid').innerHTML = 'UDID: ' + json.id
  }

  this.setState = function (str) {
    switch (str) {
      case 'check-device':
        document.getElementById('m-mob-' + self._uniqueID + '-card-checkdevice').style.display = 'inherit'
        document.getElementById('m-mob-' + self._uniqueID + '-card-logintrouble').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-card-confirm').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-card-notify').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-card-notify-confirm').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-card-register').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-card-timeout').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-card-complete').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-phone-icon').src = 'images/modal_mobile/confirm.svg'

        document.getElementById('m-mob-' + self._uniqueID + '-phone-container').style.display = 'inherit'
        document.getElementById('m-mob-' + self._uniqueID + '-phone-containerLogin').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-people-container').style.top = '0px'
        document.getElementById('m-mob-' + self._uniqueID + '-people-container').style.display = 'none'
        break
      case 'login-trouble':
        document.getElementById('m-mob-' + self._uniqueID + '-card-checkdevice').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-card-logintrouble').style.display = 'inherit'
        document.getElementById('m-mob-' + self._uniqueID + '-card-confirm').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-card-notify').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-card-notify-confirm').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-card-register').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-card-timeout').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-card-complete').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-phone-icon').src = 'images/modal_mobile/confirm.svg'

        document.getElementById('m-mob-' + self._uniqueID + '-phone-container').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-phone-containerLogin').style.display = 'inherit'
        document.getElementById('m-mob-' + self._uniqueID + '-people-container').style.top = '0px'
        document.getElementById('m-mob-' + self._uniqueID + '-people-container').style.display = 'none'

        break
      case 'complete':
        document.getElementById('m-mob-' + self._uniqueID + '-card-checkdevice').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-card-logintrouble').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-card-confirm').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-card-notify').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-card-notify-confirm').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-card-register').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-card-timeout').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-card-complete').style.display = 'inherit'
        document.getElementById('m-mob-' + self._uniqueID + '-phone-icon').src = 'images/modal_mobile/complete.svg'

        document.getElementById('m-mob-' + self._uniqueID + '-phone-container').style.display = 'inherit'
        document.getElementById('m-mob-' + self._uniqueID + '-phone-containerLogin').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-people-container').style.top = '0px'
        document.getElementById('m-mob-' + self._uniqueID + '-people-container').style.display = 'none'
        break
      default:
    }
  }

  initialize()
}

Modal_Mobile.prototype = ModalTemplate.prototype
Modal_Mobile.prototype.constructor = Modal_Mobile

module.exports = Modal_Mobile
