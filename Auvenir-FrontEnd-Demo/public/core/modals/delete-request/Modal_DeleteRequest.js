'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import styles from './modal_deleterequest.css'
/**
 * The modal is responsible for deleting the request for that user.
 * It is modal that prompts the user about deletion of request.
 * @class Modal_DeleteRequest
 * @param: {Object} ctrl     - appController for the current user
 * @param: {Object} settings - settings of the current user
 */
var Modal_DeleteRequest = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  var self = this

  /**
   * This function is responsible for building the page only when called
   * @memberof Modal_DeleteRequest
   */
  this.buildDom = function () {
    var container = Forge.build({ dom: 'div', id: 'delete-request-modal-container', class: 'delete-request-modal-container' })
    container.appendChild(Forge.build({ dom: 'div', text: '', class: 'delete-request-modal-tip', id: 'delete-request-modal-tip' }))

    var cancelBtn = Forge.buildBtn({text: 'Cancel', type: 'light', width: '128px', margin: '4px'}).obj

    cancelBtn.addEventListener('click', function (event) {
      event.preventDefault()
      self.close()
    })

    container.appendChild(cancelBtn)

    var confirmBtn = Forge.buildBtn({text: 'Delete', type: 'warning', width: '128px', margin: '4px'}).obj

    confirmBtn.addEventListener('click', function (event) {
      event.preventDefault()
      self.trigger('update-request', {_id: self.request._id, status: 'INACTIVE'})

      self.close()
    })
    container.appendChild(confirmBtn)
    return container
  }

  self.setModalName('Delete Request')
  self.setClose({window: true, global: false})
  self.setWidth('480px')
  self.setHeight('412px')
  self.setTitle('Delete Request')
  self.setHeader({show: true, color: '#363A3C', icon: 'warning'})
  self.setPosition({top: '100px'})
  self.build()

  /**
   * This function is responsible for loading the text with current request name.
   * @memberof Modal_DeleteRequest
   */
  this.loadData = function (request) {
    var templateText = 'Are you sure you would like to delete</br>'
    templateText += self.request.name + ' ? '
    templateText += 'You will lose all sub-requests as well.'
    document.getElementById('delete-request-modal-tip').innerHTML = templateText
  }
}

module.exports = Modal_DeleteRequest
