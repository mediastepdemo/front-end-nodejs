'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import styles from './Modal_DeleteFile.css'
/**
 * The modal is responsible for deleting the request for that user.
 * It is modal that prompts the user about deletion of request.
 * @class Modal_DeleteFile
 * @param: {Object} ctrl     - appController for the current user
 * @param: {Object} settings - settings of the current user
 */
var Modal_DeleteFile = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  var self = this
  var c = ctrl
  var text = c.lang.deleteFileModal
  /**
   * This function is responsible for building the page only when called
   * @memberof Modal_DeleteFile
   */
  this.buildDom = function () {
    var container = Forge.build({ dom: 'div', id: 'delete-file-modal-container'})
    // container.appendChild(Forge.build({ dom: 'div', text: '', class: 'delete-request-modal-tip', id: 'delete-request-modal-tip' }))
    var folderImg = Forge.build({ dom: 'img', src: '../../../images/delete-file-folder.svg', class: 'm-df-img'})
    var subText = Forge.build({ dom: 'div', text: text.subText, class: 'm-df-div'})

    var cancelBtn = Forge.buildBtn({text: 'Cancel', type: 'light', width: '128px', margin: '4px'}).obj

    cancelBtn.addEventListener('click', function (event) {
      event.preventDefault()
      self.close()
    })

    var confirmBtn = Forge.buildBtn({text: 'Delete', type: 'warning', width: '128px', margin: '4px'}).obj

    confirmBtn.addEventListener('click', function () {
      self.close()
    })

    container.appendChild(folderImg)
    container.appendChild(subText)
    container.appendChild(cancelBtn)
    container.appendChild(confirmBtn)
    return container
  }

  self.setModalName('Delete File')
  self.setClose({window: true, global: false})
  self.setWidth('483px')
  self.setHeight('492px')
  self.setTitle(text.title)
  self.setHeader({show: true, color: '#363A3C'})
  self.setPosition({top: '100px'})
  self.build()

  /**
   * This function is responsible for loading the text with current request name.
   * @memberof Modal_DeleteFile
   */
  this.loadData = function () {
    // var templateText = 'Are you sure you would like to delete</br>'
    // templateText += self.request.name + ' ? '
    // templateText += 'You will lose all sub-requests as well.'
    // document.getElementById('delete-request-modal-tip').innerHTML = templateText
  }
}

module.exports = Modal_DeleteFile
