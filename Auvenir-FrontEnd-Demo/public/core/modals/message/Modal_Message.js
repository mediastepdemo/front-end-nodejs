'use strict'

import ModalTemplate from '../ModalTemplate'
import Utility from '../../../js/src/utility_c'
import Forge from '../../Forge'

/**
 * Displays a message modal
 * @class Modal_Message

 * @param {Object} ctrl      - A reference to the current instance of the Main Application Controller (Controller.js)
 * @param {Object} settings - The custom settings that are used to generate the instance of this modal.
*/
var Modal_Message = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  var c = ctrl
  var textId = 'msgText' + '-' + Utility.randomString(8, Utility.ALPHANUMERIC)

  var self = this

  /**
   * Responsible for building the DOM structure for the message modal.
   * @memberof Modal_Message
   */
  this.buildDom = function () {
    var msgContainer = Forge.build({'dom': 'div', 'id': 'msgContainer'})
    var text = Forge.build({ 'dom': 'p', 'id': textId, 'text': 'Not Available' })
    msgContainer.style.marginTop = '32px'
    msgContainer.style.marginBottom = '24px'
    msgContainer.style.padding = '0px 15px'
    msgContainer.appendChild(text)

    return msgContainer
  }

  /**
   * Sets the content of the modal to be a string with styling.
   * @memberof Modal_Message
   *
   * @param {String} str   - String text for the message modal
   * @param {Object} style - A json representation of the styling on the text
   */
  this.setText = function (str, style) {
    var color = ((style && style.color) ? style.color : 'black')
    var borderWidth = ((style && style.border) ? style.border : 0)
    $('#' + textId).html('')
    if (this.isOpen()) {
      // When Message Modal is opened, then we have to add this string to our message modal.
      $('#' + textId).html('<div style="font-size: 15px; line-height: 25px; border: ' + borderWidth + 'px solid ' + color + ';color:' + color + '">' + str + '</div><br/>' + $('#' + textId).html())
    } else {
      // When Message Modal is not opened, User already closed previous error messages, so we should delete these messages.
      $('#' + textId).html('<div style="font-size: 15px; line-height: 25px; border: ' + borderWidth + 'px solid ' + color + ';color:' + color + '">' + str + '</div>')
    }
  }

  /**
   * Sets the content of the modal to be a specific DOM object structure.
   * @memberof Modal_Message
   *
   * @param {String} msg   - Dom for the modal
   * @param {Object} style - A json representation of the styling on the dom elements
   */
  this.setObject = function (msg, style) {
    var color = ((style && style.color) ? style.color : 'black')
    var borderWidth = ((style && style.border) ? style.border : 0)
    // if(msg.nodeName && (msg.nodeName.toLowerCase() === 'div')) {
    $('#' + textId).html('')
    $('#' + textId).css('color', color)
    $('#' + textId).html(msg)
    // }
  }

  self.setModalName('messageModal')
  self.setClose({'window': true, 'global': true})
  self.setWidth('460px')
  self.setHeader({'show': false})
  self.setPosition({ 'bottom': '0px'})
  self.build()
}

Modal_Message.prototype = ModalTemplate.prototype
Modal_Message.prototype.constructor = Modal_Message

module.exports = Modal_Message
