'use strict'

import ModalTemplate from '../ModalTemplate'
import Utility from '../../../js/src/utility_c'
import Forge from '../../Forge'
import log from '../../../js/src/log'

var Modal_EditClient = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  var c = ctrl
  var self = this
  var client = {}

  var check1Value = false
  var check2Value = false
  var check3Value = false

  /*
   * Local instances of the field values
   */
  var fields = {
    // Company Information
    legalName: { id: 'm-ec-legalName', type: 'text', value: '', model: {name: 'Business', field: 'name'}},
    legalNameChanged: { id: 'm-ec-legalNameChange', type: 'boolean', value: null, model: {name: 'Business', field: 'legalNameChanged'}},
    publiclyListed: { id: 'm-ec-publiclyListed', type: 'boolean', value: null, model: {name: 'Business', field: 'publiclyListed'}},
    overseasOps: { id: 'm-ec-overseasOps', type: 'boolean', value: null, model: {name: 'Business', field: 'overseasOps'}},
    parentStakeholders: { id: 'm-ec-parentStakeholders', type: 'text', value: '', model: {name: 'Business', field: 'parentStakeholders'}},
    industry: { id: 'm-ec-industry', type: 'text', value: '', model: {name: 'Business', field: 'industry'}},
    accountingFramework: { id: 'm-ec-accountingFramework', type: 'text', value: '', model: {name: 'Business', field: 'accountingFramework'}},
    address: {
      id: 'm-ec-fullAddress',
      model: {
        name: 'Business',
        field: 'address'
      },
      unit: {
        id: 'm-ec-unit',
        type: 'text',
        value: '',
        model: {
          name: 'Business',
          field: 'address.unit'
        }
      },
      streetAddress: {
        id: 'm-ec-streetAddress',
        type: 'text',
        value: '',
        model: {
          name: 'Business',
          field: 'address.streetAddress'
        }
      },
      city: { id: 'm-ec-city', type: 'text', value: '', model: {name: 'Business', field: 'address.city'}},
      stateProvince: { id: 'm-ec-stateProvince', type: 'text', value: '', model: {name: 'Business', field: 'address.stateProvince'}},
      postalCode: { id: 'm-ec-postalCode', type: 'text', value: '', model: {name: 'Business', field: 'address.postalCode'}}
    },
    // Key Contact
    name: { id: 'm-ec-fullName', type: 'text', value: '', model: {name: 'User', field: 'name'}},
    firstName: { id: 'm-ec-firstName', type: 'text', value: '', model: {name: 'User', field: 'firstName'}},
    lastName: { id: 'm-ec-lastName', type: 'text', value: '', model: {name: 'User', field: 'lastName'}},
    phone: { id: 'm-ec-phoneNumber', type: 'text', value: '', model: {name: 'User', field: 'phone'}},
    email: { id: 'm-ec-email', type: 'text', value: '', model: { name: 'User', field: 'email' } }
  }

  /**
   * This function is responsible for initializing the creation of a new Client
   * @memberof Modal_EditClient
   */
  var initialize = function () {
    log('Modal_EditClient: initialize')

    self.setModalName('dashboardModel')
    self.setClose({'window': true})
    self.setFullScreen(true)
    self.setHeader({'show': false})
    self.setPosition({ 'bottom': '0px'})
    self.setZIndex('1015')
    self.setOverflow('auto')
    self.build()
  }

  var checkboxChange = function (checkNum) {
    var property
    var value
    switch (checkNum) {
      case '1':
        property = document.getElementById(fields.legalNameChanged.id)
        value = check1Value
        break
      case '2':
        property = document.getElementById(fields.publiclyListed.id)
        value = check2Value
        break
      case '3':
        property = document.getElementById(fields.overseasOps.id)
        value = check3Value
        break
      default:
        return
    }
    if (value === true) {
      property.src = 'images/icons/checkbox-on.svg'
    } else {
      property.src = 'images/icons/checkbox-off.svg'
    }
  }

  // Loads the pre-existing data from the server.
  this.loadData = function (clientInfo) {
    const engagement = c.myEngagements.find((eng) => {
      return eng._id === c.currentEngagement._id
    })

    const clientAcl = engagement.acl.find((acl) => {
      return acl.role === 'CLIENT'
    })
    client = clientAcl.userInfo
    let business = engagement.businessID

    if (client.firstName && client.lastName) {
      document.getElementById(fields.name.id).value = client.firstName + ' ' + client.lastName
    } else if (client.firstName && !client.lastName) {
      document.getElementById(fields.name.id).value = client.firstName
    } else if (client.lastName) {
      document.getElementById(fields.name.id).value = client.lastName
    }
    if (client.phone) {
      document.getElementById(fields.phone.id).value = client.phone
    }
    if (client.email) {
      document.getElementById(fields.email.id).value = client.email
    }
    if (business.name) {
      document.getElementById(fields.legalName.id).value = business.name
    }
    if (business.legalNameChanged) {
      check1Value = business.legalNameChanged
      checkboxChange('1')
    }
    if (business.publiclyListed) {
      check2Value = business.publiclyListed
      checkboxChange('2')
    }
    if (business.overseasOps) {
      check3Value = business.overseasOps
      checkboxChange('3')
    }
    if (business.parentStakeholders) {
      document.getElementById(fields.parentStakeholders.id).value = business.parentStakeholders
    }
    if (business.industry) {
      document.getElementById(fields.industry.id).value = business.industry
    }
    if (business.accountingFramework) {
      document.getElementById(fields.accountingFramework.id).value = business.accountingFramework
    }
    if (business.address) {
      if (business.address.streetAddress) {
        document.getElementById('m-ec-address-streetNumber').value = business.address.streetAddress.substr(0, business.address.streetAddress.indexOf(' '))
        document.getElementById('m-ec-address-streetName').value = business.address.streetAddress.substr(business.address.streetAddress.indexOf(' ') + 1)
      }
      if (business.address.unit) {
        document.getElementById(fields.address.unit.id).value = business.address.unit
      }
      if (business.address.city) {
        document.getElementById(fields.address.city.id).value = business.address.city
      }
      if (business.address.stateProvince) {
        document.getElementById(fields.address.stateProvince.id).value = business.address.stateProvince
      }
      if (business.address.postalCode) {
        document.getElementById(fields.address.postalCode.id).value = business.address.postalCode
      }
    }
  }

  var industryList = [{text: 'Agriculture, Forestry, Fishing and Hunting' },
    {text: 'Mining, Quarrying, and Oil and Gas Extraction' },
    {text: 'Utilities' },
    {text: 'Construction' },
    {text: 'Manufacturing' },
    {text: 'Wholesale Trade' },
    {text: 'Retail Trade' },
    {text: 'Transportation and Warehousing' },
    {text: 'Information' },
    {text: 'Finance and Insurance' },
    {text: 'Real Estate' },
    {text: 'Professional, Scientific, and Technical Services' },
    {text: 'Management of Companies and Enterprises' },
    {text: 'Administrative and Support and Waste Management and Remediation Services' },
    {text: 'Educational Services' },
    {text: 'Health Care and Social Assistance' },
    {text: 'Arts, Entertainment, and Recreation' },
    {text: 'Accommodation and Food Services' },
    {text: 'Other Services (except Public Administration)' },
    {text: 'Public Administration' }]

  var frameworksList = [{text: 'ASPE'}, {text: 'GAAP'}, {text: 'IFRS'}, {text: 'NFP'}, {text: 'none of the above'}]

  /**
   * This function is responsible for building the dom
   * @memberof Modal_EditClient
   */
  this.buildDom = function () {
    log('Modal_EditClient: building DOM')

    var superContainer = Forge.build({'dom': 'div'})

    var header = Forge.build({'dom': 'h1', 'class': 'addClient-header', 'text': 'Edit Client'})
    var leftContainer = Forge.build({'dom': 'div', 'class': 'addClient-leftContainer'})
    var rightContainer = Forge.build({'dom': 'div', 'class': 'addClient-rightContainer'})
    var leftSection = Forge.build({'dom': 'div', 'class': 'addClient-leftSection'})
    var rightSection = Forge.build({'dom': 'div', 'class': 'addClient-rightSection'})
    leftContainer.appendChild(leftSection)
    rightContainer.appendChild(rightSection)

    var sectionHeader1 = Forge.build({'dom': 'h3', 'class': 'addClient-sectionHead', 'text': 'Company Information'})
    var legalNameInput = Forge.buildInput({'label': 'Legal Name of Entity', 'errorText': 'Not a valid name.', 'id': fields.legalName.id, 'width': '458px'})

    var checkContainer1 = Forge.build({'dom': 'div', 'style': 'margin-top:7px'})
    var checkBox1 = Forge.build({'dom': 'img', 'id': fields.legalNameChanged.id, 'src': 'images/icons/checkbox-off.svg', 'class': 'addClient-checkBox', 'style': 'cursor: pointer; margin-right: 5px'})
    var label1 = Forge.build({'dom': 'label', 'text': 'The legal name changed within the last two years', 'class': 'addClient-checkLabel', 'style': 'margin-right: 5px; font-weight: 300;'})
    checkContainer1.appendChild(checkBox1)
    checkContainer1.appendChild(label1)

    var checkContainer2 = Forge.build({'dom': 'div', 'class': 'addClient-checkContainer'})
    var checkBox2 = Forge.build({'dom': 'img', 'id': fields.publiclyListed.id, 'src': 'images/icons/checkbox-off.svg', 'class': 'addClient-checkBox', 'style': 'cursor: pointer; margin-right: 5px'})
    var label2 = Forge.build({'dom': 'label', 'text': 'The entity is publicly listed or a component of a publicly listed company', 'class': 'addClient-checkLabel', 'style': 'margin-right: 5px; font-weight: 300;'})
    checkContainer2.appendChild(checkBox2)
    checkContainer2.appendChild(label2)

    var checkContainer3 = Forge.build({'dom': 'div', 'class': 'addClient-checkContainer', 'style': 'margin-bottom: 25px'})
    var checkBox3 = Forge.build({'dom': 'img', 'id': fields.overseasOps.id, 'src': 'images/icons/checkbox-off.svg', 'class': 'addClient-checkBox', 'style': 'cursor: pointer; margin-right: 5px'})
    var label3 = Forge.build({'dom': 'label', 'text': 'The entity has operations overseas, including parent or subsidiaries', 'class': 'addClient-checkLabel', 'style': 'margin-right: 5px; font-weight: 300;'})
    checkContainer3.appendChild(checkBox3)
    checkContainer3.appendChild(label3)

    var parentInput = Forge.buildInputTextArea({'label': 'Please list parent company, owner, or shareholders with >20%', 'errorText': 'Not a valid name.', 'id': fields.parentStakeholders.id, 'width': '458px'})
    var industryInput = Forge.buildInputDropdown({'label': 'Industry', 'list': industryList, 'id': fields.industry.id, 'placeholder': 'Please Select', 'width': '458px'})
    var frameworkInput = Forge.buildInputDropdown({'label': 'Accounting Framework', 'list': frameworksList, 'id': fields.accountingFramework.id, 'placeholder': 'Please Select', 'width': '458px'})

    var addressHolder = Forge.build({'dom': 'form', 'id': 'm-ec-mapForm', 'class': 'firm-addressHolder'})

    var streetNumberLabel = Forge.build({'dom': 'p', 'text': 'Street Number', 'class': 'auv-inputTitle'})
    var streetNumberInput = Forge.build({'dom': 'input', 'id': 'm-ec-address-streetNumber', 'type': 'text', 'data-geo': 'street_number', 'errorText': 'Not a valid street number.', 'class': 'auv-input', 'style': 'width: 229px'})
    var streetLabel = Forge.build({'dom': 'p', 'text': 'Street', 'class': 'auv-inputTitle firm-geoLabel'})
    var streetInput = Forge.build({'dom': 'input', 'id': 'm-ec-address-streetName', 'type': 'text', 'data-geo': 'route', 'errorText': 'Not a valid street.', 'class': 'auv-input'})
    var unitLabel = Forge.build({'dom': 'p', 'text': 'Unit Number', 'class': 'auv-inputTitle firm-geoLabel'})
    var unitInput = Forge.build({'dom': 'input', 'id': fields.address.unit.id, 'type': 'text', 'errorText': 'Not a valid unit number.', 'class': 'auv-input', 'style': 'width: 229px'})
    var cityLabel = Forge.build({'dom': 'p', 'text': 'City', 'class': 'auv-inputTitle firm-geoLabel'})
    var cityInput = Forge.build({'dom': 'input', 'id': fields.address.city.id, 'data-geo': 'locality', 'errorText': 'Not a valid city.', 'class': 'auv-input'})
    var provinceLabel = Forge.build({'dom': 'p', 'text': 'Province/ State', 'class': 'auv-inputTitle firm-geoLabel'})
    var provinceInput = Forge.build({'dom': 'input', 'id': fields.address.stateProvince.id, 'data-geo': 'administrative_area_level_1', 'errorText': 'Not a valid province.', 'class': 'auv-input'})
    var postalLabel = Forge.build({'dom': 'p', 'text': 'PostalCode/ Zip Code', 'class': 'auv-inputTitle firm-geoLabel'})
    var postalInput = Forge.build({'dom': 'input', 'id': fields.address.postalCode.id, 'data-geo': 'postal_code', 'errorText': 'Not a valid postal code.', 'class': 'auv-input'})
    addressHolder.appendChild(streetNumberLabel)
    addressHolder.appendChild(streetNumberInput)
    addressHolder.appendChild(streetLabel)
    addressHolder.appendChild(streetInput)
    addressHolder.appendChild(unitLabel)
    addressHolder.appendChild(unitInput)
    addressHolder.appendChild(cityLabel)
    addressHolder.appendChild(cityInput)
    addressHolder.appendChild(provinceLabel)
    addressHolder.appendChild(provinceInput)
    addressHolder.appendChild(postalLabel)
    addressHolder.appendChild(postalInput)

    var sectionHeader2 = Forge.build({'dom': 'h3', 'class': 'addClient-sectionHead', 'text': 'Key Contact\'s Information'})
    var contactNameInput = Forge.buildInput({'label': 'First and Last Name', 'errorText': 'Not a valid name.', 'id': fields.name.id, 'width': '458px'})
    var contactEmailInput = Forge.buildInput({'label': 'Email Address', 'errorText': 'Not a valid email.', 'id': fields.email.id, 'width': '458px'})
    var contactPhoneInput = Forge.buildInput({'label': 'Phone Number', 'errorText': 'Not a valid phone number.', 'id': fields.phone.id, 'width': '226px'})
    var footer = Forge.build({'dom': 'div', 'class': 'addClient-footer'})
    var footerBtnHolder = Forge.build({'dom': 'div', 'class': 'addClient-footerBtnHolder'})
    var footerCancelBtn = Forge.build({'dom': 'a', 'text': 'Cancel', 'class': 'addClient-footerCancelBtn'})
    var footerAddBtn = Forge.buildBtn({'text': 'Save', 'id': 'm-ec-addBtn', 'type': 'primary', 'width': '102px', 'margin': '22px 83px 22px 25px', 'float': 'right'}).obj

    footerBtnHolder.appendChild(footerAddBtn)
    footerBtnHolder.appendChild(footerCancelBtn)
    footer.appendChild(footerBtnHolder)

    leftSection.appendChild(sectionHeader1)
    leftSection.appendChild(legalNameInput)
    leftSection.appendChild(checkContainer1)
    leftSection.appendChild(checkContainer2)
    leftSection.appendChild(checkContainer3)
    leftSection.appendChild(parentInput)
    leftSection.appendChild(industryInput)
    leftSection.appendChild(frameworkInput)
    leftSection.appendChild(addressHolder)

    rightSection.appendChild(sectionHeader2)
    rightSection.appendChild(contactNameInput)
    rightSection.appendChild(contactEmailInput)
    rightSection.appendChild(contactPhoneInput)

    superContainer.appendChild(header)
    superContainer.appendChild(leftContainer)
    superContainer.appendChild(rightContainer)
    superContainer.appendChild(footer)

    // Everytime it's a key is pressed, it will check that field's value and update DOM's value.
    legalNameInput.children[1].addEventListener('keydown', function () {
      var checkLegalName = document.getElementById(fields.legalName.id)
      if (Utility.REGEX.name.test(checkLegalName.value)) {
        checkLegalName.classList.remove('auv-error')
        footerAddBtn.disabled = false
      } else {
        checkLegalName.className = 'auv-input auv-error'
        footerAddBtn.disabled = true
      }
    })
    contactNameInput.children[1].addEventListener('keydown', function () {
      var checkContactName = document.getElementById(fields.name.id)
      if (Utility.REGEX.name.test(checkContactName.value)) {
        checkContactName.classList.remove('auv-error')
        footerAddBtn.disabled = false
      } else {
        checkContactName.className = 'auv-input auv-error'
        footerAddBtn.disabled = true
      }
    })
    contactEmailInput.children[1].addEventListener('keydown', function () {
      var checkContactEmail = document.getElementById(fields.email.id)
      if (Utility.REGEX.name.test(checkContactEmail.value)) {
        checkContactEmail.classList.remove('auv-error')
        footerAddBtn.disabled = false
      } else {
        checkContactEmail.className = 'auv-input auv-error'
        footerAddBtn.disabled = true
      }
    })
    contactPhoneInput.children[1].addEventListener('keydown', function () {
      var checkContactPhone = document.getElementById(fields.phone.id)
      if (Utility.REGEX.name.test(checkContactPhone.value)) {
        checkContactPhone.classList.remove('auv-error')
        footerAddBtn.disabled = false
      } else {
        checkContactPhone.className = 'auv-input auv-error'
        footerAddBtn.disabled = true
      }
    })

    // Changes value of check boxes.
    checkBox1.addEventListener('click', function () {
      if (check1Value === false) {
        check1Value = true
      } else {
        check1Value = false
      }
      checkboxChange('1')
    })
    checkBox2.addEventListener('click', function () {
      if (check2Value === false) {
        check2Value = true
      } else {
        check2Value = false
      }
      checkboxChange('2')
    })
    checkBox3.addEventListener('click', function () {
      if (check3Value === false) {
        check3Value = true
      } else {
        check3Value = false
      }
      checkboxChange('3')
    })

    footerCancelBtn.addEventListener('click', function () {
      self.close()
    })

    footerAddBtn.addEventListener('click', function () {
      updateFieldValues()
      updateClient()
      self.close()
    })

    return superContainer
  }

  var updateFieldValues = function () {
    fields.legalName.value = document.getElementById(fields.legalName.id).value
    fields.legalNameChanged.value = check1Value
    fields.publiclyListed.value = check2Value
    fields.overseasOps.value = check3Value
    fields.parentStakeholders.value = document.getElementById(fields.parentStakeholders.id).value
    fields.industry.value = document.getElementById(fields.industry.id).value
    fields.accountingFramework.value = document.getElementById(fields.accountingFramework.id).value

    var streetAddressField = document.getElementById('m-ec-address-streetNumber').value + ' ' + document.getElementById('m-ec-address-streetName').value
    var unitField = document.getElementById(fields.address.unit.id).value
    var cityField = document.getElementById(fields.address.city.id).value
    var provinceField = document.getElementById(fields.address.stateProvince.id).value
    var postalField = document.getElementById(fields.address.postalCode.id).value
    fields.address.streetAddress.value = streetAddressField
    fields.address.unit.value = unitField
    fields.address.city.value = cityField
    fields.address.stateProvince.value = provinceField
    fields.address.postalCode.value = postalField

    fields.name.value = document.getElementById(fields.name.id).value
    var nameField = document.getElementById(fields.name.id)
    if (nameField.value.indexOf(' ') > 0) {
      var twoNames = nameField.value.split(' ')
      fields.firstName.value = twoNames[0]
      fields.lastName.value = twoNames[1]
    } else {
      fields.firstName.value = nameField.value
      fields.lastName.value = ''
    }

    fields.phone.value = document.getElementById(fields.phone.id).value
    fields.email.value = document.getElementById(fields.email.id).value
  }

  var updateClient = function () {
    var action = 'UPDATE'
    var engagementId = c.currentEngagement._id

    var user = {
      firstName: fields.firstName.value,
      lastName: fields.lastName.value,
      email: fields.email.value,
      phone: fields.phone.value
    }

    var business = {
      name: fields.legalName.value,
      legalNameChanged: fields.legalNameChanged.value,
      publiclyListed: fields.publiclyListed.value,
      overseasOps: fields.overseasOps.value,
      parentStakeholders: fields.parentStakeholders.value,
      industry: fields.industry.value,
      accountingFramework: fields.accountingFramework.value
    }

    c.selectClient(action, engagementId, client._id, user, business)
  }

  initialize()
}

Modal_EditClient.prototype = ModalTemplate.prototype
Modal_EditClient.prototype.constructor = Modal_EditClient

module.exports = Modal_EditClient
