﻿/**
 * Created by Bao Nguyen on 7/13/2017.
 */
import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import styles from './Modal_CreateWorkingPaper.css'
import Utility from '../../../js/src/utility_c'

import Component_SetUp from '../../components/working-paper/setup'
import Component_Procedure from '../../components/working-paper/procedure'
import Component_Technique from '../../components/working-paper/technique'
import Component_Samples from '../../components/working-paper/samples'
import Component_Evidence from '../../components/working-paper/evidence'
import Component_Success from '../../components/working-paper/success'

var Modal_CreateWorkingPaper = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  var c = ctrl
  var self = this
  var text = c.lang.createWorkingPaperModal
  var currentStep = 1
  let nextStep
  var currentComponent = null
  var totalStep = 2

  var workingPaperInfo = null
  var workingPaperGenerateionInfo = {
    name: null,
    wpType: null,
    testType: null,
    samplingTechnique: null,
    sampleSize: null,

  }

  /**
   * This function is responsible for initializing the creation of a new Client
   * @memberof Modal_CreateWorkingPaper
   */
  var initialize = function () {
    log('Modal_CreateWorkingPaper: initialize')
    self.setModalName('workingpaperModel')
    self.setClose({'window': true, 'image': 'images/icons/x-large.svg'})
    self.setTitle(text.headerTitle)
    self.setFullScreen(false)
    self.setWidth('580px')
    self.setOverflow('auto')
    self.setHeader({ 'show': true, color: '#363A3C' })
    self.setPosition({'bottom': '0px'})
    self.setPadding('0px')
    self.build()
    firstComponent()
  }

  var components = {
    setUp: { linkID: 'link-ce-workingpaper-setup', obj: null, displayID: 'ce-workingpaper-setup-container', title: text.setupTitle, step: 1, complete: false },
    procedure: { linkID: 'link-ce-workingpaper-procedure', obj: null, displayID: 'ce-workingpaper-procedure-container', title: text.procedureTitle, step: 2, complete: false },
    technique: { linkID: 'link-ce-workingpaper-technique', obj: null, displayID: 'ce-workingpaper-technique-container', title: text.techniqueTitle, step: 3, complete: false },
    samples: { linkID: 'link-ce-workingpaper-samples', obj: null, displayID: 'ce-workingpaper-samples-container', title: text.samplesTitle, step: 4, complete: false },
    evidence: { linkID: 'link-ce-workingpaper-evidence', obj: null, displayID: 'ce-workingpaper-evidence-container', title: text.evidenceTitle, step: 5, complete: false },
    success: { linkID: 'link-ce-workingpaper-success', obj: null, displayID: 'ce-workingpaper-success-container', title: text.successTitle, step: 6, complete: false },
  }
  var componentArray = [
    components.setUp,
    components.procedure,
    components.technique,
    components.samples,
    components.evidence,
    components.success
  ]

  var componentContainer = 'create-workingpaper-component-container'

  var firstComponent = function () {
    currentComponent = components.setUp
    document.getElementById(currentComponent.displayID).style.display = 'inherit'
    document.getElementById(currentComponent.linkID + 'circle').classList.toggle('ce-numberCircle-active')
    document.getElementById(currentComponent.linkID + 'title').classList.add('ce-navLink-active')
  }

  /**
   * Changes the front end UI for the navigation system, and hides and shows the appropriate component.
   * If its toggling past the security page, it skips to the Dashboard.
   *
   * @param {object} name - The name of the component to show.
   */

  var viewComponent = function (n) {
    if (n < currentComponent.step) {
      for (let i = n - 1; i < componentArray.length - 1; i++) {
        componentArray[i].complete = false
        document.getElementById(componentArray[i].linkID + 'num').innerText = componentArray[i].step
        document.getElementById(componentArray[i].linkID + 'num').classList.remove('auvicon-checkmark', 'ce-nav-checkmark')
        document.getElementById(componentArray[i].linkID + 'circle').classList.remove('ce-numberCircle-active')
        document.getElementById(componentArray[i].linkID + 'circle').classList.remove('ce-numberCircle-done')
        document.getElementById(componentArray[i].linkID + 'circle').style.cursor = 'default'
        document.getElementById(componentArray[i].linkID + 'title').classList.remove('ce-navLink-active')
      }
    } else {
      document.getElementById(currentComponent.linkID + 'num').innerText = ''
      document.getElementById(currentComponent.linkID + 'num').classList.add('auvicon-checkmark', 'ce-nav-checkmark')
      document.getElementById(currentComponent.linkID + 'circle').classList.add('ce-numberCircle-done')
      document.getElementById(currentComponent.linkID + 'circle').style.cursor = 'pointer'
      document.getElementById(currentComponent.linkID + 'title').classList.add('ce-navLink-active')
    }

    document.getElementById(currentComponent.displayID).style.display = 'none'
    currentStep = n
    currentComponent = componentArray[n - 1]
    document.getElementById(currentComponent.displayID).style.display = 'inherit'
    document.getElementById(currentComponent.linkID + 'circle').classList.toggle('ce-numberCircle-active')
    document.getElementById(currentComponent.linkID + 'title').classList.toggle('ce-navLink-active')
  }

  /**
   * This function is responsible for building the page only when called.
   * @memberof Modal_CreateWorkingPaper
   */
  this.buildDom = function () {
    log('Building Create Working Paper Modal')

    var parentContainer = Forge.build({'dom': 'div', 'id': 'CreateWorkingPaperParent', class: 'ce-parent working-paper-container'})
    var systemContainer = Forge.build({'dom': 'div', 'class': 'ce-systemContainer', 'id': 'm-ce-workingpaper-systemContainer'})
    var navigationSystem = Forge.build({'dom': 'div', 'id': 'm-ce-workingpaper-navSystem', 'class': 'ce-navSystem'})
    var componentSystem = Forge.build({'dom': 'div', 'id': componentContainer, 'class': 'workingpaper-modal-component'})

    var setUpComponent = Forge.build({'dom': 'div', 'id': components.setUp.displayID, 'class': 'ce-individualContainer'})
    var procedureComponent = Forge.build({'dom': 'div', 'id': components.procedure.displayID, 'class': 'ce-individualContainer'})
    var techniqueComponent = Forge.build({'dom': 'div', 'id': components.technique.displayID, 'class': 'ce-individualContainer'})
    var samplesComponent = Forge.build({'dom': 'div', 'id': components.samples.displayID, 'class': 'ce-individualContainer'})
    var evidenceComponent = Forge.build({'dom': 'div', 'id': components.evidence.displayID, 'class': 'ce-individualContainer'})
    var successComponent = Forge.build({'dom': 'div', 'id': components.success.displayID, 'class': 'ce-individualContainer'})

    components.setUp.obj = new Component_SetUp(c, setUpComponent, this)
    components.procedure.obj = new Component_Procedure(c, procedureComponent, this)
    components.technique.obj = new Component_Technique(c, techniqueComponent, this)
    components.samples.obj = new Component_Samples(c, samplesComponent, this)
    components.evidence.obj = new Component_Evidence(c, evidenceComponent, this)
    components.success.obj = new Component_Success(c, successComponent, this)

    componentSystem.appendChild(setUpComponent)
    componentSystem.appendChild(procedureComponent)
    componentSystem.appendChild(techniqueComponent)
    componentSystem.appendChild(samplesComponent)
    componentSystem.appendChild(evidenceComponent)
    componentSystem.appendChild(successComponent)

    var navSystemLinkHolderOne = Forge.build({'dom': 'div', 'id': components.setUp.linkID, 'class': 'ce-navLinkHolder'})
    var navSystemLinkOne = Forge.build({'dom': 'span', 'id': components.setUp.linkID + 'title', 'text': components.setUp.title, 'class': 'ce-navLinkTxt'})
    var navNumberCircleOne = Forge.build({'dom': 'div', 'id': components.setUp.linkID + 'circle', 'class': 'ce-numberCircle'})
    var navNumberOne = Forge.build({'dom': 'span', 'text': 1, 'class': 'ce-number', 'id': components.setUp.linkID + 'num'})

    var navSystemLinkHolderTwo = Forge.build({'dom': 'div', 'id': components.procedure.linkID, 'class': 'ce-navLinkHolder'})
    var navSystemLinkTwo = Forge.build({'dom': 'span', 'id': components.procedure.linkID + 'title', 'text': components.procedure.title, 'class': 'ce-navLinkTxt'})
    var navNumberCircleTwo = Forge.build({'dom': 'div', 'id': components.procedure.linkID + 'circle', 'class': 'ce-numberCircle'})
    var navNumberTwo = Forge.build({'dom': 'span', 'text': 2, 'class': 'ce-number', 'id': components.procedure.linkID + 'num'})
    var navLinkHrOne = Forge.build({'dom': 'hr', 'class': 'ce-linkHr'})
    navSystemLinkHolderTwo.appendChild(navLinkHrOne)

    var navSystemLinkHolderThree = Forge.build({'dom': 'div', 'id': components.technique.linkID, 'class': 'ce-navLinkHolder'})
    var navSystemLinkThree = Forge.build({'dom': 'span', 'id': components.technique.linkID + 'title', 'text': components.technique.title, 'class': 'ce-navLinkTxt'})
    var navNumberCircleThree = Forge.build({'dom': 'div', 'id': components.technique.linkID + 'circle', 'class': 'ce-numberCircle'})
    var navNumberThree = Forge.build({'dom': 'span', 'text': 3, 'class': 'ce-number', 'id': components.technique.linkID + 'num'})
    var navLinkHrTwo = Forge.build({'dom': 'hr', 'class': 'ce-linkHr'})
    navSystemLinkHolderThree.appendChild(navLinkHrTwo)

    var navSystemLinkHolderFour = Forge.build({'dom': 'div', 'id': components.samples.linkID, 'class': 'ce-navLinkHolder'})
    var navSystemLinkFour = Forge.build({'dom': 'span', 'id': components.samples.linkID + 'title', 'text': components.samples.title, 'class': 'ce-navLinkTxt'})
    var navNumberCircleFour = Forge.build({'dom': 'div', 'id': components.samples.linkID + 'circle', 'class': 'ce-numberCircle'})
    var navNumberFour = Forge.build({'dom': 'span', 'text': 4, 'class': 'ce-number', 'id': components.samples.linkID + 'num'})
    var navLinkHrThree = Forge.build({'dom': 'hr', 'class': 'ce-linkHr'})
    navSystemLinkHolderFour.appendChild(navLinkHrThree)

    var navSystemLinkHolderFive = Forge.build({'dom': 'div', 'id': components.evidence.linkID, 'class': 'ce-navLinkHolder'})
    var navSystemLinkFive = Forge.build({'dom': 'span', 'id': components.evidence.linkID + 'title', 'text': components.evidence.title, 'class': 'ce-navLinkTxt'})
    var navNumberCircleFive = Forge.build({'dom': 'div', 'id': components.evidence.linkID + 'circle', 'class': 'ce-numberCircle'})
    var navNumberFive = Forge.build({'dom': 'span', 'text': 5, 'class': 'ce-number', 'id': components.evidence.linkID + 'num'})
    var navLinkHrFour = Forge.build({'dom': 'hr', 'class': 'ce-linkHr'})
    navSystemLinkHolderFive.appendChild(navLinkHrFour)

    navNumberCircleOne.appendChild(navNumberOne)
    navSystemLinkHolderOne.appendChild(navSystemLinkOne)
    navSystemLinkHolderOne.appendChild(navNumberCircleOne)

    navNumberCircleTwo.appendChild(navNumberTwo)
    navSystemLinkHolderTwo.appendChild(navSystemLinkTwo)
    navSystemLinkHolderTwo.appendChild(navNumberCircleTwo)

    navNumberCircleThree.appendChild(navNumberThree)
    navSystemLinkHolderThree.appendChild(navSystemLinkThree)
    navSystemLinkHolderThree.appendChild(navNumberCircleThree)

    navNumberCircleFour.appendChild(navNumberFour)
    navSystemLinkHolderFour.appendChild(navSystemLinkFour)
    navSystemLinkHolderFour.appendChild(navNumberCircleFour)

    navNumberCircleFive.appendChild(navNumberFive)
    navSystemLinkHolderFive.appendChild(navSystemLinkFive)
    navSystemLinkHolderFive.appendChild(navNumberCircleFive)

    navigationSystem.appendChild(navSystemLinkHolderOne)
    navigationSystem.appendChild(navSystemLinkHolderTwo)
    navigationSystem.appendChild(navSystemLinkHolderThree)
    navigationSystem.appendChild(navSystemLinkHolderFour)
    navigationSystem.appendChild(navSystemLinkHolderFive)

    $(navSystemLinkHolderThree).hide()
    $(navSystemLinkHolderFour).hide()
    $(navSystemLinkHolderFive).hide()

    var footer = Forge.build({'dom': 'div', 'class': 'ce-footer'})
    var footerBtnHolder = Forge.build({'dom': 'div', 'class': 'ce-footerBtnHolder'})
    var footerCancelBtn = Forge.buildBtn({
      'text': c.lang.cancel,
      'id': 'm-ce-workingpaper-cancelBtn',
      'type': 'secondary',
      'margin': '2px 0px 22px 11px',
      'float': 'right'
    }).obj
    var footerBackBtn = Forge.buildBtn({
      'text': c.lang.back,
      'id': 'm-ce-workingpaper-backBtn',
      'type': 'secondary',
      'margin': '2px 0px 22px 11px',
      'float': 'right'
    }).obj
    var footerExportBtn = Forge.buildBtn({
      'text': 'Export',
      'id': 'm-ce-workingpaper-exportBtn',
      'type': 'default',
      'margin': '2px 0px 22px 11px',
      'float': 'right'
    }).obj
    var footerPreviewBtn = Forge.buildBtn({
      'text': 'Preview',
      'id': 'm-ce-workingpaper-previewBtn',
      'type': 'default',
      'margin': '2px 0px 22px 11px',
      'float': 'right'
    }).obj
    var footerContinueBtn = Forge.buildBtn({
      'text': c.lang.continue,
      'id': 'm-ce-workingpaper-addBtn',
      'type': 'primary',
      'margin': '2px 28px 22px 11px',
      'float': 'right'
    }).obj

    footerContinueBtn.addEventListener('click', function () {
      currentComponent.complete = true
      viewNextComponent()
    })

    footerCancelBtn.addEventListener('click', function () {
      clearNewEngModal()
    })

    footerBackBtn.addEventListener('click', function () {
      viewPreviousComponent()
    })

    footerBtnHolder.appendChild(footerContinueBtn)
    footerBtnHolder.appendChild(footerCancelBtn)
    footerBtnHolder.appendChild(footerPreviewBtn)
    footerBtnHolder.appendChild(footerBackBtn)
    footerBtnHolder.appendChild(footerExportBtn)
    footer.appendChild(footerBtnHolder)

    systemContainer.appendChild(navigationSystem)
    systemContainer.appendChild(componentSystem)
    systemContainer.appendChild(footer)
    parentContainer.appendChild(systemContainer)

    // hide button back when init popup
    $(footerBackBtn).hide()
    $(footerExportBtn).hide()
    $(footerPreviewBtn).hide()

    return parentContainer
  }

  var viewNextComponent = function () {
     var test = components.samples;
    nextStep = currentStep + 1
    $('#m-ce-workingpaper-exportBtn').hide()
    $('#m-ce-workingpaper-previewBtn').hide()
    if (nextStep === 1) {
      // hide button back, show button cancel
      $('#m-ce-workingpaper-backBtn').hide()
      $('#m-ce-workingpaper-cancelBtn').show()
    } else {
      $('#m-ce-workingpaper-backBtn').show()
      $('#m-ce-workingpaper-cancelBtn').hide()

      if (nextStep === 2) {
        if (checkSetupStep()) {
          return
        }

        // if no checkbox checked => checkbox confirm not active
        var countChecked = document.getElementById('checkboxContainer').querySelectorAll('input[type="checkbox"]:checked').length
        if (countChecked === 0) {
          $('#checkboxComfirmation').attr('disabled', 'disabled')
          $('#m-ce-workingpaper-addBtn').attr('disabled', 'disabled')
        } else {
          $('#checkboxComfirmation').removeAttr('disabled')
          $('#m-ce-workingpaper-addBtn').removeAttr('disabled')
        }
      }

      if (nextStep === 3) {
        $('#workingpaper-RiskOfOverRelianceInput').parent().hide()
        $('#workingpaper-EPDRInput').parent().hide()
        $('#workingpaper-TRDInput').parent().hide()
      }

      if (nextStep === 4) {
        $('#m-ce-workingpaper-exportBtn').show()
        if (checkTechniqueStep()) {
          return
        }
        //
        $('#btn-loaddata').trigger('click')
      }

      if (nextStep === 5) {
        $('#m-ce-workingpaper-previewBtn').show()
        $('#m-ce-workingpaper-addBtn').attr('disabled', 'disabled')
      }

      if (nextStep === 6) {
        $('#m-ce-workingpaper-exportBtn').hide()
        $('#m-ce-workingpaper-previewBtn').hide()
        $('#m-ce-workingpaper-backBtn').hide()
        $('#m-ce-workingpaper-cancelBtn').hide()
        $('#m-ce-workingpaper-addBtn').hide()
        $('#m-ce-workingpaper-navSystem').hide()

        viewComponent(6)
      }
    }

    if (nextStep <= totalStep) {
      viewComponent(nextStep)
    }
  }

  var viewPreviousComponent = function () {
    currentStep = currentStep - 1

    // back to previous step
    viewComponent(currentStep)
    $('#m-ce-workingpaper-exportBtn').hide()
    $('#m-ce-workingpaper-previewBtn').hide()
    if (currentStep === 1) {
      // hide button back, show button cancel
      $('#m-ce-workingpaper-backBtn').hide()
      $('#m-ce-workingpaper-cancelBtn').show()
      $('#m-ce-workingpaper-addBtn').removeAttr('disabled')
    } else {
      $('#m-ce-workingpaper-backBtn').show()
      $('#m-ce-workingpaper-cancelBtn').hide()

      if (currentStep === 4) {
        $('#m-ce-workingpaper-exportBtn').show()
      }

      if (currentStep === 5) {
        $('#m-ce-workingpaper-previewBtn').show()
      }
    }
  }

  const checkSetupStep = () => {
    let name = document.getElementById('workingpaper-name').value
    let workingPaperType = document.getElementById('workingpaper-type').value
    let hasFailed = false

    if (name === '' || !Utility.REGEX.alphanumericName.test(name)) {
      document.getElementById('workingpaper-name').classList.add('auv-error')
      hasFailed = true
    } else {
      document.getElementById('workingpaper-name').classList.remove('auv-error')
    }

    if (workingPaperType === '' || workingPaperType !== 'Accounts Receivable') {
      document.getElementById('workingpaper-type').classList.add('auv-error')
      hasFailed = true
    } else {
      document.getElementById('workingpaper-type').classList.remove('auv-error')
    }

    if (hasFailed) {
      return true
    }
  }

  const checkTechniqueStep = () => {
    let sampleTech = document.getElementById('workingpaper-samplingTechniqueDropdown').value
    let sampleSize = document.getElementById('workingpaper-sampleSizeInput').value
    let hasFailed = false

    if (sampleSize === '' || !Utility.REGEX.alphanumericName.test(sampleSize)) {
      document.getElementById('workingpaper-sampleSizeInput').classList.add('auv-error')
      hasFailed = true
    } else {
      document.getElementById('workingpaper-sampleSizeInput').classList.remove('auv-error')
    }

    if (sampleTech === '') {
      document.getElementById('workingpaper-samplingTechniqueDropdown').classList.add('auv-error')
      hasFailed = true
    } else {
      document.getElementById('workingpaper-samplingTechniqueDropdown').classList.remove('auv-error')
    }

    if (hasFailed) {
      return true
    }
  }

  this.setTotalStep = function (numStep) {
    totalStep = numStep
    if (totalStep === 5) {
      $('#' + components.technique.linkID).show()
      $('#' + components.samples.linkID).show()
      $('#' + components.evidence.linkID).show()
    } else {
      $('#' + components.technique.linkID).hide()
      $('#' + components.samples.linkID).hide()
      $('#' + components.evidence.linkID).hide()
    }
  }

  this.onClose = () => {
    clearNewEngModal()
  }

  let clearNewEngModal = () => {
    self.close()
    currentStep = 1
    nextStep = 1
    viewComponent(1)

    let clearContent = (contentID) => {
      document.getElementById(contentID).value = ''
      document.getElementById(contentID).classList.remove('auv-error')
    }
    clearContent('workingpaper-name')
    clearContent('workingpaper-type')
    $('#m-ce-workingpaper-addBtn').show()
    $('#m-ce-workingpaper-addBtn').removeAttr('disabled')
    $('#m-ce-workingpaper-cancelBtn').show()
    $('#m-ce-workingpaper-backBtn').hide()
    $('#' + components.technique.linkID).hide()
    $('#' + components.samples.linkID).hide()
    $('#' + components.evidence.linkID).hide()
    totalStep = 2
  }

  this.setWorkingPaperInfo = function (myWorkingPaperInfo) {
    workingPaperInfo = myWorkingPaperInfo
    log('-------- workingPaperInfo ----------')
    log(workingPaperInfo)
  }

  initialize()
}

Modal_CreateWorkingPaper.prototype = ModalTemplate.prototype
Modal_CreateWorkingPaper.prototype.constructor = Modal_CreateWorkingPaper

module.exports = Modal_CreateWorkingPaper
