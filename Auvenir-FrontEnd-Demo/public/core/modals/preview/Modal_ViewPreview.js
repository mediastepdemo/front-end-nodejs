'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import styles from './Modal_ViewPreview.css'
import Utility from '../../../js/src/utility_c'

import Component_SetUp from '../../components/setup/setup'
import Component_Team from '../../components/team/team'
import Component_Customize from '../../components/customize/customize'

var Modal_ViewPreview = function (ctrl, settings) {
    ModalTemplate.call(this, ctrl, settings)
    var c = ctrl
    var self = this
    var text = c.lang.viewPreviewModal
    var currentStep = 0
    let nextStep
    var currentComponent = null

    /**
     * This function is responsible for initializing the creation of a new Client
     * @memberof Modal_ViewPreview
     */
    var initialize = function () {
        log('Modal_ViewPreview: initialize')
        self.setModalName('engagementModel')
        self.setClose({ 'window': false })
        self.setFullScreen(true)
        self.setHeader({ 'show': false })
        self.setPosition({ 'bottom': '0px' })
        self.setPadding('0px')
        self.build()
    }

    var componentContainer = 'view-preview-component-container'

    /**
     * Clears the New Engagement Modal to be used again
     */
    let clearNewEngModal = () => {
        self.close()
        currentStep = 0
        nextStep = 0
        viewComponent(0)

        let clearContent = (contentID) => {
            document.getElementById(contentID).value = ''
            document.getElementById(contentID).classList.remove('auv-error')
        }
        clearContent('engagement-name')
        clearContent('engagement-type')
        clearContent('engagement-deadline')
        clearContent('engagement-date-range-start')
        clearContent('engagement-date-range-end')
        clearContent('engagement-company')
        document.getElementById('m-ce-modalTitle').innerHTML = 'New Engagement'
        document.getElementById('m-ce-addBtn').style.display = 'block'
        document.getElementById('m-ce-cancelBtn').style.display = 'block'
        if (document.getElementById('m-ne-teamMemberName')) {
            clearContent('m-ne-teamMemberName')
            clearContent('m-ne-teamMamberEmail')
            clearContent('m-ne-teamMamberEmailSecond')
            clearContent('m-ne-teamMemberRole')
        }
        components.team.obj.viewTeamCreation()
        if (document.getElementById('team-addMemberBtn')) {
            document.getElementById('team-addMemberBtn').style.display = 'none'
        }
        document.getElementById('c-team-options').style.display = 'inherit'

        if (document.getElementById('team-invitedList')) {
            const removeInvitedList = document.getElementById('team-invitedList')
            while (removeInvitedList.firstChild) {
                removeInvitedList.removeChild(removeInvitedList.firstChild)
            }
        }
        if (document.getElementById('team-selectMember')) {
            const removeSelectMember = document.getElementById('team-selectMember')
            while (removeSelectMember.firstChild) {
                removeSelectMember.removeChild(removeSelectMember.firstChild)
            }
        }
    }

    /**
     * This function is responsible for building the page only when called.
     * @memberof Modal_ViewPreview
     */
    this.buildDom = function () {
        log('Building ViewPreview Modal')

        var parentContainer = Forge.build({ 'dom': 'div', 'id': 'ViewPreviewParent', class: 'vpw-parent' })
        var header = Forge.build({ 'dom': 'div', 'class': 'vpw-header' })
        var topContainer = Forge.build({ 'dom': 'div', id: 'top-container', 'style': 'margin: auto; height: 70px; min-width: 1028px; max-width: 1440px; padding: 0px;' })
        topContainer.innerHTML = '<img class="header-auvenirLogo" src="images/logos/auvenir/auvenir.svg" style="float:left;margin: 25px 0;" />'
        header.appendChild(topContainer)

        var titleContainer = Forge.build({ 'dom': 'div', 'class': 'vpw-titleContainer' })
        var titleText = Forge.build({ 'dom': 'div', 'class': 'vpw-headerTitle' })
        titleText.innerHTML = 'Flower Shop 2016'
        titleContainer.appendChild(titleText)

        var systemContainer = Forge.build({ 'dom': 'div', 'class': 'vpw-systemContainer', 'id': 'm-ce-systemContainer' })
        var componentSystem = Forge.build({ 'dom': 'div', 'id': componentContainer, 'class': 'vpw-componentContainer' })

        var descContainer = Forge.build({ 'dom': 'div', 'class': 'vpw-descContainer' })
        descContainer.innerHTML = '<span class="vpw-blue-bold">Confirmation Request Letter</span><span class="vpw-gray-bold" style="margin-left:30px;">Customer Account Name</span>' +
            '<div style="clear:both;height:30px;font-size:0;"></div>' +
            '<span class="vpw-blue-bold" style="font-size:14px;">To:</span><input type="text" class="sup" value="" placeholder="Customer Email Address" />' +
            '<div style="clear:both;height:30px;font-size:0;"></div>' +
            '<span class="vpw-blue-bold" style="font-size:14px;">CC:</span><input type="text" class="sup" value="" placeholder="Customer Email Address, Customer Email Address, Customer..." />' +
            '<div style="clear:both;height:30px;font-size:0;"></div>' +
            '<span class="vpw-blue-bold" style="font-size:14px;">Opening Address</span>' +
            '<div style="clear:both;height:15px;font-size:0;"></div>' +
            '<div id="opening-address-container">' +
            '<div id="oppening-address-content" class="vpw-blue-bold" style="font-size:12px;float:left;width:800px;"><p>Hi there,</p><p>Please review the following general ledger entriesand confirm their corresponding amounts by entering them in the adjacent input box below.</p></div>' +
            '<div style="float:left;min-height:1px;"><a href="javascript:void(0)" id="edit-opening-address-lnk" class="edit-lnk" title="Edit">&#x270E;</a></div>' +
            '</div>' +
            '<div id="opening-address-editor">' +
            '<textarea></textarea>' +
            '<div style="clear:both;height:5px;font-size:0;"></div>' +
            '<button id="save-opening-address">Save</button>' +
            '<button id="close-opening-address">Close</button>' +
            '</div>' +
            '<div style="clear:both;height:15px;font-size:0;"></div>'
        componentSystem.appendChild(descContainer)

        var listContainer = Forge.build({ 'dom': 'div', 'class': 'vpw-listContainer' })
        var listHeader = Forge.build({ 'dom': 'div', 'class': 'vpw-listHeader' })
        listHeader.innerHTML = '<span>Account Receiveable Journal Entries:</span><span style="color:aqua;margin-left: 10px;">Client Name</span>' +
            '<span style="font-size:12px;margin-left: 200px;">For period: February 1st, 2016 - January 31st, 2017</span>'
        var listTable = Forge.build({ 'dom': 'table', 'class': 'vpw-listTable' })
        listTable.innerHTML = '<tr>' +
            '<th><input type="checkbox" id="check-all"/></th>' +
            '<th><a href="javascript:void(0)">Date <i>&#9662;</i></a></th>' +
            '<th><a href="javascript:void(0)">Account <i>&#9662;</i></a></th>' +
            '<th><a href="javascript:void(0)">Description <i>&#9662;</i></a></th>' +
            '<th><a href="javascript:void(0)">Customer <i>&#9662;</i></a></th>' +
            '<th><a href="javascript:void(0)">Amount <i>&#9662;</i></a></th>' +
            '<th><a href="javascript:void(0)">Input Amount <i>&#9662;</i></a></th>' +
            '</tr>';

        // bind dynamic data for list
        var entries = [
            {
                id: 1,
                date: "12 June 2017",
                account: "Cash",
                description: "Payment for Invoice #957",
                customer: "Staples",
                amount: "$(1,074.23)"
            },
            {
                id: 2,
                date: "5 May 2017",
                account: "Inventory",
                description: "Invoice #324",
                customer: "Staples",
                amount: "$3,583.00"
            }
        ]
        for (var i in entries) {
            listTable.innerHTML += '<tr>' +
                '<td><input type="checkbox" class="item-check"/></td>' +
                '<td>' + entries[i].date + '</td>' +
                '<td>' + entries[i].account + '</td>' +
                '<td>' + entries[i].description + '</td>' +
                '<td>' + entries[i].customer + '</td>' +
                '<td>' + entries[i].amount + '</td>' +
                '<td><input type="text" class="item-input" placeholder="Enter Value"/></td>' +
                '</tr>'
        }
        // var entriesHtml = [];
        // var entriesServiceUrl = 'http://192.168.1.162:8080/api/request-detail';
        // $.ajax({
        //     type: "GET", //rest Type
        //     dataType: 'json', //mispelled
        //     url: entriesServiceUrl,
        //     async: false,
        //     contentType: "application/json; charset=utf-8",
        //     success: function (dataResult) {
        //         dataResult.SampleInfo.TransactionList.forEach((item) => {
        //             entriesHtml.push(item);
        //         });
        //     },
        //     error: function (e) {
        //         alert("Error!!");
        //     }
        // });
        // entriesHtml.forEach((item) => {
        //     listTable.innerHTML += '<tr>' +
        //         '<td><input type="checkbox" class="item-check"/></td>' +
        //         '<td>' + item.TransactionDate + '</td>' +
        //         '<td>' + item.AccountTypeName + '</td>' +
        //         '<td>' + item.Description + '</td>' +
        //         '<td>' + item.CustomerName + '</td>' +
        //         '<td>' + item.Amount + '</td>' +
        //         '<td><input type="text" class="item-input" placeholder="Enter Value"/></td>' +
        //         '</tr>'
        // });

        $(document).ready(function () {
            // check all checkbox handle
            $("#check-all").click(function () {
                $('input:checkbox.item-check').not(this).prop('checked', this.checked);
            });

            // opening address editor handle
            $("#edit-opening-address-lnk").click(function () {
                $("#opening-address-container").hide();
                $("#opening-address-editor").show();

                var str1 = $("#oppening-address-content p:first").text();
                var str2 = $("#oppening-address-content p:last").text();
                $("#opening-address-editor textarea").html(str1 + "\n\n" + str2);

                // close the closing address editor
                $("#closing-address-editor").hide();
                $("#closing-address-container").show();
            });
            $("#close-opening-address").click(function () {
                $("#opening-address-editor").hide();
                $("#opening-address-container").show();
            });

            // closing address editor handle
            $("#edit-closing-address-lnk").click(function () {
                $("#closing-address-container").hide();
                $("#closing-address-editor").show();

                var str1 = $("#closing-address-content p:first").text();
                var str2 = $("#closing-address-content p:last").text();
                $("#closing-address-editor textarea").html(str1 + "\n\n" + str2);

                // close the opening address editor
                $("#opening-address-editor").hide();
                $("#opening-address-container").show();
            });
            $("#close-closing-address").click(function () {
                $("#closing-address-editor").hide();
                $("#closing-address-container").show();
            });
        });

        var subContainer = Forge.build({ 'dom': 'div', 'class': 'vpw-descContainer' })
        subContainer.innerHTML = '<span class="vpw-blue-bold" style="font-size:14px;">Closing Address</span>' +
            '<div style="clear:both;height:15px;font-size:0;"></div>' +
            '<div id="closing-address-container">' +
            '<div id="closing-address-content" class="vpw-blue-bold" style="font-size:12px;float:left;width:800px;"><p>Thank-you for your cooperation during this audit, it is greatly appreciated. Please provide your signature below either digitally or by printing, signing and re-uploading this page below.</p><p>Sincerely,<br/>[Auditor Name]</p></div>' +
            '<div style="float:left;min-height:1px;"><a href="javascript:void(0)" id="edit-closing-address-lnk" class="edit-lnk" title="Edit">&#x270E;</a></div>' +
            '</div>' +
            '<div id="closing-address-editor">' +
            '<textarea></textarea>' +
            '<div style="clear:both;height:5px;font-size:0;"></div>' +
            '<button id="save-closing-address">Save</button>' +
            '<button id="close-closing-address">Close</button>' +
            '</div>' +
            '<div style="clear:both;height:15px;font-size:0;"></div>' +
            '<input type="text" class="sub" value="" placeholder="Client Name (Printed)" /><input type="text" class="sub" value="" placeholder="Customer Name (Printed)" />' +
            '<div style="clear:both;height:40px;font-size:0;"></div>' +
            '<input type="text" class="sub" value="" placeholder="Client Signature" /><input type="text" class="sub" value="" placeholder="Customer Signature" />'
        
        listContainer.appendChild(listHeader)
        listContainer.appendChild(listTable)
        componentSystem.appendChild(listContainer)
        componentSystem.appendChild(subContainer)

        var footer = Forge.build({ 'dom': 'div', 'class': 'vpw-footer' })
        var footerBtnHolder = Forge.build({ 'dom': 'div', 'class': 'vpw-footerBtnHolder' })
        var footerCancelBtn = Forge.buildBtn({
            'text': c.lang.cancel,
            'id': 'm-ce-cancelBtn',
            'type': 'secondary',
            'margin': '22px 0px 22px 11px',
            'float': 'right'
        }).obj
        var footerContinueBtn = Forge.buildBtn({
            'text': c.lang.saveAndClose,
            'id': 'm-ce-addBtn',
            'type': 'default',
            'margin': '22px 83px 22px 11px',
            'float': 'right'
        }).obj

        footerContinueBtn.addEventListener('click', function () {
            // close the modal
            clearNewEngModal();
        })

        footerCancelBtn.addEventListener('click', function () {
            // close the modal
            clearNewEngModal()
        })

        footerBtnHolder.appendChild(footerContinueBtn)
        footerBtnHolder.appendChild(footerCancelBtn)
        footer.appendChild(footerBtnHolder)

        systemContainer.appendChild(header)
        systemContainer.appendChild(titleContainer)
        systemContainer.appendChild(componentSystem)
        systemContainer.appendChild(footer)
        parentContainer.appendChild(systemContainer)

        return parentContainer
    }

    initialize()
}

Modal_ViewPreview.prototype = ModalTemplate.prototype
Modal_ViewPreview.prototype.constructor = Modal_ViewPreview

module.exports = Modal_ViewPreview