'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import styles from './Modal_MobileLogin.css'

var Modal_Login = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  var c = ctrl
  var self = this
  var firstName = ''
  var lastName = ''
  var nameID = 'm-mob-' + self._uniqueID + '-name'

  var initialize = function () {
    log('Modal_Login: initialize')

    self.buildDom()
    self.setBackground('#ffffff')
    self.setDevice({ type: 'Samsung 5', id: '9823759352385923u5923u592'})
  }

  this.buildDom = function () {
    var domContainer = Forge.build({'dom': 'div'})

    var logo = Forge.build({'dom': 'img', 'src': 'images/logos/auvenir/logo_auvenir.svg', 'alt': 'Auvenir', 'style': 'position:absolute; top:42px; left:64px'})
//       var chevron = Forge.build({'dom':'img', 'src':'images/icons/chevron.svg', 'style':'position:absolute; top:50px; right:82px'});
    // var name = Forge.build({'dom':'p', 'id':nameID, 'class':'modal-mobile-name'});

    var container = Forge.build({'dom': 'div', 'class': 'modal-mobile-content'})

    var phoneContainer = Forge.build({'dom': 'div', 'id': 'm-mob-' + self._uniqueID + '-phone-container', 'style': 'position:absolute;top:0px;right:0px;display:none'})
    var phoneImg = Forge.build({'dom': 'img', 'src': 'images/illustrations/phone.svg'})
    var phoneImgLogin = Forge.build({'dom': 'img', 'src': 'images/modal_mobile/20-layers.png'})
    var phoneFullname = Forge.build({'dom': 'p', 'id': 'm-mob-' + self._uniqueID + '-phone-fullname', 'class': 'modal-mobile-phone-title'})
    var phoneDevice = Forge.build({'dom': 'p', 'id': 'm-mob-' + self._uniqueID + '-phone-device', 'class': 'modal-mobile-phone-device'})
    var phoneIcon = Forge.build({'dom': 'img', 'id': 'm-mob-' + self._uniqueID + '-phone-icon', 'class': 'modal-mobile-phone-icon', 'src': 'images/modal_mobile/confirm.svg'})
    var phoneID = Forge.build({'dom': 'p', 'id': 'm-mob-' + self._uniqueID + '-phone-uid', 'class': 'modal-mobile-phone-uid'})

    phoneContainer.appendChild(phoneImg)
    phoneContainer.appendChild(phoneFullname)
    phoneContainer.appendChild(phoneDevice)
    phoneContainer.appendChild(phoneIcon)
    phoneContainer.appendChild(phoneID)

    /** CHECK DEVICE **/
    var CHECKDEVICE_card = Forge.build({'dom': 'div', 'id': 'm-mob-' + self._uniqueID + '-card-checkdevice', 'style': 'text-align:left;width:488px; display:none'})
    var CHECKDEVICE_title = Forge.build({'dom': 'p', 'text': 'Check your device', 'class': 'modal-mobile-card-title'})
    var CHECKDEVICE_desc = Forge.build({'dom': 'p', 'class': 'modal-mobile-card-description', 'text': 'We just sent a push notification to your device. Please swipe the notification to login to Auvenir on your desktop.'})
    var CHECKDEVICE_btnDiv = Forge.build({'dom': 'div', 'style': 'margin-top: 31px;'})
    var CHECKDEVICE_troubleshootBtn = Forge.build({'dom': 'input', 'type': 'button', 'class': 'btn modal-mobile-grayBtn', 'value': 'Troubleshoot'})
    CHECKDEVICE_btnDiv.appendChild(CHECKDEVICE_troubleshootBtn)
    CHECKDEVICE_card.appendChild(CHECKDEVICE_title)
    CHECKDEVICE_card.appendChild(CHECKDEVICE_desc)
    // CHECKDEVICE_card.appendChild(CHECKDEVICE_desc2);
    CHECKDEVICE_card.appendChild(CHECKDEVICE_btnDiv)
    CHECKDEVICE_troubleshootBtn.addEventListener('click', function () { self.setState('login-trouble') })

    // if in login
    var phoneContainerLogin = Forge.build({'dom': 'div', 'id': 'm-mob-' + self._uniqueID + '-phone-containerLogin', 'class': 'modal-mobile-phoneContainer'})
    phoneContainerLogin.appendChild(phoneImgLogin)

    var peopleContainer = Forge.build({'dom': 'div', 'id': 'm-mob-' + self._uniqueID + '-people-container', 'style': 'position:absolute;top:0px;right:0px;display:none'})
    var peopleImg = Forge.build({'dom': 'img', 'src': 'images/modal_mobile/people.png'})
    peopleContainer.appendChild(peopleImg)

    var cardsContainer = Forge.build({'dom': 'div', 'style': 'position:absolute;top:269px;left:251px'})

    /** LOGIN TROUBLE **/
    var LOGINTROUBLE_card = Forge.build({'dom': 'div', 'id': 'm-mob-' + self._uniqueID + '-card-logintrouble', 'style': 'text-align:left;width:562px; display:none;'})
    var title = Forge.build({'dom': 'p', 'text': 'Having trouble logging in?', 'class': 'modal-mobile-card-title-login', 'style': ''})

    var descContainer = Forge.build({'dom': 'ul', 'id': 'container-mehul', 'style': 'width:100%; list-style-type: none; margin:0px; padding-left:0px;'})

    var troubleshoot1LI = Forge.build({'dom': 'li', 'class': 'a-mobile-tr', 'style': 'list-style-type: none;'})
    var troubleshoot2LI = Forge.build({'dom': 'li', 'class': 'a-mobile-tr', 'style': 'list-style-type: none;'})
    var troubleshoot3LI = Forge.build({'dom': 'li', 'class': 'a-mobile-tr', 'style': 'list-style-type: none;'})

    var choiceOneDiv = Forge.build({'dom': 'div', 'class': 'modal-mobile-card-description-login', 'style': 'width:300px; height:35px; padding-bottom:5px;'})
    var choiceOneRadio = Forge.build({'dom': 'input', 'id': 'm-mob-logintrouble-choice-1', 'type': 'radio', 'name': 'radioResp', 'style': 'display:inline-block;'})

    var choiceOneContent = Forge.build({'dom': 'div', 'class': 'modal-mobile-card-description-in', 'text': 'I don\'t have access to my device.', 'style': 'display:inline-block; padding:12px;' })
    var choiceOneErrorContainer = Forge.build({'dom': 'div', 'id': 'modal-mobile-choice-one', 'class': 'modal-mobileLogin-subSection', 'style': 'padding-left:20px' })// 'style':'display: none'
    var choiceOneErrorContent = Forge.build({'dom': 'p', 'class': 'modal-mobile-card-description-sub', 'text': 'To gain access without your device, please enter the email address you use to login to Auvenir. We will send you a login token to that email address.', 'style': '' })
    var choiceOneErrorEmailContainer = Forge.build({'dom': 'div', 'style': 'margin-top: 12px; padding-bottom: 10px;'})
    var choiceOneErrorInputLabel = Forge.build({'dom': 'p', 'text': 'Email Address', 'class': 'modal-mobile-card-description-sub'})
    var choiceOneErrorEmailInput = Forge.build({'dom': 'input', 'id': 'modal-emailInputOne', 'type': 'email', 'placeholder': 'Type your email', 'class': 'land-login-input modal-mobile-input', 'style': 'display: inline-block; margin-top: 6px'})
    var choiceOneErrorEmailSubmit = Forge.build({'dom': 'button', 'id': 'modal-emailBtn', 'text': 'Send', 'class': 'land-login-btn modal-mobile-emailBtn', 'style': 'display: inline-block; margin-top: 6px'})

    // success
    var choiceOneSuccessContainer = Forge.build({'dom': 'div', 'id': 'modal-success-containerOne', 'class': 'modal-login-successContainer', 'style': 'display: none'})
    var choiceOneSuccessCheck = Forge.build({'dom': 'i', 'class': 'fa fa-check modal-login-successCheck'})
    var choiceOneSuccessTxt = Forge.build({'dom': 'span', 'text': 'Email Sent', 'class': 'modal-login-successTxt'})
    choiceOneSuccessContainer.appendChild(choiceOneSuccessCheck)
    choiceOneSuccessContainer.appendChild(choiceOneSuccessTxt)

    // unregistered email
    var choiceOneAlertContainer = Forge.build({'dom': 'div', 'id': 'modal-alert-containerOne', 'class': 'modal-login-alertContainer', 'style': 'display: none;'})
    var choiceOneAlertCheck = Forge.build({'dom': 'i', 'class': 'fa fa-exclamation-triangle modal-login-alertCheck'})
    var choiceOneAlertTxt = Forge.build({'dom': 'span', 'text': 'Please use a registered email', 'class': 'modal-mobile-card-description-sub'})
    choiceOneAlertContainer.appendChild(choiceOneAlertCheck)
    choiceOneAlertContainer.appendChild(choiceOneAlertTxt)

    choiceOneDiv.appendChild(choiceOneRadio)
    choiceOneDiv.appendChild(choiceOneContent)
    choiceOneDiv.appendChild(choiceOneErrorContainer)
    choiceOneErrorEmailContainer.appendChild(choiceOneErrorInputLabel)
    choiceOneErrorEmailContainer.appendChild(choiceOneErrorEmailInput)
    choiceOneErrorEmailContainer.appendChild(choiceOneErrorEmailSubmit)
    choiceOneErrorEmailContainer.appendChild(choiceOneSuccessContainer)
    choiceOneErrorEmailContainer.appendChild(choiceOneAlertContainer)
    choiceOneErrorContainer.appendChild(choiceOneErrorContent)
    choiceOneErrorContainer.appendChild(choiceOneErrorEmailContainer)

    choiceOneRadio.addEventListener('click', function () {
      $(choiceOneErrorContainer).toggleClass('modal-mobileLogin-subSection')
    })

    choiceOneErrorEmailSubmit.addEventListener('click', function () {
      submitEmailModal()
    })

    var choiceTwoDiv = Forge.build({'dom': 'div', 'class': 'modal-mobile-card-description-login', 'style': 'width:300px; height:35px;padding-bottom:5px;'})
    var choiceTwoRadio = Forge.build({'dom': 'input', 'id': 'm-mob-logintrouble-choice-2', 'type': 'radio', 'name': 'radioResp', 'style': 'display:inline-block;'})
    var choiceTwoContent = Forge.build({'dom': 'div', 'class': 'modal-mobile-card-description-in', 'text': 'I replaced my old device with a new one.', 'style': 'display:inline-block;  padding:12px;' })
    var choiceTwoErrorContainer = Forge.build({'dom': 'div', 'id': 'modal-mobile-choice-two', 'class': 'modal-mobileLogin-subSection', 'style': 'padding-left:20px' })// 'style':'display: none'
    var choiceTwoErrorContent = Forge.build({'dom': 'p', 'class': 'modal-mobile-card-description-sub', 'text': 'To register your new device with Auvenir, please start by downloading the mobile application. Type your mobile number below and we can text you a download link.', 'style': '' })
    // Link Texting SMS Integration
    var LTWidgetContainer = Forge.build({'dom': 'div', 'class': 'linkTextingWidget', 'style': 'margin-top:12px;'})
    var LTWidgetLabel = Forge.build({'dom': 'p', 'class': 'modal-mobile-card-description-sub', 'text': 'New Device Phone Number'})
    var LTWidgetInnerContainer = Forge.build({'dom': 'div', 'class': 'linkTextingInner', 'style': 'display: block;'})
    var LTWidgetInputContainer = Forge.build({'dom': 'input', 'class': 'linkID', 'type': 'hidden', 'value': '0258cde2-3a86-4d28-95bb-d14994314fe4'})
    var LTWidgetInputWrapper = Forge.build({'dom': 'div', 'class': 'linkTextingInputWrapper', 'style': 'margin-top: 6px;'})
    var LTWidgetInput = Forge.build({'dom': 'input', 'id': 'numberToText_linkTexting', 'class': 'land-login-input modal-mobile-input modal-mobile-textingInput', 'type': 'tel', 'value': ''})
    var LTWidgetButton = Forge.build({'dom': 'button', 'id': 'sendButton_linkTexting', 'class': 'land-login-btn modal-mobile-emailBtn modal-mobile-textingBtn', 'type': 'button', 'text': 'Text me a Link'})
    var LTWidgetError = Forge.build({'dom': 'div', 'id': 'linkTextingError', 'class': 'linkTextingError' })
    var LTSmsSuccessContainer = Forge.build({'dom': 'div', 'style': 'display: inline-block; width: 200px; position:absolute; top:250px;'})
    var LTSmsSuccessCheckmark = Forge.build({'dom': 'img', 'id': '', 'src': 'images/icons/checkActive.svg', 'class': '', 'style': 'display: inline-block; margin: 10px; margin-left:20px;'})
    var LTSmsSuccessText = Forge.build({'dom': 'div', 'class': '', 'id': '', 'text': 'Sent to your device', 'style': 'display: inline-block; color: #599ba1; width:134px; height:19px;'})
    var linkContainer = Forge.build({'dom': 'div', 'class': 'onboard-appLink', 'style': 'margin-top:12px; padding-bottom: 10px;'})
    var linkMac = Forge.build({'dom': 'img', 'src': 'images/components/macApp.png', 'style': 'width: 98px; margin-right:12px;'})
    var linkGoogle = Forge.build({'dom': 'img', 'src': 'images/components/googleApp.png', 'style': 'width: 98px;'})
    LTWidgetContainer.appendChild(LTWidgetLabel)
    LTWidgetContainer.appendChild(LTWidgetInnerContainer)
    LTWidgetInnerContainer.appendChild(LTWidgetInputContainer)
    LTWidgetInnerContainer.appendChild(LTWidgetInputWrapper)
    LTWidgetInputWrapper.appendChild(LTWidgetInput)
    LTWidgetInputWrapper.appendChild(LTWidgetButton)
    LTWidgetInnerContainer.appendChild(LTWidgetError)
    LTSmsSuccessContainer.appendChild(LTSmsSuccessCheckmark)
    LTSmsSuccessContainer.appendChild(LTSmsSuccessText)
    linkContainer.appendChild(linkMac)
    linkContainer.appendChild(linkGoogle)

    choiceTwoDiv.appendChild(choiceTwoRadio)
    choiceTwoDiv.appendChild(choiceTwoContent)
    choiceTwoDiv.appendChild(choiceTwoErrorContainer)
    choiceTwoErrorContainer.appendChild(choiceTwoErrorContent)
    choiceTwoErrorContainer.appendChild(LTWidgetContainer)
    choiceTwoErrorContainer.appendChild(linkContainer)

    choiceTwoRadio.addEventListener('click', function () {
      $(choiceTwoErrorContainer).toggleClass('modal-mobileLogin-subSection')
    })

    // TO DO - check if the text is actually sent successfully.
    // if yes, then append
    // LTWidgetContainer.appendChild(LTSmsSuccessContainer);

    var choiceThreeDiv = Forge.build({'dom': 'div', 'class': 'modal-mobile-card-description-login', 'style': 'width:300px; height:35px;padding-bottom:5px;'})
    var choiceThreeRadio = Forge.build({'dom': 'input', 'id': 'm-mob-logintrouble-choice-3', 'type': 'radio', 'name': 'radioResp', 'style': 'display:inline-block;'})
    var choiceThreeContent = Forge.build({'dom': 'div', 'class': 'modal-mobile-card-description-in', 'text': 'I need to switch accounts.', 'style': 'display:inline-block; padding:12px;' })
    var choiceThreeErrorContainer = Forge.build({'dom': 'div', 'id': 'modal-mobile-choice-three', 'class': 'modal-mobileLogin-subSection', 'style': 'padding-left:20px; padding-top:10px;' }) // 'style':'display: none'
    var choiceThreeErrorContent = Forge.build({'dom': 'p', 'class': 'modal-mobile-card-description-sub', 'text': 'To gain access as a different account holder, please enter your registered email address below. We will send you a login token to the email address provided.', 'style': '' })
    var choiceThreeErrorEmailContainer = Forge.build({'dom': 'div', 'class': '', 'style': 'margin-top: 6px;' })
    var choiceThreeErrorLabel = Forge.build({'dom': 'p', 'text': 'Email Address', 'class': 'modal-mobile-card-description-sub'})
    var choiceThreeErrorEmailInput = Forge.build({'dom': 'input', 'id': 'modal-emailInputThree', 'type': 'text', 'placeholder': 'Type your email', 'style': 'display: inline-block; margin-top: 6px', 'class': 'land-login-input modal-mobile-input'})
    var choiceThreeErrorEmailSubmit = Forge.build({
      dom: 'button',
      text: 'Send',
      style: 'display: inline-block;margin-top:6px;',
      class: 'land-login-btn modal-mobile-emailBtn'
    })
    var choiceThreeErrorRetry = Forge.build({'dom': 'input', 'type': 'button', 'value': 'Send', 'style': 'display: inline-block'})
    var choiceThreeErrorGetHelp = Forge.build({'dom': 'input', 'type': 'button', 'value': 'Send', 'style': 'display: inline-block'})

    // success
    var choiceThreeSuccessContainer = Forge.build({'dom': 'div', 'id': 'modal-success-containerThree', 'class': 'modal-login-successContainer', 'style': 'display: none'})
    var choiceThreeSuccessCheck = Forge.build({'dom': 'i', 'class': 'fa fa-check modal-login-successCheck'})
    var choiceThreeSuccessTxt = Forge.build({'dom': 'span', 'text': 'Email Sent', 'class': 'modal-login-successTxt'})
    choiceThreeSuccessContainer.appendChild(choiceThreeSuccessCheck)
    choiceThreeSuccessContainer.appendChild(choiceThreeSuccessTxt)

    // unregistered email
    var choiceThreeAlertContainer = Forge.build({'dom': 'div', 'id': 'modal-alert-containerThree', 'class': 'modal-login-successContainer', 'style': 'display: none;'})
    var choiceThreeAlertTimes = Forge.build({'dom': 'i', 'class': 'fa fa-times modal-login-alertTimes'})
    var choiceThreeAlertTxt = Forge.build({'dom': 'span', 'text': 'Whoops. Email failed to send.', 'class': 'modal-login-alertTxt'})
    choiceThreeAlertContainer.appendChild(choiceThreeAlertTimes)
    choiceThreeAlertContainer.appendChild(choiceThreeAlertTxt)

    choiceThreeDiv.appendChild(choiceThreeRadio)
    choiceThreeDiv.appendChild(choiceThreeContent)
    choiceThreeDiv.appendChild(choiceThreeErrorContainer)
    choiceThreeErrorEmailContainer.appendChild(choiceThreeErrorLabel)
    choiceThreeErrorEmailContainer.appendChild(choiceThreeErrorEmailInput)
    choiceThreeErrorEmailContainer.appendChild(choiceThreeErrorEmailSubmit)
    choiceThreeErrorEmailContainer.appendChild(choiceThreeSuccessContainer)
    choiceThreeErrorEmailContainer.appendChild(choiceThreeAlertContainer)
    choiceThreeErrorContainer.appendChild(choiceThreeErrorContent)
    choiceThreeErrorContainer.appendChild(choiceThreeErrorEmailContainer)

    choiceThreeRadio.addEventListener('click', function () {
      $(choiceThreeErrorContainer).toggleClass('modal-mobileLogin-subSection')
    })

    choiceThreeErrorEmailSubmit.addEventListener('click', function () {
      submitEmailModal()
    })

    var cancelBtn = Forge.build({'dom': 'button', 'class': 'btn modal-mobile-grayBtn btn modal-mobile-grayBtn-login ', 'text': 'Cancel', 'style': 'margin-top:20px; height:40px; width:122px; padding:0px'})

    choiceThreeDiv.appendChild(choiceThreeRadio)
    choiceThreeDiv.appendChild(choiceThreeContent)

    troubleshoot1LI.appendChild(choiceOneDiv)
    troubleshoot1LI.appendChild(choiceOneErrorContainer)

    troubleshoot2LI.appendChild(choiceTwoDiv)
    troubleshoot2LI.appendChild(choiceTwoErrorContainer)

    troubleshoot3LI.appendChild(choiceThreeDiv)
    troubleshoot3LI.appendChild(choiceThreeErrorContainer)

    descContainer.appendChild(troubleshoot1LI)
    descContainer.appendChild(troubleshoot2LI)
    descContainer.appendChild(troubleshoot3LI)
    LOGINTROUBLE_card.appendChild(title)
    LOGINTROUBLE_card.appendChild(descContainer)
    LOGINTROUBLE_card.appendChild(cancelBtn)

    cancelBtn.addEventListener('click', function () {
      self.close()
    })

    cardsContainer.appendChild(CHECKDEVICE_card)
    cardsContainer.appendChild(LOGINTROUBLE_card)

    domContainer.appendChild(logo)
    // parentDom.appendChild(name);
    // parentDom.appendChild(chevron);
    container.appendChild(phoneContainer)
    container.appendChild(phoneContainerLogin)
    container.appendChild(peopleContainer)
    container.appendChild(cardsContainer)
    domContainer.appendChild(container)
    return domContainer
  }

  this.setName = function (fN, lN) {
    firstName = fN
    lastName = lN
    document.getElementById('m-mob-' + self._uniqueID + '-name').innerHTML = firstName
    document.getElementById('m-mob-' + self._uniqueID + '-phone-fullname').innerHTML = firstName + ' ' + lastName + '\'s'
  }

  /**
   * Type, ID
   */
  this.setDevice = function (json) {
    document.getElementById('m-mob-' + self._uniqueID + '-phone-device').innerHTML = json.type
    document.getElementById('m-mob-' + self._uniqueID + '-phone-uid').innerHTML = 'UDID: ' + json.id
  }

  this.setState = function (str) {
    switch (str) {
      case 'check-device':
        document.getElementById('m-mob-' + self._uniqueID + '-card-checkdevice').style.display = 'inherit'
        document.getElementById('m-mob-' + self._uniqueID + '-card-logintrouble').style.display = 'none'
        break
      case 'login-trouble':
        document.getElementById('m-mob-' + self._uniqueID + '-card-checkdevice').style.display = 'none'
        document.getElementById('m-mob-' + self._uniqueID + '-card-logintrouble').style.display = 'inherit'
        break
      default:
    }
  }

  initialize()
}

Modal_Login.prototype = ModalTemplate.prototype
Modal_Login.prototype.constructor = Modal_Login

module.exports = Modal_Login
