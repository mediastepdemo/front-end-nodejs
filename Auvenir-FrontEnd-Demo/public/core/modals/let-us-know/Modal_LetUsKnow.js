'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import styles from './Modal_LetUsKnow.css'
/**
 * The modal is responsible for the file storage modal.
 * @class Modal_FileStorage
 * @param: {Object} ctrl     - appController for the current user
 * @param: {Object} settings - settings of the current user
 */
var Modal_FileStorage = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)
  var self = this
  var c = ctrl
  var text = c.lang.letUsKnowModal

  /**
   * This function is responsible for building the page only when called
   * @memberof Modal_FileStorage
   */
  this.buildDom = function () {
    var container = Forge.build({ dom: 'div', id: 'm-fs-modal-container'})
    var subText = Forge.build({ dom: 'div', text: text.subtext, class: 'm-fs-div'})
    var textBox = Forge.buildInputTextArea({ label: '', errorText: ' ', id: 'm-fs-textinput', width: '255px', height: '78px;' })

    var cancelBtn = Forge.buildBtn({text: c.lang.cancel, type: 'secondary', width: '128px', margin: '32px 4px 0px'}).obj

    cancelBtn.addEventListener('click', function (event) {
      event.preventDefault()
      self.close()
    })

    var confirmBtn = Forge.buildBtn({text: c.lang.send, type: 'primary', width: '128px', margin: '32px 4px 0px'}).obj

    confirmBtn.addEventListener('click', function () {
      c.sendEvent({ name: 'send-suggestion', data: {userID: c.currentUser._id, text: document.getElementById('m-fs-textinput').value, feedbackSource: 'File Storage'} })
      self.close()
    })

    container.appendChild(subText)
    container.appendChild(textBox)
    container.appendChild(cancelBtn)
    container.appendChild(confirmBtn)
    return container
  }

  self.setModalName('File Storage')
  self.setClose({window: true, global: false})
  self.setWidth('480px')
  self.setHeight('492px')
  self.setTitle(text.title)
  self.setHeader({show: true, color: '#363A3C'})
  self.setPosition({top: '100px'})
  self.build()

  /**
   * @memberof Modal_FileStorage
   */
  this.loadData = function () {
    // var templateText = 'Are you sure you would like to delete</br>'
    // templateText += self.request.name + ' ? '
    // templateText += 'You will lose all sub-requests as well.'
    // document.getElementById('delete-request-modal-tip').innerHTML = templateText
  }
}

module.exports = Modal_FileStorage
