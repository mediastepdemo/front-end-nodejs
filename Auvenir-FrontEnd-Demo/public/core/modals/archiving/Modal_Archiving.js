'use strict'

import ModalTemplate from '../ModalTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'

import styles from './Modal_Archiving.css'
/**
 * The Modal_Archiving is responsible for display the progress of archiving the engagement.
 * @class Modal_Archiving
 *
 * @param {Object} ctrl   - A reference to the current instance of the Main Application Controller (Controller.js)
 * @param {Object} settings - The custom settings that are used to generate the instance of this modal.
*/
var Modal_Archiving = function (ctrl, settings) {
  ModalTemplate.call(this, ctrl, settings)

  var c = ctrl

  var self = this
  var loadTimeout

  var errorArchiving = false
  var archiveComplete = false
  var animationComplete = false

  /**
   * Responsible for building the DOM elements and attaching them to the parent modal container.
   * @memberof Modal_Archiving
  */
  this.buildDom = function () {
    var domContainer = Forge.build({'dom': 'div'})
    var animationContainer = Forge.build({'dom': 'div', 'id': 'm-ae-submitContainer', 'class': 'm-archive-deleting'})
    var imgPlanesBox = Forge.build({'dom': 'div', 'class': 'm-archive-cover'})
    var imgPlanes = Forge.build({'dom': 'img', 'src': 'images/illustrations/text.jpg', 'class': 'm-archive-text'})

    animationContainer.appendChild(imgPlanesBox)
    animationContainer.appendChild(imgPlanes)

    var doneContainer = Forge.build({'dom': 'div', 'id': 'm-ae-doneContainer', 'style': 'display:none; margin-top: 200px;'})
    var doneCongratz = Forge.build({'dom': 'p', 'style': 'width: 100%', 'id': 'm-ae-doneCongratz', 'text': 'Congratulations!'})
    var doneText = Forge.build({'dom': 'p', 'style': 'width: 100%', 'id': 'm-ae-doneText', 'text': 'Your engagement has been archived'})
    var imgBox = Forge.build({'dom': 'img', 'src': 'images/illustrations/box.svg', 'style': 'display:block; margin:35px auto;'})
    var doneBtn = Forge.build({'dom': 'input', 'type': 'button', 'class': 'btn btn-lg', 'value': 'Return to homepage', 'style': 'width:185px; margin-bottom:20px;'})
    doneContainer.appendChild(doneCongratz)
    doneContainer.appendChild(doneText)
    doneContainer.appendChild(imgBox)
    doneContainer.appendChild(doneBtn)

    domContainer.appendChild(animationContainer)
    domContainer.appendChild(doneContainer)

    doneBtn.addEventListener('click', function () {
      self.trigger('done')
      self.close()
      reset()
    })

    return domContainer
  }

  /**
   * This sets the modal back to its original state.
   * @memberof Modal_Archiving
  */
  var reset = function () {
    document.getElementById('m-ae-submitContainer').style.display = 'inherit'
    document.getElementById('m-ae-doneContainer').style.display = 'none'
  }

  /**
   * Is triggered when there is an update to the engagement being pushed to the client side.
   * @memberof Modal_Archiving
   *
   * @param {String} tag - "update-engagement" event tag
   * @param {Object} result - The engagement information that was returned
   */
  var engagementUpdated = function (tag, result) {
    log('Modal_Archiving: Response triggered for ' + tag)

    if (c.currentEngagement) {
      var status = c.currentEngagement.status

      if (status === 'ARCHIVED') {
        archiveComplete = true
      } else {
        archiveComplete = false
      }
    }
  }

  /**
   * Sends a request to the server to submit the audit.
   * @memberof Modal_Archiving
   *
   * @param {String} taskID - ID of the task
   */
  this.startArchiving = function (taskID) {
    setTimeout(function () {
      document.getElementById('m-ae-submitContainer').classList.add('m-archive-slide')
    }, 5)

    archiveComplete = false
    if (c.currentEngagement) {
      if (!taskID) {
        responseError()
        return
      }

      c.updateEngagement({engagementID: c.currentEngagement._id, status: 'ARCHIVED'})
      setTimeout(function () {
        loadTimeout = setTimeout(function () {
          animationComplete = true
          responseSuccess()
        }, 3000)
      }, 1)
    } else {
      responseError()
    }
  }

  /**
   * Sets error and triggers the appropriate div to display
   * @memberof Modal_Archiving
  */
  var responseError = function () {
    errorArchiving = true
    isRequestComplete()
  }

  /**
   * Sets success and triggers the appropriate div to display
   * @memberof Modal_Archiving
  */
  var responseSuccess = function () {
    log('Archiving Engagement was completed successfully.')
    archiveComplete = true
    isRequestComplete()
  }

  /**
   * Triggers the change to complete if request completes successfully otherwise displays an error
   * @memberof Modal_Archiving
  */
  var isRequestComplete = function () {
    if (animationComplete && archiveComplete && !errorArchiving) {
      triggerChange()
    } else {
      triggerError()
    }
  }

  /**
   * Sets the finished container on the modal
   * @memberof Modal_Archiving
  */
  var triggerChange = function () {
    document.getElementById('m-ae-submitContainer').style.display = 'none'
    document.getElementById('m-ae-doneContainer').style.display = 'inherit'
  }

  /**
   * Sets the finished container on the modal, and display an error
   * @memberof Modal_Archiving
  */
  var triggerError = function () {
    document.getElementById('m-ae-submitContainer').style.display = 'none'
    document.getElementById('m-ae-doneContainer').style.display = 'inherit'

    document.getElementById('m-ae-doneCongratz').innerHTML = 'An error has occurred.'
    document.getElementById('m-ae-doneText').innerHTML = 'Archiving has failed. Please contact Auvenir for assistance.'
  }

  self.setModalName('arch')
  self.setClose({'window': true})
  self.setFullScreen(true)
  self.setHeader({'show': false})
  self.setPosition({ 'bottom': '0px'})
  self.build()
  c.registerForEvent('update-engagement', 'm-ae-00000001', engagementUpdated)
}

Modal_Archiving.prototype = ModalTemplate.prototype
Modal_Archiving.prototype.constructor = Modal_Archiving

module.exports = Modal_Archiving
