/* globals $ */

'use strict'

import log from '../js/src/log'
import '../css/lib/semantic-ui/components/grid.css'
import '../css/lib/semantic-ui/components/table.css'
import '../css/lib/semantic-ui/components/checkbox.css'
import '../css/lib/semantic-ui/components/checkbox.js'
import '../css/lib/semantic-ui/components/dropdown.css'
import '../css/lib/semantic-ui/components/dropdown.js'
import '../css/lib/semantic-ui/accordion.css'
import '../css/lib/semantic-ui/components/input.css'
import styles from './theme.css'

var Forge = {

  ALPHANUMERIC: '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
  ID_LENGTH: 8,

  /**
   * Responsible for building the dom object based on parameters that are passed in as options.
   * If any of the options aren't supported, they are simply ignored.
   *
   * @param {Object} options:  Json Object of Option for DOM Object
   * @return {Object} Corresponding DOM Object according to options.
   */
  build: function (options) {
    var node = document.createElement(options.dom)

    if (options.type) {
      node.setAttribute('type', options.type)
    }
    if (options.id) {
      node.setAttribute('id', options.id)
    }
    if (options.class) {
      node.setAttribute('class', options.class)
    }
    if (options.style) {
      node.setAttribute('style', options.style)
    }
    if (options.url) {
      node.setAttribute('url', options.url)
    }
    if (options.src) {
      node.setAttribute('src', options.src)
    }
    if (options.alt) {
      node.setAttribute('alt', options.alt)
    }
    if (options.a) {
      node.setAttribute('a', options.a)
    }
    if (options.value) {
      node.setAttribute('value', options.value)
    }
    if (options.placeholder) {
      node.setAttribute('placeholder', options.placeholder)
    }
    if (options.rel) {
      node.setAttribute('rel', options.rel)
    }
    if (options.for) {
      node.setAttribute('for', options.for)
    }
    if (options.checkmark) {
      node.setAttribute('checkmark', options.checkmark)
    }
    if (options.title) {
      node.setAttribute('title', options.title)
    }
    if (options.accept) {
      node.setAttribute('accept', options.accept)
    }
    if (options.role) {
      node.setAttribute('role', options.role)
    }
    if (options.pattern) {
      node.setAttribute('pattern', options.pattern)
    }
    if (options['data-toggle']) {
      node.setAttribute('data-toggle', options['data-toggle'])
    }
    if (options['aria-controls']) {
      node.setAttribute('aria-controls', options['aria-controls'])
    }
    if (options['aria-label']) {
      node.setAttribute('aria-label', options['aria-label'])
    }
    if (options['aria-hidden']) {
      node.setAttribute('aria-hidden', options['aria-hidden'])
    }
    if (options['data-geo']) {
      node.setAttribute('data-geo', options['data-geo'])
    }
    if (options['data-keywords']) {
      node.setAttribute('data-keywords', options['data-keywords'])
    }
    if (options['data-tooltip']) {
      node.setAttribute('data-tooltip', options['data-tooltip'])
    }
    if (options.name) {
      node.setAttribute('name', options.name)
    }
    if (options.action) {
      node.setAttribute('action', options.action)
    }
    if (options.method) {
      node.setAttribute('method', options.method)
    }
    if (options.checked) {
      node.setAttribute('checked', '')
    }
    if (options.href) {
      node.setAttribute('href', options.href)
    }
    if (options.target) {
      node.setAttribute('target', options.target)
    }
    if (options.onclick) {
      node.setAttribute('onclick', options.onclick)
    }
    if (options.maxlength) {
      node.setAttribute('maxlength', options.maxlength)
    }
    if (options.text) {
      var textNode = document.createTextNode(options.text)
      node.appendChild(textNode)
    }
    if (options.spellcheck) {
      node.setAttribute('spellcheck', options.spellcheck)
    }
    if (options.autocomplete) {
      node.setAttribute('autocomplete', options.autocomplete)
    }
    if (options.autocorrect) {
      node.setAttribute('autocorrect', options.autocorrect)
    }
    if (options.autocapitalize) {
      node.setAttribute('autocapitalize', options.autocapitalize)
    }
    if (options.autofocus) {
      node.setAttribute('autofocus', true)
    }
    if (options.multiple) {
      node.setAttribute('multiple', '')
    }
    if (options.rows) {
      node.setAttribute('rows', options.rows)
    }
    if (options['data-placeholder']) {
      node.setAttribute('data-placeholder', options['data-placeholder'])
    }

    return node
  },

  /*
   * Return empty div Dom object.
   *
   * @param {Object} Json object of data.
   * @return {object} Dom object of empty div.
   */
  template: function (mold) {
    var box = Forge.build({'dom': 'div'})

    return box
  },

  /**
   * Return Button Dom Object according to options in mold.
   *
   * @param      {array}    (mold)                          Json object of options for Button (Option Examples: text, id, button size)
   * @property   {string}   (required)   mold.text          Text displayed in button.
   * @property   {string}   (optional)   mold.id            ID for button
   * @property   {string}   (optional)   mold.type          Type of button form style guide
   * @property   {string}   (optional)   mold.width         Set width, should only be used in rare occasions
   * @property   {string}   (optional)   mold.margin        Set margins for button if needed
   * @property   {string}   (optional)   mold.float         Set float of button if needed
   * @property   {array}    (optional)   mold.buttonEvent   Set function for on click event
   *
   * Functions passed in using mold.itemEvent or mold.buttonEvent should be built like;
   var btnFunction = function (evnt) {
      CODE GOES HERE
    };
   *
   * @return     {array}
   * @property   {object}   mold.object   DOM element to be returned
   * @property   {string}   mold.id       ID for button
   *
   * Forge.buildBtn({text: 'required', id: '', type: '', width: '', margin: '', float: '', buttonEvent: []});
   */
  buildBtn: function (mold) {
    if (!mold.text) {
      return null
    }
    // id:String || uniqueID || null?,
    // checks if id was passed, if not assigns uniqueID
    var btnID = ''
    if (!mold.id) {
      btnID = 'forge-btn-' + this.getUniqueID()
      log('Forge setting uniqueID:' + btnID)
    } else {
      btnID = mold.id
      log('Forge setting id to: ' + mold.id)
    }

    // type:'primary || secondary || default || light || warning || ghost || link || dlink',
    var cls = 'auvbtn '
    switch (mold.type) {
      case 'primary':
        cls = cls + 'primary'
        break
      case 'primarySmall':
        cls = cls + 'primarySmall'
        break
      case 'secondary':
        cls = cls + 'secondary'
        break
      case 'darkblue':
        cls = cls + 'darkblue'
        break
      case 'default':
        cls = cls + 'default'
        break
      case 'light':
        cls = cls + 'light'
        break
      case 'warning':
        cls = cls + 'warning'
        break
      case 'ghost':
        cls = cls + 'ghost'
        break
      case 'link':
        cls = cls + 'link'
        break
      case 'dlink':
        cls = cls + 'dlink'
        break
      default:
        cls = cls + ' default'
    }

    /*
     * Nix all this and just have style sent? I'd rather cut off the available options to change standard buttons.
     * May even make users put buttons in divs and havbe them move the div to position the button but I don't like that either. -TJEP
     */
    var stl = ''

    // width:String || null,
    if (mold.width) {
      stl = stl + ' width:' + mold.width + ';'
    }

    // margin:String || null,
    if (mold.margin) {
      stl = stl + ' margin:' + mold.margin + ';'
    }

    // margin:String || null,
    if (mold.padding) {
      stl = stl + ' padding:' + mold.padding + ';'
    }

    // float:String || null,
    if (mold.float) {
      stl = stl + ' float:' + mold.float + ';'
    }

    // display:String || null,
    if (mold.display) {
      stl = stl + ' display:' + mold.display + ';'
    }

    var node = Forge.build({'dom': 'button', 'text': mold.text, 'id': btnID, 'style': stl, 'class': cls})

    if (mold.buttonEvent) {
      for (var i = 0; i < mold.buttonEvent.length; i++) {
        node.addEventListener('click', mold.buttonEvent[i])
      }
    }

    var cast = {}
    cast.obj = node
    cast.id = btnID

    return cast
  },

  /**
   * Return Input Box Dom Object according to options in mold.
   *
   * @param      {Object}                                   Json object of options for Input box (Option Examples: block, id)
   * @property   {string}   (required)   mold.label         Label to be added to the Input
   * @property   {string}   (optional)   mold.id            ID to be assigned to input or could be auto assigned
   * @property   {string}   (optional)   mold.placeholder   Placeholder for Input
   * @property   {string}   (optional)   mold.block         Whether the block is displayed side by side or one after another.
   * @property   {string}   (optional)   mold.width         You may set the width of the input field
   *
   * @return {Object} Div Dom object that consists of inputbox.
   *
   * Forge.buildInputBox({'label':'required', 'id':'', 'placeholder':'', 'block':'', 'width':''});
   */
  buildInputBox: function (mold) {
    if (!mold.label) {
      return null
    }

    var blockCls = (mold.block !== 'inline') ? 'blockInput-single' : 'blockInput-double'
    var box = Forge.build({'dom': 'div', 'class': blockCls})
    var inputID = ''
    if (!mold.id) {
      inputID = 'forge-InputBox-' + this.getUniqueID()
      log('Forge setting uniqueID:' + inputID)
    } else {
      inputID = mold.id
      log('Forge setting id to: ' + inputID)
    }

    var label = Forge.build({'dom': 'label', 'id': inputID + '-error', 'for': inputID, 'class': 'block-input-title', 'text': mold.label})
    var textInput = Forge.build({'dom': 'input', 'id': inputID, 'class': 'blockInput', 'placeholder': mold.placeholder})

    if (mold.width) {
      textInput.setAttribute('style', 'width: ' + mold.width + ';')
    }

    box.appendChild(label)
    box.appendChild(textInput)

    return box
  },

  /**
   * Build out a standard input with Title, input field and error.
   *
   * @property   {string}   (required)   mold.label         Label needs to be given to describe input
   * @property   {string}   (required)   mold.errorText     Error text to be displayed if there is an error
   * @property   {string}   (optional)   mold.id            ID to be assigned to input or could be auto assigned
   * @property   {string}   (optional)   mold.placeholder   Placeholder will not likely be used, but incase it is needed
   * @property   {string}   (optional)   mold.width         If width is diffrent than default you may override it here
   * @property   {string}   (optional)   mold.errorLeft     If the errorIcon needs to be repositioned
   *
   * @return {string} Return Unique id that structures prepand-ID-append.
   *
   * Forge.buildInput({'label':'required', 'errorText':'required', 'id':'', 'placeholder':'', 'width':'', 'errorLeft':''});
   */
  buildInput: function (mold) {
    if (!mold.errorText) {
      return null
    }

    var inputContainer = Forge.build({'dom': 'div'})

    var inputID = ''
    if (!mold.id) {
      inputID = 'forge-InputBox-' + this.getUniqueID()
      log('Forge setting uniqueID:' + inputID)
    } else {
      inputID = mold.id
      log('Forge setting id to: ' + inputID)
    }

    var titleLabel = Forge.build({'dom': 'p', 'for': inputID, 'class': 'auv-inputTitle', 'text': mold.label})
    var textInput = Forge.build({'dom': 'input', 'id': inputID, 'class': 'auv-input', 'placeholder': mold.placeholder})
    var error = Forge.build({'dom': 'p', 'class': 'auv-inputError', 'text': mold.errorText})
    var errorIcon = Forge.build({'dom': 'p', 'class': 'auvicon-caution auv-inputErrorIcon'})

    if (mold.width) {
      textInput.setAttribute('style', 'width: ' + mold.width + ';')
      errorIcon.setAttribute('style', 'margin-left: ' + (mold.width.split('p')[0] - 31) + 'px;')
    }

    if (mold.errorLeft) {
      errorIcon.setAttribute('style', 'margin-left: ' + (mold.errorLeft.split('p')[0] - 31) + 'px;')
    }

    if (mold.type) {
      textInput.setAttribute('type', mold.type)
    }

    if (mold.pattern) {
      textInput.setAttribute('pattern', mold.pattern)
    }

    if (mold.maxlength) {
      textInput.setAttribute('maxlength', mold.maxlength)
    }

    inputContainer.appendChild(titleLabel)
    inputContainer.appendChild(textInput)
    inputContainer.appendChild(error)
    inputContainer.appendChild(errorIcon)

    return inputContainer
  },

  /**
   * Build out a standard input text area with Title, and input field.
   *
   * @property   {string}   (required)   mold.label         Label needs to be given to describe input
   * @property   {string}   (required)   mold.errorText     Error text to be displayed if there is an error
   * @property   {string}   (optional)   mold.id            ID to be assigned to input or could be auto assigned
   * @property   {string}   (optional)   mold.placeholder   Placeholder will not likely be used, but incase it is needed
   * @property   {string}   (optional)   mold.width         If width is diffrent than default you may override it here
   * @property   {string}   (optional)   mold.height        If height is diffrent than default you may override it here
   *
   * @return {string} Return Unique id that structures prepand-ID-append.
   *
   * Forge.buildInputTextArea({'label':'', 'id':'', 'placeholder':'', 'width':'', 'height':''});
   */
  buildInputTextArea: function (mold) {
    var inputContainer = Forge.build({'dom': 'div'})

    var inputID = ''
    if (!mold.id) {
      inputID = 'forge-InputBox-' + this.getUniqueID()
      log('Forge setting uniqueID:' + inputID)
    } else {
      inputID = mold.id
      log('Forge setting id to: ' + inputID)
    }

    var titleLabel = Forge.build({'dom': 'p', 'for': inputID, 'class': 'auv-inputTitle', 'text': mold.label})
    var textInput = Forge.build({'dom': 'textarea', 'id': inputID, 'class': 'auv-input auv-input-textArea', 'placeholder': mold.placeholder})
    var error = Forge.build({'dom': 'p', 'class': 'auv-inputError', 'text': mold.errorText})
    var errorIcon = Forge.build({'dom': 'p', 'class': 'auvicon-caution auv-inputErrorIcon'})

    if (mold.width && mold.height) {
      textInput.setAttribute('style', 'width: ' + mold.width + '; height: ' + mold.height)
      errorIcon.setAttribute('style', 'margin-left: ' + mold.width + ';')
    } else if (mold.width) {
      textInput.setAttribute('style', 'width: ' + mold.width + ';')
      errorIcon.setAttribute('style', 'margin-left: ' + mold.width + ';')
    } else if (mold.height) {
      textInput.setAttribute('style', 'height: ' + mold.height + ';')
    }

    inputContainer.appendChild(titleLabel)
    inputContainer.appendChild(textInput)
    inputContainer.appendChild(error)
    inputContainer.appendChild(errorIcon)

    return inputContainer
  },

  /**
   * Build out a standard input and dropdown list with Title, input field and error.
   *
   * @property   {string}   (required)   mold.label         Label needs to be given to describe input
   * @property   {string}   (required)   mold.errorText     Error text to be displayed if there is an error
   * @property   {string}   (optional)   mold.id            ID to be assigned to input or could be auto assigned
   * @property   {string}   (optional)   mold.placeholder   Placeholder will not likely be used, but incase it is needed
   * @property   {string}   (optional)   mold.width         If width is diffrent than default you may override it here
   *
   * @return {string} Return Unique id that structures prepand-ID-append.
   *
   * Forge.buildInputDropdown({'label':'', 'list':'required', id':'', 'placeholder':'', 'width':'', 'style':'', 'chevronStyle':'', errorText: ''});
   */
  buildInputDropdown: function (mold) {
    if (!mold.list) {
      return null
    }

    var inputID = ''
    if (!mold.id) {
      inputID = 'forge-InputBox-' + this.getUniqueID()
      log('Forge setting uniqueID:' + inputID)
    } else {
      inputID = mold.id
      log('Forge setting id to: ' + inputID)
    }
    var inputContainer = Forge.build({'dom': 'div', 'class': 'auv-inputDD-container', 'id': inputID + '-container'})
    var titleLabel = Forge.build({'dom': 'p', 'for': inputID, 'class': 'auv-inputTitle', 'text': mold.label})
    var textInput = Forge.build({'dom': 'input', 'id': inputID, 'class': 'auv-input auv-inputDD', 'placeholder': mold.placeholder, 'style': mold.style})
    var chevronDown = Forge.build({'dom': 'div', 'class': 'auvicon-chevron-down auv-chevronDown', 'style': mold.chevronStyle})

    var attachMenu = Forge.build({'dom': 'ul', 'class': 'ddlLink inputDdl'})

    mold.list.forEach((listItem) => {
      var item = Forge.build({'dom': 'li', 'value': listItem.value, 'class': 'ddlItem auv-inputDDL-li'})
      var text = Forge.build({'dom': 'a', 'text': listItem.text, 'class': 'ddlText auv-inputDdl-text'})
      item.appendChild(text)
      attachMenu.appendChild(item)

      if (typeof window.addEventListener === 'function') {
        item.addEventListener('mousedown', function () {
          if (mold.onSelect) {
            mold.onSelect(listItem)
          }
          listSelection(listItem.text)
        })
      }
    })

    inputContainer.appendChild(titleLabel)
    inputContainer.appendChild(chevronDown)
    inputContainer.appendChild(textInput)
    if (mold.errorText) {
      var error = Forge.build({'dom': 'p', 'class': 'auv-inputError', 'text': mold.errorText})
      // var errorIcon = Forge.build({'dom': 'p', 'class': 'auvicon-caution auv-inputErrorIcon auv-ddlErrorIcon'})
      inputContainer.appendChild(error)
      // inputContainer.appendChild(errorIcon)
    }
    inputContainer.appendChild(attachMenu)

    inputContainer.addEventListener('click', function () {
      attachMenu.classList.toggle('inputDdl-after')
    })

    textInput.addEventListener('blur', function () {
      attachMenu.classList.remove('inputDdl-after')
    })

    textInput.addEventListener('keyup', (e) => {
      attachMenu.classList.remove('inputDdl-after')
      if (textInput.value.trim() === '') {
        attachMenu.classList.add('inputDdl-after')
      }
    })

    textInput.addEventListener('keydown', (e) => {
      const charCode = e.which || e.keyCode
      if (charCode === 9) {
        attachMenu.classList.remove('inputDdl-after')
      }
    })

    if (mold.width && !mold.style) {
      var widthFull = mold.width.substr(0, mold.width.length - 2)
      var leftChevron = widthFull - 19
      attachMenu.setAttribute('style', 'width: ' + mold.width + ';')
      textInput.setAttribute('style', 'width: ' + mold.width + ';')
      chevronDown.setAttribute('style', 'left: ' + leftChevron + 'px;')
    } else if (mold.width && mold.style) {
      attachMenu.setAttribute('style', 'width: ' + mold.width + ';')
    }

    var listSelection = function (item) {
      textInput.value = item
    }

    return inputContainer
  },

  /**
   * Build out a standard input for a dropdown with a Calendar attached;
   *
   * @property   {string}   (required)   mold.label         Label needs to be given to describe input
   * @property   {string}   (required)   mold.errorText     Error text to be displayed if there is an error
   * @property   {string}   (optional)   mold.id            ID to be assigned to input or could be auto assigned
   * @property   {string}   (optional)   mold.placeholder   Placeholder will not likely be used, but incase it is needed
   * @property   {string}   (optional)   mold.width         If width is diffrent than default you may override it here
   * @property   {string}   (required)   mold.errorText     Error text to be displayed if there is an error
   *
   * @return {string} Return Unique id that structures prepand-ID-append.
   *
   * Forge.buildInputDropdownCalendar({'label': '', 'id': '', 'placeholder': '', 'width': '', errorText: ''})
   */
  buildInputDropdownCalendar: function (mold) {
    var inputContainer = Forge.build({'dom': 'div', 'class': 'auv-inputDD-container'})
    var inputID = ''
    if (!mold.id) {
      inputID = 'forge-InputBox-' + this.getUniqueID()
      log('Forge setting uniqueID:' + inputID)
    } else {
      inputID = mold.id
      log('Forge setting id to: ' + inputID)
    }

    if (mold.label) {
      var titleLabel = Forge.build({'dom': 'p', 'for': inputID, 'class': 'auv-inputTitle', 'text': mold.label})
      inputContainer.appendChild(titleLabel)
    }
    var textInput = Forge.build({'dom': 'input', 'id': inputID, 'class': 'auv-input auv-inputDD', 'placeholder': mold.placeholder})
    var chevronDown = Forge.build({'dom': 'div', 'class': 'auvicon-chevron-down auv-chevronDown'})
    if (mold.width) {
      textInput.setAttribute('style', 'width: ' + mold.width + ';')
      chevronDown.setAttribute('style', 'width: ' + mold.width - 19 + ';')
    }

    if (mold.width && !mold.style) {
      var widthFull = mold.width.substr(0, mold.width.length - 2)
      var leftChevron = widthFull - 19
      chevronDown.setAttribute('style', 'left: ' + leftChevron + 'px;')
    }

    inputContainer.appendChild(chevronDown)
    inputContainer.appendChild(textInput)
    if (mold.errorText) {
      var error = Forge.build({'dom': 'p', 'class': 'auv-inputError', 'text': mold.errorText})
      // var errorIcon = Forge.build({'dom': 'p', 'class': 'auvicon-caution auv-inputErrorIcon auv-ddlErrorIcon'})
      inputContainer.appendChild(error)
      // inputContainer.appendChild(errorIcon)
    }

    return inputContainer
  },

  /**
   * Build out a input for autoComplete from a predetermined list.
   *
   * @property   {string}   (required)   mold.label         Label needs to be given to describe input
   * @property   {string}   (required)   mold.errorText     Error text to be displayed if there is an error
   * @property   {string}   (optional)   mold.id            ID to be assigned to input or could be auto assigned
   * @property   {string}   (optional)   mold.placeholder   Placeholder will not likely be used, but incase it is needed
   * @property   {string}   (optional)   mold.width         If width is diffrent than default you may override it here
   *
   * @return {string} Return Unique id that structures prepand-ID-append.
   *
   * Forge.buildInput({'label':'',  'id':''required, 'list':'required array', 'placeholder':'', 'width':''});
   */
  buildInputAutoComplete: function (mold) {
    if (!mold.id) {
      return null
    }
    if (!mold.list) {
      return null
    }

    var inputContainer = Forge.build({'dom': 'div', 'style': mold.style})

    var inputID = ''
    if (!mold.id) {
      inputID = 'forge-InputBox-' + this.getUniqueID()
      log('Forge setting uniqueID:' + inputID)
    } else {
      inputID = mold.id
      log('Forge setting id to: ' + inputID)
    }
    var availableTags = mold.list

    var titleLabel = Forge.build({'dom': 'p', 'for': inputID, 'class': 'auv-inputTitle', 'text': mold.label})
    var textInput = Forge.build({'dom': 'input', 'autocomplete': 'off', 'id': inputID, 'class': 'auv-input', 'placeholder': mold.placeholder})

    if (mold.width) {
      textInput.setAttribute('style', 'width: ' + mold.width + ';')
    }

    inputContainer.appendChild(titleLabel)
    inputContainer.appendChild(textInput)

    var JQinputID = '#' + inputID
    $(document).ready(function () {
      $(JQinputID).autocomplete({
        source: availableTags
      })
    })

    return inputContainer
  },

  /**
   * Return the Upload Button according to option mold.
   *
   * @property   {string}     (required)   mold.id               ID of button
   * @property   {Function}   (required)   mold.uploadFunction   Pass in functionality for uploaded file
   * @property   {string}     (optional)   mold.class            styling needed for button
   * @property   {string}     (optional)   mold.src              image sorce for button
   *
   * @return {Object} Return Dom object of Upload button.
   *
   * Forge.buildUploadButton({'uploadFunction':'required', 'id':'required', 'class':'', 'src':''});
   */
  buildUploadButton: function (mold) {
    if (!mold.id) {
      return null
    }
    if (!mold.uploadFunction) {
      return null
    }

    var fieldEditForm = Forge.build({'dom': 'form', 'method': 'POST', 'enctype': 'multipart/form-data'})
    var fieldEditInput = Forge.build({'dom': 'input', 'type': 'file', 'style': 'display:none;'})

    var button = Forge.build({'dom': 'img', 'id': mold.id, 'src': mold.src, 'class': mold.class, 'style': 'display: inline-block;'})
    fieldEditForm.appendChild(fieldEditInput)
    button.appendChild(fieldEditForm)

    button.addEventListener('click', function () {
      fieldEditInput.click()
    })

    fieldEditInput.addEventListener('change', function (event) {
      var i = 0
      var files = fieldEditInput.files
      if (files) {
        for (i = 0; i < files.length; i++) {
          mold.uploadFunction(fieldEditInput.files[i])
        }
      } else {
        log('No files uploaded')
      }
    }, false)

    return button
  },

  /**
   * Return Check Box Dom Object according to options in mold.
   *
   * @param {Object} Json object of options for Check box (Option Examples: text, id)
   *
   * @property {string} (required) mold.id
   * @property {string} (required) mold.text
   * @property {string} (optional) mold.title
   *
   * @return {Object} Div Dom object that consists of Checkbox.
   *
   * Forge.buildCheckbox({'id':'required', 'text':'required', 'title':''});
   */
  buildCheckbox: function (mold) {
    if (!mold.id) {
      return null
    }
    if (!mold.text) {
      return null
    }

    var box = Forge.build({'dom': 'div'})

    var title
    if (mold.title) {
      title = Forge.build({'dom': 'label', 'class': 'block-input-title', 'text': mold.title})
      box.appendChild(title)
    }

    var text = Forge.build({'dom': 'label', 'for': mold.id, 'class': 'checkmarkText', 'text': mold.text})
    var check = Forge.build({'dom': 'input', 'type': 'checkbox', 'id': mold.id, 'class': 'auvenir-checkbox'})

    box.appendChild(text)
    box.appendChild(check)

    return box
  },

  /**
   * Return Header Block title according to mold options
   *
   * @param {Object} JSON Object Options for Header block title. (ex: text, title).
   * @property {string} (required) mold.title
   * @property {string} (required) mold.text
   *
   * @return {Object} Div Dom Object that consists of Block Title.
   *
   * Forge.buildBlockTitle({'title':'required', 'text':'required'});
   */
  buildBlockTitle: function (mold) {
    if (!mold.title) {
      return null
    }
    if (!mold.text) {
      return null
    }

    var box = Forge.build({'dom': 'div'})
    var title
    var text
    if (mold.header) {
      title = Forge.build({'dom': 'h3', 'text': mold.title})
      text = Forge.build({'dom': 'p', 'class': 'block-text-header', 'text': mold.text})
    } else {
      title = Forge.build({'dom': 'h4', 'class': 'block-title', 'text': mold.title})
      text = Forge.build({'dom': 'p', 'class': 'block-subsection-instructions', 'text': mold.text})
    }

    box.appendChild(title)
    box.appendChild(text)

    return box
  },

  /**
   * Return Dropdown Select DOM objects according to mold options
   *
   * @param {Object} Json object of options for Select
   * title:string, id:string, defaultText:string, options:[string,string,string,...]
   *
   * @property {string} (required) mold.id
   * @property {string} (required) mold.defaultText
   * @property {string} (required) mold.options
   * @property {string} (optional) mold.title
   *
   * @return {Object} Div Dom Object that has dropdown select options.
   *
   * Forge.buildDropdown({'id':'required', 'defaultText':'required', 'options':'required', 'title':''});
   */
  buildDropdown: function (mold) {
    if (!mold.id) {
      return null
    }
    if (!mold.defaultText) {
      return null
    }
    if (!mold.options) {
      return null
    }

    var box = Forge.build({'dom': 'div', 'class': 'blockInput-single'})

    if (mold.title) {
      var title = Forge.build({'dom': 'label', 'for': mold.id, 'id': mold.id + '-error', 'class': 'block-input-title', 'text': mold.title})
      box.appendChild(title)
    }

    var select = Forge.build({'dom': 'select', 'id': mold.id, 'class': 'blockInput'})
    box.appendChild(select)

    var defaultValue = Forge.build({'dom': 'option', 'value': mold.defaultText, 'text': mold.defaultText})
    select.appendChild(defaultValue)

    for (var i = 0; i < mold.options.length; i++) {
      var thisOption = Forge.build({'dom': 'option', 'text': mold.options[i]})
      select.appendChild(thisOption)
    }

    return box
  },

  /**
   * Return Dom Object of Page Navigation
   *
   * @param {Object} Option for Navigagtion DOM
   *
   * @property    {string}    (required)    mold.id
   * @property    {string}    (required)    mold.icon
   * @property    {string}    (required)    mold.text
   *
   * @return {Object} DIV Dom Object consists Page Navigation DOM Object.
   *
   * Forge.buildPageNav({'id':'required', 'icon':'required', 'text':'required'});
   */
  buildPageNav: function (mold) {
    if (!mold.id) {
      return null
    }
    if (!mold.icon) {
      return null
    }
    if (!mold.text) {
      return null
    }

    var container = Forge.build({'dom': 'div', 'id': mold.id, 'class': 'dashboard-pageNav'})

    var icon = Forge.build({'dom': 'span', 'class': 'engagement-icon ' + mold.icon})
    var text = document.createTextNode(' ' + mold.text)
    container.appendChild(icon)
    container.appendChild(text)

    return container
  },

  /**
   * Return Dom Object of Widget Title
   *
   * @property   {string}    (required) mold.title
   *
   * @return {Object} Div Dom object that consists of widget Title
   *
   * Forge.buildWidgetTitle({'title':'required'});
   */
  buildWidgetTitle: function (mold) {
    if (!mold.title) {
      return null
    }

    var box = Forge.build({'dom': 'div', 'style': 'padding:10px;'})

    var title = Forge.build({'dom': 'h4', 'text': mold.title, 'class': 'widget-title'})
    var titleHR = Forge.build({'dom': 'hr', 'class': 'widget-hr'})
    box.appendChild(title)
    box.appendChild(titleHR)

    return box
  },

  /**
   * Return the input for Onboarding Page
   * THIS WILL NEED TO BE REMOVED WHEN ONBOARDING IS FINISHED AS PART OF R1!!! -Tjep
   *
   * @param {Object} mold: option for Ichiro Input(Ex. id, text)
   *
   * @property   {string}   (required)   mold.id
   * @property   {string}   (required)   mold.text
   * @property   {string}   (optional)   mold.maxlength
   *
   * @return {Object} Div Dom Object that consists of Ichiro input for Onboarding page.
   *
   * Forge.buildIchiroInput({'id':'required', 'text':'required', 'maxlength':''});
   */
  buildIchiroInput: function (mold) {
    if (!mold.id) {
      return null
    }
    if (!mold.text) {
      return null
    }
    var inputContainer = Forge.build({'dom': 'div', 'class': 'input--ichiro'})
    if (!mold.maxlength) {
      mold.maxlength = '1000'
    }
    var input = Forge.build({'dom': 'input', 'class': 'input__field--ichiro', 'type': 'text', 'id': mold.id, 'maxlength': mold.maxlength})
    var inputLabel = Forge.build({'dom': 'label', 'class': 'input__label--ichiro', 'for': mold.id})
    var inputSpan = Forge.build({'dom': 'span', 'class': 'input__label-content--ichiro', 'text': mold.text})

    inputContainer.appendChild(input)
    inputContainer.appendChild(inputLabel)
    inputLabel.appendChild(inputSpan)

    return inputContainer
  },

  /**
   * This builds out the drop down menu system.
   *
   * @property   {dom}        (required)   mold.parent        Where you are appending the menu to.
   * @property   {array}      (required)   mold.list          The item tree of what you want to build.
   * @property   {function}   (required)   mold.itemEvent     function that you want attched to the list.
   * @property   {string}     (required)   mold.menuTitle     Menue's initial title
   * @property   {string}     (required)   mold.menuTitleID   ID for menu
   * @property   {function}   (optional)   mold.listEvent     function that you want attached to the items.
   * @property   {string}     (optional)   mold.direction     Choose weather you want the menu to expand to the right or left
   * @property   {string}     (optional)   mold.container     Ether box or link
   *
   * @return {string} Return Unique id that structures prepand-ID-append.
   *
   * Functions passed in using mold.itemEvent or mold.listEvent should be built like;
   *  var itemFunction = function (evnt) {
   *    CODE GOES HERE
   *  };
   *
   * Forge.buildDropdownMenu({'parent':'required', 'list':'required array', 'itemEvent':'required function', 'menuTitle':'required string', 'menuTitleID':'required', 'listEvent':'', 'direction':'left or right', 'container':'box or link'});
   */
  buildDropdownMenu: function (mold) {
    if (!mold.parent) {
      return null
    }
    if (!mold.list) {
      return null
    }
    if (!mold.itemEvent) {
      return null
    }
    if (!mold.menuTitle) {
      return null
    }
    if (!mold.menuTitleID) {
      return null
    }

    var menuDirection = ''
    if (mold.direction === 'left') {
      menuDirection = 'ddlLeftMenu'
    }

    var containerStartClass = ''
    if (mold.container === 'box') {
      containerStartClass = 'ddlStart'
    } else if (mold.container === 'link') {
      containerStartClass = 'ddlStartLink'
    } else {
      containerStartClass = 'ddlStart'
    }

    var categoryul = Forge.build({'dom': 'ul', 'id': mold.menuTitleID + '-ul', 'class': 'ddlLink ' + containerStartClass + ' ' + menuDirection, 'style': 'width:200px; border-radius: 5px; border: 1px solid #BFBFBF;'})
    var categoryli = Forge.build({'dom': 'li', 'class': 'ddlItem'})
    var fileCategory = Forge.build({'dom': 'a', 'class': 'ddlText ddlLinkArrow noSelect', 'id': mold.menuTitleID, 'text': mold.menuTitle})
    var attachMenu = Forge.build({'dom': 'ul', 'class': 'ddlLink ' + menuDirection})

    mold.parent.appendChild(categoryul)
    categoryul.appendChild(categoryli)
    categoryli.appendChild(fileCategory)
    categoryli.appendChild(attachMenu)

    Forge.buildDropdownList({'list': mold.list, 'parent': attachMenu, 'listEvent': mold.listEvent, 'itemEvent': mold.itemEvent, 'direction': mold.direction})
  },

  /**
   * This builds out the drop down list system.
   * @param   {array}      (required)   mold.list        The item tree of what you want to build.
   * @param   {dom}        (required)   mold.parent      Where you are appending the menu to.
   * @param   {function}   (required)   mold.itemEvent   function that you want attched to the list.
   * @param   {function}   (optional)   mold.listEvent   function that you want attached to the items.
   * @param   {string}     (optional)   mold.direction   Choose weather you want the menu to expand to the right or left
   *
   * @return {string} Return Unique id that structures prepand-ID-append.
   *
   * Forge.buildDropdownList({'list':'required', 'parent':'required', 'itemEvent':'required', 'listEvent':'', 'direction':''});
   */
  buildDropdownList: function (mold) {
    if (!mold.list) {
      return null
    }
    if (!mold.parent) {
      return null
    }
    if (!mold.itemEvent) {
      return null
    }

    var menuDirection = ''
    if (mold.direction === 'left') {
      menuDirection = 'ddlLeftMenu'
    } else {
      menuDirection = 'ddlRightMenu'
    }
    var item
    var text
    for (var i = 0; i < mold.list.length; i++) {
      if (mold.list[i].contains) {
        item = Forge.build({'dom': 'li', 'value': mold.list[i].value, 'class': 'ddlItem'})
        text = Forge.build({'dom': 'a', 'text': mold.list[i].text, 'class': 'ddlText ddlLinkArrow'})
        item.appendChild(text)
        mold.parent.appendChild(item)
        var link = Forge.build({'dom': 'ul', 'value': mold.list[i].value, 'class': 'ddlLink ' + menuDirection})
        item.appendChild(link)
        if (mold.listEvent) {
          item.addEventListener('click', mold.listEvent)
        }
        Forge.buildDropdownList({'list': mold.list[i].contains, 'parent': link, 'listEvent': mold.listEvent, 'itemEvent': mold.itemEvent, 'direction': mold.direction})
      } else {
        item = Forge.build({'dom': 'li', 'value': mold.list[i].value, 'class': 'ddlItem'})
        text = Forge.build({'dom': 'a', 'text': mold.list[i].text, 'class': 'ddlText'})
        item.appendChild(text)
        mold.parent.appendChild(item)
        if (mold.itemEvent) {
          item.addEventListener('click', mold.itemEvent)
        }
      }
    }
  },

  /**
   * Return Unique id for Dom Object.
   *
   * @param {string} prepend: First string for unique id
   * @param {string} append:  Last String for Uqniue id.
   *
   * @return {string} Return Unique id that structures prepand-ID-append.
   *
   * Forge.buildBtn({'text':'required', 'id':'', 'type':'', 'width':'', 'margin':'', 'float':''});
   */
  getUniqueID: function (prepend, append) {
    var uniqueID = ''

    do {
      uniqueID = (prepend) ? (prepend + '-') : ''
      for (var i = 8; i > 0; --i) {
        var id = Forge.ALPHANUMERIC[Math.round(Math.random() * (Forge.ALPHANUMERIC.length - 1))]
        uniqueID = uniqueID + id
      }
      uniqueID = (append) ? (uniqueID + '-' + append) : uniqueID
    } while (document.getElementById(uniqueID))

    return uniqueID
  },

  /**
   * This function builds calendar under an object with the passed id.
   * All code ported in from http://codepen.io/ndne/pen/jqDIG
   * @memberOf Forge
   *
   * @param   {String}   (required)   id               This is the ID of where the calandar dom will be attached
   * @param   {String}   (optional)   calPositioning   This is the class that should consists of just
   *                                                   top/right/bottom/left px for positioning or left empty
   *
   * @returns {Object} calendarDOM
   */
  buildCalendar: function (id, calPositioning) {
    var elementTag = id + '-tag'
    var targetInput = '#' + id
    var init = false

    var monthNames = {
      0: 'January',
      1: 'February',
      2: 'March',
      3: 'April',
      4: 'May',
      5: 'June',
      6: 'July',
      7: 'August',
      8: 'September',
      9: 'October',
      10: 'November',
      11: 'December'
    }

    var calDate = { today: new Date(), browse: new Date() }
    calDate.today = new Date(calDate.today.getUTCFullYear(), calDate.today.getUTCMonth(), calDate.today.getUTCDate())

    var markup = {
      row: 'cal-row',
      cell: 'cal-cell',
      inactive: 'cal-inactive',
      currentMonth: 'cal-currentMonth',
      slctd: 'cal-slctd',
      today: 'cal-today',
      dayArea: 'cal-dayArea'
    }

    /**
     * Returns weekday 7 if weekday is 0
     * @param {Number} wd - The weekday
     * @returns {Number}
     */
    var wd = function (wd) {
      if (wd === 0) return 7
      return wd
    }

    /**
     * Responsible for building the DOM elements that are associated with SelectDate plugin,
     */
    var buildDOM = function () {
      var calBox = Forge.build({'dom': 'div', 'id': calPositioning, 'class': 'clear calendar ' + elementTag + ' ' + calPositioning})
      var calView = Forge.build({'dom': 'div', 'class': 'cal-view'})
      var calHead = Forge.build({'dom': 'div', 'class': 'cal-head'})
      var calTitle = Forge.build({'dom': 'div', 'class': 'cal-title'})
      var calMonth = Forge.build({'dom': 'span', 'class': 'cal-month'})
      var calYear = Forge.build({'dom': 'span', 'class': 'cal-year'})

      var calDays = Forge.build({'dom': 'div', 'class': 'cal-row cal-tableHead'})
      var calMon = Forge.build({'dom': 'div', 'class': 'cal-column', 'text': 'M'})
      var calTue = Forge.build({'dom': 'div', 'class': 'cal-column', 'text': 'T'})
      var calWed = Forge.build({'dom': 'div', 'class': 'cal-column', 'text': 'W'})
      var calThu = Forge.build({'dom': 'div', 'class': 'cal-column', 'text': 'T'})
      var calFri = Forge.build({'dom': 'div', 'class': 'cal-column', 'text': 'F'})
      var calSat = Forge.build({'dom': 'div', 'class': 'cal-column', 'text': 'S'})
      var calSun = Forge.build({'dom': 'div', 'class': 'cal-column', 'text': 'S'})

      var calDaysofMonth = Forge.build({'dom': 'div', 'class': 'cal-dayArea'})

      var navRow = Forge.build({'dom': 'div', 'class': 'cal-row cal-nav'})
      var prevYear = Forge.build({'dom': 'i', 'class': 'cal-btn cal-prev cal-prev-year fa fa-fast-backward'})
      var prevMonth = Forge.build({'dom': 'i', 'class': 'cal-btn cal-prev cal-prev-month fa fa-play fa-flip-horizontal'})
      var nextMonth = Forge.build({'dom': 'i', 'class': 'cal-btn cal-next cal-next-month fa fa-play'})
      var nextYear = Forge.build({'dom': 'i', 'class': 'cal-btn cal-next cal-next-year fa fa-fast-forward'})

      nextMonth.addEventListener('click', function () {
        setMonthNext()
      })
      prevMonth.addEventListener('click', function () {
        setMonthPrev()
      })
      nextYear.addEventListener('click', function () {
        setYearNext()
      })
      prevYear.addEventListener('click', function () {
        setYearPrev()
      })

      calTitle.addEventListener('click', function () {
        calDate.browse = new Date(calDate.today.getTime())
        constructDayArea(false)
      })

      calBox.appendChild(calView)
      calView.appendChild(calHead)
      calHead.appendChild(calTitle)
      calTitle.appendChild(calMonth)
      calTitle.appendChild(calYear)

      calView.appendChild(calDays)
      calDays.appendChild(calMon)
      calDays.appendChild(calTue)
      calDays.appendChild(calWed)
      calDays.appendChild(calThu)
      calDays.appendChild(calFri)
      calDays.appendChild(calSat)
      calDays.appendChild(calSun)

      calView.appendChild(calDaysofMonth)

      calView.appendChild(navRow)
      navRow.appendChild(prevYear)
      navRow.appendChild(prevMonth)
      navRow.appendChild(nextMonth)
      navRow.appendChild(nextYear)

      $(calBox).insertAfter(targetInput)
      $(targetInput).css('cursor', 'pointer')
      hide(0)
    }

    /**
     * Responsible for building the day picker that is associated with SelectDate plugin,
     * @param {Boolean} flipDirection - Tells us to flip direction?
     */
    var constructDayArea = function (flipDirection) {
      var m = calDate.browse.getUTCMonth()
      var y = calDate.browse.getUTCFullYear()

      var monthBgnDate = new Date(y, m, 0)
      var monthBgn = monthBgnDate.getTime()
      var monthEndDate = new Date(getMonthNext().getTime() - 1000 * 60 * 60 * 24)
      var monthEnd = monthEndDate.getTime()

      var monthBgnWd = wd(monthBgnDate.getUTCDay())
      var itrBgn = monthBgnDate.getTime() - (monthBgnWd - 1) * 1000 * 60 * 60 * 24

      var i = 1
      var n = 0
      var dayItr = itrBgn
      var newViewContent = ''
      newViewContent += "<div class='" + markup.row + "'>\n"
      while (n < 42) {
        var cls = ['cal-column', markup.cell]
        if (dayItr <= monthBgn) {
          cls.push(markup.inactive, 'jump-to-previous-month')
        } else if (dayItr >= monthEnd + 1) {
          cls.push(markup.inactive, 'jump-to-next-month')
        } else {
          cls.push(markup.currentMonth)
        }

        if (dayItr === calDate.slctd.getTime()) cls.push(markup.slctd)

        if (dayItr === calDate.today.getTime()) cls.push(markup.today)

        var date = new Date(dayItr)
        newViewContent += "<div class='" + cls.join(' ') + "'>" + date.getUTCDate() + '</div>\n'
        i += 1
        if (i > 7) {
          i = 1
          newViewContent += "</div>\n<div class='" + markup.row + "'>\n"
        }
        n += 1
        dayItr = dayItr + 1000 * 60 * 60 * 24
      }
      newViewContent += '</div>\n'

      changePage(newViewContent, flipDirection)
      $('.' + elementTag + ' .cal-title .cal-month').html(monthNames[m])
      $('.' + elementTag + ' .cal-title .cal-year').html(y)

      return newViewContent
    }

    /**
     * Responsible for building the day picker
     * @param {Object}  newPageContent - the html content from constructDayArea
     * @param {Boolean} flipDirection  - True for right flip, false for left flip
     */
    var changePage = function (newPageContent, flipDirection) {
      var multiplier = -1
      var mark = '-'
      if (flipDirection) {
        multiplier = 1
        mark = '+'
      }

      var oldPage = $('.' + elementTag + ' .' + markup.dayArea + ' .cal-monthArea')
      var newPage = $("<div class='cal-monthArea'></div>").css('left', (-1 * multiplier * 224) + 'px').html(newPageContent)
      $('.' + elementTag + ' .' + markup.dayArea).append(newPage)

      $('.cal-monthArea').stop(1, 1).animate({
        left: mark + '=224px'
      }, 300, function () {
        oldPage.remove()
      })
    }

    /**
     * Returns date object correlating to today's date
     * @param {Number} y - Year in YYYY
     * @param {Number} m - month in MM or M
     * @param {Number} d - day in DD or D
     * @returns {Object} Date object
     */
    var selectedDate = function (y, m, d) {
      calDate.slctd = new Date(y, m, d)
      updateInput(y, m, d)
      constructDayArea(false)
      return calDate.slctd
    }

    /**
     * Updates input based on given date
     * @param {Number} y - Year in YYYY
     * @param {Number} m - month in MM or M
     * @param {Number} d - day in DD or D
     */
    var updateInput = function (y, m, d) {
      if (m === '') m = ''
      else m = monthNames[m]
      $(targetInput).val(y + ' ' + m + ' ' + d)
    }

    var getMonthNext = function () {
      var m = calDate.browse.getUTCMonth()
      var y = calDate.browse.getUTCFullYear()
      if (m + 1 > 11) return new Date(y + 1, 0)
      else return new Date(y, m + 1)
    }

    var setMonthNext = function () {
      var m = calDate.browse.getUTCMonth()
      var y = calDate.browse.getUTCFullYear()
      if (m + 1 > 11) {
        calDate.browse.setUTCFullYear(y + 1)
        calDate.browse.setUTCMonth(0)
      } else {
        calDate.browse.setUTCMonth(m + 1)
      }
      constructDayArea(false)
    }

    var setMonthPrev = function () {
      var m = calDate.browse.getUTCMonth()
      var y = calDate.browse.getUTCFullYear()
      if (m - 1 < 0) {
        calDate.browse.setUTCFullYear(y - 1)
        calDate.browse.setUTCMonth(11)
      } else {
        calDate.browse.setUTCMonth(m - 1)
      }
      constructDayArea(true)
    }

    var setYearNext = function () {
      var y = calDate.browse.getUTCFullYear()
      calDate.browse.setUTCFullYear(y + 1)
      constructDayArea(false)
    }

    var setYearPrev = function () {
      var y = calDate.browse.getUTCFullYear()
      calDate.browse.setUTCFullYear(y - 1)
      constructDayArea(true)
    }

    /**
     * Hide the calendar
     * @param {Number} duration - The length of the animation in milliseconds.
     */
    var hide = function (duration) {
      $('.' + elementTag + ' .cal-view').slideUp(duration)
    }

    /**
     * Show the calendar
     * @param {Number} duration - The length of the animation in milliseconds.
     */
    var show = function (duration) {
      init = true
      $('.' + elementTag + ' .cal-view').slideDown(duration, function () {
        init = false
      })
    }

    buildDOM()
    selectedDate(calDate.today.getFullYear(), calDate.today.getMonth(), calDate.today.getDate())
    constructDayArea()
    updateInput('Select Date', '', '')

    $(targetInput).click(function (ms) {
      var e = $('.' + elementTag + ' .cal-view')
      var eco = e.offset()
      if (ms.pageX < eco.left || ms.pageX > eco.left + e.width() || ms.pageY < eco.top || ms.pageY > eco.top + e.height()) {
        if (!init) hide(300)
      }
    })

    $('.' + elementTag).on('click', '.jump-to-next-month', function () {
      setMonthNext()
    })

    $('.' + elementTag).on('click', '.jump-to-previous-month', function () {
      setMonthPrev()
    })

    $('.' + elementTag).on('click', '.' + markup.currentMonth, function () {
      hide(300)
    })

    $(targetInput).focus(function () {
      show(100)
      $(this).blur()
    })

    return this
  },

  /**
   * This function builds a static calendar. You pass in the ID of the element you attach it to, the positioning and whether or not it will be displayed,
   * or opened on a click of its parent.
   * All code ported in from http://codepen.io/ndne/pen/jqDIG
   * @memberOf Forge
   *
   * @param   {String}   (required)   id               This is the ID of where the calandar dom will be attached
   * @param   {String}   (optional)   calPositioning   This is the class that should consists of just
   *                                                   top/right/bottom/left px for positioning or left empty
   * @param   {String}   (optional)   display          This boolean indicates whether the Calendar should be displayed
   *                                                   or only opened on a click. Defaults to true which means it will be displayed.
   *
   * @returns {Object} calendarDOM
   */
  buildNewCalendar: function (id, calPositioning, display) {
    if (!id) {
      return null
    }

    var elementTag = id + '-tag'
    var targetInput = '#' + id
    var init = false

    var monthNames = {
      0: 'January',
      1: 'February',
      2: 'March',
      3: 'April',
      4: 'May',
      5: 'June',
      6: 'July',
      7: 'August',
      8: 'September',
      9: 'October',
      10: 'November',
      11: 'December'
    }

    var calDate = { today: new Date(), browse: new Date() }
    calDate.today = new Date(calDate.today.getUTCFullYear(), calDate.today.getUTCMonth(), calDate.today.getUTCDate())

    var markup = {
      row: 'newCal-row',
      cell: 'newCal-cell',
      firstCell: 'newCal-first-cell',
      inactive: 'newCal-inactive',
      currentMonth: 'newCal-currentMonth',
      slctd: 'newCal-slctd',
      today: 'newCal-today',
      dayArea: 'newCal-dayArea'
    }

    /**
     * Returns weekday 7 if weekday is 0
     * @param {Number} wd - The weekday
     * @returns {Number}
     */
    var wd = function (wd) {
      if (wd === 0) return 7
      return wd
    }

    /**
     * Responsible for building the DOM elements that are associated with SelectDate plugin,
     */
    var buildDOM = function () {
      var calBox = Forge.build({'dom': 'div', 'id': calPositioning, 'class': 'clear newCalendar ' + elementTag + ' ' + calPositioning})
      var calView = Forge.build({'dom': 'div', 'class': 'newCal-view'})
      var calHead = Forge.build({'dom': 'div', 'class': 'newCal-head'})
      var calTitle = Forge.build({'dom': 'div', 'class': 'newCal-title'})
      var calMonth = Forge.build({'dom': 'span', 'class': 'newCal-month'})
      var calYear = Forge.build({'dom': 'span', 'class': 'newCal-year'})

      var calDays = Forge.build({'dom': 'div', 'class': 'newCal-row newCal-tableHead'})
      var calMon = Forge.build({'dom': 'div', 'class': 'newCal-column', 'text': 'Mon'})
      var calTue = Forge.build({'dom': 'div', 'class': 'newCal-column', 'text': 'Tue'})
      var calWed = Forge.build({'dom': 'div', 'class': 'newCal-column', 'text': 'Wed'})
      var calThu = Forge.build({'dom': 'div', 'class': 'newCal-column', 'text': 'Thu'})
      var calFri = Forge.build({'dom': 'div', 'class': 'newCal-column', 'text': 'Fri'})
      var calSat = Forge.build({'dom': 'div', 'class': 'newCal-column', 'text': 'Sat'})
      var calSun = Forge.build({'dom': 'div', 'class': 'newCal-column', 'text': 'Sun'})

      var calDaysofMonth = Forge.build({'dom': 'div', 'class': 'newCal-dayArea'})

      var navRow = Forge.build({'dom': 'div', 'class': 'newCal-row newCal-nav'})
      var prevYear = Forge.build({'dom': 'i', 'class': 'newCal-btn newCal-prev newCal-prev-year fa fa-fast-backward'})
      var prevMonth = Forge.build({'dom': 'i', 'class': 'newCal-btn newCal-prev newCal-prev-month auvicon-arrow-back'})
      var nextMonth = Forge.build({'dom': 'i', 'class': 'newCal-btn newCal-next newCal-next-month auvicon-forward'})
      var nextYear = Forge.build({'dom': 'i', 'class': 'newCal-btn newCal-next newCal-next-year fa fa-fast-forward'})

      nextMonth.addEventListener('click', function () {
        setMonthNext()
      })
      prevMonth.addEventListener('click', function () {
        setMonthPrev()
      })
      nextYear.addEventListener('click', function () {
        setYearNext()
      })
      prevYear.addEventListener('click', function () {
        setYearPrev()
      })

      calTitle.addEventListener('click', function () {
        calDate.browse = new Date(calDate.today.getTime())
        constructDayArea(false)
      })

      calBox.appendChild(calView)
      calView.appendChild(calHead)
      calHead.appendChild(calTitle)
      calTitle.appendChild(calMonth)
      calTitle.appendChild(calYear)

      calView.appendChild(calDays)
      calDays.appendChild(calMon)
      calDays.appendChild(calTue)
      calDays.appendChild(calWed)
      calDays.appendChild(calThu)
      calDays.appendChild(calFri)
      calDays.appendChild(calSat)
      calDays.appendChild(calSun)

      calView.appendChild(calDaysofMonth)

      calView.appendChild(navRow)
      navRow.appendChild(prevYear)
      navRow.appendChild(prevMonth)
      navRow.appendChild(nextMonth)
      navRow.appendChild(nextYear)

      $(calBox).insertAfter(targetInput)
      $(targetInput).css('cursor', 'pointer')
      if (display === false) {
        hide(0)
      }
    }

    /**
     * Responsible for building the day picker that is associated with SelectDate plugin,
     * @param {Boolean} flipDirection - Tells us to flip direction?
     */
    var constructDayArea = function (flipDirection) {
      var m = calDate.browse.getUTCMonth()
      var y = calDate.browse.getUTCFullYear()

      var monthBgnDate = new Date(y, m, 0)
      var monthBgn = monthBgnDate.getTime()
      var monthEndDate = new Date(getMonthNext().getTime() - 1000 * 60 * 60 * 24)
      var monthEnd = monthEndDate.getTime()

      var monthBgnWd = wd(monthBgnDate.getUTCDay())
      var itrBgn = monthBgnDate.getTime() - (monthBgnWd - 1) * 1000 * 60 * 60 * 24

      var i = 1
      var n = 0
      var dayItr = itrBgn
      var newViewContent = ''
      var cls
      newViewContent += "<div class='" + markup.row + "'>\n"
      while (n < 42) {
        if (n % 7 === 0) {
          // left side has no border
          cls = ['newCal-column', markup.cell, markup.firstCell]
        } else {
          cls = ['newCal-column', markup.cell]
        }

        if (dayItr <= monthBgn) {
          cls.push(markup.inactive, 'jump-to-previous-month')
        } else if (dayItr >= monthEnd + 1) {
          cls.push(markup.inactive, 'jump-to-next-month')
        } else {
          cls.push(markup.currentMonth)
        }

        if (dayItr === calDate.slctd.getTime()) {
          cls.push(markup.slctd)
        }
        if (dayItr === calDate.today.getTime()) cls.push(markup.today)

        var date = new Date(dayItr)
        newViewContent += "<div class='" + cls.join(' ') + "'>" + date.getUTCDate() + '</div>\n'
        i += 1
        if (i > 7) {
          i = 1
          newViewContent += "</div>\n<div class='" + markup.row + "'>\n"
        }

        n += 1
        dayItr = dayItr + 1000 * 60 * 60 * 24
      }
      newViewContent += '</div>\n'

      changePage(newViewContent, flipDirection)
      $('.' + elementTag + ' .newCal-title .newCal-month').html(monthNames[m])
      $('.' + elementTag + ' .newCal-title .newCal-year').html(y)

      return newViewContent
    }

    /**
     * Responsible for building the day picker
     * @param {Object}  newPageContent - the html content from constructDayArea
     * @param {Boolean} flipDirection  - True for right flip, false for left flip
     */
    var changePage = function (newPageContent, flipDirection) {
      var multiplier = -1
      var mark = '-'
      if (flipDirection) {
        multiplier = 1
        mark = '+'
      }

      var oldPage = $('.' + elementTag + ' .' + markup.dayArea + ' .newCal-monthArea')
      var newPage = $("<div class='newCal-monthArea'></div>").css('left', (-1 * multiplier * 224) + 'px').html(newPageContent)
      $('.' + elementTag + ' .' + markup.dayArea).append(newPage)

      $('.newCal-monthArea').stop(1, 1).animate({
        left: mark + '=224px'
      }, 300, function () {
        oldPage.remove()
      })
    }

    /**
     * Returns date object correlating to today's date
     * @param {Number} y - Year in YYYY
     * @param {Number} m - month in MM or M
     * @param {Number} d - day in DD or D
     * @returns {Object} Date object
     */
    var selectedDate = function (y, m, d) {
      calDate.slctd = new Date(y, m, d)
      updateInput(y, m, d)
      constructDayArea(false)
      return calDate.slctd
    }

    /**
     * Updates input based on given date
     * @param {Number} y - Year in YYYY
     * @param {Number} m - month in MM or M
     * @param {Number} d - day in DD or D
     */
    var updateInput = function (y, m, d) {
      if (m === '') m = ''
      else m = monthNames[m]
      $(targetInput).val(y + ' ' + m + ' ' + d)
    }

    var getMonthNext = function () {
      var m = calDate.browse.getUTCMonth()
      var y = calDate.browse.getUTCFullYear()
      if (m + 1 > 11) return new Date(y + 1, 0)
      else return new Date(y, m + 1)
    }

    var setMonthNext = function () {
      var m = calDate.browse.getUTCMonth()
      var y = calDate.browse.getUTCFullYear()
      if (m + 1 > 11) {
        calDate.browse.setUTCFullYear(y + 1)
        calDate.browse.setUTCMonth(0)
      } else {
        calDate.browse.setUTCMonth(m + 1)
      }
      constructDayArea(false)
    }

    var setMonthPrev = function () {
      var m = calDate.browse.getUTCMonth()
      var y = calDate.browse.getUTCFullYear()
      if (m - 1 < 0) {
        calDate.browse.setUTCFullYear(y - 1)
        calDate.browse.setUTCMonth(11)
      } else {
        calDate.browse.setUTCMonth(m - 1)
      }
      constructDayArea(true)
    }

    var setYearNext = function () {
      var y = calDate.browse.getUTCFullYear()
      calDate.browse.setUTCFullYear(y + 1)
      constructDayArea(false)
    }

    var setYearPrev = function () {
      var y = calDate.browse.getUTCFullYear()
      calDate.browse.setUTCFullYear(y - 1)
      constructDayArea(true)
    }

    /**
     * Hide the calendar
     * @param {Number} duration - The length of the animation in milliseconds.
     */
    var hide = function (duration) {
      $('.' + elementTag + ' .newCal-view').slideUp(duration)
    }

    /**
     * Show the calendar
     * @param {Number} duration - The length of the animation in milliseconds.
     */
    var show = function (duration) {
      init = true
      $('.' + elementTag + ' .newCal-view').slideDown(duration, function () {
        init = false
      })
    }

    buildDOM()
    selectedDate(calDate.today.getFullYear(), calDate.today.getMonth(), calDate.today.getDate())
    constructDayArea()
    updateInput('Select Date', '', '')

    if (display === false) {
      $(targetInput).click(function (ms) {
        var e = $('.' + elementTag + ' .newCal-view')
        var eco = e.offset()
        if (ms.pageX < eco.left || ms.pageX > eco.left + e.width() || ms.pageY < eco.top || ms.pageY > eco.top + e.height()) {
          if (!init) hide(300)
        }
      })

      $(targetInput).click(function (ms) {
        var e = $('.' + elementTag + ' .newCal-view')
        var eco = e.offset()
        if (ms.pageX < eco.left || ms.pageX > eco.left + e.width() || ms.pageY < eco.top || ms.pageY > eco.top + e.height()) {
          if (!init) hide(300)
        }
      })

      $('.' + elementTag).on('click', '.jump-to-next-month', function () {
        setMonthNext()
      })

      $('.' + elementTag).on('click', '.jump-to-previous-month', function () {
        setMonthPrev()
      })

      $('.' + elementTag).on('click', '.' + markup.currentMonth, function () {
        hide(300)
      })

      $(targetInput).focus(function () {
        show(100)
        $(this).blur()
      })
    } else {
      $('.' + elementTag).on('click', '.jump-to-next-month', function () {
        setMonthNext()
      })

      $('.' + elementTag).on('click', '.jump-to-previous-month', function () {
        setMonthPrev()
      })

      $('.' + elementTag).on('click', '.' + markup.currentMonth, function () {
      })
    }

    return this
  },

  /**
   * This function builds tooltip based on what's passed on mold.
   * Based on the classname, it will build either whiteToolTip, basicToolTip, smallToolTip or largeToolTip.
   * @name buildTooltip
   * @memberOf
   * @param {any} mold
   * @returns {DOM} myTooltip
   */
  buildTooltip: function (mold) {
    var defaultStyle = {
      width: '',
      height: 'auto',
      background: '',
      marginTop: '30px',
      marginLeft: '',
      zIndex: '',
      left: '',
      top: '',
      arrowLeft: '',
      text: 'We think you\'ll find this useful',
      fontSize: '',
      float: '',
      right: ''
    }

    // for tooltip box
    defaultStyle.width = (mold.width) ? mold.width : defaultStyle.width
    defaultStyle.height = (mold.height) ? mold.height : defaultStyle.height
    defaultStyle.background = (mold.background) ? mold.background : defaultStyle.background
    defaultStyle.marginTop = (mold.marginTop) ? mold.marginTop : defaultStyle.marginTop
    defaultStyle.marginLeft = (mold.marginLeft) ? mold.marginLeft : defaultStyle.marginLeft
    defaultStyle.zIndex = (mold.zIndex) ? mold.zIndex : defaultStyle.zIndex
    defaultStyle.left = (mold.left) ? mold.left : defaultStyle.left
    defaultStyle.top = (mold.top) ? mold.top : defaultStyle.top
    defaultStyle.float = (mold.float) ? mold.float : defaultStyle.float
    defaultStyle.right = (mold.right) ? mold.right : defaultStyle.right

    // for tooltip arrow
    defaultStyle.arrowLeft = (mold.arrowLeft) ? mold.arrowLeft : defaultStyle.ArrowLeft

    // inside tooltip textbox
    defaultStyle.text = (mold.text) ? mold.text : defaultStyle.text
    defaultStyle.fontSize = (mold.fontSize) ? mold.fontSize : defaultStyle.fontSize

    var tooltipStyle = ''
    var arrowStyle = ''
    var textStyle = ''

    if (defaultStyle.width !== '') {
      tooltipStyle += ' width:' + defaultStyle.width + ';'
    }
    if (defaultStyle.height !== '') {
      tooltipStyle += ' height:' + defaultStyle.height + ';'
    }
    if (defaultStyle.background !== '') {
      tooltipStyle += ' background:' + defaultStyle.background + ';'
      arrowStyle += ' color:' + defaultStyle.background + ';'
    }
    if (defaultStyle.marginTop !== '') {
      tooltipStyle += ' margin-top:' + defaultStyle.marginTop + ';'
    }
    if (defaultStyle.marginLeft !== '') {
      tooltipStyle += ' margin-left:' + defaultStyle.marginLeft + ';'
    }
    if (defaultStyle.zIndex !== '') {
      tooltipStyle += ' z-index:' + defaultStyle.zIndex + ';'
    }
    if (defaultStyle.left !== '') {
      tooltipStyle += ' left:' + defaultStyle.left + ';'
    }
    if (defaultStyle.top !== '') {
      tooltipStyle += ' top:' + defaultStyle.top + ';'
    }
    if (defaultStyle.float !== '') {
      tooltipStyle += ' float:' + defaultStyle.float + ';'
    }
    if (defaultStyle.right !== '') {
      tooltipStyle += ' right:' + defaultStyle.right + ';'
    }
    if (defaultStyle.arrowLeft !== '') {
      arrowStyle += ' left:' + defaultStyle.arrowLeft + ';'
    }
    if (defaultStyle.fontSize !== '') {
      textStyle += ' font-size:' + defaultStyle.fontSize + ';'
    }

    var classTemplate = ['basicToolTip', 'smallToolTip', 'largeToolTip', 'whiteToolTip']
    var myTooltip
    var ttPointer
    var ttPointerBorder
    var ttTxt
    if (classTemplate.indexOf(mold.class) >= 0) {
      if (mold.class === 'whiteToolTip') {
        myTooltip = Forge.build({'dom': 'div', 'id': mold.id, 'class': mold.class + '-box', 'style': tooltipStyle})
        ttPointer = Forge.build({'dom': 'div', 'id': mold.id + '-arrow', 'class': mold.class + '-caret', 'style': arrowStyle})
        ttPointerBorder = Forge.build({'dom': 'div', 'id': mold.id + '-arrowBorder', 'class': 'whiteToolTip-triBorder', 'style': arrowStyle})
        ttTxt = Forge.build({'dom': 'h3', 'id': mold.id + '-mainText', 'class': mold.class + '-mainTxt', 'text': defaultStyle.text, 'style': textStyle})
        var ttDismiss = Forge.build({'dom': 'button', 'class': 'toolTip-x', 'text': 'x'})

        myTooltip.appendChild(ttPointerBorder)
        myTooltip.appendChild(ttPointer)
        myTooltip.appendChild(ttDismiss)
        myTooltip.appendChild(ttTxt)
        ttDismiss.addEventListener('click', function () {
          myTooltip.style.display = 'none'
        })
      } else if (mold.class === 'basicToolTip') {
        myTooltip = Forge.build({'dom': 'div', 'id': mold.id, 'class': mold.class + '-box', 'style': tooltipStyle})
        ttPointer = Forge.build({'dom': 'i', 'id': mold.id + '-arrow', 'class': 'fa fa-caret-up ' + mold.class + '-caret', 'style': arrowStyle})
        ttTxt = Forge.build({'dom': 'h3', 'id': mold.id + '-mainText', 'class': mold.class + '-mainTxt', 'text': defaultStyle.text, 'style': textStyle})
        var ttDismiss = Forge.build({'dom': 'button', 'class': 'toolTip-dismissContainer', 'text': 'Got It', 'style': textStyle})

        myTooltip.appendChild(ttPointer)
        myTooltip.appendChild(ttTxt)
        myTooltip.appendChild(ttDismiss)
        ttDismiss.addEventListener('click', function () {
          myTooltip.style.display = 'none'
        })
      } else if (mold.class === 'smallToolTip') {
        myTooltip = Forge.build({'dom': 'div', 'id': mold.id, 'class': mold.class + '-box', 'style': tooltipStyle})
        ttPointer = Forge.build({'dom': 'i', 'id': mold.id + '-arrow', 'class': 'fa fa-caret-up ' + mold.class + '-caret', 'style': arrowStyle})
        ttTxt = Forge.build({'dom': 'span', 'id': mold.id + '-mainText', 'class': mold.class + '-mainTxt', 'text': defaultStyle.text, 'style': textStyle})

        myTooltip.appendChild(ttPointer)
        myTooltip.appendChild(ttTxt)
      } else if (mold.class === 'largeToolTip') {
        myTooltip = Forge.build({'dom': 'a', 'id': mold.id, 'class': mold.class, 'href': '#', 'style': tooltipStyle})
        var questionTrigger = Forge.build({'dom': 'img', 'src': 'images/icons/question.svg'})
        var largeTooltipContent = Forge.build({'dom': 'span', 'class': mold.class + '-content', 'id': mold.id + '-arrow', 'style': arrowStyle})
        var largeTooltipText = Forge.build({'dom': 'span', 'class': mold.class + '-text'})
        var largeTooltipInner = Forge.build({'dom': 'span', 'class': mold.class + '-inner', 'id': mold.id + '-mainText', 'text': defaultStyle.text, 'style': textStyle})

        largeTooltipText.appendChild(largeTooltipInner)
        largeTooltipContent.appendChild(largeTooltipText)
        myTooltip.appendChild(largeTooltipContent)
        myTooltip.appendChild(questionTrigger)
      }

      if (mold.animation === 'fade') {
        myTooltip.classList.add('ttFadeBefore')
        setTimeout(function () {
          myTooltip.classList.add('ttFadeAfter')
        }, 500)
      }

      if (mold.parent) {
        mold.parent.appendChild(myTooltip)
      } else {
        document.body.appendChild(myTooltip)
      }

      return myTooltip
    }
  }

}

module.exports = Forge
