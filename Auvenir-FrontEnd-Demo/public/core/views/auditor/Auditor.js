'use strict'

import ViewTemplate from '../ViewTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'

import Widget_EngagementInfo from '../../widgets/engagement-info/Widget_EngagementInfo'
import styles from './Auditor.css'

function Auditor (ctrl, settings) {
  ViewTemplate.call(this, ctrl, settings)

  var self = this
  this.name = 'Auditor Module'
  var c = ctrl

  var widgetContainer = {pageID: 'cpa-main', CURRENT_X_OFFSET: 0, CURRENT_Y_OFFSET: 0}
  var dashboardWidgets = this.dashboardWidgets = []
  var DASHBOARD_WIDTH = 1200
  var WIDGET_WIDTH = 264
  var WIDGET_HEIGHT = 300
  var X_OFFSET = 20
  var Y_OFFSET = 20

  var noEngagementContainer = null

  /**
   * Sets up the engagements listeners
   */
  this.setupListeners = function () {
    log('Dashboard: Setting up listeners ...')
  }

  this.buildPage = function () {
    log('Auditor: Building Page')

    var parentContainer = Forge.build({'dom': 'div', 'id': 'auditorPage', style: 'min-height: 600px; margin-top: 24px;'})

    var mainDashboard = Forge.build({'dom': 'div', 'id': 'cpa-main', 'class': 'dashboard', 'style': 'display:inherit'})

    log('Building Auditor Header')
    var parentDiv = Forge.build({'dom': 'div'})

    var pageHeader = Forge.build({'dom': 'div', 'class': 'pageHeader noSelect'})
    var pageHeadContent = Forge.build({'dom': 'div', 'id': 'audit-head-content', 'class': 'pageHeader-content'})
    var pageHeadLeft = Forge.build({'dom': 'div', 'id': 'cpa-header-left', 'class': 'pageHeader-leftContainer'})
    var pageHeadText = Forge.build({
      'dom': 'span',
      'class': 'pageHeader-title',
      'text': 'My Engagements',
      'id': 'c-header-title'
    })

    var newAuditBtn = Forge.buildBtn({text: 'New Engagement', id: 'newAuditBtn', type: 'darkblue', float: 'right', margin: '6px 0px 0px;'}).obj

    parentDiv.appendChild(pageHeader)
    pageHeader.appendChild(pageHeadContent)
    pageHeadContent.appendChild(pageHeadLeft)
    pageHeadLeft.appendChild(pageHeadText)

    pageHeadContent.appendChild(newAuditBtn)

    var tooltipContainer = Forge.build({dom: 'div', style: 'position: relative'})
    var tooltipOne = Forge.buildTooltip({
      class: 'whiteToolTip',
      id: 'tooltip-createEngagement',
      right: '0px',
      float: 'right',
      marginTop: '15px',
      width: '284px',
      text: 'Click here to create your first audit engagement',
      animation: 'fade'
    })
    if (c.myEngagements && c.myEngagements.length < 1) {
      tooltipContainer.appendChild(tooltipOne)
    }
    pageHeadContent.appendChild(tooltipContainer)
    pageHeadContent.className += ' tooltipTrigger'

    noEngagementContainer = $('<div id="auditPageNoEngagement" class="noEngagement-container">' +
      '<img src="images/illustrations/computerDesktop.svg"/>' +
      '<h2 class="noEngagement-txt">You don\'t have any engagements yet. Click the button above to create one.</h2>' +
      '</div>')
    parentDiv.appendChild(noEngagementContainer[0])

    parentContainer.appendChild(parentDiv)
    parentContainer.appendChild(mainDashboard)

    newAuditBtn.addEventListener('click', function () {
      // ctrl.submitNewEngagementRequest(ctrl.currentUser._id)
      c.displayCreateEngagementModal()
    })

    return parentContainer
  }

  this.appendEngagementEle = function (engagement) {
    function addWidget (parent, widget) {
      var parentDom = document.getElementById(parent)
      parentDom.appendChild(widget)
    }

    document.getElementById('audit-head-content').lastChild.style.display = 'none' /* hiding the tooltip */
    document.getElementById('auditPageNoEngagement').style.display = 'none'

    var widget = new Widget_EngagementInfo(ctrl, engagement)
    addWidget(widgetContainer.pageID, widget.buildWidget({
      width: WIDGET_WIDTH + 'px',
      height: WIDGET_HEIGHT + 'px',
      background: '#fff',
      border: '0px solid red',
      draggable: false,
      resizable: false,
      margin: '0 24px 24px 0px'
    }))

    widget.applySettings()
    widget.refresh()
    dashboardWidgets.push(widget)
  }
  this.loadDashboard = function () {
    for (var i = 0; i < c.myEngagements.length; i++) {
      self.appendEngagementEle(c.myEngagements[i])
    }
    if (c.myEngagements.length) {
      noEngagementContainer.hide()
    }
    document.getElementById(self._moduleID).classList.add('scroll-190')
  }

  this.build()

  return this
}

module.exports = Auditor
