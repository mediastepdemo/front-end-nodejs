'use strict'

import ViewTemplate from '../ViewTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import Utility from '../../../js/src/utility_c'
import engagements from '../engagement-preview/myEngagements'

function Client (ctrl, settings) {
  ViewTemplate.call(this, ctrl, settings)
  var self = this
  this.name = 'Client Module'

  var c = ctrl
  // c.myEngagements = engagements
  var engagementData = []
  var engagementTableData = []

  var DASHBOARD_WIDTH = 1200
  var noEngagementContainer = $('<div id="auditPageNoEngagement" class="noEngagement-container">' +
      '<img src="images/illustrations/computerDesktop.svg"/>' +
      '<h2 class="noEngagement-txt">You don\'t have any audits yet. We are still waiting on your auditor.</h2>' +
      '</div>')

  /**
   * Config the title column name of engagement table
   */
  var engagement = {
    title: { text: 'Engagement Name', id: 'engagement-sort'},
    firm: { text: 'Audit Firm', id: 'firm-sort'},
    status: { text: 'Status', id: 'status-sort'},
    auditor: { text: 'Audit Assignee', id: 'audit-sort'},
    client: { text: 'Client Assignee', id: 'client-sort'},
    docs: {text: 'Completed Docs', id: 'docs-sort'},
    activity: { text: 'Last Activity', id: 'activity-sort' },
    duedate: { text: 'Due Date', id: 'duedate-sort'}
  }

  /**
   * Filter data
   */
  var engagementFiltersData = [
    {
      title: 'All',
      subMenu: []
    },
    {
      title: 'Type of Engagement',
      subMenu: ['Financial Audit', 'Review', 'Notice to reader / Compilation', 'Other']
    }
  ]

  /**
   * Initializes the Engagement Dashboard and any relevant functionality
   */
  var initialize = function () {
    registerEvents()
  }
  /**
   * This function loads data from Controller (MAC) and populate the page.
   */
  this.loadDashboard = function () {
    /* if (c.myEngagements.length) {
      noEngagementContainer.hide()
      c.myEngagements.forEach((engagement) => {
        addEngagementRow(engagement)
      })
    } */
    let engagementList = convertEngagements(c.myEngagements)
    engagementData = engagementList
    engagementTableData = engagementList
    loadData(engagementList)
    document.getElementById(self._moduleID).classList.add('scroll-190')
  }

  /**
   * This function builds page based on the data given to the page.
   */
  this.buildPage = function () {
    // PARENT
    var parentContainer = Forge.build({'dom': 'div', id: 'allClientEngagement', style: 'margin-top: 24px;', class: 'parent-of-footer'})

    // HEADER
    var pageHeader = Forge.build({ 'dom': 'div', class: 'pageHeader noSelect' })
    var pageHeadContent = Forge.build({'dom': 'div', id: 'preview-head-content', class: 'pageHeader-content'})
    var pageHeadLeft = Forge.build({ 'dom': 'div', id: 'preview-header-left', class: 'pageHeader-leftContainer' })
    var pageHeadText = Forge.build({'dom': 'span', class: 'pageHeader-title', text: 'All Engagements', 'id': 'c-header-title'})

    // TABLE & FILTER CONTAINER
    var container = Forge.build({dom: 'div', id: 'engagement-container', class: ''})

    // HEAD FILTER AND SEARCH
    var rowFilter = Forge.build({dom: 'div', id: 'engagement-filter', class: 'container-header'})

    // dropdown filter
    var engagementFilters = Forge.build({dom: 'div', id: 'engagement-filters', class: 'ui icon dropdown simple'})
    var engagementFiltersIcon = Forge.build({dom: 'i', class: 'filter icon'})
    var engagementFiltersTitle = Forge.build({dom: 'span', class: 'text', text: 'Filters'})
    var engagementFiltersCaret = Forge.build({dom: 'i', class: 'dropdown icon'})
    var engagementFiltersMenu1 = Forge.build({dom: 'div', class: 'menu lv1'})

    engagementFiltersData.forEach((elem) => {
      var engagementFiltersItem1 = Forge.build({dom: 'div', class: 'item', type: elem.title})
      var engagementFiltersItem1Title = Forge.build({dom: 'span', class: 'text', text: elem.title})
      var engagementFiltersMenu2 = Forge.build({dom: 'div', class: 'menu'})

      engagementFiltersItem1.appendChild(engagementFiltersItem1Title)

      if (elem.subMenu.length > 0) {
        elem.subMenu.forEach((el) => {
          var engagementFiltersItem2 = Forge.build({dom: 'div', class: 'item', text: el, type: elem.title})
          engagementFiltersItem2.addEventListener('click', function () {
            requestAPI(this, 'filters', el)
          })

          engagementFiltersMenu2.appendChild(engagementFiltersItem2)
        })

        engagementFiltersItem1.appendChild(engagementFiltersMenu2)
      } else {
        engagementFiltersItem1.addEventListener('click', function () {
          requestAPI(this, 'filters', elem.title)
        })
      }

      engagementFiltersMenu1.appendChild(engagementFiltersItem1)
    })

    engagementFilters.appendChild(engagementFiltersIcon)
    engagementFilters.appendChild(engagementFiltersTitle)
    engagementFilters.appendChild(engagementFiltersCaret)
    engagementFilters.appendChild(engagementFiltersMenu1)
    rowFilter.appendChild(engagementFilters)
    $(engagementFilters).dropdown({
      transition: 'drop' // you can use any ui transition
    })

    // Search
    var search = Forge.build({dom: 'div', id: 'search-box', class: 'ui search floating right'})
    var inputSearch = Forge.build({dom: 'input', id: 'engagement-search', type: 'text', class: 'prompt', placeholder: 'Search...'})

    let time = null
    inputSearch.onkeydown = function () {
      clearTimeout(time)

      time = setTimeout(function () {
        requestAPI(null, 'search', document.getElementById('engagement-search').value)
      }, 500)
    }

    search.appendChild(inputSearch)
    rowFilter.appendChild(search)

    // DATA TABLE
    var engagementTable = Forge.build({dom: 'table', id: 'engagement-table', class: 'ui selectable very basic table padded'})
    var engagementThead = Forge.build({dom: 'thead'})
    var engagementRow = Forge.build({dom: 'tr'})
    var engagementTbody = Forge.build({dom: 'tbody', id: 'engagement-tbody'})

    var engagementTitle = Forge.build({dom: 'th', text: engagement.title.text, id: engagement.title.id})
    var titleIcon = Forge.build({dom: 'i', class: 'auvicon-sort-down'})
    engagementTitle.appendChild(titleIcon)
    engagementTitle.setAttribute('data-id', 'name')
    engagementTitle.setAttribute('data-sort', '1')

    var auditFirm = Forge.build({dom: 'th', text: engagement.firm.text, id: engagement.firm.id})
    var auditFirmIcon = Forge.build({dom: 'i', class: 'auvicon-sort-down'})
    auditFirm.appendChild(auditFirmIcon)
    auditFirm.setAttribute('data-id', 'firm')
    auditFirm.setAttribute('data-sort', '1')

    var status = Forge.build({dom: 'th', text: engagement.status.text, id: engagement.status.id})
    var statusIcon = Forge.build({dom: 'i', class: 'auvicon-sort-down'})
    status.appendChild(statusIcon)
    status.setAttribute('data-id', 'status')
    status.setAttribute('data-sort', '1')

    var auditorAssigned = Forge.build({dom: 'th', text: engagement.auditor.text, id: engagement.auditor.id})
    var auditorIcon = Forge.build({dom: 'i', class: 'auvicon-sort-down'})
    auditorAssigned.appendChild(auditorIcon)
    auditorAssigned.setAttribute('data-id', 'leadAuditor')
    auditorAssigned.setAttribute('data-sort', '1')

    var clientAssigned = Forge.build({dom: 'th', text: engagement.client.text, id: engagement.client.id})
    var clientAssignedIcon = Forge.build({dom: 'i', class: 'auvicon-sort-down'})
    clientAssigned.appendChild(clientAssignedIcon)
    clientAssigned.setAttribute('data-id', 'leadClient')
    clientAssigned.setAttribute('data-sort', '1')

    var completedDocs = Forge.build({dom: 'th', text: engagement.docs.text, id: engagement.docs.id})
    var completedDocsIcon = Forge.build({dom: 'i', class: 'auvicon-sort-down'})
    completedDocs.appendChild(completedDocsIcon)
    completedDocs.setAttribute('data-id', 'completedDocsNumber')
    completedDocs.setAttribute('data-sort', '1')

    var lastActivity = Forge.build({dom: 'th', text: engagement.activity.text, id: engagement.activity.id})
    var lastActivityIcon = Forge.build({dom: 'i', class: 'auvicon-sort-down'})
    lastActivity.appendChild(lastActivityIcon)
    lastActivity.setAttribute('data-id', 'lastUpdated')
    lastActivity.setAttribute('data-sort', '1')

    var dueDate = Forge.build({dom: 'th', text: engagement.duedate.text, id: engagement.duedate.id})
    var dueDateIcon = Forge.build({dom: 'i', class: 'auvicon-sort-down'})
    dueDate.appendChild(dueDateIcon)
    dueDate.setAttribute('data-id', 'dueDate')
    dueDate.setAttribute('data-sort', '1')

    engagementRow.appendChild(engagementTitle)
    engagementRow.appendChild(auditFirm)
    engagementRow.appendChild(status)
    engagementRow.appendChild(auditorAssigned)
    engagementRow.appendChild(completedDocs)
    engagementRow.appendChild(clientAssigned)
    engagementRow.appendChild(lastActivity)
    engagementRow.appendChild(dueDate)

    engagementThead.appendChild(engagementRow)
    engagementTable.appendChild(engagementThead)
    engagementTable.appendChild(engagementTbody)

    parentContainer.appendChild(pageHeader)
    pageHeader.appendChild(pageHeadContent)
    pageHeadContent.appendChild(pageHeadLeft)
    pageHeadLeft.appendChild(pageHeadText)

    container.appendChild(rowFilter)
    container.appendChild(engagementTable)

    var mainPage = Forge.build({ 'dom': 'div', 'id': 'preview-main', 'class': 'dashboard', 'style': 'display:none;' })
    mainPage.appendChild(container)

    parentContainer.appendChild(mainPage)
    parentContainer.appendChild(noEngagementContainer[0])

    var footerComponent = Forge.build({dom: 'div', id: 'client-preview-footer'})
    footerComponent.innerHTML = c.footer
    parentContainer.appendChild(footerComponent)

    return parentContainer
  }

  var requestAPI = function (e, action, actionName) {
    // TODo: check actions
    switch (action) {
      case 'sort':
        // var targetName = e.target.childNodes.length > 0 ? $(e.target).text() : $(e.target).parent('th').text()
        var targetName = e.target.attributes.getNamedItem('data-id').nodeValue
        // TODO('call APIs for action ' + action + ' of ' + targetName + ' with action name ' + actionName)
        engagementTableData.sort(getSortOrder(targetName, actionName === 'up' ? '1' : '0'))
        loadData(engagementTableData)
        break
      case 'filters':
        // TODO('call APIs for action ' + action + ' with action name ' + actionName)
        var category = e.getAttribute('type')
        engagementTableData = filterData(category, actionName)
        if (engagementTableData.length > 0) {
          loadData(engagementTableData)
        } else {
          document.getElementById('engagement-tbody').innerHTML = ''
        }
        break
      case 'search':
        // TODO('call APIs for action ' + action + ' with keyword ' + actionName)
        engagementTableData = searchData(actionName)
        if (engagementTableData.length > 0) {
          loadData(engagementTableData)
        } else {
          document.getElementById('engagement-tbody').innerHTML = ''
        }
        break
    }
    // TODO: refresh data
    // $('#engagement-tbody tr').remove()
    // c.myEngagements.forEach((engagement, index) => {
    //   if (index < 5) addEngagementRow(engagement)
    // })
  }

  var fullName = function (firsName, lasName) {
    return firsName + ' ' + lasName
  }

  // Return css for label status
  var getCssStatus = function (status) {
    var lowercaseStatus = status.toLowerCase()
    return 'ui label ' + lowercaseStatus
  }

  // show and hide tooltip for columns completed To-Dos and completed Docs
  var showTooltip = function (dom, id) {
    $(dom).popup({
      on: 'hover',
      popup: '#completed-' + id,
      position: 'bottom center',
      delay: {
        show: 50,
        hide: 0
      },
      onHidden: function () {
        clearPopupClass(id)
      }
    })
  }

   /**
   * All event engagement
   */
  var engagementSort = function (e) {
    var target = e.target.childNodes.length > 0 ? $(e.target).find('i') : $(e.target)
    var checkDownClass = target.hasClass('auvicon-sort-down')

    target
        .removeClass(checkDownClass ? 'auvicon-sort-down' : 'auvicon-sort-up')
        .addClass(checkDownClass ? 'auvicon-sort-up' : 'auvicon-sort-down')
    requestAPI(e, 'sort', checkDownClass ? 'up' : 'down')
  }

  var engagementSearch = function (e) {
    if (e.which === 13) {
      requestAPI(e, 'search', e.target.value)
    }
  }

  var clearPopupClass = function (id) {
    var element = document.getElementById('completed-' + id)
    element.classList.remove('hidden')
  }

  var getSortOrder = function (prop, asc) {
    return function (a, b) {
      if (asc === '1') {
        return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0)
      } else {
        return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0)
      }
    }
  }

  var convertEngagements = function (engagementList) {
    let list = []
    engagementList.forEach((engagement) => {
      list.push(Utility.convertEngagement(engagement))
    })

    return list
  }

   /**
   * Registers all Engagement Dashboard events
   */
  var registerEvents = function () {
    document.getElementById('engagement-sort').addEventListener('click', engagementSort)
    document.getElementById('firm-sort').addEventListener('click', engagementSort)
    document.getElementById('status-sort').addEventListener('click', engagementSort)
    document.getElementById('audit-sort').addEventListener('click', engagementSort)
    document.getElementById('client-sort').addEventListener('click', engagementSort)
    document.getElementById('docs-sort').addEventListener('click', engagementSort)
    document.getElementById('activity-sort').addEventListener('click', engagementSort)
    document.getElementById('duedate-sort').addEventListener('click', engagementSort)

    document.getElementById('engagement-search').addEventListener('keyup', engagementSearch)
  }

  var filterData = function (category, value) {
    let filterResult = []
    let data = engagementData
    if (category === '' || value === '' || value === 'All') {
      return data
    }

    var typeList = JSON.parse(JSON.stringify(engagementFiltersData[1].subMenu))
    typeList.splice(typeList.length - 1, 1)
    var length = data.length
    for (var i = 0; i < length; i++) {
      let element = data[i]
      if (value === 'Other' && typeList.indexOf(element.type) === -1) {
        filterResult.push(element)
      } else if (value !== 'Other' && element.type === value) {
        filterResult.push(element)
      }
    }
    log('typeList', typeList)
    log('filter category: ' + category + ' value: ' + value)
    log('filter on data', data)
    log('filter result', filterResult)
    return filterResult
  }

  var searchData = function (key) {
    let searchResult = []
    let data = engagementData
    if (key === '') {
      return data
    }
    var length = data.length
    for (var i = 0; i < length; i++) {
      var regex = new RegExp(key, 'i')
      let element = data[i]
      for (let k in element) {
        let item = element[k]
        if (element.hasOwnProperty(k) && (k === 'dueDate' || k === 'lastUpdated')) {
          item = ('0' + item.getDate()).slice(-2) + '/' + ('0' + (item.getMonth() + 1)).slice(-2) + '/' + item.getFullYear().toString().substr(-2)
        }
        if (element.hasOwnProperty(k) && regex.test(item)) {
          searchResult.push(element)
          break
        }
      }
    }
    log('search key: ' + key)
    log('search on data', data)
    log('search result', searchResult)
    return searchResult
  }

  var loadData = function (engagementList) {
    if (c.myEngagements && c.myEngagements !== undefined && c.myEngagements.length === 0) {
      noEngagementContainer.show()
    } else {
      noEngagementContainer.hide()
      document.getElementById('preview-main').style.display = 'inherit'
      // c.getEngagements(c.currentUser._id, c.currentUser.type)
      engagementTableData = engagementList
      const tbody = document.getElementById('engagement-tbody')
      tbody.innerHTML = ''
      engagementList.forEach((engagement) => {
        addEngagementRow(engagement)
      })
    }
  }

  /**
   * This function will be used when looping through c.myEngagements[] and populate each engagement into the table.
   */
  var addEngagementRow = function (engagement) {
    const tbody = document.getElementById('engagement-tbody')
    var newRow = Forge.build({dom: 'tr'})
    tbody.appendChild(newRow)

    // column name engagement
    var newFirm = Forge.build({dom: 'td', 'text': engagement.firm})
    var newLinkEngagemenName = Forge.build({dom: 'a', 'text': engagement.name, href: '#'})
    var newEngagemenName = Forge.build({dom: 'td', class: 'engagement-name'})
    newEngagemenName.appendChild(newLinkEngagemenName)
    newEngagemenName.addEventListener('click', () => {
      c.loadEngagement(engagement._id)
    })

    var newStatus = Forge.build({dom: 'td', class: 'status'})
    var spanStatus = Forge.build({dom: 'span', 'text': engagement.status.toLowerCase(), class: getCssStatus(engagement.status)})
    newStatus.appendChild(spanStatus)

    newRow.appendChild(newEngagemenName)
    newRow.appendChild(newFirm)
    newRow.appendChild(newStatus)

    // Process for auditor assigned
    var newAuditAssigned = Forge.build({dom: 'td', text: engagement.leadAuditor})
    newRow.appendChild(newAuditAssigned)

    var newClientAssigned = Forge.build({dom: 'td', text: engagement.leadClient})
    // var newCompletedDocs = Forge.build({dom: 'td', text: 'completed Docs'})
    var newCompletedDocs = Forge.build({dom: 'td', class: 'completed-docs'})
    var newSpanCompletedDocs = Forge.build({dom: 'span', class: 'warning', text: engagement.completedDocs})

    let date = engagement.lastUpdated
    let dateString = ''
    if (engagement.lastUpdated) {
      dateString = ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear().toString().substr(-2)
    }
    var newLastActivity = Forge.build({dom: 'td', text: dateString})

    date = engagement.dueDate
    dateString = ''
    var newDueDate
    if (engagement.dueDate) {
      dateString = ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear().toString().substr(-2)
      newDueDate = Forge.build({dom: 'div', text: ' Due: ' + dateString, style: 'display: inline-block; width: 143px; height: 32px; border: 1px solid #E1E1E1; vertical-align: middle; border-radius: 100px; text-align: center; padding: 8px; margin-right: 24px; margin-top: 6px;', class: 'auvicon-calendar'})
    } else {
      newDueDate = Forge.build({dom: 'td', text: dateString})
    }

    newRow.appendChild(newCompletedDocs)
    newRow.appendChild(newClientAssigned)
    newRow.appendChild(newLastActivity)
    newRow.appendChild(newDueDate)

    var tooltipCompleted = Forge.build({dom: 'div', id: 'completed-' + engagement._id, class: 'ui special popup completed-todo-popup'})

    // container for information complete todo and docs
    var overdueContainer = Forge.build({dom: 'div', class: 'overdue'})

    var overdueNumber = Forge.build({dom: 'span', class: 'number', text: engagement.overdueDoc.toString()})
    var overdueTitle = Forge.build({dom: 'span', text: 'Overdue'})
    var overdueTodoText = Forge.build({dom: 'span', text: 'Documents'})

    var outstandingContainer = Forge.build({dom: 'div', class: 'oustanding'})
    var outstandingNumber = Forge.build({dom: 'span', class: 'number', text: engagement.outstandingDoc.toString()})
    var outstandingTitle = Forge.build({dom: 'span', text: 'Oustanding'})
    var outstandingTodoText = Forge.build({dom: 'span', text: 'Documents'})

    var totalContainer = Forge.build({dom: 'div', class: 'total'})
    var totalNumber = Forge.build({dom: 'span', class: 'number', text: engagement.totalDoc.toString()})
    var totalTitle = Forge.build({dom: 'span', text: 'Total'})
    var totalTodoText = Forge.build({dom: 'span', text: 'Documents'})

    var closeButton = Forge.build({dom: 'i', class: 'remove icon'})

    overdueContainer.appendChild(overdueNumber)
    overdueContainer.appendChild(overdueTitle)
    overdueContainer.appendChild(overdueTodoText)
    outstandingContainer.appendChild(outstandingNumber)
    outstandingContainer.appendChild(outstandingTitle)
    outstandingContainer.appendChild(outstandingTodoText)
    totalContainer.appendChild(totalNumber)
    totalContainer.appendChild(totalTitle)
    totalContainer.appendChild(totalTodoText)

    tooltipCompleted.appendChild(closeButton)
    tooltipCompleted.appendChild(overdueContainer)
    tooltipCompleted.appendChild(outstandingContainer)
    tooltipCompleted.appendChild(totalContainer)

    newSpanCompletedDocs.appendChild(tooltipCompleted)
    newCompletedDocs.appendChild(newSpanCompletedDocs)

    showTooltip(newSpanCompletedDocs, engagement._id)
  }

  this.build() // from viewTemplate(parentObj)

  initialize()
  return this
}

module.exports = Client
