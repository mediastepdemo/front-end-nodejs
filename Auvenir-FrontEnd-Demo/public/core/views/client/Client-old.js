'use strict'

import ViewTemplate from '../ViewTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'

import Widget_EngagementInfo from '../../widgets/engagement-info/Widget_EngagementInfo'

function Client (ctrl, settings) {
  ViewTemplate.call(this, ctrl, settings)

  this.name = 'Client Module'

  var self = this
  var c = ctrl

  var MAX_COLUMNS = 4
  var widgetContainer = {pageID: 'client-main', CURRENT_X_OFFSET: 0, CURRENT_Y_OFFSET: 0}
  var completedContainer = {pageID: 'client-completed', CURRENT_X_OFFSET: 0, CURRENT_Y_OFFSET: 0}

  var currentPage = null
  var pages = {dashboard: {pageID: 'client-main', linkID: 'clientDashboardLink'},
    completed: {pageID: 'client-completed', linkID: 'clientCompletedLink'}}

  this.dashboardWidgets = []
  var auditStatusWidget = null
  var myAuditorWidget = null
  var DASHBOARD_WIDTH = 1200
  var WIDGET_WIDTH = 264
  var WIDGET_HEIGHT = 300
  var X_OFFSET = 30
  var Y_OFFSET = 30

  var dashboardWidgets = this.dashboardWidgets = []
  /**
   * Initializes the Engagement Dashboard and any relevant functionality
   */
  var initialize = function () {
    log('Client Dashboard: Initializing ...')
    self.build()
    registerEvents()

    viewPage('dashboard')
  }

  var viewPage = this.viewPage = function (name) {
    log('View Page: ' + name)

    var page = null
    if (typeof name === 'string') {
      page = pages[name]
    } else if (typeof name === 'object') {
      if (name instanceof MouseEvent) {
        var id = name.target.id

        var pageKeys = Object.keys(pages)
        for (var i = 0; i < pageKeys.length; i++) {
          if (pages[pageKeys[i]].linkID === id) {
            page = pages[pageKeys[i]]
          }
        }
      }
    }

    if (!page) return

    if (currentPage !== null) {
      document.getElementById(currentPage.linkID).classList.toggle('active')
      document.getElementById(currentPage.pageID).style.display = 'none'
      document.getElementById(currentPage.linkID).addEventListener('click', viewPage)
    }

    currentPage = page

    document.getElementById(currentPage.linkID).classList.toggle('active')
    document.getElementById(currentPage.pageID).style.display = 'inherit'
    document.getElementById(currentPage.linkID).removeEventListener('click', viewPage)
  }

  /**
   * Sets up the engagements listeners
   */
  this.setupListeners = function () {
    log('Dashboard: Setting up listeners ...')
  }

  /**
   * Registers all Engagement Dashboard events
   */
  var registerEvents = function () {
    log('Client Dashboard: Registering Event Listeners ...')

    document.getElementById('clientDashboardLink').addEventListener('click', viewPageDashboard)
    document.getElementById('clientCompletedLink').addEventListener('click', viewPageCompleted)
  }

  /**
   * View Different Dashboards
   */
  var viewPageDashboard = function () {
    viewPage('dashboard')
  }
  var viewPageCompleted = function () {
    viewPage('completed')
  }

  /**
   * Tears down all Engagement Dashboard events
   */
  var teardownEvents = function () {
    log('Client Dashboard: Removing Event Listeners ...')
  }

  var addWidget = this.addWidget = function (parent, myWidget) {
    var parentDom = document.getElementById(parent)
    parentDom.appendChild(myWidget)
  }

  var addEngagement = function (engagement) {
    /*
    * Remove empty state elements upon engagement creation
    */
    document.getElementById('clientPageNoEngagement').style.display = 'none'

    var engContainer

    if (engagement.dateCompleted === null) {
      engContainer = widgetContainer
    } else {
      engContainer = completedContainer
    }
    /*
    *create engagement widget
    */
    var widget1 = new Widget_EngagementInfo(ctrl, engagement)
    addWidget(engContainer.pageID, widget1.buildWidget({ top: engContainer.CURRENT_Y_OFFSET + 'px',
      left: engContainer.CURRENT_X_OFFSET + 'px',
      width: WIDGET_WIDTH + 'px',
      height: WIDGET_HEIGHT + 'px',
      background: '#fff',
      border: '0px solid red',
      draggable: false,
      resizable: false }))

    widget1.applySettings()
    widget1.refresh()
    dashboardWidgets.push(widget1)

    engContainer.CURRENT_X_OFFSET = engContainer.CURRENT_X_OFFSET + WIDGET_WIDTH + X_OFFSET
    if (engContainer.CURRENT_X_OFFSET > (DASHBOARD_WIDTH - WIDGET_WIDTH)) {
      engContainer.CURRENT_X_OFFSET = 0
      engContainer.CURRENT_Y_OFFSET = engContainer.CURRENT_Y_OFFSET + WIDGET_HEIGHT + Y_OFFSET
    }
  }

  /**
   * Load one engagement in as the current engagement
   */
  this.loadDashboard = function (myEngagements) {
    myEngagements.forEach(function (e) {
      addEngagement(e)
    })
    document.getElementById(self._moduleID).classList.add('scroll-190')
  }

  this.refresh = function () {

  }

  this.buildPage = function () {
    log('Client: Building Page')

    var parentContainer = Forge.build({'dom': 'div', 'id': 'clientPage', 'style': 'min-height: 600px'})
    var completeDashboard = Forge.build({'dom': 'div', 'id': 'client-completed', 'class': 'dashboard'})
    var mainDashboard = Forge.build({'dom': 'div', 'id': 'client-main', 'class': 'dashboard'})

    log('Building Engagment Dashboard Links')
    var parentDiv = Forge.build({'dom': 'div'})

    var pageHeader = Forge.build({'dom': 'div', 'class': 'pageHeader noSelect'})
    var pageHeadContent = Forge.build({'dom': 'div', 'class': 'pageHeader-content'})
    var pageHeadLeft = Forge.build({'dom': 'div', 'id': 'c-header-left', 'class': 'pageHeader-leftContainer'})
    var pageHeadText = Forge.build({'dom': 'span', 'class': 'pageHeader-title', 'text': 'My Audits', 'id': 'c-header-title'})

    // var newAuditBtn = Forge.build({'dom':'button','id':'newAuditBtn','class':'btn btn-lg','style':'float:right; margin: 18px 0px;','text':'New Audit'});

    parentDiv.appendChild(pageHeader)
    pageHeader.appendChild(pageHeadContent)
    pageHeadContent.appendChild(pageHeadLeft)
    pageHeadLeft.appendChild(pageHeadText)

    // pageHeadContent.appendChild(newAuditBtn);

    var dashboardLinksDiv = Forge.build({'dom': 'div', 'id': 'c-navLinks', 'class': 'dashboard-pageNav-container'})
    parentDiv.appendChild(dashboardLinksDiv)

    var dashboardLink = Forge.build({'dom': 'div', 'id': 'clientDashboardLink', 'class': 'dashboard-pageNav'})
    var dashLinkFA = Forge.build({'dom': 'span', 'class': 'icon-open-folder clientDash-icon'})
    var dashLinkText = document.createTextNode(' In Progress')

    dashboardLinksDiv.appendChild(dashboardLink)
    dashboardLink.appendChild(dashLinkFA)
    dashboardLink.appendChild(dashLinkText)

    var docLink = Forge.build({'dom': 'div', 'id': 'clientCompletedLink', 'class': 'dashboard-pageNav'})
    var dashLinkFA = Forge.build({'dom': 'span', 'class': 'icon-audit-box clientDash-icon'})
    var dashLinkText = document.createTextNode(' Completed')

    dashboardLinksDiv.appendChild(docLink)
    docLink.appendChild(dashLinkFA)
    docLink.appendChild(dashLinkText)

    var noEngagementContainer = Forge.build({'dom': 'div', 'id': 'clientPageNoEngagement', 'class': 'noEngagement-container'})
    var noEngagementImage = Forge.build({'dom': 'img', 'src': 'images/illustrations/computerDesktop.svg'})
    var noEngagementTxt = Forge.build({'dom': 'h2', 'class': 'noEngagement-txt', 'text': 'You don\'t have any audits yet. We are still waiting on your auditor.'})

    noEngagementContainer.appendChild(noEngagementImage)
    noEngagementContainer.appendChild(noEngagementTxt)

    parentDiv.appendChild(noEngagementContainer)

    parentContainer.appendChild(parentDiv)
    parentContainer.appendChild(completeDashboard)
    parentContainer.appendChild(mainDashboard)
    return parentContainer
  }

  initialize()

  return this
}

Client.prototype = Object.create(ViewTemplate.prototype)
Client.prototype.constructor = Client

module.exports = Client
