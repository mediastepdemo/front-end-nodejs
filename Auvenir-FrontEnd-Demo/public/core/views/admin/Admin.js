'use strict'

import ViewTemplate from '../ViewTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'

import Widget_AnalyticsHeadline from '../../widgets/analytics-headline/Widget_AnalyticsHeadline'
import Widget_ManageUsers from '../../widgets/manage-users/Widget_ManageUsers'
import styles from './Admin.css'

/**
 *  Admin Portal for Admin user who has @auvenir.com in their email.
 *  This Portal is responsible for Managing Users Approval of user, removing user, update status of users.
 *
 *  @param {object} ctrl - Our main application Controller.
 *  @param {object} container - DOM Object of Container which Admin portal will attach to.
 */
var Admin = function (ctrl, settings) {
  ViewTemplate.call(this, ctrl, settings)

  var self = this
  var name = this.name = 'Admin'
  var c = ctrl

  var widgetContainer = 'admin-main'
  var headline1 = null
  var headline2 = null
  var headline3 = null
  var headline4 = null
  var usersWidget = null
  var chartWidget = null

  /**
   * Responsilbe for Initialize Admin Portal
   */
  var initialize = function () {
    log('Admin: initializing ...')

    if (!Widget_AnalyticsHeadline || !Widget_ManageUsers) {
      return false
    } else {
      self.build()
      registerEvents()
      return true
    }
  }

  /**
   * Register Events for Admin Portal
   */
  var registerEvents = function () {
    log('Admin: Registering Event Listeners ...')
  }

  /**
   * Add a widget dom objecto parent DOM obejct.
   *
   * @param {string} parent String of id of DOM Object that will add myWidget DOM
   * @param {object} myWidget widget dom object that will attach to parent.
   */
  this.addWidget = function (parent, myWidget) {
    var parentDom = document.getElementById(parent)
    if (parentDom) {
      parentDom.appendChild(myWidget)
    }
  }

  /**
   * Build Web Page for Admin Module.
   */

  this.buildPage = function () {
    log('Admin: Building page')

    var parentContainer = Forge.build({'dom': 'div', 'id': 'adminPage', class: 'parent-of-footer'})
    var mainDashboard = Forge.build({'dom': 'div', 'id': 'admin-main', 'class': 'dashboard dashboard-offset ui-helper-clearfix'})

    log('Building Engagment Dashboard Links')

    var pageHeader = Forge.build({'dom': 'div', 'class': 'pageHeader noSelect'})
    var pageHeadContent = Forge.build({'dom': 'div', 'class': 'pageHeader-content'})
    var pageHeadBack = Forge.build({'dom': 'div', 'class': 'pageHeader-leftContainer'})
    var pageHeadText = Forge.build({'dom': 'span', 'class': 'pageHeader-title', 'text': 'Admin', 'id': 'pageHeadBackText'})
    var getDevCredsBtnCreate = Forge.buildBtn({'id': 'm-admin-getDevCredsBtn', 'float': 'right', 'text': 'Get Credentials', 'margin': '5px 5px', 'type': 'darkblue'})
    var getDevCredsBtn = getDevCredsBtnCreate.obj
    var viewDevCredsBtnCreate = Forge.buildBtn({'id': 'm-admin-viewDevCredsBtn', 'float': 'right', 'text': 'View Credentials', 'margin': '5px 5px', 'type': 'primary'})
    var viewDevCredsBtn = viewDevCredsBtnCreate.obj

    pageHeader.appendChild(pageHeadContent)

    pageHeadContent.appendChild(pageHeadBack)
    pageHeadBack.appendChild(pageHeadText)
    pageHeadContent.appendChild(getDevCredsBtn)
    pageHeadContent.appendChild(viewDevCredsBtn)

    getDevCredsBtn.addEventListener('click', function (evt) {
      self.trigger('dev-signup', evt, {msg: 'hello'})
    })

    viewDevCredsBtn.addEventListener('click', function (evt) {
      self.trigger('view-dev-creds', evt, {})
    })

    parentContainer.appendChild(pageHeader)
    parentContainer.appendChild(mainDashboard)

    var footerComponent = Forge.build({dom: 'div', id: 'admin-footer'})
    footerComponent.innerHTML = c.footer
    parentContainer.appendChild(footerComponent)

    return parentContainer
  }

  this.credentialsSet = function (devSet) {
    if (devSet) {
      document.getElementById('m-admin-getDevCredsBtn').style.display = 'none'
      document.getElementById('m-admin-viewDevCredsBtn').style.display = 'inherit'
    } else {
      document.getElementById('m-admin-getDevCredsBtn').style.display = 'inherit'
      document.getElementById('m-admin-viewDevCredsBtn').style.display = 'none'
    }
  }

  /**
   * Load Dashboard.
   * Build 4 Head Lines for showing number of User, CPA, engagements, and Businesses and Manage User Widget for Admin portal.
   */
  this.loadDashboard = function () {
    document.getElementById(self._moduleID).classList.add('scroll-190')

    headline1 = new Widget_AnalyticsHeadline(c)
    headline2 = new Widget_AnalyticsHeadline(c)
    headline3 = new Widget_AnalyticsHeadline(c)
    headline4 = new Widget_AnalyticsHeadline(c)

    usersWidget = new Widget_ManageUsers(c)
    usersWidget.setupListeners()

    this.addWidget(widgetContainer, headline1.buildWidget({
      width: '264px',
      height: '100px',
      background: '#0071A4',
      border: '0px solid red',
      overflow: false,
      draggable: false,
      resizable: false
    }))

    this.addWidget(widgetContainer, headline2.buildWidget({
      width: '264px',
      height: '100px',
      background: '#008BC9',
      border: '0px solid red',
      overflow: false,
      draggable: false,
      resizable: false
    }))

    this.addWidget(widgetContainer, headline3.buildWidget({
      width: '264px',
      height: '100px',
      background: '#369CC9',
      border: '0px solid red',
      overflow: false,
      draggable: false,
      resizable: false
    }))

    this.addWidget(widgetContainer, headline4.buildWidget({
      width: '264px',
      height: '100px',
      background: '#65BAE0',
      border: '0px solid red',
      overflow: false,
      draggable: false,
      resizable: false
    }))

    this.addWidget(widgetContainer, usersWidget.buildWidget({
      width: '100%',
      background: '#fff',
      border: '0px solid red',
      overflow: true,
      draggable: false,
      resizable: false
    }))
    headline1.applySettings()
    headline2.applySettings()
    headline3.applySettings()
    headline4.applySettings()
    usersWidget.applySettings()

    document.getElementById(widgetContainer).style.display = 'inherit'
  }

  /**
   * Load all of datas of users, businesses, and engagements for Admin Module
   *
   * @param: {json} data - array of All user data in JSON format.  for admin module.
   */
  this.loadData = function (data) {
    log('Admin: Load WidgetData')
    if (data) {
      usersWidget.loadData(data)

      var userCount = 0
      var auditorCount = 0
      var uniqueBusinesses = []
      var uniqueEngagements = []

      for (var i = 0; i < data.length; i++) {
        var entry = data[i]
        if (entry.user.type === 'AUDITOR') {
          auditorCount++
          userCount++
        } else if (entry.user.type === 'CLIENT') {
          userCount++
        }

        for (var j = 0; j < entry.businesses.length; j++) {
          var biz = entry.businesses[j]
          if (uniqueBusinesses.indexOf(biz._id) === -1) {
            uniqueBusinesses.push(biz._id)
          }
        }

        for (var j = 0; j < entry.engagements.length; j++) {
          var eng = entry.engagements[j]
          if (uniqueEngagements.indexOf(eng._id) === -1) {
            uniqueEngagements.push(eng._id)
          }
        }
      }
      headline1.loadData({ 'title': 'Auvenir Users', 'value': userCount, 'fa': 'fa-users' })
      headline2.loadData({ 'title': 'Auditors', 'value': auditorCount, 'fa': 'fa-user' })
      headline3.loadData({ 'title': 'Businesses', 'value': uniqueBusinesses.length, 'fa': 'fa-building' })
      headline4.loadData({ 'title': 'Engagements', 'value': uniqueEngagements.length, 'fa': 'fa-paper-plane' })
    } else {
      c.displayMessage('Admin Data is not available.')
    }
  }

  /**
   * Response Handler for Verify Auditor.
   * If it approves, user's status updated to onboard and show message.
   *
   * @param {string} tagName - tagName of event-response.
   * @param {json}   data - response data of the socket event 'verify-auditor'.
   */
  var handle_verifyAuditorResponse = function (tagName, data) {
    log('> Event: verify-auditor-response')
    if (data.code) {
      c.displayMessage(data.msg, {'color': 'red'})
    } else {
      c.displayMessage('Auditor verification successful.')
      // Update Front-end Status
      document.getElementById('w-mu-approve-' + data.userID).style.display = 'none'
      document.getElementById('w-mu-edit-' + data.userID).style.display = 'inherit'
      document.getElementById('w-mu-status-' + data.userID).innerHTML = 'ONBOARDING'
    }
  }

  /**
   * Response Handler for Onboard User.
   * If it approves, user's status updated to onboard and show message.
   *
   * @param {string} tagName: tagName of event-response.
   * @param {json}   data: response data of the socket event 'onboard-user'.
   */
  var handle_OnboaordUserResponse = function (tagName, data) {
    log('> Event: onboard-user-response')
    if (data.code) {
      c.displayMessage(data.msg, {'color': 'red'})
    } else {
      c.displayMessage('User status updated.')
      // Update Front-end Status.
      document.getElementById('w-mu-approve-' + data.userID).style.display = 'none'
      document.getElementById('w-mu-edit-' + data.userID).style.display = 'inherit'
      document.getElementById('w-mu-status-' + data.userID).innerHTML = 'ONBOARDING'
    }

    headline1.loadData({ 'title': 'Auvenir Users', 'value': userCount, 'fa': 'fa-users' })
    headline2.loadData({ 'title': 'Auditors', 'value': auditorCount, 'fa': 'fa-user' })
    headline3.loadData({ 'title': 'Businesses', 'value': uniqueBusinesses.length, 'fa': 'fa-building' })
    headline4.loadData({ 'title': 'Engagements', 'value': uniqueEngagements.length, 'fa': 'fa-paper-plane' })
  }

  /**
   * Setup Listners for verifyauditor and onboard user.
   * When Verifying auditor and onboard user is succesful, we will change the status of users and also showing message.
   */
  this.setupListeners = function () {
    log('Admin DashBoard: Setting up listeners ...')
  }

  if (!initialize()) {
    return null
  } else {
    return this
  }
}

Admin.prototype = Object.create(ViewTemplate.prototype)
Admin.prototype.constructor = Admin

module.exports = Admin
