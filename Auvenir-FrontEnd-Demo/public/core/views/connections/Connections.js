'use strict'

import ViewTemplate from '../ViewTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'

var Connections = function (ctrl, settings) {
  ViewTemplate.call(this, ctrl, settings)

  this.name = 'Connections'

  var self = this
  var c = ctrl

  var initialize = function () {
    log('Connections: Initializing ...')

    self.build()
    registerEvents()
  }

  this.buildPage = function () {
    log('Connections: Building Page')

    var parentContainer = Forge.build({'dom': 'div', 'id': 'connectionsPage'})

    var pageHeader = Forge.build({'dom': 'div', 'class': 'pageHeader noSelect'})
    var pageHeadContent = Forge.build({'dom': 'div', 'class': 'pageHeader-content'})
    var pageHeadBack = Forge.build({'dom': 'div', 'class': 'pageHeader-leftContainer'})
    var pageHeadFA = Forge.build({'dom': 'i', 'id': 'connections-backButton', 'class': 'fa fa-chevron-left pageHeader-backButton'})
    var pageHeadText = Forge.build({'dom': 'span', 'class': 'pageHeader-title', 'text': 'Connections'})

    pageHeader.appendChild(pageHeadContent)

    pageHeadContent.appendChild(pageHeadBack)
    pageHeadBack.appendChild(pageHeadFA)
    pageHeadBack.appendChild(pageHeadText)

    parentContainer.appendChild(pageHeader)
    return parentContainer
  }

  var registerEvents = function () {
    log('Connections: Registering Event Listeners ...')

    document.getElementById('connections-backButton').addEventListener('click', c.goToPreviousPage)
  }

  initialize()

  return this
}

Connections.prototype = Object.create(ViewTemplate.prototype)
Connections.prototype.constructor = Connections

module.exports = Connections
