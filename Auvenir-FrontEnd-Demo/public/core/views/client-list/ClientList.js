'use strict'

import ViewTemplate from '../ViewTemplate'
import Utility from '../../../js/src/utility_c'
import Forge from '../../Forge'
require('./ClientList.css')

function ClientList (ctrl, settings) {
  ViewTemplate.call(this, ctrl, settings)

  this.name = 'Client List Module'
  var c = ctrl

  var text = c.lang.contacts

  const SORT_TYPE = { FIRM: 1, TYPE: 2, CONTACT: 3, ROLE: 4, STATUS: 5 }

  var checkIDs = []
  var contactIDs = []
  var contactArray = []

  this.buildPage = function () {
    var parentContainer = Forge.build({'dom': 'div', 'id': 'clientListPage', class: 'parent-of-footer'})

    var mainDashboard = Forge.build({
      'dom': 'div',
      'id': 'clientList-main',
      'class': 'dashboard',
      'style': 'display:inherit'
    })

    var mainPage = Forge.build({'dom': 'div', id: 'clientList-main-page'})

    var pageHeader = Forge.build({'dom': 'div', 'class': 'pageHeader noSelect'})
    var pageHeadContent = Forge.build({'dom': 'div', 'id': 'clientList-head-content', 'class': 'pageHeader-content'})
    var pageHeadLeft = Forge.build({'dom': 'div', 'id': 'clientList-header-left', 'class': 'pageHeader-leftContainer'})
    var pageHeadText = Forge.build({
      'dom': 'span',
      'class': 'pageHeader-title',
      'text': 'Contacts',
      'id': 'c-header-title'
    })

    // var newClientBtn = Forge.buildBtn({ 'id': 'clientList-newClientBtn', 'type': 'darkblue', 'text': 'Add New' }).obj
    // newClientBtn.style.float = 'right'
    // newClientBtn.style.margin = '2px 0px'

    pageHeader.appendChild(pageHeadContent)
    pageHeadContent.appendChild(pageHeadLeft)
    pageHeadLeft.appendChild(pageHeadText)
    // pageHeadContent.appendChild(newClientBtn)
    mainPage.appendChild(pageHeader)
    // No Clients
    var noClientContainer = Forge.build({'dom': 'div', 'id': 'clientList-noClients', 'class': 'clientList-noClientDiv'})
    var noClientImg = Forge.build({
      'dom': 'img',
      'src': 'images/illustrations/illustration-clients.svg',
      'style': 'margin-left:44px;'
    })
    var noClientTxt = Forge.build({
      'dom': 'h2',
      'class': 'noEngagement-txt',
      'text': 'You don\'t have any contacts.'
    })
    noClientContainer.appendChild(noClientImg)
    noClientContainer.appendChild(noClientTxt)

    mainPage.appendChild(noClientContainer)
    parentContainer.appendChild(mainPage)
    parentContainer.appendChild(mainDashboard)

    // newClientBtn.addEventListener('click', function () {
    //   c.displayAddClientModal()
    // })

    var footerComponent = Forge.build({dom: 'div', id: 'contacts-footer'})
    footerComponent.innerHTML = c.footer
    parentContainer.appendChild(footerComponent)

    return parentContainer
  }

  this.loadData = function () {
    contactArray = c.myContacts
    if (contactArray && contactArray.length > 0) {
      /* hiding the empty container */
      document.getElementById('clientList-noClients').style.display = 'none'
      var clientContainer
      /* Clearing list to rebuild */
      if (document.getElementById('w-cl-clientContainer')) {
        document.getElementById('w-cl-clientContainer').innerHTML = ''
        clientContainer = document.getElementById('w-cl-clientContainer')
      } else {
        clientContainer = Forge.build({
          'dom': 'div',
          'id': 'w-cl-clientContainer',
          'class': 'contacts-clientContainer'
        })
      }

      var filterDrop = `
        <div id="v-c-filter-ddl" class="ui dropdown contacts-filterDdl">
          <div class="text">${text.filters}</div>
          <i class="dropdown icon"></i>
          <div id="v-c-filter-menu" class="menu">
            <div class="item">${c.lang.status}</div>
          </div>
        </div>`
      var filterDropHolder = Forge.build({dom: 'div', class: 'contacts-filterDdl-holder'})
      filterDropHolder.innerHTML = filterDrop

      var searchInput = `
        <div class="ui input small">
          <input type="text" placeholder=${c.lang.search}>
        </div>`
      var searchDiv = Forge.build({'dom': 'div', class: 'contacts-searchInput'})
      searchDiv.innerHTML = searchInput

      var table = Forge.build({ 'dom': 'table', 'id': 'v-c-table', 'class': 'contacts-table' })
      var tr = Forge.build({'dom': 'tr', 'class': 'contacts-tableHeader', 'style': 'border-bottom:1px solid #'})

      var contactCheckHolder = Forge.build({ dom: 'td', style: 'width: 4%' })
      var contactCheckBox = Forge.build({dom: 'input', type: 'checkbox', class: 'contacts-checkBoxes'})
      contactCheckHolder.appendChild(contactCheckBox)

      var idNum
      contactCheckBox.addEventListener('click', function () {
        if (contactCheckBox.checked) {
          for (var i = 0; i < checkIDs.length; i++) {
            document.getElementById(checkIDs[i]).checked = true
          }
        } else {
          for (var i = 0; i < checkIDs.length; i++) {
            document.getElementById(checkIDs[i]).checked = false
          }
        }
      })

      var tdFirm = Forge.build({'dom': 'td', 'text': text.auditFirm, 'id': 'v-c-thAuditFirm', 'style': 'width: 18%;'})
      var sortFirm = Forge.build({'dom': 'span', 'class': 'icon auvicon-sort-down', 'style': 'color:#6d6d6d;cursor:pointer;font-size:11px;margin-left:4px; margin-top:3px;', 'id': 'v-c-thAuditFirmIcon'})
      sortFirm.addEventListener('click', function () {
        var data = {}
        data.type = SORT_TYPE['FIRM']
        data.arrowDomID = this.id
        data.headerID = 'v-c-thAuditFirm'
        sort(data)
      })
      // tdFirm.appendChild(sortFirm)

      var tdType = Forge.build({'dom': 'td', 'text': text.type, 'id': 'v-c-thType', 'style': 'width:18%; padding-right:25px;'})
      var sortType = Forge.build({'dom': 'span', 'class': 'icon auvicon-sort-down', 'style': 'color:#6d6d6d;cursor:pointer;font-size:11px;margin-left:4px; margin-top:3px;', 'id': 'v-c-thTypeIcon'})
      // tdType.appendChild(sortType)

      sortType.addEventListener('click', function () {
        var data = {}
        data.type = SORT_TYPE['TYPE']
        data.arrowDomID = this.id
        data.headerID = 'v-c-thType'
        sort(data)
      })

      var tdContact = Forge.build({'dom': 'td', 'text': c.lang.contact, 'id': 'v-c-thContact', 'style': 'width:18%;'})
      var sortContact = Forge.build({'dom': 'span', 'class': 'icon auvicon-sort-down', 'style': 'color:#6d6d6d;cursor:pointer;font-size:11px;margin-left:4px; margin-top:3px;', 'id': 'v-c-thContactIcon'})
      // tdContact.appendChild(sortContact)

      var tdRole = Forge.build({ 'dom': 'td', 'text': c.lang.role, 'id': 'v-c-thRole', 'style': 'width:18%; padding-right: 25px;' })
      var sortRole = Forge.build({'dom': 'span', 'class': 'icon auvicon-sort-down', 'style': 'color:#6d6d6d;cursor:pointer;font-size:11px;margin-left:4px; margin-top:3px;', 'id': 'v-c-thRoleIcon'})
      // tdRole.appendChild(sortRole)

      var tdStatus = Forge.build({ 'dom': 'td', 'text': c.lang.status, 'id': 'v-c-thStatus', 'style': 'width:18%; padding-right: 25px;' })
      var sortStatus = Forge.build({'dom': 'span', 'class': 'icon auvicon-sort-down', 'style': 'color:#6d6d6d;cursor:pointer;font-size:11px;margin-left:4px; margin-top:3px;', 'id': 'v-c-thRoleIcon'})
      // tdStatus.appendChild(sortStatus)

      var tdExpand = Forge.build({dom: 'td', style: 'width: 6%'})

      tr.appendChild(contactCheckHolder)
      tr.appendChild(tdFirm)
      tr.appendChild(tdType)
      tr.appendChild(tdContact)
      tr.appendChild(tdRole)
      tr.appendChild(tdStatus)
      tr.appendChild(tdExpand)
      table.appendChild(tr)

      // Builds a sub row and a more info row for each client
      for (var i = 0; i < contactArray.length; i++) {
        var contactID = 'contacts' + i
        contactIDs.push(contactID)

        var checkID = 'v-c-check-' + i
        checkIDs.push(checkID)

        var contactFirm = ''

        /* Match the user to the firm and add the name to their title */
        // if (contactArray[i].type === 'AUDITOR') {
        //   for (var i = 0; i < c.myEngagements.length; i++) {
        //     let acl = c.myEngagements[i].acl
        //     acl.forEach(function (user) {
        //       if (user.id === contactArray[i]._id) {
        //         contactFirm = c.myEngagements[i].firm.name
        //       }
        //     })
        //   }
        // } else if (contactArray[i].type === 'CLIENT') {
        //   for (var i = 0; i < c.myEngagements.length; i++) {
        //     let acl = c.myEngagements[i].acl
        //     acl.forEach(function (user) {
        //       if (user.id === contactArray[i]._id) {
        //         contactFirm = c.myEngagements[i].business.name
        //       }
        //     })
        //   }
        // }

        for (var j = 0; j < c.myEngagements.length; j++) {
          let acl = c.myEngagements[j].acl
          acl.forEach(function (user) {
            if (user.id === contactArray[i]._id) {
              if (contactArray[i].type === 'AUDITOR') {
                contactFirm = c.myEngagements[j].firm.name
              } else {
                contactFirm = c.myEngagements[j].business.name
              }
            }
          })
        }

        var firstLetterCap = function (element) {
          var newCase = element.toLowerCase()
          return newCase.charAt(0).toUpperCase() + newCase.slice(1)
        }

        var contactTypeData = (contactArray[i].type) ? contactArray[i].type : ''
        var contactType = firstLetterCap(contactTypeData)
        var contactFirstName = (contactArray[i].firstName) ? contactArray[i].firstName : ''
        var contactLastName = (contactArray[i].lastName) ? contactArray[i].lastName : ''
        var contactRole = (contactArray[i].jobTitle) ? contactArray[i].jobTitle : ''
        var contactStatusData = (contactArray[i].status) ? contactArray[i].status : ''
        var contactStatus = firstLetterCap(contactStatusData)
        var contactAddress = (contactArray[i].address) ? contactArray[i].address : ''
        var contactEmail = (contactArray[i].email) ? contactArray[i].email : ''
        var contactPhone = (contactArray[i].phone) ? contactArray[i].phone : ''
        // var contactInvitationDate = contactArray[i].dateCreated.substring(0, 10)
        var contactTypeColor = (contactTypeData === 'CLIENT') ? '#294474' : '#B5B5B5'

        var subTableRow = Forge.build({'dom': 'tr', 'id': 'v-c-row-' + contactID, 'class': 'contacts-tr'})
        var subCheckHolder = Forge.build({ dom: 'td' })
        var subCheckBox = Forge.build({dom: 'input', type: 'checkbox', id: checkID, class: 'contacts-checkBoxes'})
        subCheckHolder.appendChild(subCheckBox)

        subCheckBox.addEventListener('click', function () {
          if (subCheckBox.checked === true) {
            subTableRow.className += ' contacts-tr-active'
          } else {
            subTableRow.classList.remove('contacts-tr-active')
          }
        })

        var firmSubTd = Forge.build({'dom': 'td', 'id': 'v-c-contactFirm-' + i, 'class': 'contacts-td', 'text': contactFirm})

        var typeSubTd = Forge.build({'dom': 'td', 'id': 'v-c-type' + i, 'class': 'contacts-td', 'style': 'padding-right:32px;'})
        var typeSubDiv = Forge.build({dom: 'div', class: 'contacts-typeDiv', text: contactType})
        typeSubDiv.style.background = contactTypeColor
        typeSubTd.appendChild(typeSubDiv)

        var contactSubTd = Forge.build({'dom': 'td', 'id': 'v-c-contactTd-' + contactID, 'class': 'contacts-td', 'style': '', 'text': contactFirstName + ' ' + contactLastName})
        var roleSubTd = Forge.build({'dom': 'td', 'id': 'v-c-roleTd-' + contactID, 'class': 'contacts-td', 'style': '', 'text': contactRole})
        var statusSubTd = Forge.build({'dom': 'td', 'id': 'v-c-statusTd-' + contactID, 'class': 'contacts-td', 'style': '', 'text': contactStatus})
        var expandSubTd = Forge.build({dom: 'td', 'class': 'contacts-td'})
        var expandIcon = Forge.build({dom: 'div', id: 'v-c-expandIcon' + contactID, class: 'fa fa-chevron-right contacts-expandIcon'})
        expandSubTd.appendChild(expandIcon)
        subTableRow.appendChild(subCheckHolder)
        subTableRow.appendChild(firmSubTd)
        subTableRow.appendChild(typeSubTd)
        subTableRow.appendChild(contactSubTd)
        subTableRow.appendChild(roleSubTd)
        subTableRow.appendChild(statusSubTd)
        subTableRow.appendChild(expandSubTd)

        var expandedTableRow = Forge.build({'dom': 'tr', 'id': 'v-c-expandedRow-' + contactID, 'class': 'contacts-expanded-tr', style: 'display: none'})
        var expandedTd1 = Forge.build({dom: 'td'})
        var expandedTd2 = Forge.build({dom: 'td', class: 'contacts-td', style: 'padding-bottom: 10px'})
        var addressStreet = Forge.build({dom: 'span', text: contactAddress.street, class: 'contacts-extendedRowSpan'})
        var addressCity = Forge.build({dom: 'span', text: contactAddress.city, class: 'contacts-extendedRowSpan'})
        var addressPostal = Forge.build({dom: 'span', text: contactAddress.postal, class: 'contacts-extendedRowSpan'})
        var addressCountry = Forge.build({dom: 'span', text: contactAddress.country, class: 'contacts-extendedRowSpan'})
        expandedTd2.appendChild(addressStreet)
        expandedTd2.appendChild(addressCity)
        expandedTd2.appendChild(addressPostal)
        expandedTd2.appendChild(addressCountry)
        var expandedTd3 = Forge.build({dom: 'td'})
        var expandedTd4 = Forge.build({dom: 'td', class: 'contacts-td contacts-extended-td'})
        var emailLink = Forge.build({dom: 'a', text: contactEmail, href: 'mailto:' + contactEmail, class: 'contacts-extendedRowSpan contacts-emailLink'})
        var phoneSpan = Forge.build({dom: 'span', text: contactPhone, class: 'contacts-extendedRowSpan'})
        expandedTd4.appendChild(emailLink)
        expandedTd4.appendChild(phoneSpan)
        var expandedTd5 = Forge.build({dom: 'td'})
        var expandedTd6 = Forge.build({dom: 'td', class: 'contacts-td', style: 'padding-bottom: 10px;'})

        // if (contactArray[i].status === 'Pending') {
        //   var invitationSentSpan = Forge.build({dom: 'span', text: text.invitationSent, class: 'contacts-extendedRowSpan'})
        //   var dateSentSpan = Forge.build({dom: 'span', text: contactInvitationDate, class: 'contacts-extendedRowSpan'})
        //   var resendBtn = Forge.buildBtn({ 'id': 'v-c-resendInvite', 'type': 'primary', 'text': text.resendInvite, margin: '10px 0px 0px' }).obj
        //   expandedTd6.appendChild(invitationSentSpan)
        //   expandedTd6.appendChild(dateSentSpan)
        //   expandedTd6.appendChild(resendBtn)
        // }
        var expandedTd7 = Forge.build({dom: 'td'})
        expandedTableRow.appendChild(expandedTd1)
        expandedTableRow.appendChild(expandedTd2)
        expandedTableRow.appendChild(expandedTd3)
        expandedTableRow.appendChild(expandedTd4)
        expandedTableRow.appendChild(expandedTd5)
        expandedTableRow.appendChild(expandedTd6)
        expandedTableRow.appendChild(expandedTd7)

        table.appendChild(subTableRow)
        table.appendChild(expandedTableRow)
      }

      // clientContainer.appendChild(searchDiv)
      // clientContainer.appendChild(filterDropHolder)
      clientContainer.appendChild(table)
      document.getElementById('clientList-main-page').appendChild(clientContainer)

      /* Show more info under each row */
      contactIDs.forEach(function (entry) {
        document.getElementById('v-c-expandIcon' + entry).addEventListener('click', function () {
          if (document.getElementById('v-c-expandIcon' + entry).classList.contains('fa-chevron-right')) {
            document.getElementById('v-c-expandIcon' + entry).classList.remove('fa-chevron-right')
            document.getElementById('v-c-expandIcon' + entry).classList.add('fa-chevron-down')
            document.getElementById('v-c-row-' + entry).classList.add('contacts-tr-active')
            document.getElementById('v-c-expandedRow-' + entry).style.display = 'table-row'
          } else {
            document.getElementById('v-c-expandIcon' + entry).classList.remove('fa-chevron-down')
            document.getElementById('v-c-expandIcon' + entry).classList.add('fa-chevron-right')
            document.getElementById('v-c-row-' + entry).classList.remove('contacts-tr-active')
            document.getElementById('v-c-expandedRow-' + entry).style.display = 'none'
          }
        })
      })
    }
  }

  this.registerEventListeners = function () {
    // show and hide the filter menu
    if (contactArray && contactArray.length > 0) {
      document.getElementById('v-c-filter-ddl').addEventListener('click', function () {
        if (document.getElementById('v-c-filter-menu').style.display === 'none') {
          document.getElementById('v-c-filter-menu').style.display = 'block'
        } else {
          document.getElementById('v-c-filter-menu').style.display = 'none'
        }
      })
    }
  }

  /**
   * This Function is responsible for sorting contacts according to sorting type.
   * @param {object} data is json object that consists of sorting type information.
   */
  var sort = function (data) {
    let fTable = document.getElementById('wFilesTable')
    let tableNodes = $(fTable.querySelector('tbody')).children() // childNodes array

    changeSortArrow(data)

    // modifying the data in previousSort
    if (previousSort.arrowDomID !== null) { // on the same page. (same level of hierarchy)
      if (previousSort.type === data.type) { // clicked on the same field to sort
        previousSort.ascending = data.ascending
        previousSort.sortedArray.reverse()

        // This will prevent sort function to sort other fields together when all Todos or Categories or UploadedDate or UploadedBy are same.
        if (!checkAllValueSame(data)) {
          previousSort.sortedArray.forEach((item) => {
            fTable.querySelector('tbody').appendChild(item.DOM)
          })
        }
      } else {
        previousSort.arrowDomID = data.arrowDomID
        previousSort.type = data.type
        previousSort.ascending = data.ascending
        previousSort.headerID = data.headerID

        // Re-sort the items in previousSort.sortedArray by new type
        if (!checkAllValueSame(data)) {
          previousSort.sortedArray = _.sortBy(previousSort.sortedArray, [data.type])
          previousSort.sortedArray.forEach((item) => {
            fTable.querySelector('tbody').appendChild(item.DOM)
          })
        }
      }
    } else { // on the different page or just loaded the Files page.
      previousSort.arrowDomID = data.arrowDomID
      previousSort.type = data.type
      previousSort.ascending = data.ascending
      previousSort.headerID = data.headerID

      // Grab all the rows in the table.
      _.forEach(tableNodes, (oneRow, i) => {
        let item = {
          Index: i,
          DOM: oneRow,
          Name: $($(oneRow).children()[1]).children()[1].innerText,
          Todo: $(oneRow).children()[2].innerText,
          Category: $(oneRow).children()[3].innerText,
          UploadedDate: $(oneRow).children()[4].innerText,
          UploadedBy: $(oneRow).children()[5].innerText
        }
        previousSort.sortedArray.push(item)
      })

      if (!checkAllValueSame(data)) {
        previousSort.sortedArray = _.sortBy(previousSort.sortedArray, [data.type])
        previousSort.sortedArray.forEach((item) => {
          fTable.querySelector('tbody').appendChild(item.DOM)
        })
      }
    }
  }

  c.loadContacts()
  this.build()
  this.registerEventListeners()

  return this
}
ClientList.prototype = Object.create(ViewTemplate.prototype)
ClientList.prototype.constructor = ClientList

module.exports = ClientList
