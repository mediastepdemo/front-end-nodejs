'use strict'

import ViewTemplate from '../ViewTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import Component_FileManager from '../../components/file-manager/file-manager'
import styles from './fileManager.css'

var FileManager = function (ctrl, settings) {
  ViewTemplate.call(this, ctrl, settings)
  this.name = 'FileManager View'

  const self = this
  const c = ctrl
  var fileManagerDashboard = null
  var fmComponent = null

  /**
   * This function intialize the registerEvents function for engagements dashboard,
   * Runs the background function registerEvents.
   *
   * @memberof Widget_FileManager
  */
  const initialize = function () {
    log('FileManager: Initializing ...')

    self.build()
  }

  /**
   * Responsible for building the DOM elements that are associated with the Widget FileManager Modal,
   * and attaching them to parent modal container.
   * @memberof Widget_FileManager
   */
  this.buildPage = function () {
    // parent: settings.parentModule
    var parentContainer = Forge.build({'dom': 'div', 'id': 'file-manager-view', 'style': 'min-height: 600px; margin-bottom: 15px;'})
    fileManagerDashboard = Forge.build({
      'dom': 'div',
      'id': 'fileManager-dashboard',
      'class': 'dashboard fileManager-dashboard',
      'style': 'display:inherit;'
    })

    parentContainer.appendChild(fileManagerDashboard)
    return parentContainer
  }

  /**
   * This function load all the engagement files of the current audit.
   * @memberof Widget_FileManager
   * @param files [File Object]
   * @param requests [Request Object]
   */
  this.loadData = function (engagement, userType) {
    log('Loading data for file manager view')
    if (!fmComponent) {
      fmComponent = new Component_FileManager(c, fileManagerDashboard, this, engagement)
    }
    fmComponent.loadData(c.currentEngagement.files, c.currentUser.type, false)
    fmComponent.on('flash', (option) => {
      self.trigger('flash', option)
    })
  }

  initialize()

  return this
}

FileManager.prototype = Object.create(ViewTemplate.prototype)
FileManager.prototype.constructor = FileManager

module.exports = FileManager
