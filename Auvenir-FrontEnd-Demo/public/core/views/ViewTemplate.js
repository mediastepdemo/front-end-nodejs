'use strict'

import Utility from '../../js/src/utility_c'
import Forge from '../Forge'
import log from '../../js/src/log'

/**
 * The View Template is responsible for the templating of all views.
 * @class Template
 */
var ViewTemplate = function (ctrl, settings) {
  var self = this

  this.settings = {
    display: (settings && settings.display) ? settings.display : 'inherit',
    scroll: (settings && settings.scroll) ? settings.scroll : false,
    parentModule: (settings && settings.parentModule) ? settings.parentModule : '',
    contacts: (settings && settings.contacts) ? settings.contacts : ''
  }

  this._uniqueID = ''
  this._moduleID = ''
  do {
    this._uniqueID = Utility.randomString(32, Utility.ALPHANUMERIC)
    this._moduleID = 'module-' + this._uniqueID
  } while (document.getElementById(this._moduleID))

  // Event Object
  this.evts = {}

  /**
   * Sets up a listener on any specific tag
   * @param {String}   tag - The name of the event
   * @param {Function} cb  - The callback function
   */
  this.on = function (tag, cb) {
    if (typeof tag !== 'string' || typeof cb !== 'function') {
      return
    }

    if (!self.evts[tag]) {
      self.evts[tag] = cb
    } else {
      self.evts[tag] = cb
    }
  }

  /**
   * Trigger all Listening Objects Callbacks
   * @param {String} - The tag being triggered
   * @param {Object} - The event that occured (if available)
   * @param {Object} - The custom event data
   */
  this.trigger = function (tag, evt, data) {
    if (this.evts[tag]) {
      this.evts[tag](evt, data)
    }
  }

  /**
   * Returns whether the object exists in the dom.
   * @memberof Template
   *
   * @return {Boolean} parentExists?
   */
  this.exists = function () {
    return !!(document.getElementById(this._moduleID))
  }

  /**
   * Shows the module
   * @memberof Template
   */
  this.show = function () {
    if (self.exists()) {
      document.getElementById(this._moduleID).style.display = 'inherit'
      // set footer to the lowest dom position
      // document.body.appendChild(document.querySelector('.footer'))
    }
  }

  /**
   * Hides the module
   * @memberof Template
   */
  this.hide = function () {
    if (self.exists()) {
      document.getElementById(this._moduleID).style.display = 'none'
    }
  }

  /**
   * Responsible for building the complete module. Should also have buildPage implemented
   * by the specific module.
   */
  this.build = function () {
    log('Building Module ...')

    var templateContainer = Forge.build({'dom': 'div', 'id': this._moduleID, 'class': 'module'})
    var moduleContainer = self.buildPage()

    if (self.settings.scroll) {
      templateContainer.className += ' scroll'
    }

    if (self.settings.contacts) {
      templateContainer.className += ' scroll-contacts'
    }

    templateContainer.setAttribute('style', '' + 'display:' + self.settings.display)
    if (self.settings.height) {
      templateContainer.setAttribute('style', '' + 'height:' + self.settings.height)
    }
    if (moduleContainer !== null) {
      templateContainer.appendChild(moduleContainer)
      if (self.settings.parentModule !== '') {
        if (document.getElementById(self.settings.parentModule)) {
          document.getElementById(self.settings.parentModule).appendChild(templateContainer)
        }
      } else {
        document.body.appendChild(templateContainer)
      }
    }
  }

  /**
   * Interface that needs to be implemented by a Class inheriting Template Object.
   * @returns {Object} dom - The dom structure built for the page.
   */
  this.buildPage = function () {
    return null
  }

  return this
}

module.exports = ViewTemplate
