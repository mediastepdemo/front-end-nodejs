'use strict'

import ViewTemplate from '../ViewTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import styles from './requests.css'

import Component_Requests_Navigation from '../../components/requests/requests_navigation'
import Component_Requests_Details from '../../components/requests/requests_details'

var Requests = function (ctrl, settings) {
  ViewTemplate.call(this, ctrl, settings)

  this.name = 'Requests'
  var c = ctrl
  var self = this

  var REQUEST_SCHEMA = {
    engagementID: '',
    category: '',
    name: '',
    description: '',
    dueDate: null,
    items: [],
    conversationID: null,
    parent: null
  }

  var currentCategories = []
  var currentRequests = []
  var currentRequestID = null

  var components = {
    navigation: {obj: null, title: 'NAVIGATION', displayID: 'requests-navigation-container'},
    details: {obj: null, title: 'DETAILS', displayID: 'requests-details-container'}
  }

  /**
   * Initialize the Request Module
   */
  var initialize = function () {
    log(self.name + ': Initializing ...')

    self.build()
    registerEvents()
  }

  /**
   * Registers for Events with the Main Application Controller.
   */
  var registerEvents = function () {
    log(self.name + ': Registering Event Listeners ...')
    if (!c) {
      return
    }

    c.registerForEvent('add-request', 'ADD-REQ-RES-01', handle_addNewRequest)
    c.registerForEvent('update-request', 'UPDATE-REQ-RES-01', handle_updateRequest)
    c.registerForEvent('delete-file', 'UPDATE-REQ-RES-01', handle_deleteFile)
    c.registerForEvent('update-file', 'UPDATE-FILE-RES-01', handle_updateFile)
    c.registerForEvent('add-file', 'ADD-FILE-RES-01', handle_addFile)
  }

  /**
   * Handles the response to registration on update requests.
   */
  var handle_updateRequest = function (tagName, data) {
    log(self.name + ': handle_updateRequest')

    if (data && data.result && data.payload) {
      var requestID = data.payload.requestID.toString()
      var result = data.result
      if (result.name) {
        components.navigation.obj.updateRequestName(requestID, result.name)
      }
      if (currentRequestID === requestID) {
        components.details.obj.updateData(result)
      }
      if (result.status === 'INACTIVE') {
        c.sendEvent({name: 'load-engagement', data: {engagementID: data.payload.engagementID}})
      }
      if (data.payload.parent || data.payload.category) {
        components.navigation.obj.updateRequestParent(requestID, data.payload.uniqueID, data.payload.parentLvl)
      }
    }
  }

  /**
   * Handles the response to adding a new request.
   */
  var handle_addNewRequest = function (tagName, data) {
    log(self.name + ': handle_addNewRequest.')
    currentRequests.push(data.result)
    components.navigation.obj.addRequest(data)
    components.navigation.obj.updateRequestStats(currentRequests)
  }

  /**
   * Handles the response to adding a new file into the database
   */
  var handle_updateFile = function (tagName, data) {
    log(self.name + ': handle Update File')
    var requestID = data.result.newRequestID
    var previousRequestID = data.result.previousRequestID
    components.navigation.obj.updateRequestStats(currentRequests)
    if (requestID && requestID.toString() === currentRequestID.toString() || previousRequestID && previousRequestID.toString() === currentRequestID.toString()) {
      for (var i = 0; i < currentRequests.length; i++) {
        if (currentRequests[i]._id.toString() === currentRequestID.toString()) {
          return components.details.obj.loadRequest(currentRequests[i])
        }
      }
    }
  }

  /**
   * Handles the response to add a specific file from a request
   */
  var handle_addFile = function (tagName, data) {
    log(self.name + ': handel_addFile')
    if (data.result.file) {
      components.navigation.obj.updateRequestStats(currentRequests)
    }
  }

  /**
   * Handles the response to delete a specific file from a request.
   */
  var handle_deleteFile = function (tagName, data) {
    log(self.name + ': handle_deleteFile')
    if (data && data.result && data.result.fileID) {
      components.details.obj.deleteFile(data.result.fileID, data.payload.uniqueID)
      components.navigation.obj.updateRequestStats(currentRequests)
      return
    }
  }

  /**
   * Loadss the Requests and Files into the Request Module.
   * @param {Array}  categories - A string array of category names
   * @param {Array}  requests   - The requests that are in the engagement
   * @param {Array}  files      - The array of files to be mapped to requests
   */
  this.loadData = function (categories, requests, files) {
    log(self.name + ': Loading Data ...')
    if (!components.navigation.obj) {
      return
    }
    if (!components.details.obj) {
      return
    }

    REQUEST_SCHEMA.engagementID = c.currentEngagement._id

    // Load Request Navigation
    currentCategories = categories
    currentRequests = requests
    currentRequestID = null

    components.navigation.obj.loadData(c.currentEngagement._id, currentCategories, currentRequests, c.currentUser.type)

    var firstRequest = components.navigation.obj.getFirstRequest()
    if (firstRequest) {
      components.details.obj.loadRequest(firstRequest, null)
      currentRequestID = firstRequest._id
    } else {
      components.details.obj.loadRequest(null, null)
    }
  }

  this.loadRequest = function (requestID) {
    let reqObj
    for (let i = 0; i < currentRequests.length; i++) {
      if (currentRequests[i]._id === requestID) {
        reqObj = currentRequests[i]
      }
    }
    if (reqObj) {
      components.details.obj.loadRequest(reqObj, currentRequests)
    } else {
      ctrl.displayMessage('Unable to load request data. Please try agian.')
    }
  }

  /**
   * Builds the DOM page for the request modules.
   */
  this.buildPage = function () {
    log(self.name + ': Building Page')

    var parentContainer
    if (c.currentUser.type === 'AUDITOR') {
      parentContainer = Forge.build({dom: 'div', id: 'requests-container', 'class': 'm-req-page'})
    } else {
      parentContainer = Forge.build({ dom: 'div', id: 'requests-container', 'class': 'm-req-page' })
    }

    var reqNavContainer = Forge.build({ 'dom': 'div', 'id': components.navigation.displayID, 'class': 'm-req-navigation m-req-component' })
    var reqDetailsContainer = Forge.build({ 'dom': 'div', 'id': components.details.displayID, 'class': 'm-req-details m-req-component' })
    parentContainer.appendChild(reqNavContainer)
    parentContainer.appendChild(reqDetailsContainer)

    components.navigation.obj = new Component_Requests_Navigation(reqNavContainer, ctrl)
    components.details.obj = new Component_Requests_Details(reqDetailsContainer, ctrl)

    /* Request Nav Events */
    components.navigation.obj.on('flash', function (data) {
      self.trigger('flash', data)
    })

    components.navigation.obj.on('add-request', function (data) {
      log(self.name + ': Create new Request.')
      let json = {...REQUEST_SCHEMA, ...data}

      let keys = Object.keys(data)
      for (var key in keys) {
        json[keys[key]] = data[keys[key]]
      }

      c.sendEvent({name: 'add-request', data: json})
    })

    components.navigation.obj.on('update-request', function (newRequest) {
      log(self.name + ': Updating Request.')
      c.sendEvent({name: 'update-request', data: newRequest})
    })

    components.navigation.obj.on('add-category', function (name) {
      log(self.name + ': Create new Category.')
      var newCategory = {
        engagementID: c.currentEngagement._id,
        categories: name
      }
      c.sendEvent({name: 'add-category', data: newCategory})
    })

    components.navigation.obj.on('update-category', function (updatedCategoryName) {
      log(self.name + ': Updating Category.')
      var updateCategory = {
        engagementID: c.currentEngagement._id,
        categories: updatedCategoryName
      }
      c.sendEvent({name: 'update-category', data: updateCategory})
    })

    components.navigation.obj.on('view-request', function (requestID) {
      log(self.name + ': Clicked on Request (' + requestID + ')')
      var foundRequests = currentRequests.filter(function (request) {
        return request._id === requestID
      })

      if (foundRequests.length !== 1) {
        return
      }
      components.details.obj.loadRequest(foundRequests[0], currentRequests)
      currentRequestID = foundRequests[0]._id
    })

    /* Request Detail Events */
    components.details.obj.on('flash', function (data) {
      self.trigger('flash', data)
    })

    components.details.obj.on('create-activity', function (data) {
      c.createActivity(data)
    })

    components.details.obj.on('add-request', function (data) {
      log(self.name + ': Create new Request.')
      let json = {...REQUEST_SCHEMA, ...data}

      let keys = Object.keys(data)
      for (var key in keys) {
        json[keys[key]] = data[keys[key]]
      }
      c.sendEvent({name: 'add-request', data: json})
    })

    components.details.obj.on('update-request', function (data) {
      log(self.name + ': Updating Request.')
      c.sendEvent({
        name: 'update-request',
        data: {
          ...data,
          requestID: currentRequestID,
          engagementID: c.currentEngagement._id
        }
      })
    })
    components.details.obj.on('delete-request-modal', function (data) {
      log(self.name + ': Opening Delete Request Modal.')
      c.displayDeleteRequestModal(data)
    })

    components.details.obj.on('update-global-dueDate', function (data) {
      currentRequests.forEach((r) => {
        c.sendEvent({
          name: 'update-request',
          data: {
            ...data,
            requestID: r._id,
            engagementID: c.currentEngagement._id
          }
        })
      })
    })

    components.details.obj.on('delete-request-file-modal', function (data) {
      c.displayDeleteRequestFileModal(data)
    })

    components.details.obj.on('upload-file', function (data) {
      c.uploadFile(data)
    })

    components.details.obj.on('delete-file', function (data) {
      c.sendEvent({name: 'delete-file', data: data})
    })

    components.details.obj.on('cancel-upload', function (data) {
      c.cancelUpload(data)
    })

    return parentContainer
  }

  initialize()

  return this
}

Requests.prototype = Object.create(ViewTemplate.prototype)
Requests.prototype.constructor = Requests

module.exports = Requests
