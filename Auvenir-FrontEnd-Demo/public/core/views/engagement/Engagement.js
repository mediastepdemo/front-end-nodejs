'use strict'

import ViewTemplate from '../ViewTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import definePermission from '../../../js/src/definePermission'

import FileManager from '../file-manager/fileManager'
import Widget_ActivityFeed from '../../widgets/activity/Widget_ActivityFeed'
import Widget_Team from '../../widgets/team/Widget_Team'
// Main Dashboard Widgets
import ComponentEngagementOverview from '../../components/engagementOverview/engagementOverview'
import ComponentToDoList from '../../components/toDoList/toDoList'
import styles from './Engagement.css'
import _ from 'lodash'

// TODO - code cleanup after db model change
//      - There are some user type check (auditor or client) for viewing page (dashboard related.)
//      - Anything to do with dashboard will be gone. Keeping it for now for dev team to invite client.

function Engagement (ctrl, settings) {
  ViewTemplate.call(this, ctrl, settings)

  this.name = 'Engagement Module'

  var self = this
  var c = ctrl

  var currentPage = null

  var pages = {
    TODO: {pageID: 'engagement-todo', linkID: 'engagementTodoLink'},
    FILEMANAGER: {pageID: 'engagement-filemanager', linkID: 'engagementFileMangerLink'},
    ACTIVITY: {pageID: 'engagement-activity', linkID: 'engagementActivityLink'},
    TEAM: {pageID: 'engagement-team', linkID: 'engagementTeamLink'}
  }

  this.dashboardWidgets = []

  var engagementOverviewComponent = null
  var toDoComponent = null
  var teamWidget = null
  var activityWidget = null
  var fileManagerModule = null

  /**
   * Initializes the Engagement Dashboard and any relevant functionality
   */
  var initialize = function () {
    log('Client Dashboard: Initializing ...')
    self.build()
    registerEvents()

    // if (c.currentUser.type === 'CLIENT') {
    viewPage('TODO')
    // }
  }

  var viewPage = this.viewPage = function (name) {
    log('View Page: ' + name)
    var page = null
    if (typeof name === 'string') {
      page = pages[name]
    } else if (typeof name === 'object') {
      if (name instanceof MouseEvent) {
        var id = name.target.id

        var pageKeys = Object.keys(pages)
        for (var i = 0; i < pageKeys.length; i++) {
          if (pages[pageKeys[i]].linkID === id) {
            page = pages[pageKeys[i]]
          }
        }
      }
    }

    if (!page) return

    if (currentPage !== null) {
      document.getElementById(currentPage.linkID).classList.toggle('active')
      document.getElementById(currentPage.pageID).style.display = 'none'
      document.getElementById(currentPage.linkID).addEventListener('click', viewPage)
    }

    currentPage = page

    document.getElementById(currentPage.linkID).classList.toggle('active')
    document.getElementById(currentPage.pageID).style.display = 'inherit'
    document.getElementById(currentPage.linkID).removeEventListener('click', viewPage)

    window.scrollTo(0, 0)
  }

  /**
   * Sets up the engagements listeners
   */
  this.setupListeners = function () {
    log('Dashboard: Setting up listeners ...')
  }

  /**
   * Registers all Engagement Dashboard events
   */
  var registerEvents = function () {
    document.getElementById('engagement-backButton').addEventListener('click', () => {
      c.goToPreviousEngagementPage()
      toDoComponent.closeDetailPage()
    })
    document.getElementById('engagementActivityLink').addEventListener('click', viewPageActivity)
    document.getElementById('engagementTeamLink').addEventListener('click', viewPageTeam)
    document.getElementById('engagementFileMangerLink').addEventListener('click', viewPageFileManager)
    document.getElementById('engagementTodoLink').addEventListener('click', viewPageTodo)
  }

  /**
   * viewPage fix
   **/
  var viewPageActivity = function () {
    viewPage('ACTIVITY')
    activityWidget.setBorder()
    toDoComponent.closeDetailPage()
  }
  var viewPageTeam = function () {
    viewPage('TEAM')
    toDoComponent.closeDetailPage()
  }
  var viewPageFileManager = function () {
    fileManagerModule.loadData(c.currentEngagement, c.currentUser.type)
    viewPage('FILEMANAGER')
    toDoComponent.closeDetailPage()
  }
  var viewPageTodo = function () {
    toDoComponent.clearSearchAndFilter()
    toDoComponent.loadData(c.currentTodos)
    viewPage('TODO')
  }

  /**
   * Load one engagement in as the current engagement
   */
  this.loadDashboard = function () {
    if (c.currentUser.type === 'AUDITOR' || c.currentUser.type === 'CLIENT') {
      document.getElementById('engagementTodoLink').style.display = 'inline-block'
      document.getElementById('engagementFileMangerLink').style.display = 'inline-block'
      document.getElementById('engagementTeamLink').style.display = 'inline-block'
      document.getElementById('engagementActivityLink').style.display = 'inline-block'
      document.getElementById(self._moduleID).classList.add('scroll-engagement')

      activityWidget = new Widget_ActivityFeed(c, this)
      teamWidget = new Widget_Team(c, this)

      this.addWidget(pages.ACTIVITY.pageID, activityWidget.buildWidget({
        top: '0px',
        left: '0px',
        width: '100%',
        height: '800px',
        background: '#fff',
        border: '0px solid red',
        draggable: false,
        resizable: false,
        margin: '0px auto 15px'
      }))
      this.addWidget(pages.TEAM.pageID, teamWidget.buildWidget({
        top: '0px',
        left: '0px',
        width: '100%',
        height: '100%',
        background: '#fff',
        border: '0px solid red',
        draggable: false,
        resizable: false,
        margin: '0px auto 15px'
      }))
      activityWidget.applySettings()

      teamWidget.applySettings()
      activityWidget.applySettings()

      this.dashboardWidgets.push(activityWidget)
      this.dashboardWidgets.push(teamWidget)

      activityWidget.on('add-activity', (activity) => {
        engagementOverviewComponent.updateActivity(activity)
      })

      engagementOverviewComponent = new ComponentEngagementOverview(c, this)
      this.addWidget(pages.TODO.pageID, engagementOverviewComponent.buildContent())
      this.dashboardWidgets.push(engagementOverviewComponent)

      toDoComponent = new ComponentToDoList(c, document.getElementById(pages.TODO.pageID))
      this.addWidget(pages.TODO.pageID, toDoComponent.buildContent())
      this.dashboardWidgets.push(toDoComponent)
      toDoComponent.on('flash', (option) => {
        self.trigger('flash', option)
      })
      toDoComponent.on('upload-file', (data) => {
        c.uploadFile(data)
      })

      fileManagerModule = new FileManager(c, {parentModule: pages.FILEMANAGER.pageID, display: 'inherit', scroll: false})
      fileManagerModule.on('flash', (option) => {
        self.trigger('flash', option)
      })
    } else {
      c.displayMessage('You have no access to the Engagement page.')
    }
  }

  this.updateOverview = () => {
    engagementOverviewComponent.updateOverview(c.currentEngagement)
  }
  this.updateOverviewTeam = () => {
    engagementOverviewComponent.loadData()
  }

  this.refresh = function () {
    log('Engagement: Refreshing widget content')

    self.updateOverview()
    document.getElementById('engagementUserBtn').style.display = 'none'

    if (c.currentUser.type === 'AUDITOR' || c.currentUser.type === 'CLIENT') {
      if (c.currentUser.type === 'AUDITOR') {
        for (var i = 0; i < c.currentEngagement.acl.length; i++) {
          self.clientInvited(c.currentEngagement.acl[i])
        }
      }
      engagementOverviewComponent.loadData()
      activityWidget.loadData(c.currentActivities)
      fileManagerModule.loadData(c.currentEngagement, c.currentUser.type)
    }

    if (c.currentEngagement !== null) {
      let engagement = c.currentEngagement
      if (engagement.name === 'Untitled') {
        document.getElementById('a-header-title').value = ''
        document.getElementById('a-header-title').placeholder = 'Name your engagement'
        document.getElementById('a-header-title').classList.add('unt-text-edit')
        document.getElementById('a-header-pencil').style.opacity = '1'
        document.getElementById('a-header-title').focus()
      } else {
        var engagementName = engagement.name
        document.getElementById('a-header-title').classList.remove('unt-text-edit')
        document.getElementById('a-header-pencil').style.opacity = '0'
        document.getElementById('a-header-title').value = engagementName
      }

      let hasIncomplete = _.every(engagement.todos, {
        completed: false,
        status: 'ACTIVE'
      })
      // TODO - hiding the archive button for R2
      // if (!hasIncomplete || c.currentEngagement.status === 'ARCHIVED') {
      //   var archiveText = c.currentEngagement.status !== 'ARCHIVED' ? 'Archive Engagement' : 'Unarchive Engagement'
      //   var archiveButton = Forge.build({'dom': 'button', 'id': 'engagement-archive-button', 'class': 'auvbtn primary', 'text': archiveText})
      //   archiveButton.addEventListener('click', () => c.displayArchiveEngagementModal(c.currentEngagement.status === 'ARCHIVED'))

      //   if (document.getElementById('engagement-archive-button')) {
      //     document.getElementById('engagement-archive-button').innerText = archiveText
      //   } else {
      //     document.getElementById('pageHeader-buttons').appendChild(archiveButton)
      //   }
      // } else if (document.getElementById('engagement-archive-button')) {
      //   document.getElementById('engagement-archive-button').remove()
      // }
    }
  }

  this.clientInvited = (user) => {
    let userBtn = document.getElementById('engagementUserBtn')
    // only the lead auditor sees the invite client button
    if (!c.permission.isPermissionOnEngagement(definePermission.EngagementPermissions.INVITE_CLIENT)) {
      return
    }
    if (user.role === 'CLIENT') {
      if (user.status === 'INVITED') {
        userBtn.removeEventListener('click', c.displayInviteClientModal)
        userBtn.addEventListener('click', () => {
          c.sendEngagementInvite(c.currentEngagement._id, user.userInfo.email)
        })
        userBtn.innerText = 'Resend Invite'
        userBtn.style.display = ''
        return
      } else if (user.status === 'ACTIVE') {
        userBtn.style.display = 'none'
        return
      }
    }
    userBtn.addEventListener('click', c.displayInviteClientModal)
    userBtn.innerText = 'Invite Client'
    userBtn.style.display = ''
  }

  this.addWidget = function (p, myWidget) {
    var parentDom = document.getElementById(p)
    parentDom.appendChild(myWidget)
  }

  this.showHeader = function () {
    document.getElementById('engagementHeader').style.display = 'inherit'
    document.getElementById('dashboardLinks').style.display = 'inherit'
  }

  this.addTeamList = function () {
    teamWidget.addTeamList()
  }

  this.loadNewMember = function (member) {
    teamWidget.loadNewMember(member)
  }

  this.loadContacts = function () {
    teamWidget.loadContacts()
  }

  this.removeTeamMember = function (member) {
    teamWidget.removeTeamMember(member)
  }

  this.hideToDoComponents = function () {
    if (toDoComponent) {
      toDoComponent.closeDetailPage()
    }
  }

  this.buildPage = function () {
    log('Engagement: Building Page')

    var parentContainer = Forge.build({'dom': 'div', 'id': 'engagementPage', 'class': 'ui-helper-clearfix parent-of-footer'})

    /** Dashboards **/
    var todoDashboard = Forge.build({'dom': 'div', 'id': pages.TODO.pageID, 'class': 'dashboard'})
    var fileManagerDashboard = Forge.build({ 'dom': 'div', 'id': pages.FILEMANAGER.pageID, 'class': 'dashboard', 'style': 'display:none;' })
    var activityDashboard = Forge.build({'dom': 'div', 'id': pages.ACTIVITY.pageID, 'class': 'dashboard'})
    var teamDashboard = Forge.build({'dom': 'div', 'id': pages.TEAM.pageID, 'class': 'dashboard'})

    /** NAV **/
    log('Building Engagment Dashboard Links')

    var pageHeader = Forge.build({'dom': 'div', 'class': 'pageHeader noSelect', 'id': 'engagementHeader'})
    var pageHeadContent = Forge.build({'dom': 'div', 'class': 'pageHeader-content'})
    var pageHeadBack = Forge.build({'dom': 'div', 'class': 'pageHeader-leftContainer'})
    var pageHeadFA = Forge.build({ 'dom': 'i', 'id': 'engagement-backButton', 'class': 'fa fa-angle-left pageHeader-backButton' })
    var pageHeadTextTitle = Forge.build({
      'dom': 'input',
      'type': 'text',
      'class': 'unt-text',
      'placeholder': 'Name your engagement',
      'id': 'a-header-title',
      'style': 'cursor: text; border: none; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; min-width: 340px; outline: none;'
    })
    var pageHeadTextEdit = Forge.build({'dom': 'img', 'id': 'a-header-pencil', 'class': 'unt-edit', 'src': 'images/icons/pencil.png'})
    var pageHeadButtons = Forge.build({'dom': 'div', 'id': 'pageHeader-buttons', 'class': 'pageHeader-buttons'})

    $(pageHeadTextTitle).focus(function (event) {
      document.getElementById('a-header-title').classList.add('unt-text-edit')
      document.getElementById('a-header-pencil').style.opacity = '1'
    })
    $(pageHeadTextTitle).focusout(function (event) {
      var name = event.target.value.trim()
      if (name === '') {
        document.getElementById('a-header-title').style.border = 'none'
        document.getElementById('a-header-title').style.borderRadius = null
      } else {
        c.updateEngagement({engagementID: c.currentEngagement._id, name})
        // document.getElementById('auditTitle-' + c.currentEngagement._id).innerText = name
        document.getElementById('a-header-title').style.border = 'none'
        document.getElementById('a-header-title').style.borderRadius = null
        document.getElementById('a-header-title').classList.remove('unt-text-edit')
        document.getElementById('a-header-pencil').style.opacity = '0'
        event.target.placeholder = name
        event.target.value = name
      }
    })

    parentContainer.appendChild(pageHeader)
    pageHeader.appendChild(pageHeadContent)
    pageHeadContent.appendChild(pageHeadBack)
    pageHeadContent.appendChild(pageHeadButtons)
    pageHeadBack.appendChild(pageHeadFA)
    pageHeadBack.appendChild(pageHeadTextTitle)
    pageHeadBack.appendChild(pageHeadTextEdit)

    /* Invite Client Button */
    var inviteClientBtn = Forge.buildBtn({text: 'Undefined', id: 'engagementUserBtn', type: 'primary', width: '', margin: '6px 0 0 18px', float: '', buttonEvent: []}).obj
    pageHeadBack.appendChild(inviteClientBtn)

    var dashboardLinksDiv = Forge.build({'dom': 'nav', 'id': 'dashboardLinks', 'class': 'dashboard-pageNav-container'})
    parentContainer.appendChild(dashboardLinksDiv)

    /** Todo Nav Link **/
    var todoLink = Forge.build({'dom': 'div', 'id': 'engagementTodoLink', 'class': 'dashboard-pageNav'})
    var todoLinkFA = Forge.build({'dom': 'div', 'class': 'auvicon-line-requests', 'style': 'display: inline'})
    var todoLinkText = document.createTextNode(' To-Dos')
    dashboardLinksDiv.appendChild(todoLink)
    todoLink.appendChild(todoLinkFA)
    todoLink.appendChild(todoLinkText)

    /** Docs Nav Link **/
    var docLink = Forge.build({'dom': 'div', 'id': 'engagementFileMangerLink', 'class': 'dashboard-pageNav'})
    var docLinkFA = Forge.build({ 'dom': 'span', 'class': 'auvicon-line-folder engagement-icon', 'style': 'font-size: 16px' })
    var docLinkText = document.createTextNode(' Files')
    dashboardLinksDiv.appendChild(docLink)
    docLink.appendChild(docLinkFA)
    docLink.appendChild(docLinkText)

    /** Activity Nav Link **/
    var activityLink = Forge.build({'dom': 'div', 'id': 'engagementActivityLink', 'class': 'dashboard-pageNav'})
    var activityLinkFA = Forge.build({'dom': 'span', 'class': 'auvicon-line-activity engagement-icon', 'style': '20px'})
    var activityLinkText = document.createTextNode(' Activity')
    dashboardLinksDiv.appendChild(activityLink)
    activityLink.appendChild(activityLinkFA)
    activityLink.appendChild(activityLinkText)

    /** Team Nav Link **/
    var teamLink = Forge.build({'dom': 'div', 'id': 'engagementTeamLink', 'class': 'dashboard-pageNav'})
    var teamLinkFA = Forge.build({
      'dom': 'span',
      'class': 'auvicon-team engagement-icon',
      'style': 'font-size: 24px; vertical-align: text-bottom;'
    })
    var teamLinkText = document.createTextNode(' Team')
    dashboardLinksDiv.appendChild(teamLink)
    teamLink.appendChild(teamLinkFA)
    teamLink.appendChild(teamLinkText)

    parentContainer.appendChild(todoDashboard)
    parentContainer.appendChild(fileManagerDashboard)
    parentContainer.appendChild(activityDashboard)
    parentContainer.appendChild(teamDashboard)

    var footerComponent = Forge.build({dom: 'div', id: 'engagement-footer'})
    footerComponent.innerHTML = c.footer
    parentContainer.appendChild(footerComponent)

    return parentContainer
  }

  this.setBorder = function () {
    activityWidget.setBorder()
  }

  initialize()

  return this
}

Engagement.prototype = Object.create(ViewTemplate.prototype)
Engagement.prototype.constructor = Engagement

module.exports = Engagement
