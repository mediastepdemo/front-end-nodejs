'use strict'

import ViewTemplate from '../ViewTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import Utility from '../../../js/src/utility_c'

import FileManager from '../file-manager/fileManager'
import Requests from '../requests/Requests'

import Widget_ActivityFeed from '../../widgets/activity/Widget_ActivityFeed'
import Widget_Team from '../../widgets/team/Widget_Team'
import Widget_TaskManager from '../../widgets/task-manager/Widget_TaskManager'
import Widget_MyClient from '../../widgets/my-client/Widget_MyClient'
// Main Dashboard Widgets
import Widget_MyAuditor from '../../widgets/my-auditor/Widget_MyAuditor'
import Widget_CallToAction from '../../widgets/call-to-action/Widget_CallToAction'
import styles from './Engagement.css'

function Engagement (ctrl, settings) {
  ViewTemplate.call(this, ctrl, settings)

  this.name = 'Engagement Module'

  var self = this
  var c = ctrl

  var MAX_COLUMNS = 4
  var widgetContainer = 'engagement-main'
  var filemanagerContainer = 'engagement-filemanager'
  // var requestContainer = 'engagement-requests' @DELETE
  var teamContainer = 'engagement-team'
  var insightsContainer = 'engagement-insights'
  // var analysisContainer = 'engagement-analysis' @DELETE
  var clientDataContainer = 'engagement-clientData'
  var activityContainer = 'engagement-activity'

  var currentPage = null
  var pages = {
    DASHBOARD: {pageID: 'engagement-main', linkID: 'engagementDashboardLink'},
    REQUESTS: {pageID: 'engagement-requests', linkID: 'engagementRequestsLink'},
    ACTIVITY: {pageID: 'engagement-activity', linkID: 'engagementActivityLink'},
    FILEMANAGER: {pageID: 'engagement-filemanager', linkID: 'engagementFileMangerLink'}
  }

  this.dashboardWidgets = []

  var myAuditorWidget = null
  var callToAction = null
  var fileManagerWidget = null
  var activityFeedWidget = null
  var teamWidget = null
  var taskManagerWidget = null
  var auvenirRepWidget = null
  var newsFeedWidget = null
  var analysisWidget = null
  var myClientWidget = null
  var myScheduleTemplatesWidget = null
  var myClientDataWidget = null
  var myAuditOpinionWidget = null
  var requestsModule = null
  var activityWidget = null
  var fileManagerModule = null

  /**
   * Initializes the Engagement Dashboard and any relevant functionality
   */
  var initialize = function () {
    log('Client Dashboard: Initializing ...')
    self.build()
    registerEvents()

    viewPage('DASHBOARD')
  }

  var viewPage = this.viewPage = function (name) {
    log('View Page: ' + name)

    var page = null
    if (typeof name === 'string') {
      page = pages[name]
    } else if (typeof name === 'object') {
      if (name instanceof MouseEvent) {
        var id = name.target.id

        var pageKeys = Object.keys(pages)
        for (var i = 0; i < pageKeys.length; i++) {
          if (pages[pageKeys[i]].linkID === id) {
            page = pages[pageKeys[i]]
          }
        }
      }
    }

    if (!page) return

    if (currentPage !== null) {
      document.getElementById(currentPage.linkID).classList.toggle('active')
      document.getElementById(currentPage.pageID).style.display = 'none'
      document.getElementById(currentPage.linkID).addEventListener('click', viewPage)
    }

    currentPage = page

    document.getElementById(currentPage.linkID).classList.toggle('active')
    document.getElementById(currentPage.pageID).style.display = 'inherit'
    document.getElementById(currentPage.linkID).removeEventListener('click', viewPage)

    window.scrollTo(0, 0)
  }

  /**
   * Response for Get Auditor List
   *
   * @param: {string} tagName , the name of Socket Event.
   * @param: {json} data, result of socket Event.
   */
  var responseGetAuditorList = function (tagName, data) {
    log('> Event: get-auditor-list-response')
    log(data)

    document.getElementById('progressOverlay').style.display = 'none'

    if (data.code !== 0) {
      c.displayMessage(data.msg)
    } else {
      myAuditorWidget.displayAuditors(data.result)
    }
  }

  /**
   * Response for Add Auditor from Socket.
   *
   * @param: {string} tagName , the name of Socket Event.
   * @param: {json} data, result of socket Event.
   */
  var responseAddAuditor = function (tagName, data) {
    log('> Event: get-auditor-list-response')
    log(data)

    document.getElementById('progressOverlay').style.display = 'none'
    if (data.code !== 0) {
      c.displayMessage(data.msg)
    } else {
      myAuditorWidget.setAuditor(data.result)
    }
  }

  /**
   * Response for result of Notify Client from Socket.
   *
   * @param: {string} tagName , the name of Socket Event.
   * @param: {json} data, result of socket Event.
   */
  var responseNotifyClient = function (tagName, data) {
    log('> Event: notify-client-response')
    log(data)
    document.getElementById('progressOverlay').style.display = 'none'

    if (data.code !== 0) {
      c.displayMessage(data.msg)
    } else {
      c.displayMessage('SENT')
    }
  }

  /**
   * Sets up the engagements listeners
   */
  this.setupListeners = function () {
    log('Dashboard: Setting up listeners ...')

    c.registerForEvent('get-auditor-list', 'ENG-GETAUDLIST-000', responseGetAuditorList)
    c.registerForEvent('add-auditor', 'ENG-ADDAUDITOR-000', responseAddAuditor)
    // c.registerForEvent('complete-audit', 'ENG-COMPLETEAUDIT-000', responseCompleteAudit)
    c.registerForEvent('notify-client', 'ENG-NOTIFYCLIENT-000', responseNotifyClient)
  }

  /**
   * Registers all Engagement Dashboard events
   */
  var registerEvents = function () {
    log('Client Dashboard: Registering Event Listeners ...')

    document.getElementById('goToWizardBtn').addEventListener('click', goToWizard)
    document.getElementById('engagement-backButton').addEventListener('click', c.goToPreviousPage)

    document.getElementById('engagementDashboardLink').addEventListener('click', viewPageDashboard)
    document.getElementById('engagementRequestsLink').addEventListener('click', viewPageRequests)
    document.getElementById('engagementActivityLink').addEventListener('click', viewPageActivity)
    document.getElementById('engagementTeamLink').addEventListener('click', viewPageTeam)
    document.getElementById('engagementFileMangerLink').addEventListener('click', viewPageFileManager)
    document.getElementById('engagementAnalysisLink').addEventListener('click', viewPageAnalysis)
  }

  /**
   * viewPage fix
   **/
  var viewPageDashboard = function () {
    viewPage('DASHBOARD')
  }
  var viewPageRequests = function () {
    viewPage('REQUESTS')
  }
  var viewPageActivity = function () {
    viewPage('ACTIVITY')
    activityWidget.setBorder()
  }
  var viewPageTeam = function () {
    viewPage('TEAM')
  }
  var viewPageFileManager = function () {
    viewPage('FILEMANAGER')
  }
  var viewPageAnalysis = function () {
    viewPage('ANALYSIS')
  }

  /**
   * Tears down all Engagement Dashboard events
   */
  var teardownEvents = function () {
    log('Client Dashboard: Removing Event Listeners ...')
  }

  /**
   * Continues the Active Audit and transitions to the Audit Dashboard
   */
  var goToWizard = function () {
    log('Client Dashboard Event: Continue Audit')

    if (c.currentEngagement !== null) {
      self.trigger('go-to-wizard')
      if (c.currentEngagement.auditStarted === null) {
        document.getElementById('goToWizardBtn').innerHTML = 'Begin'
        taskManagerWidget.displayTasks()
      } else {
        document.getElementById('goToWizardBtn').innerHTML = 'Go to Audit File'
      }
    }
  }

  this.requestAuditorList = function () {
    log('Engagement Dashboard: Request for Auditor List')

    document.getElementById('progressOverlay').style.display = 'inherit'
    c.sendEvent({name: 'get-auditor-list', data: {}})
  }

  /**
   * Load one engagement in as the current engagement
   */
  this.loadDashboard = function () {
    // AUDITOR VIEW
    if (c.currentUser.type === 'AUDITOR') {
      document.getElementById('goToWizardBtn').style.display = 'none'
      document.getElementById('engagementDashboardLink').style.display = 'inline-block'
      document.getElementById('engagementRequestsLink').style.display = 'inline-block'
      document.getElementById('engagementActivityLink').style.display = 'inline-block'
      document.getElementById('engagementFileMangerLink').style.display = 'inline-block'
      document.getElementById('engagementTeamLink').style.display = 'none'
      document.getElementById('engagementAnalysisLink').style.display = 'none'
      document.getElementById('engagementClientDataLink').style.display = 'none'
      document.getElementById(self._moduleID).classList.add('scroll-190')

      activityWidget = new Widget_ActivityFeed(c, this)
      teamWidget = new Widget_Team(c, this)

      this.addWidget(activityContainer, activityWidget.buildWidget({
        top: '0px',
        left: '0px',
        width: '100%',
        height: '800px',
        background: '#fff',
        border: '0px solid red',
        draggable: false,
        resizable: false
      }))
      this.addWidget(teamContainer, teamWidget.buildWidget({
        top: '0px',
        left: '0px',
        width: '100%',
        height: '100%',
        background: '#fff',
        border: '0px solid red',
        draggable: false,
        resizable: false
      }))

      activityWidget.applySettings()
      teamWidget.applySettings()

      this.dashboardWidgets.push(activityWidget)
      this.dashboardWidgets.push(teamWidget)

      taskManagerWidget = new Widget_TaskManager(c, this)
      this.addWidget(widgetContainer, taskManagerWidget.buildWidget({
        top: '0px',
        right: '0px',
        width: '840px',
        height: '630px',
        background: '#fff',
        border: '0px solid red',
        float: 'right',
        draggable: false,
        resizable: false
      }))
      taskManagerWidget.applySettings()
      taskManagerWidget.setupListeners()
      this.dashboardWidgets.push(taskManagerWidget)

      myClientWidget = new Widget_MyClient(c, this)
      this.addWidget(widgetContainer, myClientWidget.buildWidget({
        top: '0px',
        left: '0px',
        width: '264px',
        height: '300px',
        background: '#fff',
        border: '0px solid red',
        draggable: false,
        resizable: false
      }))
      myClientWidget.applySettings()
      this.dashboardWidgets.push(myClientWidget)

      // requestsModule = new Requests(c, {parentModule: requestContainer, display: 'inherit', scroll: false}) @DELETE
      // requestsModule.on('flash', function (option) {
      //   self.trigger('flash', option)
      // })
      fileManagerModule = new FileManager(c, {parentModule: filemanagerContainer, display: 'inherit', scroll: false})
      fileManagerModule.on('update-file', function (data) {
        document.getElementById('progressOverlay').style.display = 'inherit'
        self.trigger('update-file', data)
      })
      fileManagerModule.on('update-selected-files', function (data) {
        self.trigger('update-selected-files', data)
      })
      fileManagerModule.on('flash', function (option) {
        self.trigger('flash', option)
      })
      fileManagerModule.on('delete-selected-files', function (data) {
        self.trigger('delete-selected-files', data)
      })
    } else if (c.currentUser.type === 'CLIENT') {
      document.getElementById('engagementDashboardLink').style.display = 'inline-block'
      document.getElementById('engagementActivityLink').style.display = 'none'
      document.getElementById('engagementTeamLink').style.display = 'none'
      document.getElementById('engagementAnalysisLink').style.display = 'none'
      // document.getElementById('engagementRequestsLink').style.display = 'none' @DELETE
      document.getElementById('engagementClientDataLink').style.display = 'none'

      // Page Widget
      teamWidget = new Widget_Team(c, this)
      this.addWidget(teamContainer, teamWidget.buildWidget({
        top: '0px',
        left: '0px',
        width: '100%',
        height: '100%',
        background: '#fff',
        border: '0px solid red',
        draggable: false,
        resizable: false
      }))
      teamWidget.applySettings()
      this.dashboardWidgets.push(teamWidget)

      // Main Dashboard Widgets
      activityFeedWidget = new Widget_ActivityFeed(c, this)
      this.addWidget(widgetContainer, activityFeedWidget.buildWidget({
        width: '840px',
        height: '595px',
        background: '#fff',
        border: '0px solid red',
        draggable: false,
        resizable: false,
        overflow: false
      }))
      activityFeedWidget.applySettings()
      this.dashboardWidgets.push(activityFeedWidget)

      myAuditorWidget = new Widget_MyAuditor(c, this)
      this.addWidget(widgetContainer, myAuditorWidget.buildWidget({
        width: '264px',
        height: '300px',
        background: '#fff',
        border: '0px solid red',
        draggable: false,
        resizable: false
      }))
      myAuditorWidget.applySettings()
      this.dashboardWidgets.push(myAuditorWidget)

      callToAction = new Widget_CallToAction(c, this)
      this.addWidget(widgetContainer, callToAction.buildWidget({
        left: '0px',
        width: '100%',
        height: '142px',
        background: '#fff',
        border: '0px solid red',
        draggable: false,
        resizable: false
      }))
      callToAction.applySettings()
      this.dashboardWidgets.push(callToAction)
    } else {
      c.displayMessage('User should probably not be accessing the engagement dashboard')
    }
  }

  this.selectAuditor = function (auditor) {
    log('Engagement Dashboard: Request for Auditor List')

    document.getElementById('progressOverlay').style.display = 'inherit'
    c.sendEvent({name: 'add-auditor', data: {userID: auditor._id, engagementID: c.currentEngagement._id}})
  }

  this.loadRequest = function (reqID) {
    requestsModule.loadRequest(reqID)
    viewPage('REQUESTS')
  }

  this.refresh = function () {
    log('Engagement: Refreshing widget content')
    if (c.currentUser.type === 'AUDITOR') {
      viewPageDashboard()
      activityWidget.loadData(c.currentActivities)
      myClientWidget.refresh()
      taskManagerWidget.refresh()
      requestsModule.loadData(
        c.currentEngagement.categories,
        c.currentRequests,
        c.currentFiles
      )
      fileManagerModule.loadData(c.currentFiles, c.currentRequests, c.currentEngagement._id, c.currentUser.type)
    } else if (c.currentUser.type === 'CLIENT') {
      activityFeedWidget.loadData(c.currentActivities)
      callToAction.loadData()
      myAuditorWidget.loadAuditor()
    }

    if (c.currentEngagement !== null) {
      if (c.currentEngagement.name === 'Untitled') {
        document.getElementById('a-header-title').value = ''
        document.getElementById('a-header-title').placeholder = 'Name your engagement'
        document.getElementById('a-header-title').classList.add('unt-text-edit')
        document.getElementById('a-header-pencil').style.display = 'block'
        document.getElementById('a-header-title').focus()
      } else {
        var engagementName = c.currentEngagement.name
        document.getElementById('a-header-title').classList.remove('unt-text-edit')
        document.getElementById('a-header-pencil').style.display = 'none'
        document.getElementById('a-header-title').value = engagementName
      }
      if (c.currentEngagement.auditStarted === null) {
        document.getElementById('goToWizardBtn').innerHTML = 'Begin'
      } else {
        document.getElementById('goToWizardBtn').innerHTML = 'Go to Audit File'
      }
    }
  }

  this.addWidget = function (p, myWidget) {
    var parentDom = document.getElementById(p)
    parentDom.appendChild(myWidget)
  }

  this.showHeader = function () {
    document.getElementById('engagementHeader').style.display = 'inherit'
    document.getElementById('dashboardLinks').style.display = 'inherit'
  }

  this.buildPage = function () {
    log('Engagement: Building Page')

    var parentContainer = Forge.build({'dom': 'div', 'id': 'engagementPage', 'class': 'ui-helper-clearfix', 'style': 'min-height: 600px'})

    /** Dashboards **/
    var mainDashboard = Forge.build({'dom': 'div', 'id': 'engagement-main', 'class': 'dashboard ui-helper-clearfix'})
    var fileManagerDashboard = Forge.build({'dom': 'div', 'id': 'engagement-filemanager', 'style': 'display:none;'})
    var requestsDashboard = Forge.build({'dom': 'div', 'id': 'engagement-requests', 'class': 'dashboard'})
    var activityDashboard = Forge.build({'dom': 'div', 'id': 'engagement-activity', 'class': 'dashboard'})
    var teamDashboard = Forge.build({'dom': 'div', 'id': 'engagement-team', 'class': 'dashboard'})
    var analysisDashboard = Forge.build({'dom': 'div', 'id': 'engagement-analysis', 'class': 'dashboard'})
    var insightsDashboard = Forge.build({'dom': 'div', 'id': 'engagement-insights', 'class': 'dashboard'})
    var clientDataDashboard = Forge.build({'dom': 'div', 'id': 'engagement-clientData', 'class': 'dashboard'})
    /** NAV **/
    log('Building Engagment Dashboard Links')

    var pageHeader = Forge.build({'dom': 'div', 'class': 'pageHeader noSelect', 'id': 'engagementHeader'})
    var pageHeadContent = Forge.build({'dom': 'div', 'class': 'pageHeader-content'})
    var pageHeadBack = Forge.build({'dom': 'div', 'class': 'pageHeader-leftContainer'})
    var pageHeadFA = Forge.build({
      'dom': 'i',
      'id': 'engagement-backButton',
      'class': 'fa fa-angle-left pageHeader-backButton'
    })
    var pageHeadTextTitle = Forge.build({
      'dom': 'input',
      'type': 'text',
      'class': 'unt-text',
      'placeholder': 'Name your engagement',
      'id': 'a-header-title',
      'style': 'cursor: text; border: none; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; min-width: 340px; outline: none;'
    })
    var pageHeadTextEdit = Forge.build({'dom': 'img', 'id': 'a-header-pencil', 'class': 'unt-edit', 'src': 'images/icons/pencil.png'})

    $(pageHeadTextTitle).focus(function (event) {
      document.getElementById('a-header-title').classList.add('unt-text-edit')
      document.getElementById('a-header-pencil').style.display = 'block'
    })
    $(pageHeadTextTitle).focusout(function (event) {
      var name = event.target.value.trim()
      if (name === '') {
        document.getElementById('a-header-title').style.border = 'none'
        document.getElementById('a-header-title').style.borderRadius = null
      } else {
        c.updateEngagement({engagementID: c.currentEngagement._id, name})
        document.getElementById('auditTitle-' + c.currentEngagement._id).innerText = name
        document.getElementById('a-header-title').style.border = 'none'
        document.getElementById('a-header-title').style.borderRadius = null
        document.getElementById('a-header-title').classList.remove('unt-text-edit')
        document.getElementById('a-header-pencil').style.display = 'none'
        event.target.placeholder = name
        event.target.value = name
      }
    })

    var auditWizBtn = Forge.build({
      'dom': 'button',
      'id': 'goToWizardBtn',
      'class': 'btn btn-lg',
      'style': 'float:right; margin: 18px 0px;',
      'text': 'Go to Audit File'
    })

    parentContainer.appendChild(pageHeader)
    pageHeader.appendChild(pageHeadContent)
    pageHeadContent.appendChild(pageHeadBack)
    pageHeadBack.appendChild(pageHeadFA)
    pageHeadBack.appendChild(pageHeadTextTitle)
    pageHeadBack.appendChild(pageHeadTextEdit)
    pageHeadContent.appendChild(auditWizBtn)

    var dashboardLinksDiv = Forge.build({'dom': 'div', 'id': 'dashboardLinks', 'class': 'dashboard-pageNav-container'})
    parentContainer.appendChild(dashboardLinksDiv)

    /** Dashboard Nav Link **/
    var dashboardLink = Forge.build({'dom': 'div', 'id': 'engagementDashboardLink', 'class': 'dashboard-pageNav'})
    var dashLinkFA = Forge.build({'dom': 'div', 'class': 'auvicon-line-home', 'style': 'display:inline'})
    var dashLinkText = document.createTextNode(' Dashboard')
    dashboardLinksDiv.appendChild(dashboardLink)
    dashboardLink.appendChild(dashLinkFA)
    dashboardLink.appendChild(dashLinkText)

    /** Schedule Nav Link **/
    var scheduleLink = Forge.build({'dom': 'div', 'id': 'engagementRequestsLink', 'class': 'dashboard-pageNav'})
    var dashLinkFA = Forge.build({
      'dom': 'div',
      'class': 'auvicon-line-requests',
      'style': 'display: inline; font-size: 14px;'
    })
    var dashLinkText = document.createTextNode(' Requests')
    dashboardLinksDiv.appendChild(scheduleLink)
    scheduleLink.appendChild(dashLinkFA)
    scheduleLink.appendChild(dashLinkText)

    var clientDataLink = Forge.build({'dom': 'div', 'id': 'engagementClientDataLink', 'class': 'dashboard-pageNav'})
    var clientDataLinkFA = Forge.build({'dom': 'span', 'class': 'icon-closed-folder engagement-icon'})
    var clientDataLinkText = document.createTextNode(' Client Data')
    dashboardLinksDiv.appendChild(clientDataLink)
    clientDataLink.appendChild(clientDataLinkFA)
    clientDataLink.appendChild(clientDataLinkText)

    /** Docs Nav Link **/
    var docLink = Forge.build({'dom': 'div', 'id': 'engagementFileMangerLink', 'class': 'dashboard-pageNav'})
    var dashLinkFA = Forge.build({
      'dom': 'span',
      'class': 'auvicon-line-folder engagement-icon',
      'style': 'font-size: 16px'
    })
    var dashLinkText = document.createTextNode(' Files')
    dashboardLinksDiv.appendChild(docLink)
    docLink.appendChild(dashLinkFA)
    docLink.appendChild(dashLinkText)

    /** Activity Nav Link **/
    var activityLink = Forge.build({'dom': 'div', 'id': 'engagementActivityLink', 'class': 'dashboard-pageNav'})
    var dashLinkFA = Forge.build({'dom': 'span', 'class': 'auvicon-line-activity engagement-icon', 'style': '20px'})
    var dashLinkText = document.createTextNode(' Activity')
    dashboardLinksDiv.appendChild(activityLink)
    activityLink.appendChild(dashLinkFA)
    activityLink.appendChild(dashLinkText)

    /** Analysis Nav Link **/
    var analysisLink = Forge.build({'dom': 'div', 'id': 'engagementAnalysisLink', 'class': 'dashboard-pageNav'})
    var dashLinkFA = Forge.build({'dom': 'i', 'class': 'fa fa-line-chart'})
    var dashLinkText = document.createTextNode(' Analysis')
    dashboardLinksDiv.appendChild(analysisLink)
    analysisLink.appendChild(dashLinkFA)
    analysisLink.appendChild(dashLinkText)

    /** Team Nav Link **/
    var teamLink = Forge.build({'dom': 'div', 'id': 'engagementTeamLink', 'class': 'dashboard-pageNav'})
    var dashLinkFA = Forge.build({
      'dom': 'span',
      'class': 'icon-two-heads engagement-icon',
      'style': 'font-size: 24px; vertical-align: sub;'
    })
    var dashLinkText = document.createTextNode('Team')
    dashboardLinksDiv.appendChild(teamLink)
    teamLink.appendChild(dashLinkFA)
    teamLink.appendChild(dashLinkText)

    parentContainer.appendChild(mainDashboard)
    parentContainer.appendChild(fileManagerDashboard)
    // parentContainer.appendChild(requestsDashboard) @DELETE
    parentContainer.appendChild(activityDashboard)
    parentContainer.appendChild(teamDashboard)
    // parentContainer.appendChild(analysisDashboard) @DELETE
    parentContainer.appendChild(insightsDashboard)
    parentContainer.appendChild(clientDataDashboard)

    return parentContainer
  }

  this.setBorder = function () {
    if (c.currentUser.type === 'CLIENT') {
      activityFeedWidget.setBorder()
    } else {
      activityWidget.setBorder()
    }
  }

  this.hideHeader = function () {
    document.getElementById('engagementHeader').style.display = 'none'
    document.getElementById('dashboardLinks').style.display = 'none'
  }
  initialize()

  return this
}

Engagement.prototype = Object.create(ViewTemplate.prototype)
Engagement.prototype.constructor = Engagement

module.exports = Engagement
