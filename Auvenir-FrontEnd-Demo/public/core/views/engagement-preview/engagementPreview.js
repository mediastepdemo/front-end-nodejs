'use strict'

import ViewTemplate from '../ViewTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import styles from './engagementPreview.css'
import engagements from './myEngagements'
import Utility from '../../../js/src/utility_c'

// ***************     NOTE      ********************
// please do not change class in our original css.
// if you would like to change the style, please add a new class that will override our original style.
// please do not delete the original classes that is attached to the elements here. please add new class

// Note - this view should have the following setting for widths
// min-width: 1024px;
// max-width: 1440px;

function EngagementPreview (ctrl, settings) {
  ViewTemplate.call(this, ctrl, settings)
  var self = this
  var c = ctrl
  this.name = 'Engagement Preview'
  var text = c.lang.engagementPreview

  var resizePopup = function () { $('.ui.popup').css('max-height', $(window).height()) }

  var c = ctrl
  // c.myEngagements = engagements
  var engagementData = []
  var filter_value = null

  // var DASHBOARD_WIDTH = 1200
  var noEngagementContainer = $('<div id="auditPageNoEngagement" class="noEngagement-container">' +
      '<img src="images/illustrations/computerDesktop.svg"/>' +
      '<h2 class="noEngagement-txt">' + text.noEngagement + '</h2>' +
      '</div>')

  /**
   * Config the title column name of engagement table
   */
  var engagement = {
    company: {text: text.company, id: 'company-sort'},
    title: {text: text.engagementName, id: 'engagement-sort'},
    status: {text: text.status, id: 'status-sort'},
    auditor: {text: text.auditAssignee, id: 'audit-sort'},
    todo: {text: text.completedToDos, id: 'todo-sort'},
    client: {text: text.clientAssignee, id: 'client-sort'},
    docs: {text: text.completedDocs, id: 'docs-sort'},
    activity: {text: text.lastActivity, id: 'activity-sort'},
    duedate: {text: text.dueDate, id: 'duedate-sort'}
  }

  // Declare ui for tooltip
  /* var tooltipNewEngagement = Forge.buildTooltip({
    class: 'whiteToolTip',
    id: 'tooltip-createEngagement',
    right: '24px',
    float: 'right',
    marginTop: '15px',
    width: '284px',
    text: 'Click here to create your first audit engagement',
    animation: 'fade'
  }) */

  /**
   * Filter data
   */
  var engagementFiltersData = [
    {
      title: text.all,
      id: 'all',
      subMenu: []
    },
    {
      title: text.typeOfEngagement,
      id: 'type',
      subMenu: [text.financialAudit, text.review, text.noticeToReaderCompilation, text.other]
    }
  ]
  /**
   * Initializes the Engagement Dashboard and any relevant functionality
   */
  var initialize = function () {
    setupListeners()
    registerEvents()
  }
  /**
   * This function loads data from Controller (MAC) and populate the page.
   */
  this.loadDashboard = function () {
    clearSearchAndFilter()
    let engagementList = convertEngagements(c.myEngagements)
    engagementData = engagementList
    loadData(engagementList)
    document.getElementById(self._moduleID).classList.add('scroll-190')
  }

  var checkSearchAndFilterData = function (data) {
    var result = data
    var searchKey = document.getElementById('engagement-search').value
    if (searchKey) {
      result = searchData(searchKey, result)
    }

    if (filter_value) {
      result = filterData(filter_value.category, filter_value.value, result)
    }

    return result
  }

  var clearSearchAndFilter = function () {
    filter_value = null
    document.getElementById('engagement-search').value = ''
    var filter = document.getElementById('engagement-filter')
    if (filter) filter.querySelector('.text').innerText = 'Filter'
  }

  /**
   * This function builds page based on the data given to the page.
   */
  this.buildPage = function () {
    // PARENT
    var parentContainer = Forge.build({'dom': 'div', 'id': 'engagementPreview', style: 'margin-top: 24px;', class: 'parent-of-footer'})

    // HEADER
    var pageHeader = Forge.build({ 'dom': 'div', 'class': 'pageHeader noSelect' })
    var pageHeadContent = Forge.build({'dom': 'div', 'id': 'preview-head-content', 'class': 'pageHeader-content'})
    var pageHeadLeft = Forge.build({ 'dom': 'div', 'id': 'preview-header-left', 'class': 'pageHeader-leftContainer' })
    var newAuditBtn = Forge.buildBtn({text: text.newEngagement, id: 'newAuditBtn', type: 'darkblue', float: 'right', margin: '5px 0px 0px;'}).obj
    var pageHeadText = Forge.build({
      'dom': 'span',
      'class': 'pageHeader-title',
      'text': 'All Engagements',
      'id': 'c-header-title'
    })

    // TABLE & FILTER CONTAINER
    var container = Forge.build({dom: 'div', id: 'engagement-container', class: ''})

    // HEAD FILTER AND SEARCH
    var rowFilter = Forge.build({dom: 'div', id: 'engagement-filter', class: 'container-header'})

    // dropdown filter
    var engagementFilters = Forge.build({dom: 'div', id: 'engagement-filters', class: 'ui icon dropdown simple'})
    var engagementFiltersIcon = Forge.build({dom: 'i', class: 'filter icon'})
    var engagementFiltersTitle = Forge.build({dom: 'span', class: 'text', text: text.filters})
    var engagementFiltersCaret = Forge.build({dom: 'i', class: 'dropdown icon'})
    var engagementFiltersMenu1 = Forge.build({dom: 'div', class: 'menu lv1'})

    /* var engagementFiltersData = [
      {
        title: 'Type of Engagement',

        subMenu: ['Audits', 'Compilations', 'Reviews', 'Notice To Reader']
      }
    ] */

    engagementFiltersData.forEach((elem) => {
      var engagementFiltersItem1 = Forge.build({dom: 'div', class: 'item', type: elem.title})
      var engagementFiltersItem1Title = Forge.build({dom: 'span', class: 'text', text: elem.title})
      var engagementFiltersMenu2 = Forge.build({dom: 'div', class: 'menu'})

      engagementFiltersItem1.appendChild(engagementFiltersItem1Title)

      if (elem.subMenu.length > 0) {
        elem.subMenu.forEach((el) => {
          var engagementFiltersItem2 = Forge.build({dom: 'div', class: 'item', text: el, type: elem.title})
          engagementFiltersItem2.addEventListener('click', function () {
            filter_value = {category: this.getAttribute('type'), value: el}
            loadData(engagementData)
          })

          engagementFiltersMenu2.appendChild(engagementFiltersItem2)
        })

        engagementFiltersItem1.appendChild(engagementFiltersMenu2)
      } else {
        engagementFiltersItem1.addEventListener('click', function () {
          filter_value = {category: this.getAttribute('type'), value: elem.title}
          loadData(engagementData)
        })
      }

      engagementFiltersMenu1.appendChild(engagementFiltersItem1)
    })

    engagementFilters.appendChild(engagementFiltersIcon)
    engagementFilters.appendChild(engagementFiltersTitle)
    engagementFilters.appendChild(engagementFiltersCaret)
    engagementFilters.appendChild(engagementFiltersMenu1)
    rowFilter.appendChild(engagementFilters)
    $(engagementFilters).dropdown({
      transition: 'drop' // you can use any ui transition
    })

    // Search
    var search = Forge.build({dom: 'div', id: 'search-box', class: 'ui search floating right'})
    var inputSearch = Forge.build({dom: 'input', id: 'engagement-search', type: 'text', class: 'prompt', placeholder: c.lang.search})

    let time = null
    inputSearch.onkeydown = function () {
      clearTimeout(time)

      time = setTimeout(function () {
        loadData(engagementData)
      }, 500)
    }

    search.appendChild(inputSearch)
    rowFilter.appendChild(search)

    // DATA TABLE
    var engagementTable = Forge.build({dom: 'table', id: 'engagement-table', class: 'ui selectable very basic table padded'})
    var engagementThead = Forge.build({dom: 'thead'})
    var engagementRow = Forge.build({dom: 'tr'})
    var engagementTbody = Forge.build({dom: 'tbody', id: 'engagement-tbody'})

    var company = Forge.build({dom: 'th', text: engagement.company.text, id: engagement.company.id})
    var companyIcon = Forge.build({dom: 'i', class: 'auvicon-sort-down'})
    company.appendChild(companyIcon)
    company.setAttribute('data-id', 'business')
    company.setAttribute('data-sort', '1')

    var engagementTitle = Forge.build({dom: 'th', text: engagement.title.text, id: engagement.title.id})
    var titleIcon = Forge.build({dom: 'i', class: 'auvicon-sort-down'})
    engagementTitle.appendChild(titleIcon)
    engagementTitle.setAttribute('data-id', 'name')
    engagementTitle.setAttribute('data-sort', '1')

    var status = Forge.build({dom: 'th', text: engagement.status.text, id: engagement.status.id})
    var statusIcon = Forge.build({dom: 'i', class: 'auvicon-sort-down'})
    status.appendChild(statusIcon)
    status.setAttribute('data-id', 'status')
    status.setAttribute('data-sort', '1')

    var auditorAssigned = Forge.build({dom: 'th', text: engagement.auditor.text, id: engagement.auditor.id})
    var auditorIcon = Forge.build({dom: 'i', class: 'auvicon-sort-down'})
    auditorAssigned.appendChild(auditorIcon)
    auditorAssigned.setAttribute('data-id', 'leadAuditor')
    auditorAssigned.setAttribute('data-sort', '1')

    var completedToDo = Forge.build({dom: 'th', text: engagement.todo.text, id: engagement.todo.id})
    var completedToDoIcon = Forge.build({dom: 'i', class: 'auvicon-sort-down'})
    completedToDo.appendChild(completedToDoIcon)
    completedToDo.setAttribute('data-id', 'completedToDoNumber')
    completedToDo.setAttribute('data-sort', '1')

    var clientAssigned = Forge.build({dom: 'th', text: engagement.client.text, id: engagement.client.id})
    var clientAssignedIcon = Forge.build({dom: 'i', class: 'auvicon-sort-down'})
    clientAssigned.appendChild(clientAssignedIcon)
    clientAssigned.setAttribute('data-id', 'leadClient')
    clientAssigned.setAttribute('data-sort', '1')

    var completedDocs = Forge.build({dom: 'th', text: engagement.docs.text, id: engagement.docs.id})
    var completedDocsIcon = Forge.build({dom: 'i', class: 'auvicon-sort-down'})
    completedDocs.appendChild(completedDocsIcon)
    completedDocs.setAttribute('data-id', 'completedDocsNumber')
    completedDocs.setAttribute('data-sort', '1')

    var lastActivity = Forge.build({dom: 'th', text: engagement.activity.text, id: engagement.activity.id})
    var lastActivityIcon = Forge.build({dom: 'i', class: 'auvicon-sort-down'})
    lastActivity.appendChild(lastActivityIcon)
    lastActivity.setAttribute('data-id', 'lastUpdated')
    lastActivity.setAttribute('data-sort', '1')

    var dueDate = Forge.build({dom: 'th', text: engagement.duedate.text, id: engagement.duedate.id})
    var dueDateIcon = Forge.build({dom: 'i', class: 'auvicon-sort-down'})
    dueDate.appendChild(dueDateIcon)
    dueDate.setAttribute('data-id', 'dueDate')
    dueDate.setAttribute('data-sort', '1')

    engagementRow.appendChild(company)
    engagementRow.appendChild(engagementTitle)
    engagementRow.appendChild(status)
    engagementRow.appendChild(auditorAssigned)
    engagementRow.appendChild(completedToDo)
    engagementRow.appendChild(clientAssigned)
    engagementRow.appendChild(completedDocs)
    engagementRow.appendChild(lastActivity)
    engagementRow.appendChild(dueDate)

    engagementThead.appendChild(engagementRow)
    engagementTable.appendChild(engagementThead)
    engagementTable.appendChild(engagementTbody)

    // var engagementColumn = Forge.build({dom: 'td'})
    // engagementRow.appendChild(engagementColumn)

    parentContainer.appendChild(pageHeader)
    pageHeader.appendChild(pageHeadContent)
    pageHeadContent.appendChild(pageHeadLeft)
    pageHeadContent.appendChild(newAuditBtn)
    pageHeadLeft.appendChild(pageHeadText)

    var tooltipContainer = Forge.build({dom: 'div', style: 'position: relative'})

    if (c.myEngagements && c.myEngagements.length < 1) {
      var tooltipOne = Forge.buildTooltip({
        class: 'whiteToolTip',
        id: 'tooltip-createEngagement',
        right: '16px',
        float: 'right',
        marginTop: '15px',
        width: '284px',
        text: text.engagementTT,
        animation: 'fade',
        arrowLeft: '200px'
      })
      tooltipContainer.appendChild(tooltipOne)
    }
    pageHeadContent.appendChild(tooltipContainer)
    pageHeadContent.className += ' tooltipTrigger'

    noEngagementContainer = $('<div id="auditPageNoEngagement" class="noEngagement-container">' +
      '<img src="images/illustrations/computerDesktop.svg"/>' +
      '<h2 class="noEngagement-txt">' + text.noEngagement + '</h2>' +
      '</div>')

    container.appendChild(rowFilter)
    container.appendChild(engagementTable)

    var mainPage = Forge.build({ 'dom': 'div', 'id': 'preview-main', 'class': 'dashboard', 'style': 'display:none' })
    mainPage.appendChild(container)
    parentContainer.appendChild(noEngagementContainer[0])
    parentContainer.appendChild(mainPage)

    newAuditBtn.addEventListener('click', function () {
      // this function will open modal to create a new engagement.
      c.displayCreateEngagementModal()
    })

    var footerComponent = Forge.build({dom: 'div', id: 'preview-footer'})
    footerComponent.innerHTML = c.footer
    parentContainer.appendChild(footerComponent)

    return parentContainer
  }

  var fullName = function (firsName, lasName) {
    return firsName + ' ' + lasName
  }

  // Return css for label status
  var getCssStatus = function (status) {
    var lowercaseStatus = status.toLowerCase()
    return 'ui label ' + lowercaseStatus
  }

  // show and hide tooltip for columns completed To-Dos and completed Docs
  var showTooltip = function (dom, id, prefix) {
    $(dom).popup({
      on: 'hover',
      popup: '#' + prefix + '-' + id,
      // position: 'bottom center',
      lastResort: 'bottom right',
      onShow: function () {
        resizePopup()
      }
    })
  }

  var engagementSort = function (e) {
    var target = e.target.childNodes.length > 0 ? $(e.target).find('i') : $(e.target)
    var checkDownClass = target.hasClass('auvicon-sort-down')
    var asc = (checkDownClass ? '1' : '0')

    target
        .removeClass(checkDownClass ? 'auvicon-sort-down' : 'auvicon-sort-up')
        .addClass(checkDownClass ? 'auvicon-sort-up' : 'auvicon-sort-down')
    var field = this.getAttribute('data-id')
    engagementData.sort(getSortOrder(field, asc))
    loadData(engagementData)
  }

  var engagementSearch = function (e) {
    if (e.which === 13) {
      loadData(engagementData)
    }
  }

  var clearPopupClass = function (id, prefix) {
    var element = document.getElementById(prefix + '-' + id)
    element.classList.remove('hidden')
  }

  var getSortOrder = function (prop, asc) {
    log('prop: ' + prop + ' asc:' + asc)
    return function (a, b) {
      var value_a = ''
      var value_b = ''
      switch (prop) {
        case 'business':
          value_a = (a[prop] ? a[prop].name : '')
          value_b = (b[prop] ? b[prop].name : '')
          break
        default:
          value_a = a[prop]
          value_b = b[prop]
          break
      }
      if (asc === '1') {
        return (value_a > value_b) ? 1 : ((value_a < value_b) ? -1 : 0)
      } else {
        return (value_b > value_a) ? 1 : ((value_b < value_a) ? -1 : 0)
      }
    }
  }

  // event response handler
  var handle_createEngagement = function (tag, data) {
    if (document.getElementById('preview-main').style.display === 'none') {
      document.getElementById('preview-main').style.display = 'inherit'
      noEngagementContainer.hide()
    }
    addEngagementRow(Utility.convertEngagement(data.engagement))
    clearSearchAndFilter()
    c.loadEngagement(data.engagement._id)
  }

  var convertEngagements = function (engagementList) {
    let list = []
    engagementList.forEach((engagement) => {
      list.push(Utility.convertEngagement(engagement))
    })
    return list
  }

   /**
   * Registers all Engagement Dashboard events
   */
  var registerEvents = function () {
    document.getElementById('engagement-sort').addEventListener('click', engagementSort)
    document.getElementById('company-sort').addEventListener('click', engagementSort)
    document.getElementById('status-sort').addEventListener('click', engagementSort)

    document.getElementById('audit-sort').addEventListener('click', engagementSort)
    document.getElementById('todo-sort').addEventListener('click', engagementSort)
    document.getElementById('client-sort').addEventListener('click', engagementSort)

    document.getElementById('docs-sort').addEventListener('click', engagementSort)
    document.getElementById('activity-sort').addEventListener('click', engagementSort)
    document.getElementById('duedate-sort').addEventListener('click', engagementSort)
  }

  /**
   * Setup Listener
   */
  var setupListeners = function () {
    c.registerForEvent('create-engagement', 'PREVIEW_NEWENGAGEMENT-000', handle_createEngagement)
  }
  var filterData = function (category, value, data) {
    let filterResult = []
    if (category === '' || value === '' || value === 'All') {
      return data
    }

    var typeList = JSON.parse(JSON.stringify(engagementFiltersData[1].subMenu))
    typeList.splice(typeList.length - 1, 1)
    var length = data.length
    for (var i = 0; i < length; i++) {
      let element = data[i]
      if (value === 'Other' && typeList.indexOf(element.type) === -1) {
        filterResult.push(element)
      } else if (value !== 'Other' && element.type === value) {
        filterResult.push(element)
      }
    }
    log('typeList', typeList)
    log('filter category: ' + category + ' value: ' + value)
    log('filter on data', data)
    log('filter result', filterResult)
    return filterResult
  }

  var searchData = function (key, data) {
    let result = []
    if (key === '') {
      return data
    }
    var length = data.length
    for (var i = 0; i < length; i++) {
      var regex = new RegExp(key, 'i')
      let element = data[i]
      for (let k in element) {
        let item = element[k]
        if (element.hasOwnProperty(k) && (k === 'dueDate' || k === 'lastUpdated')) {
          item = ('0' + item.getDate()).slice(-2) + '/' + ('0' + (item.getMonth() + 1)).slice(-2) + '/' + item.getFullYear().toString().substr(-2)
        }
        if (element.hasOwnProperty(k) && k === 'business' && item.name !== undefined) {
          item = item.name
        }
        if (element.hasOwnProperty(k) && regex.test(item)) {
          result.push(element)
          break
        }
      }
    }
    log('search key: ' + key)
    log('search on data', data)
    log('search result', result)
    return result
  }

  var loadData = function (engagementList) {
    let tooltip = document.getElementById('tooltip-createEngagement')
    if (c.myEngagements && c.myEngagements !== undefined && c.myEngagements.length === 0) {
      noEngagementContainer.show()
      if (tooltip) tooltip.style.display = 'block'
    } else {
      var data = checkSearchAndFilterData(engagementList)
      noEngagementContainer.hide()
      if (tooltip) tooltip.style.display = 'none'
      document.getElementById('preview-main').style.display = 'inherit'
      // c.getEngagements(c.currentUser._id, c.currentUser.type)
      const tbody = document.getElementById('engagement-tbody')
      tbody.innerHTML = ''
      data.forEach((engagement) => {
        addEngagementRow(engagement)
      })
    }
  }

  /**
   * This function will be used when looping through c.myEngagements[] and populate each engagement into the table.
   */
  var addEngagementRow = function (engagement) {
    const tbody = document.getElementById('engagement-tbody')
    var newRow = Forge.build({dom: 'tr'})
    tbody.appendChild(newRow)
       // column name engagement
    var newCompany = Forge.build({dom: 'td', 'text': engagement.business.name})
    var newLinkEngagemenName = Forge.build({dom: 'a', 'text': engagement.name, href: '#'})
    var newEngagemenName = Forge.build({dom: 'td', class: 'engagement-name'})
    newEngagemenName.appendChild(newLinkEngagemenName)
    newEngagemenName.addEventListener('click', () => {
      c.loadEngagement(engagement._id)
    })
    var newStatus = Forge.build({dom: 'td', class: 'status'})
    var spanStatus = Forge.build({dom: 'span', 'text': engagement.status.toLowerCase(), class: getCssStatus(engagement.status)})
    newStatus.appendChild(spanStatus)

    newRow.appendChild(newCompany)
    newRow.appendChild(newEngagemenName)
    newRow.appendChild(newStatus)

    // Process for auditor assigned
    var newAuditAssigned = Forge.build({dom: 'td', 'text': engagement.leadAuditor})
    newRow.appendChild(newAuditAssigned)

    // Process for complete todo
    var newCompletedToDos = Forge.build({dom: 'td', class: 'completed-todos'})
    var newSpanCompletedToDo = Forge.build({dom: 'span', class: 'warning', text: engagement.completedToDo})

    var newClientAssigned = Forge.build({dom: 'td', text: engagement.leadClient})

    var newCompletedDocs = Forge.build({dom: 'td', class: 'completed-docs'})
    var newSpanCompletedDocs = Forge.build({dom: 'span', class: 'warning', text: engagement.completedDocs})

    let date = engagement.lastUpdated
    let dateString = ''
    if (engagement.lastUpdated) {
      dateString = ('0' + (date.getMonth() + 1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2) + '/' + date.getFullYear().toString().substr(-2)
    }
    var newLastActivity = Forge.build({dom: 'td', text: dateString})

    date = engagement.dueDate
    dateString = ''
    var newDueDate
    if (engagement.dueDate) {
      dateString = ('0' + (date.getMonth() + 1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2) + '/' + date.getFullYear().toString().substr(-2)
      newDueDate = Forge.build({dom: 'div', text: text.due + ':' + dateString, style: 'display: inline-block; width: 143px; height: 32px; border: 1px solid #E1E1E1; vertical-align: middle; border-radius: 100px; text-align: center; padding: 8px; margin-right: 24px; margin-top: 6px;', class: 'engagement-cal auvicon-calendar'})
    } else {
      newDueDate = Forge.build({dom: 'td', text: dateString})
    }

    newRow.appendChild(newCompletedToDos)
    newRow.appendChild(newClientAssigned)
    newRow.appendChild(newCompletedDocs)
    newRow.appendChild(newLastActivity)
    newRow.appendChild(newDueDate)

    var tooltipCompleted = Forge.build({dom: 'div', id: 'completed-' + engagement._id, class: 'ui special popup completed-todo-popup'})
    var tooltipCompletedDoc = Forge.build({dom: 'div', id: 'completedDoc-' + engagement._id, class: 'ui special popup completed-todo-popup'})

    // container for information complete todo and docs
    var overdueContainer = Forge.build({dom: 'div', class: 'overdue'})

    var overdueNumber = Forge.build({dom: 'span', class: 'number', text: engagement.overdueToDo.toString()})
    var overdueTitle = Forge.build({dom: 'span', text: text.overdue})
    var overdueTodoText = Forge.build({dom: 'span', text: text.toDos})

    var outstandingContainer = Forge.build({dom: 'div', class: 'oustanding'})
    var outstandingNumber = Forge.build({dom: 'span', class: 'number', text: engagement.outstandingToDo.toString()})
    var outstandingTitle = Forge.build({dom: 'span', text: text.outstanding})
    var outstandingTodoText = Forge.build({dom: 'span', text: text.toDos})

    var totalContainer = Forge.build({dom: 'div', class: 'total'})
    var totalNumber = Forge.build({dom: 'span', class: 'number', text: engagement.totalToDo.toString()})
    var totalTitle = Forge.build({dom: 'span', text: text.total})
    var totalTodoText = Forge.build({dom: 'span', text: text.toDos})

    var closeButton = Forge.build({dom: 'i', class: 'remove icon'})

    var overdueContainerDoc = Forge.build({dom: 'div', class: 'overdue'})
    var overdueNumberDoc = Forge.build({dom: 'span', class: 'number', text: engagement.overdueDoc.toString()})
    var overdueTitleDoc = Forge.build({dom: 'span', text: text.overdue})
    var overdueDocText = Forge.build({dom: 'span', text: text.documents})

    var outstandingContainerDoc = Forge.build({dom: 'div', class: 'oustanding'})
    var outstandingNumberDoc = Forge.build({dom: 'span', class: 'number', text: engagement.outstandingDoc.toString()})
    var outstandingTitleDoc = Forge.build({dom: 'span', text: text.outstanding})
    var outstandingDocText = Forge.build({dom: 'span', text: text.documents})

    var totalContainerDoc = Forge.build({dom: 'div', class: 'total'})
    var totalNumberDoc = Forge.build({dom: 'span', class: 'number', text: engagement.totalDoc.toString()})
    var totalTitleDoc = Forge.build({dom: 'span', text: text.total})
    var totalDocText = Forge.build({dom: 'span', text: text.documents})

    var closeButtonDoc = Forge.build({dom: 'i', class: 'remove icon'})

    overdueContainer.appendChild(overdueNumber)
    overdueContainer.appendChild(overdueTitle)
    overdueContainer.appendChild(overdueTodoText)
    outstandingContainer.appendChild(outstandingNumber)
    outstandingContainer.appendChild(outstandingTitle)
    outstandingContainer.appendChild(outstandingTodoText)
    totalContainer.appendChild(totalNumber)
    totalContainer.appendChild(totalTitle)
    totalContainer.appendChild(totalTodoText)

    tooltipCompleted.appendChild(closeButton)
    tooltipCompleted.appendChild(overdueContainer)
    tooltipCompleted.appendChild(outstandingContainer)
    tooltipCompleted.appendChild(totalContainer)

    newSpanCompletedToDo.appendChild(tooltipCompleted)
    newCompletedToDos.appendChild(newSpanCompletedToDo)

    showTooltip(newSpanCompletedToDo, engagement._id, 'completed')

    overdueContainerDoc.appendChild(overdueNumberDoc)
    overdueContainerDoc.appendChild(overdueTitleDoc)
    overdueContainerDoc.appendChild(overdueDocText)
    outstandingContainerDoc.appendChild(outstandingNumberDoc)
    outstandingContainerDoc.appendChild(outstandingTitleDoc)
    outstandingContainerDoc.appendChild(outstandingDocText)
    totalContainerDoc.appendChild(totalNumberDoc)
    totalContainerDoc.appendChild(totalTitleDoc)
    totalContainerDoc.appendChild(totalDocText)

    tooltipCompletedDoc.appendChild(closeButtonDoc)
    tooltipCompletedDoc.appendChild(overdueContainerDoc)
    tooltipCompletedDoc.appendChild(outstandingContainerDoc)
    tooltipCompletedDoc.appendChild(totalContainerDoc)

    newSpanCompletedDocs.appendChild(tooltipCompletedDoc)
    newCompletedDocs.appendChild(newSpanCompletedDocs)

    showTooltip(newSpanCompletedDocs, engagement._id, 'completedDoc')
  }

  this.build() // from viewTemplate(parentObj)

  initialize()
  return this
}

module.exports = EngagementPreview
