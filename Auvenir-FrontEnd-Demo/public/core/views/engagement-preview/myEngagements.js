const todo = [
  {
    name: 'Send engagement letter',
    description: 'Need to send letter to Florance.',
    category: 'Select',
    dueDate: '03/05/18',
    comments: [],
    requests: [],
    createdBy: {id: 'A1', name: 'Bob Robson'},
    auditorAssignee: {id: 'A1', name: 'Bob Robson'},
    clientAssignee: '',
    complete: false
  },
  {
    name: 'Revise & edit document request list',
    description: '^^^',
    category: 'Admin',
    dueDate: '04/08/16',
    comments: [],
    requests: [],
    createdBy: {id: 'A1', name: 'Bob Robson'},
    auditorAssignee: {id: 'A2', name: 'Alice Miller'},
    clientAssignee: '',
    complete: false
  },
  {
    name: 'This has just Comments',
    description: '^^^',
    category: 'Admin',
    dueDate: '04/08/18',
    comments: [
        {content: 'A1', sender: {id: 'A1', name: 'Bob Robson'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'A2', sender: {id: 'A2', name: 'Alice Miller'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C1', sender: {id: 'C1', name: 'Florance Nightly'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C2', sender: {id: 'C2', name: 'Bruce Smith'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'A3', sender: {id: 'A1', name: 'Fred Perry'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C1', sender: {id: 'C1', name: 'Florance Nightly'}, files: 'ObjectId', dateCreated: 'Date', status: 'OK'}
    ],
    requests: [],
    createdBy: {id: 'A1', name: 'Bob Robson'},
    auditorAssignee: {id: 'A2', name: 'Alice Miller'},
    clientAssignee: '',
    complete: false
  },
  {
    name: 'This has just Requests',
    description: '^^^',
    category: 'Admin',
    dueDate: '04/08/18',
    comments: [],
    requests: [
        { name: 'Q1', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: true, dateCreated: '', dateUploaded: '' },
        { name: 'Q2', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: '', dateCreated: '', dateUploaded: '' },
        { name: 'Q3', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: '', dateCreated: '', dateUploaded: '' },
        { name: 'Q4', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: false, dateCreated: '', dateUploaded: '' }
    ],
    createdBy: {id: 'A1', name: 'Bob Robson'},
    auditorAssignee: {id: 'A2', name: 'Alice Miller'},
    clientAssignee: '',
    complete: false
  },
  {
    name: 'Financial Statement',
    description: 'Hi Bruce, we need the Financial statements from X date to Y date.',
    category: 'Financials',
    dueDate: '04/30/18',
    comments: [
        {content: 'A1', sender: {id: 'A1', name: 'Bob Robson'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'A2', sender: {id: 'A2', name: 'Alice Miller'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C1', sender: {id: 'C1', name: 'Florance Nightly'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C2', sender: {id: 'C2', name: 'Bruce Smith'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'A3', sender: {id: 'A1', name: 'Fred Perry'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C1', sender: {id: 'C1', name: 'Florance Nightly'}, files: 'ObjectId', dateCreated: 'Date', status: 'OK'}
    ],
    requests: [
        { name: 'Q1', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: true, dateCreated: '', dateUploaded: '' },
        { name: 'Q2', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: '', dateCreated: '', dateUploaded: '' },
        { name: 'Q3', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: '', dateCreated: '', dateUploaded: '' },
        { name: 'Q4', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: false, dateCreated: '', dateUploaded: '' }
    ],
    createdBy: {id: 'A1', name: 'Bob Robson'},
    auditorAssignee: {id: 'A2', name: 'Alice Miller'},
    clientAssignee: {id: 'C1', name: 'Florance Nightly'},
    complete: true
  },
  {
    name: 'General Ledger',
    description: 'Gen Ledger for Q1-Q4 2016.',
    category: 'Controls',
    dueDate: '04/30/18',
    comments: [
        {content: 'A3', sender: {id: 'A1', name: 'Fred Perry'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C1', sender: {id: 'C1', name: 'Florance Nightly'}, files: 'ObjectId', dateCreated: 'Date', status: 'OK'}
    ],
    requests: [
        { name: 'General Ledger', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: '', dateCreated: '', dateUploaded: '' },
        { name: 'General Ledger', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: true, dateCreated: '', dateUploaded: '' }
    ],
    createdBy: {id: 'A1', name: 'Bob Robson'},
    auditorAssignee: {id: 'A1', name: 'Fred Perry'},
    clientAssignee: {id: 'C1', name: 'Florance Nightly'},
    complete: false
  },
  {
    name: 'Business Cycle Controls',
    description: 'Description goes here.',
    category: 'Controls',
    dueDate: '04/28/18',
    comments: [
        {content: 'A4', sender: {id: 'A4', name: 'Shelly O’Neil'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C2', sender: {id: 'C2', name: 'Bruce Smith'}, files: 'ObjectId', dateCreated: 'Date', status: 'OK'}
    ],
    requests: [
        { name: 'Business', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: true, dateCreated: '', dateUploaded: '' },
        { name: 'Cycle', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: false, dateCreated: '', dateUploaded: '' },
        { name: 'Controls', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: '', dateCreated: '', dateUploaded: '' }
    ],
    createdBy: {id: 'A1', name: 'Bob Robson'},
    auditorAssignee: {id: 'A4', name: 'Shelly O’Neil'},
    clientAssignee: {id: 'C2', name: 'Bruce Smith'},
    complete: false
  },
  {
    name: 'Fraud and Error',
    description: 'Description goes here.',
    category: 'Financials',
    dueDate: '03/30/18',
    comments: [
        {content: 'A1', sender: {id: 'A1', name: 'Bob Robson'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C2', sender: {id: 'C2', name: 'Bruce Smith'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C3', sender: {id: 'C3', name: 'Sally Baggot'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'A3', sender: {id: 'A1', name: 'Fred Perry'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'}
    ],
    requests: [
        { name: 'Q1', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: true, dateCreated: '', dateUploaded: '' },
        { name: 'Q2', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: '', dateCreated: '', dateUploaded: '' },
        { name: 'Q3', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: false, dateCreated: '', dateUploaded: '' },
        { name: 'Q4', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: '', dateCreated: '', dateUploaded: '' },
        { name: 'Summary', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: false, dateCreated: '', dateUploaded: '' },
        { name: 'Q2', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: '', dateCreated: '', dateUploaded: '' },
        { name: 'Q3', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: false, dateCreated: '', dateUploaded: '' },
        { name: 'Q4', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: '', dateCreated: '', dateUploaded: '' },
        { name: 'Summary', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: false, dateCreated: '', dateUploaded: '' },
        { name: 'Q2', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: '', dateCreated: '', dateUploaded: '' },
        { name: 'Q3', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: false, dateCreated: '', dateUploaded: '' },
        { name: 'Q4', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: '', dateCreated: '', dateUploaded: '' },
        { name: 'Summary', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: false, dateCreated: '', dateUploaded: '' },
        { name: 'Q2', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: '', dateCreated: '', dateUploaded: '' },
        { name: 'Q3', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: false, dateCreated: '', dateUploaded: '' },
        { name: 'Q4', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: '', dateCreated: '', dateUploaded: '' },
        { name: 'Summary', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: false, dateCreated: '', dateUploaded: '' }
    ],
    createdBy: {id: 'A1', name: 'Bob Robson'},
    auditorAssignee: {id: 'A4', name: 'Shelly O’Neil'},
    clientAssignee: {id: 'C1', name: 'Florance Nightly'},
    complete: false
  },
  {
    name: 'Description of business',
    description: 'Description goes here.',
    category: 'Controls',
    dueDate: '04/20/18',
    comments: [
        {content: 'A1', sender: {id: 'A1', name: 'Bob Robson'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C2 lakjhds kajsdh alskdjh jhajlksjdh aksjhdlfk jhslakjhds', sender: {id: 'C2', name: 'Bruce Smith'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C1 lakjhds lkajhsdf kajsdh flkajhs alskdjh flakjhsd jhajlksjdh aksjhdlfk jhslakjhds lkajhsdf kajsdh flkajhs alskdjh flakjhsd jhajlksjdh aksjhdlfkajhs', sender: {id: 'C1', name: 'Florance Nightly'}, files: 'ObjectId', dateCreated: 'Date', status: 'OK'},
        {content: 'C3 lakjhds flkajshd lfkjhas dlkjhflak sjdlkf jhajlksjdh flkjha slkdjh flkajhs dlkjhf laksjhd lkfjha sdhj flkjahs dlkjfha', sender: {id: 'C3', name: 'Sally Baggot'}, files: 'ObjectId', dateCreated: 'Date', status: 'OK'},
        {content: 'C2 lkajhsdflkajsdh flkajhsdl kajhsdlkfjhalkdj hfl aksjhdlfkajhs dlkfjha ', sender: {id: 'C2', name: 'Bruce Smith'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C1 lkjahsldkfjh alksjdh flkajhs dlkfjh alkjsdhf lkasjhd fh aslkjdhflkajhsdlkjhas', sender: {id: 'C1', name: 'Florance Nightly'}, files: 'ObjectId', dateCreated: 'Date', status: 'OK'},
        {content: 'C3 lkajsdh flkajhsd flkajhs dlkfjh alskdjh flkajshd lfkjha slkdjhf laksjhd flkajhsd lfkjah sdlkjfh alkj', sender: {id: 'C3', name: 'Sally Baggot'}, files: 'ObjectId', dateCreated: 'Date', status: 'OK'},
        {content: 'C2 lkajsh dfklajhs dklfjha lkdjh flakjshd flakjhsd flkajh sdlkjhf alksjdh flkajhs dlkfjha sldkjhf alksjdh flakjdhs flkajsdfh ', sender: {id: 'C2', name: 'Bruce Smith'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C1 lakjhds flkajshd lfkjhas dlkjhflak sjdlkf jhajlksjdh flkjha slkdjh flkajhs dlkjhf laksjhd lkfjha sdhj flkjahs dlkjfha', sender: {id: 'C1', name: 'Florance Nightly'}, files: 'ObjectId', dateCreated: 'Date', status: 'OK'},
        {content: 'C3 lkajhsdflkajsdh flkajhsdl kajhsdlkfjhalkdj hfl aksjhdlfkajhs dlkfjha ', sender: {id: 'C3', name: 'Sally Baggot'}, files: 'ObjectId', dateCreated: 'Date', status: 'OK'},
        {content: 'C2 lkjahsldkfjh alksjdh flkajhs dlkfjh alkjsdhf lkasjhd fh aslkjdhflkajhsdlkjhas', sender: {id: 'C2', name: 'Bruce Smith'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C1 lkajsdh flkajhsd flkajhs dlkfjh alskdjh flkajshd lfkjha slkdjhf laksjhd flkajhsd lfkjah sdlkjfh alkj', sender: {id: 'C1', name: 'Florance Nightly'}, files: 'ObjectId', dateCreated: 'Date', status: 'OK'},
        {content: 'C3 lkajsh dfklajhs dklfjha lkdjh flakjshd flakjhsd flkajh sdlkjhf alksjdh flkajhs dlkfjha sldkjhf alksjdh flakjdhs flkajsdfh ', sender: {id: 'C3', name: 'Sally Baggot'}, files: 'ObjectId', dateCreated: 'Date', status: 'OK'}
    ],
    requests: [
        { name: String, currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: true, dateCreated: '', dateUploaded: '' }
    ],
    createdBy: {id: 'A1', name: 'Bob Robson'},
    auditorAssignee: {id: 'A4', name: 'Shelly O’Neil'},
    clientAssignee: {id: 'C3', name: 'Sally Baggot'},
    complete: false
  },
  {
    name: 'Overview of operations',
    description: 'Description goes here.',
    category: 'Controls',
    dueDate: '04/20/18',
    comments: [
        {content: 'A1', sender: {id: 'A1', name: 'Bob Robson'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C3', sender: {id: 'C3', name: 'Sally Baggot'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C1', sender: {id: 'C1', name: 'Florance Nightly'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'A1', sender: {id: 'A1', name: 'Bob Robson'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'}
    ],
    requests: [
        { name: String, currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: true, dateCreated: '', dateUploaded: '' },
        { name: String, currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: false, dateCreated: '', dateUploaded: '' }
    ],
    createdBy: {id: 'A1', name: 'Bob Robson'},
    auditorAssignee: {id: 'A5', name: 'Frank Sinat'},
    clientAssignee: {id: 'C3', name: 'Sally Baggot'},
    complete: false
  },
  {
    name: 'Financial Statement',
    description: 'Hi Bruce, we need the Financial statements from X date to Y date.',
    category: 'Financials',
    dueDate: '04/30/18',
    comments: [
        {content: 'A1', sender: {id: 'A1', name: 'Bob Robson'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'A2', sender: {id: 'A2', name: 'Alice Miller'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C1', sender: {id: 'C1', name: 'Florance Nightly'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C2', sender: {id: 'C2', name: 'Bruce Smith'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'A3', sender: {id: 'A1', name: 'Fred Perry'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C1', sender: {id: 'C1', name: 'Florance Nightly'}, files: 'ObjectId', dateCreated: 'Date', status: 'OK'}
    ],
    requests: [
        { name: 'Q1', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: true, dateCreated: '', dateUploaded: '' },
        { name: 'Q2', currentFile: '', prevFile: [ 'set to \'\'' ], viewed: '', dateCreated: '', dateUploaded: '' },
        { name: 'Q3', currentFile: '', prevFile: [ 'set to null' ], viewed: '', dateCreated: '', dateUploaded: '' },
        { name: 'Q4', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: false, dateCreated: '', dateUploaded: '' }
    ],
    createdBy: {id: 'A1', name: 'Bob Robson'},
    auditorAssignee: {id: 'A2', name: 'Alice Miller'},
    clientAssignee: {id: 'C1', name: 'Florance Nightly'},
    complete: true
  },
  {
    name: 'General Ledger',
    description: 'Gen Ledger for Q1-Q4 2016.',
    category: 'Controls',
    dueDate: '04/30/18',
    comments: [
        {content: 'A3', sender: {id: 'A1', name: 'Fred Perry'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C1', sender: {id: 'C1', name: 'Florance Nightly'}, files: 'ObjectId', dateCreated: 'Date', status: 'OK'}
    ],
    requests: [
        { name: 'General Ledger', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: '', dateCreated: '', dateUploaded: '' },
        { name: 'General Ledger', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: true, dateCreated: '', dateUploaded: '' }
    ],
    createdBy: {id: 'A1', name: 'Bob Robson'},
    auditorAssignee: {id: 'A1', name: 'Fred Perry'},
    clientAssignee: {id: 'C1', name: 'Florance Nightly'},
    complete: false
  },
  {
    name: 'Business Cycle Controls',
    description: 'Description goes here.',
    category: 'Controls',
    dueDate: '04/28/18',
    comments: [
        {content: 'A4', sender: {id: 'A4', name: 'Shelly O’Neil'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C2', sender: {id: 'C2', name: 'Bruce Smith'}, files: 'ObjectId', dateCreated: 'Date', status: 'OK'}
    ],
    requests: [
        { name: 'Business', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: true, dateCreated: '', dateUploaded: '' },
        { name: 'Cycle', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: false, dateCreated: '', dateUploaded: '' },
        { name: 'Controls', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: '', dateCreated: '', dateUploaded: '' }
    ],
    createdBy: {id: 'A1', name: 'Bob Robson'},
    auditorAssignee: {id: 'A4', name: 'Shelly O’Neil'},
    clientAssignee: {id: 'C2', name: 'Bruce Smith'},
    complete: false
  },
  {
    name: 'Fraud and Error',
    description: 'Description goes here.',
    category: 'Financials',
    dueDate: '03/30/18',
    comments: [
        {content: 'A1', sender: {id: 'A1', name: 'Bob Robson'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C2', sender: {id: 'C2', name: 'Bruce Smith'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C3', sender: {id: 'C3', name: 'Sally Baggot'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'A3', sender: {id: 'A1', name: 'Fred Perry'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'}
    ],
    requests: [
        { name: 'Q1', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: true, dateCreated: '', dateUploaded: '' },
        { name: 'Q2', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: '', dateCreated: '', dateUploaded: '' },
        { name: 'Q3', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: false, dateCreated: '', dateUploaded: '' },
        { name: 'Q4', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: '', dateCreated: '', dateUploaded: '' },
        { name: 'Summary', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: false, dateCreated: '', dateUploaded: '' }
    ],
    createdBy: {id: 'A1', name: 'Bob Robson'},
    auditorAssignee: {id: 'A4', name: 'Shelly O’Neil'},
    clientAssignee: {id: 'C1', name: 'Florance Nightly'},
    complete: false
  },
  {
    name: 'Description of business',
    description: 'Description goes here.',
    category: 'Controls',
    dueDate: '04/20/18',
    comments: [
        {content: 'A1', sender: {id: 'A1', name: 'Bob Robson'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C2', sender: {id: 'C2', name: 'Bruce Smith'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C1', sender: {id: 'C1', name: 'Florance Nightly'}, files: 'ObjectId', dateCreated: 'Date', status: 'OK'},
        {content: 'C3', sender: {id: 'C3', name: 'Sally Baggot'}, files: 'ObjectId', dateCreated: 'Date', status: 'OK'}
    ],
    requests: [
        { name: String, currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: true, dateCreated: '', dateUploaded: '' }
    ],
    createdBy: {id: 'A1', name: 'Bob Robson'},
    auditorAssignee: {id: 'A4', name: 'Shelly O’Neil'},
    clientAssignee: {id: 'C3', name: 'Sally Baggot'},
    complete: false
  },
  {
    name: 'Overview of operations',
    description: 'Description goes here.',
    category: 'Controls',
    dueDate: '04/20/18',
    comments: [
        {content: 'A1', sender: {id: 'A1', name: 'Bob Robson'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C3', sender: {id: 'C3', name: 'Sally Baggot'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C1', sender: {id: 'C1', name: 'Florance Nightly'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'A1', sender: {id: 'A1', name: 'Bob Robson'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'}
    ],
    requests: [
        { name: String, currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: true, dateCreated: '', dateUploaded: '' },
        { name: String, currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: false, dateCreated: '', dateUploaded: '' }
    ],
    createdBy: {id: 'A1', name: 'Bob Robson'},
    auditorAssignee: {id: 'A5', name: 'Frank Sinat'},
    clientAssignee: {id: 'C3', name: 'Sally Baggot'},
    complete: false
  },
  {
    name: 'Financial Statement',
    description: 'Hi Bruce, we need the Financial statements from X date to Y date.',
    category: 'Financials',
    dueDate: '04/30/18',
    comments: [
        {content: 'A1', sender: {id: 'A1', name: 'Bob Robson'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'A2', sender: {id: 'A2', name: 'Alice Miller'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C1', sender: {id: 'C1', name: 'Florance Nightly'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C2', sender: {id: 'C2', name: 'Bruce Smith'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'A3', sender: {id: 'A1', name: 'Fred Perry'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C1', sender: {id: 'C1', name: 'Florance Nightly'}, files: 'ObjectId', dateCreated: 'Date', status: 'OK'}
    ],
    requests: [
        { name: 'Q1', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: true, dateCreated: '', dateUploaded: '' },
        { name: 'Q2', currentFile: '', prevFile: [ 'set to \'\'' ], viewed: '', dateCreated: '', dateUploaded: '' },
        { name: 'Q3', currentFile: '', prevFile: [ 'set to null' ], viewed: '', dateCreated: '', dateUploaded: '' },
        { name: 'Q4', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: false, dateCreated: '', dateUploaded: '' }
    ],
    createdBy: {id: 'A1', name: 'Bob Robson'},
    auditorAssignee: {id: 'A2', name: 'Alice Miller'},
    clientAssignee: {id: 'C1', name: 'Florance Nightly'},
    complete: true
  },
  {
    name: 'General Ledger',
    description: 'Gen Ledger for Q1-Q4 2016.',
    category: 'Controls',
    dueDate: '04/30/18',
    comments: [
        {content: 'A3', sender: {id: 'A1', name: 'Fred Perry'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C1', sender: {id: 'C1', name: 'Florance Nightly'}, files: 'ObjectId', dateCreated: 'Date', status: 'OK'}
    ],
    requests: [
        { name: 'General Ledger', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: '', dateCreated: '', dateUploaded: '' },
        { name: 'General Ledger', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: true, dateCreated: '', dateUploaded: '' }
    ],
    createdBy: {id: 'A1', name: 'Bob Robson'},
    auditorAssignee: {id: 'A1', name: 'Fred Perry'},
    clientAssignee: {id: 'C1', name: 'Florance Nightly'},
    complete: false
  },
  {
    name: 'Business Cycle Controls',
    description: 'Description goes here.',
    category: 'Controls',
    dueDate: '04/28/18',
    comments: [
        {content: 'A4', sender: {id: 'A4', name: 'Shelly O’Neil'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C2', sender: {id: 'C2', name: 'Bruce Smith'}, files: 'ObjectId', dateCreated: 'Date', status: 'OK'}
    ],
    requests: [
        { name: 'Business', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: true, dateCreated: '', dateUploaded: '' },
        { name: 'Cycle', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: false, dateCreated: '', dateUploaded: '' },
        { name: 'Controls', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: '', dateCreated: '', dateUploaded: '' }
    ],
    createdBy: {id: 'A1', name: 'Bob Robson'},
    auditorAssignee: {id: 'A4', name: 'Shelly O’Neil'},
    clientAssignee: {id: 'C2', name: 'Bruce Smith'},
    complete: false
  },
  {
    name: 'Fraud and Error',
    description: 'Description goes here.',
    category: 'Financials',
    dueDate: '03/30/18',
    comments: [
        {content: 'A1', sender: {id: 'A1', name: 'Bob Robson'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C2', sender: {id: 'C2', name: 'Bruce Smith'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C3', sender: {id: 'C3', name: 'Sally Baggot'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'A3', sender: {id: 'A1', name: 'Fred Perry'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'}
    ],
    requests: [
        { name: 'Q1', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: true, dateCreated: '', dateUploaded: '' },
        { name: 'Q2', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: '', dateCreated: '', dateUploaded: '' },
        { name: 'Q3', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: false, dateCreated: '', dateUploaded: '' },
        { name: 'Q4', currentFile: '', prevFile: [ 'ObjectId(file._id)' ], viewed: '', dateCreated: '', dateUploaded: '' },
        { name: 'Summary', currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: false, dateCreated: '', dateUploaded: '' }
    ],
    createdBy: {id: 'A1', name: 'Bob Robson'},
    auditorAssignee: {id: 'A4', name: 'Shelly O’Neil'},
    clientAssignee: {id: 'C1', name: 'Florance Nightly'},
    complete: false
  },
  {
    name: 'Description of business',
    description: 'Description goes here.',
    category: 'Controls',
    dueDate: '04/20/18',
    comments: [
        {content: 'A1', sender: {id: 'A1', name: 'Bob Robson'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C2', sender: {id: 'C2', name: 'Bruce Smith'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C1', sender: {id: 'C1', name: 'Florance Nightly'}, files: 'ObjectId', dateCreated: 'Date', status: 'OK'},
        {content: 'C3', sender: {id: 'C3', name: 'Sally Baggot'}, files: 'ObjectId', dateCreated: 'Date', status: 'OK'}
    ],
    requests: [
        { name: String, currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: true, dateCreated: '', dateUploaded: '' }
    ],
    createdBy: {id: 'A1', name: 'Bob Robson'},
    auditorAssignee: {id: 'A4', name: 'Shelly O’Neil'},
    clientAssignee: {id: 'C3', name: 'Sally Baggot'},
    complete: false
  },
  {
    name: 'Overview of operations',
    description: 'Description goes here.',
    category: 'Controls',
    dueDate: '04/20/18',
    comments: [
        {content: 'A1', sender: {id: 'A1', name: 'Bob Robson'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C3', sender: {id: 'C3', name: 'Sally Baggot'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'C1', sender: {id: 'C1', name: 'Florance Nightly'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'},
        {content: 'A1', sender: {id: 'A1', name: 'Bob Robson'}, files: 'ObjectId', dateCreated: 'Date', status: 'READ'}
    ],
    requests: [
        { name: String, currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: true, dateCreated: '', dateUploaded: '' },
        { name: String, currentFile: {id: 'file-id-ABC', name: 'File Name is here'}, prevFile: [ 'ObjectId(file._id)' ], viewed: false, dateCreated: '', dateUploaded: '' }
    ],
    createdBy: {id: 'A1', name: 'Bob Robson'},
    auditorAssignee: {id: 'A5', name: 'Frank Sinat'},
    clientAssignee: {id: 'C3', name: 'Sally Baggot'},
    complete: false
  }
]
const engagementTest1 = {
  '_id': '58f62f1226415d20006e0402',
  'status': 'ABANDONED',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementTest2 = {
  '_id': '58f62f1226415d20006e04021',
  'status': 'ABANDONED',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementTest3 = {
  '_id': '58f62f1226415d20006e04023',
  'status': 'ACTIVE',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementTest4 = {
  '_id': '58f62f1226415d20006e04024',
  'status': 'ACTIVE',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementTest5 = {
  '_id': '58f62f1226415d20006e04025',
  'status': 'ABANDONED',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementTest6 = {
  '_id': '58f62f1226415d20006e04026',
  'status': 'ABANDONED',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementTest7 = {
  '_id': '58f62f1226415d20006e04027',
  'status': 'ARCHIVED',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementTest8 = {
  '_id': '58f62f1226415d20006e040218',
  'status': 'PLANNING',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementTest9 = {
  '_id': '58f62f1226415d20006e040239',
  'status': 'PLANNING',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementTest10 = {
  '_id': '58f62f1226415d20006e0402410',
  'status': 'PLANNING',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementTest11 = {
  '_id': '58f62f1226415d20006e0402511',
  'status': 'ABANDONED',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementTest12 = {
  '_id': '58f62f1226415d20006e0402612',
  'status': 'COMPLETED',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementTest13 = {
  '_id': '58f62f1226415d20006e04013',
  'status': 'COMPLETED',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementTest14 = {
  '_id': '58f62f1226415d20006e0402114',
  'status': 'COMPLETED',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementTest15 = {
  '_id': '58f62f1226415d20006e0402315',
  'status': 'ABANDONED',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementTest16 = {
  '_id': '58f62f1226415d20006e0402416',
  'status': 'ABANDONED',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementTest17 = {
  '_id': '58f62f1226415d20006e0402517',
  'status': 'ABANDONED',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementTest18 = {
  '_id': '58f62f1226415d20006e0402618',
  'status': 'ABANDONED',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementTest19 = {
  '_id': '58f62f1226415d20006e0402718',
  'status': 'ABANDONED',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementTest20 = {
  '_id': '58f62f1226415d20006e04021820',
  'status': 'ABANDONED',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementTest21 = {
  '_id': '58f62f1226415d20006e04023921',
  'status': 'ABANDONED',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementTest22 = {
  '_id': '58f62f1226415d20006e040241022',
  'status': 'ABANDONED',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementTest23 = {
  '_id': '58f62f1226415d20006e04025123',
  'status': 'ABANDONED',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementTest24 = {
  '_id': '58f62f1226415d20006e0402624',
  'status': 'ABANDONED',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagement = {
  '_id': '58f62f1226415d20006e0402',
  'status': 'ACTIVE',
  'type': 'Compilations',
  '__v': 0,
  'dueDate': null,
  'dateCompleted': null,
  'lastUpdated': null,
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'The Flower Shop 2017',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}
const engagementA = {
  '_id': '58f62f1226415d20006e0402',
  'status': 'ACTIVE',
  'type': 'Audits',
  '__v': 0,
  'dueDate': '12/09/2017',
  'dateCompleted': null,
  'lastUpdated': '12/09/2017',
  'dateCreated': '2017-04-18T15:21:54.702Z',
  'categories': [
      {name: 'Admin', color: '#5C9BA0'},
      {name: 'Financials', color: '#757575'},
      {name: 'Controls', color: '#2B4876'},
      {name: 'Select', color: '#000'}
  ],
  'files': [],
  'todos': todo,
  'firm': {
    '_id': '58f62e1426415d20006e03ff',
    'name': 'Firm ABC',
    'creater': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    },
    'currentOwner': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Doe',
      'firstName': 'John'
    }
  },
  'business': {
    '_id': '58f62e1426415d20006e03fe',
    'name': 'compABC',
    'keyContact': {
      '_id': '58f62e1426415d20006e03fe',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  },
  'name': 'Luna Cafe Compilation',
  'acl': [{
    'id': '58f62e1426415d20006e03fe',
    'role': 'AUDITOR',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'auditor@email.com',
      'type': 'AUDITOR',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'John',
      'firstName': 'Doe'
    }
  }, {
    'id': '58f62e1426415d20006e03fe',
    'role': 'CLIENT',
    'status': 'ACTIVE',
    'lead': true,
    'userInfo': {
      '_id': '58f62e1426415d20006e03fe',
      'status': 'ACTIVE',
      'email': 'client@email.com',
      'type': 'CLIENT',
      '__v': 1,
      'verified': true,
      'lastLogin': '2017-04-18T17:20:21.795Z',
      'howdid': '',
      'jobTitle': 'Senior Auditor',
      'lastName': 'Smith',
      'firstName': 'Trevor'
    }
  }]
}

const myEngagements = []
myEngagements.push(engagementTest1)
myEngagements.push(engagementTest2)
myEngagements.push(engagementTest3)
myEngagements.push(engagementTest4)
myEngagements.push(engagementTest5)
myEngagements.push(engagementTest6)
myEngagements.push(engagementTest7)
myEngagements.push(engagementTest8)
myEngagements.push(engagementTest9)
myEngagements.push(engagementTest10)
myEngagements.push(engagementTest11)
myEngagements.push(engagementTest12)
myEngagements.push(engagementTest13)
myEngagements.push(engagementTest14)
myEngagements.push(engagementTest15)
myEngagements.push(engagementTest16)
myEngagements.push(engagementTest17)
myEngagements.push(engagementTest18)
myEngagements.push(engagementTest19)
myEngagements.push(engagementTest20)
myEngagements.push(engagementTest21)
myEngagements.push(engagementTest22)
myEngagements.push(engagementTest23)
myEngagements.push(engagementTest24)

myEngagements.push(engagement)
myEngagements.push(engagementA)

module.exports = myEngagements
