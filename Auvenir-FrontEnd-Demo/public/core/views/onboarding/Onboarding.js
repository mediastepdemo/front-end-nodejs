'use strict'

import ViewTemplate from '../ViewTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'

import Component_Welcome from '../../components/welcome/welcome'
import Component_Personal from '../../components/personal/personal'
import Component_Security from '../../components/security/security'
import Component_Firm from '../../components/firm/firm'
import Component_Business from '../../components/business/business'
// TODO uncomment when intgrations are in place
// import Component_Accounting from '../../components/accounting/accounting'
import Component_Bank from '../../components/bank/bank'
import Component_Files from '../../components/files/files'
import styles from './onboarding.css'

/**
 *  Onboarding Module
 *  This Module is the controller for all onboarding pages. Every user must go through here until they are set to ACTIVE.
 *
 *  @param {object} ctrl - Our main application Controller.
 *  @param {object} settings - Any setting that can be added. Currently not in use.
 */

var Onboarding = function (ctrl, settings) {
  ViewTemplate.call(this, ctrl, settings)

  var self = this
  var name = this.name = 'Onboarding'
  var c = ctrl

  var currentStep = 0
  var currentUserAcl = c.currentUser.type
  let skipOnboard = c.currentUser.skipOnboard
  let onboardInfoReq = c.currentUser.skipOnboarding // <- should do the full onboard
  let passwordSetReq = c.currentUser.passwordSetRequired // <- user needs to set password
  // if `onboardInfoRequired` // then `passwordSetRequired` will be set
  let passwordReset = c.currentUser.passwordResetRequired // <- user has forgotten password and should be prompted for a new password
  var currentComponent = null
  var componentArray = []
  var mainHeader = 'header-container'

  var components = {
    welcome: { linkID: 'link-onboarding-welcome', obj: null, displayID: 'onboarding-welcome-container', title: 'WELCOME', step: null, complete: false, pageHeight: '886px' },
    personal: { linkID: 'link-onboarding-personal', obj: null, displayID: 'onboarding-personal-container', title: 'PERSONAL', step: null, complete: false, pageHeight: '1200px' },
    firm: { linkID: 'link-onboarding-firm', obj: null, displayID: 'onboarding-firm-container', title: 'FIRM', step: null, complete: false, pageHeight: '1600px' },
    business: { linkID: 'link-onboarding-business', obj: null, displayID: 'onboarding-business-container', title: 'BUSINESS', step: null, complete: false, pageHeight: '1230px' },
    // TODO uncomment when integrations are in place
    // accounting: { linkID: 'link-onboarding-accounting', obj: null, displayID: 'onboarding-accounting-container', title: 'ACCOUNTING', step: null, complete: false },
    bank: { linkID: 'link-onboarding-bank', obj: null, displayID: 'onboarding-bank-container', title: 'BANK', step: null, complete: false, pageHeight: '1072px' },
    files: { linkID: 'link-onboarding-files', obj: null, displayID: 'onboarding-files-container', title: 'FILES', step: null, complete: false, pageHeight: '820px' },
    security: { linkID: 'link-onboarding-security', obj: null, displayID: 'onboarding-security-container', title: 'SECURITY', step: null, complete: false, pageHeight: '830px' }
  }

  var componentContainer = 'onboarding-component-container'

  /**
   *  Builds page and components
   */
  var initialize = function () {
    self.build()
    buildComponents()
    // this enables scrollbar
    document.getElementById(self._moduleID).style.height = '100%'
    document.getElementById(self._moduleID).style.position = 'relative'
  }

  var viewNextComponent = this.viewNextComponent = function () {
    $('body').scrollTop(0)
    var nextStep = currentStep + 1
    viewComponent(nextStep)
  }

  /**
   *  Builds components, and component containers based on users Acl.
   */
  var buildComponents = function () {
    var componentSystem = document.getElementById(componentContainer)

    if (currentUserAcl) {
      var welcomeComponent = null
      var personalComponent = null
      var firmComponent = null
      var businessComponent = null
      // TODO uncomment when integrations are in place.
      // var accountingComponent = null
      var bankComponent = null
      var filesComponent = null
      var securityComponent = null

      if (skipOnboard) {
        securityComponent = Forge.build({'dom': 'div', 'id': components.security.displayID, 'class': 'onboarding-individualContainer'})
        componentSystem.appendChild(securityComponent)
        components.security.obj = new Component_Security(c, securityComponent, this)
        components.security.step = 0
        componentArray.push(components.security)
      } else if (currentUserAcl === 'ADMIN') {
        personalComponent = Forge.build({'dom': 'div', 'id': components.personal.displayID, 'class': 'onboarding-individualContainer'})
        securityComponent = Forge.build({'dom': 'div', 'id': components.security.displayID, 'class': 'onboarding-individualContainer'})

        componentSystem.appendChild(personalComponent)
        componentSystem.appendChild(securityComponent)

        components.personal.obj = new Component_Personal(c, personalComponent, this)
        components.security.obj = new Component_Security(c, securityComponent, this)
        components.personal.step = 0
        components.security.step = 1

        components.security.obj.on('accept-agreement', function () {
          self.trigger('accept-agreement')
        })
        components.security.obj.on('display-term', function () {
          c.displayTermsModal()
        })

        componentArray.push(components.personal, components.security)
        document.getElementById(self._moduleID).style.minHeight = '920px'
      } else if (currentUserAcl === 'AUDITOR') {
        personalComponent = Forge.build({'dom': 'div', 'id': components.personal.displayID, 'class': 'onboarding-individualContainer'})
        firmComponent = Forge.build({'dom': 'div', 'id': components.firm.displayID, 'class': 'onboarding-individualContainer'})
        securityComponent = Forge.build({'dom': 'div', 'id': components.security.displayID, 'class': 'onboarding-individualContainer'})

        let badgesContainer = Forge.build({'dom': 'div', 'class': 'onboarding-badgesContainer'})
        let comodoLink = Forge.build({'dom': 'a', 'href': 'https://ssl.comodo.com/', 'target': '_blank'})
        let mcafeeLink = Forge.build({'dom': 'a', 'href': 'https://www.mcafeesecure.com/', 'target': '_blank'})
        let comodoImg = Forge.build({'dom': 'img', 'src': 'images/onboarding/comodo.png', 'class': 'badgeImages'})
        let mcafeeImg = Forge.build({'dom': 'img', 'src': 'images/onboarding/mcafee.svg', 'class': 'badgeImages', 'style': 'margin-top: 10px'})
        comodoLink.appendChild(comodoImg)
        mcafeeLink.appendChild(mcafeeImg)
        badgesContainer.appendChild(comodoLink)
        badgesContainer.appendChild(mcafeeLink)

        componentSystem.appendChild(personalComponent)
        componentSystem.appendChild(firmComponent)
        componentSystem.appendChild(securityComponent)
        document.getElementById('OnboardingPage').appendChild(badgesContainer)

        components.personal.obj = new Component_Personal(c, personalComponent, this)
        components.firm.obj = new Component_Firm(c, firmComponent, this)
        components.security.obj = new Component_Security(c, securityComponent, this)

        components.personal.step = 0
        components.firm.step = 1
        components.security.step = 2

        componentArray.push(components.personal, components.firm, components.security)
        document.getElementById(self._moduleID).style.minHeight = '1200px'
      } else if (currentUserAcl === 'CLIENT') {
        personalComponent = Forge.build({'dom': 'div', 'id': components.personal.displayID, 'class': 'onboarding-individualContainer'})
        businessComponent = Forge.build({'dom': 'div', 'id': components.business.displayID, 'class': 'onboarding-individualContainer'})
        // TODO uncomment when integrations are in place
        // accountingComponent = Forge.build({'dom': 'div', 'id': components.accounting.displayID, 'class': 'onboarding-individualContainer'})
        bankComponent = Forge.build({'dom': 'div', 'id': components.bank.displayID, 'class': 'onboarding-individualContainer'})
        filesComponent = Forge.build({'dom': 'div', 'id': components.files.displayID, 'class': 'onboarding-individualContainer'})
        securityComponent = Forge.build({'dom': 'div', 'id': components.security.displayID, 'class': 'onboarding-individualContainer'})

        let badgesContainer = Forge.build({'dom': 'div', 'class': 'onboarding-badgesContainer'})
        let comodoLink = Forge.build({'dom': 'a', 'href': 'https://ssl.comodo.com/', 'target': '_blank'})
        let mcafeeLink = Forge.build({'dom': 'a', 'href': 'https://www.mcafeesecure.com/', 'target': '_blank'})
        let comodoImg = Forge.build({'dom': 'img', 'src': 'images/onboarding/comodo.png', 'class': 'badgeImages'})
        let mcafeeImg = Forge.build({'dom': 'img', 'src': 'images/onboarding/mcafee.svg', 'class': 'badgeImages', 'style': 'margin-top: 10px'})
        comodoLink.appendChild(comodoImg)
        mcafeeLink.appendChild(mcafeeImg)
        badgesContainer.appendChild(comodoLink)
        badgesContainer.appendChild(mcafeeLink)

        componentSystem.appendChild(personalComponent)
        componentSystem.appendChild(businessComponent)
        // TODO uncomment when integrations are in place
        // componentSystem.appendChild(accountingComponent)
        componentSystem.appendChild(bankComponent)
        componentSystem.appendChild(filesComponent)
        componentSystem.appendChild(securityComponent)

        components.personal.obj = new Component_Personal(c, personalComponent, this)
        components.business.obj = new Component_Business(c, businessComponent, this)
        // TODO uncomment when integrations are in place
        // components.accounting.obj = new Component_Accounting(c, accountingComponent, this)
        components.bank.obj = new Component_Bank(c, bankComponent, this)
        components.files.obj = new Component_Files(c, filesComponent, this)
        components.security.obj = new Component_Security(c, securityComponent, this)

        components.personal.step = 0
        components.business.step = 1
        // TODO uncomment when integrations are in place and modify the following components step number
        // components.accounting.step = 2
        components.bank.step = 2
        components.files.step = 3
        components.security.step = 4

        // TODO - setup Listeners for all components (please add as you build component)
        components.business.obj.setupListeners()
        components.files.obj.setupListeners()

        components.files.obj.on('add-integration', function (data) {
          c.sendEvent({ name: 'add-integration', data: data })
        })
        components.files.obj.on('create-activity', function (data) {
          c.createActivity(data)
        })
        components.files.obj.on('upload-file', function (data) {
          c.uploadFile(data)
        })
        components.files.obj.on('flash', function (option) {
          self.trigger('flash', option)
        })
        components.security.obj.on('accept-agreement', function () {
          self.trigger('accept-agreement')
        })

        // updating the array of components to display
        // TODO re-insert 'components.accounting, components.bank,' when integrations are in place.
        componentArray.push(components.personal, components.business, components.bank, components.files, components.security)

        // Start with the Welcome Component
        welcomeComponent = Forge.build({'dom': 'div', 'id': components.welcome.displayID, 'class': 'onboarding-systemContainer'})
        components.welcome.obj = new Component_Welcome(c, welcomeComponent, this)
        document.getElementById('OnboardingPage').appendChild(welcomeComponent)
        document.getElementById('OnboardingPage').appendChild(badgesContainer)
        document.getElementById('onboarding-systemContainer').style.display = 'none'

        components.welcome.obj.on('move-to-next', function () {
          document.getElementById('onboarding-welcome-container').style.display = 'none'
          document.getElementById('onboarding-systemContainer').style.display = 'block'
          document.getElementById(self._moduleID).style.minHeight = '950px'
        })
        document.getElementById(self._moduleID).style.minHeight = '886px'
      }

      self.buildNavigation()
    }
  }

  /**
   * Builds the page, including the containers for the navigation system and Components
   * As well as the continue button which calls the View Component function.
   */
  this.buildPage = function () {
    log('Onboarding: Building page')

    var parentContainer = Forge.build({'dom': 'div', 'id': 'OnboardingPage', class: 'parent-of-footer'})
    var systemContainer = Forge.build({'dom': 'div', 'class': 'onboarding-systemContainer', 'id': 'onboarding-systemContainer'})
    var navSystem = Forge.build({'dom': 'div', 'id': 'm-o-navSystem', 'class': 'onboarding-navSystem'})
    var componentSystem = Forge.build({'dom': 'div', 'id': componentContainer, 'class': 'onboarding-componentContainer'})

    systemContainer.appendChild(navSystem)
    systemContainer.appendChild(componentSystem)
    parentContainer.appendChild(systemContainer)

    var footerComponent = Forge.build({dom: 'div', id: 'onboarding-footer'})
    footerComponent.innerHTML = c.footer
    parentContainer.appendChild(footerComponent)
    return parentContainer
  }

  /**
   * Changes the front end UI for the navigation system, and hides and shows the appropriate component.
   * If its toggling past the security page, it skips to the Dashboard.
   *
   * @param {object} name - The name of the component to show.
   */

  var viewComponent = function (n) {
    document.getElementById(currentComponent.linkID + 'circle').classList.toggle('onboarding-numberCircle-active')
    document.getElementById(currentComponent.linkID + 'title').classList.toggle('onboarding-navLink-active')

    if (n < currentComponent.step) {
      for (let i = n; i < componentArray.length; i++) {
        componentArray[i].complete = false
        document.getElementById(componentArray[i].linkID + 'num').innerText = componentArray[i].step + 1
        document.getElementById(componentArray[i].linkID + 'num').classList.remove('auvicon-checkmark', 'onboarding-nav-checkmark')
        document.getElementById(componentArray[i].linkID + 'circle').style.cursor = 'default'
      }
    } else {
      document.getElementById(currentComponent.linkID + 'num').innerText = ''
      document.getElementById(currentComponent.linkID + 'num').classList.add('auvicon-checkmark', 'onboarding-nav-checkmark')
      document.getElementById(currentComponent.linkID + 'circle').style.cursor = 'pointer'
    }

    document.getElementById(currentComponent.displayID).style.display = 'none'
    currentStep = n
    currentComponent = componentArray[n]
    document.getElementById(currentComponent.displayID).style.display = 'inherit'
    document.getElementById(currentComponent.linkID + 'circle').classList.toggle('onboarding-numberCircle-active')
    document.getElementById(currentComponent.linkID + 'title').classList.toggle('onboarding-navLink-active')
    document.getElementById(self._moduleID).style.minHeight = currentComponent.pageHeight
  }

  /**
   * Checks to see what components have been built and adds them to their parents. Add's their link number to their step. Add's each component to the array of components.
   * Build the navigation Links based on the components. Then finally sets it to display the personal component as the first component.
  */
  this.buildNavigation = function () {
    log('Loading Components into Onboarding')

    // Loop through components to see if they should be built.
    for (var i = 0; i < componentArray.length; i++) {
      var navSystem = document.getElementById('m-o-navSystem')
      var navSystemLinkHolder = Forge.build({'dom': 'div', 'id': componentArray[i].linkID, 'class': 'onboarding-navLinkHolder'})
      var navSystemLink = Forge.build({'dom': 'span', 'id': componentArray[i].linkID + 'title', 'text': componentArray[i].title, 'class': 'onboarding-navLinkTxt'})

      var navNumberCircle = Forge.build({'dom': 'div', 'id': componentArray[i].linkID + 'circle', 'class': 'onboarding-numberCircle'})
      var navNumber = Forge.build({'dom': 'span', 'text': i + 1, 'class': 'onboarding-number', 'id': componentArray[i].linkID + 'num'})

      navNumberCircle.addEventListener('click', (e) => {
        e.stopPropagation()
        if (e.target.id === components.personal.linkID + 'circle' || e.target.id === components.personal.linkID + 'num') {
          if (componentArray[components.personal.step].complete || currentStep > components.personal.step) {
            viewComponent(components.personal.step)
          }
        } else if (e.target.id === components.business.linkID + 'circle' || e.target.id === components.business.linkID + 'num') {
          if (componentArray[components.business.step].complete || currentStep > components.business.step) {
            viewComponent(components.business.step)
          }
        } else if (e.target.id === components.firm.linkID + 'circle' || e.target.id === components.firm.linkID + 'num') {
          if (componentArray[components.firm.step].complete || currentStep > components.firm.step) {
            viewComponent(components.firm.step)
          }
        } else if (e.target.id === components.bank.linkID + 'circle' || e.target.id === components.bank.linkID + 'num') {
          if (componentArray[components.bank.step].complete || currentStep > components.bank.step) {
            viewComponent(components.bank.step)
          }
        } else if (e.target.id === components.files.linkID + 'circle' || e.target.id === components.files.linkID + 'num') {
          if (currentStep > components.files.step) {
            viewComponent(components.files.step)
          }
        }
      })

      navNumberCircle.appendChild(navNumber)

      navSystemLinkHolder.appendChild(navSystemLink)
      navSystemLinkHolder.appendChild(navNumberCircle)

      if (i > 0) {
        var navLinkHr = Forge.build({'dom': 'hr', 'class': 'onboarding-linkHr'})
        navSystemLinkHolder.appendChild(navLinkHr)
      }

      navSystem.appendChild(navSystemLinkHolder)
    }

    currentStep = 0
    currentComponent = componentArray[currentStep]
    document.getElementById(currentComponent.displayID).style.display = 'inherit'
    document.getElementById(currentComponent.linkID + 'circle').classList.toggle('onboarding-numberCircle-active')
    document.getElementById(currentComponent.linkID + 'title').classList.toggle('onboarding-navLink-active')
  }

  var setupPhoneValidation = this.setupPhoneValidation = function (elementId) {
    document.getElementById(elementId).addEventListener('keydown', function (e) {
      var key = e.keyCode ? e.keyCode : e.which
      if (!([8, 9, 13, 27, 46, 110, 190].indexOf(key) !== -1 ||
         (key === 65 && (e.ctrlKey || e.metaKey)) ||
         (key >= 35 && key <= 40) ||
         (key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) ||
         (key >= 96 && key <= 105)
       )) e.preventDefault()
    })
  }
  /**
   * Sets up the listeners
   */
  this.setupListeners = function () {
    log('Dashboard: Setting up listeners ...')
    c.registerForEvent('onboarding-complete', 'ONBOARDING-0000', onboardingCompleteResponse)

    $('#personal-role').focusout(function () {
      if ($(this).val() === c.lang.personalComponent.roles.other) {
        $(this).val(c.lang.pleaseType)
      }
      if ($(this).val() === c.lang.pleaseType) {
        $(this).focus()
        $(this).select()
      }
    })
    $('#personal-role').click(function () {
      if ($(this).val() === c.lang.pleaseType) {
        $(this).val(null)
      }
    })

    $('#personal-referral').focusout(function () {
      if ($(this).val() === c.lang.personalComponent.other) {
        $(this).val(c.lang.pleaseType)
      }
      if ($(this).val() === c.lang.pleaseType) {
        $(this).focus()
        $(this).select()
      }
    })

    $('#personal-referral').click(function () {
      if ($(this).val() === c.lang.pleaseType) {
        $(this).val(null)
      }
    })

    // Listens for button click from component and moves to next function.
    if (components.personal.obj !== null) {
      setupPhoneValidation('personal-phoneNumber')
      components.personal.obj.on('move-to-next', function () {
        setupPhoneValidation('personal-phoneNumber')
        components.personal.complete = true
        viewNextComponent()
      })
    }
    if (components.business.obj !== null) {
      components.business.obj.on('move-to-next', function () {
        components.business.complete = true
        viewNextComponent()
      })
    }
    // TODO uncomment when integrations are in place
    // if(components.accounting.obj !== null){
    //   components.accounting.obj.on('move-to-next', function() {
    //     viewNextComponent();
    //   });
    // };
    if (components.firm.obj !== null) {
      setupPhoneValidation('firm-phone')
      components.firm.obj.on('move-to-next', function () {
        components.firm.complete = true
        viewNextComponent()
      })
    }
    if (components.files.obj !== null) {
      components.files.obj.on('move-to-next', function () {
        components.files.complete = true
        viewNextComponent()
      })
    }

    if (components.bank.obj !== null) {
      components.bank.obj.on('move-to-next', function () {
        viewNextComponent()
      })
      components.bank.obj.on('open-finicity', function () {
        c.displayBankIntegrationModal()
      })
    }

    if (components.security.obj !== null) {
      components.security.obj.on('move-to-next', function () {
        components.security.complete = true
        self.trigger('accept-agreement')
      })
    }
  }

  /**
   * Function that responds to Onboarding Complete response from Socket.
   */
  var onboardingCompleteResponse = function (tag, data) {
    log('******** ONBOARD-COMPLETE Response ********')
    document.getElementById('progressOverlay').style.display = 'none'
    if (data.code !== 0) {
      c.displayMessage(data.msg)
    } else {
      self.trigger('onboarding-complete', null, data)
    }
  }

  this.loadData = function () {
    if (components.personal.obj !== null) {
      components.personal.obj.loadUserData()
    }
    if (components.business.obj !== null) {
      components.business.obj.loadUserData()
    }
    // TODO uncomment when integrations are in place
    // if (components.accounting.obj !== null) {
    //   components.accounting.obj.loadUserData();
    // }
    if (components.firm.obj !== null) {
      components.firm.obj.loadUserData()
    }
  }

  /* Sends the newly integrated accounts to the Bank Component */
  this.addAccounts = function (data, bankName) {
    components.bank.obj.addAccounts(data, bankName)
  }
  /* Displays The Accounts */
  this.showAccounts = function () {
    components.bank.obj.showAccounts()
  }

  initialize()

  return this
}

Onboarding.prototype = Object.create(ViewTemplate.prototype)
Onboarding.prototype.constructor = Onboarding

module.exports = Onboarding
