'use strict'

import ViewTemplate from '../ViewTemplate'
import Utility from '../../../js/src/utility_c'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import styles from './header.css'

var Header = function (ctrl, settings) {
  ViewTemplate.call(this, ctrl, settings)

  var moduleName = this.name = 'Header'
  var c = ctrl
  var self = this

  settings = {
    back: { active: false },
    brand: { active: true },
    nav: { active: true },
    profile: { active: false },
    dashboard: { active: true},
    // files: { active: true},
    height: '70px'
  }

  var selectedItem = null
  var ddlItems = []

  var headerID = 'header-container'
  var contentContainerID = 'header-content'
  var logoID = 'h-f-brand'
  var navID = 'h-f-navigation'
  var engagementLinkID = 'h-engagementsLink'
  var clientListLinkID = 'h-clientListLink'
  var ddlProfile_ID = 'h-ddl-prf'
  var ddlDropdown_ID = 'h-ddl-dropdownMenu'
  var ddlProfile_title = 'dashboardUsername'

  var mainHeader = 'header-content'

  var ddlProfile_title_sub = 'dropdownUsername'

  var current_active_link

  var name = { firstName: '', lastName: '', email: ''}

  var initialize = function () {
    log('Header: initializing!@#!124')

    self.build()
    registerEvents()

    c.registerForEvent('update-user', 'header-00000001', updatedUser)
  }

  this.buildPage = function () {
    log('Header: building')

    var parentContainer = Forge.build({'dom': 'div', 'id': 'header-container'})
    var headerContent = Forge.build({'dom': 'div', 'id': 'header-content', 'style': 'margin:auto'})

    /** Back Arrow **/
    // var backDiv = Forge.build({'dom': 'div', 'id': 'h-f-back', 'class': 'pull-left', 'style': 'margin: 30px 11px 0px 20px;font-size: 25px;cursor:pointer'})
    // var backIcon = Forge.build({'dom': 'i', 'class': 'fa fa-share fa-flip-horizontal'})
    // backDiv.appendChild(backIcon)
    // headerContent.appendChild(backDiv)

    /** Brand **/
    var brandDiv = Forge.build({'dom': 'div', 'id': 'h-f-brand', 'class': 'header-brand pull-left'})
    var brandImg = Forge.build({'dom': 'img', id: 'header-blue-logo', 'src': 'images/logos/auvenir/auvenir.svg', 'class': 'header-auvenirLogo'})
    brandDiv.appendChild(brandImg)
    headerContent.appendChild(brandDiv)

    /** Navigation **/
    var navDiv = Forge.build({'dom': 'div', 'id': 'h-f-navigation', 'class': 'pull-left', 'style': 'margin: 25px 20px 0px 22px; display:inherit;'})
    // var navDashboard = Forge.build({'dom': 'span', 'id': dashboardLinkID, 'class': 'header-menu', 'text': 'Engagements', 'style': 'display:none'})
    // var navRequests = Forge.build({'dom': 'span', 'id': 'h-requestLink', 'class': 'header-menu', 'text': 'Requests', 'style': 'display:none'})
    // var navFiles = Forge.build({'dom': 'span', 'id': 'h-filesLink', 'class': 'header-menu', 'text': 'Files', 'style': 'display:none;'})
    var navEngagements = Forge.build({'dom': 'span', 'id': engagementLinkID, 'class': 'header-menu', 'text': 'Engagements', 'style': 'display:none;'})
    var navClientList = Forge.build({'dom': 'span', 'id': clientListLinkID, 'class': 'header-menu', 'text': 'Contacts', 'style': 'display:none;'})
    // navDiv.appendChild(navDashboard)
    current_active_link = engagementLinkID
    // navDashboard.classList.add('header-menu-active')

    // navDiv.appendChild(navRequests)
    // navDiv.appendChild(navFiles)
    navDiv.appendChild(navEngagements)
    navDiv.appendChild(navClientList)
    headerContent.appendChild(navDiv)

    /** Profile **/
    var profileDDL = Forge.build({'dom': 'div', 'id': 'h-ddl-prf', 'class': 'au-dropdownHolder', 'style': 'display:none;'})

    var profileDDLTrigger = Forge.build({'dom': 'div', 'class': 'au-dropdown-trigger'})
    var triggerSpan = Forge.build({'dom': 'span', 'id': 'dashboardUsername', 'class': 'header-userName', 'text': 'User Name'})
    var triggerIcon = Forge.build({'dom': 'i', 'class': 'auvicon-chevron-down', 'style': 'padding-left:7px; font-size:7px'})
    profileDDLTrigger.appendChild(triggerSpan)
    profileDDLTrigger.appendChild(triggerIcon)

    var profileDDLContainer = Forge.build({'dom': 'div', 'class': 'au-dropdown-container'})
    var connectorDiv = Forge.build({'dom': 'div', 'class': 'au-dropdown-hoverConnector'})
    var connectorIcon = Forge.build({'dom': 'i', 'class': 'fa fa-caret-up au-dropdown-caret', 'style': 'font-size:56px'})
    var connectorShadow = Forge.build({'dom': 'div', 'class': 'au-dropdown-shadow'})
    connectorDiv.appendChild(connectorIcon)
    connectorDiv.appendChild(connectorShadow)
    profileDDLContainer.appendChild(connectorDiv)

    var ddlContent = Forge.build({'dom': 'div', 'id': 'h-ddl-dropdown-menu', 'class': 'au-dropdown-menu'})

    var usernameContainer = Forge.build({'dom': 'div', 'id': 'dropdownUsernameHolder', 'class': 'au-dropdown-usernameHolder'})
    var usernameBoth = Forge.build({'dom': 'span', 'id': 'dropdownUsername', 'class': 'au-dropdown-username', 'text': 'User Name'})

    var profileItemList = Forge.build({'dom': 'div', 'id': 'au-dropdown-itemList', 'class': 'au-dropdown-subContainer'})
    var profileItem1 = Forge.build({'dom': 'div', 'id': 'h-ddl-item-settings', 'class': 'au-dropdown-linkContainer'})
    var item1Link = Forge.build({'dom': 'a', 'class': 'au-dropdownLink', 'text': 'Settings'})
    var profileItem2 = Forge.build({'dom': 'div', 'class': 'au-dropdown-linkContainer'})
    var item2Link = Forge.build({'dom': 'a', 'id': 'h-ddl-signOut', 'href': '/logout', 'class': 'au-dropdownLink', 'text': 'Sign Out'})
    profileItem2.addEventListener('click', function () { window.location.href = '/logout' })
    profileItemList.appendChild(profileItem1)
    profileItemList.appendChild(profileItem2)
    profileItem1.appendChild(item1Link)
    profileItem2.appendChild(item2Link)

    usernameContainer.appendChild(usernameBoth)
    ddlContent.appendChild(usernameContainer)
    ddlContent.appendChild(profileItemList)

    profileDDLContainer.appendChild(ddlContent)
    profileDDL.appendChild(profileDDLTrigger)
    profileDDL.appendChild(profileDDLContainer)
    headerContent.appendChild(profileDDL)
    parentContainer.appendChild(headerContent)

    return parentContainer
  }

  var registerEvents = function () {
    log('Header: registering events')

    // document.getElementById(backID).style.display = (settings.back.active) ? 'inherit' : 'none'
    document.getElementById(logoID).style.display = (settings.brand.active) ? 'inherit' : 'none'
    document.getElementById(navID).style.display = (settings.nav.active) ? 'inherit' : 'none'
    document.getElementById(ddlProfile_ID).style.display = (settings.profile.active) ? 'inherit' : 'none'
    document.getElementById(contentContainerID).style.width = settings.width
    document.getElementById(contentContainerID).style.height = settings.height

    document.getElementById(logoID).addEventListener('click', function () {
      self.trigger('header-logo-click')
      self.removeAuditorLogo()
    })

    document.getElementById('h-ddl-item-settings').addEventListener('click', function () {
      self.trigger('settings-click')
      self.removeAuditorLogo()
    })
    document.getElementById(clientListLinkID).addEventListener('click', function () {
      self.trigger('client-list-link-click', {linkID: clientListLinkID})
      self.removeAuditorLogo()
    })
    document.getElementById(engagementLinkID).addEventListener('click', function () {
      self.trigger('engagements-link-click', {linkID: engagementLinkID})
      self.removeAuditorLogo()
    })

    // Remove Sign Out from Tab Index (to keep it from showing the profile dropdown on tab)
    document.getElementById('h-ddl-signOut').tabIndex = -1
  }

  var updatedUser = function (tag, result) {
    log('Header: Response triggered for ' + tag)
    log(result)
    if (result) {
      setUsername(result.result.fields)
    }
  }

  var setUsername = this.setUsername = function (json) {
    if (json.firstName) { name.firstName = json.firstName }
    if (json.lastName) { name.lastName = json.lastName }
    if (json.email) { name.email = json.email }

    var userName = ''
    var userTitleName = ''

    if (name.firstName === '' && name.lastName !== '') {
      userName = name.lastName
      userTitleName = name.lastName
    } else if (name.firstName !== '' && name.lastName === '') {
      userName = name.firstName
      userTitleName = name.firstName
    } else if (name.firstName === '' && name.lastName === '') {
      userName = name.email
      userTitleName = name.email
    } else {
      userName = name.firstName + ' ' + name.lastName
      userTitleName = name.firstName
    }

    // TODO - add elipsers if name too long for minimal font-size

    document.getElementById(ddlProfile_title).innerHTML = userTitleName
    document.getElementById(ddlProfile_title_sub).innerHTML = userName

    /* SETTING FONT SIZE OF NAME BASED ON WIDTH */

    var adjustFontSize = function () {
      var element = document.getElementById('dropdownUsername')
      var elementLength = element.innerHTML.length
      var elementFontSize = element.style.fontSize

      if (elementLength > 34) {
        elementFontSize = '12px'
        element.innerHTML = userName.substring(0, 35) + '...'
      } else if (elementLength > 32) {
        elementFontSize = '12px'
      } else if (elementLength > 26) {
        elementFontSize = '14px'
      } else if (elementLength > 22) {
        elementFontSize = '16px'
      } else if (elementLength > 20) {
        elementFontSize = '18px'
      } else if (elementLength > 18) {
        elementFontSize = '20px'
      } else if (elementLength > 15) {
        elementFontSize = '22px'
      } else {
        elementFontSize = '24px'
      }

      document.getElementById('dropdownUsername').style.fontSize = elementFontSize
    }

    adjustFontSize()
  }

  this.getSettings = function () {
    return settings
  }

  // Set Current Link and change font color.
  this.setCurrentLink = function (linkID) {
    if (current_active_link !== linkID) {
      document.getElementById(current_active_link).classList.remove('header-menu-active')
      current_active_link = linkID
      document.getElementById(current_active_link).classList.add('header-menu-active')
    }
  }

  this.viewFixedMode = function (width) {
    document.getElementById('header-content').style.minWidth = '1028px'
    document.getElementById('header-content').style.maxWidth = '1440px'
    document.getElementById('header-content').style.padding = '0px'
    document.getElementById('h-ddl-prf').style.marginRight = ''
    document.getElementById('header-content').style.margin = 'auto'
    document.getElementById('h-f-brand').style.position = ''
    document.getElementById('h-f-brand').style.left = ''
    document.getElementById('h-f-brand').style.marginLeft = ''
    document.getElementById('header-container').style['z-index'] = '1000'
  }

  this.viewFullMode = function () {
    document.getElementById('header-content').style.width = '100%'
    document.getElementById('h-f-brand').style.paddingLeft = '16px'

    document.getElementById('h-ddl-prf').style.marginRight = ''
    document.getElementById('h-f-brand').style.paddingLeft = ''
    document.getElementById('h-f-brand').style.position = ''
    document.getElementById('h-f-brand').style.left = ''
    document.getElementById('h-f-brand').style.marginLeft = ''
  }

  this.viewOnboardingMode = function () {
    document.getElementById(ddlProfile_ID).style.display = 'none'
    document.getElementById('header-container').classList.add('header-onboard')
    document.getElementById('header-container').style.height = '227px'
    document.getElementById('header-container').style['z-index'] = '0'
    document.getElementById('header-content').style.minWidth = '1028px'
    document.getElementById('header-content').style.maxWidth = '1440px'
    document.getElementById('header-content').style.margin = '0px 0px 0px 40px'

    document.getElementById('h-f-brand').style.paddingLeft = '0px'
    document.getElementById('h-f-brand').style.position = 'absolute'
    document.getElementById('h-f-brand').style.left = '50%'
    document.getElementById('h-f-brand').style.marginLeft = '-67px'
    document.getElementById('h-ddl-prf').style.display = 'none'
  }

  this.exitOnboardingMode = function () {
    document.getElementById('header-container').style.height = '70px'
    document.getElementById('header-container').style.WebkitTransition = 'height 0.5s'
    document.getElementById('header-container').style.Transition = 'height 0.5s'
    document.getElementById('header-container').classList.remove('header-onboard')
  }

  this.toggleLogo = function () {
    var logoDom = document.getElementById(logoID)
    if (logoDom.style.display !== 'none') {
      logoDom.style.display = 'none'
    } else {
      logoDom.style.display = 'inherit'
    }
  }

  this.toggleColorLogo = function () {
    document.getElementById(logoID).children[0].src = 'images/logos/auvenir/logo_auvenir.svg'
  }

  this.showLogo = function () {
    document.getElementById(logoID).style.display = 'inherit'
  }

  this.hideLogo = function () {
    document.getElementById(logoID).style.display = 'none'
  }

  this.toggleNav = function () {
    var navDom = document.getElementById(navID)
    if (navDom.style.display !== 'none') {
      navDom.style.display = 'none'
    } else {
      navDom.style.display = 'inherit'
    }
  }

  this.showColorBackgroundHeader = function () {
    document.getElementById(headerID).classList.add('header-auvenir')
  }

  this.showWhiteBackgroundHeader = function () {
    document.getElementById(headerID).classList.add('header-auvenir-client')
  }

  this.showNav = function () {
    document.getElementById(navID).style.display = 'inherit'
  }

  this.hideNav = function () {
    document.getElementById(navID).style.display = 'none'
  }

  this.toggleEngagementsLink = function () {
    var dom = document.getElementById(engagementLinkID)
    if (dom.style.display !== 'none') {
      dom.style.display = 'none'
    } else {
      dom.style.display = 'inherit'
    }
  }

  this.showEngagementsLink = function () {
    document.getElementById(engagementLinkID).style.display = 'inline'
  }

  this.hideEngagementsLink = function () {
    document.getElementById(engagementLinkID).style.display = 'none'
  }

  this.toggleClientListLink = function () {
    var dom = document.getElementById(clientListLinkID)
    if (dom.style.display !== 'none') {
      dom.style.display = 'none'
    } else {
      dom.style.display = 'inherit'
    }
  }

  this.showClientListLink = function () {
    document.getElementById(clientListLinkID).style.display = 'inline'
  }

  this.hideClientListLink = function () {
    document.getElementById(clientListLinkID).style.display = 'none'
  }

  this.setClientNavigation = function () {
    document.getElementById(engagementLinkID).classList.add('header-menu-client')
    document.getElementById(clientListLinkID).classList.add('header-menu-client')
  }

  this.toggleProfileDDL = function () {
    var dom = document.getElementById(ddlProfile_ID)
    if (dom.style.display !== 'none') {
      dom.style.display = 'none'
    } else {
      dom.style.display = 'inherit'
    }
  }

  this.showProfileDDL = function () {
    document.getElementById(ddlProfile_ID).style.display = 'inherit'
  }

  /** Add Auditor Logo if it exists **/
  this.addAuditorLogo = function (firm) {
    if (firm.logo) {
      let logoImg = Forge.build({dom: 'img', src: Utility.getLogoFirm(firm.logo), class: 'header-firmLogo', id: 'header-auditorLogo'})
      let headerContent = document.getElementById('header-content')
      headerContent.insertBefore(logoImg, headerContent.firstChild)
      setTimeout(function () {
        $('#header-auditorLogo').addClass('header-firmLogoView')
      }, 100)
      document.getElementById('header-blue-logo').style.marginLeft = '25px'
    }
  }

  this.removeAuditorLogo = function () {
    if (document.getElementById('header-auditorLogo')) {
      $('#header-auditorLogo').remove()
      document.getElementById('header-blue-logo').style.marginLeft = '0px'
    }
  }

  this.setClientHeader = function () {
    self.showWhiteBackgroundHeader()
    self.setClientNavigation()
    self.toggleColorLogo()
    self.showNav()
    self.showProfileDDL()
    self.showEngagementsLink()
    self.showClientListLink()
  }

  this.setAdminHeader = function () {
    self.showColorBackgroundHeader()
    self.showProfileDDL()
  }

  this.setAuditorHeader = function () {
    self.showColorBackgroundHeader()
    self.showProfileDDL()
    self.showClientListLink()
    self.showEngagementsLink()
    self.showNav()
  }

  initialize()

  return this
}

Header.prototype = Object.create(ViewTemplate.prototype)
Header.prototype.constructor = Header

module.exports = Header
