'use strict'

import ViewTemplate from '../ViewTemplate'
import log from '../../../js/src/log'
import Forge from '../../Forge'
import styles from './settings.css'
import CardAuditorAccount from './cards/card-account/CardAuditorAccount'
import CardNoticfications from './cards/card-notifications/CardNotifications'

var AuditorSettings = function (ctrl, settings) {
  ViewTemplate.call(this, ctrl, settings)

  var self = this
  var name = this.name = 'AuditorSettings'
  var c = ctrl

  var currentCard = null
  var cards = {
    auditorAccount: {linkID: 'link-setting-account', obj: null, displayID: 'settings-auditorAccount-container' },
    notifications: {linkID: 'link-setting-notifications', obj: null, displayID: 'settings-notifications-container' }
  }

  var cardContainer = 'settings-cards-container'

  var initialize = function () {
    cards.auditorAccount.obj = new CardAuditorAccount(c, cards.auditorAccount.displayID, this)
    cards.notifications.obj = new CardNoticfications(c, cards.notifications.displayID, this)

    self.build()
    document.getElementById(self._moduleID).classList.add('scroll-settings')
  }

  this.buildPage = function () {
    log('AuditorSettings: Building page')

    var parentContainer = Forge.build({'dom': 'div', 'id': 'settingsPage', class: 'parent-of-footer'})
    var settingsDashboard = Forge.build({'dom': 'div', 'class': 'dashboard', 'style': 'display:inherit; margin-bottom: 0px; min-height: 880px', 'id': 'settingsDashboard'})

    var navSystemAuditor = Forge.build({'dom': 'div', 'id': 'm-s-navSystemAuditor-id', 'class': 'm-s-navSystemAuditor'})
    var navSystemAuditorHead = Forge.build({'dom': 'div', 'id': 'm-s-navSystemAuditorHead-id', 'text': 'Settings', 'class': 'm-s-navSystemAuditorHead'})
    var navSystemAuditorHeadLine = Forge.build({'dom': 'hr', 'class': 'm-s-navHr'})

    var navSystemLinkAuditor = Forge.build({'dom': 'div', 'id': cards.auditorAccount.linkID + '-auditor', 'text': 'Account', 'class': 'm-s-navLink m-s-navLink-active'})
    var navSystemAuditorNotifications = Forge.build({'dom': 'div', 'id': cards.notifications.linkID + '-auditor', 'text': 'Notifications', 'class': 'm-s-navLink'})

    var cardSystem = Forge.build({'dom': 'div', 'id': 'cardContainer', 'class': 'm-s-cardContainer'})
    var auditorAccountCard = Forge.build({'dom': 'div', 'id': cards.auditorAccount.displayID})
    var notificationsCard = Forge.build({'dom': 'div', 'id': cards.notifications.displayID, 'class': 'm-s-cardContainer', 'style': 'display:none;'})

    navSystemAuditor.appendChild(navSystemAuditorHead)
    navSystemAuditor.appendChild(navSystemAuditorHeadLine)
    navSystemAuditor.appendChild(navSystemLinkAuditor)
    navSystemAuditor.appendChild(navSystemAuditorNotifications)

    settingsDashboard.appendChild(navSystemAuditor)
    parentContainer.appendChild(settingsDashboard)

    navSystemLinkAuditor.addEventListener('click', function () {
      viewCard('auditorAccount', '-auditor')
    })

    navSystemAuditorNotifications.addEventListener('click', function () {
      viewCard('notifications', '-auditor')
    })

    cardSystem.appendChild(auditorAccountCard)
    cardSystem.appendChild(notificationsCard)

    settingsDashboard.appendChild(cardSystem)

    var footerComponent = Forge.build({dom: 'div', id: 'admin-settings-footer'})
    footerComponent.innerHTML = c.footer
    parentContainer.appendChild(footerComponent)

    return parentContainer
  }

  this.viewAccount = function () {
    viewCard('auditorAccount', '-auditor')
  }

  var viewCard = this.viewCard = function (name, option) {
    if (cards[name]) {
      if (currentCard.linkID !== cards[name].linkID) {
        if (!option) option = ''
        document.getElementById(currentCard.linkID + option).classList.toggle('m-s-navLink-active')
        currentCard.obj.hide()
        currentCard = cards[name]
        currentCard.obj.show()
        document.getElementById(currentCard.linkID + option).classList.toggle('m-s-navLink-active')
      }
    }
  }

  var addCard = function (parent, myCard) {
    var parentDom = document.getElementById(parent)
    parentDom.appendChild(myCard)
  }

  /**
   * Load one engagement in as the current engagement
   */
  this.loadDashboard = function () {
    // add cards
    addCard(cards.auditorAccount.displayID, cards.auditorAccount.obj.buildCard({width: '840px', background: 'white'}))
    addCard(cards.notifications.displayID, cards.notifications.obj.buildCard({ width: '840px', background: 'white'}))

    cards.notifications.obj.applySettings()
    cards.notifications.obj.loadData(c.currentUser.notifications)
    // build out account info
    var businessName = ''
    var firmName = ''
    var phone = ''
    var acl = ''
    var firmSize = ''
    var logo = ''

    if (c.currentFirm) {
      c.currentFirm.acl.forEach((element) => {
        if (element.id === c.currentUser._id) {
          acl = element
        }
      })

      firmName = (c.currentFirm.name) ? c.currentFirm.name : ''
      logo = (c.currentFirm.logo) ? c.currentFirm.logo : ''
      firmSize = (c.currentFirm.size) ? c.currentFirm.size : ''
    }
    var fullName = c.currentUser.firstName + ' ' + c.currentUser.lastName
    var account = {
      fullName: fullName,
      firstName: c.currentUser.firstName,
      lastName: c.currentUser.lastName,
      email: c.currentUser.email,
      jobTitle: c.currentUser.jobTitle,
      companyName: businessName,
      phone: c.currentUser.phone,
      language: c.currentUser.language,
      businessPhone: phone,
      firmName: firmName,
      acl: acl,
      logo: logo,
      firmSize: firmSize
    }

    document.getElementById('m-s-navSystemAuditor-id').style.display = 'block'
    document.getElementById(cards.auditorAccount.displayID).style.display = 'block'

    // apply settings and load data
    cards.auditorAccount.obj.applySettings()
    cards.auditorAccount.obj.loadData(account)
    cards.auditorAccount.obj.setupListeners()

    // set current card and view it
    currentCard = cards.auditorAccount
    viewCard('auditorAccount')
  }

  initialize()
  return this
}

AuditorSettings.prototype = Object.create(ViewTemplate.prototype)
AuditorSettings.prototype.constructor = AuditorSettings

module.exports = AuditorSettings
