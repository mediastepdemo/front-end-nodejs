'use strict'

import CardTemplate from '../CardTemplate'
import Utility from '../../../../../js/src/utility_c'
import log from '../../../../../js/src/log'
import Forge from '../../../../Forge'
import styles from './CardNotifications.css'

  /**
   * CardNotifications is responsible for Settings/My Devices page.
   * @class CardNotifications
   *
   * @param: {Object} ctrl      - appController for the current user
   * @param: {Object} container - parent container
   * @param: {Object} owner
   */
var CardNotifications = function (ctrl, container, owner) {
  CardTemplate.call(this, ctrl, container, owner)

  this.name = 'CardNotifications: Notifications'
  this.parent = container
  var owner = owner
  var c = ctrl
  var myDevices = []

  this.setupListeners = function () {
    log('Card Notifications: Setting up listeners ...')
  }

  this.loadData = function (data) {
    for (var key in data) {
      $('#' + key).prop('checked', data[key].email)
    }
  }

  /**
   * Building My notifications page.
   * @memberOf CardNotifications
   * @returns  {DOM} widgetContentDiv
   */
  this.buildContent = function (settings) {
    var widgetContentDiv = Forge.build({'dom': 'div', 'id': 'card-Notifications', 'style': 'width:800px;'})

    var titleDiv = Forge.build({'dom': 'div', 'style': ''})
    var title = Forge.build({'dom': 'div', 'text': 'Notifications Settings', 'class': 'card-account-title'})
    var titleHR = Forge.build({'dom': 'hr', 'class': 'card-hr'})
    var cardNote = Forge.build({'dom': 'p', 'class': 'c-notifications-cardNote', 'id': 'pNotificationNote', 'text': 'We will alert you when important updates and changes have occurred to your engagement.\n You can customize your notifications below.'})
    var subHeader = Forge.build({'dom': 'h3', 'class': 'c-notifications-subHeader', 'id': 'h3SubHeader', 'text': 'By Email'})
    var subHR = Forge.build({'dom': 'hr', 'class': 'card-hr'})
    var whenNotifyNote = Forge.build({'dom': 'p', 'class': 'c-notifications-cardSubNote', 'style': 'padding-top:20px;height:50px;', 'id': 'pNotificationNote', 'text': 'Get notified when someone:'})
    var deviceCardDiv = Forge.build({'dom': 'div', 'id': 'card-notifications-div', 'style': 'width: 100%;'})

    var notificationType1 = Forge.build({'dom': 'div', 'class': 'notifications-type'})
    var label1 = Forge.build({'dom': 'label', 'class': 'c-notifications-switch'})
    var NotifyBTN1 = Forge.build({'dom': 'input', 'type': 'checkbox', 'id': 'engagementInvite', 'class': 'cmn-toggle cmn-toggle-round', 'value': 'engagementInvite'})
    var div1 = Forge.build({'dom': 'div', 'for': 'cmn-toggle-1', 'class': 'checkboxSlider round'}) // 'text':'Is invited to your engagement'
    var notificationTypeNote1 = Forge.build({'dom': 'p', 'class': 'notifications-type-text', 'text': 'Is invited to your engagement'})
    var notifyTypeNote1HR = Forge.build({'dom': 'hr', 'class': 'c-notifications-midHr'})

    var notificationType2 = Forge.build({'dom': 'div', 'class': 'notifications-type'})
    var label2 = Forge.build({'dom': 'label', 'class': 'c-notifications-switch'})
    var NotifyBTN2 = Forge.build({'dom': 'input', 'type': 'checkbox', 'id': 'joinEngagement', 'class': 'cmn-toggle cmn-toggle-round', 'value': 'joinEngagement'})
    var div2 = Forge.build({'dom': 'div', 'for': 'cmn-toggle-1', 'class': 'checkboxSlider round'}) // 'text':'Is invented to your engagement'
    var notificationTypeNote2 = Forge.build({'dom': 'p', 'class': 'notifications-type-text', 'text': 'Has joined  your engagement'})
    var notifyTypeNote2HR = Forge.build({'dom': 'hr', 'class': 'c-notifications-midHr'})

    var notificationType3 = Forge.build({'dom': 'div', 'class': 'notifications-type'})
    var label3 = Forge.build({'dom': 'label', 'class': 'c-notifications-switch'})
    var NotifyBTN3 = Forge.build({'dom': 'input', 'type': 'checkbox', 'id': 'newComment', 'class': 'cmn-toggle cmn-toggle-round', 'value': 'newComment'})
    var div3 = Forge.build({'dom': 'div', 'for': 'cmn-toggle-1', 'class': 'checkboxSlider round'}) // 'text':'Is invented to your engagement'
    var notificationTypeNote3 = Forge.build({'dom': 'p', 'class': 'notifications-type-text', 'text': 'Comments on an engagement'})
    var notifyTypeNote3HR = Forge.build({'dom': 'hr', 'class': 'c-notifications-midHr'})

    var notificationType4 = Forge.build({'dom': 'div', 'class': 'notifications-type'})
    var label4 = Forge.build({'dom': 'label', 'class': 'c-notifications-switch'})
    var NotifyBTN4 = Forge.build({'dom': 'input', 'type': 'checkbox', 'id': 'newRequest', 'class': 'cmn-toggle cmn-toggle-round', 'value': 'newRequest'})
    var div4 = Forge.build({'dom': 'div', 'for': 'cmn-toggle-1', 'class': 'checkboxSlider round'}) // 'text':'Is invented to your engagement'
    var notificationTypeNote4 = Forge.build({'dom': 'p', 'class': 'notifications-type-text', 'text': 'Creates a new request within a To-Do'})
    var notifyTypeNote4HR = Forge.build({'dom': 'hr', 'class': 'c-notifications-midHr'})

    var notificationType5 = Forge.build({'dom': 'div', 'class': 'notifications-type'})
    var label5 = Forge.build({'dom': 'label', 'class': 'c-notifications-switch'})
    var NotifyBTN5 = Forge.build({'dom': 'input', 'type': 'checkbox', 'id': 'newTodo', 'class': 'cmn-toggle cmn-toggle-round', 'value': 'newTodo'})
    var div5 = Forge.build({'dom': 'div', 'for': 'cmn-toggle-1', 'class': 'checkboxSlider round'}) // 'text':'Is invented to your engagement'
    var notificationTypeNote5 = Forge.build({'dom': 'p', 'class': 'notifications-type-text', 'text': 'Creates a new To-Do'})
    var notifyTypeNote5HR = Forge.build({'dom': 'hr', 'class': 'c-notifications-midHr'})

    var notificationType6 = Forge.build({'dom': 'div', 'class': 'notifications-type'})
    var label6 = Forge.build({'dom': 'label', 'class': 'c-notifications-switch'})
    var NotifyBTN6 = Forge.build({'dom': 'input', 'type': 'checkbox', 'id': 'documentUploaded', 'class': 'cmn-toggle cmn-toggle-round', 'value': 'documentUploaded'})
    var div6 = Forge.build({'dom': 'div', 'for': 'cmn-toggle-1', 'class': 'checkboxSlider round'}) // 'text':'Is invented to your engagement'
    var notificationTypeNote6 = Forge.build({'dom': 'p', 'class': 'notifications-type-text', 'text': 'Uploads a document'})
    var notifyTypeNote6HR = Forge.build({'dom': 'hr', 'class': 'c-notifications-midHr'})

    var notificationType7 = Forge.build({'dom': 'div', 'class': 'notifications-type'})
    var label7 = Forge.build({'dom': 'label', 'class': 'c-notifications-switch'})
    var NotifyBTN7 = Forge.build({'dom': 'input', 'type': 'checkbox', 'id': 'todoCompleted', 'class': 'cmn-toggle cmn-toggle-round', 'value': 'todoCompleted'})
    var div7 = Forge.build({'dom': 'div', 'for': 'cmn-toggle-1', 'class': 'checkboxSlider round'}) // 'text':'Is invented to your engagement'
    var notificationTypeNote7 = Forge.build({'dom': 'p', 'class': 'notifications-type-text', 'text': 'Marks a To-Do as complete'})

    label1.appendChild(NotifyBTN1)
    label1.appendChild(div1)
    notificationType1.appendChild(label1)
    notificationType1.appendChild(notificationTypeNote1)
    notificationType1.appendChild(notifyTypeNote1HR)

    label2.appendChild(NotifyBTN2)
    label2.appendChild(div2)
    notificationType2.appendChild(label2)
    notificationType2.appendChild(notificationTypeNote2)
    notificationType2.appendChild(notifyTypeNote2HR)

    label3.appendChild(NotifyBTN3)
    label3.appendChild(div3)
    notificationType3.appendChild(label3)
    notificationType3.appendChild(notificationTypeNote3)
    notificationType3.appendChild(notifyTypeNote3HR)

    label4.appendChild(NotifyBTN4)
    label4.appendChild(div4)
    notificationType4.appendChild(label4)
    notificationType4.appendChild(notificationTypeNote4)
    notificationType4.appendChild(notifyTypeNote4HR)

    label5.appendChild(NotifyBTN5)
    label5.appendChild(div5)
    notificationType5.appendChild(label5)
    notificationType5.appendChild(notificationTypeNote5)
    notificationType5.appendChild(notifyTypeNote5HR)

    label6.appendChild(NotifyBTN6)
    label6.appendChild(div6)
    notificationType6.appendChild(label6)
    notificationType6.appendChild(notificationTypeNote6)
    notificationType6.appendChild(notifyTypeNote6HR)

    label7.appendChild(NotifyBTN7)
    label7.appendChild(div7)
    notificationType7.appendChild(label7)
    notificationType7.appendChild(notificationTypeNote7)

    titleDiv.appendChild(title)
    titleDiv.appendChild(titleHR)

    widgetContentDiv.appendChild(titleDiv)
    widgetContentDiv.appendChild(cardNote)
    widgetContentDiv.appendChild(subHeader)
    widgetContentDiv.appendChild(subHR)
    widgetContentDiv.appendChild(whenNotifyNote)
    widgetContentDiv.appendChild(deviceCardDiv)

    widgetContentDiv.appendChild(notificationType1)
    widgetContentDiv.appendChild(notificationType2)
    widgetContentDiv.appendChild(notificationType3)
    widgetContentDiv.appendChild(notificationType4)
    widgetContentDiv.appendChild(notificationType5)
    widgetContentDiv.appendChild(notificationType6)
    widgetContentDiv.appendChild(notificationType7)

    NotifyBTN1.addEventListener('click', function (evt) {
      var action = $(this).prop('checked')
      c.sendEvent({name: 'set-notification', data: {userID: c.currentUser._id, notificationType: this.value, value: action}})
    })
    NotifyBTN2.addEventListener('click', function (evt) {
      var action = $(this).prop('checked')
      c.sendEvent({name: 'set-notification', data: {userID: c.currentUser._id, notificationType: this.value, value: action}})
    })
    NotifyBTN3.addEventListener('click', function (evt) {
      var action = $(this).prop('checked')
      c.sendEvent({name: 'set-notification', data: {userID: c.currentUser._id, notificationType: this.value, value: action}})
    })
    NotifyBTN4.addEventListener('click', function (evt) {
      var action = $(this).prop('checked')
      c.sendEvent({name: 'set-notification', data: {userID: c.currentUser._id, notificationType: this.value, value: action}})
    })
    NotifyBTN5.addEventListener('click', function (evt) {
      var action = $(this).prop('checked')
      c.sendEvent({name: 'set-notification', data: {userID: c.currentUser._id, notificationType: this.value, value: action}})
    })
    NotifyBTN6.addEventListener('click', function (evt) {
      var action = $(this).prop('checked')
      c.sendEvent({name: 'set-notification', data: {userID: c.currentUser._id, notificationType: this.value, value: action}})
    })
    NotifyBTN7.addEventListener('click', function (evt) {
      var action = $(this).prop('checked')
      c.sendEvent({name: 'set-notification', data: {userID: c.currentUser._id, notificationType: this.value, value: action}})
    })
    return widgetContentDiv
  }

  return this
}

CardNotifications.prototype = Object.create(CardTemplate.prototype)
CardNotifications.prototype.constructor = CardNotifications

module.exports = CardNotifications
