'use strict'

import CardTemplate from '../CardTemplate'
import Utility from '../../../../../js/src/utility_c'
import log from '../../../../../js/src/log'
import Forge from '../../../../Forge'
import FlashAlertWidgets from '../../../../widgets/flash-alert/flash-alert'
import styles from './CardAccount.css'

var CardAdminAccount = function (ctrl, container, owner) {
  CardTemplate.call(this, ctrl, container, owner)
  var FlashAlert = new FlashAlertWidgets()
  this.name = 'CardAdminAccount: About You'
  this.parent = container
  var c = ctrl

  this.isComplete = function () {
    return true
  }

  var fields = {
    fullName: {id: 'acc-ay-fullName', type: 'text', value: '', model: {name: 'User', field: 'fullName'}, validation: { regex: Utility.REGEX.name }},
    phone: {id: 'acc-ay-phone', type: 'text', value: '', model: {name: 'User', field: 'phone'}, validation: { regex: Utility.REGEX.phone }},
    email: {id: 'acc-ay-email', type: 'text', value: '', model: {name: 'User', field: 'email'}, validation: { regex: Utility.REGEX.email }}
  }

  // enables card, poor naming here, look to refactor later
  this.Card = function () {
    document.getElementById(fields.fullName.id).disabled = false
    document.getElementById(fields.phone.id).disabled = false
    document.getElementById(fields.email.id).disabled = false
  }

  this.disableCard = function () {
    document.getElementById(fields.fullName.id).disabled = true
    document.getElementById(fields.phone.id).disabled = true
    document.getElementById(fields.email.id).disabled = true
  }

  var isFieldValid = function (data) {
    // We use updateDOM to get the updated value (data has old values).
    var updateDOM = document.getElementById(data.id)
    var valid = true
    // Sets valid if field is valid
    if (data.validation) {
      if (data.validation.required) {
        valid = valid && updateDOM.value.length > 0
      }

      if (data.validation.min) {
        valid = valid && updateDOM.value.length >= data.validation.min
      }

      if (data.validation.max) {
        valid = valid && updateDOM.value.length <= data.validation.max
      }

      if (data.validation.regex) {
        var regex = new RegExp(data.validation.regex)
        valid = valid && regex.test(updateDOM.value)
      }
    } else {
      valid = true
    }
    if (valid) {
      let node = document.getElementById(data.model.field + 'Label')
      node.classList.remove('card-account-error')

      if (node.hasChildNodes()) {
        for (let i = 0; i < node.childNodes.length; i++) {
          if (node.childNodes[i].tagName === 'IMG' && node.childNodes[i].id === data.model.field + 'Error') {
            node.removeChild(node.childNodes[i])
          }
        }
      }
    } else {
      let node = document.getElementById(data.model.field + 'Label')

      if (node.hasChildNodes()) {
        for (let i = 0; i < node.childNodes.length; i++) {
          if (node.childNodes[i].tagName === 'IMG' && node.childNodes[i].id === data.model.field + 'Error') {
            node.removeChild(node.childNodes[i])
          }
        }
      }

      node.className += ' card-account-error'
      node.innerHTML += ' <img src="images/icons/warning.svg" id="' + data.model.field + 'Error' + '" class="card-account-errorImage">'
    }
    return valid
  }

  function updateToDB (data, model) {
    log('Update new information based on what user typed.')

    var updateDOM
    var json = {}

    var length = data.length
    for (var i = 0; i < length; i++) {
      updateDOM = document.getElementById(data[i].id)
      if (!isFieldValid(data[i])) {
        // Nothing changes if not valid.
        continue
      }

      if (data[i].value === updateDOM.value) {
        // Nothing Changes We don't need to update mongo db.
        continue
      }

      if (data[i].model.field === 'fullName') {
        var name = updateDOM.value.split(' ')
        json['firstName'] = name[0]
        json['lastName'] = name[1]
      } else {
        json[data[i].model.field] = updateDOM.value
      }
    }

    if (Object.keys(json).length === 0) {
      return
    }

    if (model === 'User') {
      json.userID = c.currentUser._id
      c.sendEvent({name: 'update-user', data: json})
    }
  }

  this.loadData = function (data) {
    var keys = Object.keys(data)
    for (var i = 0; i < keys.length; i++) {
      var key = keys[i]
      var field = fields[key]
      if (keys[i] === 'acl') {
        field = fields['role']
      }
      if (key === 'email') {
        document.getElementById(field.id).disabled = true
      }
      if (field) {
        var value = data[key]
        if (keys[i] === 'acl') {
          fields['role'].value = value['role']
          fields['role']['model']['acl'] = value
          value = value['role']
        } else {
          fields[key].value = value
        }
        if (value) {
          document.getElementById(field.id).value = value
        }
      }
    }
  }

  var updateUserResponse = function (tag, data) {
    log('> Event: update-user-response')
    log(data)
    var keys = Object.keys(data.data)
    for (var i = 0; i < keys.length; i++) {
      if (keys[i] === 'firstName') {
        fields['fullName'].value = data.data[keys[i]]
      } else if (keys[i] === 'lastName') {
        fields['fullName'].value = fields['fullName'].value + ' ' + data.data[keys[i]]
      } else if (fields[keys[i]]) {
        fields[keys[i]].value = data.data[keys[i]]
        document.getElementById(fields[keys[i]].id).value = data.data[keys[i]]
      }
    }
  }

  this.setupListeners = function () {
    c.registerForEvent('update-user', 'CARDACCNT-UPDATEUSER-000', updateUserResponse)
  }

  this.buildContent = function (settings) {
    // Create DOM objects
    var container = Forge.build({'dom': 'div', 'id': 'contentContainer'})

    var titleContainer = Forge.build({'dom': 'div', 'id': 'titleContainer', 'class': 'card-account-titleDiv'})
    var title = Forge.build({'dom': 'div', 'id': 'titleContent', 'class': 'card-account-title', 'text': 'Account Settings'})
    var titleHr = Forge.build({'dom': 'hr', 'id': 'titleHr', 'class': 'card-hr'})

    var nameContainer = Forge.build({'dom': 'div', 'id': 'nameContainer', 'class': 'card-account-textInputContainer'})
    var nameLabel = Forge.build({'dom': 'label', 'id': 'fullNameLabel', 'text': 'First and Last Name', 'class': 'card-account-label'})
    var nameInput = Forge.build({'dom': 'input', 'id': fields.fullName.id, 'type': 'text', 'placeholder': '', 'class': 'card-account-input'})

    var emailContainer = Forge.build({'dom': 'div', 'id': 'emailContainer', 'class': 'card-account-textInputContainer'})
    var emailLabel = Forge.build({'dom': 'label', 'id': 'emailLabel', 'text': 'Email Address', 'class': 'card-account-label'})
    var emailInput = Forge.build({'dom': 'input', 'id': fields.email.id, 'type': 'text', 'placeholder': '', 'class': 'card-account-input'})

    var phoneContainer = Forge.build({'dom': 'div', 'id': 'phoneContainer', 'class': 'card-account-textInputContainer'})
    var phoneLabel = Forge.build({'dom': 'label', 'id': 'phoneLabel', 'text': 'Phone Number', 'class': 'card-account-label'})
    var phoneInput = Forge.build({'dom': 'input', 'id': fields.phone.id, 'type': 'text', 'placeholder': '', 'class': 'card-account-input', 'style': 'width: 225px'})

    var deactivateAccount = Forge.buildBtn({ 'type': 'link', 'text': 'Deactivate My Account' }).obj
    deactivateAccount.style.display = 'inline-block'
    deactivateAccount.style.marginTop = '55px'

    var updateAccount = Forge.buildBtn({ 'type': 'primary', 'id': 'ac-updatebtn', 'text': 'Update' }).obj
    updateAccount.style.display = 'inline-block'
    updateAccount.style.marginTop = '55px'
    updateAccount.style.marginLeft = '25px'
    updateAccount.disabled = true

    var loaderContainer = Forge.build({'dom': 'div', 'id': 'loaderContainer', 'class': 'update-loader', 'style': 'display: none;'})
    var loaderImage = Forge.build({'dom': 'img', 'src': 'images/loading/spinner2.svg', 'style': 'width:40px; height:40px; position:relative; top:-1px'})
    loaderContainer.appendChild(loaderImage)
    updateAccount.appendChild(loaderContainer)

    // link DOM objects together
    // title
    titleContainer.appendChild(title)
    titleContainer.appendChild(titleHr)

    // standard input fields
    nameContainer.appendChild(nameLabel)
    nameContainer.appendChild(nameInput)

    emailContainer.appendChild(emailLabel)
    emailContainer.appendChild(emailInput)

    phoneContainer.appendChild(phoneLabel)
    phoneContainer.appendChild(phoneInput)

    // Link assembled DOM objects to page container
    container.appendChild(titleContainer)
    container.appendChild(nameContainer)
    container.appendChild(emailContainer)
    container.appendChild(phoneContainer)
    container.appendChild(deactivateAccount)
    container.appendChild(updateAccount)

    // event listeners
    nameInput.addEventListener('blur', function () {
      isFieldValid(fields.fullName)
      checkInputValidation()
    })

    phoneInput.addEventListener('blur', function () {
      isFieldValid(fields.phone)
      checkInputValidation()
    })

    deactivateAccount.addEventListener('click', function () {
      c.displayDeactivateModal()
    })

    updateAccount.addEventListener('click', function () {
      enableInput(true)
      var progress = document.getElementById('loaderContainer')
      progress.style.display = 'inherit'

      setTimeout(function () {
        updateToDB([fields.fullName, fields.phone], 'User')

        setTimeout(function () {
          progress.style.display = 'none'
          FlashAlert.flash({
            content: '<div class="send-message-success-alert"><div class="icon-check"></div><span>Your account has been updated.</span></div>',
            type: 'SUCCESS'
          })
          enableInput(false)
          updateAccount.disabled = true
        }, 1000)
      }, 1000)
    })

    var enableInput = function (flg) {
      document.getElementById(fields.fullName.id).disabled = flg
      document.getElementById(fields.phone.id).disabled = flg
    }

    var checkInputValidation = function () {
      // check for input fields validation
      var fullNameCheck = document.getElementById('fullNameLabel')
      var phoneCheck = document.getElementById('phoneLabel')

      if (fullNameCheck.value === '' || fullNameCheck.classList.contains('card-account-error')) {
        updateAccount.disabled = true
        return
      }
      if (phoneCheck.value === '' || phoneCheck.classList.contains('card-account-error')) {
        updateAccount.disabled = true
        return
      }
      updateAccount.disabled = false
    }

    return container
  }

  return this
}

CardAdminAccount.prototype = Object.create(CardTemplate.prototype)
CardAdminAccount.prototype.constructor = CardAdminAccount

module.exports = CardAdminAccount
