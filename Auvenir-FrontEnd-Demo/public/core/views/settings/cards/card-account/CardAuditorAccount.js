'use strict'

import CardTemplate from '../CardTemplate'
import Utility from '../../../../../js/src/utility_c'
import log from '../../../../../js/src/log'
import Forge from '../../../../Forge'
import FlashAlertWidgets from '../../../../widgets/flash-alert/flash-alert'
import styles from './CardAccount.css'

var CardAuditorAccount = function (ctrl, container, ownerObj) {
  CardTemplate.call(this, ctrl, container, ownerObj)
  var FlashAlert = new FlashAlertWidgets()

  this.name = 'CardAuditorAccount: About You'
  this.parent = container
  var c = ctrl

  this.isComplete = function () {
    return true
  }

  /* Local instances of the field values
   * Purpose: Comparison with received values when running getChanges()
   * You'll probably will want to look at isFieldValid() after adding validations here
   */
  var fields = {
    fullName: {id: 'acc-ay-fullName', type: 'text', value: '', model: {name: 'User', field: 'fullName'}, validation: { regex: Utility.REGEX.name }},
    phone: {id: 'acc-ay-phoneNumber', type: 'text', value: '', model: {name: 'User', field: 'phone'}, validation: { regex: Utility.REGEX.phone }},
    email: {id: 'acc-ay-email', type: 'text', value: '', model: {name: 'User', field: 'email'}, validation: { regex: Utility.REGEX.email }},
    firmName: {id: 'acc-ay-firmName', type: 'text', value: '', model: {name: 'Firm', field: 'name'}, validation: { regex: Utility.REGEX.name }},
    role: {id: 'acc-ay-role', type: 'text', value: '', model: {name: 'User', field: 'jobTitle'}, validation: { regex: Utility.REGEX.alphanumericName }},
    firmSize: {id: 'acc-ay-firmSize', type: 'text', value: '', model: {name: 'Firm', field: 'size'}},
    logo: {id: 'acc-ay-firmLogo', type: 'img', value: 'images/icons/camera.svg', model: {name: 'Firm', field: 'logo'}, validation: { regex: Utility.REGEX.imageFormat }}
  }

  var employeeList = [ {text: '1'}, {text: '2-3'}, {text: '4-10'}, {text: '11-20'}, {text: '21-50'}, {text: '50+'} ]
  /**
   * Enable all your content card's dom objects
   * And any styling
   */
  this.Card = function () {
    document.getElementById(fields.fullName.id).disabled = false
    document.getElementById(fields.firmSize.id).disabled = false
    document.getElementById(fields.logo.id).disabled = false
    document.getElementById(fields.role.id).disabled = false
    document.getElementById(fields.phone.id).disabled = false
    document.getElementById(fields.firmName.id).disabled = false
    document.getElementById(fields.email.id).disabled = false
  }

  /**
   * Disable all your content card's dom objects
   * And any styling
   */
  this.disableCard = function () {
    document.getElementById(fields.fullName.id).disabled = true
    document.getElementById(fields.firmSize.id).disabled = true
    document.getElementById(fields.logo.id).disabled = true
    document.getElementById(fields.role.id).disabled = true
    document.getElementById(fields.phone.id).disabled = true
    document.getElementById(fields.firmName.id).disabled = true
    document.getElementById(fields.email.id).disabled = true
  }

  /**
   * Client-side validation, return true/false if errors exist
   * Side-effects: marks/unmarks fields if errors exists
   */
  var isFieldValid = function (data) {
    // We use updateDOM to get the updated value (data has old values).
    var updateDOM = document.getElementById(data.id)
    var valid = true
    // Sets valid if field is valid
    if (data.validation) {
      if (data.validation.required) {
        valid = valid && updateDOM.value.length > 0
      }

      if (data.validation.min) {
        valid = valid && updateDOM.value.length >= data.validation.min
      }

      if (data.validation.max) {
        valid = valid && updateDOM.value.length <= data.validation.max
      }

      if (data.validation.regex) {
        var regex = new RegExp(data.validation.regex)
        valid = valid && regex.test(updateDOM.value)
      }
    } else {
      valid = true
    }
    if (valid) {
      document.getElementById(data.id).classList.remove('auv-error')
    } else {
      document.getElementById(data.id).className += ' auv-error'
    }
    return valid
  }

  /**
   *  Update Mongo DB if there is any changes.
   */
  function updateToDB (data, model) {
    log('Update new information based on what user typed.')

    var updateDOM
    var json = {}

    var length = data.length
    for (var i = 0; i < length; i++) {
      updateDOM = document.getElementById(data[i].id)
      if (!isFieldValid(data[i])) {
        // Nothing changes if not valid.
        continue
      }

      if (data[i].value === updateDOM.value) {
        // Nothing Changes We don't need to update mongo db.
        continue
      }

      var fieldName = data[i].model.field
      if (fieldName === 'fullName') {
        var name = updateDOM.value.split(' ')
        json['firstName'] = name[0]
        json['lastName'] = name[1]
      } else {
        json[fieldName] = updateDOM.value
      }
    }

    if (Object.keys(json).length === 0) {
      return
    }

    if (model === 'Firm') {
      json.firmID = c.currentFirm._id
      c.sendEvent({name: 'update-firm', data: json})
    } else if (model === 'User') {
      json.userID = c.currentUser._id
      c.sendEvent({name: 'update-user', data: json})
    }
  }

  function updateLogo (src) {
    log('Updating Logo on Account Setting Page')
    document.getElementById(fields.logo.id).src = src
    document.getElementById(fields.logo.id).style.display = 'block'
  }

  /**
   * Upload Profile & Logo pictrue.
   */
  function uploadPic (file, type) {
    log('Upload new Picture ' + type + ' : ' + file.name)
    let baseUri = `${c.getSettings().protocol}://${c.getSettings().domain}:${c.getSettings().port}`
    let uri = baseUri + '/firm-logo-upload'

    var fd = new FormData()
    fd.append('userID', c.currentUser._id)
    fd.append('firmID', c.currentFirm._id)
    fd.append('date', new Date().toUTCString())
    fd.append('file', file)

    var xhr = new XMLHttpRequest()
    xhr.addEventListener('load', () => {
      if (type === 'logo') {
        let { logo } = JSON.parse(xhr.response)
        var myFirm = c.currentFirm._id
        c.updateFirm({ firmID: myFirm, logo })
      }
    })
    xhr.upload.onprogress = function (evt) {
      if (evt.lengthComputable) {
        var rounded = Math.round(((evt.loaded / evt.total) * 100) * 10) / 10
        var percentComplete = Math.min(rounded, 99.9)
        log('Percent: ' + percentComplete + ' Loaded: ' + evt.loaded + 'total: ' + evt.total)
      }
    }
    xhr.open('POST', uri, true)
    xhr.send(fd)
  }

  this.loadData = function (data) {
    var keys = Object.keys(data)
    for (var i = 0; i < keys.length; i++) {
      var key = keys[i]
      var field = fields[key]
      if (key === 'jobTitle') {
        field = fields['role']
      }
      if (key === 'email') {
        document.getElementById(field.id).disabled = true
      }
      if (key === 'firmSize') {
        document.getElementById(field.id).setAttribute('readOnly', 'true')
      }
      if (field) {
        var value = data[key]
        field.value = value
        if (value) {
          switch (field.type) {
            case 'text':
              document.getElementById(field.id).value = value
              break
            case 'img':
              if (value) {
                if (key === 'logo') {
                  let picSrc = Utility.getLogoFirm(value)
                  updateLogo(picSrc)
                }
              }
              break
            default:
              log('Unknown type')
          }
        }
      }
    }
  }

  /**
   * Handler for Update-User Event Response from Socket.
   *
   * @param tag: {String} Tag Name
   * @param data: {JSON} The Result of User update Event.
   */
  var updateUserResponse = function (tag, data) {
    log('> Event: update-user-response')
    log(data)
    var keys = Object.keys(data.data)
    for (var i = 0; i < keys.length; i++) {
      if (keys[i] === 'firstName') {
        fields['fullName'].value = data.data[keys[i]]
      } else if (keys[i] === 'lastName') {
        fields['fullName'].value = fields['fullName'].value + ' ' + data.data[keys[i]]
      } else if (fields[keys[i]]) {
        fields[keys[i]].value = data.data[keys[i]]
        document.getElementById(fields[keys[i]].id).value = data.data[keys[i]]
      }
    }
  }

  /**
   * Handler for Update-Firm Event Response from Socket.
   *
   * @param tagName: {String} Tag Name
   * @param data: {JSON} The Result of Firm update Event.
   */
  var updateFirmResponse = function (tagName, data) {
    log('> Event: update-firm-response')
    log(data)
    var keys = Object.keys(data.data)
    for (var i = 0; i < keys.length; i++) {
      var key
      if (keys[i] === 'name') {
        key = 'firmName'
      } else if (keys[i] === 'size') {
        key = 'firmSize'
      } else if (keys[i] === 'logo') {
        key = 'logo'
      } else {
        key = null
      }
      if (key && fields[key]) {
        fields[key].value = data.data[keys[i]]
        document.getElementById(fields[key].id).value = data.data[keys[i]]
      }
    }
  }

  /**
   * Setup Listener
   */
  this.setupListeners = function () {
    c.registerForEvent('update-user', 'CARDACCNT-UPDATEUSER-000', updateUserResponse)
    c.registerForEvent('update-firm', 'ONBOARDDIRM_UPDATEFIRM-000', updateFirmResponse)
  }

  var testCharacters = function (value) {
    var CheckSum = {REGEX: {notAllowed: /[<>]/}}
    if (CheckSum.REGEX.notAllowed.test(value)) {
      return false
    } else {
      return true
    }
  }
  /**
   * @return a DOM object
   */
  this.buildContent = function (settings) {
    var widgetContentDiv = Forge.build({'dom': 'div'})

    var titleDiv = Forge.build({'dom': 'div'})
    var title = Forge.build({'dom': 'h4', 'text': 'Account Settings', 'class': 'card-account-title'})
    var titleHR = Forge.build({'dom': 'hr', 'class': 'card-hr'})
    titleDiv.appendChild(title)
    titleDiv.appendChild(titleHR)

    var name = Forge.build({'dom': 'div', 'id': 'divName', 'class': 'inputMargin', 'style': 'padding-top: 6px;'})
    var fullName = Forge.buildInput({'label': 'First and Last Name', 'errorText': 'Not a valid name.', 'id': fields.fullName.id, 'placeholder': ''})
    name.appendChild(fullName)

    var email = Forge.build({'dom': 'div', 'id': 'divEmail', 'class': 'inputMargin'})
    var emailInput = Forge.buildInput({'label': 'Email Address', 'errorText': 'Not a valid Email.', 'id': fields.email.id, 'placeholder': ''})
    email.appendChild(emailInput)

    var phoneNumber = Forge.build({'dom': 'div', 'id': 'divPhoneNumber', 'class': 'inputMargin'})
    var mobilePhone = Forge.buildInput({'label': 'Phone Number', 'errorText': 'Not a valid Phone number.', 'id': fields.phone.id, 'placeholder': ''})
    phoneNumber.appendChild(mobilePhone)

    var companyInfo = Forge.build({'dom': 'div', 'id': 'divCompanyInfo', 'class': 'inputMargin', 'style': 'margin-top: 56px;'})

    var divFirmName = Forge.build({'dom': 'div', 'id': 'divFirmName'})
    var firmName = Forge.buildInput({'label': 'Firm Name', 'errorText': 'Not a valid name.', 'id': fields.firmName.id, 'placeholder': ''})
    divFirmName.appendChild(firmName)

    var divRole = Forge.build({'dom': 'div', 'id': 'divRole'})
    var role = Forge.buildInput({'label': 'Role', 'errorText': 'Not a valid role.', 'id': fields.role.id, 'placeholder': ''})
    divRole.appendChild(role)

    var divFirmSize = Forge.build({'dom': 'div', 'id': 'divFirmSize', 'style': 'width: 52%;'})
    var firmSize = Forge.buildInputDropdown({'label': 'Firm Size', 'id': fields.firmSize.id, 'placeholder': 'Select size of your Firm', 'list': employeeList})
    divFirmSize.appendChild(firmSize)

    companyInfo.appendChild(divFirmName)
    companyInfo.appendChild(divRole)
    companyInfo.appendChild(divFirmSize)

    var myWidgetLoading = Forge.build({'dom': 'div', 'class': 'accountSettingPhotoUploading', 'id': 'accountSettingPhotoUploading', 'style': 'display:none;'})
    var myWidgetLoadingImg = Forge.build({'dom': 'img', 'src': 'images/loading/spinner.svg', 'class': 'f-widget-loading-image img-circle'})
    myWidgetLoading.appendChild(myWidgetLoadingImg)

    var cardAccountLogoDiv = Forge.build({'dom': 'div', 'id': 'cardAccountLogoDiv', 'style': 'height: 90px;'})
    var firmLogoDiv = Forge.build({'dom': 'div', 'id': 'firmLogoDiv'})
    var logoField = Forge.build({'dom': 'img', 'id': fields.logo.id, 'class': 'img-circle card-account-logo', 'src': fields.logo.value})
    var logoFieldEditForm = Forge.build({'dom': 'form', 'action': 'images/profilePhotos', 'method': 'POST', 'enctype': 'multipart/form-data'})
    var logoFieldEditInput = Forge.build({'dom': 'input', 'type': 'file', 'name': 'image', 'id': 'settingNewLogoUpload', 'class': 'card-account-logoEdit', 'accept': 'image/*', 'style': 'display : none;'})
    logoFieldEditInput.webkitdirectory = false
    logoFieldEditInput.multiple = false
    logoFieldEditForm.appendChild(logoFieldEditInput)

    var firmLogoDiv2 = Forge.build({'dom': 'div', 'class': 'card-account-firmLogoDiv2', 'id': 'firmLogoDiv2', 'style': 'width: 200px; left: 11%; position: relative;bottom: 52px;'})
    var logoLabel = Forge.build({'dom': 'h5', 'id': 'logoLabel', 'text': 'Firm Logo', 'class': 'card-account-label', 'style': 'position: absolute; bottom: 40px;'})
    var logoUploadBtn = Forge.buildBtn({'type': 'default', 'text': 'Update', 'id': 'logoUploadBtn'}).obj

    var logoMsgContainer = Forge.build({'dom': 'div', 'class': 'inputMargin'})
    var logoMessage = Forge.build({'dom': 'p', 'class': 'account-error-message', 'style': 'margin-top: 8px;'})
    logoMsgContainer.appendChild(logoMessage)

    var deactivateAccount = Forge.buildBtn({ 'type': 'secondary', 'text': 'Deactivate My Account' }).obj
    deactivateAccount.style.display = 'inline-block'
    deactivateAccount.style.marginTop = '55px'

    var updateAccount = Forge.buildBtn({ 'type': 'primary', 'id': 'ac-updatebtn', 'text': 'Update' }).obj
    updateAccount.style.display = 'inline-block'
    updateAccount.style.marginTop = '55px'
    updateAccount.style.marginLeft = '25px'
    updateAccount.disabled = true

    var loaderContainer = Forge.build({'dom': 'div', 'id': 'loaderContainer', 'class': 'update-loader', 'style': 'display: none;'})
    var loaderImage = Forge.build({'dom': 'img', 'src': 'images/loading/spinner2.svg', 'style': 'width:40px; height:40px; position:relative; top:-1px'})
    loaderContainer.appendChild(loaderImage)
    updateAccount.appendChild(loaderContainer)

    firmLogoDiv.appendChild(logoField)
    firmLogoDiv.appendChild(logoFieldEditForm)
    cardAccountLogoDiv.appendChild(firmLogoDiv)
    firmLogoDiv2.appendChild(logoLabel)
    firmLogoDiv2.appendChild(logoUploadBtn)
    cardAccountLogoDiv.appendChild(firmLogoDiv2)

    cardAccountLogoDiv.classList.add('inputMargin')

    widgetContentDiv.appendChild(titleDiv)
    widgetContentDiv.appendChild(name)
    widgetContentDiv.appendChild(email)
    widgetContentDiv.appendChild(phoneNumber)
    widgetContentDiv.appendChild(companyInfo)
    widgetContentDiv.appendChild(cardAccountLogoDiv)
    widgetContentDiv.appendChild(logoMsgContainer)
    widgetContentDiv.appendChild(deactivateAccount)
    widgetContentDiv.appendChild(updateAccount)

    // Everytime it's a key is pressed, it will check that field's value and update DOM's value.
    fullName.children[1].addEventListener('keydown', function () {
      var checkName = document.getElementById(fields.fullName.id)
      if (fields.fullName.validation.regex.test(checkName.value) && testCharacters(checkName.value)) {
        checkName.classList.remove('auv-error')
      } else {
        checkName.className = 'auv-input auv-error'
      }
    })
    emailInput.children[1].addEventListener('blur', function () {
      var checkEmail = document.getElementById(fields.email.id)
      if (fields.email.validation.regex.test(checkEmail.value)) {
        checkEmail.classList.remove('auv-error')
      } else {
        checkEmail.className = 'auv-input auv-error'
      }
    })
    mobilePhone.children[1].addEventListener('blur', function () {
      var checkMobilePhone = document.getElementById(fields.phone.id)
      if (fields.phone.validation.regex.test(checkMobilePhone.value)) {
        checkMobilePhone.classList.remove('auv-error')
      } else {
        checkMobilePhone.className = 'auv-input auv-error'
      }
    })
    firmName.children[1].addEventListener('keydown', function () {
      var checkFirmName = document.getElementById(fields.firmName.id)
      if (fields.firmName.validation.regex.test(checkFirmName.value)) {
        checkFirmName.classList.remove('auv-error')
      } else {
        checkFirmName.className = 'auv-input auv-error'
      }
    })
    role.children[1].addEventListener('keydown', function () {
      var checkRole = document.getElementById(fields.role.id)
      if (fields.role.validation.regex.test(checkRole.value)) {
        checkRole.classList.remove('auv-error')
      } else {
        checkRole.className = 'auv-input auv-error'
      }
    })

    logoField.addEventListener('click', () => {
      $('#settingNewLogoUpload').click()
    })

    logoUploadBtn.addEventListener('click', function (event) {
      $('#settingNewLogoUpload').click()
    })

    logoFieldEditInput.addEventListener('change', (e) => {
      let img = e.target.files[0]

      if (img) {
        if (img.size <= 2000000) {
          if (fields.logo.validation.regex.test(img.name)) {
            let reader = new FileReader()
            reader.addEventListener('load', (e) => {
              document.getElementById('acc-ay-firmLogo').style.border = 'transparent'
              logoMessage.style.display = 'none'
              document.getElementById('acc-ay-firmLogo').src = reader.result
            })

            // read the image file as a data URL.
            reader.readAsDataURL(img)
          } else {
            document.getElementById('acc-ay-firmLogo').style.border = 'solid 1px #f3672f'
            logoMessage.innerText = '*Please select a valid image file.'
            logoMessage.style.display = 'inherit'
            updateAccount.disabled = true
          }
        } else {
          document.getElementById('acc-ay-firmLogo').style.border = 'solid 1px #f3672f'
          logoMessage.innerText = '*Your image is too big. Please upload an image less than 2MB.'
          logoMessage.style.display = 'inherit'
          updateAccount.disabled = true
        }
      }
      setTimeout(function () {
        checkInputValidation()
      }, 600)
    })

    var doUploadLogo = function () {
      let img = document.getElementById('settingNewLogoUpload')
      img = img.files[0]
      if (img) {
        uploadPic(img, 'logo')
      }
    }

    var enableInput = function (flg) {
      document.getElementById(fields.fullName.id).disabled = flg
      document.getElementById(fields.phone.id).disabled = flg
      document.getElementById(fields.firmName.id).disabled = flg
      document.getElementById(fields.role.id).disabled = flg
    }

    var checkInputValidation = function () {
      // check for input fields validation
      var updateBtn = document.getElementById('ac-updatebtn')
      var fullNameCheck = document.getElementById(fields.fullName.id)
      var phoneCheck = document.getElementById(fields.phone.id)
      var firmNameCheck = document.getElementById(fields.firmName.id)
      var roleCheck = document.getElementById(fields.role.id)

      if (fullNameCheck.value === '' || fullNameCheck.classList.contains('auv-error')) {
        updateBtn.disabled = true
        return
      }
      if (phoneCheck.value === '' || phoneCheck.classList.contains('auv-error')) {
        updateBtn.disabled = true
        return
      }
      if (firmNameCheck.value === '' || firmNameCheck.classList.contains('auv-error')) {
        updateBtn.disabled = true
        return
      }
      if ((logoMessage.style.display === 'inherit' || logoMessage.style.display === 'block') && logoMessage.innerText !== '') {
        updateAccount.disabled = true
        return
      }
      if (roleCheck.value === '' || roleCheck.classList.contains('auv-error')) {
        // updateBtn.disabled = true
        // return
      }
      updateBtn.disabled = false
    }

    // Everytime it's out focused, it will checks that field's value and DOM's value, if it's different, it will updates.
    fullName.children[1].addEventListener('blur', function () {
      isFieldValid(fields.fullName)
      checkInputValidation()
    })
    role.children[1].addEventListener('blur', function () {
      isFieldValid(fields.role)
      checkInputValidation()
    })
    firmName.children[1].addEventListener('blur', function () {
      isFieldValid(fields.firmName)
      checkInputValidation()
    })
    mobilePhone.children[1].addEventListener('blur', function () {
      isFieldValid(fields.phone)
      checkInputValidation()
    })

    firmSize.children[2].addEventListener('blur', function () {
      setTimeout(function () {
        isFieldValid(fields.firmSize)
        checkInputValidation()
      }, 1000)
    })

    deactivateAccount.addEventListener('click', function () {
      c.displayDeactivateModal()
    })

    updateAccount.addEventListener('click', function () {
      enableInput(true)
      var progress = document.getElementById('loaderContainer')
      progress.style.display = 'inherit'

      setTimeout(function () {
        updateToDB([fields.fullName, fields.role, fields.phone], 'User')
        setTimeout(function () {
          updateToDB([fields.firmName, fields.firmSize], 'Firm')
        }, 1000)
        doUploadLogo()

        setTimeout(function () {
          progress.style.display = 'none'
          FlashAlert.flash({
            content: '<div class="send-message-success-alert"><div class="icon-check"></div><span>Your account has been updated.</span></div>',
            type: 'SUCCESS'
          })
          enableInput(false)
          updateAccount.disabled = true
        }, 1000)
      }, 1000)
    })

    return widgetContentDiv
  }

  return this
}

CardAuditorAccount.prototype = Object.create(CardTemplate.prototype)
CardAuditorAccount.prototype.constructor = CardAuditorAccount

module.exports = CardAuditorAccount
