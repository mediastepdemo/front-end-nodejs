'use strict'

import Forge from '../../../Forge'
import Utility from '../../../../js/src/utility_c'
import log from '../../../../js/src/log'
import styles from './CardTemplate.css'

var CardTemplate = function (ctrl, container, ownerObj) {
  this.name = ''
  this.parent = container // dashboard container
  var owner = ownerObj
  var c = ctrl
  var self = this
  var cardID = null

  var currentSettings = {
    width: 'auto',
    height: 'auto',
    background: '#fff',
    border: '0px solid black',
    color: '#000',
    fontSize: '12px',
    font: 'Lato'
  }

  /**
   * Return true or false depending on wether the parent object exists in the DOM.
   */
  this.parentExists = function () {
    return !!(document.getElementById(this.parent))
  }

  var saveSettings = function (settings) {
    currentSettings.width = (settings.width) ? settings.width : currentSettings.width
    currentSettings.height = (settings.height) ? settings.height : currentSettings.height
    currentSettings.background = (settings.background) ? settings.background : currentSettings.background
    currentSettings.border = (settings.border) ? settings.border : currentSettings.border
    currentSettings.color = (settings.color) ? settings.color : currentSettings.color
    currentSettings.fontSize = (settings.fontSize) ? settings.fontSize : currentSettings.fontSize
    currentSettings.font = (settings.font) ? settings.font : currentSettings.font
    currentSettings.header = (settings.header) ? settings.header : null
    currentSettings.footer = (settings.footer) ? settings.footer : null
    currentSettings.overflow = (settings.overflow) ? settings.overflow : false
    currentSettings.hidden = (settings.hidden) ? settings.hidden : false
  }

  this.applySettings = function () {
    $('#' + cardID)[0].style.display = 'inline-block'
    $('#' + cardID)[0].style.verticalAlign = 'top'
    $('#' + cardID)[0].style.margin = '0px 0px 10px 0px'
    $('#' + cardID)[0].style.width = (currentSettings.width) ? currentSettings.width : null
    $('#' + cardID)[0].style.height = (currentSettings.height) ? currentSettings.height : null
    $('#' + cardID)[0].style.background = (currentSettings.background) ? currentSettings.background : null
    $('#' + cardID)[0].style.border = (currentSettings.border) ? currentSettings.border : null
    $('#' + cardID)[0].style.color = (currentSettings.color) ? currentSettings.color : null
    $('#' + cardID)[0].style.fontSize = (currentSettings.fontSize) ? currentSettings.fontSize : null
    $('#' + cardID)[0].style.font = (currentSettings.font) ? currentSettings.font : null
    if (currentSettings.overflow) {
      $('#' + cardID)[0].style.overflow = 'auto'
    }
    if (currentSettings.hidden) {
      $('#' + cardID)[0].style.display = 'none'
    }
  }

  /**
   * Triggered by the toggleActive() function.  Responsible for disabling all dom elements.
   * Needs to be implemented in a block in order to be integrated.
   */
  this.disableCard = function () {
  }

  /**
   * Loads the JSON object data into the block.
   * Needs to be implemented in a block in order to be integrated.
   */
  this.loadData = function (json) {
  }

  /**
   * Returns a JSON object representation of the block data.
   * Needs to be implemented in a block in order to be integrated.
   */
  this.getJSON = function () {
    return {}
  }

  /**
   * Responsible for building the entire DOM for the content block and attaching it to the
   * global DOM. It will build it based on the required features and tabs.
   */
  this.buildCard = function (settings) {
    log(this.name + ': Building card')
    saveSettings(settings)

    var uniqueID = Utility.randomString(32, Utility.ALPHANUMERIC)
    while (document.getElementById('card-' + uniqueID)) {
      uniqueID = Utility.randomString(32, Utility.ALPHANUMERIC)
    }

    cardID = 'card-' + uniqueID

    var myCard = Forge.build({'dom': 'div', 'id': cardID, 'class': 'module-card', 'style': 'box-shadow: 0 1px 3px rgba(0, 0, 0, 0.25); border-radius: 4px;'})

    /** HEADER **/
    if (currentSettings.header && currentSettings.header.active) {
      var myCardHeader = Forge.build({'dom': 'div', 'id': 'cardHeader-' + uniqueID, 'style': 'position:absolute;top:0px;width:100%; height:20px;background:lightgray;'})
      var cardClose = Forge.build({'dom': 'span', 'text': 'X', 'class': 'w-closeIcon', 'id': 'cardClose-' + uniqueID})

      var title = 'N/A'
      if (currentSettings.header && currentSettings.header.title) {
        title = currentSettings.header.title
      }

      var cardTitle = Forge.build({'dom': 'label', 'text': title, 'style': 'text-align: center'})
      cardClose.addEventListener('click', function (event) {
        var cardContainer = $('#' + cardID)
        cardContainer.fadeOut(150)

        document.getElementById(cardID).parentNode.removeChild(document.getElementById('cardClose-' + uniqueID))
      })

      var headHR = Forge.build({'dom': 'hr'})
      myCardHeader.appendChild(cardTitle)
      myCardHeader.appendChild(cardClose)
      myCardHeader.appendChild(headHR)
      myCard.appendChild(myCardHeader)
    }

    /** CONTENT **/
    var myCardContent = Forge.build({'dom': 'div', 'id': 'cardContent-' + uniqueID, 'style': 'width:inherit; height:inherit'})
    var content = this.buildContent()

    if (content) {
      myCardContent.appendChild(content)
    } else {
      var warningDOM = Forge.build({'dom': 'span', 'text': 'buildContent() must return a dom object!'})
      myCardContent.appendChild(warningDOM)
    }
    myCard.appendChild(myCardContent)

    /** FOOTER **/
    if (currentSettings.footer && currentSettings.footer.active) {
      var myCardFooter = Forge.build({'dom': 'div', 'id': 'cardFooter-' + uniqueID, 'style': 'position:absolute;bottom:0px;width:100%; height:20px;background:lightgray'})
      var footHR = Forge.build({'dom': 'hr'})
      var footLabel = Forge.build({'dom': 'a', 'text': 'This is the bottom!'})
      myCardFooter.appendChild(footHR)
      myCardFooter.appendChild(footLabel)
      myCard.appendChild(myCardFooter)
    }

    return myCard
  }

  /**
   * Responsible for returning a DOM object that will display the unique content and functionality
   * of the specific block. Needs to implemented by each block.
   */
  this.buildContent = function () {
    var warningDOM = Forge.build({'dom': 'span', 'text': 'BUILD CONTENT NOT YET SUPPORTED'})
    return warningDOM
  }

  /**
   * Displays the block on the screen
   */
  this.show = function () {
    document.getElementById(this.parent).style.display = 'inherit'
  }

  /**
   *Hides the block from the screen
   */
  this.hide = function () {
    log(this.parent)
    document.getElementById(this.parent).style.display = 'none'
  }

  // Event Object
  this.evts = {}

  /**
   * Sets up a listener on any specific tag
   * @param {String}   tag - The name of the event
   * @param {Function} cb  - The callback function
   */
  this.on = function (tag, cb) {
    if (typeof tag !== 'string' || typeof cb !== 'function') {
      return
    }

    if (self.evts[tag] === undefined) {
      self.evts[tag] = cb
    } else {
      self.evts[tag] = cb
    }
  }

  /**
   * Trigger all Listening Objects Callbacks
   * @param {String} - The tag being triggered
   * @param {Object} - The event that occured (if available)
   * @param {Object} - The custom event data
   */
  this.trigger = function (tag, evt, data) {
    if (this.evts[tag]) {
      this.evts[tag](evt, data)
    }
  }

  return this
}

module.exports = CardTemplate
