'use strict'

import CardTemplate from '../CardTemplate'
import Utility from '../../../../../js/src/utility_c'
import log from '../../../../../js/src/log'
import Forge from '../../../../Forge'
import styles from './CardMyDevices.css'

import Modal_Custom from '../../../../modals/custom/Modal_Custom'

/**
 * cardMyDevices is responsible for Settings/My Devices page.
 * @class cardMydevices
 *
 * @param: {Object} ctrl      - appController for the current user
 * @param: {Object} container - parent container
 * @param: {Object} owner
 */
var CardMyDevices = function (ctrl, container, owner) {
  CardTemplate.call(this, ctrl, container, owner)

  this.name = 'CardDevices: My Devices'
  this.parent = container
  var owner = owner
  var c = ctrl
  var myDevices = []
  var settings = {
    modalName: 'viewDevice',
    close: { 'window': true, 'global': false },
    width: '484px',
    header: { 'show': true, 'color': '#363a3c'},
    position: { 'bottom': '0px' },
    title: ''
  }
  var viewDeviceModal = new Modal_Custom(c, settings)
  var viewDeviceContainer = Forge.build({'dom': 'div', 'id': 'viewDevice-container', 'style': 'height:285px; text-align:center'})
  viewDeviceModal.setObject(viewDeviceContainer, settings)

/**
 * Get Device picture path depends on device.
 * @memberOf cardMydevices
 * @param   {JSON}   data
 * @returns {string} img_path
 */
  var getDevicePic = function (data) {
    log('Get Device\'s image...', data)
    var img_path = 'images/illustrations/devices/'
    if (data.brand) { // auth.devices
      if (data.brand === 'Apple' || data.brand === 'apple') {
        if (data.model) {
          if (data.model.includes('iPhone')) {
            img_path = img_path + 'iPhone.svg'
          } else if (data.model.includes('iPad')) {
            img_path = img_path + 'iPad.svg'
          } else if (data.model.includes('watch') || data.model.includes('Watch')) {
            img_path = img_path + 'appleWatch.svg'
          } else {
            img_path = img_path + 'iPhone.svg'
          }
        } else {
          img_path = img_path + 'macbook.svg'
        }
      } else if (data.brand === 'Samsung' || data.brand === 'samsung') {
        if (data.model.includes('Galaxy')) {
          img_path = img_path + 'Android.svg'
        } else if (data.model.match(/tab/)) {
          img_path = img_path + 'tablet.svg'
        } else {
          img_path = img_path + 'Android.svg'
        }
      } else {
        if (data.model) {
          if (data.model.match(/tab/)) {
            img_path = img_path + 'tablet.svg'
          } else {
            img_path = img_path + 'Android.svg'
          }
        } else {
          img_path = img_path + 'Android.svg'
        }
      }
    } else { // auth.devices
      if (data.device) {
        if (data.device.match(/Mac/)) { // device string includes brandname and their model name
          img_path = img_path + 'macbook.svg'
        } else if (data.device.match(/iPhone/)) {
          img_path = img_path + 'iPhone.svg'
        } else if (data.device.match(/iPad/)) {
          img_path = img_path + 'iPad.svg'
        } else if (data.os.match(/Android/) || data.device === 'Mobile') {
          img_path = img_path + 'Android.svg'
        } else {
          img_path = img_path + 'Desktop.svg'
        }
      } else {
        img_path = img_path + 'Desktop.svg'
      }
    }
    return img_path
  }

/**
 * This function set Margin on img based on what device is passed
 * @memberOf cardMydevices
 * @param   {string} src
 * @returns  {string} style
 */
  var setImgStyle = function (src) {
    var style = ''
    if (src === 'images/illustrations/devices/Desktop.svg') {
      style = 'margin-top: 7px'
    } else if (src === 'images/illustrations/devices/macbook.svg') {
      style = 'margin-top: 3px'
    } else if (src === 'images/illustrations/devices/iPad.svg') {
      style = 'margin-top:-3px'
    } else if (src === 'images/illustrations/devices/tablet.svg') {
      style = 'margin-top: -3px'
    } else if (src === 'images/illustrations/devices/appleWatch.svg') {
      style = 'margin-top:-1px; width: 33px; margin-left: 15px; margin-right: 15px'
    } else {
      style = 'margin-top: 0px'
    }
    return style
  }

/**
 * Remove Device from device list and front-end.
 * @memberOf cardMydevices
 * @param    {JSON} data
 */
  var removeDevice = function (data) {
    log('Remove Device : ' + JSON.stringify(data.device))
    for (var i = 0; i < myDevices.length; i++) {
      if (myDevices[i].uniqueID === data.device.uniqueID) {
        var DeviceElement = document.getElementById('divDevice-' + myDevices[i].domID)
        var HorizontalRow = DeviceElement.previousElementSibling
        myDevices.splice(i, 1)
        document.getElementById('card-devices-div').removeChild(DeviceElement)
        document.getElementById('pNumDevices').innerHTML = 'You have registered ' + myDevices.length.toString() + ' devices for secure login.'
        if (HorizontalRow) {
          document.getElementById('card-devices-div').removeChild(HorizontalRow)
        }

        break
      }
    }
    document.getElementById('progressOverlay').style.display = 'none'
  }

/**
 * Response to socket event (remove-device)
 * @memberOf cardMydevices
 * @param    {JSON} data
 */
  var responseSocketEvent = function (tagName, data) {
    if (data.code) {
      c.displayMessage(data.msg, {'color': 'red'})
    } else {
      removeDevice(data)
      if (data.logout) {
        setTimeout(function () {
          window.location.href = '/logout'
        }, 500)
      }
    }
  }

/**
 * Setup listener for remove-device-response.
 * @memberOf cardMydevices
 */
  this.setupListeners = function () {
    log('Card My Device: Setting up listeners ...')
    c.registerForEvent('remove-device', 'mydevice-000', responseSocketEvent)
  }

/**
 * Show device to Message Modal.
 * @memberOf cardMydevices
 * @param {JSON} device
 */
  var viewDevice = function (device) {
    log('Viewing Device:' + device.name)
    log('DEVICE JSON INFO: ' + JSON.stringify(device))

    var divViewDevice = Forge.build({'dom': 'div', 'id': 'viewDevice-div-box', 'style': 'height:100%; text-align:center'})
    var imgsrc = getDevicePic(device)
    var devImg = Forge.build({'dom': 'img', 'src': imgsrc, 'class': 'c-devices-deviceViewImg'})
    var devCustomName = Forge.build({'dom': 'span', 'text': device.name, 'id': 'device-viewCustomName', 'class': 'c-devices-viewCustomName'})

    var app

    var columnLeft = []
    var columnRight = []

    if (device.type === 'MOBILE') { // idq device
      app = 'mobileApp'
      columnLeft.push('Device: ')
      var modelType = device.modelType ? ' ' + device.modelType : ''
      columnRight.push(device.brand + ' ' + device.model + modelType)
      columnLeft.push('OS: ')
      columnRight.push(device.os)
      if (device.serialNum !== undefined && device.serialNum != null && device.serialNum !== '') {
        columnLeft.push('Serial: ')
        columnRight.push(device.serialNum)
      }
      if (device.color !== undefined && device.color != null && device.color !== '') {
        columnLeft.push('Color: ')
        columnRight.push(device.color)
      }
      columnLeft.push('Last Used: ')
      columnRight.push(Utility.dateProcess(device.access.lastLogin, {military: false, month: {type: 'string', length: 'long'}, day: { append: true }, output: 'MM DD YYYY'}))
    } else if (device.type === 'DESKTOP') {
      app = 'webApp'
      columnLeft.push('Device: ')
      columnRight.push(device.model)
      columnLeft.push('OS: ')
      columnRight.push(device.os)
      columnLeft.push('Browser: ')
      columnRight.push(device.browser)
      columnLeft.push('Last Used: ')
      columnRight.push(Utility.dateProcess(device.access.lastLogin, {military: false, month: {type: 'string', length: 'long'}, day: { append: true }, output: 'MM DD YYYY'}))
    }
    var disconnectDiv = Forge.build({'dom': 'div', 'id': 'c-devices-disconnectDiv'})
    var disconnectBtn = Forge.build({'dom': 'button', 'class': 'btn-lg ghost c-devices-viewDisconnectButton', 'text': 'Disconnect Device' })
    var disconnectConfirm = Forge.build({'dom': 'div', 'style': 'display: none; margin-top: 32px', 'class': 'c-devices-disconnectConfirm', 'id': 'c-devices-disconnectConfirm'})
    var disconnectAlert = Forge.build({'dom': 'div', 'class': 'c-devices-disconAlert', 'style': 'display: inline-block; font-size: 16px; color: #6d6d6d; padding-right: 16px;', 'text': 'Disconnect this device?'})
    var disconnectCancel = Forge.buildBtn({'type': 'dlink', 'text': 'Cancel', 'style': 'display: inline-block', 'margin': '0 8px 0 0'}).obj
    var disconnectYes = Forge.buildBtn({'type': 'warning', 'text': 'Disconnect', 'style': 'display: inline-block'}).obj
    var table = Forge.build({'dom': 'table', 'id': 'viewDevice-tabel', 'style': 'margin: 8px 0px 8px -20px; display:inline-block;'})

    for (var i = 0; i < columnRight.length; i++) {
      var tr = Forge.build({'dom': 'tr'})
      var td1 = Forge.build({'dom': 'td', 'text': columnLeft[i], 'class': 'c-devices-deviceInfo'})
      var td2 = Forge.build({'dom': 'td', 'text': columnRight[i], 'class': 'c-devices-deviceInfo'})

      tr.appendChild(td1)
      tr.appendChild(td2)
      table.appendChild(tr)
    }

    divViewDevice.appendChild(devImg)
    divViewDevice.appendChild(table)
    disconnectDiv.appendChild(disconnectBtn)
    disconnectDiv.appendChild(disconnectConfirm)
    disconnectConfirm.appendChild(disconnectAlert)
    disconnectConfirm.appendChild(disconnectCancel)
    disconnectConfirm.appendChild(disconnectYes)
    divViewDevice.appendChild(disconnectDiv)

    document.getElementById('viewDevice-container').innerHTML = null
    document.getElementById('viewDevice-container').appendChild(divViewDevice)
    document.getElementById('m-' + viewDeviceModal._modalID + '-title-text').innerText = device.name

    viewDeviceModal.open()

    disconnectBtn.addEventListener('click', function (evt) {
      disconnectBtn.style.display = 'none'
      disconnectConfirm.style.display = 'block'
    })
    disconnectCancel.addEventListener('click', function (evt) {
      disconnectConfirm.style.display = 'none'
      disconnectBtn.style.display = 'initial'
    })
    disconnectYes.addEventListener('click', function (evt) {
      document.getElementById('progressOverlay').style.display = 'inherit'
      c.sendEvent({name: 'remove-device', data: {userID: c.currentUser._id, device: device, app: app}})
      viewDeviceModal.close()
    })
  }

/**
 * Adds devices into each card on My Devices page.
 * @memberOf cardMydevices
 * @param    {JSON} data
 * @returns  {DOM} divDevice
 */
  var addDevice = function (data) {
    log('Adding the Device: ' + data.name)
    do {
      var devId = Utility.randomString(8, Utility.ALPHANUMERIC)
    } while (document.getElementById('divDevice-' + devId))

    var device = data
    device.domID = devId
    myDevices.push(device)
    var imgsrc = getDevicePic(device)
    var imgStyle = setImgStyle(imgsrc)

    var divDevice = Forge.build({'dom': 'div', 'class': 'c-devices-divDevice', 'id': 'divDevice-' + devId})
    var devImg = Forge.build({'dom': 'img', 'src': imgsrc, 'id': 'device-img-' + devId, 'class': 'c-devices-deviceImg', 'align': 'bottom', 'style': imgStyle})
    var devNamesHolder = Forge.build({'dom': 'div', 'class': 'c-devices-nameHolder'})
    var devCustomName = Forge.build({'dom': 'span', 'text': data.name, 'id': 'device-customName-' + devId, 'class': 'c-devices-deviceCustomName'})
    var devName
    if (data.brand) { // idq device
      devName = Forge.build({'dom': 'span', 'text': data.brand + ' ' + data.model, 'id': 'device-name-' + devId, 'class': 'c-devices-deviceName'})
      devName = Forge.build({'dom': 'span', 'text': data.brand + ' ' + data.model, 'id': 'device-name-' + devId, 'class': 'c-devices-deviceName'})
    } else { // reg device
      devName = Forge.build({'dom': 'span', 'text': data.model, 'id': 'device-name-' + devId, 'class': 'c-devices-deviceName'})
    }
    var viewDeviceBtn = Forge.build({'dom': 'button', 'id': 'btn-view-' + devId, 'class': 'btn-lg ghost c-devices-viewDeviceBtn', 'style': 'float:right', 'text': 'View' })
    devNamesHolder.appendChild(devCustomName)
    devNamesHolder.appendChild(devName)
    divDevice.appendChild(devImg)
    divDevice.appendChild(devNamesHolder)
    divDevice.appendChild(viewDeviceBtn)

    viewDeviceBtn.addEventListener('click', function (evt) {
      viewDevice(device)
    })
    document.getElementById('card-devices-div').appendChild(divDevice)

    return divDevice
  }

/**
 * Populates all input fields with the information sent in data about the CLIENT
 * @memberOf cardMydevices
 * @param    {JSON} data
 */
  this.loadData = function (data, addingAnother) {
    log('My Devices Loading Data.')
    // refresh view.
    document.getElementById('card-devices-div').innerHTML = ''
    if (data) {
      for (var i = 0; i < data.length; i++) {
        var hr = Forge.build({'dom': 'hr', 'style': 'margin-top: 15px; width: 862px; margin-left: -5px'})
        if (addingAnother) {
          document.getElementById('card-devices-div').appendChild(hr)
        }
        addDevice(data[i])
        if (i < data.length - 1 && data.length !== 1) {
          document.getElementById('card-devices-div').appendChild(hr)
        }
      }
    }
    document.getElementById('pNumDevices').innerHTML = 'You have registered ' + myDevices.length.toString() + ' devices for secure login.'
  }

/**
 * Building My Devices page.
 * @memberOf cardMydevices
 * @returns  {DOM} widgetContentDiv
 */
  this.buildContent = function (settings) {
    var widgetContentDiv = Forge.build({'dom': 'div', 'id': 'card-Mydevices', 'style': 'width:800px'})

    var titleDiv = Forge.build({'dom': 'div', 'style': ''})
    var title = Forge.build({'dom': 'div', 'text': 'My Devices', 'class': 'card-account-title'})
    var titleHR = Forge.build({'dom': 'hr', 'class': 'card-hr'})
    var numDevices = Forge.build({'dom': 'p', 'class': 'c-devices-numDevices', 'id': 'pNumDevices'})
    var deviceCardDiv = Forge.build({'dom': 'div', 'id': 'card-devices-div', 'style': 'width: 100%'})
    var addDeviceButton = Forge.build({'dom': 'button', 'class': 'btn-sm ghost c-devices-addDeviceBtn', 'style': 'font-weight: bold', 'text': '+ Add Another' })

    titleDiv.appendChild(title)
    titleDiv.appendChild(titleHR)

    widgetContentDiv.appendChild(titleDiv)
    widgetContentDiv.appendChild(numDevices)
    widgetContentDiv.appendChild(deviceCardDiv)
    widgetContentDiv.appendChild(addDeviceButton)

    addDeviceButton.addEventListener('click', function () {
      c.displayInstallAppModal()
    })

    return widgetContentDiv
  }

  return this
}

CardMyDevices.prototype = Object.create(CardTemplate.prototype)
CardMyDevices.prototype.constructor = CardMyDevices

module.exports = CardMyDevices
