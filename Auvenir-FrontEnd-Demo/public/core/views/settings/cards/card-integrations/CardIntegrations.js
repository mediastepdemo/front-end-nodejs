'use strict'

import CardTemplate from '../CardTemplate'
import Utility from '../../../../../js/src/utility_c'
import log from '../../../../../js/src/log'
import Forge from '../../../../Forge'
import styles from './CardIntegrations.css'
import Modal_Custom from '../../../../modals/custom/Modal_Custom'

/**
 * CardIntegration is responsible for Settings/Integrations page.
 * @class CardIntegrations
 *
 */

var CardIntegrations = function (ctrl, container, owner) {
  CardTemplate.call(this, ctrl, container, owner)

  this.name = 'CardIntegrations: Google Drive'
  this.parent = container
  var c = ctrl
  var self = this

  // for warning modal
  var settings = {
    modalName: 'remove-integration-warning',
    close: { 'window': true, 'global': false },
    width: '484px',
    header: { 'show': true, 'color': '#363a3c', 'icon': 'warning' },
    position: { 'bottom': '0px' },
    title: 'Integration Removal'
  }
  var WarningModal = new Modal_Custom(c, settings)
  var deleteStorageContainer = Forge.build({'dom': 'div', 'id': 'files-deleteStorage-container'})

  document.getElementById(WarningModal.contentID).innerHTML = ''
  document.getElementById(WarningModal.contentID).appendChild(deleteStorageContainer)

  var responseSocketEvent = function (tagName, data) {
    log('Card_Integrations: response to Socket Event > add-Integration')
    log(data)
    c.currentIntegrations.gdrive.uid = data.uid
  }

  this.setupListeners = () => {
    log('Card Integrations: Setting up listeners...')
    c.registerForEvent('add-integration', 'Card_Integrations_001', responseSocketEvent)
  }

  var addIntegration = () => {
    if (c.gdriveManager) {
      c.gdriveManager.getAllConsumers()
      c.gdriveManager.on('get-gdrive-integrations', (data) => {
        document.getElementById('card-integration-loading').style.display = 'none'
        if (data.status === 200) {
          if (data.msg) {
            document.getElementById('settings-gDrive-removeAllBtn').style.display = 'none'
          } else {
            c.currentIntegrations.gdrive.consumers = data.consumers
            if (c.currentIntegrations.gdrive.consumers.length === 0) {
              document.getElementById('settings-gDrive-removeAllBtn').style.display = 'none'
            } else {
              c.currentIntegrations.gdrive.consumers.forEach((oneConsumer, i) => {
                var conId
                do {
                  conId = Utility.randomString(8, Utility.ALPHANUMERIC)
                } while (document.getElementById('c-integration-consumerDiv-' + conId))

                var oneConsumerDiv = Forge.build({ 'dom': 'div', 'class': 'c-integration-oneConsumerDiv', 'id': 'c-integration-consumerDiv-' + conId })
                var consumerIcon = Forge.build({ 'dom': 'i', 'class': 'auvicon-line-user c-integration-consumerName', 'style': 'width: 28px;' })
                var consumerName = Forge.build({ 'dom': 'div', 'text': oneConsumer, 'id': 'c-integration-consumerName-' + conId, 'class': 'c-integration-consumerName' })
                var trashIcon = Forge.build({ 'dom': 'i', 'class': 'auvicon-trash settings-integration-trashIcon' })
                var folderContainer = Forge.build({ 'dom': 'div', 'style': 'display: inherit; margin-top: 16px;' })
                oneConsumerDiv.appendChild(consumerIcon)
                oneConsumerDiv.appendChild(consumerName)
                oneConsumerDiv.appendChild(trashIcon)
                oneConsumerDiv.appendChild(folderContainer)

                if (i > 0) {
                  var hr = Forge.build({ 'dom': 'hr', 'style': 'margin: 18px 0px; width: 800px;' })
                  document.getElementById('gDrive-integration-card').appendChild(hr)
                }

                document.getElementById('gDrive-integration-card').appendChild(oneConsumerDiv)

                trashIcon.addEventListener('click', () => {
                  var messageDiv = Forge.build({ 'dom': 'div', 'style': 'padding: 24px 37px 28px;' })
                  var warning = Forge.build({ 'dom': 'div', 'text': 'Warning: ' })
                  warning.style.color = '#fd6d47'
                  warning.style.display = 'inline-block'
                  warning.style.marginRight = '5px'
                  var message = Forge.build({ 'dom': 'div', 'text': 'You are about to remove ' + oneConsumer + ". This may limit access to files or remove files that have already been integrated. Are you sure you'd like to remove this integration?" })
                  message.style.display = 'inline'
                  var deleteBtnGroup = Forge.build({ 'dom': 'div', 'style': 'margin: 0 auto 37px; display: block' })
                  var deleteCancelBtn = Forge.buildBtn({ 'text': 'Cancel', 'id': 'settings-deleteStorage-cancelBtn', 'type': 'light', 'margin': '0 8px 0 0' }).obj
                  deleteCancelBtn.style.display = 'inline-block'
                  var deleteBtn = Forge.buildBtn({ 'text': 'Remove', 'type': 'warning' }).obj
                  deleteBtn.style.display = 'inline-block'
                  deleteBtnGroup.appendChild(deleteCancelBtn)
                  deleteBtnGroup.appendChild(deleteBtn)
                  messageDiv.appendChild(warning)
                  messageDiv.appendChild(message)

                  deleteStorageContainer.innerHTML = ''
                  deleteStorageContainer.appendChild(messageDiv)
                  deleteStorageContainer.appendChild(deleteBtnGroup)

                  WarningModal.open()

                  deleteCancelBtn.addEventListener('click', () => {
                    WarningModal.close()
                  })

                  deleteBtn.addEventListener('click', () => {
                    c.gdriveManager.removeGDrive(oneConsumer)
                    c.gdriveManager.on('disconnect-result', (removeResult) => {
                      if (removeResult.msg === 'success') {
                        WarningModal.close()
                        self.trigger('flash', { content: 'Account ' + removeResult.email + ' removed successfully.', type: 'SUCCESS' })

                        // delete the dom.
                        oneConsumerDiv.remove()
                        if (i > 0) {
                          hr.remove()
                        } else if (i === 0 && c.currentIntegrations.gdrive.consumers.length > 1) {
                          document.getElementById('gDrive-integration-card').removeChild(document.getElementById('gDrive-integration-card').childNodes[1])
                        }

                        // modifying the array
                        c.currentIntegrations.gdrive.consumers.splice(i, 1)
                        if (c.currentIntegrations.gdrive.consumers.length === 0) {
                          document.getElementById('settings-gDrive-removeAllBtn').style.display = 'none'
                        }

                        loadData()
                      } else {
                        WarningModal.close()
                        self.trigger('flash', { content: removeResult.msg, type: 'ERROR' })
                      }
                    })
                  })
                })

                c.gdriveManager.on('get-gdrive-folders', (result) => {
                  document.getElementById('card-integration-loading').style.display = 'none'
                  if (result.status === 200) {
                    if (result.folders) {
                      let folders = result.folders
                      folders.forEach((folder) => {
                        // TODO - in the future, if we want to turn on/off for individual folders, we need uniqueID for each div.
                        var folderIcon = Forge.build({ 'dom': 'i', 'class': 'auvicon-line-folder settings-integration-folder settings-integration-folderIcon' })
                        var folderText = Forge.build({ 'dom': 'div', 'text': folder, 'class': 'settings-integration-folder', 'style': 'margin-left: 10px;' })
                        var folderDiv = Forge.build({ 'dom': 'div', 'style': 'padding-left: 16px; display: block;' })
                        folderContainer.appendChild(folderDiv)
                        folderDiv.appendChild(folderIcon)
                        folderDiv.appendChild(folderText)
                      })
                    }
                  } else if (result.status === 500) {
                    self.trigger('flash', { content: result.msg, type: 'ERROR' })
                  }
                })
                c.gdriveManager.getFolderInfos(oneConsumer)
              })
            }
          }
        } else if (data.status === 500) {
          self.trigger('flash', { content: data.msg, type: 'ERROR' })
        } else {
          self.trigger('flash', { content: data.msg, type: 'ERROR' })
        }
      })
    }
  }

  var loadData = this.loadData = (data) => {
    log('Integrations loading data.')
    document.getElementById('card-integration-loading').style.display = 'block'
    $('#gDrive-integration-card').empty()

    var integrations = c.currentUser.integrations
    if (integrations.length > 0) {
      for (let i = 0; i < integrations.length; i++) {
        var hr = Forge.build({'dom': 'hr', 'style': 'margin: 18px auto; width: 770px;'})
        if (integrations[i].name === 'Google Drive') {
          c.currentIntegrations.gdrive.uid = integrations[i].uid

          addIntegration()
          if (i < (integrations.length - 1) && integrations.length !== 1) {
            document.getElementById('gDrive-integration-card').appendChild(hr)
          }
        }
      }
    }
  }

  this.buildContent = (settings) => {
    var widgetContentDiv = Forge.build({'dom': 'div', 'id': 'card-integrations-container', 'style': 'width:800px;'})
    var titleDiv = Forge.build({'dom': 'div', 'style': ''})
    var title = Forge.build({'dom': 'div', 'text': 'Integrations', 'class': 'card-account-title'})
    var titleHR = Forge.build({'dom': 'hr', 'class': 'widget-hr'})
    var cardIntegrationsDiv = Forge.build({'dom': 'div', 'id': 'card-integrations-div', 'style': 'width: 100%;'})
    titleDiv.appendChild(title)
    titleDiv.appendChild(titleHR)
    widgetContentDiv.appendChild(titleDiv)
    widgetContentDiv.appendChild(cardIntegrationsDiv)

    var gDriveIcon = Forge.build({'dom': 'img', 'src': 'core/components/files/img/google-drive-logo.svg', 'class': 'settings-gDrive-icon'})
    var gDriveTitle = Forge.build({'dom': 'div', 'text': 'Google Drive', 'class': 'settings-gDrive-title'})
    var gDriveDesc = Forge.build({'dom': 'div', 'text': 'Google Drive lets you store files securely online, access them from anywhere and collaborate with others. This integration allows you to import Google Drive files.', 'class': 'settings-gDrive-text settings-gDrive-desc'})
    var gDriveAccountContainer = Forge.build({'dom': 'div', 'style': 'margin-top: 48px;', 'id': 'settings-gDriveAcc-container'})
    var gDriveAccountTitle = Forge.build({'dom': 'div', 'text': 'Accounts', 'class': 'settings-gDrive-accTitle'})
    var gDriveAccountDiv = Forge.build({'dom': 'div', 'id': 'gDrive-integration-card', 'style': 'margin-top: 24px;'})
    var myWidgetLoading = Forge.build({'dom': 'div', 'class': 'progress-overlay card-account-uploadLoading', 'id': 'card-integration-loading', 'style': 'display:none;'})
    var progressSpinner = Forge.build({'dom': 'img', 'src': '../../../../../images/loading/spinner.svg', 'style': 'width: 60px; height: 60px;'})
    myWidgetLoading.appendChild(progressSpinner)
    var gDriveBtnGroup = Forge.build({ 'dom': 'div', 'style': 'display: table; margin: 51px auto;' })
    var gDriveAddAccBtn = Forge.buildBtn({'text': 'Add Account', 'type': 'primary', 'margin': '36px auto 0px'}).obj
    gDriveAddAccBtn.style.display = 'inline-block'
    gDriveAddAccBtn.style.marginRight = '9px'
    var gDriveRemoveBtn = Forge.buildBtn({'text': 'Remove Integration', 'id': 'settings-gDrive-removeAllBtn', 'type': 'light', 'margin': '36px auto 0px'}).obj
    gDriveRemoveBtn.style.display = 'inline-block'

    cardIntegrationsDiv.appendChild(gDriveIcon)
    cardIntegrationsDiv.appendChild(gDriveTitle)
    cardIntegrationsDiv.appendChild(gDriveDesc)
    cardIntegrationsDiv.appendChild(gDriveAccountContainer)
    gDriveAccountContainer.appendChild(gDriveAccountTitle)
    gDriveAccountContainer.appendChild(gDriveAccountDiv)
    gDriveAccountContainer.appendChild(myWidgetLoading)
    gDriveAccountContainer.appendChild(gDriveBtnGroup)
    gDriveBtnGroup.appendChild(gDriveAddAccBtn)
    gDriveBtnGroup.appendChild(gDriveRemoveBtn)

    gDriveAddAccBtn.addEventListener('click', () => {
      c.gdriveManager.openPicker()
      c.gdriveManager.on('setup-result', (setupResult) => {
        if (setupResult.result === 'success') {
          loadData()
          gDriveRemoveBtn.style.display = 'inline-block'
          document.getElementById('progressOverlay').style.display = 'none'

          // googleDrive Folder Integration.
          c.gdriveManager.integrateGDrive()
          c.gdriveManager.on('integration-result', (result) => {
            if (result.status === 200) {
              if (result.code === 0) {
                self.trigger('flash', { content: result.msg, type: 'SUCCESS' })
              } else {
                self.trigger('flash', { content: result.msg, type: 'ERROR' })
              }
            } else if (result.status === 500) {
              self.trigger('flash', { content: result.msg, type: 'ERROR' })
            } else {
              self.trigger('flash', { content: 'Unknown Error status. Please try again.', type: 'ERROR'})
            }
          })
        } else {
          document.getElementById('progressOverlay').style.display = 'none'
          self.trigger('flash', { content: setupResult.msg, type: 'ERROR' })
        }
      })
    })

    gDriveRemoveBtn.addEventListener('click', () => {
      var messageDiv = Forge.build({'dom': 'div', 'style': 'padding: 24px 37px 28px;'})
      var warning = Forge.build({'dom': 'div', 'text': 'Warning: '})
      warning.style.color = '#fd6d47'
      warning.style.display = 'inline-block'
      warning.style.marginRight = '5px'
      var message = Forge.build({'dom': 'div', 'text': 'You are about to remove all Google Drive Integrations. This may limit access to files or remove files that have already been integrated. Are you sure you\'d like to remove all integrations?'})
      message.style.display = 'inline'
      var deleteBtnGroup = Forge.build({'dom': 'div', 'style': 'margin: 0 auto 37px; display: block'})
      var deleteCancelBtn = Forge.buildBtn({ 'text': 'Cancel', 'id': 'settings-deleteStorage-cancelBtn', 'type': 'light', 'margin': '0 8px 0 0' }).obj
      deleteCancelBtn.style.display = 'inline-block'
      var deleteBtn = Forge.buildBtn({ 'text': 'Remove', 'type': 'warning' }).obj
      deleteBtn.style.display = 'inline-block'
      deleteBtnGroup.appendChild(deleteCancelBtn)
      deleteBtnGroup.appendChild(deleteBtn)
      messageDiv.appendChild(warning)
      messageDiv.appendChild(message)

      deleteStorageContainer.innerHTML = ''
      deleteStorageContainer.appendChild(messageDiv)
      deleteStorageContainer.appendChild(deleteBtnGroup)

      WarningModal.open()

      deleteCancelBtn.addEventListener('click', () => {
        WarningModal.close()
      })

      deleteBtn.addEventListener('click', () => {
        for (let i = 0; i < c.currentIntegrations.gdrive.consumers.length; i++) {
          c.gdriveManager.removeGDrive(c.currentIntegrations.gdrive.consumers[i])
          c.gdriveManager.on('disconnect-result', (removeResult) => {
            if (removeResult.msg === 'success') {
              self.trigger('flash', { content: 'Account ' + removeResult.email + ' removed successfully.', type: 'SUCCESS' })
              loadData()
              if (i === c.currentIntegrations.gdrive.consumers.length - 1) {
                WarningModal.close()
              }
            } else {
              WarningModal.close()
              self.trigger('flash', { content: removeResult.msg, type: 'ERROR' })
            }
          })
        }
      })
    })

    return widgetContentDiv
  }

  return this
}

CardIntegrations.prototype = Object.create(CardTemplate.prototype)
CardIntegrations.prototype.constructor = CardIntegrations

module.exports = CardIntegrations
