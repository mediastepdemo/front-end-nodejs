'use strict'

import ViewTemplate from '../ViewTemplate'
import log from '../../../js/src/log'
import Forge from '../../Forge'
import styles from './settings.css'
import CardClientAccount from './cards/card-account/CardClientAccount'
// import CardMyDevices from './cards/card-mydevices/CardMyDevices'
// import CardDeactivateAccount from './cards/card-deactivate-account/CardDeactivateAccount' @DELETE
import CardNotifications from './cards/card-notifications/CardNotifications'
// import CardIntegrations from './cards/card-integrations/CardIntegrations'

var ClientSettings = function (ctrl, settings) {
  ViewTemplate.call(this, ctrl, settings)

  var self = this
  var name = this.name = 'ClientSettings'
  var c = ctrl

  // var card_deactivate = null
  var currentCard = null

  var cards = {
    account: { linkID: 'link-setting-account', obj: null, displayID: 'settings-account-container' },
    notifications: { linkID: 'link-setting-notifications', obj: null, displayID: 'settings-notifications-container' },
    integrations: { linkID: 'link-setting-integrations', obj: null, displayID: 'settings-integrations-container' }
    // devices: { linkID: 'link-setting-device', obj: null, displayID: 'settings-devices-container' }
  }

  var cardContainer = 'settings-cards-container'

  var initialize = function () {
    cards.account.obj = new CardClientAccount(c, cards.account.displayID, this)
    // cards.devices.obj = new CardMyDevices(c, cards.devices.displayID, this)
    // card_deactivate = new CardDeactivateAccount(c, '', this) @DELETE
    cards.notifications.obj = new CardNotifications(c, cards.notifications.displayID, this)
    // cards.integrations.obj = new CardIntegrations(c, cards.integrations.displayID, this)

    self.build()
    document.getElementById(self._moduleID).classList.add('scroll-settings')
  }

  var viewCard = function (name, option) {
    if (cards[name] && cards[name].obj) {
      if (currentCard.linkID !== cards[name].linkID) {
        if (!option) option = ''
        document.getElementById(currentCard.linkID + option).classList.toggle('m-s-navLink-active')
        currentCard.obj.hide()
        currentCard = cards[name]
        currentCard.obj.show()
        document.getElementById(currentCard.linkID + option).classList.toggle('m-s-navLink-active')
      }
    }
  }

  var addCard = function (parent, myCard) {
    var parentDom = document.getElementById(parent)
    parentDom.appendChild(myCard)
  }

  // Add a new device
  // this.addDevice = function (device) {
  //   log('Adding new device', device)
  //   cards.devices.obj.loadData([device], true)
  // }

  this.buildPage = function () {
    log('ClientSettings: Building Page')

    // containers
    var parentContainer = Forge.build({'dom': 'div', 'id': 'settingsPage', class: 'parent-of-footer'})
    var settingsDashboard = Forge.build({'dom': 'div', 'id': 'settingsDashboard', 'class': 'dashboard', 'style': 'display:inherit; margin-bottom: 0px; min-height: 880px'})

    // nav system
    var navSystem = Forge.build({'dom': 'div', 'id': 'navSystem', 'class': 'm-s-navSystem'})
    var navTitle = Forge.build({'dom': 'div', 'id': 'navTitle', 'class': 'm-s-navSystemHead', 'text': 'Settings'})
    var navHr = Forge.build({'dom': 'hr', 'class': 'm-s-navHr'})

    // nav links
    // account
    var navLinkOne = Forge.build({'dom': 'div', 'id': cards.account.linkID, 'class': 'm-s-navLink m-s-navLink-active', 'text': 'Account'})
    // notifications
    var navLinkTwo = Forge.build({'dom': 'div', 'id': cards.notifications.linkID, 'class': 'm-s-navLink', 'text': 'Notifications'})
    // integrations
    // var navLinkThree = Forge.build({'dom': 'div', 'id': cards.integrations.linkID, 'class': 'm-s-navLink', 'text': 'Integrations'})
    // devices
    // var navLinkFour = Forge.build({'dom': 'div', 'id': cards.devices.linkID, 'class': 'm-s-navLink', 'text': 'Devices'})

    // card system
    var cardSystem = Forge.build({'dom': 'div', 'id': 'cardContainer', 'class': 'm-s-cardContainer'})
    var accountCard = Forge.build({'dom': 'div', 'id': cards.account.displayID})
    var notificationsCard = Forge.build({'dom': 'div', 'id': cards.notifications.displayID, 'class': 'm-s-cardContainer', 'style': 'display:none;'})
    // var integrationsCard = Forge.build({'dom': 'div', 'id': cards.integrations.displayID, 'class': 'm-s-cardContainer', 'style': 'display:none;'})
    // var devicesCard = Forge.build({'dom': 'div', 'id': cards.devices.displayID, 'class': 'm-s-cardContainer', 'style': 'display:none;'})

    // event listeners
    navLinkOne.addEventListener('click', function () {
      viewCard('account')
    })

    navLinkTwo.addEventListener('click', function () {
      viewCard('notifications')
    })

    // navLinkThree.addEventListener('click', function () {
    //   cards.integrations.obj.loadData()
    //   viewCard('integrations')
    // })

    // navLinkFour.addEventListener('click', function () {
    //   viewCard('devices')
    // })

    // assemble nav system
    navSystem.appendChild(navTitle)
    navSystem.appendChild(navHr)
    navSystem.appendChild(navLinkOne)
    navSystem.appendChild(navLinkTwo)
    // navSystem.appendChild(navLinkThree)
    // navSystem.appendChild(navLinkFour)

    // assemble card system
    cardSystem.appendChild(accountCard)
    cardSystem.appendChild(notificationsCard)
    // cardSystem.appendChild(integrationsCard)
    // cardSystem.appendChild(devicesCard)

    // assemble settings dashboard
    settingsDashboard.appendChild(navSystem)
    settingsDashboard.appendChild(cardSystem)

    // assemble parent container
    parentContainer.appendChild(settingsDashboard)

    var footerComponent = Forge.build({dom: 'div', id: 'client-settings-footer'})
    footerComponent.innerHTML = c.footer
    parentContainer.appendChild(footerComponent)

    return parentContainer
  }

  this.viewAccount = function () {
    viewCard('account')
  }

  this.loadDashboard = function () {
    // add cards
    addCard(cards.account.displayID, cards.account.obj.buildCard({width: '840px', background: 'white'}))
    // addCard(cards.devices.displayID, cards.devices.obj.buildCard({width: '840px', background: 'white'}))
    addCard(cards.notifications.displayID, cards.notifications.obj.buildCard({width: '840px', background: 'white'}))
    // addCard(cards.integrations.displayID, cards.integrations.obj.buildCard({ width: '840px', background: 'white'}))

    // add account info
    var account = {
      fullName: c.currentUser.firstName + ' ' + c.currentUser.lastName,
      firstName: c.currentUser.firstName,
      lastName: c.currentUser.lastName,
      email: c.currentUser.email,
      phone: c.currentUser.phone
      // profilePicture: c.currentUser.profilePicture
    }

    // display the nav system and account card by default
    document.getElementById('navSystem').style.display = 'block'
    document.getElementById(cards.account.displayID).style.display = 'block'

    // apply settings and load data
    cards.account.obj.applySettings()
    cards.account.obj.loadData(account)
    cards.account.obj.setupListeners()

    cards.notifications.obj.applySettings()
    cards.notifications.obj.loadData(c.currentUser.notifications)

    // cards.integrations.obj.applySettings()
    // cards.integrations.obj.loadData()
    // cards.integrations.obj.on('flash', (data) => {
    //   self.trigger('flash', data)
    // })

    // var allDevices = []

    // for (var i = 0; i < c.currentUser.auth.devices.length; i++) {
    //   allDevices.push(c.currentUser.auth.devices[i])
    // }
    // cards.devices.obj.applySettings()
    // cards.devices.obj.loadData(allDevices)

    currentCard = cards.account
    viewCard('account')
  }

  initialize()
  return this
}

ClientSettings.prototype = Object.create(ViewTemplate.prototype)
ClientSettings.prototype.constructor = ClientSettings

module.exports = ClientSettings
