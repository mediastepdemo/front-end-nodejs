'use strict'

import ViewTemplate from '../ViewTemplate'
import log from '../../../js/src/log'
import Forge from '../../Forge'
import styles from './settings.css'
import CardAdminAccount from './cards/card-account/CardAdminAccount'

var AdminSettings = function (ctrl, settings) {
  ViewTemplate.call(this, ctrl, settings)

  var self = this
  var name = this.name = 'AdminSettings'
  var c = ctrl

  var currentCard = null

  var cards = {
    account: { linkID: 'link-setting-account', obj: null, displayID: 'settings-account-container' }
  }

  var cardContainer = 'settings-cards-container'

  var initialize = function () {
    cards.account.obj = new CardAdminAccount(c, cards.account.displayID, this)
    currentCard = cards.account
    self.build()
  }

  var viewCard = function (name, option) {
    if (cards[name] && cards[name].obj) {
      if (currentCard.linkID !== cards[name].linkID) {
        if (!option) option = ''
        document.getElementById(currentCard.linkID + option).classList.toggle('m-s-navLink-active')
        currentCard.obj.hide()
        currentCard = cards[name]
        currentCard.obj.show()
        document.getElementById(currentCard.linkID + option).classList.toggle('m-s-navLink-active')
      }
    }
  }

  var addCard = function (parent, myCard) {
    var parentDom = document.getElementById(parent)
    parentDom.appendChild(myCard)
  }

  this.viewAccount = function () {
    viewCard('account')
  }

  this.buildPage = function () {
    log('AdminSettings: Building Page')

    // containers
    var parentContainer = Forge.build({'dom': 'div', 'id': 'settingsPage', class: 'parent-of-footer'})
    var settingsDashboard = Forge.build({'dom': 'div', 'id': 'settingsDashboard', 'class': 'dashboard', 'style': 'display:inherit; margin-bottom: 0px; min-height: 880px'})

    // nav system
    var navSystem = Forge.build({'dom': 'div', 'id': 'navSystem', 'class': 'm-s-navSystem'})
    var navTitle = Forge.build({'dom': 'div', 'id': 'navTitle', 'class': 'm-s-navSystemHead', 'text': 'Settings'})
    var navHr = Forge.build({'dom': 'hr', 'id': 'navHr', 'class': 'm-s-navHr'})

    // nav links
    var navLinkOne = Forge.build({'dom': 'div', 'id': cards.account.linkID, 'class': 'm-s-navLink m-s-navLink-active', 'text': 'Account'})

    // card system
    var cardSystem = Forge.build({'dom': 'div', 'id': 'cardContainer', 'class': 'm-s-cardContainer'})
    var accountCard = Forge.build({ 'dom': 'div', 'id': cards.account.displayID })

    // event listeners
    navLinkOne.addEventListener('click', function () {
      viewCard('account')
    })

    // assemble nav system
    navSystem.appendChild(navTitle)
    navSystem.appendChild(navHr)
    navSystem.appendChild(navLinkOne)

    // assemble card system
    cardSystem.appendChild(accountCard)

    // assemble settings dashboard
    settingsDashboard.appendChild(navSystem)
    settingsDashboard.appendChild(cardSystem)

    // assemble parent container
    parentContainer.appendChild(settingsDashboard)

    var footerComponent = Forge.build({dom: 'div', id: 'admin-settings-footer'})
    footerComponent.innerHTML = c.footer
    parentContainer.appendChild(footerComponent)

    return parentContainer
  }

  this.loadDashboard = function () {
    // add cards
    addCard(cards.account.displayID, cards.account.obj.buildCard({width: '840px', background: 'white'}))

    // add account info
    var account = {
      fullName: c.currentUser.firstName + ' ' + c.currentUser.lastName,
      firstName: c.currentUser.firstName,
      lastName: c.currentUser.lastName,
      email: c.currentUser.email,
      phone: c.currentUser.phone
    }

    // display the nav system and account card by default
    document.getElementById('navSystem').style.display = 'block'
    document.getElementById(cards.account.displayID).style.display = 'block'

    // apply settings and load data
    cards.account.obj.applySettings()
    cards.account.obj.loadData(account)
    cards.account.obj.setupListeners()

    currentCard = cards.account
    viewCard('account')
  }

  initialize()
  return this
}

AdminSettings.prototype = Object.create(ViewTemplate.prototype)
AdminSettings.prototype.constructor = AdminSettings

module.exports = AdminSettings
