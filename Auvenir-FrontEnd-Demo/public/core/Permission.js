'use strict'

import log from '../js/src/log'
const definePermission = require('../js/src/definePermission')
const Permission = function (ctrl) {
  const self = this
  const c = ctrl
  self.acl = null

  var getCurrentUser = function () {
    return c.currentUser
  }

  var getCurrentEngagement = function () {
    return c.currentEngagement
  }

  var getCurrentFirm = function () {
    return c.currentFirm
  }

  var getCurrentBusiness = function () {
    return c.currentBusiness
  }

  self.canSeeAllClientTodo = function () {
    return (self.isAdminClient() || self.isLeadClient())
  }

  self.canSeeAllTodo = function () {
    /**
     * TODO:
     * if user is admin: return true
     * if user is lead: return true
     */

    return (self.isAdminAuditor() || self.isLeadAuditor())
  }

  self.hasPermissionInTodo = function (permission, todo) {
    /**
     * TODO:
     * if user is Auditor & is not lead & user is not assigned to Auditor assignee: return false
     * if user is Client & is not lead & user is not assigned to Client assignee: return false
     * else: return this.permissions.includes(permission)
     */
    var result = (self.getPermissionInTodo(todo).indexOf(permission) !== -1)

    log('call hasPermissionInTodo - permission: ' + permission + ' result: ' + result)

    return result
  }

  self.getPermissionInTodo = function (todo) {
    var permission = []
    if (self.isAdminAuditor() === true) {
      permission = definePermission.Roles.ADMIN_AUDITOR
    }
    if (self.isLeadAuditor() === true) {
      permission = permission.concat(definePermission.Roles.LEAD_AUDITOR)
    }
    if (self.isAuditor(todo) === true) {
      permission = permission.concat(definePermission.Roles.AUDITOR)
    }
    if (self.isAdminClient() === true) {
      permission = permission.concat(definePermission.Roles.ADMIN_CLIENT)
    }
    if (self.isLeadClient() === true) {
      permission = permission.concat(definePermission.Roles.LEAD_CLIENT)
    }
    if (self.isClient(todo) === true) {
      permission = permission.concat(definePermission.Roles.CLIENT)
    }

    log('call getPermissionInTodo')
    log(permission)

    return permission
  }

  self.isPermissionOnEngagement = function (permission) {
    var arr_permission = []
    if (self.isTypeClient() === true) {
      definePermission.Roles.CLIENT.includes(permission) ? arr_permission.push(permission) : arr_permission
      if (self.isLeadClient() === true) {
        definePermission.Roles.LEAD_CLIENT.includes(permission) ? arr_permission.push(permission) : arr_permission
      }
      if (self.isAdminClient() === true) {
        definePermission.Roles.ADMIN_CLIENT.includes(permission) ? arr_permission.push(permission) : arr_permission
      }
    } else if (self.isTypeAuditor() === true) {
      definePermission.Roles.AUDITOR.includes(permission) ? arr_permission.push(permission) : arr_permission
      if (self.isLeadAuditor() === true) {
        definePermission.Roles.LEAD_AUDITOR.includes(permission) ? arr_permission.push(permission) : arr_permission
      }
      if (self.isAdminAuditor() === true) {
        definePermission.Roles.ADMIN_AUDITOR.includes(permission) ? arr_permission.push(permission) : arr_permission
      }
    }
    return arr_permission.includes(permission)
  }

  self.isAdminAuditor = function () {
    var acl = self.getAclUser()
    var result = (acl.admin === true && acl.role.toUpperCase() === 'AUDITOR')
    log('isAdminAuditor: ' + result)
    return result
  }

  self.isLeadAuditor = function () {
    var acl = self.getAclUser()
    var result = (acl.lead === true && acl.role.toUpperCase() === 'AUDITOR')
    log('isLeadAuditor: ' + result)
    return result
  }

  self.isAuditor = function (todo) {
    var acl = self.getAclUser()
    var result = (todo.auditorAssignee && todo.auditorAssignee._id === getCurrentUser()._id &&
                  acl.lead === false && acl.role.toUpperCase() === 'AUDITOR')
    log('isAuditor: ' + result)
    return result
  }

  self.isAdminClient = function () {
    var acl = self.getAclUser()
    var result = (acl.bAdmin === true && acl.role.toUpperCase() === 'CLIENT')
    log('isAdminClient: ' + result)
    return result
  }

  self.isLeadClient = function () {
    var acl = self.getAclUser()
    var result = (acl.lead === true && acl.role.toUpperCase() === 'CLIENT')
    log('isLeadClient: ' + result)
    return result
  }

  self.isClient = function (todo) {
    var acl = self.getAclUser()
    var result = (todo.clientAssignee && todo.clientAssignee._id === getCurrentUser()._id &&
                  acl.lead === false && acl.role.toUpperCase() === 'CLIENT')
    log('isClient: ' + result)
    return result
  }
  self.isTypeClient = function () {
    var acl = self.getAclUser()
    var result = (acl.role.toUpperCase() === 'CLIENT')
    log('isTypeClient: ' + result)
    return result
  }
  self.isTypeAuditor = function () {
    var acl = self.getAclUser()
    var result = (acl.role.toUpperCase() === 'AUDITOR')
    log('isTypeAuditor: ' + result)
    return result
  }
  self.getAclUser = function () {
    var engagement = getCurrentEngagement()
    var user = getCurrentUser()
    var firm = getCurrentFirm()
    var businesses = getCurrentBusiness()

    var acl = {
      role: '',      // engagement.acl.role
      lead: false,   // engagement.alc.lead
      admin: false,  // firms.acl.admin
      bAdmin: false, // businesses.acl.admin
      type: '',      // user.type
      engagementId: null
    }
    acl.type = user.type
    if (engagement && engagement.acl) {
      // check current engagement
      if (self.acl && self.acl.engagementId === engagement._id) return self.acl

      acl.engagementId = engagement._id
      engagement.acl.forEach(function (item) {
        if (item.id === user._id) {
          acl.role = item.role
          acl.lead = item.lead
          return false
        }
      })
    }

    if (firm) {
      firm.acl.forEach(function (item) {
        if (item.id === user._id) {
          acl.admin = item.admin
          return false
        }
      })
    }

    if (businesses && businesses.acl) {
      businesses.acl.forEach(function (item) {
        if (item.id === user._id) {
          acl.bAdmin = item.admin
          return false
        }
      })
    }

    log('getCurrentUser')
    log(c.currentUser)

    log('getCurrentEngagement')
    log(c.currentEngagement)

    log('getCurrentFirm')
    log(c.currentFirm)

    log('getCurrentBusiness')
    log(c.currentBusiness)

    log('-----------------permission.getAclUser--------------------')
    log(acl)

    self.acl = acl

    return acl
  }
}

module.exports = Permission
