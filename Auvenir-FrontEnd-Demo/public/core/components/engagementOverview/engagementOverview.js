'use strict'

import componentTemplate from '../ComponentTemplate'
import log from '../../../js/src/log'
import Forge from '../../Forge'
import moment from 'moment'
import Utility from '../../../js/src/utility_c'
import './engagementOverview.css'

var ComponentEngagementOverview = function (ctrl, container) {
  componentTemplate.call(this)

  this.name = 'Engagement Overview'
  this.parent = container
  var c = ctrl

  let activityArray = []

  // Populates all input fields with the information sent in data about the CLIENT
  this.loadData = function (currentFiles, currentRequests, currentEngagement, currentUserType) {
    log('Loading Engagement Overview Info')
    registerEvents()
    let auditNameList = document.getElementById(overviewData.auditorTeam.id)
    auditNameList.innerHTML = ''
    c.currentEngagement.acl.forEach((user) => {
      if (user.role === 'AUDITOR') {
        let userName = Forge.build({dom: 'p', text: user.userInfo.firstName + ' ' + user.userInfo.lastName, style: 'font-size: 14px; line-height: 17px; color: #363A3C; margin: 0px 4px;'})
        let userTitle = Forge.build({ dom: 'p', text: user.userInfo.jobTitle, style: 'font-size: 14px; line-height: 17px; margin-left: 4px;' })
        auditNameList.appendChild(userName)
        auditNameList.appendChild(userTitle)
      }
    })

    let clientNameList = document.getElementById(overviewData.clientTeam.id)
    clientNameList.innerHTML = ''
    c.currentEngagement.acl.forEach((user) => {
      if (user.role === 'CLIENT') {
        let userName = Forge.build({dom: 'p', text: user.userInfo.firstName + ' ' + user.userInfo.lastName, style: 'font-size: 14px; line-height: 17px; color: #363A3C; margin: 0px 4px;'})
        let userTitle = Forge.build({ dom: 'p', text: user.userInfo.jobTitle, style: 'font-size: 14px; line-height: 17px; margin-left: 4px;' })
        clientNameList.appendChild(userName)
        clientNameList.appendChild(userTitle)
      }
    })
  }

  var registerEvents = function () {
    log('Team: Registering Event Listeners ...')
  }

  const overviewData = {
    status: {id: 'engOverview-status', data: ''},
    todo: {id: 'engOverview-todo', data: ''},
    todoBar: {id: 'engOverview-todoBar', data: ''},
    totalTodo: {id: 'engOverview-totalTodo', data: ''},
    clientTodo: {id: 'engOverview-clientTodo', data: ''},
    clientTodoBar: {id: 'engOverview-clientTodoBar', data: ''},
    dueDate: {id: 'engOverview-dueDate', data: ''},
    auditorTeam: {id: 'engOverview-auditorTeam', data: ''},
    overDueTodo: {id: 'engOverview-overDueTodo', data: ''},
    outstandingTodo: {id: 'engOverview-outstandingTodo', data: ''},
    clientTeam: {id: 'engOverview-clientTeam', data: ''},
    overdueDocs: {id: 'engOverview-overdueDocs', data: ''},
    outstandingDocs: {id: 'engOverview-outstandingDocs', data: ''},
    totalDocs: {id: 'engOverview-totalDocs', data: ''}
  }

  this.updateOverview = (data) => {
    let engagementData = Utility.convertEngagement(data)

    let status = engagementData.status
    let totalToDo = data.todos.length
    let completedToDo = engagementData.completedToDoNumber
    let outstandingTodo = engagementData.outstandingToDo
    let overdueToDo = engagementData.overdueToDo
    let dueDate = engagementData.dueDate
    let totalDocumentReq = engagementData.totalDoc
    let uploadedDocuments = engagementData.completedDocsNumber
    let outstandingDoc = engagementData.outstandingDoc
    let overdueDocs = engagementData.overdueDoc

    let statusText = document.getElementById(overviewData.status.id)
    let todoText = document.getElementById(overviewData.todo.id)
    let todoBarText = document.getElementById(overviewData.todoBar.id)
    let clientTodoText = document.getElementById(overviewData.clientTodo.id)
    let clientTodoBarText = document.getElementById(overviewData.clientTodoBar.id)
    let dueDateText = document.getElementById(overviewData.dueDate.id)
    let overDueTodoNum = document.getElementById(overviewData.overDueTodo.id)
    let outstandingTodoNum = document.getElementById(overviewData.outstandingTodo.id)
    let totalTodoNum = document.getElementById(overviewData.totalTodo.id)
    let overdueDocsNum = document.getElementById(overviewData.overdueDocs.id)
    let outstandingDocsNum = document.getElementById(overviewData.outstandingDocs.id)
    let totalDocsNum = document.getElementById(overviewData.totalDocs.id)

    // Engagment Status
    if (status && status !== overviewData.status.data) {
      overviewData.status.data = status
      statusText.innerHTML = status.toLowerCase()
    } else {
      log('No need to update status')
    }

    // Completed ToDo
    if (totalToDo !== 0) {
      if (completedToDo !== overviewData.todo.data || totalToDo !== overviewData.totalTodo.data) {
        overviewData.todo.data = completedToDo
        let text = `My Completed To-Dos: ${Math.round(completedToDo / totalToDo * 100)}% (${completedToDo}/${totalToDo})`
        todoText.innerHTML = text
        todoBarText.style.width = `${completedToDo / totalToDo * 100}%`
      } else {
        log('No need to update completedToDo')
      }
    } else {
      let text = `My Completed To-Dos: 0% (0/0)`
      todoText.innerHTML = text
      todoBarText.style.width = `0%`
    }

    // Completed Doc
    if (uploadedDocuments !== 0 && totalDocumentReq !== 0) {
      if (uploadedDocuments !== overviewData.clientTodo.data || totalDocumentReq !== overviewData.totalDocs.data) {
        overviewData.clientTodo.data = uploadedDocuments
        overviewData.totalDocs.data = totalDocumentReq
        let text = `Client Complete Documents: ${Math.round(uploadedDocuments / totalDocumentReq * 100)}% (${uploadedDocuments}/${totalDocumentReq})`
        clientTodoText.innerHTML = text
        clientTodoBarText.style.width = `${uploadedDocuments / totalDocumentReq * 100}%`
      } else {
        log('No need to update uploadedDocuments')
      }
    } else {
      let text = `Client Complete Documents: 0% (0/0)`
      clientTodoText.innerHTML = text
      clientTodoBarText.style.width = '0'
    }

    // Due date
    if (dueDate !== overviewData.dueDate.data) {
      overviewData.dueDate.data = dueDate
      var json = {
        military: false,
        month: {type: 'integer', length: 'short'},
        day: {append: false},
        output: 'MM/DD/YYYY'
      }
      dueDateText.value = Utility.dateProcess(data.dueDate, json)
    } else {
      log('No need to update dueDate')
    }

    // Over Due Todos
    if (overdueToDo !== overviewData.overDueTodo.data) {
      overviewData.overDueTodo.data = overdueToDo
      overDueTodoNum.innerHTML = overdueToDo
      if (overdueToDo > 0) {
        overDueTodoNum.parentNode.classList.add('terColor')
        let overTodoAlert = Forge.build({dom: 'div', class: 'auvicon-caution', style: 'display: inline-block; height: 38px; padding: 3px; vertical-align: top;'})
        overDueTodoNum.appendChild(overTodoAlert)
      } else {
        overDueTodoNum.parentNode.classList.remove('terColor')
      }
    } else {
      log('No need to update overdueToDo')
    }

    // Outstanding Todos
    if (outstandingTodo !== overviewData.outstandingTodo.data) {
      overviewData.outstandingTodo.data = outstandingTodo
      outstandingTodoNum.innerHTML = outstandingTodo
    } else {
      log('No need to update outstandingTodo')
    }

    // Total Todos
    if (totalToDo !== overviewData.totalTodo.data) {
      overviewData.totalTodo.data = totalToDo
      totalTodoNum.innerHTML = totalToDo
    } else {
      log('No need to update totalToDo')
    }

    // Overdue Docs
    if (overdueDocs !== overviewData.overdueDocs.data) {
      overviewData.overdueDocs.data = overdueDocs
      overdueDocsNum.innerHTML = overdueDocs
      if (overdueToDo > 0) {
        overdueDocsNum.parentNode.classList.add('terColor')
        let overTodoAlert = Forge.build({dom: 'div', class: 'auvicon-caution', style: 'display: inline-block; height: 38px; padding: 3px; vertical-align: top;'})
        overdueDocsNum.appendChild(overTodoAlert)
      } else {
        overdueDocsNum.parentNode.classList.remove('terColor')
      }
    } else {
      log('No need to update overdueDocs')
    }

    // Outstanding Docs
    if (outstandingDoc !== overviewData.outstandingDocs.data) {
      overviewData.outstandingDocs.data = outstandingDoc
      outstandingDocsNum.innerHTML = outstandingDoc
    } else {
      log('No need to update outstandingDoc')
    }

    // Total Docs
    if (totalDocumentReq) {
      overviewData.totalDocs.data = totalDocumentReq
      totalDocsNum.innerHTML = totalDocumentReq
    } else {
      log('No need to update totalDocumentReq')
    }
  }

  // Activity Feed
  this.updateActivity = (activity) => {
    const recentActivity = document.getElementById('engOverview-recentActivity')
    while (recentActivity.firstChild) {
      recentActivity.removeChild(recentActivity.firstChild)
    }
    for (let i = 0; i < activity.length; i++) {
      if (i < 4) {
        let activityContainer = Forge.build({ dom: 'div', style: 'display: block;', class: 'activitytext' })
        let activityText = Forge.build({ dom: 'div', text: activity[i].text, style: 'display: inline-block;' })
        let activiyTime = Forge.build({ dom: 'div', text: activity[i].time, style: 'display: inline-block; float: right;' })

        activityContainer.appendChild(activityText)
        activityContainer.appendChild(activiyTime)
        recentActivity.appendChild(activityContainer)
      }
    }
  }

  this.buildContent = function () {
    var parentSection = Forge.build({dom: 'section', class: 'ui accordion', style: 'background: #fff; margin-bottom: 8px; box-shadow: 0 2px 4px 0 rgba(0,0,0,0.1); padding: 12px 24px; border-radius: 4px; color: #757575; '})

    /*
     * Container for the Engagement Overview
     * Note - class: 'title' is used for semantic-ui
     */
    var engOverview = Forge.build({dom: 'div', class: 'title', style: 'height: 50px; width: 100%; vertical-align: middle;'})

    /* Container and functionlity for Status of Engagement */
    var overviewTitle = Forge.build({dom: 'p', text: 'Engagement Overview', style: 'display: inline-block; padding-right: 24px; margin: 0; font-size: 20px; font-weight: 400; line-height: 24px;'})
    var engagementStatus = Forge.build({dom: 'div', id: overviewData.status.id, text: 'Not Set', style: `text-transform: capitalize; width:65px; height:16px; display: inline-block; background:#4C8496; color: #fff; text-align: center; font-size: 11px; font-weight: bold; border-radius: 10px; margin: 0px 24px 0px 0px;`})

    /* Container for Engagement Overview items */
    var engOverRightFloat = Forge.build({dom: 'dov', style: 'float: right;'})

    /* Container and functionality for Completed To-Do list */
    var myOutstandingBox = Forge.build({dom: 'div', style: 'display: inline-block; font-size: 12px; text-align: right; line-height: 15px; color: #9B9B9B; margin-right: 16px; vertical-align: top;'})
    var selfTodo = Forge.build({dom: 'p', id: overviewData.todo.id, text: 'Not Set'})
    var selfTotalBar = Forge.build({dom: 'div', class: 'auv-loadingBar'})
    var selfDoneBar = Forge.build({dom: 'div', id: overviewData.todoBar.id, class: 'auv-loadingToDo', style: `width:0%`})
    myOutstandingBox.appendChild(selfTodo)
    myOutstandingBox.appendChild(selfTotalBar)
    selfTotalBar.appendChild(selfDoneBar)

    /* Container and functionality for Completed Documents list */
    var clientOutstandingBox = Forge.build({dom: 'div', style: 'display: inline-block; font-size: 12px; text-align: right; line-height: 15px; color: #9B9B9B; margin-right: 16px; vertical-align: top;'})
    var clientTodo = Forge.build({dom: 'p', id: overviewData.clientTodo.id, text: `Client Complete Documents: NOT SET`})
    var clientTotalBar = Forge.build({dom: 'div', class: 'auv-loadingBar'})
    var clientDoneBar = Forge.build({dom: 'div', id: overviewData.clientTodoBar.id, class: 'auv-loadingDocs', style: `width: 0%`})
    clientOutstandingBox.appendChild(clientTodo)
    clientOutstandingBox.appendChild(clientTotalBar)
    clientTotalBar.appendChild(clientDoneBar)

    /* Due date of Engagment, need to work out the actual functionality here */
    var engagementDueDate = Forge.buildInputDropdownCalendar({label: '', 'id': overviewData.dueDate.id, placeholder: '', width: '143px'})
    engagementDueDate.style = 'display: inline-block; padding: 8px; margin-right: 24px;'
    $(function () {
      $(`#${overviewData.dueDate.id}`).datepicker()
    })
    engagementDueDate.onchange = function (e) {
      const formatDate = function (date, prettyDate) {
        if (prettyDate === null || prettyDate === undefined) {
          return (date === null ? '' : Utility.getFormattedDate(date, '/'))
        }

        return moment(date).format('dddd, MMM Do')
      }

      const checkDueDate = function ({ dueDate }) {
        var now = new Date(formatDate(new Date()))

        return (dueDate >= now)
      }

      setTimeout(function () {
        var data = {
          dueDate: new Date(e.target.value)
        }
        var checkDue = checkDueDate(data)
        log('dueDate : ' + checkDue)
        if (!checkDue) {
          // go back to previous duedate
          var json = {
            military: false,
            month: {type: 'integer', length: 'short'},
            day: {append: false},
            output: 'MM/DD/YYYY'
          }
          engagementDueDate.value = Utility.dateProcess(c.currentEngagement.dueDate, json)
        } else {
          data.engagementID = c.currentEngagement._id
          c.updateEngagement(data)
        }
      }, 500)
    }

    /* Expands out engagment to show client and autior teams and recient activity */
    var engagementExpand = Forge.build({dom: 'td', class: 'auvicon-forward', style: 'display: inline-block; vertical-align: middle; transition: all 0.3s;'})
    engagementExpand.addEventListener('click', () => {
      document.getElementById('engagementDetails').classList.toggle('active')
      engagementExpand.classList.toggle('overviewOpen')
    })

    engOverview.appendChild(overviewTitle)
    engOverview.appendChild(engagementStatus)
    engOverview.appendChild(engOverRightFloat)
    engOverRightFloat.appendChild(myOutstandingBox)
    engOverRightFloat.appendChild(clientOutstandingBox)
    engOverRightFloat.appendChild(engagementDueDate)
    engOverRightFloat.appendChild(engagementExpand)

    /* Contains the details, both Team and Activity for the Engagement */
    var engOverviewDetails = Forge.build({dom: 'div', class: 'content', id: 'engagementDetails'})

    /* Container to Team Info */
    var engOverDetailsTeams = Forge.build({dom: 'div', style: 'float: left; clear: both;'})

    /* Audit team Container */
    const auditTeam = Forge.build({dom: 'div', class: '', style: 'width: 309px; display: inline-block; margin-right: 8px;'})
    const auditNames = Forge.build({dom: 'div', class: 'auv-engOverCard', style: 'display: inline-block; height: 292px; width: 180px; margin-right: 7px; vertical-align: top; padding: 4px 16px;'})
    const auditTeamTitle = Forge.build({dom: 'div', style: 'color: #314052; font-size: 16px; padding: 8px 0px; margin-bottom: 8px; border-bottom: 1px solid rgba(151,151,151,0.29);', text: 'Audit Team'})
    const auditNameList = Forge.build({dom: 'div', id: overviewData.auditorTeam.id, style: 'overflow-y: auto; height: 238px; width: 164px;'})
    auditNames.appendChild(auditTeamTitle)
    auditNames.appendChild(auditNameList)

    /* Container for Audit info of To-Dos */
    var auditNumber = Forge.build({dom: 'div', style: 'width: 122px; display: inline-block;'})

    var overTodo = Forge.build({dom: 'div', class: 'auv-engOverCard', style: 'height: 92px; padding: 8px; vertical-align: top; margin-bottom: 8px;'})
    var overTodoNumber = Forge.build({dom: 'p', id: overviewData.overDueTodo.id, text: 'NOT SET', style: `font-size: 40px; margin: 0; height: 48px;`})
    var overTodoAlert = Forge.build({dom: 'div', class: 'auvicon-caution', style: 'display: inline-block; height: 38px; padding: 3px; vertical-align: top;'})
    var overTodoText = Forge.build({dom: 'p', text: 'Overdue To-Dos', style: 'font-size: 12px;'})
    overTodo.appendChild(overTodoNumber)
    overTodoNumber.appendChild(overTodoAlert)
    overTodo.appendChild(overTodoText)

    var outTodo = Forge.build({dom: 'div', class: 'auv-engOverCard', style: 'height: 92px; padding: 8px; vertical-align: top; margin-bottom: 8px;'})
    var outTodoNumber = Forge.build({dom: 'p', id: overviewData.outstandingTodo.id, text: 'NOT SET', style: 'font-size: 40px; margin: 0; height: 48px;'})
    var outTodoText = Forge.build({dom: 'p', text: 'Outstanding To-Dos', style: 'font-size: 12px;'})
    outTodo.appendChild(outTodoNumber)
    outTodo.appendChild(outTodoText)

    var totalTodo = Forge.build({dom: 'div', class: 'auv-engOverCard', style: 'height: 92px; padding: 8px; vertical-align: top;'})
    var totalTodoNumber = Forge.build({dom: 'p', id: overviewData.totalTodo.id, text: 'NOT SET', style: 'font-size: 40px; margin: 0; height: 48px;'})
    var totalTodoText = Forge.build({dom: 'p', text: 'Total To-Dos', style: 'font-size: 12px;'})
    totalTodo.appendChild(totalTodoNumber)
    totalTodo.appendChild(totalTodoText)

    auditTeam.appendChild(auditNames)
    auditTeam.appendChild(auditNumber)
    auditNumber.appendChild(overTodo)
    auditNumber.appendChild(outTodo)
    auditNumber.appendChild(totalTodo)

    /* Client Team container */
    const clientTeam = Forge.build({dom: 'div', class: '', style: 'width: 309px; display: inline-block; margin-right: 8px;'})
    const clientNames = Forge.build({dom: 'div', class: 'auv-engOverCard', style: 'display: inline-block; height: 292px; width: 180px; margin-right: 7px; vertical-align: top; padding: 4px 16px;'})
    const clientTeamTitle = Forge.build({dom: 'div', style: 'color: #314052; font-size: 16px; padding: 8px 0px; margin-bottom: 8px; border-bottom: 1px solid rgba(151,151,151,0.29);', text: 'Client Team'})
    const clientNameList = Forge.build({dom: 'div', id: overviewData.clientTeam.id, style: 'overflow-y: auto; height: 238px; width: 164px;'})
    clientNames.appendChild(clientTeamTitle)
    clientNames.appendChild(clientNameList)

    /* Container for Client info of Docs */
    var clientNumber = Forge.build({dom: 'div', style: 'width: 122px; display: inline-block;'})

    var overDocs = Forge.build({dom: 'div', class: 'auv-engOverCard', style: 'height: 92px; width: 122px; padding: 8px; vertical-align: top; margin-bottom: 8px;'})
    var overDocsNumber = Forge.build({dom: 'p', id: overviewData.overdueDocs.id, text: overviewData.overdueDocs.data, style: `font-size: 40px; margin: 0; height: 48px;`})
    var overDocsAlert = Forge.build({dom: 'span', class: 'auvicon-caution', style: 'display: inline-block; height: 38px; padding: 3px; vertical-align: top;'})
    var overDocsText = Forge.build({dom: 'p', text: 'Overdue Documents', style: 'font-size: 12px;'})
    overDocs.appendChild(overDocsNumber)
    overDocsNumber.appendChild(overDocsAlert)
    overDocs.appendChild(overDocsText)

    var outDocs = Forge.build({dom: 'div', class: 'auv-engOverCard', style: 'height: 92px; width: 122px; padding: 8px; vertical-align: top; margin-bottom: 8px;'})
    var outDocsNumber = Forge.build({dom: 'p', id: overviewData.outstandingDocs.id, text: overviewData.totalDocs.data - overviewData.clientTodo.data, style: 'font-size: 40px; margin: 0; height: 48px;'})
    var outDocsText = Forge.build({dom: 'p', text: 'Outstanding Documents', style: 'font-size: 12px;'})
    outDocs.appendChild(outDocsNumber)
    outDocs.appendChild(outDocsText)

    var totalDoc = Forge.build({dom: 'div', class: 'auv-engOverCard', style: 'height: 92px; width: 122px; padding: 8px; vertical-align: top;'})
    var totalDocNumber = Forge.build({dom: 'p', id: overviewData.totalDocs.id, text: overviewData.totalDocs.data, style: 'font-size: 40px; margin: 0; height: 48px;'})
    var totalDocText = Forge.build({dom: 'p', text: 'Total Documents', style: 'font-size: 12px;'})
    totalDoc.appendChild(totalDocNumber)
    totalDoc.appendChild(totalDocText)

    clientTeam.appendChild(clientNames)
    clientTeam.appendChild(clientNumber)
    clientNumber.appendChild(overDocs)
    clientNumber.appendChild(outDocs)
    clientNumber.appendChild(totalDoc)

    /* Container for Audit Activity */
    /* These are dummy data */
    var recentActivityContainer = Forge.build({dom: 'div', class: 'auv-engOverCard', style: 'width: auto; height: 292px; overflow: hidden; min-width: 300px;'})
    var recentActivityTitle = Forge.build({ dom: 'div', text: 'Recent Activity', class: 'recent-activity', style: '' })
    var recentActivityBody = Forge.build({ dom: 'div', id: 'engOverview-recentActivity', style: 'padding: 0 16px;' })

    recentActivityContainer.appendChild(recentActivityTitle)
    recentActivityContainer.appendChild(recentActivityBody)

    engOverviewDetails.appendChild(engOverDetailsTeams)
    engOverDetailsTeams.appendChild(auditTeam)
    engOverDetailsTeams.appendChild(clientTeam)
    engOverviewDetails.appendChild(recentActivityContainer)

    parentSection.appendChild(engOverview)
    parentSection.appendChild(engOverviewDetails)

    return parentSection
  }

  return this
}

ComponentEngagementOverview.prototype = Object.create(componentTemplate.prototype)
ComponentEngagementOverview.prototype.constructor = ComponentEngagementOverview

module.exports = ComponentEngagementOverview
