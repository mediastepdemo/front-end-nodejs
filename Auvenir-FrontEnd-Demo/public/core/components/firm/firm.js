'use strict'

import ComponentTemplate from '../ComponentTemplate'
import Utility from '../../../js/src/utility_c'
import log from '../../../js/src/log'
import Forge from '../../Forge'
import styles from './firm.css'

var Component_Firm = function (ctrl, container, owner) {
  ComponentTemplate.call(this, ctrl, container, owner)

  this.name = 'Firm'
  var parentDOM = container
  var c = ctrl
  var self = this
  var text = c.lang.firmComponent
  var checkboxFlagOne = false
  var checkboxFlagTwo = false
  var checkboxFlagThree = false

  this.isComplete = function () {
    return true
  }

  /*
   * Local instances of the field values
  */
  var fields = {
    name: {id: 'firm-name', type: 'text', value: '', model: {name: 'Firm', field: 'name'}},
    // nameChange: {id: 'firm-nameChange', type: 'boolean', value: null, model: {name: 'Firm', field: 'nameChange'}},
    // previousName: {id: 'firm-previousName', type: 'text', value: '', model: {name: 'Firm', field: 'previousName'}},
    website: {id: 'firm-website', type: 'text', value: '', model: {name: 'Firm', field: 'website'}},
    size: {id: 'firm-size', type: 'text', value: '', model: {name: 'Firm', field: 'size'}},
    phone: {id: 'firm-phone', type: 'text', value: '', model: {name: 'Firm', field: 'phone'}},
    logo: {id: 'firm-logo', type: 'text', value: '', model: {name: 'Firm', field: 'logo'}},
    address: {
      id: 'firm-fullAddress',
      model: {name: 'Firm', field: 'address'},
      unit: {id: 'firm-unit', type: 'text', value: '', model: {name: 'Firm', field: 'address.unit'}},
      streetAddress: {id: 'firm-streetAddress', type: 'text', value: '', model: {name: 'Firm', field: 'address.streetAddress'}},
      city: {id: 'firm-city', type: 'text', value: '', model: {name: 'Firm', field: 'address.city'}},
      stateProvince: {id: 'firm-stateProvince', type: 'text', value: '', model: {name: 'Firm', field: 'address.stateProvince'}},
      // country: {id: 'firm-country', type: 'text', value: '', model: {name: 'Firm', field: 'address.country'}},
      postalCode: {id: 'firm-postalCode', type: 'text', value: '', model: {name: 'Firm', field: 'address.postalCode'}}
    },
    affiliated: {id: 'firm-affiliated', type: 'boolean', value: false, model: {name: 'Firm', field: 'affiliated'}},
    affiliatedFirmName: {id: 'firm-affiliated-name', type: 'text', value: '', model: { name: 'Firm', field: 'affiliatedFirmName' }},
    logoDisplayAgreed: {id: 'firm-logo-header', type: 'boolean', value: true, model: { name: 'Firm', field: 'logoDisplayAgreed' }}
  }

  var employeeList = [ {text: '1'}, {text: '2-3'}, {text: '4-10'}, {text: '11-20'}, {text: '21-50'}, {text: '50+'} ]
  var provinceList = [
    {text: 'Ontario'},
    {text: 'Quebec'},
    {text: 'British Columbia'},
    {text: 'Alberta'},
    {text: 'Manitoba'},
    {text: 'Saskatchewan'},
    {text: 'Nova Scotia'},
    {text: 'Newfoundland'},
    {text: 'New Brunswick'},
    {text: 'Prince Edward Island'},
    {text: 'Northwest Territories'},
    {text: 'Nunavut'},
    {text: 'Yukon'} ]

  /**
   * @return a DOM object
   */
  var buildContent = function (settings) {
    if (!parentDOM) {
      return
    }

    var componentContentDiv = Forge.build({'dom': 'div'})

    var firmTitle = Forge.build({'dom': 'h3', 'text': text.header, 'class': 'component-title'})

    var youInputContain = Forge.build({'dom': 'div', 'class': 'onB-inputContainer'})
    var firmNameInput = Forge.buildInput({'label': 'Firm Name', 'errorText': text.nameError, 'id': fields.name.id, 'placeholder': ''})
    // var nameChangeContainer = Forge.build({'dom': 'div'})
    // var checkBoxOne = Forge.build({'dom': 'img', 'id': fields.nameChange.id, 'src': 'images/icons/checkbox-off.svg', 'style': 'cursor: pointer; margin-right: 5px'})
    // var nameChangeText = Forge.build({ 'dom': 'label', 'text': text.checkOne, 'style': 'font-weight: 300; margin-bottom: 19px' })
    // var firmNameChangeInput = Forge.buildInput({'label': text.prevName, 'errorText': 'Not a valid firm name.', 'id': fields.previousName.id, 'placeholder': ''})
    var websiteInput = Forge.buildInput({'label': text.website, 'errorText': 'Not a valid website.', 'id': fields.website.id, 'placeholder': ''})
    var phoneInput = Forge.buildInput({'label': text.phone, 'errorText': 'Not a valid phone number.', 'id': fields.phone.id, 'placeholder': '', 'maxlength': '10'})
    // firmNameChangeInput.style.display = 'none'

    // nameChangeContainer.appendChild(checkBoxOne)
    // nameChangeContainer.appendChild(nameChangeText)
    // nameChangeContainer.appendChild(firmNameChangeInput)

    var addressHolder = Forge.build({'dom': 'form', 'id': 'firm-mapForm', 'class': 'firm-addressHolder'})
    var streetAddress = Forge.buildInput({'label': text.streetAddress, 'errorText': 'Not a valid address.', 'id': fields.address.streetAddress.id, 'placeholder': ''})
    var unitInput = Forge.buildInput({'label': text.suite, 'errorText': 'Not a valid Unit or Suite.', 'id': fields.address.unit.id, 'placeholder': ''})
    var cityInput = Forge.buildInput({'label': text.city, 'errorText': 'Not a valid City.', 'id': fields.address.city.id, 'placeholder': ''})
    var provinceInput = Forge.buildInputDropdown({'label': text.province, 'errorText': 'Not a valid Province.', 'id': fields.address.stateProvince.id, 'placeholder': '', 'list': provinceList})
    // var countryInput = Forge.buildInput({'label': text.country, 'errorText': 'Not a valid Country.', 'id': fields.address.country.id, 'placeholder': ''})
    var postalInput = Forge.buildInput({'label': text.postal, 'errorText': 'Not a valid Postal or Zip Code.', 'id': fields.address.postalCode.id, 'placeholder': ''})

    addressHolder.appendChild(streetAddress)
    addressHolder.appendChild(unitInput)
    addressHolder.appendChild(cityInput)
    addressHolder.appendChild(provinceInput)
    // addressHolder.appendChild(countryInput)
    addressHolder.appendChild(postalInput)

    var sizeInput = Forge.buildInputDropdown({'label': text.numberEmployees, 'id': fields.size.id, 'placeholder': '', 'list': employeeList, 'style': 'margin-bottom:0px', errorText: 'Please fill out number of employees'})

    var photoDiv = Forge.build({'dom': 'div', 'class': 'firm-photoSection'})
    var logoImg = Forge.build({'dom': 'img', 'id': fields.logo.id, 'src': '', 'class': 'component-uploadedImg', 'style': 'display: none;'})
    var photoFieldEditForm = Forge.build({'dom': 'form', 'action': 'images/profilePhotos', 'method': 'POST', 'enctype': 'multipart/form-data'})
    var photoFieldEditInput = Forge.build({'dom': 'input', 'type': 'file', 'name': 'image', 'id': 'firmNewPhotoUpload', 'class': 'card-account-photoEdit', 'accept': 'image/*'})
    photoFieldEditInput.webkitdirectory = false
    photoFieldEditInput.multiple = false
    photoFieldEditForm.appendChild(photoFieldEditInput)
    var myWidgetLoading = Forge.build({'dom': 'div', 'class': 'card-account-uploadLoading', 'id': 'firm-logoUploading', 'style': 'display:none;'})

    var photoEmptyContainer = Forge.build({'dom': 'div', 'id': 'firm-emptyPic', 'class': 'component-photoContainer', 'style': ''})
    var photoEmptyCamera = Forge.build({'dom': 'span', 'id': 'firm-emptyCamera', 'class': 'auvicon-camera component-emptyPhoto', 'style': 'display: none'})
    var uploadCoverBtn = Forge.buildBtn({'text': text.logoBtn, 'type': 'default', 'margin': '24px auto 16px', display: 'block'}).obj
    var headerLogoCheckContainer = Forge.build({dom: 'div'})
    var checkBoxThree = Forge.build({'dom': 'img', 'id': fields.logoDisplayAgreed.id, 'checked': '', 'src': 'images/icons/checkbox-off.svg', 'style': 'cursor: pointer; margin: 5px 5px 0px 0px'})
    var headerLogoText = Forge.build({ 'dom': 'label', 'text': text.logoWarn, 'class': 'firm-headerLogo-label'})
    var photoErrIcon = Forge.build({'dom': 'span', 'class': 'auvicon-caution component-photoErr'})
    var photoErrMsgContainer = Forge.build({'dom': 'div', 'style': 'height: 20px; width: 170px; display: none;'})
    var photoErrMsg = Forge.build({'dom': 'p', 'class': 'component-photoErrMsg'})
    photoErrMsgContainer.appendChild(photoErrIcon)
    photoErrMsgContainer.appendChild(photoErrMsg)
    headerLogoCheckContainer.appendChild(checkBoxThree)
    headerLogoCheckContainer.appendChild(headerLogoText)
    checkBoxThree.disabled = true

    var continueBtn = Forge.buildBtn({'text': c.lang.continue, 'type': 'primary', 'margin': '48px auto 0px', id: 'onboard-firm-continue'}).obj
    continueBtn.disabled = true
    continueBtn.style.display = 'block'

    var agreementContainer = Forge.build({'dom': 'div', 'style': 'margin-left: 78px;'})
    var checkBoxTwo = Forge.build({'dom': 'img', 'id': fields.affiliated.id, 'src': 'images/icons/checkbox-off.svg', 'style': 'cursor: pointer; margin-right: 5px'})
    var infoTag = Forge.build({dom: 'div', class: 'firm-iTag', 'text': 'i', id: 'firm-infoTag'})
    var agreementText = Forge.build({ 'dom': 'label', 'text': text.affiliated, 'style': 'font-weight: 300;' })
    var affiliatedField = Forge.buildInput({ 'label': text.affiliatedName, 'id': fields.affiliatedFirmName.id, 'errorText': 'Not a valid firm name.', 'placeholder': '' })
    affiliatedField.style.marginTop = '17px'
    affiliatedField.style.display = 'none'
    infoTag.style.display = 'none'

    youInputContain.appendChild(firmNameInput)
    // youInputContain.appendChild(nameChangeContainer)
    youInputContain.appendChild(websiteInput)
    youInputContain.appendChild(addressHolder)
    youInputContain.appendChild(sizeInput)
    youInputContain.appendChild(phoneInput)
    photoEmptyContainer.appendChild(photoEmptyCamera)
    photoEmptyContainer.appendChild(logoImg)
    photoEmptyContainer.appendChild(photoFieldEditForm)
    photoEmptyContainer.appendChild(myWidgetLoading)
    photoDiv.appendChild(photoEmptyContainer)
    photoDiv.appendChild(uploadCoverBtn)
    photoDiv.appendChild(headerLogoCheckContainer)
    photoDiv.appendChild(photoErrMsgContainer)
    agreementContainer.appendChild(checkBoxTwo)
    agreementContainer.appendChild(agreementText)
    agreementContainer.appendChild(infoTag)
    agreementContainer.appendChild(affiliatedField)

    componentContentDiv.appendChild(firmTitle)
    componentContentDiv.appendChild(photoDiv)
    componentContentDiv.appendChild(youInputContain)
    componentContentDiv.appendChild(agreementContainer)

    componentContentDiv.appendChild(continueBtn)

    parentDOM.appendChild(componentContentDiv)

    infoTag.addEventListener('click', function () {
      c.displayFirmAffiliationModal()
    })

    self.loadUserData()

    // Everytime it's out of focus, it will check that field's value and update DOM's value.
    /**
     * Each Input has its validation is passed here instead of having this function for each input
     * Vallidation shows error on blur, while on each keyup it will only attach a pass/fail class
     * that gives no user feedback. There is propbably a better way to do this though.
     */
    const setInputValidation = (element, testType, array) => {
      element.addEventListener('blur', () => {
        if (testType()) {
          array.value = element.value
          updateToDB(array)
        } else {
          element.classList.add('auv-error')
        }
      })
      element.addEventListener('keyup', () => {
        if (testType()) {
          element.classList.remove('auv-error')
          element.classList.remove('auv-validationFailed')
        } else {
          element.classList.add('auv-validationFailed')
        }
        self.checkInputValidation()
      })
    }

    /*
     * Functions containing the validation to run for each field.
     */
    const firmNameValidation = () => { return (Utility.REGEX.businessName.test(firmNameInput.children[1].value)) }
    const phoneInputValidation = () => { return (Utility.REGEX.phone.test(phoneInput.children[1].value)) }

    /*
     * Function that will add validation to fields.
     * 1st param: Element that validation is to be added to.
     * 2nd param: Function with validaion from above
     * 3rd param: JSON to be updated if value is correct
     */
    setInputValidation(firmNameInput.children[1], firmNameValidation, fields.name)
    setInputValidation(phoneInput.children[1], phoneInputValidation, fields.phone)

    postalInput.children[1].addEventListener('blur', function () {
      var postalField = document.getElementById(fields.address.postalCode.id)
      postalField.classList.remove('auv-error')
      if (postalField.value.length === 5) {
        if (Utility.REGEX.zipCode.test(postalField.value)) {
          fields.address.postalCode.value = postalField.value
          updateToDB(fields.address)
        } else {
          postalField.classList.add('auv-error')
        }
        self.checkInputValidation()
      } else if (postalField.value.length === 6) {
        if (Utility.REGEX.postalCode.test(postalField.value)) {
          fields.address.postalCode.value = postalField.value
          updateToDB(fields.address)
        } else {
          postalField.classList.add('auv-error')
        }
        self.checkInputValidation()
      } else {
        postalField.classList.add('auv-error')
      }
    })
    postalInput.children[1].addEventListener('keyup', () => {
      var postalField = document.getElementById(fields.address.postalCode.id)
      if (postalField.value.length === 5) {
        if (Utility.REGEX.zipCode.test(postalField.value)) {
          postalField.classList.remove('auv-validationFailed')
        } else {
          postalField.classList.add('auv-validationFailed')
        }
      } else if (postalField.value.length === 6) {
        if (Utility.REGEX.postalCode.test(postalField.value)) {
          postalField.classList.remove('auv-validationFailed')
        } else {
          postalField.classList.add('auv-validationFailed')
        }
      } else {
        postalField.classList.add('auv-validationFailed')
      }
      self.checkInputValidation()
    })

    const setAddressInputValidation = (element, testType, array) => {
      element.addEventListener('blur', () => {
        if (testType()) {
          array.value = element.value
          updateToDB(fields.address)
        } else {
          element.classList.add('auv-error')
        }
      })
      element.addEventListener('keyup', () => {
        if (testType()) {
          element.classList.remove('auv-error')
          element.classList.remove('auv-validationFailed')
        } else {
          element.classList.add('auv-validationFailed')
        }
        self.checkInputValidation()
      })
    }

    const addressValidation = () => { return (Utility.REGEX.businessName.test(streetAddress.children[1].value)) }
    const unitValidation = () => { return (Utility.REGEX.alphanumericName.test(unitInput.children[1].value)) }
    const cityValidation = () => { return (Utility.REGEX.city.test(cityInput.children[1].value)) }
    const provinceValidation = () => { return (Utility.REGEX.stateProvince.test(provinceInput.children[1].value)) }
    // const countryInputValidation = () => { return (Utility.REGEX.country.test(countryInput.children[1].value)) }

    setAddressInputValidation(streetAddress.children[1], addressValidation, fields.address.streetAddress)
    setAddressInputValidation(unitInput.children[1], unitValidation, fields.address.unit)
    setAddressInputValidation(cityInput.children[1], cityValidation, fields.address.city)
    setAddressInputValidation(provinceInput.children[1], provinceValidation, fields.address.stateProvince)
    // setAddressInputValidation(countryInput.children[1], countryInputValidation, fields.address.country)

    sizeInput.children[2].addEventListener('blur', () => {
      setTimeout(() => {
        var sizeField = document.getElementById(fields.size.id)
        if (sizeField.value.trim() !== '') {
          fields.size.value = sizeField.value
          updateToDB(fields.size)
        }
        self.checkInputValidation()
      }, 300)
    })

    websiteInput.children[1].addEventListener('blur', function () {
      var websiteField = document.getElementById(fields.website.id)
      if (Utility.REGEX.urlNoHTTPWWW.test(websiteField.value)) {
        // websiteInput.children[1].classList.remove('auv-validationFailed')
        websiteInput.children[1].classList.remove('auv-error')
        fields.website.value = websiteField.value
        updateToDB(fields.website)
      } else {
        // websiteInput.children[1].classList.add('auv-validationFailed')
        websiteInput.children[1].classList.add('auv-error')
      }
      self.checkInputValidation()
    })

    checkBoxTwo.addEventListener('click', function () {
      if (!checkboxFlagTwo) {
        checkboxFlagTwo = true
        document.getElementById(fields.affiliated.id).src = 'images/icons/checkbox-on.svg'
        affiliatedField.style.display = 'inherit'
        infoTag.style.display = 'inherit'
      } else {
        checkboxFlagTwo = false
        document.getElementById(fields.affiliated.id).src = 'images/icons/checkbox-off.svg'
        affiliatedField.style.display = 'none'
        infoTag.style.display = 'none'
      }
      if (checkboxFlagTwo) {
        fields.affiliated.value = 'true'
      } else {
        fields.affiliated.value = 'false'
      }

      affiliatedField.children[1].addEventListener('blur', function () {
        self.checkInputValidation()
        updateToDB(fields.affiliatedFirmName)
      })

      affiliatedField.children[1].addEventListener('keyup', function () {
        var affiliatedCom = document.getElementById(fields.affiliatedFirmName.id)
        affiliatedCom.classList.remove('auv-error')
        if (Utility.REGEX.name.test(affiliatedCom.value)) {
          fields.affiliatedFirmName.value = affiliatedCom.value
        } else {
          affiliatedCom.classList.add('auv-error')
        }
        self.checkInputValidation()
      })

      updateToDB(fields.affiliated)
    })

    checkBoxThree.addEventListener('click', function () {
      if (!checkboxFlagThree) {
        checkboxFlagThree = true
        document.getElementById(fields.logoDisplayAgreed.id).src = 'images/icons/checkbox-on.svg'
      } else {
        checkboxFlagThree = false
        document.getElementById(fields.logoDisplayAgreed.id).src = 'images/icons/checkbox-off.svg'
      }
      if (checkboxFlagThree) {
        fields.logoDisplayAgreed.value = 'true'
      } else {
        fields.logoDisplayAgreed.value = 'false'
      }
      updateToDB(fields.logoDisplayAgreed)
    })

    // simulate the file upload click
    uploadCoverBtn.addEventListener('click', function (evt) {
      $('#firmNewPhotoUpload').click()
    })

    // uploading photo functionality
    photoFieldEditInput.addEventListener('change', function (event) {
      let pic = event.target.files[0]

      if (pic) {
        if (pic.size <= 2000000) {
          if (Utility.REGEX.imageFormat.test(pic.name)) {
            let reader = new FileReader()
            reader.addEventListener('load', (e) => {
              photoEmptyContainer.style.border = 'transparent'
              photoErrMsgContainer.style.display = 'none'
              photoEmptyCamera.classList.remove('auvicon-camera')
              document.getElementById(fields.logo.id).src = reader.result
              document.getElementById(fields.logo.id).style.display = 'inherit'
            })

            // read the image file as a data URL.
            reader.readAsDataURL(pic)
            uploadLogo(pic)
            document.getElementById(fields.logoDisplayAgreed.id).disabled = false
          } else {
            photoErrMsgContainer.style.display = 'inherit'
            photoEmptyContainer.style.border = 'solid 1px #f3672f'
            photoErrMsg.innerText = '*Please select appropriate picture file.'
          }
        } else {
          photoErrMsgContainer.style.display = 'inherit'
          photoEmptyContainer.style.border = 'solid 1px #f3672f'
          photoErrMsg.innerText = '*The file you have chosen is too big. (Max file size 2MB)'
        }
      }
    }, false)

    // sending the signal that the button has been clicked to Onboarding Module.
    continueBtn.addEventListener('click', function () {
      var streetAddressField = document.getElementById(fields.address.streetAddress.id).value
      var unitField = document.getElementById(fields.address.unit.id).value
      var cityField = document.getElementById(fields.address.city.id).value
      var provinceField = document.getElementById(fields.address.stateProvince.id).value
// var countryField = document.getElementById(fields.address.country.id).value
      var postalField = document.getElementById(fields.address.postalCode.id).value
      fields.address.streetAddress.value = streetAddressField
      fields.address.unit.value = unitField
      fields.address.city.value = cityField
      fields.address.stateProvince.value = provinceField
      // fields.address.country.value = countryField
      fields.address.postalCode.value = postalField
      updateToDB(fields.address)

      self.trigger('move-to-next')
    })
  }

  /**
   * checking the checkbox and input fields validation and enable / disable the continue button.
   */
  this.checkInputValidation = function () {
    // check for input fields validation
    var continueBtn = document.getElementById('onboard-firm-continue')
    var firmName = document.getElementById(fields.name.id)
    var website = document.getElementById(fields.website.id)
    var firmNumber = document.getElementById(fields.size.id)
    var addressField = document.getElementById(fields.address.streetAddress.id)
    var cityField = document.getElementById(fields.address.city.id)
    var provinceField = document.getElementById(fields.address.stateProvince.id)
    // var countryField = document.getElementById(fields.address.country.id)
    var postalField = document.getElementById(fields.address.postalCode.id)
    // var affiliatedField = document.getElementById(fields.affiliatedFirmName.id)

    if (firmName.value === '' || firmName.classList.contains('auv-error')) {
      continueBtn.disabled = true
    } else if (website.value === '' || website.classList.contains('auv-error')) {
      continueBtn.disabled = true
    } else if (firmNumber.value === '' || firmNumber.classList.contains('auv-error')) {
      continueBtn.disabled = true
    } else if (addressField.value === '' || addressField.classList.contains('auv-error')) {
      continueBtn.disabled = true
    } else if (cityField.value === '' || cityField.classList.contains('auv-error')) {
      continueBtn.disabled = true
    } else if (provinceField.value === '' || provinceField.classList.contains('auv-error')) {
      continueBtn.disabled = true
    // } else if (countryField.value === '' || countryField.classList.contains('auv-error')) {
      // continueBtn.disabled = true
    } else if (postalField.value === '' || postalField.classList.contains('auv-error')) {
      continueBtn.disabled = true
    } else {
      continueBtn.disabled = false
    }
  }

  // sets logo based on whether there is a file or not.
  var setLogo = function (pic) {
    if (!pic) {
      document.getElementById(fields.logo.id).style.display = 'none'
      document.getElementById('firm-emptyCamera').style.display = 'inherit'
    } else {
      document.getElementById(fields.logoDisplayAgreed.id).disabled = false
      var srcPic = Utility.getLogoFirm(pic)
      document.getElementById(fields.logo.id).src = srcPic
      document.getElementById('firm-emptyCamera').style.display = 'none'
      document.getElementById(fields.logo.id).style.display = 'inherit'
    }
  }

  // loads pre-existing user data.
  this.loadUserData = function () {
    log('Firm Component: Loading Data')

    var firmName
    var firmAddress
    var addressStreet
    var addressUnit
    var addressCity
    var addressProvince
    // var addressCountry
    var addressPostal
    var firmSize
    var firmPhone
    var firmLogo
    var firmWebsite
    var firmAffiliated
    var affiliatedFirm
    var logoDisplayAgreedFirm

    if (c.currentFirm) {
      firmName = c.currentFirm.name || ''
      firmAddress = c.currentFirm.address || ''
      addressStreet = c.currentFirm.address.streetAddress || ''
      addressUnit = c.currentFirm.address.unit || ''
      addressCity = c.currentFirm.address.city || ''
      addressProvince = c.currentFirm.address.stateProvince || ''
      // addressCountry = c.currentFirm.address.country || ''
      addressPostal = c.currentFirm.address.postalCode || ''
      firmSize = c.currentFirm.size || ''
      firmPhone = c.currentFirm.phone || ''
      firmLogo = c.currentFirm.logo || ''
      firmWebsite = c.currentFirm.website || ''
      firmAffiliated = c.currentFirm.affiliated || null
      affiliatedFirm = c.currentFirm.affiliatedFirmName || ''
      logoDisplayAgreedFirm = c.currentFirm.logoDisplayAgreed || null
    }

    if (firmName !== null) {
      if (firmName !== '') {
        document.getElementById(fields.name.id).value = firmName.trim()
      }
    }

    if (firmSize !== null) {
      if (firmSize !== '') {
        document.getElementById(fields.size.id).value = firmSize
      }
    }

    if (firmPhone !== null) {
      if (firmPhone !== '') {
        document.getElementById(fields.phone.id).value = firmPhone
      }
    }

    if (firmWebsite !== null) {
      if (firmWebsite !== '') {
        document.getElementById(fields.website.id).value = firmWebsite
      }
    }

    if (firmAddress !== null) {
      if (firmAddress !== '') {
        if (addressStreet) {
          fields.address.streetAddress.value = addressStreet
          document.getElementById(fields.address.streetAddress.id).value = addressStreet
        }
        if (addressUnit) {
          fields.address.unit.value = addressUnit
          document.getElementById(fields.address.unit.id).value = addressUnit
        }
        if (addressCity) {
          fields.address.city.value = addressCity
          document.getElementById(fields.address.city.id).value = addressCity
        }
        if (addressProvince) {
          fields.address.stateProvince.value = addressProvince
          document.getElementById(fields.address.stateProvince.id).value = addressProvince
        }
        /* if (addressCountry) {
          fields.address.country.value = addressCountry
          document.getElementById(fields.address.country.id).value = addressCountry
        } */
        if (addressPostal) {
          fields.address.postalCode.value = addressPostal
          document.getElementById(fields.address.postalCode.id).value = addressPostal
        }
      }
    }

    if (firmAffiliated !== null) {
      if (firmAffiliated === true) {
        checkboxFlagTwo = true
        document.getElementById(fields.affiliated.id).src = 'images/icons/checkbox-on.svg'
        document.getElementById(fields.affiliatedFirmName.id).parentElement.style.display = 'inherit'
        document.getElementById('firm-infoTag').style.display = 'inherit'
        if (affiliatedFirm !== '') {
          document.getElementById(fields.affiliatedFirmName.id).value = affiliatedFirm
        }
      }
    }

    if (logoDisplayAgreedFirm !== null) {
      if (logoDisplayAgreedFirm === true) {
        checkboxFlagThree = true
        document.getElementById(fields.logoDisplayAgreed.id).src = 'images/icons/checkbox-on.svg'
      }
    }

    if (firmLogo !== null && firmLogo !== '') {
      if (firmLogo.length === 24 || firmLogo.length === 12) {
        setLogo('firms/' + c.currentFirm._id.toString())
      } else {
        setLogo(firmLogo)
      }
    } else {
      setLogo()
    }
    self.checkInputValidation()
  }

  /**
   * Handler for Update-Firm Event Response from Socket.
   *
   * @param tag: {String} Tag Name
   * @param Data: {JSON} The Result of Login Event.
   */
  var updateFirmResponse = function (tagName, data) {
    log('> Event: update-firm-response')
    log(data)
    var keys = Object.keys(data.data)
    for (var i = 0; i < keys.length; i++) {
      var key
      if (keys[i] === 'name') {
        key = 'firmName'
      } else {
        key = null
      }
      if (key && fields[key]) {
        fields[key].value = data.data[keys[i]]
        document.getElementById(fields[key].id).value = data.data[keys[i]]
      }
    }
  }

  /**
   * Setup Listener
   */
  this.setupListeners = function () {
    c.registerForEvent('update-firm', 'ONBOARDDIRM_UPDATEFIRM-000', updateFirmResponse)
  }

  /**
   *  Update Mongo DB if there is any changes.
   */
  function updateToDB (data) {
    log('Update new information based on what user typed.')
    var json = {}

    if (data.id === 'firm-fullAddress') {
      var cityData
      var postalData
      var streetData
      var provinceData
      var unitData
      // var countryData

      if (data.city.value) {
        cityData = data.city.value
      }
      if (data.postalCode.value) {
        postalData = data.postalCode.value
      }
      if (data.stateProvince.value) {
        provinceData = data.stateProvince.value
      }
      if (data.streetAddress.value) {
        streetData = data.streetAddress.value
      }
      if (data.unit.value) {
        unitData = data.unit.value
      }
      /* if (data.country.value) {
        countryData = data.country.value
      } */
      json.address = {
        city: cityData,
        postalCode: postalData,
        stateProvince: provinceData,
        streetAddress: streetData,
        unit: unitData
        // country: countryData
      }
    } else {
      var fieldName = data.model.field
      json[fieldName] = data.value
    }
    json.firmID = c.currentFirm._id
    c.updateFirm(json)
  }

  /**
   * Upload Logo pictrue.
   */
  function uploadLogo (file) {
    log('Upload new Logo Picture :' + file.name)

    var uri = '/firm-logo-upload'
    var xhr = new XMLHttpRequest()
    var fd = new FormData()
    fd.append('date', new Date().toUTCString())
    fd.append('firmID', c.currentFirm._id)
    fd.append('file', file)

    xhr.addEventListener('load', (e) => {
      if (e.target.status === 200) {
        var fileName = xhr.responseText
        var myFirm = c.currentFirm._id
        c.updateFirm({firmID: myFirm, logo: fileName})
      } else {
        c.displayMessage(xhr.responseText, {color: 'red'})
      }
    })

    xhr.upload.onprogress = function (evt) {
      if (evt.lengthComputable) {
        var rounded = Math.round(((evt.loaded / evt.total) * 100) * 10) / 10
        var percentComplete = Math.min(rounded, 99.9)
        log('Percent: ' + percentComplete + ' Loaded: ' + evt.loaded + 'total: ' + evt.total)
      }
    }

    xhr.open('POST', uri, true)
    xhr.send(fd)
  }

  buildContent()
}

Component_Firm.prototype = ComponentTemplate.prototype
Component_Firm.prototype.constructor = ComponentTemplate

module.exports = Component_Firm
