'use strict'

import styles from './ComponentTemplate.css'
/**
 * Component Template is responsible for generating components dom
 */
var ComponentTemplate = function (ctrl) {
  var self = this

  // event object
  this.evts = {}

  // setup listener
  this.on = function (tag, cb) {
    if (typeof tag !== 'string' || typeof cb !== 'function') {
      return
    }
    if (!self.evts[tag]) {
      self.evts[tag] = cb
    } else {
      self.evts[tag] = cb
    }
  }

  // trigger events
  this.trigger = function (tag, evt, data) {
    if (this.evts[tag]) {
      this.evts[tag](evt, data)
    }
  }
}

module.exports = ComponentTemplate
