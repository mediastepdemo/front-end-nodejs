/* Globals Utility */
'use strict'

import ComponentTemplate from '../ComponentTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import Utility from '../../../js/src/utility_c'
import moment from 'moment'

var Component_Requests_Details = function (container, ctrl) {
  ComponentTemplate.call(this, container)

  this.name = 'Request Details'
  var self = this
  var parentDOM = container

  var currentRequest = null
  var currentName = ''
  var currentDescription = ''
  var mapping = []
  var pickedDate = null
  const emptyPageId = 'empty-request-page'

  /**
   * Loads the specified request into the dom fields and updates the tracked request.
   * @param {Object} request - The current request to load.
   */
  this.loadRequest = function (request, currentRequests) {
    log(self.name + ': Loading Request ...')

    if (!request) {
      if (ctrl.currentUser.type === 'AUDITOR') {
        document.getElementById('m-req-requestname').innerText = 'Please Create a Request'
        document.getElementById('m-req-description').innerText = 'Customize description'
      } else {
        document.getElementById('m-req-requestname').innerText = 'Untitled Request'
        document.getElementById('m-req-description').innerText = 'No description yet'
      }

      document.getElementById('m-req-requestNameEditor').disabled = true
      document.getElementById('m-req-requestDescriptionEditor').disabled = true

      document.getElementById('m-req-buttoncontainer').style.display = 'none'
      document.getElementById('req-detail-empty').style.display = 'block'
      document.getElementById('req-detail-state2').style.display = 'none'

      var itemDiv = document.getElementById('req-det-requestItems')
      if (itemDiv.firstChild) {
        while (itemDiv.firstChild) {
          itemDiv.removeChild(itemDiv.firstChild)
        }
      }
      if (document.getElementById('req-det-requestItems').classList.contains('m-req-files-scroll')) {
        document.getElementById('req-det-requestItems').classList.remove('m-req-files-scroll')
      }
      if (ctrl.currentUser.type === 'AUDITOR') {
        if (!$(`#${emptyPageId}`).length) {
          document.getElementById('requests-details-container').appendChild($(`
          <div class='req-empty-page' id='${emptyPageId}'>
            <h1>You don't have any requests yet. Click the Create Category button on the left to create a category then create requests. Or go to dashboard to set default requests.</h1>
          </div>`)[0])
        }
      } else {
        if (!$(`#${emptyPageId}`).length) {
          document.getElementById('requests-details-container').appendChild($(`
          <div class='req-empty-page' id='${emptyPageId}'>
            <h1>Waiting on your auditor to create requests</h1>
          </div>`)[0])
        }
      }
    } else {
      const emptyPage = document.getElementById('empty-request-page')
      if (emptyPage) {
        $(emptyPage).remove()
      }

      document.getElementById('m-req-description').innerText = request.description || 'Customize description'
      document.getElementById('m-req-requestname').innerText = request.name

      if (ctrl.currentUser.type === 'AUDITOR') {
        document.getElementById('m-req-requestNameEditor').disabled = false
        document.getElementById('m-req-requestDescriptionEditor').disabled = false
      }

      document.getElementById('m-req-buttoncontainer').style.display = 'block'
      document.getElementById('req-detail-empty').style.display = 'none'
      document.getElementById('req-detail-state2').style.display = 'block'

      let dueDateLabel = 'Select Due Date'

      if (ctrl.currentUser.type !== 'AUDITOR') {
        dueDateLabel = 'Due Date'
      }
      if (request.dueDate) {
        dueDateLabel = 'Due: ' + moment(request.dueDate).utc().format('MMM DD YYYY')
      }
      if (dueDateLabel === 'Due Date') {
        document.getElementById('req-det-duedateContainer').style.display = 'none'
      } else {
        $('#req-det-duedate').text(dueDateLabel)
      }

      currentRequest = request
      currentName = request.name
      currentDescription = request.description

      var itemDiv = document.getElementById('req-det-requestItems')
      if (itemDiv.firstChild) {
        while (itemDiv.firstChild) {
          itemDiv.removeChild(itemDiv.firstChild)
        }
      }

      if (request.items) {
        if (request.items.length > 5) {
          if (!document.getElementById('req-det-requestItems').classList.contains('m-req-files-scroll')) {
            document.getElementById('req-det-requestItems').classList.add('m-req-files-scroll')
          }
        } else {
          if (document.getElementById('req-det-requestItems').classList.contains('m-req-files-scroll')) {
            document.getElementById('req-det-requestItems').classList.remove('m-req-files-scroll')
          }
        }
        for (var i = 0; i < request.items.length; i++) {
          if (request.items[i].status === 'ACTIVE') {
            self.addFile(request.items[i], itemDiv, currentRequests)
          }
        }
      }

      var subReqDiv = document.getElementById('req-det-subRequests')
      if (subReqDiv.firstChild) {
        while (subReqDiv.firstChild) {
          subReqDiv.removeChild(subReqDiv.firstChild)
        }
      }

      addSubRequest(currentRequest)
    }
  }

  /**
   * Handles the request to delete a file from the currently displayed request.
   * @param {String} fileID   - ObjectId from the db.
   * @param {String} uniqueID - UniqueID pointing to a specific dom element chunk.
   */
  this.deleteFile = function (fileID, uniqueID) {
    log(self.name + ': Deleting the file id: ' + fileID + ' uniqueID: ' + uniqueID + '...')

    if (!uniqueID) {
      // Set unique ID if there is no Unique ID.
      for (var i = 0; i < mapping.length; i++) {
        var mapItem = mapping[i]
        if (mapItem.id === fileID) {
          uniqueID = mapItem.uniqueID
          log(self.name + ': Removing item from map')
          mapping.splice(i, 1)
          break
        }
      }
    }

    if (uniqueID) {
      var element = document.getElementById('req-det-item-' + uniqueID)
      if (element) {
        element.parentNode.removeChild(element)
      }
    }
  }

  /**
   * This will take any files added to a request and display them on the screen
   * TODO currentRequests paramater is being used to build the drop down menu for switching
   */
  this.addFile = function (file, parent, currentRequests) {
    log(self.name + ': Adding a new request file ...')
    if (file.status === 'INACTIVE') {
      return log('Current file is INACTIVE, skipping')
    }

    var uniqueID = ''
    do {
      log(self.name + ': Generating Unique ID')
      uniqueID = Forge.getUniqueID()
    } while (document.getElementById('req-det-item-' + uniqueID))

    if (file._id) {
      mapping.push({uniqueID: uniqueID, id: file._id})
    } else {
      mapping.push({uniqueID: uniqueID, id: null})
    }
    var path = file.path || '/'
    var newItem = Forge.build({'dom': 'div', 'id': 'req-det-item-' + uniqueID, 'class': 'req-det-itemContainer'})
    var newItemIcon = Forge.build({'dom': 'div', 'class': 'auvicon-line-file req-det-itemIcon'})
    var newItemName = Forge.build({'dom': 'div', 'text': path + file.name, 'class': 'req-det-itemName'})

    var styleValue = 'inherit'
    if (file.status === 'UPLOADING' || !file._id) {
      var newItemUploadContainer = Forge.build({'dom': 'div', 'id': 'req-det-item-prg-' + uniqueID, 'class': 'req-det-itemUploadContainer'})
      var newItemUploadProgress = Forge.build({'dom': 'div', 'id': 'req-det-progress-' + uniqueID, 'class': 'req-det-itemUploadProgress'})
      var newItemUploadText = Forge.build({'dom': 'div', 'text': 'Uploading...', 'class': 'req-det-itemUploadText'})
      var newItemStop = Forge.build({'dom': 'div', 'id': 'req-det-item-stp-' + uniqueID, 'class': 'auvicon-ex req-det-itemDelete'})

      newItemUploadContainer.appendChild(newItemUploadProgress)
      newItemUploadContainer.appendChild(newItemUploadText)
      newItem.appendChild(newItemUploadContainer)
      newItem.appendChild(newItemStop)
      newItemStop.addEventListener('click', function () {
        document.getElementById('fileInputHandler').value = ''
        self.trigger('cancel-upload', { uniqueID: uniqueID })
        parent.removeChild(newItem)
      })

      styleValue = 'none'
    }

    var newItemDelete = Forge.build({'dom': 'div', 'id': 'req-det-item-del-' + uniqueID, 'class': 'auvicon-trash req-det-itemDelete', 'style': 'display:' + styleValue})
    newItem.appendChild(newItemDelete)
    newItemDelete.addEventListener('click', function () {
      log('Deleting file ... uid:(' + uniqueID + ')')
      for (var i = 0; i < mapping.length; i++) {
        if (uniqueID === mapping[i].uniqueID) {
          var json = {requestID: currentRequest._id, fileID: mapping[i].id, uniqueID: uniqueID, engagementID: ctrl.currentEngagement._id}
          self.trigger('delete-request-file-modal', json)
          break
        }
      }
    })

    newItem.appendChild(newItemIcon)
    newItem.appendChild(newItemName)
    parent.appendChild(newItem)
    return uniqueID
  }

  /**
   * Setup Request dictionaries as well as dropdown menu.
   */
  var setupRequestDDMenu = function (requests) {
    // reset requestDictionary and DD Menu.
    var requestDictionary = {}
    var requestDDMenu = []

    // Create Dictionary and Dropdown menu
    for (var i = 0; i < requests.length; i++) {
      var requestObj
      if (requestDictionary[requests[i]._id]) {
        // If request ID is already in our request Dictionary, It means this request is parent of other request.
        requestObj = requestDictionary[requests[i]._id].obj
        requestObj.text = requests[i].name
      } else {
        // If request is not exists in our request dictionary, create a new request with empty children Requests.
        requestObj = {text: requests[i].name, value: requests[i]._id, items: requests[i].items}
        requestDDMenu.push(requestObj)
        requestDictionary[requests[i]._id] = {obj: requestObj, parentDDList: requestDDMenu}
      }

      if (requests[i].parent) {
        // Remove from current parent, since it has different parent.
        var parentDropdownList = requestDictionary[requestObj.value].parentDDList
        for (var j = 0; j < parentDropdownList.length; j++) {
          var index = -1
          if (parentDropdownList[j].value.toString() === requestObj.value.toString()) {
            index = j
          }
        }
        if (index > 0) {
          parentDropdownList.splice(index, 1)
        }
        if (requestDictionary[requests[i].parent]) {
          // If request's parent exists in our dropdown dictionary.
          // Add this object to DD Menu list and set this request's parentDropdownlists to parent's dropdown list.
          if (requestDictionary[requests[i].parent].obj.contains) {
            requestDictionary[requests[i].parent].obj.contains.push(requestObj)
          } else {
            requestDictionary[requests[i].parent].obj.contains = [requestObj]
          }

          requestObj.parentDropdownList = requestDictionary[requests[i].parent].obj.contains
        } else {
          // If request's parent does not exists in our dropdown dictionary.
          // Add new reqeust to our dictioary that's children contains this request object
          var parentRequest = {text: undefined, value: requests[i].parent, contains: [requestObj]}
          requestDDMenu.push(parentRequest)
          requestDictionary[requests[i].parent] = {obj: parentRequest, parentDDList: requestDDMenu}
        }
      }
    }
    return requestDDMenu
  }

  /**
   * This will check if the current request has any requests itself and if so will
   * display them, if not it will display a message to add a sub request.
   * @param {Array} request array of currently select req
   */
  var addSubRequest = function (request) {
    if (currentRequest.requests) {
      if (currentRequest.requests.length === 0) {
        var detailNoSubReq = Forge.build({'dom': 'div', 'class': 'req-det-noSubRequests', 'text': 'There are no requested items in General ledger.'})
        var detailAddRequest = Forge.build({'dom': 'span', 'class': 'req-det-addRequestSpan', 'text': ' Add a sub request.', 'id': 'req-det-addRequest'})

        var parentDiv = document.getElementById('req-det-subRequests')
        parentDiv.appendChild(detailNoSubReq)
        detailNoSubReq.appendChild(detailAddRequest)

        detailAddRequest.addEventListener('click', function () {
          self.trigger('add-request', {})
        })
      }
    }
  }

  /**
   * Updates the current tracked values and the dom.
   */
  this.updateData = function (data) {
    log(self.name + ': Updating the data ...')

    var name = data.name
    var description = data.description
    if (name) {
      if (name !== currentName) {
        document.getElementById('m-req-requestname').innerText = name
        currentName = name
      }
    }
    if (description) {
      if (description !== currentDescription) {
        document.getElementById('m-req-description').innerText = description
        currentDescription = description
      }
    }
  }

  /**
   * Sends an update request to the server.
   */
  var updateRequest = function () {
    var json = {
      requestID: currentRequest._id,
      engagementID: currentRequest.engagementID
    }

    var name = document.getElementById('m-req-requestname').innerText
    var description = document.getElementById('m-req-description').innerText
    if (name !== currentName) {
      json.name = name
    }
    if (description !== currentDescription) {
      json.description = description
    }
    self.trigger('update-request', json)
  }

  /**
   * @return a DOM object
   */
  var buildContent = function (settings) {
    if (!parentDOM) {
      log('No parent dom exist.')
      return
    }

    var componentContentDiv = Forge.build({'dom': 'div'})

    var requestTitleContainer = Forge.build({'dom': 'div', 'id': 'm-req-namecontainer', 'class': 'req-det-requestTitleContainer'})

    var requestButtonContainer = Forge.build({'dom': 'div', 'id': 'm-req-buttoncontainer', 'style': 'float:right; margin-right: 24px;'})

    var duedateContainer = Forge.build({'dom': 'div', 'id': 'req-det-duedateContainer', 'class': 'req-det-duedateContainer', 'text': ''})
    var dueDateInput = Forge.build({'dom': 'div', 'id': 'req-det-duedate', 'class': 'req-det-duedate', 'text': 'Select date'})

    var dndContainer = Forge.build({'dom': 'div', 'id': 'req-detail-state2', 'class': 'req-det-fileUploadContainer noSelect'})
    var emptyContainer = Forge.build({'dom': 'div', 'id': 'req-detail-empty', 'class': 'req-det-fileUploadContainer noSelect'})
    var dndImg = Forge.build({'dom': 'div', 'class': 'auvicon-line-cloud-upload', 'style': 'margin: auto; display: block; font-size: 57px; color: #e1e1e1;'})
    var dndInputUploadText = Forge.build({'dom': 'div', 'class': 'req-det-uploadText', 'text': 'Drag and drop files here or', 'id': 'photoUploadText'})
    var dndInputUploadBrowse = Forge.build({'dom': 'p', 'class': 'req-det-browseText', 'text': 'Browse', 'id': 'photoBrowseText'})

    var requestNameContainer = Forge.build({'dom': 'div'})
    var requestName = Forge.build({'dom': 'div', 'class': 'req-det-requestName', 'text': 'General Ledger', 'id': 'm-req-requestname'})
    var requestNameEditor = Forge.build({'dom': 'input', 'class': 'req-det-requestNameEditor', 'text': 'General Ledger', 'id': 'm-req-requestNameEditor'})
    var requestNamePencil = Forge.build({dom: 'div', class: 'auvicon-edit req-det-pencil'})

    var requestDescriptionContianer = Forge.build({'dom': 'div'})
    var requestDescription = Forge.build({'dom': 'div', 'class': 'req-det-requestDescription', 'text': 'Description', 'id': 'm-req-description'})
    var requestDescriptionEditor = Forge.build({'dom': 'input', 'class': 'req-det-requestDescriptionEditor', 'id': 'm-req-requestDescriptionEditor', placeholder: 'Customize description'})
    var requestDescriptionPencil = Forge.build({dom: 'div', class: 'auvicon-edit req-det-pencil'})
    var deleteIcon = Forge.build({'dom': 'img', 'src': 'images/icons/delete-icon.png', 'class': 'req-det-deleteRequest', 'id': 'req-det-delete-request'})
    deleteIcon.addEventListener('mouseover', function () { this.src = 'images/icons/icon-delete-green.png' })
    deleteIcon.addEventListener('mouseout', function () { this.src = 'images/icons/delete-icon.png' })
    deleteIcon.addEventListener('click', function () {
      self.trigger('delete-request-modal', currentRequest)
    })

    var confirmBtn = Forge.build({dom: 'button', id: 'req-det-confirm-btn', class: 'req-det-confirmationBtn btn', text: 'Yes, Use on All'})
    confirmBtn.addEventListener('click', function (event) {
      event.preventDefault()
      self.trigger('update-global-dueDate', {dueDate: pickedDate})
    })

    var cancelBtn = Forge.buildBtn({text: 'No, Just this Once', id: 'req-det-cancel-btn', type: 'dlink', margin: '16px'}).obj
    cancelBtn.addEventListener('click', function (event) {
      event.preventDefault()
      self.trigger('update-request', {dueDate: pickedDate})
    })

    var dueDateConfirmationDiv = Forge.build({'dom': 'div', 'class': 'req-det-confirmationDiv', 'text': '', 'id': 'req-det-confirmation'})
    var dueDateConfirmationHeader = Forge.build({'dom': 'p', 'class': 'req-det-confirmationHeader', 'text': 'Use Due Date On All Requests?', 'id': 'photoBrowseText'})
    var dueDateConfirmationText = Forge.build({'dom': 'p', 'class': 'req-det-confirmationText', 'text': 'would you like to use August 23, 2021 as the due date for all your requests?', 'id': 'photoBrowseText'})

    dueDateConfirmationDiv.appendChild(dueDateConfirmationHeader)
    dueDateConfirmationDiv.appendChild(dueDateConfirmationText)
    dueDateConfirmationDiv.appendChild(confirmBtn)
    dueDateConfirmationDiv.appendChild(cancelBtn)
    componentContentDiv.appendChild(requestTitleContainer)
    componentContentDiv.appendChild(requestButtonContainer)
    requestTitleContainer.appendChild(requestNameContainer)
    requestNameContainer.appendChild(requestName)
    requestNameContainer.appendChild(requestNameEditor)
    requestNameContainer.appendChild(requestNamePencil)
    requestTitleContainer.appendChild(requestDescriptionContianer)
    requestDescriptionContianer.appendChild(requestDescription)
    requestDescriptionContianer.appendChild(requestDescriptionEditor)
    requestDescriptionContianer.appendChild(requestDescriptionPencil)

    requestButtonContainer.appendChild(duedateContainer)

    duedateContainer.appendChild(dueDateInput)

    // Requests' names, descriptions and due dates can only be set by auditor. Requests can only be deleted by auditor
    if (ctrl.currentUser.type === 'AUDITOR') {
      $(requestName).addClass('req-det-requestName-auditor-view')
      $(requestDescription).addClass('req-det-requestDescription-auditor-view')

      requestName.addEventListener('click', function () {
        this.style.display = 'none'
        var editor = document.getElementById('m-req-requestNameEditor')
        editor.value = this.innerText
        editor.style.display = 'inline-block'
        editor.focus()
      })

      requestNameEditor.addEventListener('blur', function () {
        if (this.value) {
          var isUnique = true
          for (let i = 0; i < ctrl.currentRequests.length; i++) {
            if (ctrl.currentRequests[i]._id !== currentRequest._id && ctrl.currentRequests[i].name === this.value) {
              isUnique = false
            }
          }
          if (isUnique) {
            this.style.display = 'none'
            document.getElementById('m-req-requestname').innerText = this.value
            document.getElementById('m-req-requestname').style.display = 'inline-block'
            updateRequest()
            this.style.border = null
            this.style.borderRadius = null
          } else {
            this.style.display = 'none'
            document.getElementById('m-req-requestname').innerText = currentRequest.name
            this.value = currentRequest.name
            document.getElementById('m-req-requestname').style.display = 'inline-block'
            this.style.border = null
            this.style.borderRadius = null
            self.trigger('flash', {content: 'Request name should be unique.', type: 'ERROR'})
          }
        } else {
          this.style.border = 'solid 1px #f3672f'
          this.style.borderRadius = '4px'
          this.placeholder = 'Please choose a request name'
        }
      })

      requestDescription.addEventListener('click', function () {
        this.style.display = 'none'
        var editor = document.getElementById('m-req-requestDescriptionEditor')
        editor.value = currentRequest.description
        editor.style.display = 'inline-block'
        editor.focus()
      })

      requestDescriptionEditor.addEventListener('blur', function () {
        if (this.value) {
          this.style.display = 'none'
          document.getElementById('m-req-description').innerText = this.value
          document.getElementById('m-req-description').style.display = 'inline-block'
          updateRequest()
          this.style.border = null
          this.style.borderRadius = null
        } else {
          this.style.border = 'solid 1px #f3672f'
          this.style.borderRadius = '4px'
          this.placeholder = 'Please write a description'
        }
      })

      let deleteIcon = Forge.build({ 'dom': 'div', 'class': 'auvicon-trash req-det-deleteRequest', 'id': 'req-det-delete-request' })
      deleteIcon.addEventListener('click', function () {
        self.trigger('delete-request-modal', currentRequest)
      })

      requestButtonContainer.appendChild(deleteIcon)

      const duedateContainerDom = $(duedateContainer)
      duedateContainerDom.addClass('req-det-duedateContainer-auditor-view')
      duedateContainerDom.on('click', () => {
        let datePicker = duedateContainerDom.find('.ui-datepicker-inline.ui-datepicker.ui-widget.ui-widget-content.ui-helper-clearfix.ui-corner-all')
        if (!datePicker.length) {
          duedateContainerDom.datepicker({
            firstDay: 1,
            dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
            showOtherMonths: true,
            minDate: new Date(),
            dateFormat: 'yy-mm-dd',
            onSelect: (dateText) => {
              pickedDate = new Date(dateText)
              $(dueDateInput).text('Due: ' + moment(pickedDate).utc().format('MMM DD YYYY'))
              datePicker.hide()
              $(dueDateConfirmationDiv).show()
            }
          })
          datePicker = duedateContainerDom.find('.ui-datepicker-inline.ui-datepicker.ui-widget.ui-widget-content.ui-helper-clearfix.ui-corner-all')
        } else {
          if (!datePicker.is(':visible') && !$(dueDateConfirmationDiv).is(':visible')) {
            datePicker.show()
            if (currentRequest && currentRequest.dueDate) {
              duedateContainerDom.datepicker('setDate', currentRequest.dueDate.slice(0, 10))
            }
          }
          if ($(dueDateConfirmationDiv).is(':visible')) {
            $(dueDateConfirmationDiv).hide()
          }
        }
      })
    }

    duedateContainer.appendChild(dueDateConfirmationDiv)
    dndContainer.appendChild(dndImg)
    dndContainer.appendChild(dndInputUploadText)
    dndContainer.appendChild(dndInputUploadBrowse)
    componentContentDiv.appendChild(dndContainer)
    componentContentDiv.appendChild(emptyContainer)

    var detailItemsContainer = Forge.build({'dom': 'div', 'id': 'req-det-requestItems'})
    componentContentDiv.appendChild(detailItemsContainer)

    var detailSubReqContainer = Forge.build({'dom': 'div', 'style': 'display:none;'})
    var detailSubReqHeader = Forge.build({'dom': 'p', 'class': 'req-det-requestItemHeader', 'text': 'Requested Items', 'id': 'm-req-subRequestname'})
    var detailSubRequests = Forge.build({'dom': 'div', 'id': 'req-det-subRequests'})

    componentContentDiv.appendChild(detailSubReqContainer)
    detailSubReqContainer.appendChild(detailSubReqHeader)
    detailSubReqContainer.appendChild(detailSubRequests)

    var fileInput = Forge.build({dom: 'input', id: 'fileInputHandler', type: 'file', style: 'display:none'})
    dndContainer.appendChild(fileInput)

    fileInput.addEventListener('change', function (evt) {
      fileUpload_Selection(this.files)
    })
    attachDragnDrop(dndContainer)

    dndContainer.addEventListener('click', function (evt) {
      var inputFileDom = document.getElementById('fileInputHandler')
      inputFileDom.click()
    })

    parentDOM.appendChild(componentContentDiv)
  }

  /**
   * Handles the request to upload a files array. Currently supports single file upload.
   *
   */
  var fileUpload_Selection = function (files) {
    if (files.length === 1) {
      var parentDiv = document.getElementById('req-det-requestItems')
      var file = files[0]
      if (Utility.checkMimeType(file.type)) {
        var uniqueID = self.addFile(files[0], parentDiv, null)
        if (currentRequest.items.length > 3) {
          if (!document.getElementById('req-det-requestItems').classList.contains('m-req-files-scroll')) {
            document.getElementById('req-det-requestItems').classList.add('m-req-files-scroll')
          }
        }

        if (uniqueID) {
          self.trigger('upload-file', {
            file: files[0],
            uniqueID: uniqueID,
            requestID: currentRequest._id,
            progress: fileUpload_Progress,
            callback: fileUpload_Complete
          })
        }
      } else {
        log('Unable to upload file, Do not support file Type.')
        self.trigger('flash', {content: 'File cannot upload. Please Check File Type.', type: 'ERROR'})
      }
    }
  }
  /**
   *
   */
  var fileUpload_Progress = function (err, json) {
    if (err) {
      log(err)
    } else {
      if (document.getElementById('req-det-progress-' + json.uniqueID)) {
        document.getElementById('req-det-progress-' + json.uniqueID).style.width = json.value + '%'
      }
    }
  }

  /**
   * Called when a file upload has been completed and returns the associated data
   * about the file upload. This includes the information about the domID and db objects.
   * @param {Object} err  - The custom error object returned
   * @param {Object} data - The data being returned about the file uploaded
   */
  var fileUpload_Complete = function (err, data) {
    if (err) {
      // TODO take appropriate action
    } else {
      document.getElementById('fileInputHandler').value = ''
      log(self.name + ': File upload complete')
      self.trigger('flash', { content: 'File upload is done.', type: 'SUCCESS' })
      document.getElementById('req-det-item-prg-' + data.uniqueID).style.display = 'none'
      document.getElementById('req-det-item-stp-' + data.uniqueID).style.display = 'none'
      document.getElementById('req-det-item-del-' + data.uniqueID).style.display = 'inherit'
      for (var i = 0; i < mapping.length; i++) {
        if (data.uniqueID === mapping[i].uniqueID) {
          mapping[i].id = data.file._id
        }
      }
      data.file.requestID = currentRequest._id
      self.trigger('create-activity', {engagementID: currentRequest.engagementID,
        operation: 'CREATE',
        type: 'File',
        original: {},
        updated: data.file
      })
    }
  }

  /**
   * Drag and drop function for files???
   */
  var attachDragnDrop = function (dom) {
    log(self.name + ': setup request file dropzone')

    dom.ondragover = dom.ondragenter = function (evt) {
      this.style.borderColor = '#5CD4C0'
      evt.stopPropagation()
      evt.preventDefault()
    }

    dom.ondragleave = function (evt) {
      this.style.borderColor = '#c4c4c4'
    }

    dom.ondrop = function (evt) {
      this.style.borderColor = '#c4c4c4'

      evt.stopPropagation()
      evt.preventDefault()

      fileUpload_Selection(event.dataTransfer.files)
    }
  }

  buildContent()
}

Component_Requests_Details.prototype = ComponentTemplate.prototype
Component_Requests_Details.prototype.constructor = ComponentTemplate

module.exports = Component_Requests_Details
