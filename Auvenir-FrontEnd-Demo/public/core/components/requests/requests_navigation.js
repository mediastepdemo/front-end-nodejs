/* Globals $ */

'use strict'

import ComponentTemplate from '../ComponentTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'

var Component_Requests_Navigation = function (container, ctrl) {
  ComponentTemplate.call(this, container)

  this.name = 'Request Nav'
  var parentDOM = container
  var self = this

  var PADDING_LEFT = 8

  var numOfCompletedRequests = 0
  var numOfRequests = 0
  var currentAddingCategoryId = null
  var currentAddingRequestId = null

  var reqTree = [] // all categories and requests
  var requestMapping = []

  var userType = ''
  /**
   * Is responsible for loading the categories and the requests into the DOM.
   */
  this.loadData = function (engagementID, categories, requests, user) {
    log(self.name + ': Loading the data')
    requestMapping = []
    reqTree = buildRequestTree(categories, requests)

    userType = user
    if (userType === 'CLIENT') {
      document.getElementById('req-nav-newCatBtn').style.display = 'none'
    }

    // clear requests container
    var reqNav = document.getElementById('request-categories')
    if (reqNav.firstChild) {
      while (reqNav.firstChild) {
        reqNav.removeChild(reqNav.firstChild)
      }
    }

    buildTreeStructure(reqTree, reqNav)
    self.updateRequestStats(requests)
  }

  var registerEvents = function () {
    ctrl.registerForEvent('add-category', 'ADD-CATEGORY-RES-01', handle_addCategory)
  }

  var createCategoryFailed = function (btn, box) {
    btn.attr('disabled', 'disabled')
    btn.css('cursor', 'not-allowed')
    box.style.border = 'solid 1px #f3672f'
    box.style.borderRadius = '4px'
  }

  var createCategorySuccess = function (btn, box) {
    btn.removeAttr('disabled')
    btn.css('cursor', 'pointer')
    box.style.border = null
    box.style.borderRadius = null
  }

  var handle_addCategory = function (tagName, data) {
    var reqCategoryBox = document.getElementById('reqCategoryBox-' + currentAddingCategoryId)
    var reqCatInput = document.getElementById('reqCategoryInput-' + currentAddingCategoryId)
    var reqCatAdd = document.getElementById('reqAdd-' + currentAddingCategoryId)
    var reqCatListDiv = document.getElementById('reqListDiv-' + currentAddingCategoryId)

    const createBtn = $('#req-nav-newCatBtn')

    if (data.result.msg && currentAddingCategoryId) {
      createCategoryFailed(createBtn, reqCategoryBox)
    } else {
      createCategorySuccess(createBtn, reqCategoryBox)
      reqCatInput.style.display = 'none'
      reqCatAdd.style.display = 'inherit'
      var reqCatTitle = Forge.build({'dom': 'div', 'id': 'reqTitle-' + currentAddingCategoryId, 'text': reqCatInput.value, 'class': 'req-nav-catTitle'})
      reqCategoryBox.appendChild(reqCatTitle)
      reqCategoryBox.insertBefore(reqCatTitle, reqCatListDiv)

      reqCategoryBox.addEventListener('mouseover', function () {
        reqCatAdd.style.opacity = '1'
      })
      reqCategoryBox.addEventListener('mouseout', function () {
        reqCatAdd.style.opacity = '0'
      })
    }
  }

  this.unmarkComplete = function (requestID) {
    for (let i = 0; i < requestMapping.length; i++) {
      var elem = requestMapping[i]
      if (elem._id === requestID) {
        log('Match found. Removing checkmark')
        var matchDom = document.getElementById('reqCheckmark-' + elem.domID)
        if (matchDom) {
          matchDom.parentNode.removeChild(matchDom)
        } else {
          log('Unable to find dom object in dom tree')
        }
        break
      }
    }
  }

  this.requestCompleted = function (requestID) {
    for (let i = 0; i < requestMapping.length; i++) {
      var elem = requestMapping[i]
      if (elem._id === requestID) {
        log('Match found. Updating Navmenu to show checkmark')
        var matchDom = document.getElementById('reqTitle-' + elem.domID)
        if (matchDom) {
          if (document.getElementById('reqCheckmark-' + elem.domID)) {
            log('Checkmark already added')
          } else {
            var reqListFinMark = Forge.build({dom: 'div', class: 'auvicon-checkmark req-nav-checkmark', id: 'reqCheckmark-' + elem.domID})
            matchDom.appendChild(reqListFinMark)
            $(reqListFinMark).insertBefore($(matchDom))
          }
        } else {
          log.error('Unable to find dom object in dom tree')
        }
        break
      }
    }
  }

  /**
   * Takes an array of Request objects and generates the appropriate tree structure.
   * This is used to display the tree structure to the user.
   */
  var buildRequestTree = function (categories, requests) {
    log(self.name + ': Building Request Tree')
    if (!requests) {
      return []
    }
    if (typeof categories !== 'object') {
      return []
    }
    if (typeof requests !== 'object') {
      return []
    }

    var requestTree = []
    buildRequest(requests, null, requestTree)

    var result = []
    for (var i = 0; i < categories.length; i++) {
      var json = { name: categories[i], requests: [] }

      for (var j = 0; j < requestTree.length; j++) {
        if (json.name === requestTree[j].category) {
          json.requests.push(requestTree[j])
        }
      }
      result.push(json)
    }
    return result
  }

  /**
   * Adds a request from the flat array of requests into the appropriate location
   * in the request tree.
   */
  var buildRequest = function (requests, pObj, tree) {
    for (var i = 0; i < requests.length; i++) {
      var x = (pObj) ? pObj._id : null
      if (!pObj && !requests[i].parent) {
        if (requests[i].status === 'INACTIVE') {
          log('Request is marked INACTIVE')
        } else {
          let json = requests[i]
          json.requests = []
          tree.push(json)
          buildRequest(requests, tree[tree.length - 1], tree[tree.length - 1])
        }
      } else if (!pObj) {
        continue
      } else if (requests[i].parent === pObj._id) {
        if (requests[i].status === 'INACTIVE') {
          log('Request is marked INACTIVE')
        } else {
          let json = requests[i]
          json.requests = []
          tree.requests.push(json)
          buildRequest(requests, tree.requests[tree.requests.length - 1],
          tree.requests[tree.requests.length - 1])
        }
      }
    }
  }

  /**
   * Is responsible for searching the request tree and returning the first request object.
   * @return requestObject or null if empty
   */
  this.getFirstRequest = function () {
    if (reqTree.length > 0) {
      var firstCategory = reqTree[0]
      if (firstCategory.requests && firstCategory.requests.length > 0) {
        return reqTree[0].requests[0]
      }
    }
    return null
  }

  this.updateRequestParent = function (requestID, uniqueID, parentLvl) {
    if (parentLvl >= 0) {
      // document.getElementById('reqListBox-' + uniqueID).style.paddingLeft = (parseInt(parentLvl) + 1) * PADDING_LEFT + 'px'
      document.getElementById('reqTitle-' + uniqueID).style.paddingLeft = (parseInt(parentLvl) + 1) * PADDING_LEFT + 'px'
    }
  }

  /**
   * Searches for the specific request obj and updates the name
   */
  this.updateRequestName = function (requestID, name) {
    log(self.name + ': Updateing request name ...')

    log('Updating the tree')
    for (let i = 0; i < reqTree.length; i++) {
      var categoryObj = reqTree[i]
      var result = getTreeElement(categoryObj.requests, requestID, name)
      if (result !== null) {
        log('result found')
      }
    }

    log('Updating the dom')
    for (let i = 0; i < requestMapping.length; i++) {
      var elem = requestMapping[i]
      if (elem._id === requestID) {
        log('Match found. Updating Navmenu name to' + name)
        var matchDom = document.getElementById('reqTitle-' + elem.domID)
        if (matchDom) {
          matchDom.innerHTML = name
        }
        break
      }
    }
  }

  var getTreeElement = function (reqs, requestID, name) {
    for (var i = 0; i < reqs.length; i++) {
      var req = reqs[i]
      if (req._id === requestID) {
        req.name = name
        return req
      } else {
        if (req.requests && req.requests.length > 0) {
          var result = getTreeElement(req.requests, requestID, name)
          if (result !== null) {
            return result
          }
        }
      }
    }
    return null
  }

  /**
   * Triggered by the Request module when a response comes back down from the server.
   */
  this.addRequest = function (data) {
    log(self.name + ': Adding new request')

    if (!data && !data.payload && !data.result) {
      return
    }
    let lvl = data.payload.lvl

    if (!data.payload.lvl) {
      lvl = 0
    } else {
      if (typeof lvl !== 'number' && lvl.isNaN()) {
        if (typeof parseInt(lvl) === 'number') {
          lvl = parseInt(lvl)
          if (lvl < 0) {
            return
          }
        } else {
          return
        }
      }
    }

    lvl = lvl + 1

    var uniqueID = Forge.getUniqueID()
    var newParent = document.getElementById('reqListDiv-' + data.payload.uniqueID)

    var reqListBox = Forge.build({'dom': 'div', 'id': 'reqListBox-' + uniqueID, 'class': 'req-nav-listBox'})
    var reqListInput = Forge.build({'dom': 'input', 'id': 'reqTitleInput-' + uniqueID, 'type': 'text', 'value': '', 'placeholder': 'Add Name', 'class': 'req-nav-listInput', 'style': 'padding-left:' + (PADDING_LEFT * lvl) + 'px'})
    var reqListDrag = Forge.build({'dom': 'i', 'id': 'reqDrag-' + uniqueID, 'class': 'auvicon-drag req-nav-listDrag'})
    var reqListAdd = Forge.build({ 'dom': 'div', 'id': 'reqAdd-' + uniqueID, 'class': 'auvicon-add req-nav-listAdd' })
    var reqCatListDiv = Forge.build({ 'dom': 'div', 'id': 'reqListDiv-' + uniqueID, 'class': 'sortable req-nav-listDiv', 'style': 'display:inherit; min-height: 4px;' })
    reqListInput.addEventListener('keyup', function (evt) {
      if (evt.code === 'Enter') {
        if (!this.value) {
          ctrl.displayMessage('Please Enter Request Name.')
        } else {
          self.trigger('view-request', data.result._id)
          this.blur()
        }
      }
    })
    reqListBox.appendChild(reqListInput)
    reqListBox.appendChild(reqListDrag)
    reqListBox.appendChild(reqListAdd)

    if (!newParent) {
      return
    }

    newParent.appendChild(reqListBox)
    requestMapping.push({ domID: uniqueID, _id: data.result._id, lvl: lvl })

    var json = {}
    json.category = data.result.category
    json.parent = data.result._id
    json.uniqueID = uniqueID
    json.lvl = lvl
    reqListAdd.addEventListener('click', function () {
      $('.sortable').sortable('option', 'disabled', true)
      log('Request Icon: Add request clicked')
      self.trigger('add-request', json)
    })
    // Checking if the name has changed, removing the input field and showing the regular one
    // It then sends an update-request to the server
    reqListInput.addEventListener('blur', function () {
      $(reqListBox).removeClass('editing')
      log('Request Input: blurring input')
      if (reqListInput.value) {
        var isUnique = true
        for (let i = 0; i < ctrl.currentRequests.length; i++) {
          if (ctrl.currentRequests[i].name === reqListInput.value) {
            isUnique = false
          }
        }
        if (isUnique) {
          reqListInput.style.display = 'none'

          var reqListTitle = Forge.build({'dom': 'div', 'id': 'reqTitle-' + uniqueID, 'text': reqListInput.value, 'class': 'req-nav-listTitle noSelect', 'style': 'padding-left:' + (PADDING_LEFT * lvl) + 'px'})
          reqListBox.appendChild(reqListTitle)
          reqListBox.appendChild(reqCatListDiv)
          self.trigger('update-request', {name: reqListInput.value, requestID: data.result._id})

          reqListBox.addEventListener('mouseover', function (e) {
            reqListAdd.style.opacity = '1'
            reqListDrag.style.opacity = '1'

            const checkMark = document.getElementById('reqCheckmark-' + uniqueID)
            if (checkMark) {
              checkMark.style.opacity = '0'
            }
            e.stopPropagation()
          })

          reqListBox.addEventListener('mouseout', function () {
            reqListDrag.style.opacity = '0'
            reqListAdd.style.opacity = '0'
            const checkMark = document.getElementById('reqCheckmark-' + uniqueID)
            if (checkMark) {
              checkMark.style.opacity = '1'
            }
          })

          setupSortableList()
          $('.sortable').sortable('option', 'disabled', false)

          reqListBox.addEventListener('click', function (e) {
            log('Request Item: clicking box')
            var currentReqID
            var thisId = this.getAttribute('id').replace(/^[^-]+-/, '')
            for (var i = 0; i < requestMapping.length; i++) {
              if (requestMapping[i].domID === thisId) {
                currentReqID = requestMapping[i]._id
                continue
              }
            }
            self.trigger('view-request', currentReqID)

            e.stopPropagation()
          })

          reqListBox.style.border = null
          reqListBox.style.borderRadius = null
        } else {
          $('.sortable').sortable('option', 'disabled', true)
          self.trigger('flash', { content: 'Request name should be unique.', type: 'ERROR' })
          reqListInput.focus()
          reqListBox.style.border = 'solid 1px #f3672f'
          reqListBox.style.borderRadius = '4px'
        }
      } else {
        $('.sortable').sortable('option', 'disabled', true)
        self.trigger('flash', { content: 'Please choose a request name', type: 'ERROR' })
        reqListInput.focus()
        reqListBox.style.border = 'solid 1px #f3672f'
        reqListBox.style.borderRadius = '4px'
      }
    })

    $(reqListInput).on('focus', function () {
      $(reqListBox).addClass('editing')
    })
    reqListInput.focus()
  }

  /**
   * This is responsible for adding a request to the DOM, as well as adding the
   * domID<->requestID mapping into the requestMapping array. Recursive through
   * all child requests.
   */
  var buildInitialRequest = function (reqItem, parentEle, lvl) {
    log(self.name + ': Building request (' + reqItem._id + ')')
    var uniqueID = Forge.getUniqueID()
    var json = {}
    json.category = reqItem.category
    json.parent = reqItem._id
    json.uniqueID = uniqueID
    json.lvl = lvl
    var reqListBox = Forge.build({'dom': 'div', 'id': 'reqListBox-' + uniqueID, 'class': 'req-nav-listBox'})
    var reqListTitle = Forge.build({'dom': 'div', 'id': 'reqTitle-' + uniqueID, 'text': reqItem.name, 'class': 'req-nav-listTitle noSelect', 'style': 'padding-left:' + (PADDING_LEFT * lvl) + 'px'})
    var reqCatListDiv = Forge.build({ 'dom': 'div', 'id': 'reqListDiv-' + uniqueID, 'style': 'display:inherit; min-height: 4px;' })

    if (userType === 'AUDITOR') {
      reqCatListDiv.className = 'sortable'
      var reqListDrag = Forge.build({'dom': 'i', 'id': 'reqDrag-' + uniqueID, 'class': 'auvicon-drag req-nav-listDrag'})
      var reqListAdd = Forge.build({'dom': 'div', 'id': 'reqAdd-' + uniqueID, 'class': 'auvicon-add req-nav-listAdd'})
      reqListBox.appendChild(reqListDrag)
      reqListBox.appendChild(reqListAdd)

      reqListAdd.addEventListener('click', function () {
        log('Request Icon: Add request clicked')
        self.trigger('add-request', json)
      })

      reqListBox.addEventListener('mouseover', function (e) {
        reqListAdd.style.opacity = '1'
        reqListDrag.style.opacity = '1'

        if (document.getElementById('reqCheckmark-' + uniqueID)) {
          document.getElementById('reqCheckmark-' + uniqueID).style.opacity = '0'
        }

        e.stopPropagation()
      })

      reqListBox.addEventListener('mouseout', function () {
        reqListDrag.style.opacity = '0'
        reqListAdd.style.opacity = '0'

        if (document.getElementById('reqCheckmark-' + uniqueID)) {
          document.getElementById('reqCheckmark-' + uniqueID).style.opacity = '1'
        }
      })
    }
    requestMapping.push({ domID: uniqueID, _id: reqItem._id, lvl: lvl })

    if (reqItem.items.length > 0) {
      var activeFiles = 0
      for (var i = 0; i < reqItem.items.length; i++) {
        if (reqItem.items[i].status === 'ACTIVE') {
          activeFiles++
        }
      }

      if (activeFiles > 0) {
        var reqListFinMark = Forge.build({dom: 'div', class: 'auvicon-checkmark req-nav-checkmark', id: 'reqCheckmark-' + uniqueID})
        reqListBox.appendChild(reqListFinMark)
      }
    }

    reqListBox.addEventListener('click', function (e) {
      log('Request Box: View request.')
      var requestID
      var thisId = this.getAttribute('id').replace(/^[^-]+-/, '')
      for (var i = 0; i < requestMapping.length; i++) {
        if (requestMapping[i].domID === thisId) {
          requestID = requestMapping[i]._id
          break
        }
      }
      if (requestID) {
        self.trigger('view-request', requestID)
      }
      e.stopPropagation()
    })

    reqListBox.appendChild(reqListTitle)
    reqListBox.appendChild(reqCatListDiv)
    parentEle.appendChild(reqListBox)

    if (reqItem.requests) {
      for (var i = 0; i < reqItem.requests.length; i++) {
        buildInitialRequest(reqItem.requests[i], reqCatListDiv, (lvl + 1))
      }
    }

    setupSortableList()
    $('.sortable').sortable('option', 'disabled', false)
  }

  var findCatByReqID = function (requestArray, requestID) {
    for (let i = 0; i < requestArray.length; i++) {
      if (requestArray[i]._id === requestID) {
        return requestArray[i].category
      } else {
        if (requestArray[i].requests.length !== 0) {
          findCatByReqID(requestArray[i].requests, requestID)
        }
      }
    }
  }

  var updateChildRequest = function (childArray, category, parentLvl) {
    for (let i = 0; i < childArray.length; i++) {
      var childObjDomID = childArray[i].id
      var childObjUniqueID = childObjDomID.substr(11)
      var childObjReq = requestMapping.find((element) => {
        return element.domID === childObjUniqueID
      })
      var childObjReqID = childObjReq._id

      self.trigger('update-request', { category: category, parentLvl: parentLvl, uniqueID: childObjUniqueID, requestID: childObjReqID})

      if (childArray[i].childNodes[childArray[i].childNodes.length - 1].childNodes.length !== 0) {
        updateChildRequest(childArray[i].childNodes[childArray[i].childNodes.length - 1].childNodes, category, parentLvl + 1)
      }
    }
  }

  /**
   * This function searches childNodes of the given node and remove 'sortable-parent' class which highlights
   * the parent request of the current request.
   */
  var searchChildNodes = function (currentNode) {
    $(currentNode).children().find('.sortable-parent').each(function () {
      $(this).removeClass('sortable-parent')
    })
  }

  /**
   * This function setup and initialize 'Sortable' jquery ui.
   * Sortable allows user to drag and drop the list in the appropriate category.
   * more info about sortable options: http://api.jqueryui.com/sortable/#option-connectWith
   *
   * Methods triggered when using sortable ui.
   * - create: triggered when sortable ui is initialised.
   * - start: triggered when user starts sorting by click and drag.
   * - change: triggered when user is still sorting the item and the DOM position has changed.
   * - update: triggered when user stopped sorting and the DOM position has changed.
   * - receive: triggered when the item user is sorting dropped into different sort list.
   * - out: tiggered when the item user is sorting is moved away from the droppable area. (outside of 'ui-sortable')
   * - stop: triggered when the user stopped sorting by dropping the item. The position doesn't have to be changed.
   */
  var setupSortableList = function () {
    $('.sortable').sortable({
      forcePlaceholderSize: true,
      placeholder: 'req-sortable-placeholder',
      helper: 'clone',
      cancel: 'div.req-nav-listAdd, div.req-nav-listTItle',
      connectWith: '.sortable',
      axis: 'y',
      items: '> div',
      classes: {
        'ui-sortable': 'no-background-hover',
        'ui-sortable-disabled': 'disabled-sortable',
        'ui-sortable-helper': 'drag-helper'
      },
      create: function (event, ui) {
        log('request sortable initialised')
      },
      start: function (event, ui) {
        var movedObjUniqueID = ui.item[0].id.substr(11)
        document.getElementById('reqDrag-' + movedObjUniqueID).style.color = 'transparent'
      },
      change: function (event, ui) {
        ui.placeholder[0].style.height = '32px'

        if (ui.sender) {
          searchChildNodes(ui.sender[0])
          $(ui.sender[0]).parents().find('.ui-sortable').each(function () {
            searchChildNodes(this)
          })
        }
        $(ui.item[0]).parents().find('.ui-sortable').each(function () {
          searchChildNodes(this)
        })
        $(ui.placeholder[0]).parent().prev().addClass('sortable-parent')

        var movedObjUniqueID = ui.item[0].id.substr(11)
        document.getElementById('reqDrag-' + movedObjUniqueID).style.color = 'transparent'
      },
      stop: function (event, ui) {
        if (ui.sender) {
          searchChildNodes(ui.sender[0])
          $(ui.sender[0]).parents().find('.ui-sortable').each(function () {
            searchChildNodes(this)
          })
        }
        if ($(ui.placeholder[0]).parent().length !== 0) {
          $(ui.placeholder[0]).parent().prev().removeClass('sortable-parent')
        }
        $(ui.item[0]).parent().prev().removeClass('sortable-parent')

        var movedObjDomID = ui.item[0].id
        var movedObjUniqueID = ui.item[0].id.substr(11)
        var movedObjReq = requestMapping.find((element) => {
          return element.domID === movedObjUniqueID
        })
        var movedObjReqID = movedObjReq._id

        // only update when the location is changed
        if (event.target.id !== document.getElementById(movedObjDomID).parentNode.id) {
          var parentUniqueID = document.getElementById(movedObjDomID).parentNode.id.substr(11)
          var parentObjReq = requestMapping.find((element) => {
            return element.domID === parentUniqueID
          })

          var parentObjReqLvl = parentObjReq.lvl
          if (!parentObjReqLvl) {
            parentObjReqLvl = 0
          }
          var parentObjReqID
          if (parentObjReq) {
            parentObjReqID = parentObjReq._id
          } else {
            parentObjReqID = 'not found'
          }

          if (parentObjReqID === undefined) {
            parentObjReqID = 'category'
          }

          var parentCategory

          if (parentObjReqID === 'category') { // parent is category
            for (var i = 0; i < reqTree.length; i++) {
              if (reqTree[i].name.toUpperCase() === document.getElementById('reqTitle-' + parentUniqueID).innerText) {
                parentCategory = reqTree[i].name
              }
            }
          } else if (parentObjReqID !== 'not found') { // parent is request
            if (reqTree.length > 1) {
              for (let i = 0; i < reqTree.length; i++) {
                if (reqTree[i].requests.length !== 0) {
                  parentCategory = findCatByReqID(reqTree[i].requests, parentObjReqID)
                }
              }
            } else if (reqTree.length === 1) {
              parentCategory = reqTree[0].name
            }
          }
          self.trigger('update-request', { parent: parentObjReqID, category: parentCategory, parentLvl: parentObjReqLvl, uniqueID: movedObjUniqueID, requestID: movedObjReqID})

          if (ui.item[0].childNodes[2].childNodes.length === 0) {
            log('no child objects attached to this object ')
          } else {
            updateChildRequest(ui.item[0].childNodes[ui.item[0].childNodes.length - 1].childNodes, parentCategory, parentObjReqLvl + 1)
          }
        }
      },
      update: function (event, ui) {
        if ($(ui.placeholder[0]).parent().length !== 0) {
          $(ui.placeholder[0]).parent().prev().removeClass('sortable-parent')
        }
        if (ui.sender) {
          searchChildNodes(ui.sender[0])
          $(ui.sender[0]).parents().find('.ui-sortable').each(function () {
            searchChildNodes(this)
          })
        }
        $(ui.item[0]).parents().find('.ui-sortable').each(function () {
          searchChildNodes(this)
        })
        var movedObjUniqueID = ui.item[0].id.substr(11)
        document.getElementById('reqDrag-' + movedObjUniqueID).style.color = 'transparent'
      },
      receive: function (event, ui) {
        var movedObjUniqueID = ui.item[0].id.substr(11)
        document.getElementById('reqDrag-' + movedObjUniqueID).style.color = 'transparent'
      },
      out: function (event, ui) {
        var movedObjUniqueID = ui.item[0].id.substr(11)
        document.getElementById('reqDrag-' + movedObjUniqueID).style.color = 'transparent'
      }
    }).sortable('disable')
  }

  /**
   * Is responsible for taking the requestTree and generating the dom for all of the
   * items. This includes both the categories and the requests.
   */
  var buildTreeStructure = function (requests, parent) {
    requests.forEach((request) => {
      const uniqueID = Forge.getUniqueID()
      var reqCategoryBox = Forge.build({'dom': 'div', 'id': 'reqCategoryBox-' + uniqueID, 'class': 'req-nav-catBox noSelect'})
      var reqCatTitle = Forge.build({'dom': 'div', 'id': 'reqTitle-' + uniqueID, 'text': request.name, 'class': 'req-nav-catTitle'})
      var reqCatListDiv = Forge.build({ 'dom': 'div', 'id': 'reqListDiv-' + uniqueID, 'style': 'display:inherit; min-height: 4px;' })

      reqCategoryBox.appendChild(reqCatTitle)

      if (userType === 'AUDITOR') {
        reqCatListDiv.className = 'sortable'
        var reqCatAdd = Forge.build({'dom': 'div', 'id': 'reqAdd-' + uniqueID, 'class': 'auvicon-add req-nav-listAdd'})
        reqCatAdd.addEventListener('click', function () {
          self.trigger('add-request', {
            category: request.name,
            uniqueID
          })
        })
        reqCategoryBox.appendChild(reqCatAdd)
        reqCategoryBox.addEventListener('mouseover', function () {
          reqCatAdd.style.opacity = '1'
        })
        reqCategoryBox.addEventListener('mouseout', function () {
          reqCatAdd.style.opacity = '0'
        })
      }

      reqCategoryBox.appendChild(reqCatListDiv)
      parent.appendChild(reqCategoryBox)

      requestMapping.push({ domID: uniqueID, _id: request._id })

      if (request.requests) {
        request.requests.forEach((request) => {
          buildInitialRequest(request, reqCatListDiv, 1)
        })
      }
    })
  }

  /**
   * Loops through the flat requests array and updates the request complete
   * coounter if a request has a file.
   */
  this.updateRequestStats = function (requests) {
    log(self.name + ': Updating the request statistics')
    if (!requests) {
      return
    }
    numOfRequests = 0
    numOfCompletedRequests = 0
    for (var i = 0; i < requests.length; i++) {
      if (requests[i].status !== 'INACTIVE') {
        numOfRequests++
        let fileExist = false
        if (requests[i].items.length) {
          for (let j = 0; j < requests[i].items.length; j++) {
            let file = requests[i].items[j]
            if (file.status === 'ACTIVE') {
              fileExist = true
            }
          }
        }
        if (fileExist) {
          self.requestCompleted(requests[i]._id)
          numOfCompletedRequests++
        } else {
          self.unmarkComplete(requests[i]._id)
        }
      }
    }
    document.getElementById('request-completedNumber').innerHTML = numOfCompletedRequests
    document.getElementById('request-totalNumber').innerHTML = numOfRequests
  }

  /**
   * Build a new Cateogry DOM object and appends it to the DOM.
   */
  var buildNewCategory = function (categoryContainer) {
    log(self.name + ': Building a new category ...')
    var uniqueID = Forge.getUniqueID()
    var reqCategoryBox = Forge.build({'dom': 'div', 'id': 'reqCategoryBox-' + uniqueID, 'class': 'req-nav-catBox noSelect'})
    var reqCatInput = Forge.build({'dom': 'input', 'id': 'reqCategoryInput-' + uniqueID, 'type': 'text', placeholder: 'UNTITLED', 'class': 'req-nav-catInput'})
    var reqCatAdd = Forge.build({'dom': 'div', 'id': 'reqAdd-' + uniqueID, 'class': 'auvicon-add req-nav-listAdd', 'style': 'display:none;'})
    var reqCatListDiv = Forge.build({'dom': 'div', 'id': 'reqListDiv-' + uniqueID, 'class': 'sortable', 'style': 'display:inherit; min-height: 4px;'})
    reqCategoryBox.appendChild(reqCatInput)
    reqCategoryBox.appendChild(reqCatAdd)
    reqCategoryBox.appendChild(reqCatListDiv)

    reqCatInput.addEventListener('blur', function () {
      $(reqCategoryBox).removeClass('req-nav-catBox-editing')
      updateCategoryName(uniqueID)
    })
    reqCatAdd.addEventListener('click', function () {
      log(self.name + ': Adding new request ...')
      var newRequest = {}
      newRequest.category = reqCatInput.value
      newRequest.uniqueID = uniqueID
      newRequest.lvl = 0
      self.trigger('add-request', newRequest)
    })

    $(reqCatInput).on('focus', () => {
      $(reqCategoryBox).addClass('req-nav-catBox-editing')
    })

    categoryContainer.appendChild(reqCategoryBox)
    reqCatInput.focus()
  }

  /**
   * Updating the category name. Only allows you to change the category name
   * once. We aren't allowed to change it multiple times. Only when initially created.
   */
  var updateCategoryName = function (uniqueID) {
    log(self.name + ': Updating category name ...')
    var reqCategoryBox = document.getElementById('reqCategoryBox-' + uniqueID)
    var reqCatInput = document.getElementById('reqCategoryInput-' + uniqueID)
    const createBtn = $('#req-nav-newCatBtn')

    if (reqCatInput.value && reqCatInput.value !== 'UNTITLED') {
      createCategoryFailed(createBtn, reqCategoryBox)
      self.trigger('add-category', reqCatInput.value)
      currentAddingCategoryId = uniqueID
    } else {
      createCategoryFailed(createBtn, reqCategoryBox)
      self.trigger('flash', { content: 'Please choose a category name', type: 'ERROR'})
    }
  }

  /**
   * Generates the DOM structure for the Request Navigation component.
   * It is responsible for returning said DOM so that it can be built.
   */
  var buildContent = function (settings) {
    log(self.name + ': Building dom content ...')
    if (!parentDOM) {
      return
    }
    var componentContentDiv = Forge.build({'dom': 'div'})

    // Header stuffs n things
    var headerContainer = Forge.build({'dom': 'div', 'style': 'height: 60px; margin: 0px 16px; border-bottom:1px solid #e1e1e1;'})
    var navHeaderTitle = Forge.build({'dom': 'p', 'text': 'Requests', 'class': 'req-nav-title'})
    var navHeaderSearch = Forge.build({'dom': 'div', 'class': 'auvicon-line-search req-nav-search'})
    var requestDDL = Forge.build({'dom': 'div', 'id': 'm-req-prf', 'class': 'request-dropdownHolder'})

    var requestDDLTrigger = Forge.build({'dom': 'div', 'class': ''})/* @TJEP need to add proper drop down */
    var triggerSpan = Forge.build({'dom': 'span', 'id': 'dashboardUsername', 'class': 'request-navTitle', 'text': 'All Requests'})
    var triggerIcon = Forge.build({'dom': 'i', 'class': 'auvicon-chevron-down', 'style': 'padding-left:7px; font-size:10px; display:none;'})/* @TJEP display:none until post 0.7 */

    requestDDLTrigger.appendChild(triggerSpan)
    requestDDLTrigger.appendChild(triggerIcon)

    var requestDDLContainer = Forge.build({'dom': 'div', 'class': 'au-dropdown-container'})
    var connectorDiv = Forge.build({'dom': 'div', 'class': 'request-dropdown-hoverConnector'})
    var connectorIcon = Forge.build({'dom': 'i', 'class': 'fa fa-caret-up au-dropdown-caret', 'style': 'font-size:56px'})
    var connectorShadow = Forge.build({'dom': 'div', 'class': 'au-dropdown-shadow'})

    connectorDiv.appendChild(connectorIcon)
    connectorDiv.appendChild(connectorShadow)
    requestDDLContainer.appendChild(connectorDiv)

    var ddlContent = Forge.build({'dom': 'div', 'id': 'm-req-dropdown-menu', 'class': 'request-dropdown-menu'})

    var requestItemList = Forge.build({'dom': 'div', 'id': 'au-dropdown-itemList'})
    var requestItem0 = Forge.build({'dom': 'div', 'id': 'm-req-item-settings', 'class': 'au-dropdown-linkContainer'})
    var item0Link = Forge.build({'dom': 'a', 'class': 'au-dropdownLink', 'text': 'All Requests'})
    var requestItem1 = Forge.build({'dom': 'div', 'id': 'm-req-item-settings', 'class': 'au-dropdown-linkContainer'})
    var item1Link = Forge.build({'dom': 'a', 'class': 'au-dropdownLink', 'text': 'Completed Requests'})
    var requestItem2 = Forge.build({'dom': 'div', 'id': 'm-req-item-settings', 'class': 'au-dropdown-linkContainer'})
    var item2Link = Forge.build({'dom': 'a', 'class': 'au-dropdownLink', 'text': 'Incomplete Requests'})

    var seperator = Forge.build({'dom': 'div', 'style': 'height: 2px;border-bottom: 1px solid; color: #ddd;'})
    var requestItem3 = Forge.build({'dom': 'div', 'id': 'm-req-item-changeTemplate', 'class': 'au-dropdown-linkContainer'})
    var selectTemplateLink = Forge.build({'dom': 'a', 'class': 'au-dropdownLink', 'text': 'Select from Template'})
    var requestItem4 = Forge.build({'dom': 'div', 'id': 'm-req-item-dueDate', 'class': 'au-dropdown-linkContainer'})
    var selectDueDateLink = Forge.build({'dom': 'a', 'class': 'au-dropdownLink', 'text': 'Select Main Due Date'})

    requestItemList.appendChild(requestItem0)
    requestItemList.appendChild(requestItem1)
    requestItemList.appendChild(requestItem2)
    requestItem0.appendChild(item0Link)
    requestItem1.appendChild(item1Link)
    requestItem2.appendChild(item2Link)
    requestItemList.appendChild(seperator)
    requestItemList.appendChild(requestItem3)
    requestItemList.appendChild(requestItem4)
    requestItem3.appendChild(selectTemplateLink)
    requestItem4.appendChild(selectDueDateLink)

    // ddlContent.appendChild(requestBtnContainerDiv);
    ddlContent.appendChild(requestItemList)

    requestDDLContainer.appendChild(ddlContent)
    requestDDL.appendChild(requestDDLTrigger)
    requestDDL.appendChild(requestDDLContainer)
    // navHeader.appendChild(navHeaderTitle);
    headerContainer.appendChild(requestDDL)
    componentContentDiv.appendChild(headerContainer)

    /** ******Request List Body**********************/
    // Request list gets built out here. @MATT - This gets redone on loadData, can I remove it here?
    var contentContainer = Forge.build({'dom': 'div', 'style': 'min-height: 321px; padding:10px 8px;overflow-x: hidden; overflow-y: auto;'})
    var categoryContainer = Forge.build({ 'dom': 'div', 'id': 'request-categories', 'class': '' })

    var newCategoryBtn = Forge.buildBtn({'id': 'req-nav-newCatBtn', 'text': 'Create Category', 'type': 'link', 'margin': ' 10px 16px'}).obj
    newCategoryBtn.addEventListener('click', function () {
      buildNewCategory(categoryContainer)
    })

    contentContainer.appendChild(categoryContainer)
    contentContainer.appendChild(newCategoryBtn)
    componentContentDiv.appendChild(contentContainer)

    /** *****Footer stuff***************************/
    var footerContainer = Forge.build({'dom': 'div', 'style': 'border-top: 1px solid #e1e1e1; height: 74px; width: 264px;'})
    var navFooterText = Forge.build({'dom': 'p', 'style': 'font-weight:bold;font-size:16px;text-align:center;margin:24px;color:#707070;'})
    var navCompletedReq = Forge.build({'dom': 'span', 'id': 'request-completedNumber'})
    var navFootOf = Forge.build({'dom': 'span', 'text': ' of '})
    var navTotalReq = Forge.build({'dom': 'span', 'id': 'request-totalNumber'})
    var navFootComplete = Forge.build({'dom': 'span', 'text': ' Complete'})

    footerContainer.appendChild(navFooterText)
    navFooterText.appendChild(navCompletedReq)
    navFooterText.appendChild(navFootOf)
    navFooterText.appendChild(navTotalReq)
    navFooterText.appendChild(navFootComplete)
    componentContentDiv.appendChild(footerContainer)

    parentDOM.appendChild(componentContentDiv)
  }

  buildContent()
  registerEvents()
}

Component_Requests_Navigation.prototype = ComponentTemplate.prototype
Component_Requests_Navigation.prototype.constructor = ComponentTemplate

module.exports = Component_Requests_Navigation
