/**
 * Created by Bao Nguyen on 7/13/2017.
 */
/**
 * Created by Bao Nguyen on 7/13/2017.
 */
'use strict'

import ComponentTemplate from '../ComponentTemplate'
import Utility from '../../../js/src/utility_c'
import Forge from '../../Forge'
import log from '../../../js/src/log'

import './technique.css'

var Component_Technique = function (ctrl, container, owner) {
  ComponentTemplate.call(this, ctrl, container, owner)

  var parentDOM = container
  var c = ctrl
  var self = this
  var text = c.lang.workingPaperTechiqueComponent

  var fields = {
    samplingTechniqueDropdown: {id: 'workingpaper-samplingTechniqueDropdown', type: 'text', value: '', model: {name: 'workingpaper-samplingTechniqueDropdown', field: 'samplingTechniqueDropdown'}},
    RiskOfOverRelianceInput: {id: 'workingpaper-RiskOfOverRelianceInput', type: 'text', value: '', model: {name: 'workingpaper-RiskOfOverRelianceInput', field: 'RiskOfOverRelianceInput'}},
    EPDRInput: {id: 'workingpaper-EPDRInput', type: 'text', value: '', model: {name: 'workingpaper-EPDRInput', field: 'EPDRInput'}},
    TRDInput: {id: 'workingpaper-TRDInput', type: 'text', value: '', model: {name: 'workingpaper-TRDInput', field: 'TRDInput'}},
    sampleSizeInput: {id: 'workingpaper-sampleSizeInput', type: 'text', value: '', model: {name: 'workingpaper-sampleSizeInput', field: 'sampleSizeInput'}},
  }

  var samplingTechniqueList = []

  const initialize = function () {
    log('Component_workingPaperProcedure: Initializing...')
    registerEvents()

    $(`#${fields.RiskOfOverRelianceInput.id}`).parent().hide()
    $(`#${fields.EPDRInput.id}`).parent().hide()
    $(`#${fields.TRDInput.id}`).parent().hide()
  }

  const registerEvents = function () {
    c.registerForEvent('load-technique-type', 'LOAD-WORKING-PAPER-TECHNIQUE-TYPE-001', responseLoadData)
  }

  var responseLoadData = function (tag, data) {
    bindingData(data).then(buildContent())
  }

  var bindingData = function (data) {
    return new Promise((resolve, reject) => {
      var dataResult = JSON.parse(data.result)

      for (let item in dataResult) {
        samplingTechniqueList.push({text: dataResult[item]})
      }

      resolve()
    })
  }

  var buildContent = function (settings) {
    if (!parentDOM) {
      return
    }

    var componentBody = Forge.build({'dom': 'div', 'id': 'workingpaper-technique-component-body', 'class': 'workingpaper-modal-body'})
    var inputContainer = Forge.build({'dom': 'div', 'class': 'm-ce-setup-inputContainer'})

    var title = Forge.build({dom: 'h4', class: 'm-ce-technique-title', text: text.performanceMaterialityLabel})

    var inputrow1 = Forge.build({dom: 'div', class: 'input-inline-block'})
    var inputrow2 = Forge.build({dom: 'div', class: 'input-inline-block'})

    var samplingTechniqueDropdown = Forge.buildInputDropdown({'label': text.samplingTechniqueLabel, 'list': samplingTechniqueList, 'id': fields.samplingTechniqueDropdown.id, 'placeholder': 'Select from list', errorText: text.samplingTechniqueLabelError})
    var riskOfOverRelianceInput = Forge.buildInput({'label': text.RiskOfOverRelianceLabel, 'errorText': text.RiskOfOverRelianceLabelError, 'id': fields.RiskOfOverRelianceInput.id, 'placeholder': '0.00', width: '150px'})
    var EPDRInput = Forge.buildInput({'label': text.EPDRLabel, 'errorText': text.EPDRLabelError, 'id': fields.EPDRInput.id, 'placeholder': '0.00', width: '150px'})
    var TRDInput = Forge.buildInput({'label': text.TRDLabel, 'errorText': text.TRDLabelError, 'id': fields.TRDInput.id, 'placeholder': '0.00', width: '150px'})
    var sampleSizeInput = Forge.buildInput({'label': text.sampleSizeLabel, 'errorText': text.sampleSizeLabelError, 'id': fields.sampleSizeInput.id, 'placeholder': '0.00', width: '150px'})
    var editSampleSizeLink = Forge.build({dom: 'div', id: 'editSampleSizeInput', text: 'edit', style: 'display: none;'})
    editSampleSizeLink.onclick = function () {
      $(`#${fields.sampleSizeInput.id}`).removeAttr('disabled')
    }

    var timeRiskOfOverReliance = null
    riskOfOverRelianceInput.children[1].onblur = riskOfOverRelianceInput.children[1].onkeydown = function () {
      clearTimeout(timeRiskOfOverReliance)
      var context = this
      timeRiskOfOverReliance = setTimeout(function () {
        fields.RiskOfOverRelianceInput.value = riskOfOverRelianceInput.children[1].value
        if (fields.RiskOfOverRelianceInput.value === '' || !Utility.REGEX.unitNumber.test(fields.RiskOfOverRelianceInput.value)) {
          context.classList.add('auv-error')
        } else {
          context.classList.remove('auv-error')
          CalculateSampleSize()
        }
      }, 100)
    }

    var timeEPDRInput = null
    EPDRInput.children[1].onblur = EPDRInput.children[1].onkeydown = function () {
      clearTimeout(timeEPDRInput)
      var context = this
      timeEPDRInput = setTimeout(function () {
        fields.EPDRInput.value = EPDRInput.children[1].value
        if (fields.EPDRInput.value === '' || !Utility.REGEX.unitNumber.test(fields.EPDRInput.value)) {
          context.classList.add('auv-error')
        } else {
          context.classList.remove('auv-error')
          CalculateSampleSize()
        }
      }, 100)
    }

    var timeTRDInput = null
    TRDInput.children[1].onblur = TRDInput.children[1].onkeydown = function () {
      clearTimeout(timeTRDInput)
      var context = this
      timeTRDInput = setTimeout(function () {
        fields.TRDInput.value = TRDInput.children[1].value
        if (fields.TRDInput.value === '' || !Utility.REGEX.unitNumber.test(fields.TRDInput.value)) {
          context.classList.add('auv-error')
        } else {
          context.classList.remove('auv-error')
          CalculateSampleSize()
        }
      }, 100)
    }

    var timeSampleSize = null
    sampleSizeInput.children[1].onblur = sampleSizeInput.children[1].onkeydown = function () {
      clearTimeout(timeSampleSize)
      var context = this
      timeSampleSize = setTimeout(function () {
        fields.sampleSizeInput.value = sampleSizeInput.children[1].value
        if (fields.sampleSizeInput.value === '' || !Utility.REGEX.unitNumber.test(fields.sampleSizeInput.value)) {
          context.classList.add('auv-error')
        } else {
          context.classList.remove('auv-error')
        }
      }, 100)
    }

    var timeTechnique = null
    samplingTechniqueDropdown.children[2].onblur = samplingTechniqueDropdown.children[2].onkeydown = function (e) {
      clearTimeout(timeTechnique)
      var context = this
      timeTechnique = setTimeout(function () {
        fields.samplingTechniqueDropdown.value = samplingTechniqueDropdown.children[2].value
        if (fields.samplingTechniqueDropdown.value === '') {
          context.classList.add('auv-error')
          sampleSizeInput.text = ''
        } else {
          context.classList.remove('auv-error')
        }
      }, 100)
      if (samplingTechniqueDropdown.children[2].value.toLowerCase() === 'monetary unit sampling') {
        $(`#${fields.RiskOfOverRelianceInput.id}`).parent().show()
        $(`#${fields.EPDRInput.id}`).parent().show()
        $(`#${fields.TRDInput.id}`).parent().show()
        $(`#${fields.sampleSizeInput.id}`).attr('disabled', 'disabled')
        $(`#${editSampleSizeLink.id}`).show()
      } else {
        $(`#${fields.RiskOfOverRelianceInput.id}`).parent().hide()
        $(`#${fields.EPDRInput.id}`).parent().hide()
        $(`#${fields.TRDInput.id}`).parent().hide()
        $(`#${fields.sampleSizeInput.id}`).removeAttr('disabled')
        $(`#${editSampleSizeLink.id}`).hide()

        // reset value
        $(`#${fields.RiskOfOverRelianceInput.id}`).val('')
        $(`#${fields.EPDRInput.id}`).val('')
        $(`#${fields.TRDInput.id}`).val('')
        $(`#${fields.sampleSizeInput.id}`).val('')
      }
    }

    inputrow1.appendChild(riskOfOverRelianceInput)
    inputrow1.appendChild(EPDRInput)
    inputrow1.appendChild(TRDInput)

    inputrow2.appendChild(sampleSizeInput)
    inputrow2.appendChild(editSampleSizeLink)

    inputContainer.appendChild(samplingTechniqueDropdown)
    inputContainer.appendChild(inputrow1)
    inputContainer.appendChild(inputrow2)

    componentBody.appendChild(inputContainer)

    parentDOM.appendChild(componentBody)

    let CalculateSampleSize = function () {
      let sampleSizeValue = Number(fields.TRDInput.value) + Number(fields.EPDRInput.value) + Number(fields.RiskOfOverRelianceInput.value)
      sampleSizeInput.children[1].value = sampleSizeValue
    }
  }

  initialize()
}

Component_Technique.prototype = ComponentTemplate.prototype
Component_Technique.prototype.constructor = ComponentTemplate

module.exports = Component_Technique
