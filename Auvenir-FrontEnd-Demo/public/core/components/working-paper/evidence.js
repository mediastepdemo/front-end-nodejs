/**
 * Created by Bao Nguyen on 7/13/2017.
 */
/**
 * Created by Bao Nguyen on 7/13/2017.
 */
'use strict'

import ComponentTemplate from '../ComponentTemplate'
import Utility from '../../../js/src/utility_c'
import Forge from '../../Forge'
import log from '../../../js/src/log'

import styles from './evidence.css'

var Component_Evidence = function (ctrl, container, owner) {
  ComponentTemplate.call(this, ctrl, container, owner)

  var parentDOM = container
  var c = ctrl
  var self = this
  var text = c.lang.workingPaperEvidenceComponent
  var confirmEvidenceFlg = false

  var fields = {
    clientAssigneeDropdown: {id: 'workingpaper-clientAssigneeDropdown', type: 'text', value: '', model: {name: 'workingpaper-clientAssigneeDropdown', field: 'clientAssigneeDropdown'}},
    dueDate: {id: 'workingpaper-dueDate', type: 'text', value: '', model: {name: 'workingpaper-dueDate', field: 'dueDate'}},
    openingAddressInput: {id: 'workingpaper-openingAddressInput', type: 'text', value: '', model: {name: 'workingpaper-openingAddressInput', field: 'openingAddressInput'}},
    closingAddressInput: {id: 'workingpaper-closingAddressInput', type: 'text', value: '', model: {name: 'workingpaper-closingAddressInput', field: 'closingAddressInput'}},
    confirmCheckbox: {id: 'workingpaper-evidence-confirmCheckbox', type: 'text', value: '', model: {name: 'workingpaper-confirmCheckbox', field: 'confirmCheckbox'}},
  }

  var typeList = [ {text: 'Accounts Receivable'}, {text: 'Accounts Payable'}, {text: 'Inventory'}, {text: 'Cash'} ]

  var buildContent = function (settings) {
    if (!parentDOM) {
      return
    }

    var componentBody = Forge.build({'dom': 'p', 'id': 'workingPaper-evidence-component-body', 'class': 'workingpaper-modal-body'})

    var inputContainer = Forge.build({'dom': 'div', 'class': 'm-ce-evidence-inputContainer'})

    var inputrow1 = Forge.build({dom: 'div', class: 'input-left-align-block'})

    var clientAssigneeDropdown = Forge.buildInputDropdown({'label': text.clientAssigneeLabel, 'list': typeList, 'id': fields.clientAssigneeDropdown.id, 'placeholder': 'Select from list', errorText: text.clientAssigneeLabelError, width: '250px'})
    var dueDate = Forge.buildInputDropdownCalendar({'label': text.dueDateLabel, 'id': fields.dueDate.id, 'placeholder': text.dates, width: '160px', errorText: text.dueDateLabel})
    $(function () {
      $('#workingpaper-dueDate').datepicker()
    })

    var timeDueDate = null
    dueDate.children[2].onchange = dueDate.children[2].onkeydown = function () {
      clearTimeout(timeDueDate)
      var context = this
      context.classList.remove('auv-error')
      timeDueDate = setTimeout(function () {
        fields.dueDate.value = dueDate.children[2].value
        if (fields.dueDate.value === '' || !Utility.REGEX.date.test(fields.dueDate.value) || !Utility.isDateInFuture(fields.dueDate.value, true)) {
          context.classList.add('auv-error')
          if (!Utility.isDateInFuture(fields.deadline.value, true)) {
            context.nextSibling.innerHTML = text.dueDatePastError
          } else {
            context.nextSibling.innerHTML = text.dueDateLabelError
          }
        } else {
          context.classList.remove('auv-error')
        }
      }, 100)
    }

    var openingAddress = Forge.buildInputTextArea({ label: text.confirmationLetterOpenAddressLabel, errorText: text.confirmationLetterClosingAddressLabelError, 'id': fields.openingAddressInput.id, 'width': '520px', placeholder: `This is what your client and client's customer will see`})
    var closingAddress = Forge.buildInputTextArea({ label: text.confirmationLetterClosingAddressLabel, errorText: text.confirmationLetterClosingAddressLabelError, 'id': fields.closingAddressInput.id, 'width': '520px', placeholder: `This is what your client and client's customer will see`})

    const divCheckboxConfirmation = Forge.build({dom: 'div', class: 'title  mag-top-35'})
    const confirmCheckbox = Forge.build({dom: 'input', type: 'checkbox', id: 'checkboxEvidenceComfirmation'})
    const confirmCheckboxLabel = Forge.build({dom: 'label', id: 'checkboxEvidenceComfirmationLabel', style: 'max-width: 450px;', text: text.confirmationCheck, class: 'auv-inputTitle auv-inputTitle-checkbox'})

    confirmCheckbox.onclick = function () {
      confirmEvidenceFlg = !confirmEvidenceFlg
      if (confirmEvidenceFlg === true) {
        document.getElementById('m-ce-workingpaper-addBtn').removeAttribute('disabled')
      }
      else {
        document.getElementById('m-ce-workingpaper-addBtn').setAttribute('disabled', 'disabled')
      }
    }

    inputrow1.appendChild(clientAssigneeDropdown)
    inputrow1.appendChild(dueDate)

    divCheckboxConfirmation.appendChild(confirmCheckbox)
    divCheckboxConfirmation.appendChild(confirmCheckboxLabel)

    inputContainer.appendChild(inputrow1)
    inputContainer.appendChild(openingAddress)
    inputContainer.appendChild(closingAddress)
    inputContainer.appendChild(divCheckboxConfirmation)

    componentBody.appendChild(inputContainer)

    parentDOM.appendChild(componentBody)
  }

  buildContent()
}

Component_Evidence.prototype = ComponentTemplate.prototype
Component_Evidence.prototype.constructor = ComponentTemplate

module.exports = Component_Evidence
