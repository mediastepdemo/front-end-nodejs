/**
 * Created by Bao Nguyen on 7/27/2017.
 */

'use strict'

import ComponentTemplate from '../ComponentTemplate'
import Utility from '../../../js/src/utility_c'
import Forge from '../../Forge'
import log from '../../../js/src/log'

import './success.css'

var Component_Success = function (ctrl, container, owner) {
  ComponentTemplate.call(this, ctrl, container, owner)

  var parentDOM = container
  var c = ctrl
  var self = this
  var text = c.lang.workingPaperSetupComponent

  var buildContent = function (settings) {
    if (!parentDOM) {
      return
    }

    var componentBody = Forge.build({'dom': 'p', 'id': 'workingpaper-success-component-body', 'class': 'workingpaper-modal-body'})

    var inputContainer = Forge.build({'dom': 'div', 'class': 'm-ce-success-inputContainer'})

    var devContainer = Forge.build({'dom': 'div', 'style': 'width:520px; margin:150px auto 20px auto; text-align:center'})
    inputContainer.appendChild(devContainer)
    var loaderImage = Forge.build({'dom': 'img', 'src': 'images/icons/clipboard.png', 'style': 'width:107px; height:141px;'})
    devContainer.appendChild(loaderImage)
    var textDiv = Forge.build({'dom': 'div', class: 'success-alert', 'text': `Success! You've successfully setup your Working Papers and confirmation letters.`})
    devContainer.appendChild(textDiv)

    // add-success button => click to create new todo
    var renderSuccessButton = function () {
      const createNewTodo = Forge.buildBtn({text: 'Success', id: 'auv-working-paper-success-createToDo', type: 'primary', margin: '50px auto 0px auto'}).obj

      createNewTodo.addEventListener('click', () => {

        const dataTodoNew = {
          name: 'Account Receivable',
          status: 'ACTIVE',
          categoryName: 'Working Papers',
          dueDate: new Date()
        }

        createTodo(dataTodoNew)
        owner.close()
      })

      devContainer.appendChild(createNewTodo)
    }
    renderSuccessButton()

    inputContainer.appendChild(devContainer)
    componentBody.appendChild(inputContainer)

    parentDOM.appendChild(componentBody)
  }

  /**
   *  Update Mongo DB if there is a request.
   */
  var createTodo = function (data) {
    log('Adding new Todo working paper')

    data.createdBy = c.currentUser._id
    data.engagementID = c.currentEngagement._id
    log(JSON.stringify(data))
    c.sendEvent({name: 'create-todo', data: data})
  }

  buildContent()
}

Component_Success.prototype = ComponentTemplate.prototype
Component_Success.prototype.constructor = ComponentTemplate

module.exports = Component_Success
