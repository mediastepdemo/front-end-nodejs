/**
 * Created by Bao Nguyen on 7/13/2017.
 */
'use strict'

import ComponentTemplate from '../ComponentTemplate'
import Utility from '../../../js/src/utility_c'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import _ from 'lodash'

import './procedure.css'

var Component_Procedure = function (ctrl, container, owner) {
  ComponentTemplate.call(this, ctrl, container, owner)

  var parentDOM = container
  var c = ctrl
  var self = this
  var text = c.lang.workingPaperProcedureComponent

  var confirmFlg = false

  var testTypeComponentArray = []

  var checkboxFlag1 = false
  var checkboxFlag2 = false
  var checkboxFlag3 = false
  var checkboxFlag4 = false
  var checkboxFlag5 = false
  var checkboxFlag6 = false
  var checkboxFlag7 = false

  const initialize = function () {
    log('Component_workingPaperProcedure: Initializing...')
    registerEvents()
  }

  const registerEvents = function () {
    c.registerForEvent('load-test-type', 'LOAD-WORKING-PAPER-TEST-TYPE-001', responseLoadData)
  }

  var responseLoadData = function (tag, data) {
    bindingData(data).then(buildContent())
  }

  var bindingData = function (data) {
    return new Promise((resolve, reject) => {
      var dataResult = JSON.parse(data.result)

      dataResult.forEach((item) => {
        let testTypeComponent = {}
        testTypeComponent.id = item.TestTypeId
        testTypeComponent.title = item.TestTypeName
        testTypeComponent.content = item.TestTypeDesc

        testTypeComponentArray.push(testTypeComponent)
      })

      log(testTypeComponentArray)

      resolve()
    })
  }

  var buildContent = function (settings) {
    if (!parentDOM) {
      return
    }

    var componentBody = Forge.build({dom: 'div', 'id': 'workingpaper-procedure-component-body', class: 'workingpaper-modal-body'})

    var title = Forge.build({dom: 'h4', class: 'm-ce-procedure-title', text: text.headerLabel})
    var inputContainer = Forge.build({dom: 'div', class: 'm-ce-procedure-inputContainer'})

    var testTypeContainer1 = Forge.build({dom: 'div', id: 'checkboxContainer', class: 'item-checkbox ui accordion'})

    const divCheckboxConfirmation = Forge.build({dom: 'div', class: 'title  mag-top-35'})
    const divCheckboxTitle = Forge.build({dom: 'div', class: 'checkbox'})
    const confirmCheckbox = Forge.build({dom: 'input', type: 'checkbox', id: 'checkboxComfirmation'})
    const confirmCheckboxLabel = Forge.build({dom: 'label', id: 'checkboxComfirmationLabel', style: 'max-width: 450px;', text: 'By selecting this check box, I confirm the above procedural steps are appropriate for the purposes of [ this test ]', class: 'auv-inputTitle auv-inputTitle-checkbox'})

    var buildCheckBoxTestType = function (container) {
      for (let i = 0; i < testTypeComponentArray.length; i++) {
        const divTitle = Forge.build({dom: 'div', class: 'title'})
        const divContent = Forge.build({dom: 'div', id: testTypeComponentArray[i].id + 'Content', class: 'content', text: testTypeComponentArray[i].content })
        const iconDropdown = Forge.build({dom: 'span', id: testTypeComponentArray[i].id + 'Icon', class: 'auvicon-forward icon-collapse'})

        const divCheckbox = Forge.build({dom: 'div', class: 'checkbox'})
        const testTypeCheckbox = Forge.build({dom: 'input', type: 'checkbox', id: testTypeComponentArray[i].id, class: 'checkboxTypeTest'})
        const testTypeCheckboxLabel = Forge.build({dom: 'label', id: testTypeComponentArray[i].id + 'Label', text: testTypeComponentArray[i].title, class: 'auv-inputTitle auv-inputTitle-checkbox'})

        divCheckbox.appendChild(testTypeCheckbox)
        divCheckbox.appendChild(testTypeCheckboxLabel)
        divCheckbox.appendChild(iconDropdown)

        divTitle.appendChild(divCheckbox)

        container.appendChild(divTitle)
        container.appendChild(divContent)

        testTypeCheckbox.onclick = function () {
          document.getElementById(testTypeCheckbox.id + 'Content').classList.toggle('show-detail-active')
          document.getElementById(testTypeCheckbox.id + 'Icon').classList.toggle('overviewOpen')
          var countChecked = document.getElementById('checkboxContainer').querySelectorAll('input[type="checkbox"]:checked').length
          if (countChecked === 0) {
            document.getElementById(confirmCheckbox.id).setAttribute('disabled', 'disabled')
            document.getElementById(confirmCheckbox.id).checked = false
            document.getElementById('m-ce-workingpaper-addBtn').setAttribute('disabled', 'disabled')
          } else {
            document.getElementById(confirmCheckbox.id).removeAttribute('disabled')
          }
        }

        iconDropdown.onclick = function () {
          log('Click on dropdown ', iconDropdown.id)
          document.getElementById(iconDropdown.id.replace('Icon', 'Content')).classList.toggle('show-detail-active')
          document.getElementById(iconDropdown.id).classList.toggle('overviewOpen')
        }
      }
    }
    buildCheckBoxTestType(testTypeContainer1)

    confirmCheckbox.onclick = function () {
      confirmFlg = !confirmFlg
      if (confirmFlg == true) {
        document.getElementById('m-ce-workingpaper-addBtn').removeAttribute('disabled')
      }
      else {
        document.getElementById('m-ce-workingpaper-addBtn').setAttribute('disabled', 'disabled')
      }
    }

    divCheckboxTitle.appendChild(confirmCheckbox)
    divCheckboxTitle.appendChild(confirmCheckboxLabel)
    divCheckboxConfirmation.appendChild(divCheckboxTitle)

    inputContainer.appendChild(testTypeContainer1)
    inputContainer.appendChild(divCheckboxConfirmation)

    componentBody.appendChild(title)
    componentBody.appendChild(inputContainer)

    parentDOM.appendChild(componentBody)
  }

  initialize()
}

Component_Procedure.prototype = ComponentTemplate.prototype
Component_Procedure.prototype.constructor = ComponentTemplate

module.exports = Component_Procedure
