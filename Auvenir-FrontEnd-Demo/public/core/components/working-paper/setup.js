/**
 * Created by Bao Nguyen on 7/13/2017.
 */
'use strict'

import ComponentTemplate from '../ComponentTemplate'
import Utility from '../../../js/src/utility_c'
import Forge from '../../Forge'
import log from '../../../js/src/log'

import './setup.css'

var Component_SetUp = function (ctrl, container, owner) {
  ComponentTemplate.call(this, ctrl, container, owner)

  var parentDOM = container
  var c = ctrl
  var self = this
  var text = c.lang.workingPaperSetupComponent

  /*
   * Local instances of the field values
   */
  var fields = {
    name: {id: 'workingpaper-name', type: 'text', value: '', model: {name: 'workingpaper', field: 'name'}},
    type: {id: 'workingpaper-type', type: 'text', value: '', model: {name: 'workingpaper', field: 'type'}}
  }

  var typeList = []

  const initialize = function () {
    log('Component_SetUp: Initializing...')
    registerEvents()
  }

  const registerEvents = function () {
    c.registerForEvent('load-workingpaper-type', 'LOAD-WORKING-PAPER-TYPE-000', responseLoadData)
  }

  var responseLoadData = function (tag, data) {
    bindingData(data).then(buildContent())
  }

  var bindingData = function (data) {
    return new Promise((resolve, reject) => {
      var dataResult = JSON.parse(data.result)

      for (let item in dataResult) {
        typeList.push({text: dataResult[item]})
      }

      resolve()
    })
  }

  var buildContent = function () {
    if (!parentDOM) {
      return
    }

    var componentBody = Forge.build({'dom': 'p', 'id': 'workingpaper-setup-component-body', 'class': 'workingpaper-modal-body'})

    var inputContainer = Forge.build({'dom': 'div', id: 'workingpaper-setup-inputContainer', 'class': 'm-ce-setup-inputContainer'})
    var nameInput = Forge.buildInput({'label': text.nameLabel, 'errorText': text.nameError, 'id': fields.name.id, 'placeholder': 'Type name here', width: '520px'})
    var typeInput = Forge.buildInputDropdown({'label': text.typeLabel, 'list': typeList, 'id': fields.type.id, 'placeholder': 'Select from list', errorText: text.typeLabelError})

    var timeName = null
    nameInput.children[1].onblur = nameInput.children[1].onkeydown = function () {
      clearTimeout(timeName)
      var context = this
      timeName = setTimeout(function () {
        fields.name.value = nameInput.children[1].value
        if (fields.name.value === '' || !Utility.REGEX.alphanumericName.test(fields.name.value)) {
          context.classList.add('auv-error')
        } else {
          context.classList.remove('auv-error')
        }
      }, 100)
    }

    var timeInput = null
    typeInput.children[2].onblur = typeInput.children[2].onkeydown = function () {
      clearTimeout(timeInput)
      var context = this
      timeInput = setTimeout(function () {
        fields.type.value = typeInput.children[2].value
        if (fields.type.value === '' || fields.type.value !== 'Accounts Receivable') {
          context.classList.add('auv-error')
          owner.setTotalStep(2)
        } else {
          context.classList.remove('auv-error')
          nameInput.children[1].value = typeInput.children[2].value + ' - working paper'
          owner.setTotalStep(5)
        }
      }, 100)
    }

    inputContainer.appendChild(typeInput)
    inputContainer.appendChild(nameInput)

    componentBody.appendChild(inputContainer)

    parentDOM.appendChild(componentBody)
  }

  initialize()
}

Component_SetUp.prototype = ComponentTemplate.prototype
Component_SetUp.prototype.constructor = ComponentTemplate

module.exports = Component_SetUp
