/**
 * Created by Bao Nguyen on 7/13/2017.
 */
/**
 * Created by Bao Nguyen on 7/13/2017.
 */
'use strict'

import ComponentTemplate from '../ComponentTemplate'
import Utility from '../../../js/src/utility_c'
import Forge from '../../Forge'
import log from '../../../js/src/log'

import './samples.css'

var async = require('async')

var Component_Samples = function (ctrl, container, owner) {
  ComponentTemplate.call(this, ctrl, container, owner)

  var parentDOM = container
  var c = ctrl
  var self = this
  var text = c.lang.workingPaperSamplesComponent

  var samplesList = []
  var totalRecords = 0
  const initialize = function () {
    log('Woringpaper_Component_Samples: Initializing...')
    buildContent()
    registerEvents()
  }

  const registerEvents = function () {
    log('Samples List: Registering Event Listeners ...')
    c.registerForEvent('get-samples', 'GET-SAMPLES-LIST-000', function (tag, data) {
      var dataResult = JSON.parse(data.result)
      log(dataResult)
      const samplesTable = document.getElementById('samples-table')
      const tbody = samplesTable.querySelector('tbody')
      tbody.innerHTML = ''
      $(tbody).scroll(function () {
        if (tbody.scrollHeight - $(tbody).scrollTop() == $(tbody).outerHeight()) {
          getSamples()
        }
      })
      $('#btn-loaddata').click(function () {
        getSamples()
      })
      dataResult.SampleInfo.TransactionList.forEach((item) => {
        samplesList.push(item)
      })
      totalRecords = dataResult.SampleInfo.TotalRecords
      showSamples(dataResult.SampleInfo.TransactionList)
    })
  }

  var formatDate = function (date, prettyDate) {
    if (prettyDate === null || prettyDate === undefined) {
      return (date === null ? '' : Utility.getFormattedDate(date, '/'))
    }

    return moment(date).format('dddd, MMM Do')
  }

  var getSamples = function () {
    var skip = samplesList.length
    var take = 75
    var url = 'http://192.168.1.162:8080/api/samples?skip=' + skip + '&take=' + take
    if (samplesList.length < totalRecords) {
      $.ajax({
        type: 'GET', // rest Type
        dataType: 'json', // mispelled
        url: url,
        async: false,
        contentType: 'application/json; charset=utf-8',
        success: function (dataResult) {
          dataResult.SampleInfo.TransactionList.forEach((item) => {
            samplesList.push(item)
          })
          showSamples(dataResult.SampleInfo.TransactionList)
          totalRecords = dataResult.SampleInfo.TotalRecords
        }
      })
    }
  }

  var showSamples = function (items) {
    const samplesTable = document.getElementById('samples-table')
    const tbody = samplesTable.querySelector('tbody')
    items.forEach((item) => {
      const newRow = Forge.build({ dom: 'tr', class: 'newRow' })
      tbody.appendChild(newRow)

          /* Row Selection Checkbox */
      const selectSamples = Forge.build({ dom: 'td', style: 'width: 4%; background: /*firebrick*/;' })
      newRow.appendChild(selectSamples)
      var renderSelectBox = function () {
        const samplesCheckbox = Forge.build({ dom: 'input', type: 'checkbox', style: 'margin-left: 16px;' })
        selectSamples.appendChild(samplesCheckbox)

        samplesCheckbox.onclick = function () {
          var countSamplesChecked = document.getElementById('samples-table').querySelectorAll('tbody input[type="checkbox"]:checked').length
          var countSamplesCheckbox = document.getElementById('samples-table').querySelectorAll('tbody input[type="checkbox"]').length

          var checkboxAll = document.getElementById('cb-select-all-samples')
          log('Click on checkbox ', countSamplesChecked, countSamplesCheckbox)
          if (countSamplesChecked === countSamplesCheckbox) {
            checkboxAll.checked = true
          } else {
            checkboxAll.checked = false
          }
        }
      }
      renderSelectBox()

      const itemTransactionDate = Forge.build({ dom: 'td', style: 'width: 30%; background: /*red*/;', text: formatDate(item.TransactionDate) })
      const itemAccountTypeName = Forge.build({ dom: 'td', style: 'width: 30%; background: /*red*/;', text: item.AccountTypeName })
      const itemDescription = Forge.build({ dom: 'td', style: 'width: 30%; background: /*red*/;', text: item.Description })
      const itemCustomerName = Forge.build({ dom: 'td', style: 'width: 30%; background: /*red*/;', text: item.CustomerName })
      const itemAmount = Forge.build({ dom: 'td', style: 'width: 30%; background: /*red*/;', text: item.Amount })

      newRow.appendChild(itemTransactionDate)
      newRow.appendChild(itemAccountTypeName)
      newRow.appendChild(itemDescription)
      newRow.appendChild(itemCustomerName)
      newRow.appendChild(itemAmount)
    })
  }

  var buildContent = function (settings) {
    if (!parentDOM) {
      return
    }

    var componentBody = Forge.build({'dom': 'p', 'id': 'samples-component-body', 'class': 'workingpaper-modal-body'})

    var inputContainer = Forge.build({'dom': 'div', 'class': 'm-ce-samples-inputContainer'})

    // create samples information area
    // ---------------------------------------------------------------------------------------------------
    var samplesInformation = Forge.build({'dom': 'div', 'class': 'samples-information'})
    var infoColLeft = Forge.build({dom: 'div', style: 'display: inline-block; width: 50%'})
    var infoColRight = Forge.build({dom: 'div', style: 'display: inline-block; width: 50%;'})
    var info1 = Forge.build({dom: 'p', text: 'Select 0 out of 76 samples.'})
    var info2 = Forge.build({dom: 'p', text: 'Value of $0.00 CDN from selected samples.'})
    var info3 = Forge.build({dom: 'p', text: text.performanceMaterialityLabel})
    var info4 = Forge.build({dom: 'p', text: '$25,567.65 CDN', class: 'performance-materiality'})

    infoColLeft.appendChild(info1)
    infoColLeft.appendChild(info2)
    infoColRight.appendChild(info3)
    infoColRight.appendChild(info4)
    samplesInformation.appendChild(infoColLeft)
    samplesInformation.appendChild(infoColRight)

    // create data table area
    // ---------------------------------------------------------------------------------------------------
    const samplesTable = Forge.build({dom: 'table', 'id': 'samples-table', 'class': 'samples-table', style: 'width: 100%; display: block'})

    const thead = Forge.build({dom: 'thead', style: 'border-bottom: 1px solid #ddd;'})
    samplesTable.appendChild(thead)

    const tbody = Forge.build({ dom: 'tbody', style: 'display: inline-block; max-height: 200px; overflow-y: auto'})
    samplesTable.appendChild(tbody)

    const samplesTitleRow = Forge.build({dom: 'tr'})
    thead.appendChild(samplesTitleRow)

    const samplesSelectAllBox = Forge.build({dom: 'th', style: 'width: 4%;'})
    const samplesSelectAllCheck = Forge.build({dom: 'input', 'id': 'cb-select-all-samples', type: 'checkbox', style: 'margin-left: 16px;'})
    samplesTitleRow.appendChild(samplesSelectAllBox)
    samplesSelectAllBox.appendChild(samplesSelectAllCheck)

    const samplesDate = Forge.build({dom: 'th', text: text.thDateLabel, class: 'table-header', style: 'width: 28%;'})
    samplesDate.setAttribute('data-id', 'name')
    samplesDate.setAttribute('data-sort', '1')
    var dateSort = Forge.build({'dom': 'i', 'class': 'icon auvicon-sort-down samples-fieldSort-icon'})
    samplesDate.appendChild(dateSort)

    const samplesAccount = Forge.build({dom: 'th', text: text.thAccountLabel, class: 'table-header', style: 'width: 28%;'})
    samplesAccount.setAttribute('data-id', 'name')
    samplesAccount.setAttribute('data-sort', '1')
    var accountSort = Forge.build({'dom': 'i', 'class': 'icon auvicon-sort-down samples-fieldSort-icon'})
    samplesAccount.appendChild(accountSort)

    const samplesDescription = Forge.build({dom: 'th', text: text.thDescriptionLabel, class: 'table-header', style: 'width: 28%;'})
    samplesDescription.setAttribute('data-id', 'name')
    samplesDescription.setAttribute('data-sort', '1')
    var descriptionSort = Forge.build({'dom': 'i', 'class': 'icon auvicon-sort-down samples-fieldSort-icon'})
    samplesDescription.appendChild(descriptionSort)

    const samplesCustomer = Forge.build({dom: 'th', text: text.thCustomerLabel, class: 'table-header', style: 'width: 28%;'})
    samplesCustomer.setAttribute('data-id', 'name')
    samplesCustomer.setAttribute('data-sort', '1')
    var customerSort = Forge.build({'dom': 'i', 'class': 'icon auvicon-sort-down samples-fieldSort-icon'})
    samplesCustomer.appendChild(customerSort)

    const samplesAmount = Forge.build({dom: 'th', text: text.thAmountLabel, class: 'table-header', style: 'width: 28%;'})
    samplesAmount.setAttribute('data-id', 'name')
    samplesAmount.setAttribute('data-sort', '1')
    var amountSort = Forge.build({'dom': 'i', 'class': 'icon auvicon-sort-down samples-fieldSort-icon'})
    samplesAmount.appendChild(amountSort)

    samplesTitleRow.appendChild(samplesDate)
    samplesTitleRow.appendChild(samplesAccount)
    samplesTitleRow.appendChild(samplesDescription)
    samplesTitleRow.appendChild(samplesCustomer)
    samplesTitleRow.appendChild(samplesAmount)

    inputContainer.appendChild(samplesInformation)
    inputContainer.appendChild(samplesTable)

    var divLoadMore = Forge.build({ 'dom': 'div', 'style': 'text-align: center'})
    var btnloadata = Forge.build({ dom: 'input', 'id': 'btn-loaddata', type: 'button', value: 'Load more', style: 'width: 160px; margin-top: 20px' })
    divLoadMore.appendChild(btnloadata)
    inputContainer.appendChild(divLoadMore)
    componentBody.appendChild(inputContainer)

    parentDOM.appendChild(componentBody)
  }

  initialize()
}

Component_Samples.prototype = ComponentTemplate.prototype
Component_Samples.prototype.constructor = ComponentTemplate

module.exports = Component_Samples
