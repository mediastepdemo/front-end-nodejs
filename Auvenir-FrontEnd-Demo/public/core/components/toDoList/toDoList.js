﻿'use strict'

import ComponentTemplate from '../ComponentTemplate'
import log from '../../../js/src/log'
import Forge from '../../Forge'
import moment from 'moment'
import Utility from '../../../js/src/utility_c'
import _ from 'lodash'
import FlashAlert from '../../widgets/flash-alert/flash-alert'
import Modal_Custom from '../../modals/custom/Modal_Custom'
import COMMON from '../../../js/src/common'
import definePermission from '../../../js/src/definePermission'
import './toDoList.css'
var async = require('async')

const Component_ToDoList = function (ctrl, container) {
    ComponentTemplate.call(this, ctrl, container)

    this.name = 'ToDo Component'
    this.parent = container
    const self = this
    const c = ctrl
    const flashAlert = new FlashAlert()
    const settings = c.getSettings()
    let scrollPos = null
    var downloadZipModal = null // for download zip bulk-action
    var lastAction
    var fields = {
        name: {
            id: 'todo-name',
            type: 'text',
            value: '',
            model: { name: 'todos', field: 'name' },
            validation: { min: 1, max: 255, regex: /[~!@#$%^&*+?><,.]/ }
        }
    }

    // for view management
    var filter_value = null
    var previousSort = { headerDOM: null, type: null, ascending: null }
    var currentFilter = { // TODO - this will be used in the future for advanced filter
        btnDOM: null,
        type: ''
    }

    // for import from gdrive & auvicloud
    var cloudSourceModal = null
    var currentTodo
    var currentRequest

    const initialize = function () {
        log('Component_TodoList: Initializing...')

        buildModal()
        registerEvents()
    }

    // Populates all input fields with the information sent in data about the CLIENT
    const loadData = this.loadData = function (data) {
        log('Loading ToDo Info')
        todoRows(checkSearchAndFilterData(data))
    }

    var checkSearchAndFilterData = function (data) {
        var result = searchData(data)
        if (filter_value) {
            result = filterData(filter_value.type, filter_value.value, result)
        }
        return result
    }

    this.clearSearchAndFilter = function () {
        filter_value = null
        document.getElementById('todo-search').value = ''
        var filter = document.getElementById('todo-filter-dropdown')
        if (filter) filter.querySelector('.text').innerText = 'Filter'
    }

    const registerEvents = function () {
        log('ToDo: Registering Event Listeners ...')
        c.registerForEvent('create-todo', 'TODOLIST-CREATETODO-000', eventResponse)
        c.registerForEvent('update-todo', 'TODOLIST-UPDATETODO-001', eventResponse)
        c.registerForEvent('create-category', 'TODOLIST-CREATECATEGORY-002', eventResponse)
        c.registerForEvent('update-category', 'TODOLIST-UPDATECATEGORY-003', eventResponse)
        c.registerForEvent('update-bulk-category', 'TODOLIST-UPDATEBULKCATEGORY-004', eventResponse)
        c.registerForEvent('delete-category', 'TODOLIST-UPDATEBULKCATEGORY-005', eventResponse)
        c.registerForEvent('update-engagement', 'TODOLIST-UPDATE-ENGAGEMENT-006', eventResponse)
        c.registerForEvent('get-user-list', 'TODOLIST-GETCOMMENTUSERS-005', userEventResponse)
        c.registerForEvent('bulk-todos-update', 'TODOLIST-BULK-UPDATE-006', eventResponse)
        c.registerForEvent('undo-bulk-action', 'TODOLIST-UNDO-BULK-ACTION-007', eventResponse)
        c.registerForEvent('add-existing-file', 'TODOLIST-ADD-EXISTING-FILE-008', handle_addExistingFileResponse)
        c.registerForEvent('load-engagement', 'LOAD-ENGAGEMNT-RENDER-TODO-000', function (tag, data) {
            self.clearSearchAndFilter()
            self.loadData(c.currentTodos)
        })
    }

    var eventResponse = function (tag, data) {
        log('> Event: event-response')
        loadData(c.currentTodos)

        scrollToCurrent()

        if (tag === 'create-todo') {
            if (c.currentTodos.length > 0 && c.currentTodos[0]._id) {
                let input = document.querySelector('[data-id="' + c.currentTodos[0]._id + '"] .newTodoInput')
                input.focus()
                input.select()
            }
        }

        if (data.code === 0 && tag === 'update-todo') {
            // reload current selected todo
            let todo = _.find(data.result.todos, { _id: data.payload.todoId })
            loadTodo(todo)
            lastAction = data.payload.lastAction
            var selectedRequest
            // focus and preselect Request name input after added an empty request
            switch (data.payload.lastAction) {
                case 'add-request':
                    selectedRequest = document.querySelector('#auv-todo-detailsReqBox .detReqForFile:last-of-type input')
                    selectedRequest.focus()
                    selectedRequest.select()
                    break
                case 'post-comment':
                    document.getElementById('comment-input').value = ''
                    loadData(c.currentTodos)
                    break
                default:
            }
        }
    }

    var userEventResponse = (tag, data) => {
        log('> Event: user-event-response')
        log(data)
        let users = data.result

        users = users.map((user) => {
            let userName = (user.firstName + ' ' + user.lastName).trim()
            user.name = userName !== '' ? userName : user.email
            return user
        })

        users.forEach((user, index) => {
            let commenterNameNodes = document.querySelectorAll('.comment-user-' + user._id)
            commenterNameNodes.forEach((node) => {
                node.innerText = user.name
            })
        })

        if (lastAction === 'post-comment') {
            scrollToBottom(document.getElementById('todo-details-content'))
            lastAction = null
        }
    }

    /**
     * Handler for Add-existing-file event response from socket
     * @param {String} tagName
     * @param {JSON} data { code, msg, engagement, requestID }
     */
    var handle_addExistingFileResponse = function (tagName, data) {
        let { code, msg, engagement, requestID, fileID } = data
        let requestObj
        if (!code) { // code: 0
            lastAction = data.payload.lastAction
            if (c.currentEngagement._id.toString() === engagement._id.toString()) {
                c.currentEngagement = engagement
                c.currentFiles = engagement.files
            }
            c.myEngagements.forEach((oneEngagement) => {
                if (oneEngagement._id.toString() === engagement._id.toString()) {
                    oneEngagement = engagement
                }
            })
            engagement.todos.forEach((todo) => {
                if (todo.requests.length > 0) {
                    todo.requests.forEach((request) => {
                        if (request._id.toString() === requestID.toString()) {
                            requestObj = request
                            // TODO - this should be gone after db model changes (when each request will have engagementID and todoID in it)
                            requestObj.todoID = todo._id
                            loadTodo(todo)
                        }
                    })
                }
            })

            // create activity upon success
            let activity = {
                engagementID: engagement._id,
                operation: 'UPDATE',
                type: 'Request-file',
                original: data.payload.originalRequest,
                updated: requestObj
            }
            c.createActivity(activity)
        } else {
            self.trigger('flash', { content: data.msg, type: 'ERROR' })
        }
    }

    const setScrollPos = () => {
        scrollPos = (window.pageYOffset || document.scrollTop) - (document.clientTop || 0)
    }
    const scrollToCurrent = () => {
        window.scrollTo(0, scrollPos)
    }

    var getCategory = function () {
        return c.currentEngagement.categories
    }

    /**
     * sort list todo from json data
     */
    var getSortOrder = function (prop, asc) {
        return function (a, b) {
            var value_a = ''
            var value_b = ''
            switch (prop) {
                case 'category':
                    value_a = (a[prop] ? a[prop].name : '')
                    value_b = (b[prop] ? b[prop].name : '')
                    break
                case 'clientAssignee':
                    value_a = (a[prop] ? a[prop].firstName + ' ' + a[prop].lastName : '')
                    value_b = (b[prop] ? b[prop].firstName + ' ' + b[prop].lastName : '')
                    break
                case 'auditorAssignee':
                    value_a = (a[prop] ? a[prop].firstName + ' ' + a[prop].lastName : '')
                    value_b = (b[prop] ? b[prop].firstName + ' ' + b[prop].lastName : '')
                    break
                default:
                    value_a = a[prop]
                    value_b = b[prop]
                    break
            }
            if (asc === '1') {
                return (value_a > value_b) ? 1 : ((value_a < value_b) ? -1 : 0)
            } else {
                return (value_b > value_a) ? 1 : ((value_b < value_a) ? -1 : 0)
            }
        }
    }

    /**
     * search list todo from json data
     */
    var searchData = function (data) {
        var result = []
        var key = document.getElementById('todo-search').value
        if (key === '') {
            return data
        }
        var length = data.length
        for (var i = 0; i < length; i++) {
            var regex = new RegExp(key, 'i')
            var clientAssignedName = (data[i].clientAssignee ? data[i].clientAssignee.firstName + ' ' + data[i].clientAssignee.lastName : 'unassigned')
            var auditorAssigneeName = (data[i].auditorAssignee ? data[i].auditorAssignee.firstName + ' ' + data[i].auditorAssignee.lastName : 'unassigned')
            var permistionList = c.permission.getPermissionInTodo(c.currentTodos)
            // suggest
            if (permistionList.includes(definePermission.TodoPermissions.GENERAL_W)) {
                let userLead = c.getUsersByRole('CLIENT', false, true)
                clientAssignedName = (data[i].clientAssignee === null ? 'unassigned' : userLead[0].userInfo.firstName + ' ' + userLead[0].userInfo.lastName)
            }
            // if (c.currentUser.type.toUpperCase() === 'AUDITOR') {
            //   let userLead = c.getUsersByRole('CLIENT', false, true)
            //   var clientAssignedName = (data[i].clientAssignee === null ? 'unassigned' : userLead[0].userInfo.firstName + ' ' + userLead[0].userInfo.lastName)
            // }
            var categoryName = (data[i].category ? data[i].category.name : '')
            if (regex.test(data[i].name) ||
                regex.test(categoryName) ||
                regex.test(clientAssignedName) ||
                regex.test(auditorAssigneeName)) {
                // TODO - regex.test(dueDate) is required.
                result.push(data[i])
            }
        }
        return result
    }

    /**
     * Get a list of Todos list that marked to criteria
     * @name filterData
     * @param {String, Number}   type (filter type)
     * @param {String, Boolean}   value (filter value)
     * @param {Object}   data (list of todos)
     * @returns {Array} list of Todos data
     */
    const filterData = function (type, value, data) {
        var result = []
        let length = data.length
        switch (type) {
            case COMMON.FILTER_ENUM.COMPLETE:
                for (let i = 0; i < length; i++) {
                    if (data[i].completed) {
                        result.push(data[i])
                    }
                }
                break
            case COMMON.FILTER_ENUM.ASSIGNEE:
                for (let i = 0; i < length; i++) {
                    var auditorAssignee = (data[i].auditorAssignee ? data[i].auditorAssignee._id : '')
                    var clientAssignee = (data[i].clientAssignee ? data[i].clientAssignee._id : '')
                    if (auditorAssignee === value || clientAssignee === value) {
                        result.push(data[i])
                    }
                }
                break
            case COMMON.FILTER_ENUM.COMMENT:
                for (let i = 0; i < length; i++) {
                    if (data[i].comments.length > 0) {
                        result.push(data[i])
                    }
                }
                break
            case COMMON.FILTER_ENUM.REQUEST:
                for (let i = 0; i < length; i++) {
                    if (data[i].requests.length > 0) {
                        result.push(data[i])
                    }
                }
                break
            case COMMON.FILTER_ENUM.DUEDATE:
                for (let i = 0; i < length; i++) {
                    if (data[i].dueDate) {
                        result.push(data[i])
                    }
                }
                break
            case COMMON.FILTER_ENUM.OUTSTANDING:
                for (let i = 0; i < length; i++) {
                    let isOutstanding = isOutstandingTodo(data[i])
                    if (isOutstanding) {
                        result.push(data[i])
                    }
                }
                break
            default:
                result = data
                break
        }
        return result
    }

    var isOutstandingTodo = function (todo) {
        if (todo === null || todo === undefined) return false
        if (todo.completed) return false
        if (todo.dueDate !== null && (new Date(todo.dueDate)) <= (new Date())) return false
        return true
    }

    /**
     * validate function
     */
    var isFieldValid = function (updateDOM) {
        // We use updateDOM to get the updated value (data has old values).
        var data = fields.name
        var valid = true
        // Sets valid if field is valid
        if (data.validation) {
            if (data.validation.required) {
                valid = valid && updateDOM.value.length > 0
            }

            if (data.validation.min) {
                valid = valid && updateDOM.value.length >= data.validation.min
            }

            if (data.validation.max) {
                valid = valid && updateDOM.value.length <= data.validation.max
            }

            if (data.validation.regex) {
                var regex = new RegExp(data.validation.regex)
                valid = valid && !regex.test(updateDOM.value)
            }
        } else {
            valid = true
        }

        if (valid) {
            updateDOM.classList.remove('auv-error')
        } else {
            updateDOM.className += ' auv-error'
        }
        return valid
    }

    /**
     * validate dueDate of Todo should be less than or equal to dueDate of Engagement
     */
    const checkDueDate = function ({ todoId, dueDate }) {
        let engagementDueDate = new Date(c.currentEngagement.dueDate)
        var now = new Date(formatDate(new Date()))
        return (dueDate <= engagementDueDate && dueDate >= now)
    }

    const displaySourceModal = function (options) {
        applyModalOption(options)
        cloudSourceModal.open()
    }

    const applyModalOption = function ({ gdrive, auvCloud, local }) {
        let cloudBtnGroup = document.getElementById('todo-source-btnGroup')
        cloudBtnGroup.innerHTML = ''

        if (gdrive) {
            let googleDriveBox = Forge.build({ 'dom': 'a', 'id': 'todo-cloud-googleDrive', 'class': 'todo-cloud-container', style: 'vertical-align: middle;' })
            let googleLogo = Forge.build({ 'dom': 'img', 'id': 'todo-cloud-googleDrive-img', 'src': 'core/components/files/img/google-drive-logo-black.svg', 'class': 'todo-cloud-icon' })
            let googleText = Forge.build({ 'dom': 'div', 'text': 'Google Drive', 'class': 'files-storage-text' })
            googleDriveBox.appendChild(googleLogo)
            googleDriveBox.appendChild(googleText)
            googleDriveBox.addEventListener('mouseover', () => {
                googleLogo.style.borderRightColor = '#599ba1'
                googleLogo.src = 'core/components/files/img/google-drive-logo.svg'
            })
            googleDriveBox.addEventListener('mouseleave', () => {
                googleLogo.style.borderRightColor = '#e1e1e1'
                googleLogo.src = 'core/components/files/img/google-drive-logo-black.svg'
            })
            googleDriveBox.addEventListener('click', (e) => {
                cloudSourceModal.close()
                // open google drive single file upload picker
                c.gdriveManager.openPicker({ option: 'FILE', engagementID: c.currentEngagement._id, todoID: currentTodo._id, requestID: currentRequest._id, oldRequest: currentRequest })
                c.gdriveManager.on('file-upload-result', (result) => {
                    let { file, requestID, todoID, engagement, msg, type, oldRequest } = result
                    let todo = engagement.todos.find((todo) => { return todo._id.toString() === todoID.toString() })
                    let request = todo.requests.find((request) => { return request._id.toString() === requestID.toString() })
                    if (type === 'success') {
                        // TODO - this should be gone after db model changes (when each request will have engagementID and todoID in it)
                        // oldRequest.todoId = todoID
                        request.todoID = todoID
                        let activity = {
                            engagementID: engagement._id,
                            operation: 'UPDATE',
                            type: 'Request-file',
                            original: oldRequest,
                            updated: request
                        }
                        c.createActivity(activity)

                        if (engagement) {
                            if (c.currentEngagement._id.toString() === engagement._id.toString()) {
                                c.currentEngagement = engagement
                                c.currentFiles = engagement.files
                            }
                            c.myEngagements.forEach((oneEngagement) => {
                                if (oneEngagement._id.toString() === engagement._id.toString()) {
                                    oneEngagement = engagement
                                }
                            })
                        }
                        engagement.todos.forEach((todo) => {
                            if (todo.requests.length > 0) {
                                todo.requests.forEach((request) => {
                                    if (request._id.toString() === requestID.toString()) {
                                        loadTodo(todo)
                                    }
                                })
                            }
                        })
                    } else {
                        self.trigger('flash', { content: `${msg}`, type: 'ERROR' })
                    }
                })
            })
            cloudBtnGroup.appendChild(googleDriveBox)
        }
        if (auvCloud) {
            let auvDriveBox = Forge.build({ 'dom': 'a', 'class': 'todo-cloud-container', 'id': 'todo-cloud-auvenirDrive', style: 'vertical-align: middle;' })
            let auvDriveIcon = Forge.build({ 'dom': 'img', 'id': 'todo-cloud-googleDrive-img', 'src': 'images/icon.png', 'class': 'todo-cloud-icon', 'style': 'width: 52px; left: 0; float: left;' })
            let auvDriveText = Forge.build({ 'dom': 'div', 'text': 'Auvenir', 'class': 'files-storage-text', 'style': 'margin-top: 16px; padding-left: 8px;' })
            auvDriveBox.appendChild(auvDriveIcon)
            auvDriveBox.appendChild(auvDriveText)
            auvDriveBox.addEventListener('mouseover', () => {
                auvDriveIcon.style.borderRightColor = '#599ba1'
                // auvDriveIcon.src = ''
            })
            auvDriveBox.addEventListener('mouseleave', () => {
                auvDriveIcon.style.borderRightColor = '#e1e1e1'
                // auvDriveIcon.src = ''
            })
            auvDriveBox.addEventListener('click', () => {
                cloudSourceModal.close()
                c.displayFileManagerModal(currentRequest, currentTodo)
            })
            cloudBtnGroup.appendChild(auvDriveBox)
        }
        if (local) {
            let localDriveBox = Forge.build({ 'dom': 'a', 'class': 'todo-cloud-container', 'id': 'todo-cloud-auvenirDrive', style: 'vertical-align: middle;' })
            let localDriveIcon = Forge.build({ 'dom': 'div', 'id': 'todo-cloud-auvenirDrive-img', 'class': 'auvicon-line-folder todo-cloud-icon', 'style': 'float: left; width: 54px;padding-left: 6px; padding-top: 9px; color: #363a3c;' })
            let localDriveText = Forge.build({ 'dom': 'div', 'text': 'Local', 'class': 'files-storage-text', 'style': 'margin-top: 16px; padding-left: 8px;' })
            let uploadButton = document.createElement('input')
            uploadButton.type = 'file'
            uploadButton.style.display = 'none'
            uploadButton.id = 'todo-request-upload-' + currentRequest._id
            uploadButton.dataset.todo = currentTodo._id
            uploadButton.accept = 'image/*,application/pdf,text/plain,text/csv,application/msword,application/vnd.ms-excel,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.wordprocessingml.document'
            localDriveBox.appendChild(uploadButton)

            localDriveBox.appendChild(localDriveIcon)
            localDriveBox.appendChild(localDriveText)
            localDriveBox.addEventListener('mouseover', () => {
                localDriveIcon.style.borderRightColor = '#599ba1'
                localDriveIcon.classList = 'auvicon-folder todo-cloud-icon'
                localDriveIcon.style.color = '#599ba1'
            })
            localDriveBox.addEventListener('mouseleave', () => {
                localDriveIcon.style.borderRightColor = '#e1e1e1'
                localDriveIcon.classList = 'auvicon-line-folder todo-cloud-icon'
                localDriveIcon.style.color = '#363a3c'
            })
            localDriveBox.addEventListener('click', (e) => {
                cloudSourceModal.close()
                uploadButton.click()
            })
            uploadButton.addEventListener('change', (e) => {
                fileUpload_Selection(e.target.files, currentTodo._id, currentRequest._id, currentRequest._id)
            })
            cloudBtnGroup.appendChild(localDriveBox)
        }
    }

    const buildModal = function () {
        var cloudSourceContainer = Forge.build({ 'dom': 'div', 'id': 'todo-cloud-modal-container' })
        var cloudSourceSettings = {
            modalName: 'todo-cloud-modal',
            close: { 'window': true, 'global': false },
            header: { 'show': true, 'color': '#363a3c' },
            position: { 'bottom': '0px' },
            title: 'Choose A File Source'
        }
        cloudSourceModal = new Modal_Custom(c, cloudSourceSettings)

        document.getElementById(cloudSourceModal.contentID).innerHTML = ''
        document.getElementById(cloudSourceModal.contentID).appendChild(cloudSourceContainer)

        // TODO - change classes & ids
        // cloud button groups can be an widget in the future to be used in files & toDoList.
        let messageDiv = Forge.build({ 'dom': 'span', 'class': 'files-deleteStorage-msg', 'text': 'Please choose a cloud storage you would like to import a file from.', style: 'margin:32px;' })
        let cloudBtnGroup = Forge.build({ 'dom': 'div', 'id': 'todo-source-btnGroup', 'style': 'display: block; margin: 32px auto 48px;' })

        cloudSourceContainer.appendChild(messageDiv)
        cloudSourceContainer.appendChild(cloudBtnGroup)

        var downloadZipContainer = Forge.build({ 'dom': 'div', 'id': 'download-zip-container' })
        var downloadZipSettings = {
            modalName: 'download-zip',
            close: { 'window': true, 'global': false },
            width: '484px',
            header: { 'show': true, 'color': '#363a3c' },
            position: { 'bottom': '0px' },
            title: 'Ready To Download'
        }
        downloadZipModal = new Modal_Custom(c, downloadZipSettings)

        document.getElementById(downloadZipModal.contentID).innerHTML = ''
        document.getElementById(downloadZipModal.contentID).appendChild(downloadZipContainer)
    }

    /**
     * This function is responsible for building the page only when called.
     */
    this.buildContent = function () {
        const parentSection = Forge.build({ 'dom': 'section', 'id': 'todo-container', 'class': 'todo-container', style: 'min-height: 600px; background: #fff; box-shadow: 0 2px 4px 0 rgba(0,0,0,0.1); padding: 0px; border-radius: 4px; color: #757575;' })

        var toolbar = Forge.build({ dom: 'div', style: 'clear: both; width: 100%; margin: 0px 0px 16px;', id: 'todo-toolbar' })

        var filterContainer = Forge.build({ 'dom': 'div', 'id': 'filter-container', 'class': 'filter-container', 'style': 'margin: 10px 0px 0px 10px;' })
        toolbar.appendChild(filterContainer)

        var bulkContainer = Forge.build({ 'dom': 'div', 'id': 'bulk-container', 'class': 'bulk-container' })
        // TODO - code cleanup
        // if (c.currentUser.type === 'AUDITOR') {
        toolbar.appendChild(bulkContainer)
        // }

        var actIconContainer = Forge.build({ 'dom': 'div', 'class': 'act-icon-container' })
        toolbar.appendChild(actIconContainer)

        var btnUndo = Forge.build({ 'dom': 'i', 'id': 'btn-todo-undo', 'class': 'fa fa-undo disabled' })
        btnUndo.setAttribute('title', 'Undo')
        // TODO - hiding this for now.
        // actIconContainer.appendChild(btnUndo)

        const searchInput = Forge.build({ 'dom': 'input', 'type': 'text', 'id': 'todo-search', 'placeholder': 'Search...', 'class': 'auv-input', 'style': 'width:150px; height: 33px; margin:8px 10px 0px 0px; position: absolute;right:0' })
        searchInput.setAttribute('maxlength', 255)
        toolbar.appendChild(searchInput)

        parentSection.appendChild(toolbar)

        const todoTable = Forge.build({ dom: 'table', 'id': 'todo-table', 'class': 'todo-table', style: 'width: 100%;' })

        const thead = Forge.build({ dom: 'thead', style: 'border-bottom: 1px solid #ddd;' })
        todoTable.appendChild(thead)

        const tbody = Forge.build({ dom: 'tbody' })
        todoTable.appendChild(tbody)

        const todoTitleRow = Forge.build({ dom: 'tr' })
        thead.appendChild(todoTitleRow)

        const todoSelectAllBox = Forge.build({ dom: 'th', style: 'width: 4%;' })
        const todoSelectAllCheck = Forge.build({ dom: 'input', 'id': 'cb-select-all-todo', type: 'checkbox', style: 'margin-left: 16px;' })
        todoTitleRow.appendChild(todoSelectAllBox)
        todoSelectAllBox.appendChild(todoSelectAllCheck)

        const todoTitle = Forge.build({ dom: 'th', text: 'To-Dos', class: 'table-header', style: 'width: 28%;' })
        todoTitle.setAttribute('data-id', 'name')
        todoTitle.setAttribute('data-sort', '1')
        var titleSort = Forge.build({ 'dom': 'i', 'class': 'icon auvicon-sort-down todo-fieldSort-icon' })
        todoTitle.appendChild(titleSort)

        const category = Forge.build({ dom: 'th', text: 'Category', class: 'table-header', style: 'width: 10%;' })
        category.setAttribute('data-id', 'category')
        category.setAttribute('data-sort', '1')
        var categorySort = Forge.build({ 'dom': 'i', 'class': 'icon auvicon-sort-down todo-fieldSort-icon' })
        category.appendChild(categorySort)

        const clientAssigned = Forge.build({ dom: 'th', text: 'Client Assignee', class: 'table-header', style: 'width: 12%;' })
        clientAssigned.setAttribute('data-id', 'clientAssignee')
        clientAssigned.setAttribute('data-sort', '1')
        var clientSort = Forge.build({ 'dom': 'i', 'class': 'icon auvicon-sort-down todo-fieldSort-icon' })
        clientAssigned.appendChild(clientSort)

        const dueDate = Forge.build({ dom: 'th', text: 'Due Date', class: 'table-header', style: 'width: 10%;' })
        dueDate.setAttribute('data-id', 'dueDate')
        dueDate.setAttribute('data-sort', '1')
        var dueDateSort = Forge.build({ 'dom': 'i', 'class': 'icon auvicon-sort-down todo-fieldSort-icon' })
        dueDate.appendChild(dueDateSort)

        const auditorAssigned = Forge.build({ dom: 'th', text: 'Audit Assignee', class: 'table-header', style: 'width: 13%;' })
        auditorAssigned.setAttribute('data-id', 'auditorAssignee')
        auditorAssigned.setAttribute('data-sort', '1')
        var auditorSort = Forge.build({ 'dom': 'i', 'class': 'icon auvicon-sort-down todo-fieldSort-icon' })
        auditorAssigned.appendChild(auditorSort)

        const openDetails = Forge.build({ dom: 'th', style: 'width: 4%;' })
        const requestAlert = Forge.build({ dom: 'th', style: 'width: 4%;' })
        const fileNumber = Forge.build({ dom: 'th', style: 'width: 4%;' })
        const commentAlert = Forge.build({ dom: 'th', style: 'width: 4%;' })
        const completedCheck = Forge.build({ dom: 'th', style: 'width: 4%;' })

        todoTitleRow.appendChild(todoTitle)
        todoTitleRow.appendChild(category)
        todoTitleRow.appendChild(clientAssigned)
        todoTitleRow.appendChild(dueDate)
        todoTitleRow.appendChild(auditorAssigned)
        todoTitleRow.appendChild(openDetails)
        todoTitleRow.appendChild(requestAlert)
        todoTitleRow.appendChild(fileNumber)
        todoTitleRow.appendChild(commentAlert)
        todoTitleRow.appendChild(completedCheck)

        /* All elements for the details part of a to do */
        const todoDetails = Forge.build({ dom: 'div', id: 'auv-todo-details', style: 'display:none; width: 600px; height: 650px; padding: 16px; background: #fff; position: fixed; bottom: 48px; right: 0; z-index: 10; border-radius: 4px; box-shadow: 0 2px 4px 0 rgba(0,0,0,0.37);' })
        todoDetails.style.maxHeight = (window.innerHeight - 260) + 'px'
        window.addEventListener('resize', () => {
            todoDetails.style.maxHeight = (window.innerHeight - 260) + 'px'
        })

        const addRequestBtn = Forge.buildBtn({ text: 'Add New Request', type: 'primary', id: 'add-request-btn' }).obj
        addRequestBtn.addEventListener('click', addRequest)
        todoDetails.appendChild(addRequestBtn)

        const auditorsList = Forge.build({ dom: 'div', id: 'auv-todo-details-auditors', style: 'display: inline-block; font-size: 14px; font-weight: bold; line-height: 20px;' })
        todoDetails.appendChild(auditorsList)

        const dueDateText = Forge.build({ dom: 'div', text: 'Due: ', style: 'display: inline-block; color: #707070; font-size: 14px; font-weight: bold; line-height: 20px;' })
        const changeDueDate = Forge.build({ dom: 'div', text: ``, id: 'auv-todo-dueDate', class: 'auvicon-calendar', style: 'height: 32px;display: inline-block;padding: 0px 16px;margin: 0px 24px; vertical-align: middle;border: 1px solid #E1E1E1;border-radius: 100px;color: #707070;font-weight: bold;line-height: 27px;' })
        if (c.currentUser.type === 'CLIENT') {
            todoDetails.appendChild(dueDateText)
        }
        todoDetails.appendChild(changeDueDate)

        const dueDateDiv2 = Forge.build({ dom: 'div', id: 'auv-todo-dueDateDiv2', style: 'display: inline-block; color: #707070; font-size: 14px; font-weight: bold; line-height: 20px;' })
        const changeDueDate2 = Forge.build({ dom: 'div', text: ``, id: 'auv-todo-dueDate2', class: 'auvicon-calendar', style: 'height: 32px; width: 32px; display: inline-block;padding: 0px 16px;margin: 0px 2px; vertical-align: middle;border: 1px solid #E1E1E1;border-radius: 50%;color: #707070;font-weight: bold;line-height: 27px; padding-left:8px; padding-top: 4px;' })
        const dueDateText2 = Forge.build({ dom: 'div', id: 'auv-todo-dueDatetext2', style: 'display: inline-block; color: #707070; font-size: 14px; font-weight: bold; line-height: 20px;' })
        dueDateDiv2.appendChild(changeDueDate2)
        dueDateDiv2.appendChild(dueDateText2)
        todoDetails.appendChild(dueDateDiv2);

        const closeDetails = Forge.build({ dom: 'div', class: 'auvicon-ex', style: 'cursor: pointer; padding: 4px; position: absolute; top: 11px; right: 11px;' })
        closeDetails.addEventListener('click', () => {
            document.getElementById('auv-todo-details').style.display = 'none'
            currentRequest = null
            currentTodo = null
        })
        todoDetails.appendChild(closeDetails)

        const topHR = Forge.build({ dom: 'hr' })
        todoDetails.appendChild(topHR)

        const detailsTitle = Forge.build({ dom: 'input', type: 'text', id: 'todoDetailsName', placeholder: 'Please name your To-Do', class: 'detailsName' })
        todoDetails.appendChild(detailsTitle)

        const detailsCategory = Forge.build({ dom: 'div', text: 'Category', id: 'todoDetailsCategory', class: 'todoCategory', style: 'display: inline-block;' })
        todoDetails.appendChild(detailsCategory)

        const detailsDescription = Forge.build({ dom: 'textarea', rows: '', id: 'todoDetailsDescription', placeholder: 'Add a description here', class: 'detailsDescription' })
        todoDetails.appendChild(detailsDescription)

        
        //
        const detailsScrollArea = Forge.build({ dom: 'div', id: 'todo-details-content', style: 'max-height: 495px; overflow-y: auto; overflow-x: hidden; margin-right: -16px;' })
        detailsScrollArea.style.height = (window.innerHeight - 400) + 'px'
        window.addEventListener('resize', () => {
            detailsScrollArea.style.height = (window.innerHeight - 400) + 'px'
        })
        todoDetails.appendChild(detailsScrollArea)
        
        //
        //working paper
        const wpDescription = Forge.build({ dom: 'table', 'id': 'todo-table-wpDescription', 'class': 'todo-table', style: 'width: 100%;' })
        const wpDescriptionTbody = Forge.build({ dom: 'tbody' })
        wpDescription.appendChild(wpDescriptionTbody);
        detailsScrollArea.appendChild(wpDescription);

        //client
        const wpClient = Forge.build({ dom: 'table', 'id': 'todo-table-wpclient', 'class': 'todo-table', style: 'width: 100%;border-spacing: 0' })
        const wpClientTbody = Forge.build({ dom: 'tbody' })
        wpClient.appendChild(wpClientTbody);
        detailsScrollArea.appendChild(wpClient);
        //
        const detailsReqBox = Forge.build({ dom: 'div', id: 'auv-todo-detailsReqBox', style: ' margin-right: 16px;' })
        detailsScrollArea.appendChild(detailsReqBox)

        const customerList = Forge.build({ dom: 'div', id: 'auv-todo-details-customers', style: 'display: none; font-size: 14px; font-weight: bold; line-height: 20px;' })
        todoDetails.appendChild(customerList)

        const detailsCommentBox = Forge.build({ dom: 'div', 'id': 'comment-box' })
        detailsScrollArea.appendChild(detailsCommentBox)

        const detailsComments = Forge.build({ dom: 'p', text: 'Comments', style: 'color: #757575; font-size: 16px; font-weight: bold; margin: 16px 0px;' })
        detailsCommentBox.appendChild(detailsComments)

        const commentCountFlag = Forge.build({ dom: 'span', class: 'auvicon-messages commentFlag' })
        const commentCount = Forge.build({ dom: 'span', text: '0', class: 'details-comment-count commentNumber' })
        commentCountFlag.appendChild(commentCount)
        detailsComments.appendChild(commentCountFlag)

        const detailsNewComments = Forge.build({ dom: 'div', id: 'todoDetailsNewComments' })
        detailsCommentBox.appendChild(detailsNewComments)

        const detailsCommentList = Forge.build({ dom: 'div', id: 'todoDetailsCommentList' })
        detailsCommentBox.appendChild(detailsCommentList)

        const commentForm = Forge.build({ dom: 'div', class: 'todo-comment-form', id: 'comment-form' })
        const detailsAddComment = Forge.build({ dom: 'input', type: 'text', id: 'comment-input', class: 'comment-input', placeholder: 'Type a comment' })
        detailsAddComment.addEventListener('keypress', function (e) {
            var key = e.which || e.keyCode
            if (key === 13) { // 13 is enter
                event_updateComments(detailsAddComment)
            }
        })
        const attachmentBtn = Forge.build({ dom: 'div', class: 'auvicon-line-attach comment-attachment' })
        const uploadFileBtn = Forge.build({ dom: 'input', type: 'file', id: 'comment-file-button', style: 'display: none;', accept: 'image/*,application/pdf,text/plain,text/csv,application/msword,application/vnd.ms-excel,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.wordprocessingml.document' })
        uploadFileBtn.multiple = false

        const detailsPostComment = Forge.buildBtn({ text: 'Post', type: 'primary', id: 'comment-button', style: '' }).obj
        detailsPostComment.addEventListener('click', event_updateComments)

        commentForm.appendChild(detailsAddComment)
        commentForm.appendChild(attachmentBtn)
        commentForm.appendChild(detailsPostComment)
        commentForm.appendChild(uploadFileBtn)
        todoDetails.appendChild(commentForm)

        attachmentBtn.addEventListener('click', () => {
            uploadFileBtn.click()
        })

        container.appendChild(todoDetails)
        parentSection.appendChild(todoTable)

        const progressFunction = function (err, data) {
            if (err) {
                // not necessarily file-upload error.
                // self.trigger('flash', {content: 'There was an error while uploading your file.'})
            } else {
                if (document.getElementById('todo-comment-progress-' + data.uniqueID)) {
                    document.getElementById('todo-comment-progress-' + data.uniqueID).style.width = data.value + '%'
                }
            }
        }
        const completeFunction = function (err, data) {
            if (err) {
                let { uniqueID } = data
                document.getElementById('todo-comment-error-' + uniqueID).style.display = 'inline-block'
                // TODO - handling file upload error should be dealt here
            } else {
                let { file, engagement, todoID, uniqueID } = data
                let todoComment = {
                    todoId: todoID,
                    lastAction: 'post-comment-attachment',
                    newComment: {
                        content: file.name,
                        file: file._id,
                        sender: c.currentUser._id,
                        dateCreated: file.dateUploaded,
                        status: 'OK'
                    }
                }
                updateTodo(todoComment)
                setTimeout(() => {
                    if (document.getElementById('todo-comment-progress-div-' + uniqueID)) {
                        document.getElementById('todo-comment-progress-div-' + uniqueID).style.display = 'none'
                    }
                }, 2000)
            }
        }

        uploadFileBtn.addEventListener('change', (evt) => {
            let uniqueID = Utility.randomString(7, Utility.ALPHANUMERIC)
            let todoID = evt.target.dataset.todoId
            let date = new Date()
            let dateFormat = {
                military: false,
                month: { type: 'integer', length: 'short' },
                day: { append: false },
                output: 'MM/DD/YYYY'
            }
            let detailsComment = document.getElementById('todoDetailsCommentList')
            let oneComment = Forge.build({ dom: 'div', class: 'todo-comment-container', id: 'todo-comment-div-' + uniqueID })
            let senderDiv = Forge.build({ dom: 'div', style: 'display:block;' })
            let sender = Forge.build({ dom: 'span', class: 'detCommentUser', text: c.currentUser.firstName + ' ' + c.currentUser.lastName })
            let createdDate = Forge.build({ dom: 'div', class: 'detComment', text: Utility.dateProcess(date, dateFormat), style: 'display: inline-block;' })
            let commentDiv = Forge.build({ dom: 'div', style: 'display:block;' })
            let fileName = Forge.build({ dom: 'p', class: 'detComment comment-fileName', text: evt.target.files[0].name })
            let errorMsg = Forge.build({ dom: 'div', class: 'auvicon-caution todo-comment-error', id: 'todo-comment-error-' + uniqueID })
            let fileIcon = Forge.build({ dom: 'div', class: 'auvicon-line-attach comment-attachment', style: 'cursor: default; float: left; margin: 0 8px; position: static;' })
            let progressBarContainer = Forge.build({ dom: 'div', id: 'todo-comment-progress-div-' + uniqueID, class: 'req-det-itemUploadContainer' })
            let progressBar = Forge.build({ dom: 'div', id: 'todo-comment-progress-' + uniqueID, class: 'req-det-itemUploadProgress' })
            senderDiv.appendChild(sender)
            senderDiv.appendChild(createdDate)
            commentDiv.appendChild(errorMsg)
            commentDiv.appendChild(fileIcon)
            commentDiv.appendChild(fileName)
            progressBarContainer.appendChild(progressBar)
            commentDiv.appendChild(progressBarContainer)
            oneComment.appendChild(senderDiv)
            oneComment.appendChild(commentDiv)
            detailsComment.appendChild(oneComment)

            fileUpload_Selection(evt.target.files, todoID, null, uniqueID, progressFunction, completeFunction)
        })

        // search json data
        var time = null
        searchInput.onkeydown = function () {
            clearTimeout(time)

            time = setTimeout(function () {
                loadData(c.currentTodos)
            }, 500)
        }

        const trs = todoTable.querySelectorAll('thead th')
        var count = trs.length
        for (var i = 0; i < count; i++) {
            trs[i].addEventListener('click', function () {
                var data = c.currentTodos

                var field = this.getAttribute('data-id')
                var asc = this.getAttribute('data-sort')
                if (field === null) return

                // if there's something that was sorted before, take the active class out from their dom
                if (previousSort.headerDOM) {
                    previousSort.headerDOM.setAttribute('class', 'table-header')
                    previousSort.headerDOM.querySelector('i').setAttribute('class', 'icon auvicon-sort-down todo-fieldSort-icon')
                }
                previousSort = { headerDOM: this, type: field, ascending: asc }
                if (field === 'dueDate') field = 'dueDateString'
                data.sort(getSortOrder(field, asc))
                this.setAttribute('data-sort', (asc === '1' ? '0' : '1'))
                this.setAttribute('class', 'table-header todo-sort-active')
                this.querySelector('i').setAttribute('class', (asc === '1' ? 'icon auvicon-sort-up todo-fieldSort-icon todo-sort-active' : 'icon auvicon-sort-down todo-fieldSort-icon todo-sort-active'))

                switch (field) {
                    case 'name':
                    case 'dueDate':
                    case 'dueDateString':
                    case 'category':
                    case 'clientAssignee':
                    case 'auditorAssignee':
                        loadData(data)
                        break
                    default:
                }
            })
        }

        btnUndo.onclick = function () {
            log('Event: click btnUndo')
            if (this.classList.contains('disabled')) return
            undoBulkAction()
        }

        todoSelectAllCheck.onclick = function () {
            log('Click on todoSelectAllCheck')

            var findCheckbox = document.getElementById('todo-table').querySelectorAll('tbody input[type="checkbox"]')
            var length = findCheckbox.length
            var checked = this.checked
            document.getElementById('todo-bulk-dropdown').classList.toggle('disabled', (checked !== true))
            for (var i = 0; i < length; i++) {
                findCheckbox[i].checked = checked
            }
        }

        return parentSection
    }

    /**
     * get data of all checkbox is checked
     */
    var getDataAllCheckbox = function () {
        var findCheckbox = document.getElementById('todo-table').querySelectorAll('tbody input[type="checkbox"]')
        var length = findCheckbox.length
        var data = []
        for (var i = 0; i < length; i++) {
            if (findCheckbox[i].checked) {
                data.push(findCheckbox[i].closest('tr').getAttribute('data-id'))
            }
        }
        log(data)
        return data
    }

    /**
     * get file objs organised by Todo names that are in checked Todos.
     */
    var getFileByTodo = function () {
        let fileItems = {}
        let checkedTodoIDs = getDataAllCheckbox()
        checkedTodoIDs.forEach((todoID) => {
            let todo = c.currentTodos.find((todo) => todo._id.toString() === todoID.toString())
            if (todo.requests.length > 0) {
                todo.requests.forEach((request, i) => {
                    if (request.currentFile) {
                        let fileItem = c.currentEngagement.files.find((file) => file._id.toString() === request.currentFile.toString())
                        if (!fileItems[todo.name]) {
                            fileItems[todo.name] = {}
                        }
                        fileItems[todo.name][fileItem._id] = fileItem
                    }
                })
            }
        })
        return fileItems
    }

    /**
     * show this content when list todo is emplty
     */
    var todoEmpty = function () {
        var todoTable = document.getElementById('todo-table').querySelector('tbody')
        todoTable.innerHTML = ''
        const newRow = Forge.build({ dom: 'tr', id: 'empty-todo' })
        todoTable.appendChild(newRow)

        const todoTd = Forge.build({ dom: 'td', 'style': 'text-algin:center;' })
        todoTd.setAttribute('colspan', 11)
        newRow.appendChild(todoTd)

        var devContainer = Forge.build({ 'dom': 'div', 'style': 'width:250px; margin:150px auto 20px auto; text-align:center' })
        todoTd.appendChild(devContainer)
        var loaderImage = Forge.build({ 'dom': 'img', 'src': 'images/icons/clipboard.png', 'style': 'width:107px; height:141px;' })
        devContainer.appendChild(loaderImage)
        var textDiv = Forge.build({ 'dom': 'div', 'text': 'To add additional items to your list press the \'Create To-Do\' button above', 'style': 'margin-top:20px' })
        devContainer.appendChild(textDiv)
    }

    /**
     * generate bulk-actions dropdown
     */
    var createBulkDropDown = function () {
        var dropDown = Forge.build({ 'dom': 'div', 'id': 'todo-bulk-dropdown', 'class': 'ui dropdown disabled' })
        dropDown.setAttribute('tabindex', '0')
        var divBulkText = Forge.build({ 'dom': 'div', 'text': 'Bulk Actions', 'class': 'text bold' })
        divBulkText.setAttribute('data-id', '')
        dropDown.appendChild(divBulkText)
        var carrotDown = Forge.build({ 'dom': 'i', 'class': 'dropdown icon' })
        dropDown.appendChild(carrotDown)
        var menu = Forge.build({ 'dom': 'div', 'class': 'menu' })
        dropDown.appendChild(menu)

        var btnDownloadAttachment = Forge.build({ 'dom': 'button', 'text': 'Download Attachments', 'class': 'item' })
        menu.appendChild(btnDownloadAttachment)

        var btnMarkAsComplete = Forge.build({ 'dom': 'button', 'text': 'Mark as complete', 'class': 'item' })
        if (c.currentUser.type === 'AUDITOR') {
            menu.appendChild(btnMarkAsComplete)
        }

        var itemParent = Forge.build({ 'dom': 'div', 'text': 'Assign to', 'class': 'item parent' })
        menu.appendChild(itemParent)

        var icon = Forge.build({ 'dom': 'i', 'class': 'fa fa-caret-right' })
        itemParent.appendChild(icon)

        var btnDelete = Forge.build({ 'dom': 'button', 'text': 'Delete', 'class': 'item' })
        if (c.currentUser.type === 'AUDITOR') {
            menu.appendChild(btnDelete)
        }

        var menuSub = Forge.build({ 'dom': 'div', 'class': 'menu' })
        itemParent.appendChild(menuSub)
        getBulkClientList().forEach(function (cItem) {
            let text = 'unassigned'
            if (cItem.id !== null && cItem.id !== '') {
                text = cItem.userInfo.firstName + ' ' + cItem.userInfo.lastName
            }
            var btnAssign = Forge.build({ 'dom': 'button', 'text': text + ' (client)', 'class': 'item' })
            btnAssign.setAttribute('data-id', cItem.id)
            menuSub.appendChild(btnAssign)

            btnAssign.onmousedown = function () {
                log('Bulk Action: Assigned')
                var clientAssignee = this.getAttribute('data-id')
                var data = getDataAllCheckbox()
                if (data.length > 0) {
                    var json = {}
                    json.type = COMMON.TODOS_BULK_UPDATE_TYPE.CLIENT_ASSIGN
                    json.updateList = []
                    for (var i = 0; i < data.length; i++) {
                        let todoIndex = c.currentEngagement.todos.findIndex((todo) => {
                            return todo._id.toString() === data[i].toString()
                        })
                        if (todoIndex !== -1) {
                            let assignee = c.currentEngagement.todos[todoIndex].clientAssignee
                            if (assignee && assignee._id.toString() === clientAssignee.toString()) {
                                continue
                            }
                            json.updateList.push({ todoId: data[i], value: clientAssignee })
                        }
                    }
                    if (json.updateList.length !== 0) {
                        c.bulkTodosUpdate(json)
                    }
                }
            }
        })

        getBulkAuditorList().forEach(function (cItem) {
            var btnAssign = Forge.build({ 'dom': 'button', 'text': cItem.userInfo.firstName + ' ' + cItem.userInfo.lastName + ' (auditor)', 'class': 'item' })
            btnAssign.setAttribute('data-id', cItem.id)
            menuSub.appendChild(btnAssign)

            btnAssign.onmousedown = function () {
                log('Bulk Action: Assigned')
                var auditorAssignee = this.getAttribute('data-id')
                var data = getDataAllCheckbox()
                if (data.length > 0) {
                    var json = {}
                    json.type = COMMON.TODOS_BULK_UPDATE_TYPE.AUDITOR_ASSIGN
                    json.updateList = []
                    for (var i = 0; i < data.length; i++) {
                        let todoIndex = c.currentEngagement.todos.findIndex((todo) => {
                            return todo._id.toString() === data[i].toString()
                        })
                        if (todoIndex !== -1) {
                            let assignee = c.currentEngagement.todos[todoIndex].auditorAssignee
                            if (assignee && assignee._id.toString() === auditorAssignee.toString()) {
                                continue
                            }
                            json.updateList.push({ todoId: data[i], value: auditorAssignee })
                        }
                    }
                    if (json.updateList.length !== 0) {
                        c.bulkTodosUpdate(json)
                    }
                }
            }
        })

        btnDownloadAttachment.onmousedown = function () {
            log('Bulk Action: Download Attachment')
            let fileItems = getFileByTodo()

            let path = `/download-folder?eID=${c.currentEngagement._id}&structure=Todo`
            let fd = new FormData()
            fd.append('items', JSON.stringify(fileItems))
            let req = new XMLHttpRequest()
            req.onreadystatechange = () => {
                if (req.readyState === 4 && req.status === 200) {
                    // show zip file ready modal
                    let downloadMessageDiv = Forge.build({ 'dom': 'div', 'style': 'display: block; margin: 20px auto 0px; width: 360px;' })
                    let pictureDiv = Forge.build({ 'dom': 'div', 'style': 'display: block; margin: 34px auto 0px;' })
                    let folderPicture = Forge.build({ 'dom': 'img', 'src': '../../../images/illustrations/box.svg', 'style': 'width: 160px;height:158px; margin-left: 16px;' })
                    let downloadMessage1 = Forge.build({ 'dom': 'span', 'class': 'files-deleteStorage-msg', 'text': 'A zip file containing your selected items are ready.' })
                    let downloadBtnGroup = Forge.build({ 'dom': 'div', 'style': 'display: block; margin: 32px auto 48px;' })
                    let downloadBtn = Forge.buildBtn({ 'text': 'Download', 'type': 'primary', 'id': 'fm-downloadBtn' }).obj
                    downloadBtn.style.display = 'inline-block'

                    pictureDiv.appendChild(folderPicture)
                    downloadMessageDiv.appendChild(downloadMessage1)
                    downloadBtnGroup.appendChild(downloadBtn)

                    $(`#${downloadZipModal.contentID}`).children()[0].innerHTML = ''
                    $(`#${downloadZipModal.contentID}`).children()[0].appendChild(pictureDiv)
                    $(`#${downloadZipModal.contentID}`).children()[0].appendChild(downloadMessageDiv)
                    $(`#${downloadZipModal.contentID}`).children()[0].appendChild(downloadBtnGroup)

                    downloadZipModal.open()

                    downloadBtn.addEventListener('click', (e) => {
                        window.open(`${settings.protocol}://${settings.domain}:${settings.port}/${JSON.parse(req.response).url}/${c.currentEngagement.name}`, '_blank')
                        downloadZipModal.close()
                    })
                } else if (req.readyState === 4 && req.status === 500) {
                    self.trigger('flash', { content: req.response.msg, type: 'ERROR' })
                }
            }
            req.open('POST', path, true)
            req.send(fd)
        }

        btnMarkAsComplete.onmousedown = function () {
            log('Bulk Action: Mark As Complete')
            c.displayMarkCompleteToDoModal()
        }

        btnDelete.onmousedown = function () {
            log('Bulk Action: Delete')
            c.displayDeleteToDoModal()
        }

        var bulkContainer = document.getElementById('bulk-container')
        if (bulkContainer) {
            bulkContainer.innerHTML = ''
            bulkContainer.appendChild(dropDown)
        }
    }

    /**
     * generate filter dropdown
     */
    var createFilterDropDown = function () {
        var check = document.getElementById('todo-filter-dropdown')
        if (check !== null) return

        var dropDown = Forge.build({ 'dom': 'div', 'id': 'todo-filter-dropdown', 'class': 'ui dropdown filter' })
        dropDown.setAttribute('tabindex', '0')
        var icon = Forge.build({ 'dom': 'i', 'class': 'fa fa-filter' })
        dropDown.appendChild(icon)
        var divBulkText = Forge.build({ 'dom': 'div', 'text': 'Filter', 'class': 'text bold' })
        divBulkText.setAttribute('data-id', '')
        dropDown.appendChild(divBulkText)
        var icon = Forge.build({ 'dom': 'i', 'class': 'dropdown icon' })
        dropDown.appendChild(icon)
        var menu = Forge.build({ 'dom': 'div', 'class': 'menu' })
        dropDown.appendChild(menu)

        var btnShowAll = Forge.build({ 'dom': 'button', 'text': 'Show All', 'class': 'item' })
        menu.appendChild(btnShowAll)

        var btnDueDate = Forge.build({ 'dom': 'button', 'text': 'Due Date', 'class': 'item' })
        // TODO - There's no defined requirement for how we want to filter the due date. Hiding for now.
        // menu.appendChild(btnDueDate)

        var itemParent = Forge.build({ 'dom': 'div', 'text': 'Assigned', 'class': 'item parent' })
        menu.appendChild(itemParent)

        var icon = Forge.build({ 'dom': 'i', 'class': 'fa fa-caret-right' })
        itemParent.appendChild(icon)

        var menuSub = Forge.build({ 'dom': 'div', 'class': 'menu dropdown-empty' })
        itemParent.appendChild(menuSub)
        getFilterClientList().forEach(function (cItem) {
            let text = 'unassigned'
            if (cItem.id !== null && cItem.id !== '') {
                text = cItem.userInfo.firstName + ' ' + cItem.userInfo.lastName
            }
            var btnAssigned = Forge.build({ 'dom': 'button', 'text': text, 'class': 'item' })
            btnAssigned.setAttribute('data-id', cItem.id)
            menuSub.appendChild(btnAssigned)

            btnAssigned.onmousedown = function () {
                log('Filter: Assigned')
                divBulkText.innerText = this.innerText
                let assigned = this.getAttribute('data-id')
                filter_value = { type: COMMON.FILTER_ENUM.ASSIGNEE, value: assigned }
                loadData(c.currentTodos)
            }
        })
        getFilterAuditorList().forEach(function (cItem) {
            var btnAssigned = Forge.build({ 'dom': 'button', 'text': cItem.userInfo.firstName + ' ' + cItem.userInfo.lastName, 'class': 'item' })
            btnAssigned.setAttribute('data-id', cItem.id)
            menuSub.appendChild(btnAssigned)

            btnAssigned.onmousedown = function () {
                log('Filter: Assigned')
                divBulkText.innerText = this.innerText
                let assigned = this.getAttribute('data-id')
                filter_value = { type: COMMON.FILTER_ENUM.ASSIGNEE, value: assigned }
                loadData(c.currentTodos)
            }
        })

        var btnWithComment = Forge.build({ 'dom': 'button', 'text': 'With Comments', 'class': 'item' })
        menu.appendChild(btnWithComment)

        var btnComplete = Forge.build({ 'dom': 'button', 'text': 'Complete', 'class': 'item' })
        menu.appendChild(btnComplete)

        var btnOutstanding = Forge.build({ 'dom': 'button', 'text': 'Outstanding', 'class': 'item' })
        menu.appendChild(btnOutstanding)

        var btnFlaggedForRequest = Forge.build({ 'dom': 'button', 'text': 'Flagged For Request', 'class': 'item' })
        // TODO - hiding this for now until we have data model ready for notification flag
        // menu.appendChild(btnFlaggedForRequest)

        btnShowAll.onmousedown = function () {
            log('Filter: Show All')
            divBulkText.innerText = this.innerText
            filter_value = { type: null, value: null }
            loadData(c.currentTodos)
        }

        // TODO - hiding this until we define the behaviour
        // btnDueDate.onmousedown = function () {
        //   log('Filter: Due Date')
        //   divBulkText.innerText = this.innerText
        //   let assigned = this.getAttribute('data-id')
        //   filter_value = {type: COMMON.FILTER_ENUM.DUEDATE, value: assigned}
        //   loadData(c.currentTodos)
        // }

        btnWithComment.onmousedown = function () {
            log('Filter: With Comments')
            divBulkText.innerText = this.innerText
            filter_value = { type: COMMON.FILTER_ENUM.COMMENT, value: null }
            loadData(c.currentTodos)
        }

        btnComplete.onmousedown = function () {
            log('Filter: Complete')
            divBulkText.innerText = this.innerText
            filter_value = { type: COMMON.FILTER_ENUM.COMPLETE, value: null }
            loadData(c.currentTodos)
        }

        btnOutstanding.onmousedown = function () {
            log('Filter: Outstanding')
            divBulkText.innerText = this.innerText
            filter_value = { type: COMMON.FILTER_ENUM.OUTSTANDING, value: null }
            loadData(c.currentTodos)
        }

        // TODO - hiding this because we haven't had todo notification flag working
        // btnFlaggedForRequest.onmousedown = function () {
        //   log('Filter: Flagged For Request')
        //   divBulkText.innerText = this.innerText
        //   filter_value = {type: COMMON.FILTER_ENUM.REQUEST, value: true}
        //   loadData(c.currentTodos)
        // }

        var filterContainer = document.getElementById('filter-container')
        filterContainer.innerHTML = ''
        filterContainer.appendChild(dropDown)
    }

    /**
     * generate category dropdown
     */
    var createCategoryDropDown = function (data) {
        // if current user hasn't for a permission to update General fields, show label only
        if (!c.permission.hasPermissionInTodo(definePermission.TodoPermissions.GENERAL_W, data.todo)) {
            let currentCategory = getCategory().find((item) => {
                return data.todo.category && item._id === data.todo.category._id
            })

            let categoryLabel = Forge.build({ 'dom': 'span', text: 'Uncategorized', class: 'ui label todo-category-readonly' })
            if (currentCategory) {
                categoryLabel.innerText = currentCategory.name
                categoryLabel.style = 'background: ' + currentCategory.color + ';color: white; line-height: 18px;'
                categoryLabel.className = 'ui label todoCategory todo-category'
            }
            return categoryLabel
        }

        var classDisabled = (data.todo != null && data.todo.completed ? 'disabled' : '')
        var dropDown = Forge.build({ 'dom': 'div', 'class': 'ui dropdown todoCategory todo-category todo-bulkDdl todoCategoryEmpty ' + classDisabled })
        dropDown.setAttribute('tabindex', '0')
        var divBulkText = Forge.build({ 'dom': 'div', 'text': data.name, 'class': '' })
        divBulkText.setAttribute('data-id', '')
        dropDown.appendChild(divBulkText)
        var icon = Forge.build({ 'dom': 'i', 'class': 'auvicon-chevron-down todo-categoryChevron' })
        dropDown.appendChild(icon)
        var menu = Forge.build({ 'dom': 'div', 'class': 'menu dropdown-empty' })
        dropDown.appendChild(menu)

        var addItem = Forge.build({ 'dom': 'div', 'text': 'Add New Category', 'class': 'item act_item' })
        menu.appendChild(addItem)

        addItem.addEventListener('click', function () {
            var todoId = this.closest('tr').getAttribute('data-id')
            c.displayAddCategoryModal(todoId)
        })

        if (data.todo && data.todo.category) {
            dropDown.classList.remove('todoCategoryEmpty')
        }

        if (getCategory().length > 0) {
            var editItem = Forge.build({ 'dom': 'div', 'text': 'Edit Categories', 'class': 'item act_item' })
            menu.appendChild(editItem)
            editItem.addEventListener('click', function () {
                c.displayEditCategoryModal()
            })
        }

        if (data.todo !== null) {
            showOnListTodo()
        } else {
            showOnInsertTodo()
        }

        function showOnInsertTodo() {
            // generate category item
            getCategory().forEach(function (item) {
                if (item._id !== 'uncategorized') {
                    var itemDiv = Forge.build({ 'dom': 'div' })
                    var btnItem = Forge.build({ 'dom': 'button', 'text': item.name })
                    itemDiv.appendChild(btnItem)
                    btnItem.setAttribute('data-id', item._id)
                    btnItem.setAttribute('data-color', item.color)
                    menu.appendChild(itemDiv)
                    btnItem.onmousedown = function () {
                        log('Event: click on category name')
                        divBulkText.innerHTML = this.innerHTML
                        divBulkText.setAttribute('data-id', this.getAttribute('data-id'))
                        dropDown.style = 'color: white; border: none; background: ' + this.getAttribute('data-color')
                        // divBulkText.style = ''
                    }
                }
            })
        }

        function showOnListTodo() {
            // generate category item
            getCategory().forEach(function (item) {
                if (item._id !== 'uncategorized') {
                    var editItem = Forge.build({ 'dom': 'div', 'text': item.name, 'class': 'item' })
                    editItem.setAttribute('data-id', item._id)
                    editItem.setAttribute('data-name', item.name)
                    menu.appendChild(editItem)
                    editItem.addEventListener('click', function () {
                        log('Event: click on category name')
                        // update Todo whith selected category
                        var data = {
                            todoId: this.closest('tr').getAttribute('data-id'),
                            categoryId: this.getAttribute('data-id'),
                            categoryName: this.getAttribute('data-name')
                        }
                        log(data)
                        updateTodo(data)
                    })
                    // get category of Todo
                    if (data.todo.category !== null && item._id === data.todo.category._id) {
                        divBulkText.innerHTML = item.name
                        dropDown.style = `background: ${item.color}; border: none;`
                        divBulkText.style = 'color: white;'
                        editItem.classList.add('selected')
                        dropDown.classList.remove('todoCategoryEmpty')
                    }
                }
            })
        }

        return dropDown
    }

    var scrollToBottom = (element) => {
        element.scrollTop = element.scrollHeight
    }

    /**
     * generate client-assigned dropdown
     */
    var renderClientField = function (todo, clientListData) {
        var clientList = clientListData.data
        if (clientListData.readOnly === true) {
            var text = 'Unassigned'
            if (clientList !== undefined && clientList !== null) {
                text = clientList.userInfo.firstName + ' ' + clientList.userInfo.lastName
            }
            return Forge.build({ 'dom': 'p', 'text': text, 'class': 'text' })
        } else {
            var classDisabled = (todo != null && todo.completed ? 'disabled' : '')
            var dropDown = Forge.build({ 'dom': 'div', 'class': 'ui dropdown client todo-bulkDdl ' + classDisabled })
            dropDown.setAttribute('tabindex', '0')
            var divBulkText = Forge.build({ 'dom': 'div', 'text': 'Unassigned', 'class': 'text' })
            dropDown.appendChild(divBulkText)
            var icon = Forge.build({ 'dom': 'i', 'class': 'dropdown icon' })
            dropDown.appendChild(icon)
            var menu = Forge.build({ 'dom': 'div', 'class': 'menu' })
            dropDown.appendChild(menu)

            // list Client of Auditor
            clientList.forEach(function (cItem) {
                var fullname = 'Unassigned'
                if (cItem.id !== null && cItem.id !== '') {
                    fullname = cItem.userInfo.firstName + ' ' + cItem.userInfo.lastName
                }
                var item = Forge.build({ 'dom': 'button', 'text': fullname, 'class': 'item' })
                item.setAttribute('data-id', cItem.id)
                menu.appendChild(item)
                divBulkText.setAttribute('data-selected-id', '')
                if (clientListData.userType.toUpperCase() === 'AUDITOR') {
                    if (todo !== null && todo.clientAssignee !== null) {
                        divBulkText.innerHTML = fullname
                        divBulkText.setAttribute('data-selected-id', cItem.id)
                        item.classList.add('selected')
                    }
                } else {
                    if (todo !== null && todo.clientAssignee !== null && cItem.id === todo.clientAssignee._id) {
                        divBulkText.innerHTML = fullname
                        divBulkText.setAttribute('data-selected-id', cItem.id)
                        item.classList.add('selected')
                    }
                }
                item.onmousedown = function () {
                    log('Event: click client-dropdown')
                    let currentID = divBulkText.getAttribute('data-selected-id')
                    let changedID = this.getAttribute('data-id')
                    if (currentID !== changedID) {
                        if (todo === null) {
                            divBulkText.innerHTML = fullname
                            divBulkText.setAttribute('data-id', this.getAttribute('data-id'))
                            return
                        }
                        var data = {
                            todoId: this.closest('tr').getAttribute('data-id'),
                            clientAssignee: this.getAttribute('data-id')
                        }
                        log(data)
                        updateTodo(data)
                    }
                }
            })
            return dropDown
        }
    }

    /**
     * generate auditor-assigned dropdown
     */
    var renderAuditorField = function (todo, auditorListData) {
        var auditorList = auditorListData.data
        if (auditorListData.readOnly === true) {
            var text = ''
            if (auditorList !== undefined && auditorList !== null) {
                text = auditorList.userInfo.firstName + ' ' + auditorList.userInfo.lastName
            }
            return Forge.build({ 'dom': 'p', 'text': text, 'class': 'text' })
        } else {
            var classDisabled = (todo != null && todo.completed ? 'disabled' : '')
            var dropDown = Forge.build({ 'dom': 'div', 'class': 'ui dropdown auditor todo-bulkDdl ' + classDisabled })
            dropDown.setAttribute('tabindex', '0')
            var divBulkText = Forge.build({ 'dom': 'div', 'text': 'Unassigned', 'class': 'text' })
            dropDown.appendChild(divBulkText)
            var icon = Forge.build({ 'dom': 'i', 'class': 'dropdown icon' })
            dropDown.appendChild(icon)
            var menu = Forge.build({ 'dom': 'div', 'class': 'menu' })
            dropDown.appendChild(menu)

            // list Auditor
            auditorList.forEach(function (cItem) {
                var fullname = 'Unassigned'
                if (cItem.id !== null && cItem.id !== '') {
                    fullname = cItem.userInfo.firstName + ' ' + cItem.userInfo.lastName
                }
                var item = Forge.build({ 'dom': 'button', 'text': fullname, 'class': 'item' })
                item.setAttribute('data-id', cItem.id)
                menu.appendChild(item)
                if (todo != null && todo.auditorAssignee !== null && cItem.id === todo.auditorAssignee._id) {
                    divBulkText.innerHTML = fullname
                    item.setAttribute('data-selected-id', cItem.id)
                    divBulkText.setAttribute('data-selected-id', cItem.id)
                    item.classList.add('selected')
                }
                item.onmousedown = function () {
                    log('Event: click auditor-dropdown')
                    let currentID = divBulkText.getAttribute('data-selected-id')
                    let changedID = this.getAttribute('data-id')
                    if (currentID !== changedID) {
                        if (todo === null) {
                            divBulkText.innerHTML = fullname
                            divBulkText.setAttribute('data-id', this.getAttribute('data-id'))
                            return
                        }
                        var data = {
                            todoId: this.closest('tr').getAttribute('data-id'),
                            auditorAssignee: this.getAttribute('data-id')
                        }
                        log(data)
                        updateTodo(data)
                    }
                }
            })

            return dropDown
        }
    }

    /**
     * format date function
     */
    var formatDate = function (date, prettyDate) {
        if (prettyDate === null || prettyDate === undefined) {
            return (date === null ? '' : Utility.getFormattedDate(date, '/'))
        }

        return moment(date).format('dddd, MMM Do')
    }

    /**
     * generate list todo
     */
    var todoRows = function (data) {
        // document.getElementById('btn-todo-undo').classList.toggle('disabled', (c.undoTodos.length === 0))
        document.getElementById('cb-select-all-todo').checked = false
        const todoTable = document.getElementById('todo-table')
        const tbody = todoTable.querySelector('tbody')
        tbody.innerHTML = ''

        createFilterDropDown()
        if ((c.permission.isTypeClient && c.permission.canSeeAllClientTodo()) || c.permission.isTypeAuditor()) {
            createBulkDropDown()
        }

        // add-todo button
        var renderAddTodoButton = function () {
            var checkElement = document.getElementById('auv-todo-createToDo')
            if (checkElement !== null) checkElement.remove()
            // suggest
            if (!c.permission.isPermissionOnEngagement(definePermission.EngagementPermissions.TODO_W_ADD)) {
                return
            }
            // if ((c.permission.isAdminAuditor() && !c.permission.isLeadAuditor()) || c.currentUser.type !== 'AUDITOR') {
            //   return
            // }
            var toolbar = document.getElementById('todo-toolbar')
            const createNewTodo = Forge.buildBtn({ text: 'Create To-Do', id: 'auv-todo-createToDo', type: 'primary', margin: '10px 0px 0px 10px' }).obj
            toolbar.insertBefore(createNewTodo, toolbar.firstChild)

            createNewTodo.addEventListener('click', () => {
                self.clearSearchAndFilter()
                createTodo({})
            })
        }
        renderAddTodoButton()

        var renderAddWorkingPaperButton = function () {
            var checkElement = document.getElementById('auv-todo-createWorkingPaper')
            if (checkElement !== null) checkElement.remove()

            if (!c.permission.isPermissionOnEngagement(definePermission.EngagementPermissions.TODO_W_ADD)) {
                return
            }

            var toolbar = document.getElementById('todo-toolbar')
            var filter = document.getElementById('filter-container')
            const createNewWorkingPaper = Forge.buildBtn({ text: 'Create Working Paper', id: 'auv-todo-createWorkingPaper', type: 'primary', margin: '10px 0px 0px 10px' }).obj
            toolbar.insertBefore(createNewWorkingPaper, filter)

            createNewWorkingPaper.addEventListener('click', () => {
                c.displayCreateWorkingPaperModal()
            })
        }
        renderAddWorkingPaperButton()

        if (data.length === 0) {
            todoEmpty()
            return
        }

        var placeholder = (data.length > 1 ? 'Write your To-do here' : 'Write your first To-do here')
        document.getElementById('todo-search').disabled = false
        var formCheck = document.getElementById('form-todo')
        if (formCheck !== null) {
            formCheck.remove()
        }

        data.forEach((todo) => {
            // let permistionList = [definePermission.TodoPermissions.AUDITOR_ASSIGNEE_W, definePermission.TodoPermissions.CLIENT_ASSIGNEE_W_LEAD]
            let permistionList = c.permission.getPermissionInTodo(todo)
            const newRow = Forge.build({ dom: 'tr', class: 'newRow' })
            newRow.setAttribute('data-id', todo._id)
            newRow.setAttribute('data-requests', todo.requests.length)
            newRow.setAttribute('data-completed', todo.completed)
            tbody.appendChild(newRow)
            if (todo.completed) {
                newRow.classList.add('todoCompleted')
            }

            /* Row Selection Checkbox */
            const selectToDo = Forge.build({ dom: 'td', style: 'width: 4%; background: /*firebrick*/;' })
            newRow.appendChild(selectToDo)
            var renderSelectBox = function () {
                // suggest
                if (!permistionList.includes(definePermission.TodoPermissions.BULK_W)) {
                    return
                }
                // if (c.permission.isAdminAuditor() && !c.permission.isLeadAuditor()) {
                //   return
                // }
                const toDoCheckbox = Forge.build({ dom: 'input', type: 'checkbox', style: 'margin-left: 16px;' })
                selectToDo.appendChild(toDoCheckbox)

                toDoCheckbox.onclick = function () {
                    var countChecked = document.getElementById('todo-table').querySelectorAll('tbody input[type="checkbox"]:checked').length
                    var countCheckbox = document.getElementById('todo-table').querySelectorAll('tbody input[type="checkbox"]').length

                    document.getElementById('todo-bulk-dropdown').classList.toggle('disabled', (countChecked === 0))

                    var checkboxAll = document.getElementById('cb-select-all-todo')
                    log('Click on checkbox ', countChecked, countCheckbox)
                    if (countChecked === countCheckbox) {
                        checkboxAll.checked = true
                    } else {
                        checkboxAll.checked = false
                    }
                }
            }
            renderSelectBox()

            /* Todo title (todo's name) */
            const newTodoTitle = Forge.build({ dom: 'td', style: 'width: 30%; background: /*red*/;' })
            var renderTodoName = function () {
                if (!c.permission.hasPermissionInTodo(definePermission.TodoPermissions.GENERAL_W, todo) || todo.completed) {
                    const newTodoInput = Forge.build({ dom: 'span', text: todo.name, class: 'todo-name-readonly' })
                    newTodoTitle.appendChild(newTodoInput)
                    return
                }
                const newTodoInput = Forge.build({ dom: 'input', type: 'text', placeholder: placeholder, value: todo.name, class: 'newTodoInput' })
                newTodoInput.setAttribute('data-dbname', todo.name)
                newTodoInput.setAttribute('maxlength', 255)
                newTodoTitle.appendChild(newTodoInput)
                if (todo.completed) {
                    newTodoInput.readOnly = true
                }
                newTodoInput.onblur = function () {
                    if (!isFieldValid(this) || this.value === this.getAttribute('data-dbname')) return
                    var data = {
                        todoId: this.closest('tr').getAttribute('data-id'),
                        name: this.value
                    }
                    log(data)
                    updateTodo(data)
                }
                newTodoInput.onkeyup = function () {
                    isFieldValid(this)
                }
            }
            renderTodoName()

            /* Category dropdown */
            const newCategory = Forge.build({ dom: 'td', style: 'width: 10%; background: /*orange*/;' })
            newCategory.appendChild(createCategoryDropDown({ name: 'Select', 'todo': todo }))

            /* Client assignee dropdown */
            const newClientAssigned = Forge.build({ dom: 'td', style: 'width: 13%; background: /*yellow*/;' })
            let clientListData = getTodoClientList(permistionList, todo)
            newClientAssigned.appendChild(renderClientField(todo, clientListData))

            /* Due date's datepicker */
            const dueDateTd = Forge.build({ dom: 'td', style: 'width: 10%; position: relative;' })
            var renderDueDate = function () {
                if (!c.permission.hasPermissionInTodo(definePermission.TodoPermissions.GENERAL_W, todo) || todo.completed) {
                    var due = formatDate(todo.dueDate)
                    const newDueDate = Forge.build({ dom: 'span', text: (due !== '' ? due : '') })
                    dueDateTd.appendChild(newDueDate)
                    return
                }
                const iconCalendar = Forge.build({ dom: 'i', 'class': 'fa auvicon-calendar todo-calendar' })
                var newDueDate = Forge.build({ 'dom': 'input', value: formatDate(todo.dueDate), 'class': 'auv-input input-due-date datepicker mg5', 'placeholder': 'Select' })
                newDueDate.setAttribute('data-dbdate', formatDate(todo.dueDate))
                dueDateTd.appendChild(iconCalendar)
                dueDateTd.appendChild(newDueDate)
                if (todo.completed) {
                    newDueDate.classList.toggle('datepicker', false)
                }
                newDueDate.onkeydown = function () {
                    return false
                }

                iconCalendar.onclick = function () {
                    this.nextSibling.focus()
                }

                newDueDate.onchange = function () {
                    log('Event: update duedate')
                    var self = this
                    setScrollPos()

                    setTimeout(function () {
                        var data = {
                            todoId: self.closest('tr').getAttribute('data-id'),
                            dueDate: new Date(self.value)
                        }
                        var checkDue = checkDueDate(data)
                        log('dueDate compare with engagement and now: ' + checkDue)
                        if (newDueDate.getAttribute('data-dbdate') === self.value || !checkDue) {
                            self.classList.toggle('todo-due-date-error', true)
                        } else {
                            updateTodo(data)
                        }
                    }, 500)
                }
            }
            renderDueDate()

            /* Auditor assignee dropdown */
            const newAuditorAssigned = Forge.build({ dom: 'td', style: 'width: 10%; background: /*green*/;' })
            let auditorListData = getTodoAuditorList(permistionList, todo)
            newAuditorAssigned.appendChild(renderAuditorField(todo, auditorListData))

            /* Click this to open todo details. */
            const newOpenDetails = Forge.build({ dom: 'td', style: 'width: 4%; text-align: center; cursor: pointer; background: /*cyan;*/' })
            const openDetails = Forge.build({ dom: 'img', sytle: 'pointer-events: none; cursor: default;', src: '../../images/icons/icon_slideOutMenu.svg' })

            newOpenDetails.appendChild(openDetails)
            // TODO - code cleanup (everything should be disabled but still be displayable.)
            if (!todo.completed) {
                newOpenDetails.addEventListener('click', () => {
                    document.getElementById('auv-todo-details').style.display = 'inherit'
                    loadTodo(todo)
                })
            }
            newOpenDetails.addEventListener('click', () => {
                loadTodo(todo)
            })

            /* New request alert flag */
            /* If there is an unviewed file, a flag with the number of unviewed files is displated here. */
            const newRequestAlert = Forge.build({ dom: 'td' })
            let numNewFiles = 0
            for (let i = 0; i < todo.requests.length; i++) {
                if (todo.requests[i].viewed === false && todo.requests[i].status !== 'INACTIVE') {
                    numNewFiles++
                }
            }
            if (numNewFiles > 0 && !todo.completed) {
                const newRequestFlag = Forge.build({ dom: 'div', class: 'newRequestFlag' })
                const newRequestNumber = Forge.build({ dom: 'div', text: numNewFiles, class: 'newRequestNumber' })
                newRequestAlert.appendChild(newRequestFlag)
                newRequestAlert.appendChild(newRequestNumber)
                newRow.style.fontWeight = 'bold'
            }

            // If the todo had a uploaded file an icon with the number of files is displayed
            const newFileNumber = Forge.build({ dom: 'td', style: 'width: 4%; background: /*blue*/;' })
            let numFiles = 0
            for (let i = 0; i < todo.requests.length; i++) {
                if (todo.requests[i].currentFile && todo.requests[i].status !== 'INACTIVE') {
                    numFiles++
                }
            }
            if (numFiles > 0) {
                const fileIcon = Forge.build({ dom: 'div', class: 'auvicon-file-empty fileIcon' })
                const fileNumber = Forge.build({ dom: 'div', text: numFiles, class: 'fileNumber' })
                newFileNumber.appendChild(fileIcon)
                newFileNumber.appendChild(fileNumber)
            }

            /* New comment bubble */
            // If there is a comment that hasn't been seen yet there is a orange buble displayed with the number of new messages
            const newCommentAlert = Forge.build({ dom: 'td', style: 'width: 4%; background: /*indigo*/;' })
            let unreadComments = 0
            for (let i = 0; i < todo.comments.length; i++) {
                if (todo.comments[i].status === 'OK') {
                    unreadComments++
                }
            }
            if (unreadComments > 0 && !todo.completed) {
                const commentFlag = Forge.build({ dom: 'div', class: 'auvicon-messages commentFlag' })
                const commentNumber = Forge.build({ dom: 'div', text: unreadComments, class: 'commentNumber' })
                newCommentAlert.appendChild(commentFlag)
                commentFlag.appendChild(commentNumber)
                // TODO - hiding this for now
                // newRow.style.fontWeight = 'bold'
            }

            // Contains functionality for marking a todo as completed
            const newCompletedCheck = Forge.build({ dom: 'td', style: 'width: 4%; background: /*violet*/;' })
            var renderCompleteBtn = function () {
                let completeBtn = null
                if (todo.completed) {
                    completeBtn = Forge.build({ dom: 'div', class: 'auvicon-circle-checkmark completeBtn priColor' })
                } else {
                    completeBtn = Forge.build({ dom: 'div', class: 'auvicon-line-circle-checkmark completeBtn lightgreyColor-light' })
                }

                if (c.permission.hasPermissionInTodo(definePermission.TodoPermissions.STATUS_W_COMPLETE, todo)) {
                    completeBtn.addEventListener('click', function () {
                        var completed = true
                        if (this.closest('tr').getAttribute('data-completed') === 'true') completed = false
                        var data = {
                            todoId: this.closest('tr').getAttribute('data-id'),
                            completed: completed
                        }
                        log(data)
                        updateTodo(data)
                    })
                }
                newCompletedCheck.appendChild(completeBtn)
            }
            renderCompleteBtn()

            newRow.appendChild(newTodoTitle)
            newRow.appendChild(newCategory)
            newRow.appendChild(newClientAssigned)
            newRow.appendChild(dueDateTd)
            newRow.appendChild(newAuditorAssigned)
            newRow.appendChild(newOpenDetails)
            newRow.appendChild(newRequestAlert)
            newRow.appendChild(newFileNumber)
            newRow.appendChild(newCommentAlert)
            newRow.appendChild(newCompletedCheck)
        })

        // checkbox all
        var checkboxall = todoTable.querySelector('input[type="checkbox"]')
        checkboxall.classList.toggle('hide', todoTable.querySelectorAll('tbody input[type="checkbox"]').length === 0)

        setTimeout(function () {
            let engagementDueDate = c.currentEngagement.dueDate
            if (engagementDueDate && engagementDueDate !== undefined) {
                engagementDueDate = new Date(engagementDueDate)
                $('.datepicker').datepicker({ minDate: 0, maxDate: engagementDueDate, dateFormat: 'mm/dd/yy' })
            } else {
                $('.datepicker').datepicker({ minDate: 0, dateFormat: 'mm/dd/yy' })
            }
        }, 500)
    }

    var getTodoAuditorList = function (pemissionList, todo) {
        if (pemissionList.includes(definePermission.TodoPermissions.AUDITOR_ASSIGNEE_W)) {
            return { readOnly: false, data: c.getUsersByRole('AUDITOR', false, false) }
        } else {
            var acl = c.currentEngagement.acl
            var returnedValue = null
            _.map(acl, function (user) {
                if (todo.auditorAssignee._id === user.id) {
                    returnedValue = user
                }
            })
            return { readOnly: true, data: returnedValue }
        }
    }

    var getTodoClientList = function (pemissionList, todo) {
        if (!pemissionList.includes(definePermission.TodoPermissions.CLIENT_ASSIGNEE_W_LEAD) && !pemissionList.includes(definePermission.TodoPermissions.CLIENT_ASSIGNEE_W_GENERAL)) {
            var acl = c.currentEngagement.acl
            var returnedValue = null
            _.map(acl, function (user) {
                if (todo.clientAssignee !== null && todo.clientAssignee._id === user.id) {
                    returnedValue = user
                }
            })
            return { readOnly: true, data: returnedValue }
        }

        if (pemissionList.includes(definePermission.TodoPermissions.CLIENT_ASSIGNEE_W_GENERAL)) {
            return { readOnly: false, userType: c.currentUser.type, data: c.getUsersByRole('CLIENT', false, false) }
        }

        if (pemissionList.includes(definePermission.TodoPermissions.CLIENT_ASSIGNEE_W_LEAD)) {
            let hasUnassigned = true
            if (c.currentUser.type.toUpperCase() === 'CLIENT') {
                hasUnassigned = false
            }
            return { readOnly: false, userType: c.currentUser.type, data: c.getUsersByRole('CLIENT', hasUnassigned, true) }
        }
    }

    var getFilterAuditorList = function () {
        var hasWritePermission = false
        var returnedValue = []
        var existedUsers = []
        _.map(c.currentTodos, function (todo) {
            var pemissionList = c.permission.getPermissionInTodo(todo)
            if (pemissionList.includes(definePermission.TodoPermissions.AUDITOR_ASSIGNEE_W)) {
                hasWritePermission = true
            }
            _.map(c.currentEngagement.acl, function (user) {
                if (todo.auditorAssignee._id === user.id && !existedUsers.includes(user.id)) {
                    existedUsers.push(user.id)
                    returnedValue.push(user)
                }
            })
        })
        if (hasWritePermission === true) return c.getUsersByRole('AUDITOR', false, false)
        return returnedValue
    }

    var getFilterClientList = function () {
        var hasWritePermission = false
        var isLead = false
        var returnedValue = []
        var existedUsers = []
        _.map(c.currentTodos, function (todo) {
            var pemissionList = c.permission.getPermissionInTodo(todo)
            if (pemissionList.includes(definePermission.TodoPermissions.CLIENT_ASSIGNEE_W_GENERAL)) {
                hasWritePermission = true
            }
            if (pemissionList.includes(definePermission.TodoPermissions.CLIENT_ASSIGNEE_W_LEAD)) {
                isLead = true
            }
            _.map(c.currentEngagement.acl, function (user) {
                if (todo.clientAssignee !== null && todo.clientAssignee._id === user.id && !existedUsers.includes(user.id)) {
                    existedUsers.push(user.id)
                    returnedValue.push(user)
                }
            })
        })
        if (hasWritePermission === true) {
            return c.getUsersByRole('CLIENT', false, false)
        } else if (isLead === true) {
            return c.getUsersByRole('CLIENT', true, true)
        }
        return returnedValue
    }

    var getBulkAuditorList = function () {
        var hasWritePermission = false
        _.map(c.currentTodos, function (todo) {
            var pemissionList = c.permission.getPermissionInTodo(todo)
            if (pemissionList.includes(definePermission.TodoPermissions.AUDITOR_ASSIGNEE_W)) {
                hasWritePermission = true
            }
        })
        if (hasWritePermission === true) return c.getUsersByRole('AUDITOR', false, false)
        return []
    }

    var getBulkClientList = function () {
        var hasWritePermission = false
        var isLead = false
        var returnedValue = []
        _.map(c.currentTodos, function (todo) {
            var pemissionList = c.permission.getPermissionInTodo(todo)
            if (pemissionList.includes(definePermission.TodoPermissions.CLIENT_ASSIGNEE_W_GENERAL)) {
                hasWritePermission = true
            }
            if (pemissionList.includes(definePermission.TodoPermissions.CLIENT_ASSIGNEE_W_LEAD)) {
                isLead = true
            }
        })
        if (hasWritePermission === true) {
            return c.getUsersByRole('CLIENT', false, false)
        } else if (isLead === true) {
            return c.getUsersByRole('CLIENT', true, true)
        }
        return returnedValue
    }

    /**
     *
     * @param {JSON} err
     * @param {JSON} json
     */
    var fileUploadProgress = function (err, data) {
        if (err) {
            self.trigger('flash', { content: 'There was an error while uploading your file.' })
        } else {
            let { uniqueID, value } = data

            document.getElementById('todo-request-progress-div-' + uniqueID).style.display = 'inline-block'
            if (document.getElementById('todo-request-progress-' + uniqueID)) {
                document.getElementById('todo-request-progress-' + uniqueID).style.width = value + '%'
            }
        }
    }

    /**
     * This function handles the callback from upload-file
     * @param {JSON} err { msg }
     * @param {JSON} data { file, engagement, requestID, uniqueID }
     */
    var fileUploadComplete = function (err, data) {
        if (err) {
            self.trigger('flash', { content: 'There was an error while uploading your file.' })
        } else {
            let { file, engagement, todoID, requestID, oldRequest } = data
            // create activity for successful file upload
            let currentRequest
            // loadTodo with the latest info.
            engagement.todos.forEach((todo) => {
                if (todo.requests.length > 0) {
                    todo.requests.forEach((request) => {
                        if (request._id.toString() === requestID.toString()) {
                            currentRequest = request
                            loadTodo(todo)
                        }
                    })
                }
            })
            // Load Todo list table
            if (engagement && engagement.todos && engagement.todos.length > 0) {
                loadData(engagement.todos.reverse())
            }

            // TODO - this should be gone after db model changes (when each request will have engagementID and todoID in it)
            // oldRequest.todoID = todoID
            currentRequest.todoID = todoID

            let activity = {
                engagementID: engagement._id,
                operation: 'UPDATE',
                type: 'Request-file',
                original: oldRequest,
                updated: currentRequest
            }
            c.createActivity(activity)
        }
    }

    var fileUpload_Selection = function (files, todoID, requestID, uniqueID, progressFunction, completeFunction) {
        if (files.length === 1) { // only accepts one file for each request.
            let file = files[0]

            if (!file.type) {
                // .DS_Store file
            } else if (!Utility.checkMimeType(file.type)) {
                self.trigger('flash', { content: 'Not a valid file type.', type: 'ERROR' })
                document.getElementById('todo-comment-div-' + uniqueID).remove()
            } else {
                let todo = c.currentTodos.find((todo) => { return todo._id.toString() === todoID.toString() })
                let request = {}
                if (requestID) {
                    request = todo.requests.find((request) => { return request._id.toString() === requestID.toString() })
                }

                self.trigger('upload-file', {
                    file: file,
                    uniqueID: uniqueID,
                    engagementID: c.currentEngagement._id,
                    todoID: todoID,
                    requestID: requestID,
                    oldRequest: request,
                    progress: progressFunction || fileUploadProgress,
                    callback: completeFunction || fileUploadComplete
                })
            }
        } else {
            self.trigger('flash', { content: 'You can only upload one file for each request.', type: 'ERROR' })
        }
    }

    /**
     * Add Drag and drop function for file upload to the dom that is passed in.
     */
    var attachDragnDrop = function (dom, todo, request) {
        dom.ondragover = dom.ondragenter = function (evt) {
            this.style.borderColor = '#5CD4C0'
            evt.stopPropagation()
            evt.preventDefault()
        }

        dom.ondragleave = function (evt) {
            this.style.borderColor = '#c4c4c4'
        }

        dom.ondrop = function (evt) {
            this.style.borderColor = '#c4c4c4'

            evt.stopPropagation()
            evt.preventDefault()

            fileUpload_Selection(evt.dataTransfer.files, todo._id, request._id, request._id)
        }

        dom.ondragend = function (evt) {
            evt.dataTransfer.clearData()
        }
    }

    /**
     * Every ToDo will be able to open a details box that will be populated with the information of that ToDo
     * This handles adding in each Request for file and comments, and populates relivant info.
     */
    const loadTodo = (todo) => {
        currentTodo = todo;
        
        var typyWorkingPaperName = 'Working Papers';
        let permissions = c.permission.getPermissionInTodo(todo)
        if (!permissions.includes(definePermission.TodoPermissions.REQUEST_W)) {
            document.getElementById('add-request-btn').style.display = 'none'
        }
        if (document.getElementById('add-request-btn')) {
            document.getElementById('add-request-btn').dataset.todoId = todo._id
        }
        if (!permissions.includes(definePermission.TodoPermissions.COMMENT_W)) {
            document.getElementById('comment-button').style.display = 'none'
            document.getElementById('comment-input').style.display = 'none'
        }
        if (todo.category != null && todo.category.name == typyWorkingPaperName) {
            document.getElementById('add-request-btn').style.display = 'none';
            document.getElementById('auv-todo-detailsReqBox').style.display = 'none';
            document.getElementById('auv-todo-details-auditors').innerHTML = '';
            document.getElementById('auv-todo-details-auditors').style.display = 'inline-block';
            document.getElementById('auv-todo-details-customers').innerHTML = '';
            document.getElementById('auv-todo-details-customers').style.display = 'block';

            document.getElementById('todoDetailsName').style.display = 'none';
            document.getElementById('todoDetailsCategory').style.display = 'none';
            document.getElementById('todoDetailsDescription').style.display = 'none';
            document.getElementById('todo-table-wpDescription').style.display = 'block';
            document.getElementById('todo-table-wpclient').style.display = 'table';
            document.getElementById('auv-todo-dueDate').style.display = 'none';
            document.getElementById('auv-todo-dueDateDiv2').style.display = 'inline-block';
        }
        else {
            document.getElementById('add-request-btn').style.display = 'inline-block';
            document.getElementById('auv-todo-detailsReqBox').style.display = 'block';
            document.getElementById('auv-todo-details-auditors').style.display = 'none';
            document.getElementById('auv-todo-details-customers').style.display = 'none';
            document.getElementById('todoDetailsName').style.display = 'block';
            document.getElementById('todoDetailsCategory').style.display = 'block';
            document.getElementById('todoDetailsDescription').style.display = 'block';
            document.getElementById('todo-table-wpDescription').style.display = 'none';
            document.getElementById('todo-table-wpclient').style.display = 'none';
            document.getElementById('auv-todo-dueDate').style.display = 'inline-block';
            document.getElementById('auv-todo-dueDateDiv2').style.display = 'none';
        }

        
        //auditors
        var auditors = [];
        auditors.push({
            name: 'Alim',
            profilePicture: '../../images/profilePhotos/auditors/karen.png'
        });
        auditors.push({
            name: 'Allicia',
            profilePicture: '../../images/profilePhotos/auditors/lalit.png'
        });
        if (auditors.length > 0) {
            for (var i = 0; i < auditors.length; i++) {
                var element = Forge.build({ dom: 'div', src: auditors[i].profilePicture, style: 'margin-right: 20px; display: inline-block;' })
                element.appendChild(Forge.build({ dom: 'img', src: auditors[i].profilePicture, style: 'margin-right: 5px; display: inline-block;vertical-align:middle' }));
                element.appendChild(Forge.build({ dom: 'span', text: auditors[i].name }))
                document.getElementById('auv-todo-details-auditors').appendChild(element)
            }
        }

        document.getElementById('auv-todo-dueDate').innerHTML = formatDate(todo.dueDate)
        document.getElementById('comment-button').dataset.todoId = todo._id
        document.getElementById('comment-input').dataset.todoId = todo._id
        document.getElementById('comment-file-button').dataset.todoId = todo._id
        document.getElementById('todoDetailsName').value = todo.name;
        document.getElementById('todoDetailsName').readOnly = false;
        document.getElementById('todoDetailsCategory').innerHTML = todo.category ? todo.category.name : 'uncategorized'
        document.getElementById('todoDetailsCategory').style.background = todo.category ? todo.category.color : '#B5B5B5'
        document.getElementById('todoDetailsDescription').value = todo.description ? todo.description : '';
        document.getElementById('todoDetailsDescription').readOnly = false;
        if (todo.category != null && todo.category.name == typyWorkingPaperName) {
            document.getElementById('todoDetailsName').value = 'Accounts Receiveable';
            document.getElementById('todoDetailsName').readOnly = true;
            document.getElementById('todoDetailsDescription').value = 'Please review and save the confirmation letters for the following requests that your client will send to their customers.';
            document.getElementById('todoDetailsDescription').readOnly = true;
        }

        var todoTableWPDescription = document.getElementById('todo-table-wpDescription')
        var todoTableWPDescriptiontbody = todoTableWPDescription.querySelector('tbody')
        todoTableWPDescriptiontbody.innerHTML = '';
        var todoTableWPDescriptiontbodyTr = "<tr>";
        todoTableWPDescriptiontbodyTr += "<td colspan='2' style='padding-bottom:15px; padding-top:5px; font-weight: bold'>" + "Flower Shop 2016" + "</td>";
        todoTableWPDescriptiontbodyTr += "</tr>";
        todoTableWPDescriptiontbodyTr += "<tr>";
        todoTableWPDescriptiontbodyTr += "<td style='width: 30px; vertical-align: top'>" + '<div class="auvicon-line-circle-checkmark completeBtn lightgreyColor-light"></div>' + "</td>";
        todoTableWPDescriptiontbodyTr += "<td>" + '<div class="detailsName" style="width: auto; margin:0px; font-weight: 500; margin-bottom: 5px">Accounts Receivable' + '<div class="todoCategory" style="display: inline-block; height: 20px !important; margin-left: 15px; background: ' + (todo.category ? todo.category.color : '#27457a') + ';">' + (todo.category ? todo.category.name : 'uncategorized') + '</div>' + '</div>';
        todoTableWPDescriptiontbodyTr += '<div class="detailsDescription" style="width: auto; height: auto; margin:0px; margin-bottom: 5px">Please review and save the confirmation letters for the following requests that your client will send to their customers.</div>';
        todoTableWPDescriptiontbodyTr += '<div style="width: auto; margin:0px; margin-bottom: 5px">Requests Confirmed: 20% (1/5)</div>';
        todoTableWPDescriptiontbodyTr += '<div class="processbar"><div class="percent" style="width: 20%;"></div></div>' + "</td>";
        todoTableWPDescriptiontbodyTr += "</tr>";
        todoTableWPDescriptiontbody.innerHTML = todoTableWPDescriptiontbodyTr;
        document.getElementById('auv-todo-dueDatetext2').innerHTML = formatDate(todo.dueDate)
        //client
        var signItems = [
            {
                id: 1,
                name: "Staples",
                isSendToClient: true,
                isClientApproved: false,
                isSendCustomer: false,
                isCustomerApproved: false,
                isCompleted: false
            },
            {
                id: 2,
                name: "Walmart",
                isSendToClient: true,
                isClientApproved: true,
                isSendCustomer: false,
                isCustomerApproved: false,
                isCompleted: false
            },
            {
                id: 3,
                name: "Officeworks",
                isSendToClient: true,
                isClientApproved: true,
                isSendCustomer: true,
                isCustomerApproved: false,
                isCompleted: false
            },
            {
                id: 4,
                name: "Acme Inc.",
                isSendToClient: true,
                isClientApproved: true,
                isSendCustomer: true,
                isCustomerApproved: true,
                isCompleted: true
            },
            {
                id: 5,
                name: "XYZ Co.",
                isSendToClient: false,
                isClientApproved: false,
                isSendCustomer: false,
                isCustomerApproved: false,
                isCompleted: false
            }
        ]
        var todoTableWClient = document.getElementById('todo-table-wpclient');
        var todoTableWClienttbody = todoTableWClient.querySelector('tbody')
        todoTableWClienttbody.innerHTML = '';
        var innerHtml = "";
        innerHtml += '<tr>' +
            '<td style="width: 30px; border:0"><input type="checkbox" id="todo-table-wpclient-checkall" /></td>' +
            '<td colspan="5" style="border:0">' +
            '<select class="wp-bulkaction"><option>Bulk Actions</option></select>';
        if (c.currentUser.type == 'AUDITOR') {
            innerHtml += '<button class="primary generatewp complete">Generate Working Paper</button>';
        }
        innerHtml += '</td > '+'</tr>';
        todoTableWClienttbody.innerHTML = innerHtml;
        
        for (var i = 0; i < signItems.length; i++) {
            innerHtml = '<tr>' +
                '<td style="width: 30px"><input type="checkbox" class="markcompleted" /></td>' +
                '<td style="width: 30px; text-align: center">';
            if (signItems[i].isCompleted) {
                innerHtml +=
                    '<div class="auvicon-circle-checkmark completeBtn priColor"></div>';
            }
            else {
                innerHtml +=
                    '<div class="auvicon-line-circle-checkmark completeBtn lightgreyColor-light"></div>';
            }
            innerHtml += '</td > ' +
                '<td>' +
                signItems[i].name +
                '</td > ' +
                '<td style="text-align: right">';
            
            if (c.currentUser.type == 'AUDITOR') {
                if (signItems[i].isSendToClient) {
                    if (signItems[i].isClientApproved) {
                        if (signItems[i].isSendCustomer) {
                            if (signItems[i].isCustomerApproved) {
                                innerHtml += '<button class="primary approved">Approved, Customer</button>';
                                innerHtml += '<button class="primary view">View</button>';
                            }
                            else {
                                innerHtml += '<button class="primary review">Review, Customer</button>';
                                innerHtml += '<button class="primary view">View</button>';
                                innerHtml += '<button class="primary send">Send to Customer</button>';
                            }
                        }
                        else {
                            innerHtml += '<button class="primary approved">Approved, Client</button>';
                            innerHtml += '<button class="primary view">View</button>';
                            innerHtml += '<button class="primary send">Send to Customer</button>';
                        }
                    }
                    else {
                        innerHtml += '<button class="primary review">Review, Client</button>';
                        innerHtml += '<button class="primary view">View</button>';
                        innerHtml += '<button class="primary send">Resend to Client</button>';
                    }
                }
                else {
                    innerHtml += '<button class="primary review">Review, Auditor</button>';
                    innerHtml += '<button class="primary view">View</button>';
                    innerHtml += '<button class="primary send">Send to Client</button>';
                }
            }
            if (c.currentUser.type == 'CLIENT') {
                if (signItems[i].isSendToClient) {
                    if (signItems[i].isClientApproved) {
                        if (signItems[i].isSendCustomer) {
                            if (signItems[i].isCustomerApproved) {
                                innerHtml += '<button class="primary approved">Approved, Customer</button>';
                                innerHtml += '<button class="primary view">View</button>';
                            }
                            else {
                                innerHtml += '<button class="primary review">Review, Customer</button>';
                                innerHtml += '<button class="primary view">View</button>';
                            }
                        }
                        else {
                            innerHtml += '<button class="primary approved">Approved, Client</button>';
                            innerHtml += '<button class="primary view">View</button>';
                        }
                    }
                    else {
                        innerHtml += '<button class="primary review">Review, Client</button>';
                        innerHtml += '<button class="primary view">View</button>';
                        innerHtml += '<button class="primary send">Send to Auditor</button>';
                    }
                }
                else {
                    innerHtml += '<button class="primary review">Review, Auditor</button>';
                    innerHtml += '<button class="primary view">View</button>';
                }
            }
            
            innerHtml += '</td > ' +
                '<td style="padding-top: 5px; width:30px"> ' +
                '<i class="fa fa-cloud-upload"  style="font-size:25px;color:#636363" ></i>' +
                '</td > ' +
                '<td  style="padding-top: 5px; width:30px"> ' +
                '<img src="../../images/mode-circular-button.png" style="width:25px" />' +
                '</td > ' +
                '</tr>';
            todoTableWClienttbody.innerHTML += innerHtml;
        }
        
        // view preview handle
        $('.primary.view').click(function () {
            c.displayViewPreviewModal();

            // hide all address editors
            $("#opening-address-editor").hide();
            $("#opening-address-container").show();
            $("#closing-address-editor").hide();
            $("#closing-address-container").show();
        });

        $(".generatewp").click(function () {
            c.displayModalGenerateWPConfirm();
        });
        $('#todo-table-wpclient-checkall').click(function () {
            $(".markcompleted").prop("checked", $(this).is(':checked'));
        });
        $('#todo-table-wpclient .markcompleted').click(function () {
            var countChecked = 0;
            var countUnChecked = 0;
            $('#todo-table-wpclient .markcompleted').each(function (item) {
                if ($(this).is(':checked')) {
                    countChecked++;
                }
                else {
                    countUnChecked++;
                }
            });
            if (countChecked == signItems.length) {
                $("#todo-table-wpclient-checkall").prop("checked", true);
            }
            if (countUnChecked > 0) {
                $("#todo-table-wpclient-checkall").prop("checked", false);
            }
        });
        let descriptionTimeout = null
        document.getElementById('todoDetailsDescription').onkeydown = () => {
            clearTimeout(descriptionTimeout)

            descriptionTimeout = setTimeout(() => {
                let descriptionText = document.getElementById('todoDetailsDescription').value
                let todoDescription = {
                    todoId: todo._id,
                    lastAction: 'update-todo-description',
                    description: descriptionText
                }
                updateTodo(todoDescription)
            }, 2000)
        }
        const detailsReqBox = document.getElementById('auv-todo-detailsReqBox')
        detailsReqBox.innerHTML = ''

        // Builds out empty request or populates each request for file.
        log('load-todo: ', todo.requests)
        if (todo.requests.length > 0) {
            const detailsDnDText = Forge.build({ dom: 'p', text: 'Drag and Drop files or browse.', class: 'detailsDnD' })
            detailsReqBox.appendChild(detailsDnDText)

            const detailsRequestContainer = Forge.build({ dom: 'div', id: 'todoDetailsReqCont' })
            detailsReqBox.appendChild(detailsRequestContainer)

            todo.requests.forEach((request, i) => {
                let requestOriginalName = request.name
                if (request.status !== 'INACTIVE') {
                    const requestElement = Forge.build({ dom: 'div', id: 'todo-req-box-' + i })
                    requestElement.dataset.id = request._id
                    attachDragnDrop(requestElement, todo, request)
                    var _temp_option = ''
                    if (c.permission.isTypeClient()) {
                        _temp_option = Forge.build({ dom: 'div', style: 'display: none', class: 'ui dropdown auvicon-line-circle-more todo-circle-more todo-icon-hover' })
                    } else {
                        _temp_option = Forge.build({ dom: 'div', class: 'ui dropdown auvicon-line-circle-more todo-circle-more todo-icon-hover' })
                    }

                    const options = _temp_option
                    const rightIcons = Forge.build({ dom: 'span', style: 'float: right;' })

                    const nameLabel = Forge.build({ dom: 'span', text: request.name, style: 'display:inline-block;vertical-align:middle' })
                    nameLabel.dataset.id = request._id
                    let progressBarContainer = Forge.build({ dom: 'div', id: 'todo-request-progress-div-' + request._id, class: 'req-det-itemUploadContainer request-progressBar' })
                    let progressBar = Forge.build({ dom: 'div', id: 'todo-request-progress-' + request._id, class: 'req-det-itemUploadProgress' })
                    progressBarContainer.appendChild(progressBar)
                    const fileName = Forge.build({ dom: 'div', 'style': 'display:none; vertical-align: top; margin-top: 4px; width: 250px;' })

                    if (request.currentFile) {
                        requestElement.className = 'detReqForFile detReqWithFile'
                        const checkmark = Forge.build({ dom: 'span', class: 'auvicon-checkmark icon-button', style: 'font-size: 16px; vertical-align: middle; display:inline-block' })
                        const file = Forge.build({ dom: 'span', class: 'auvicon-line-file icon-button', style: 'color: #707070; font-size: 22px; margin: 0 8px; vertical-align: middle; display:inline-block' })
                        const download = Forge.build({ dom: 'span', class: 'auvicon-line-download icon-button todo-download-button todo-icon-hover' }) // style: 'color: #707070; font-size: 22px; margin: 0 8px; vertical-align: super; cursor: pointer;' })
                        let currentFile = c.currentEngagement.files.find((f) => {
                            return f._id.toString() === request.currentFile.toString()
                        })

                        nameLabel.style.display = 'none'
                        fileName.innerText = currentFile ? currentFile.name : request.name
                        fileName.style.display = 'inline-block'

                        requestElement.appendChild(checkmark)
                        requestElement.appendChild(file)

                        rightIcons.appendChild(download)
                        rightIcons.appendChild(options)
                        download.addEventListener('click', (e) => {
                            if (permissions.includes(definePermission.TodoPermissions.FILE_W)) {
                                let engagementID = c.currentEngagement._id
                                if (currentFile.source) {
                                    if (currentFile.source.name === 'Google Drive') {
                                        if (currentFile.source.fid) {
                                            window.open(`${settings.protocol}://${settings.domain}:${settings.port}/googleDrive/downloadFile/${currentFile.source.fid}/${currentFile.name}`, '_blank')
                                        }
                                    }
                                } else {
                                    window.open(`${settings.protocol}://${settings.domain}:${settings.port}/downloadFile/${currentFile._id}/${engagementID}/${currentFile.name}`, '_blank')
                                }
                            }
                        })
                    } else {
                        requestElement.className = 'detReqForFile'
                        const importBtn = Forge.build({ 'dom': 'div', 'class': 'auvicon-line-cloud-upload todo-cloud-button todo-icon-hover' })
                        const add = Forge.build({ dom: 'label', for: 'todo-request-upload-' + request._id, class: 'auvicon-line-circle-add todo-circle-add todo-icon-hover' })
                        const uploadButton = document.createElement('input')
                        uploadButton.type = 'file'
                        uploadButton.style.display = 'none'
                        uploadButton.id = 'todo-request-upload-' + request._id
                        uploadButton.dataset.todo = todo._id
                        uploadButton.accept = 'image/*,application/pdf,text/plain,text/csv,application/msword,application/vnd.ms-excel,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                        add.appendChild(uploadButton)

                        rightIcons.appendChild(importBtn)
                        rightIcons.appendChild(add)

                        uploadButton.addEventListener('change', (e) => {
                            fileUpload_Selection(e.target.files, todo._id, request._id, request._id)
                        })
                        importBtn.addEventListener('click', (e) => {
                            currentRequest = request
                            let sourceOption = {
                                gdrive: (c.currentUser.type === 'CLIENT'),
                                auvCloud: true,
                                local: false
                            }
                            displaySourceModal(sourceOption)
                        })
                    }

                    options.addEventListener('click', (e) => {
                        e.target.classList.toggle('active')

                        window.addEventListener('click', () => {
                            options.classList.remove('active')
                        })

                        e.stopPropagation()
                    })

                    let detailsDropdownMenu = (request) => {
                        let menu = Forge.build({ 'dom': 'div', 'class': 'menu' })
                        let replaceItem = Forge.build({ dom: 'a', href: '#', class: 'item', text: 'Replace file' })
                        let withdrawItem = Forge.build({ dom: 'a', href: '#', class: 'item', text: 'Withdraw file' })
                        let deleteItem = Forge.build({ dom: 'a', href: '#', class: 'item', text: 'Delete request' })
                        let duplicateItem = Forge.build({ dom: 'a', href: '#', class: 'item', text: 'Copy request' })

                        replaceItem.dataset.todoId = todo._id
                        replaceItem.dataset.reqId = request._id
                        withdrawItem.dataset.todoId = todo._id
                        withdrawItem.dataset.reqId = request._id
                        deleteItem.dataset.todoId = todo._id
                        deleteItem.dataset.reqId = request._id

                        duplicateItem.dataset.todoId = todo._id
                        if (permissions.includes(definePermission.TodoPermissions.FILE_W)) {
                            if (request.currentFile) {
                                menu.appendChild(replaceItem)
                                menu.appendChild(withdrawItem)
                            }
                            replaceItem.addEventListener('click', (e) => {
                                replaceFile(e, request)
                                options.classList.remove('active')
                            })

                            withdrawItem.addEventListener('click', (e) => {
                                withdrawFile(e, request)
                                options.classList.remove('active')
                            })
                        }

                        if (permissions.includes(definePermission.TodoPermissions.REQUEST_W)) {
                            menu.appendChild(deleteItem)
                            menu.appendChild(duplicateItem)
                            deleteItem.addEventListener('click', (e) => {
                                deleteRequest(e)
                                options.classList.remove('active')
                            })

                            duplicateItem.addEventListener('click', (e) => {
                                duplicateRequest(e, request)
                                options.classList.remove('active')
                            })
                        }

                        return menu
                    }
                    options.appendChild(detailsDropdownMenu(request))

                    requestElement.appendChild(nameLabel)
                    requestElement.appendChild(progressBarContainer)
                    requestElement.appendChild(fileName)
                    requestElement.appendChild(rightIcons)

                    rightIcons.appendChild(options)
                    detailsRequestContainer.appendChild(requestElement)

                    // add input to change request name if user has REQUEST_W ("write to request") permission
                    if (permissions.includes(definePermission.TodoPermissions.REQUEST_W)) {
                        nameLabel.addEventListener('click', (e) => {
                            const nameInput = document.createElement('input')
                            nameInput.type = 'text'
                            nameInput.className = 'auv-input auv-todo-detailsInput'
                            nameInput.placeholder = 'Untitled Request'
                            nameInput.dataset.id = request._id
                            nameInput.style.verticalAlign = 'middle'
                            nameInput.style.width = request.currentFile ? '250px' : '280px'
                            if (request.name !== 'Untitled Request') {
                                nameInput.value = request.name
                            } else {
                                nameInput.value = ''
                            }

                            // hide the label
                            e.target.style.display = 'none'
                            // insertAfter
                            let parent = e.target.parentNode
                            if (parent.lastChild === e.target) {
                                // add the nameInput after the nameLabel.
                                parent.appendChild(nameInput)
                            } else {
                                // else the nameLabel has siblings, insert the nameInput between nameLabel and it's next sibling.
                                parent.insertBefore(nameInput, e.target.nextSibling)
                            }

                            nameInput.focus()
                            nameInput.select()

                            const updateRequest = (e) => {
                                let maxRequestNameSize = 100
                                if (e.target.value.trim() === '') {
                                    flashAlert.flash({ type: 'ERROR', content: 'Request name must not be empty' })
                                    e.target.focus()
                                    return false
                                }
                                if (e.target.value.trim().length > maxRequestNameSize) {
                                    flashAlert.flash({ type: 'ERROR', content: 'Request name can not have more than ' + maxRequestNameSize + ' characters' })
                                    return false
                                }

                                let oldRequest = request
                                oldRequest.name = requestOriginalName
                                todo.requests[i].name = e.target.value.trim()

                                let data = {
                                    todoId: todo._id,
                                    lastAction: 'update-request',
                                    requests: todo.requests,
                                    oldRequest: oldRequest,
                                    updatedRequest: { _id: request._id, name: e.target.value.trim() }
                                }

                                if (!_.isEqual(requestOriginalName, e.target.value.trim())) {
                                    updateTodo(data)
                                }
                            }

                            nameInput.addEventListener('keypress', (e) => {
                                if (e.key === 13 || e.keyCode === 13 || e.which === 13) {
                                    updateRequest(e)
                                }
                            })
                            nameInput.addEventListener('blur', updateRequest)
                        })
                    }
                    // TODO - this was generating error and don't know why this is included in this but leaving this code here for now.
                    // if (!permissions.includes(definePermission.TodoPermissions.COMMENT_W)) {
                    //   const todoDetails = document.getElementById('auv-todo-details')
                    //   const commentForm = document.getElementById('comment-form')
                    //   if (commentForm) {
                    //     todoDetails.removeChild(commentForm)
                    //   }
                    // }
                }
            })
            // const request = todo.requests[i]
        } else {
            const detailsNoReqImg = Forge.build({ dom: 'img', src: '../../images/icons/clipboard.png', style: 'margin: 20px auto 42px; display: block;' })
            detailsReqBox.appendChild(detailsNoReqImg)

            const detailsNoReqText = Forge.build({ dom: 'p', class: 'detNoReqText', text: 'Add new document requests within this task by clicking on the ‘Add New Request’ button above.' })
            detailsReqBox.appendChild(detailsNoReqText)
        }

        // Displays all comments on To-Do
        let unreadComments = 0
        const detailsComment = document.getElementById('todoDetailsCommentList')
        const commentNumber = document.querySelector('#auv-todo-details .commentNumber')
        detailsComment.innerHTML = ''
        const commentAry = todo.comments
        if (commentAry.length > 0) {
            for (let i = 0; i < commentAry.length; i++) {
                let dateFormat = {
                    military: false,
                    month: { type: 'integer', length: 'short' },
                    day: { append: false },
                    output: 'MM/DD/YYYY'
                }

                let senderObj = _.find(c.currentEngagement.acl, { id: commentAry[i].sender })
                let senderFullName = `${senderObj.userInfo.firstName} ${senderObj.userInfo.lastName}`
                let oneComment = Forge.build({ dom: 'div', 'class': 'todo-comment-container' })
                let senderDiv = Forge.build({ dom: 'div', style: 'display:block;' })
                let addUser = Forge.build({ dom: 'span', class: 'detCommentUser', text: senderFullName })
                let createdDate = Forge.build({ dom: 'div', class: 'detComment', text: Utility.dateProcess(commentAry[i].dateCreated, dateFormat), style: 'display: inline-block;' })
                senderDiv.appendChild(addUser)
                senderDiv.appendChild(createdDate)
                let commentDiv = Forge.build({ dom: 'div', style: 'display:block;' })
                let addComment = null
                let fileIcon = null
                if (commentAry[i].file) {
                    addComment = Forge.build({ dom: 'p', class: 'detComment comment-fileName clickable-attachment', text: commentAry[i].content })
                    addComment.dataset.fileID = commentAry[i].file
                    fileIcon = Forge.build({ dom: 'div', class: 'auvicon-line-attach comment-attachment', style: 'cursor: default; float: left; margin: 0 8px; position: static;' })
                    addComment.addEventListener('click', (evt) => {
                        let settings = c.getSettings()
                        if (evt.target.dataset.fileID) {
                            window.open(`${settings.protocol}://${settings.domain}:${settings.port}/downloadFile/${evt.target.dataset.fileID}/${c.currentEngagement._id}/${todo.comments[i].content}`, '_blank')
                        }
                    })
                } else {
                    if (permissions.includes(definePermission.TodoPermissions.ADMIN_AUDITOR)) {
                        addComment = Forge.build({ dom: 'p', class: 'detComment', style: 'font-weight: bold;', text: commentAry[i].content })
                    } else {
                        addComment = Forge.build({ dom: 'p', class: 'detComment', text: commentAry[i].content })
                    }
                }
                if (fileIcon) {
                    commentDiv.appendChild(fileIcon)
                }
                commentDiv.appendChild(addComment)
                oneComment.appendChild(senderDiv)
                oneComment.appendChild(commentDiv)
                detailsComment.appendChild(oneComment)
                getUserList({
                    _id: { $in: _.map(commentAry, 'sender') }
                })

                if (commentAry[i].status === 'OK') {
                    unreadComments++
                }
            }
        }
        commentNumber.innerText = unreadComments
    }

    const event_updateComments = (e) => {
        // save scroll position to avoid jumping to top when new todoList rendered
        setScrollPos()

        let maxCommentSize = 512
        if (document.getElementById('comment-input').value.trim() === '') {
            return false
        }
        if (document.getElementById('comment-input').value.trim().length > maxCommentSize) {
            flashAlert.flash({ type: 'ERROR', content: 'Comment content can not have more than ' + maxCommentSize + ' characters' })
            return false
        }

        let todoID = () => {
            if (e.dataset) {
                return e.dataset.todoId
            }
            if (e.target.dataset) {
                return e.target.dataset.todoId
            }
        }
        let data = {
            todoId: todoID(),
            lastAction: 'post-comment',
            newComment: {
                content: document.getElementById('comment-input').value,
                sender: c.currentUser._id,
                dateCreated: Date.now()
            }
        }

        updateTodo(data)
    }

    const addRequest = (e) => {
        // save scroll position to avoid jumping to top when new todoList rendered
        setScrollPos()
        let data = {
            todoId: e.target.dataset.todoId,
            lastAction: 'add-request',
            newRequest: { name: 'Untitled Request', currentFile: null, previousFiles: [] }
        }

        updateTodo(data)
    }

    const replaceFile = (e, request) => {
        currentRequest = request
        let sourceOption = {
            gdrive: (c.currentUser.type === 'CLIENT'),
            auvCloud: true,
            local: true
        }
        displaySourceModal(sourceOption)
    }

    const withdrawFile = (e, request) => {
        let data = {
            todoId: e.target.dataset.todoId,
            lastAction: 'withdraw-file',
            requestId: e.target.dataset.reqId,
            withdrawFile: request.currentFile
        }
        updateTodo(data)
    }

    const duplicateRequest = (e, request) => {
        e.preventDefault()

        let newRequest = { name: request.name }
        // don't copy the file. Only request should be copied.
        newRequest.currentFile = null
        newRequest.previousFiles = []

        let data = {
            todoId: e.target.dataset.todoId,
            lastAction: 'duplicate-request',
            newRequest: newRequest
        }

        updateTodo(data)
    }

    const deleteRequest = (e) => {
        let data = {
            todoId: e.target.dataset.todoId,
            lastAction: 'delete-request',
            requestDeleteId: e.target.dataset.reqId
        }

        updateTodo(data)
    }

    /**
     *  Update Mongo DB if there is a request.
     */
    var createTodo = function (data) {
        log('Adding new Todo')
        // save scroll position to avoid jumping to top when new todoList rendered
        setScrollPos()

        data.createdBy = c.currentUser._id
        data.engagementID = c.currentEngagement._id
        log(JSON.stringify(data))
        c.sendEvent({ name: 'create-todo', data: data })
    }

    var undoBulkAction = function () {
        log('undoing bulk action......')
        if (c.undoTodos.length === 0) return
        let data = {}
        data.createdBy = c.currentUser._id
        data.engagementID = c.currentEngagement._id
        data.undoTodos = c.undoTodos
        log(JSON.stringify(data))
        c.sendEvent({ name: 'undo-bulk-action', data: data })
    }

    /**
     *  Update Mongo DB if there is a request.
     */
    var updateTodo = function (data, callback) {
        log('Updating Todo...')
        // save scroll position to avoid jumping to top when new todoList rendered
        setScrollPos()

        data.currentUserID = c.currentUser._id
        data.engagementID = c.currentEngagement._id
        c.sendEvent({ name: 'update-todo', data: data })

        typeof callback !== 'function' || callback(data)
    }

    var getUserList = function (conditions) {
        log('Requesting for users list by conditions...')
        conditions.currentUserID = c.currentUser._id
        c.sendEvent({ name: 'get-user-list', data: conditions })
    }

    this.closeDetailPage = function () {
        document.getElementById('auv-todo-details').style.display = 'none'
    }

    initialize()
}

Component_ToDoList.prototype = ComponentTemplate.prototype
Component_ToDoList.prototype.constructor = ComponentTemplate

module.exports = Component_ToDoList
