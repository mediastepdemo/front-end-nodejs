'use strict'

import ComponentTemplate from '../ComponentTemplate'
import Utility from '../../../js/src/utility_c'
import Forge from '../../Forge'
import log from '../../../js/src/log'

import styles from './team.css'

var Component_Team = function (ctrl, container, owner) {
  ComponentTemplate.call(this, ctrl, container, owner)

  var parentDOM = container
  var c = ctrl
  var self = this
  var text = c.lang.teamComponent

  /*
   * Local instances of the field values
   */
  var fields = {
    name: {id: 'team-member-name', type: 'text', value: '', model: {name: 'user', field: 'name'}},
    email: {id: 'team-member-name', type: 'text', value: '', model: {name: 'user', field: 'email'}},
    role: {id: 'team-member-role', type: 'text', value: '', model: {name: 'user', field: 'role'}},
    permission: {id: 'team-member-permission', type: 'text', value: '', model: {name: 'user', field: 'permission'}}
  }

  // Pull the name of the engagement from the setup screen
  var engagementName = document.getElementById('engagement-name') ? document.getElementById('engagement-name').value : 'That'

  var engagementTitle = engagementName

  var permissionList = [ {text: c.lang.admin}, {text: c.lang.general} ]

  var teamMemberList = [{text: text.addNew}]

  // pull the data entered about the new team member
  // var newMembers = []

  /**
   * @return a DOM object
   */
  var buildContent = function (settings) {
    if (!parentDOM) {
      return
    }

    var componentBody = Forge.build({'dom': 'p', 'id': 'team-component-body', 'class': 'component-body'})
    var header = Forge.build({'dom': 'h3', 'id': 'team-component-header', 'class': 'team-header', 'text': text.header})
    var teamList = Forge.build({'dom': 'div', 'id': 'team-invitedList'})
    const selectTeam = Forge.build({dom: 'div', id: 'team-selectMember'})
    const newMember = Forge.build({dom: 'div', id: 'team-newMember'})

    var gridContainer = Forge.build({'dom': 'div', 'id': 'c-team-options', 'class': 'ui grid', 'style': 'margin: 73px auto 150px'})
    var singleMemberDiv = Forge.build({'dom': 'div', 'class': 'eight wide column', 'style': 'border-right: 1px solid #accdd0;'})
    var singleImage = Forge.build({'dom': 'img', 'src': '../../images/create-engagement/single-man.png'})
    var singleSubText = Forge.build({'dom': 'p', 'class': 'team-subtext', 'text': text.singleSub})
    var singleContinueBtn = Forge.buildBtn({
      'text': c.lang.continue,
      'id': 'team-continue-btn',
      'type': 'primary'
    }).obj

    singleContinueBtn.addEventListener('click', function () {
      self.trigger('continue')
    })
    singleMemberDiv.appendChild(singleImage)
    singleMemberDiv.appendChild(singleSubText)
    singleMemberDiv.appendChild(singleContinueBtn)
    gridContainer.appendChild(singleMemberDiv)

    var multiMemberDiv = Forge.build({'dom': 'div', 'class': 'eight wide column', 'style': 'border-left: 1px solid #accdd0;'})
    var multiImage = Forge.build({'dom': 'img', 'src': '../../images/create-engagement/three-men.png'})
    var multiSubText = Forge.build({'dom': 'p', 'class': 'team-subtext', 'text': text.multiSub})
    var multiAddBtn = Forge.buildBtn({
      'text': text.addBtn,
      'id': 'team-add-btn',
      'type': 'primary'
    }).obj

    multiAddBtn.addEventListener('click', function () {
      document.getElementById('m-ce-addBtn').style.display = ''
      document.getElementById('m-ce-cancelBtn').style.display = ''
      self.viewTeamCreation()
    })
    multiMemberDiv.appendChild(multiImage)
    multiMemberDiv.appendChild(multiSubText)
    multiMemberDiv.appendChild(multiAddBtn)
    gridContainer.appendChild(multiMemberDiv)

    componentBody.appendChild(header)
    componentBody.appendChild(teamList)
    componentBody.appendChild(selectTeam)
    componentBody.appendChild(newMember)
    componentBody.appendChild(gridContainer)
    parentDOM.appendChild(componentBody)
  }

  // change to the teamCreation screen
  this.viewTeamCreation = function () {
    let userContact = c.myContacts
    userContact.forEach((contact) => {
      if (contact.type === 'AUDITOR') {
        let contactName = ''
        let email = contact.email
        let id = contact._id
        let firstName = contact.firstName || ''
        let lastName = contact.lastName || ''
        let role = contact.jobTitle || 'No role specified.'
        if (firstName.trim() === '' && lastName.trim() === '') {
          contactName = contact.email
        } else {
          contactName = `${firstName} ${lastName} (${role})`
        }

        const found = teamMemberList.some(function (el) {
          return el.email === email
        })

        if (!found) {
          teamMemberList.push({'text': contactName, email, firstName, lastName, role, _id: id})
        }
      }
    })

    document.getElementById('c-team-options').style.display = 'none'
    var creationContainer = Forge.build({'dom': 'div', 'class': 'team-creationContainer'})
    var selectClientInput = Forge.buildInputDropdown({'label': 'Select the member you\'d like to add to your team', id: 'm-ne-contactName', 'list': teamMemberList, 'placeholder': '', width: '349px'})

    // var selectPermissionInput = Forge.buildInputDropdown({'label': 'Select the permission level you\'d like to assign to them', 'list': permissionList, 'placeholder': '', width: '349px'})
    var addBtn = Forge.buildBtn({'text': text.addBtn, 'id': 'team-addBtn', 'type': 'primary'}).obj
    addBtn.disabled = true // disabled add button on page load

    var timeInput = null
    selectClientInput.children[2].addEventListener('blur', () => {
      clearTimeout(timeInput)
      if (selectClientInput.children[2].value === teamMemberList[0].text) {
        hideCreationPages()
        viewAddNewMember()
      }

      timeInput = setTimeout(() => {
        // Remove disabled (Enable) Add button if selected value in member list
        addBtn.disabled = teamMemberList.findIndex((member) => {
          return member.text === selectClientInput.children[2].value
        }) === -1
      }, 100)
    })

    addBtn.addEventListener('click', function () {
      let newMember = {}

      let memberName = document.getElementById('m-ne-contactName').value
      for (let i = 0; i < teamMemberList.length; i++) {
        if (teamMemberList[i].text === memberName) {
          newMember.email = teamMemberList[i].email
          newMember.firstName = teamMemberList[i].text
          addInvitationBox(teamMemberList[i])
        }
      }

      c.addMultipleTeamMembers(newMember, c.currentEngagement._id)

      selectClientInput.children[2].value = ''
      this.innerHTML = text.addAnother
    })

    creationContainer.appendChild(selectClientInput)
    // creationContainer.appendChild(selectPermissionInput)
    creationContainer.appendChild(addBtn)
    document.getElementById('team-selectMember').appendChild(creationContainer)
  }

  var viewAddNewMember = function () {
    // document.getElementById('team-component-header').innerHTML = engagementTitle
    var memberContainer = Forge.build({'dom': 'div', 'id': 'c-team-member', 'class': 'team-memberContainer'})
    var memberName = Forge.buildInput({'label': text.nameLabel, 'errorText': text.nameError, 'id': 'm-ne-teamMemberName', 'placeholder': '', 'width': '349px'})
    var memberEmail = Forge.buildInput({'label': text.emailLabel, 'errorText': text.emailError, 'id': 'm-ne-teamMamberEmail', 'placeholder': '', 'width': '349px'})
    var memberSecondEmail = Forge.buildInput({'label': text.reEmailLabel, 'errorText': text.reEmailError, 'id': 'm-ne-teamMamberEmailSecond', 'placeholder': '', 'width': '349px'})
    var memberRole = Forge.buildInput({'label': text.roleLabel, 'errorText': text.roleError, 'id': 'm-ne-teamMemberRole', 'placeholder': '', 'width': '349px'})
    // var memberPermissionInput = Forge.buildInputDropdown({'label': text.permissionLabel, 'list': permissionList, 'placeholder': '', 'width': '353px'})
    var inviteBtn = Forge.buildBtn({'text': text.inviteBtn, 'id': 'team-inviteBtn', 'type': 'primary'}).obj

    memberEmail.children[1].style.textTransform = 'lowercase'
    memberSecondEmail.children[1].style.textTransform = 'lowercase'

    memberContainer.appendChild(memberName)
    memberContainer.appendChild(memberEmail)
    memberContainer.appendChild(memberSecondEmail)
    memberContainer.appendChild(memberRole)
    // memberContainer.appendChild(memberPermissionInput)
    memberContainer.appendChild(inviteBtn)

    var timeMemberName = null
    memberName.children[1].onblur = memberName.children[1].onkeydown = function () {
      clearTimeout(timeMemberName)
      var context = this
      context.classList.remove('auv-error')
      timeMemberName = setTimeout(function () {
        let teamMemberName = document.getElementById('m-ne-teamMemberName').value
        if (teamMemberName.trim() === '') {
          context.classList.add('auv-error')
        } else {
          context.classList.remove('auv-error')
        }
      }, 100)
    }

    var timeTeamMemberEmail = null
    memberEmail.children[1].onblur = memberEmail.children[1].onkeydown = function () {
      clearTimeout(timeTeamMemberEmail)
      var context = this
      timeTeamMemberEmail = setTimeout(function () {
        let teamMemberEmail = document.getElementById('m-ne-teamMamberEmail').value
        if (teamMemberEmail === '' || !Utility.REGEX.email.test(teamMemberEmail)) {
          context.classList.add('auv-error')
        } else {
          context.classList.remove('auv-error')
        }
      }, 100)
    }

    var timeMemberSecondEmail = null
    memberSecondEmail.children[1].onblur = memberSecondEmail.children[1].onkeydown = function () {
      clearTimeout(timeMemberSecondEmail)
      var context = this
      timeMemberSecondEmail = setTimeout(function () {
        let teamMemberEmailSecond = document.getElementById('m-ne-teamMamberEmailSecond').value
        let teamMemberEmail = document.getElementById('m-ne-teamMamberEmail').value
        if (teamMemberEmailSecond === '' || teamMemberEmail.toLowerCase() !== teamMemberEmailSecond.toLowerCase()) {
          context.classList.add('auv-error')
        } else {
          context.classList.remove('auv-error')
        }
      }, 100)
    }

    var timeMemberRole = null
    memberRole.children[1].onblur = memberRole.children[1].onkeydown = function () {
      clearTimeout(timeMemberRole)
      var context = this
      context.classList.remove('auv-error')
      timeMemberRole = setTimeout(function () {
        let teamMemberRole = document.getElementById('m-ne-teamMemberRole').value
        if (teamMemberRole.trim() === '' || !Utility.REGEX.alphanumericName.test(teamMemberRole)) {
          context.classList.add('auv-error')
        } else {
          context.classList.remove('auv-error')
        }
      }, 100)
    }

    inviteBtn.addEventListener('click', function () {
      if (checkTeamInputs()) {
        return
      }

      const newMember = {}
      let memberName = document.getElementById('m-ne-teamMemberName').value
      let nameArray = (memberName).split(' ')
      let fName = ''
      let lName = ''

      // all middle name will be included as first name.
      // last name in the nameArray will be the last name.
      if (nameArray.length > 2) {
        for (let i = 0; i < nameArray.length; i++) {
          if (i !== (nameArray.length - 1)) {
            fName += nameArray[i]
            if (i < (nameArray.length - 1)) {
              fName += ' '
            }
          } else {
            lName += nameArray[i]
          }
        }
      } else {
        fName = nameArray[0]
        lName = nameArray[1] || ''
      }

      newMember.firstName = fName
      newMember.lastName = lName
      newMember.email = document.getElementById('m-ne-teamMamberEmail').value.toLowerCase()
      newMember.role = document.getElementById('m-ne-teamMemberRole').value
      // newMembers.push(newMember)
      memberContainer.style.display = 'none'
      c.addMultipleTeamMembers(newMember, c.currentEngagement._id)
      addInvitationBox(newMember)
      addAnotherMemberBtn()
    })

    let checkTeamInputs = () => {
      let teamMemberName = document.getElementById('m-ne-teamMemberName').value
      let teamMamberEmail = document.getElementById('m-ne-teamMamberEmail').value
      let teamMamberEmailSecond = document.getElementById('m-ne-teamMamberEmailSecond').value
      let teamMemberRole = document.getElementById('m-ne-teamMemberRole').value

      let hasFailed = false

      if (teamMemberName.trim() === '') {
        document.getElementById('m-ne-teamMemberName').classList.add('auv-error')
        hasFailed = true
      } else {
        document.getElementById('m-ne-teamMemberName').classList.remove('auv-error')
      }
      if (teamMamberEmail === '' || !Utility.REGEX.email.test(teamMamberEmail)) {
        document.getElementById('m-ne-teamMamberEmail').classList.add('auv-error')
        hasFailed = true
      } else {
        document.getElementById('m-ne-teamMamberEmail').classList.remove('auv-error')
      }
      if (teamMamberEmailSecond === '' || teamMamberEmail.toLowerCase() !== teamMamberEmailSecond.toLowerCase()) {
        document.getElementById('m-ne-teamMamberEmailSecond').classList.add('auv-error')
        hasFailed = true
      } else {
        document.getElementById('m-ne-teamMamberEmailSecond').classList.remove('auv-error')
      }
      if (teamMemberRole.trim() === '' || !Utility.REGEX.alphanumericName.test(teamMemberRole)) {
        document.getElementById('m-ne-teamMemberRole').classList.add('auv-error')
        hasFailed = true
      } else {
        document.getElementById('m-ne-teamMemberRole').classList.remove('auv-error')
      }

      if (hasFailed) {
        return true
      }
    }

    document.getElementById('team-newMember').appendChild(memberContainer)

    memberName.children[1].addEventListener('blur', function () {
      fields.name.value = memberName.children[1].value
    })

    memberEmail.children[1].addEventListener('blur', function () {
      fields.email.value = memberEmail.children[1].value
    })

    memberRole.children[1].addEventListener('blur', function () {
      fields.role.value = memberRole.children[1].value
    })

    // memberPermissionInput.children[2].addEventListener('blur', function () {
    //   fields.permission.value = memberPermissionInput.children[1].value
    // })
  }

  var addInvitationBox = function (data) {
    var memberBox = Forge.build({'dom': 'div', 'class': 'team-memberBox'})
    var memberCheck = Forge.build({dom: 'span', class: 'auvicon-checkmark team-checkmark'})
    var memberInfo = Forge.build({dom: 'p', text: `${text.sentInfo} ${data.firstName} ${data.lastName} (${data.role}) ${text.at} ${data.email}`, class: 'team-boxText'})
    memberBox.appendChild(memberCheck)
    memberBox.appendChild(memberInfo)
    document.getElementById('team-invitedList').appendChild(memberBox)
  }

  var addAnotherMemberBtn = function () {
    var anotherMemberBtn = Forge.buildBtn({'text': text.addAnother, 'id': 'team-addMemberBtn', 'type': 'primary'}).obj
    anotherMemberBtn.addEventListener('click', function () {
      // document.getElementById('c-team-member').innerHTML = ''
      const myNode = document.getElementById('team-selectMember')
      while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild)
      }
      const myNode2 = document.getElementById('team-newMember')
      while (myNode2.firstChild) {
        myNode2.removeChild(myNode2.firstChild)
      }
      self.viewTeamCreation()
      anotherMemberBtn.style.display = 'none'
    })
    document.getElementById('team-component-body').appendChild(anotherMemberBtn)
  }

  var hideCreationPages = function () {
    var creationPages = document.getElementsByClassName('team-creationContainer')
    for (var i = 0; i < creationPages.length; i++) {
      creationPages[i].style.display = 'none'
    }
  }

  buildContent()
}

Component_Team.prototype = ComponentTemplate.prototype
Component_Team.prototype.constructor = ComponentTemplate

module.exports = Component_Team
