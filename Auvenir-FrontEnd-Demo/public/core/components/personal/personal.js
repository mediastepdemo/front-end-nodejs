'use strict'

import ComponentTemplate from '../ComponentTemplate'
import Utility from '../../../js/src/utility_c'
import Forge from '../../Forge'
import log from '../../../js/src/log'

import styles from './personal.css'

var Component_Personal = function (ctrl, container, owner) {
  ComponentTemplate.call(this, ctrl, container, owner)

  this.name = 'Personal'
  var parentDOM = container
  var c = ctrl
  var self = this
  var text = c.lang.personalComponent

  this.isComplete = function () {
    return true
  }

  /*
   * Local instances of the field values
   */
  var fields = {
    name: { id: 'personal-name', type: 'text', value: '', model: {name: 'User', field: 'name'}},
    firstName: { id: 'personal-firstName', type: 'text', value: '', model: {name: 'User', field: 'firstName'}},
    lastName: { id: 'personal-lastName', type: 'text', value: '', model: {name: 'User', field: 'lastName'}},
    jobTitle: { id: 'personal-role', type: 'text', value: '', model: {name: 'User', field: 'jobTitle'}},
    phone: { id: 'personal-phoneNumber', type: 'text', value: '', model: {name: 'User', field: 'phone'}},
    email: { id: 'personal-email', type: 'text', value: '', model: {name: 'User', field: 'email'}},
    referral: { id: 'personal-referral', type: 'text', value: '', model: {name: 'User', field: 'referral'}},
    agreements: { value: 'agreements', model: { name: 'User', field: 'agreements', agreements: [] } }
  }

  var referralList = [
    {text: text.conference},
    {text: text.press},
    {text: text.publication},
    {text: text.referral},
    {text: text.search},
    {text: text.onAd},
    {text: text.printAd},
    {text: text.assistant},
    {text: text.other} ]

  var roleList = [
    {text: text.roles.partner},
    {text: text.roles.it},
    {text: text.roles.manager},
    {text: text.roles.auditjunior},
    {text: text.roles.managingpartner},
    {text: text.roles.auditsenior},
    {text: text.roles.consultant},
    {text: text.roles.seniormanager},
    {text: text.roles.other} ]

  var roleLabel
  if (c.currentUser.type === 'AUDITOR') {
    roleLabel = text.roleFirmLabel
  } else {
    roleLabel = text.roleCompanyLabel
  }

  /**
   * @return a DOM object
   */
  var buildContent = function (settings) {
    if (!parentDOM) {
      return
    }
    // var componentContentDiv = Forge.build({'dom':'div'});
    var componentBody = Forge.build({'dom': 'p', 'id': 'component-body', 'class': 'component-body'})

    var personalTitle = Forge.build({'dom': 'h3', 'text': text.header, 'class': 'component-title'})

    var youInputContain = Forge.build({'dom': 'div', 'class': 'onB-inputContainer'})

    var nameInput = Forge.buildInput({'label': text.nameLabel, 'errorText': text.nameError, 'id': fields.name.id, 'placeholder': ''})
    var emailInput = Forge.buildInput({'label': text.emailLabel, 'errorText': text.emailError, 'id': fields.email.id, 'placeholder': '', 'maxlength': '100'})
    var reEmailInput = Forge.buildInput({'label': text.reEmailLabel, 'errorText': text.reEmailError, id: 'personal-secondEmail', 'placeholder': '', 'maxlength': '100'})
    var roleInput = Forge.buildInputDropdown({'label': roleLabel, 'id': fields.jobTitle.id, 'list': roleList})
    var phoneInput = Forge.buildInput({'label': text.phoneLabel, 'errorText': text.phoneError, 'id': fields.phone.id, 'placeholder': '', 'maxlength': '10', 'width': '226px'})
    var referralInput = Forge.buildInputDropdown({label: text.referralLabel, 'id': fields.referral.id, 'list': referralList})

    var continueBtn = Forge.buildBtn({'text': c.lang.continue, 'type': 'primary', 'margin': '32px auto 0px', 'id': 'personal-coninueBtn' }).obj
    continueBtn.disabled = true
    continueBtn.style.display = 'block'

    var agreementContainer = Forge.build({'dom': 'div', 'style': 'margin-left: 78px;'})
    var checkBox = Forge.build({'dom': 'img', 'id': 'agreement-personal', 'src': 'images/icons/checkbox-off.svg', 'style': 'cursor: pointer; margin-right: 8px'})
    var agreementText1 = Forge.build({'dom': 'label', 'text': text.firstCheckLabel, 'style': 'margin-right: 5px; font-weight: 300;'})
    var privacyPolicy = Forge.build({'dom': 'a', 'class': 'personal-terms', 'text': text.privacy, 'style': 'margin-right: 5px'})
    var agreementText2 = Forge.build({'dom': 'label', 'class': '', 'text': ` ${text.and} `, 'style': 'margin-right: 5px; font-weight: 300;'})
    var termsAndConditions = Forge.build({'dom': 'a', 'class': 'personal-terms', 'text': text.terms})

    var checkboxFlag = false
    var checkboxFlag2 = false

    var checkboxChange = function (imgDom, checked) {
      var updatedStatus

      if (imgDom) {
        if (typeof checked === 'boolean') {
          if (!checked) {
            imgDom.src = 'images/icons/checkbox-on.svg'
            updatedStatus = true
          } else {
            imgDom.src = 'images/icons/checkbox-off.svg'
            updatedStatus = false
          }
        }
      }

      return updatedStatus
    }

    /**
     * checking the checkbox and input fields validation and enable / disable the continue button.
     */
    var checkInputValidation = function () {
      // check for checkbox
      if (!checkboxFlag) {
        continueBtn.disabled = true
      } else {
        // check if auditor checkbox
        if (c.currentUser.type === 'AUDITOR') {
          if (!checkboxFlag2) {
            continueBtn.disabled = true
            return
          } else {
            continueBtn.disabled = false
          }
        }
        var checkName = document.getElementById(fields.name.id)
        var checkEmail = document.getElementById(fields.email.id)
        var checkReEmail = document.getElementById('personal-secondEmail')
        var checkRole = document.getElementById(fields.jobTitle.id)
        var checkPhone = document.getElementById(fields.phone.id)
        var checkReferral = document.getElementById(fields.referral.id)
        // check for input fields validation
        if (checkName.classList.contains('auv-error') || checkName.classList.contains('auv-validationFailed') || checkName.value === '') {
          continueBtn.disabled = true
        } else if (checkEmail.classList.contains('auv-error') || checkEmail.classList.contains('auv-validationFailed') || checkEmail.value === '') {
          continueBtn.disabled = true
        } else if (checkReEmail.classList.contains('auv-error') || checkReEmail.classList.contains('auv-validationFailed') || checkReEmail.value === '') {
          continueBtn.disabled = true
        } else if (checkRole.classList.contains('auv-error') || checkRole.classList.contains('auv-validationFailed') || checkRole.value === '') {
          continueBtn.disabled = true
        } else if (checkPhone.classList.contains('auv-error') || checkPhone.classList.contains('auv-validationFailed') || checkPhone.value === '') {
          continueBtn.disabled = true
        } else {
          continueBtn.disabled = false
        }
      }
    }

    checkBox.addEventListener('click', function () {
      checkboxFlag = checkboxChange(checkBox, checkboxFlag)
      checkInputValidation()
    })

    youInputContain.appendChild(nameInput)
    youInputContain.appendChild(emailInput)
    youInputContain.appendChild(reEmailInput)
    if (c.currentUser.type === 'AUDITOR') {
      youInputContain.appendChild(roleInput)
      youInputContain.appendChild(phoneInput)
      youInputContain.appendChild(referralInput)
    } else {
      youInputContain.appendChild(roleInput)
      youInputContain.appendChild(phoneInput)
    }
    agreementContainer.appendChild(checkBox)
    agreementContainer.appendChild(agreementText1)
    agreementContainer.appendChild(privacyPolicy)
    agreementContainer.appendChild(agreementText2)
    agreementContainer.appendChild(termsAndConditions)

    componentBody.appendChild(personalTitle)
    componentBody.appendChild(youInputContain)
    componentBody.appendChild(agreementContainer)

    parentDOM.appendChild(componentBody)
    parentDOM.appendChild(continueBtn)

    self.loadUserData()

    // Everytime it's out of focus, it will check that field's value and update DOM's value.
    emailInput.children[1].addEventListener('blur', function () {
      var emailField = document.getElementById(fields.email.id)
      if (Utility.REGEX.email.test(emailField.value)) {
        emailField.classList.remove('auv-error')
      } else {
        emailField.classList.add('auv-error')
      }
      checkInputValidation()
    })

    reEmailInput.children[1].addEventListener('blur', function () {
      var emailField = document.getElementById(fields.email.id)
      var reEmailField = document.getElementById('personal-secondEmail')
      if (emailField.value === reEmailField.value) {
        reEmailField.classList.remove('auv-error')
        fields.email.value = emailField.value
        updateToDB(fields.email)
      } else {
        reEmailField.classList.add('auv-error')
      }
      checkInputValidation()
    })

    roleInput.children[1].addEventListener('blur', function () {
      var roleField = document.getElementById(fields.jobTitle.id)
      roleField.classList.remove('auv-error')
      if (Utility.REGEX.name.test(roleField.value)) {
        roleInput.children[1].classList.remove('auv-validationFailed')
        roleInput.children[1].classList.remove('auv-error')
        fields.jobTitle.value = roleField.value
        updateToDB(fields.jobTitle)
      } else {
        roleInput.children[1].classList.add('auv-validationFailed')
        roleInput.children[1].classList.add('auv-error')
      }
      checkInputValidation()
    })

    nameInput.children[1].addEventListener('blur', function () {
      var nameField = document.getElementById(fields.name.id)
      if (Utility.REGEX.name.test(nameField.value)) {
        nameInput.children[1].classList.remove('auv-validationFailed')
        nameInput.children[1].classList.remove('auv-error')
        fields.name.value = nameField.value
        updateToDB(fields.name)
      } else {
        nameInput.children[1].classList.add('auv-validationFailed')
        nameInput.children[1].classList.add('auv-error')
      }
      checkInputValidation()
    })

    referralInput.children[2].addEventListener('blur', function () {
      var referralField = document.getElementById(fields.referral.id)
      referralField.classList.remove('auv-error')
      if (Utility.REGEX.name.test(referralField.value)) {
        fields.referral.value = referralField.value
        updateToDB(fields.referral)
      } else {
        referralField.classList.add('auv-error')
      }
      checkInputValidation()
    })

    /**
     * Each Input has its validation is passed here instead of having this function for each input
     * Vallidation shows error on blur, while on each keyup it will only attach a pass/fail class
     * that gives no user feedback. There is propbably a better way to do this though.
     */
    const setInputValidation = (element, testType, array) => {
      element.addEventListener('blur', () => {
        if (testType()) {
          array.value = element.value
          updateToDB(array)
        } else {
          element.classList.add('auv-error')
        }
        checkInputValidation()
      })
      element.addEventListener('keyup', () => {
        if (testType()) {
          element.classList.remove('auv-validationFailed')
          element.classList.remove('auv-error')
        } else {
          element.classList.add('auv-validationFailed')
        }
        checkInputValidation()
      })
    }

    /*
     * Functions containing the validation to run for each field.
     */
    const emailValidation = () => { return (Utility.REGEX.email.test(emailInput.children[1].value)) }
    const phoneValidation = () => { return (Utility.REGEX.phone.test(phoneInput.children[1].value)) }

    /*
     * Function that will add validation to fields.
     * 1st param: Element that validation is to be added to.
     * 2nd param: Function with validaion from above
     * 3rd param: JSON to be updated if value is correct
     */
    setInputValidation(emailInput.children[1], emailValidation, fields.email)
    setInputValidation(phoneInput.children[1], phoneValidation, fields.phone)

    // sending the signal that the button has been clicked to Onboarding Module.
    continueBtn.addEventListener('click', function () {
      self.trigger('move-to-next')
      updateToDB(fields.agreements)
    })

    // Displays terms of service
    termsAndConditions.addEventListener('click', function () {
      // c.displayTermsModal()
      window.open('/terms', '_blank')
    })
    // Displays privacy statement
    privacyPolicy.addEventListener('click', function () {
      // c.displayPrivacyModal()
      window.open('/privacy', '_blank')
    })

    var type = c.currentUser.type
    if (type === 'AUDITOR') {
      var confirmationContainer = Forge.build({'dom': 'div', 'style': 'margin-left: 78px; margin-top: 20px;'})
      var checkBox2 = Forge.build({'dom': 'img', 'id': 'agreement-personal-cpa', 'src': 'images/icons/checkbox-off.svg', 'style': 'cursor: pointer; '})
      var confirmationText1 = Forge.build({'dom': 'label', 'text': text.secondCheckLabel, 'style': 'font-weight: 300; margin-left: 8px;'})
      var confirmationText2 = Forge.build({'dom': 'label', 'class': '', 'text': text.secondCheckLabelTwo, 'style': 'font-weight: 300; margin-left: 30px;'})
      confirmationContainer.appendChild(checkBox2)
      confirmationContainer.appendChild(confirmationText1)
      confirmationContainer.appendChild(confirmationText2)
      componentBody.appendChild(confirmationContainer)

      checkBox2.addEventListener('click', function () {
        checkboxFlag2 = checkboxChange(checkBox2, checkboxFlag2)
        checkInputValidation()
      })
    }
  }

  // loads pre-existing user data.
  this.loadUserData = function () {
    log('Personal Component: Loading Data')

    var firstName = c.currentUser.firstName
    var lastName = c.currentUser.lastName
    var phone = c.currentUser.phone
    var email = c.currentUser.email
    var role = c.currentUser.jobTitle
    var referral = c.currentUser.referral

    if (firstName !== null || lastName !== null) {
      if (firstName !== '' || lastName !== '') {
        var name = firstName + ' ' + lastName
        name = name.trim()
        $(fields.name.id).val(name)
        document.getElementById(fields.name.id).value = name
      }
    }

    if (email !== null) {
      if (email !== '') {
        document.getElementById(fields.email.id).value = email
        document.getElementById('personal-secondEmail').value = email
      }
    }

    if (role != null) {
      if (role !== '') {
        document.getElementById(fields.jobTitle.id).value = role
      }
    }

    if (phone !== null) {
      if (phone !== '') {
        document.getElementById(fields.phone.id).value = phone
      }
    }

    if (referral !== null) {
      if (referral !== '') {
        document.getElementById(fields.referral.id).value = referral
      }
    }
  }

  /**
   *  Update Mongo DB if there is any changes.
   */
  function updateToDB (data) {
    log('Update new information based on what user typed.')
    var fieldName = data.model.field
    var json = {}
    if (fieldName === 'agreements') {
      json['agreements'] = [
        { agreementID: 'TAC-0', timeStamp: Date.now() },
        { agreementID: 'PP-0', timeStamp: Date.now() }
      ]
    } else if (fieldName === 'name') {
      let nameArray = (data.value).split(' ')
      let fName = ''
      let lName = ''

      // all middle name will be included as first name.
      // last name in the nameArray will be the last name.
      if (nameArray.length > 2) {
        for (let i = 0; i < nameArray.length; i++) {
          if (i !== (nameArray.length - 1)) {
            fName += nameArray[i]
            if (i < (nameArray.length - 1)) {
              fName += ' '
            }
          } else {
            lName += nameArray[i]
          }
        }
      } else {
        fName = nameArray[0]
        lName = nameArray[1] || ''
      }

      json['firstName'] = fName
      json['lastName'] = lName
    } else {
      json[fieldName] = data.value
    }
    json.userID = c.currentUser._id
    c.updateUser(json)
  }

  buildContent()
}

Component_Personal.prototype = ComponentTemplate.prototype
Component_Personal.prototype.constructor = ComponentTemplate

module.exports = Component_Personal
