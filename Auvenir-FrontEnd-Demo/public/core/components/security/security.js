'use strict'

import ComponentTemplate from '../ComponentTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import styles from './security.css'
/**
 * Component_Security is responsible for building Mobile Security dom object and
 * attach itself to the parent.
 * @class Component_Security
 *
 * @param: {Object} ctrl     - appController for the current user
 * @param: {Object} settings - settings for the current user
 */
var Component_Security = function (ctrl, container, owner) {
  ComponentTemplate.call(this, ctrl, container, owner)

  this.name = 'Security'
  var c = ctrl
  var self = this
  var parentDOM = container
  var text = c.lang.securityComponent
  var currentUserAcl = c.currentUser.type

  var fields = {
    name: {id: 'password', type: 'text', value: '', model: {name: 'password', field: 'password'}, validation: {min: 8, max: 255, regex: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/}}
  }

  var setOnboardingPassword = function (value) {
    c.onboardingPassword = value
  }

  var isFieldValid = function (dom) {
    // We use updateDOM to get the updated value (data has old values).
    var updateDOM = dom
    var data = fields.name
    var valid = true
    // Sets valid if field is valid
    if (data.validation) {
      if (data.validation.required) {
        valid = valid && updateDOM.value.length > 0
      }

      if (data.validation.min) {
        valid = valid && updateDOM.value.length >= data.validation.min
      }

      if (data.validation.max) {
        valid = valid && updateDOM.value.length <= data.validation.max
      }

      if (data.validation.regex) {
        var regex = new RegExp(data.validation.regex)
        valid = valid && regex.test(updateDOM.value)
      }
    } else {
      valid = true
    }

    if (valid) {
      dom.classList.remove('auv-error')
    } else {
      dom.className += ' auv-error'
    }
    return valid
  }

   /**
    * buildComponent function builds the security dom object and attach itself to the parent dom
    * @memberOf Component_Security
    */
  var buildComponent = function () {
    var componentTitle = Forge.build({'dom': 'p', 'id': 'security-title', 'class': 'component-title', 'text': text.header})
    var componentHeader = Forge.build({'dom': 'p', 'id': 'security-description', 'class': 'component-description', 'text': text.subText, style: 'width: 632px'})
    var componentBody = Forge.build({'dom': 'p', 'id': 'security-body', 'class': 'component-body'})

    var passwordContainer = Forge.build({'dom': 'div', style: 'width: 632px; margin: auto'})
    var passOne = Forge.buildInput({label: text.createPass, id: 'first-password', errorText: text.subTextError, type: 'password'})
    var passTwo = Forge.buildInput({label: text.confirmPass, id: 'second-password', errorText: 'Your passwords don\'t match.', type: 'password'})
    var continueBtn = Forge.buildBtn({'text': text.btn, 'type': 'primary', 'margin': '32px auto 0px', 'id': 'security-continueBtn'}).obj
    continueBtn.style.display = 'block'
    continueBtn.disabled = true
    passwordContainer.appendChild(passOne)
    passwordContainer.appendChild(passTwo)

    componentBody.appendChild(passwordContainer)

    parentDOM.appendChild(componentTitle)
    parentDOM.appendChild(componentHeader)
    parentDOM.appendChild(componentBody)
    parentDOM.appendChild(continueBtn)

    passOne.children[1].setAttribute('maxlength', 50)
    passOne.children[1].addEventListener('blur', function () {
      isFieldValid(this)
      checkInputValidation()
    })
    var timePassOne = null
    passOne.children[1].onkeydown = function () {
      clearTimeout(timePassOne)
      var context = this
      timePassOne = setTimeout(function () {
        isFieldValid(context)
        checkInputValidation()
      }, 400)
    }

    passTwo.children[1].setAttribute('maxlength', 50)
    passTwo.children[1].addEventListener('blur', function () {
      validatePasswordTwo()
      checkInputValidation()
    })
    var timePassTwo = null
    passTwo.children[1].onkeydown = function () {
      clearTimeout(timePassTwo)
      var context = this
      timePassTwo = setTimeout(function () {
        validatePasswordTwo()
        checkInputValidation()
      }, 400)
    }

    continueBtn.addEventListener('click', function () {
      setOnboardingPassword(passOne.children[1].value)
      self.trigger('move-to-next')
    })

    var validatePasswordTwo = function () {
      var firstPassword = document.getElementById('first-password')
      var secondPassword = document.getElementById('second-password')
      if (firstPassword.value === secondPassword.value) {
        secondPassword.classList.remove('auv-error')
      } else {
        secondPassword.className += ' auv-error'
      }
    }

    var checkInputValidation = function () {
      // check for input fields validation
      if (passOne.children[1].value === '' || passOne.children[1].classList.contains('auv-error')) {
        continueBtn.disabled = true
        return
      }
      if (passTwo.children[1].value === '' || passTwo.children[1].classList.contains('auv-error')) {
        continueBtn.disabled = true
        return
      }
      continueBtn.disabled = false
    }

    return parentDOM
  }

  buildComponent()
}

Component_Security.prototype = ComponentTemplate.prototype
Component_Security.prototype.constructor = ComponentTemplate

module.exports = Component_Security
