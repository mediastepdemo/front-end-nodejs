'use strict'

import ComponentTemplate from '../ComponentTemplate'
import Utility from '../../../js/src/utility_c'
import Forge from '../../Forge'
import log from '../../../js/src/log'

import styles from './welcome.css'

var Component_Welcome = function (ctrl, container, owner) {
  ComponentTemplate.call(this, ctrl, container, owner)

  this.name = 'Personal'
  var parentDOM = container
  var c = ctrl
  var self = this
  var text = c.lang.welcomeComponent

  this.isComplete = function () {
    return true
  }

  /* Retrieves audit information */
  var auditData = c.auditInfoOnboarding()

  var data = {
    auditorLogo: '<img class="welcome-logo" src="images/profilePhotos/default.png">',
    auditorName: 'Andrew Auditor',
    auditorFirm: 'Deloitte',
    engagementName: 'Flower Shop Audit',
    fiscalYear: '2017'
  }

  /**
   * @return a DOM object
   */
  var buildContent = function (settings) {
    if (!parentDOM) {
      return
    }
    // var componentContentDiv = Forge.build({'dom':'div'});
    var componentBody = Forge.build({'dom': 'div', 'id': 'welcome-body', 'class': 'component-body'})
    componentBody.innerHTML = `
      <h3 id="welcome-body" class="component-title">${text.header}</h3>
      <img class="welcome-envelope" src="images/illustrations/envelope.png">
      <div class="welcome-boxHolder">
        <p class="welcome-label">${text.auditorLabel}</p>
        <div class="welcome-boxDiv">
          ${data.auditorLogo}
          <p class="welcome-names"><span class="welcome-bold">${auditData.client ? auditData.client : auditData.auditor}</span> from <span class="welcome-bold">${auditData.bussiness ? auditData.bussiness : auditData.firm}</span></p>
        </div>
      </div>
      <div class="welcome-boxHolder">
        <p class="welcome-label">${text.engagementLabel}</p>
        <div class="welcome-boxDiv">
          <p class="welcome-bold welcome-engagementName">${auditData.engagement}</p>
        </div>
      </div>`

    var continueBtn = Forge.buildBtn({'text': text.getStarted, 'type': 'primary', 'margin': '23px auto 0px', 'id': 'welcome-continueBtn' }).obj
    continueBtn.style.display = 'block'

    // componentBody.appendChild(personalTitle)
    // componentBody.appendChild(envelopeImg)

    parentDOM.appendChild(componentBody)
    parentDOM.appendChild(continueBtn)

    // sending the signal that the button has been clicked to Onboarding Module.
    continueBtn.addEventListener('click', function () {
      self.trigger('move-to-next')
    })
  }

  buildContent()
}

Component_Welcome.prototype = ComponentTemplate.prototype
Component_Welcome.prototype.constructor = ComponentTemplate

module.exports = Component_Welcome
