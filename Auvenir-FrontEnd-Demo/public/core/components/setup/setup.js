'use strict'

import ComponentTemplate from '../ComponentTemplate'
import Utility from '../../../js/src/utility_c'
import Forge from '../../Forge'
import log from '../../../js/src/log'

import styles from './setup.css'

var Component_SetUp = function (ctrl, container, owner) {
  ComponentTemplate.call(this, ctrl, container, owner)

  var parentDOM = container
  var c = ctrl
  var self = this
  var text = c.lang.setupComponent

  /*
   * Local instances of the field values
   */
  var fields = {
    name: {id: 'engagement-name', type: 'text', value: '', model: {name: 'engagement', field: 'name'}},
    type: {id: 'engagement-type', type: 'text', value: '', model: {name: 'engagement', field: 'type'}},
    company: {id: 'engagement-company', type: 'text', value: '', model: {name: 'engagement', field: 'company'}},
    deadline: {id: 'engagement-deadline', type: 'text', value: '', model: {name: 'engagement', field: 'deadline'}},
    dateRangeStart: {id: 'engagement-date-range-start', type: 'text', value: '', model: {name: 'engagement', field: 'dateRangeStart'}},
    dateRangeEnd: {id: 'engagement-date-range-end', type: 'text', value: '', model: {name: 'engagement', field: 'dateRangeEnd'}}
  }

  /*
   * Bringing in the localization language
   */

  var typeList = [ {text: text.audit}, {text: c.lang.review}, {text: text.notice}, {text: text.other} ]

  /**
   * @return a DOM object
   */
  var buildContent = function (settings) {
    if (!parentDOM) {
      return
    }

    // var componentContentDiv = Forge.build({'dom':'div'});
    var componentBody = Forge.build({'dom': 'p', 'id': 'setup-component-body', 'class': 'component-body'})

    var header = Forge.build({'dom': 'h3', 'class': 'm-ce-setup-header', 'text': 'Set Up Your Engagement'})

    var inputContainer = Forge.build({'dom': 'div', 'class': 'm-ce-setup-inputContainer'})
    var nameInput = Forge.buildInput({'label': text.nameLabel, 'errorText': text.nameError, 'id': fields.name.id, 'placeholder': '', 'width': '258px'})
    var typeInput = Forge.buildInputDropdown({'label': text.typeLabel, 'list': typeList, 'id': fields.type.id, 'placeholder': '', 'width': '258px', errorText: text.typeLabelError})
    var companyInput = Forge.buildInput({'label': text.companyLabel, 'errorText': text.companyError, 'id': fields.company.id, 'placeholder': '', 'width': '258px'})

    const dateStartEndBox = Forge.build({dom: 'div', style: 'position: relative;'})
    var deadlineInput = Forge.buildInputDropdownCalendar({'label': text.deadlineLabel, 'id': fields.deadline.id, 'placeholder': text.dates, width: '160px', errorText: text.deadlineLabelError})
    deadlineInput.readOnly = true
    var dateRangeStartInput = Forge.buildInputDropdownCalendar({'label': text.dateRangeStart, 'id': fields.dateRangeStart.id, 'placeholder': text.dates, width: '160px', errorText: text.dateRangeStartError})
    dateRangeStartInput.readOnly = true
    var dash = Forge.build({dom: 'hr', class: 'm-ce-setup-hr'})
    var dateRangeEndInput = Forge.buildInputDropdownCalendar({'label': ' ', 'id': fields.dateRangeEnd.id, 'placeholder': text.dates, width: '160px'})
    dateRangeEndInput.readOnly = true
    dateRangeEndInput.style = 'top: 18px; left: 193px; position: absolute;'
    $(function () {
      $('#engagement-deadline').datepicker({minDate: 0})
      $('#engagement-date-range-start').datepicker()
      $('#engagement-date-range-end').datepicker()
    })

    var timeName = null
    nameInput.children[1].onblur = nameInput.children[1].onkeydown = function () {
      clearTimeout(timeName)
      var context = this
      timeName = setTimeout(function () {
        fields.name.value = nameInput.children[1].value
        if (fields.name.value === '' || !Utility.REGEX.alphanumericName.test(fields.name.value)) {
          context.classList.add('auv-error')
        } else {
          context.classList.remove('auv-error')
        }
      }, 100)
    }

    var timeInput = null
    typeInput.children[2].onblur = typeInput.children[2].onkeydown = function (e) {
      clearTimeout(timeInput)
      var context = this
      timeInput = setTimeout(function () {
        fields.type.value = typeInput.children[2].value
        if (fields.type.value === '' || !Utility.REGEX.businessName.test(fields.type.value)) {
          context.classList.add('auv-error')
        } else {
          context.classList.remove('auv-error')
        }
      }, 100)
    }

    var timeCompany = null
    companyInput.children[1].onblur = companyInput.children[1].onkeydown = function () {
      clearTimeout(timeCompany)
      var context = this
      timeCompany = setTimeout(function () {
        fields.company.value = companyInput.children[1].value
        if (fields.company.value === '' || !Utility.REGEX.businessName.test(fields.company.value)) {
          context.classList.add('auv-error')
        } else {
          context.classList.remove('auv-error')
        }
      }, 100)
    }

    var timeDeadline = null
    deadlineInput.children[2].onchange = deadlineInput.children[2].onkeydown = function () {
      clearTimeout(timeDeadline)
      var context = this
      context.classList.remove('auv-error')
      timeDeadline = setTimeout(function () {
        fields.deadline.value = deadlineInput.children[2].value
        if (fields.deadline.value === '' || !Utility.REGEX.date.test(fields.deadline.value) || !Utility.isDateInFuture(fields.deadline.value, true)) {
          context.classList.add('auv-error')
          if (!Utility.isDateInFuture(fields.deadline.value, true)) {
            context.nextSibling.innerHTML = c.lang.setupComponent.deadlinePastError
          } else {
            context.nextSibling.innerHTML = c.lang.setupComponent.deadlineLabelError
          }
        } else {
          context.classList.remove('auv-error')
        }
      }, 100)
    }

    var timeRangeStart = null
    dateRangeStartInput.children[2].onchange = dateRangeStartInput.children[2].onkeydown = function () {
      clearTimeout(timeRangeStart)
      var context = this
      context.classList.remove('auv-error')
      timeRangeStart = setTimeout(function () {
        fields.dateRangeStart.value = dateRangeStartInput.children[2].value
        log('fields.dateRangeStart.value: ' + fields.dateRangeStart.value)
        if (fields.dateRangeStart.value === '' || !Utility.REGEX.date.test(fields.dateRangeStart.value)) {
          context.classList.add('auv-error')
        } else {
          context.classList.remove('auv-error')
        }
      }, 100)
    }

    var timeRangeEnd = null
    dateRangeEndInput.children[2].onchange = dateRangeEndInput.children[2].onkeydown = function () {
      clearTimeout(timeRangeEnd)
      var context = this
      context.classList.remove('auv-error')
      timeRangeEnd = setTimeout(function () {
        fields.dateRangeEnd.value = dateRangeEndInput.children[2].value
        log('fields.dateRangeEnd.value: ' + fields.dateRangeEnd.value)
        if (fields.dateRangeStart.value === '' || !Utility.REGEX.date.test(fields.dateRangeEnd.value)) {
          context.classList.add('auv-error')
        } else {
          context.classList.remove('auv-error')
        }
      }, 100)
    }

    inputContainer.appendChild(nameInput)
    inputContainer.appendChild(typeInput)
    inputContainer.appendChild(companyInput)
    inputContainer.appendChild(deadlineInput)
    inputContainer.appendChild(dateStartEndBox)
    dateStartEndBox.appendChild(dateRangeStartInput)
    dateStartEndBox.appendChild(dash)
    dateStartEndBox.appendChild(dateRangeEndInput)

    componentBody.appendChild(header)
    componentBody.appendChild(inputContainer)

    parentDOM.appendChild(componentBody)
  }

  buildContent()
}

Component_SetUp.prototype = ComponentTemplate.prototype
Component_SetUp.prototype.constructor = ComponentTemplate

module.exports = Component_SetUp
