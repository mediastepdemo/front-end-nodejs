'use strict'

import ComponentTemplate from '../ComponentTemplate'
import Utility from '../../../js/src/utility_c'
import log from '../../../js/src/log'
import Forge from '../../Forge'

import styles from './business.css'

var Component_Business = function (ctrl, container, owner) {
  ComponentTemplate.call(this, ctrl, container, owner)

  this.name = 'Business'
  var parentDOM = container
  var c = ctrl
  var self = this

  var text = c.lang.businessComponent

  this.isComplete = function () {
    return true
  }

  /*
   * Local instances of the field values
  */
  var fields = {
    businessName: { id: 'business-name', type: 'text', value: '', model: {name: 'Business', field: 'name'}},
    legalNameChanged: { id: 'business-legalNameChange', type: 'boolean', value: null, model: {name: 'Business', field: 'legalNameChanged'}},
    publiclyListed: { id: 'business-publiclyListed', type: 'boolean', value: null, model: {name: 'Business', field: 'publiclyListed'}},
    overseasOps: { id: 'business-overseasOps', type: 'boolean', value: null, model: {name: 'Business', field: 'overseasOps'}},
    parentStakeholders: { id: 'business-parentStakeholders', type: 'text', value: '', model: {name: 'Business', field: 'parentStakeholders'}},
    industry: {id: 'business-industry', type: 'text', value: '', model: {name: 'Business', field: 'industry'}},
    fiscalYearEnd: { id: 'business-fiscal-year-end', type: 'date', value: '', model: {name: 'Business', field: 'fiscalYearEnd'}},
    accountingFramework: { id: 'accounting-framework', type: 'text', value: '', model: {name: 'Business', field: 'accountingFramework'}}
  }

  var frameworksList = [ {text: text.aspe}, {text: text.ifrs}, {text: text.nfpgaap}, {text: text.usgaap}, {text: text.none} ]
  /**
   * @return a DOM object
   */
  var buildContent = function (settings) {
    if (!parentDOM) {
      return
    }

    var componentContentDiv = Forge.build({'dom': 'div'})

    var businessTitle = Forge.build({'dom': 'h3', 'text': text.title, 'class': 'component-title'})

    var youInputContain = Forge.build({'dom': 'div', 'class': 'onB-inputContainer', 'style': 'margin: auto; width: 465px'})
    var businessNameInput = Forge.buildInput({'label': text.nameInput, 'errorText': 'Not a valid business name.', 'id': fields.businessName.id, 'placeholder': 'Name of your company'})

    var checkboxContainer = Forge.build({dom: 'div'})
    checkboxContainer.innerHTML = `
      <div id=${fields.legalNameChanged} class="ui checkbox business-checkbox">
        <input type="checkbox" name="example">
        <label>${text.nameChangeCheckbox}</label>
      </div>
      <div id=${fields.publiclyListed} class="ui checkbox business-checkbox">
        <input type="checkbox" name="example">
        <label>${text.publiclyListedCheckbox}</label>
      </div>
      <div id=${fields.overseasOps} class="ui checkbox business-checkbox">
        <input type="checkbox" name="example">
        <label>${text.overseasCheckbox}</label>
      </div>`

    var parentInput = Forge.buildInputTextArea({ 'label': text.parentInput, 'errorText': 'Not a valid name.', 'id': fields.parentStakeholders.id, 'width': '458px'})
    var industryInput = Forge.buildInputDropdown({ 'label': text.industryInput, 'list': c.lang.industryList, errorText: 'Please select an Industry', 'id': fields.industry.id, 'placeholder': 'Please Select', 'width': '458px'})
    var fiscalYearEndInput = Forge.buildInputDropdownCalendar({'label': text.yearInput, 'id': fields.fiscalYearEnd.id, 'placeholder': 'Fiscal Year End', width: '260px'})
    var AccFrameworkInput = Forge.buildInputDropdown({'label': text.accountingFramework, 'list': frameworksList, 'id': fields.accountingFramework.id, 'placeholder': 'Select your software.', 'style': 'margin-bottom:0px'})

    var continueBtn = Forge.buildBtn({'text': 'Continue', 'type': 'primary', 'margin': '48px auto 0', 'id': 'onboard-business-continue'}).obj
    continueBtn.style.display = 'block'

    youInputContain.appendChild(businessNameInput)
    youInputContain.appendChild(checkboxContainer)
    youInputContain.appendChild(parentInput)
    youInputContain.appendChild(industryInput)
    youInputContain.appendChild(fiscalYearEndInput)
    youInputContain.appendChild(AccFrameworkInput)

    componentContentDiv.appendChild(businessTitle)
    componentContentDiv.appendChild(youInputContain)
    componentContentDiv.appendChild(continueBtn)

    parentDOM.appendChild(componentContentDiv)

    self.loadUserData()

    /**
     * Each Input has its validation is passed here instead of having this function for each input
     * Vallidation shows error on blur, while on each keyup it will only attach a pass/fail class
     * that gives no user feedback. There is propbably a better way to do this though.
     */
    const setInputValidation = (element, testType, array) => {
      element.addEventListener('blur', () => {
        if (testType()) {
          continueBtn.disabled = false
          array.value = element.value
          updateToDB(array)
        } else {
          continueBtn.disabled = true
          element.classList.add('auv-error')
        }
      })
      element.addEventListener('keyup', () => {
        if (testType()) {
          continueBtn.disabled = false
          element.classList.remove('auv-error')
          element.classList.remove('auv-validationFailed')
        } else {
          continueBtn.disabled = true
          element.classList.add('auv-validationFailed')
        }
      })
      var checkName = document.getElementById(fields.businessName.id)
      var checkAccFramework = document.getElementById(fields.accountingFramework.id)
      var checkIndustry = document.getElementById(fields.industry.id)
      var checkFiscalYearEnd = document.getElementById(fields.fiscalYearEnd.id)
      // check for input fields validation
      if (checkName.classList.contains('auv-error') || checkName.classList.contains('auv-validationFailed') || checkName.value === '') {
        continueBtn.disabled = true
      } else if (checkAccFramework.classList.contains('auv-error') || checkAccFramework.classList.contains('auv-validationFailed') || checkAccFramework.value === '') {
        continueBtn.disabled = true
      } else if (checkIndustry.classList.contains('auv-error') || checkIndustry.classList.contains('auv-validationFailed') || checkIndustry.value === '') {
        continueBtn.disabled = true
      } else if (checkFiscalYearEnd.classList.contains('auv-error') || checkFiscalYearEnd.classList.contains('auv-validationFailed') || checkFiscalYearEnd.value === '') {
        continueBtn.disabled = true
      } else {
        continueBtn.disabled = false
      }
    }

    /*
     * Functions containing the validation to run for each field.
     */
    const businessNameValidation = () => { return (Utility.REGEX.businessName.test(businessNameInput.children[1].value && businessNameInput.children[1].value.trim() !== '')) }
    const AccFrameworkValidation = () => { return (Utility.REGEX.alphanumeric.test(AccFrameworkInput.children[2].value && AccFrameworkInput.children[2].value.trim() !== '')) }
    const industryValidation = () => { return (Utility.REGEX.alphanumeric.test(industryInput.children[2].value && industryInput.children[2].value.trim() !== '')) }
    // const fiscalYearEndValidation = () => { return (Utility.REGEX.alphanumeric.test(fiscalYearEndInput.children[2].value && fiscalYearEndInput.children[2].value.trim() !== '')) }

    /*
     * Function that will add validation to fields.
     * 1st param: Element that validation is to be added to.
     * 2nd param: Function with validaion from above
     * 3rd param: JSON to be updated if value is correct
     */
    setInputValidation(businessNameInput.children[1], businessNameValidation, fields.businessName)
    setInputValidation(AccFrameworkInput.children[2], AccFrameworkValidation, fields.accountingFramework)
    setInputValidation(industryInput.children[2], industryValidation, fields.industry)
    // setInputValidation(fiscalYearEndInput.children[2], fiscalYearEndValidation, fields.fiscalYearEnd)

    // sending the signal that the button has been clicked to Onboarding Module.
    document.getElementById('onboard-business-continue').addEventListener('click', function () {
      var yearField = document.getElementById(fields.fiscalYearEnd.id)
      if (yearField.value.trim() !== 'Select Date') {
        fields.fiscalYearEnd.value = yearField.value
        updateToDB(fields.fiscalYearEnd)
      }
      self.trigger('move-to-next')
    })

    // triggers the calendar
    $(function () {
      $('#business-fiscal-year-end').datepicker()
    })
  }

  // loads pre-existing user data.
  this.loadUserData = function () {
    log('Business Component: Loading Data')

    var businessName = ''
    var fiscalYearEnd = ''
    var accountingFramework = ''
    var industry = ''

    if (c.currentBusiness) {
      businessName = c.currentBusiness.name || ''
      fiscalYearEnd = c.currentBusiness.fiscalYearEnd || ''
      accountingFramework = c.currentBusiness.accountingFramework || ''
      industry = c.currentBusiness.industry || ''
    }

    if (businessName !== null) {
      if (businessName !== '') {
        if (businessName !== 'Unknown') {
          document.getElementById(fields.businessName.id).value = businessName
        }
      }
    }

    if (fiscalYearEnd !== null) {
      if (fiscalYearEnd !== '') {
        var formatDate = Utility.getFormattedDatePicker(fiscalYearEnd)
        document.getElementById(fields.fiscalYearEnd.id).value = formatDate
      }
    }

    if (accountingFramework !== null) {
      if (accountingFramework !== '') {
        document.getElementById(fields.accountingFramework.id).value = accountingFramework
      }
    }

    if (industry !== null) {
      if (industry !== '') {
        document.getElementById(fields.industry.id).value = industry
      }
    }
  }

  /**
   * Handler for Update-Business Event Response from Socket.
   *
   * @param tag: {String} Tag Name
   * @param Data: {JSON} The Result of Login Event.
   */
  var updateBusinessResponse = function (tagName, data) {
    log('> Event: update-business-response')
    log(data)
    var keys = Object.keys(data.data)
    for (var i = 0; i < keys.length; i++) {
      var key
      if (keys[i] === 'name') {
        key = 'businessName'
      } else {
        key = null
      }
      if (key && fields[key]) {
        fields[key].value = data.data[keys[i]]
        document.getElementById(fields[key].id).value = data.data[keys[i]]
      }
    }
  }

  /**
   * Setup Listener
   */
  this.setupListeners = function () {
    c.registerForEvent('update-business', 'ONBOARDBUSINESS_UPDATEBUSINESS-000', updateBusinessResponse)
  }

  /**
   *  Update Mongo DB if there is any changes.
   */
  function updateToDB (data) {
    log('Update new information based on what user typed.')
    var fieldName = data.model.field
    var json = {}
    json[fieldName] = data.value
    if (c.currentBusiness) {
      json.businessID = c.currentBusiness._id
    }

    c.sendEvent({name: 'update-business', data: json})
  }

  buildContent()
}

Component_Business.prototype = ComponentTemplate.prototype
Component_Business.prototype.constructor = ComponentTemplate

module.exports = Component_Business
