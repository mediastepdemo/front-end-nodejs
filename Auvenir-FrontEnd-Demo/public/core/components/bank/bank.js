'use strict'

import ComponentTemplate from '../ComponentTemplate'
import Forge from '../../Forge'
import styles from './bank.css'
// import '../../../../Integration-Finicity-dev/public/js/index'

var Component_Bank = function (ctrl, container, ownerObj) {
  ComponentTemplate.call(this, ctrl, container, ownerObj)

  this.name = 'Bank'
  var parentDOM = container
  var owner = ownerObj
  var c = ctrl
  var self = this
  var text = c.lang.bankComponent

  this.isComplete = function () {
    return true
  }

  var bankFields = [
    { id: 'bank-rbcBtn', name: 'RBC', src: 'images/logos/bank/rbc.png', 'imgClass': 'onboardBank-rbcImg'},
    { id: 'bank-bmoBtn', name: 'BMO', src: 'images/logos/bank/bmo.png', 'imgClass': 'onboardBank-bmoImg'},
    { id: 'bank-cibcBtn', name: 'CIBC', src: 'images/logos/bank/cibc.png', 'imgClass': 'onboardBank-cibcImg'},
    { id: 'bank-tdBtn', name: 'TD', src: 'images/logos/bank/td.png', 'imgClass': 'onboardBank-tdImg'},
    { id: 'bank-scotiabankBtn', name: 'Scotiabank', src: 'images/logos/bank/scotiabank.png', 'imgClass': 'onboardBank-scotiabankImg'},
    { id: 'bank-pcBtn', name: 'PC', src: 'images/logos/bank/pc.png', 'imgClass': 'onboardBank-pcImg'}]

  var bankList = []

    /**
     * @return a DOM object
     */
  var buildContent = function (settings) {
    if (!parentDOM) {
      return
    }
    var componentContentDiv = Forge.build({'dom': 'div'})

    var bankTitle = Forge.build({'dom': 'h3', 'text': 'Integrate with your Bank', 'class': 'component-title'})
    var bankSubTitle = Forge.build({'dom': 'p', 'text': 'We will pull recent bank statements and any other information required by your auditor', 'class': 'component-subTitle'})

    var banksSection = Forge.build({dom: 'div', id: 'bank-banksSection', style: 'margin-top: -16px'})
    var dropDownHolder = Forge.build({'dom': 'div', 'class': 'ui-widget'})
    var bankDropdown = Forge.buildInputAutoComplete({'id': 'bank-selectBankDropdown', 'label': '', 'placeholder': 'Type to select your bank', 'width': '362px', 'list': bankList, 'style': 'text-align: center; margin-top: 52px; margin-bottom:0px;'})
    dropDownHolder.appendChild(bankDropdown)

    var bankTicketHolder = Forge.build({'dom': 'div', 'id': 'bank-ticketContainer', 'class': 'onboardFinancial-ticketContainer'})

    var removeHighlight = function () {
      document.getElementById('bank-rbcBtn').classList.remove('bankTicket-highlight')
      document.getElementById('bank-bmoBtn').classList.remove('bankTicket-highlight')
      document.getElementById('bank-cibcBtn').classList.remove('bankTicket-highlight')
      document.getElementById('bank-tdBtn').classList.remove('bankTicket-highlight')
      document.getElementById('bank-scotiabankBtn').classList.remove('bankTicket-highlight')
      document.getElementById('bank-pcBtn').classList.remove('bankTicket-highlight')
    }

    for (var i = 0; i < bankFields.length; i++) {
      var ticketContainer = Forge.build({'dom': 'div', 'id': bankFields[i].id, 'class': 'onboardFinancial-selectContainer'})
      var ticketLogo = Forge.build({'dom': 'div', 'class': 'onboardFinancial-selectLogo ' + bankFields[i].imgClass})
      var ticketImg = Forge.build({'dom': 'img', 'src': bankFields[i].src, 'class': 'onboardFinancial-LogoImg'})
      var ticketTxt = Forge.build({'dom': 'div', 'text': bankFields[i].name, 'class': 'onboardFinancial-text'})
      ticketLogo.appendChild(ticketImg)
      ticketContainer.appendChild(ticketTxt)
      ticketContainer.appendChild(ticketLogo)
      bankTicketHolder.appendChild(ticketContainer)

          // adding event listener in a wrapper.
      if (typeof window.addEventListener === 'function') {
        (function (x) {
          ticketContainer.addEventListener('click', function () {
            bankDropdown.children[1].value = bankFields[x].name
            removeHighlight()
            document.getElementById(bankFields[x].id).classList.add('bankTicket-highlight')
            c.displayBankIntegrationModal()
          })
        })(i)
      }
      bankList.push(bankFields[i].name)
    }

    var btnHolder = Forge.build({dom: 'div', style: 'width: 240px; margin: 10px auto'})
    var skipBtn = Forge.buildBtn({'text': 'Skip', 'type': 'light', 'width': '102px', 'margin': '0px 36px 0px 0px'}).obj
    var integrateBtn = Forge.buildBtn({'text': 'Integrate', 'type': 'primary', 'width': '102px'}).obj
    skipBtn.style.display = 'inline'
    integrateBtn.style.display = 'inline'
    // sending the signal that the button has been clicked to Onboarding Module.
    skipBtn.addEventListener('click', function () {
      self.trigger('move-to-next')
    })

    integrateBtn.addEventListener('click', function () {
      self.trigger('open-finicity')
    })

    btnHolder.appendChild(skipBtn)
    btnHolder.appendChild(integrateBtn)

    var FooterHolder = Forge.build({'dom': 'div', 'class': 'onboardFinancial-footHolder'})
    var FooterLine = Forge.build({'dom': 'p', 'text': 'Not ready to integrate right now?', 'class': 'onboardFinancial-footLine'})
    var FooterLine2 = Forge.build({'dom': 'p', 'text': 'You can skip and find it in your settings later.', 'class': 'onboardFinancial-footLine'})
    FooterHolder.appendChild(FooterLine)
    FooterHolder.appendChild(FooterLine2)

    var accountsSection = Forge.build({dom: 'div', id: 'bank-accountsSection', style: 'display: none; margin-top: -16px; text-align: center'})
    var accountsSubTitle = Forge.build({'dom': 'p', 'text': text.accSubTitle, 'class': 'component-subTitle'})
    var accountsSubSection = Forge.build({'dom': 'div'})
    accountsSubSection.innerHTML = `
    <table class="ui very basic table onboardBank-accountTable" id="ob-b-accountsTable">
      <thead>
        <tr class="left aligned">
          <th class="onboardBank-tableHead-left onboardBank-tableTxt">${text.bank}</th>
          <th class="onboardBank-tableHead-right onboardBank-tableTxt">${text.account}</th>
        </tr>
      </thead>
      <tbody id="w-team-tableBody">
      </tbody>
    </table>`

    var continueBtn = Forge.buildBtn({'text': c.lang.continue, 'type': 'primary', 'width': '102px', margin: '45px auto 0px'}).obj

    continueBtn.addEventListener('click', function () {
      self.trigger('move-to-next')
    })

    banksSection.appendChild(bankSubTitle)
    banksSection.appendChild(dropDownHolder)
    banksSection.appendChild(bankTicketHolder)
    banksSection.appendChild(btnHolder)
    banksSection.appendChild(FooterHolder)
    accountsSection.appendChild(accountsSubTitle)
    accountsSection.appendChild(accountsSubSection)
    accountsSection.appendChild(continueBtn)
    componentContentDiv.appendChild(bankTitle)
    componentContentDiv.appendChild(banksSection)
    componentContentDiv.appendChild(accountsSection)

    parentDOM.appendChild(componentContentDiv)
  }

  var getProtectedAccount = function (accNum) {
    var accString = accNum.toString()
    var length = accString.length
    var blur = length - 3
    var exes = accString.substr(3, blur)
    var lastThree = accString.substr(blur, length)
    var protectedNumber = ''
    for (var i = 0; i < exes.length; i++) {
      protectedNumber += 'X'
    }
    protectedNumber += lastThree
    return protectedNumber
  }

  /* Pull in the accounts that have been integrated */
  this.addAccounts = function (data, bankName) {
    var accountsSelected = data.accountsSelected
    for (var i = 0; i < accountsSelected.length; i++) {
      var protectedNumber = getProtectedAccount(data.accountsSelected[i].number)
      var accountRow = `
        <tr>
          <td class="component-subTitle onboardBank-tableTxt onboardBank-blackTxt">${bankName}</td>
          <td class="component-subTitle onboardBank-tableTxt onboardBank-blackTxt onboardBank-tableHead-right">${protectedNumber}</td>
        </tr>`
      $(accountRow).prependTo('#ob-b-accountsTable')
    }
  }
  this.showAccounts = function () {
    document.getElementById('bank-banksSection').style.display = 'none'
    document.getElementById('bank-accountsSection').style.display = 'block'
  }

  buildContent()
}

Component_Bank.prototype = ComponentTemplate.prototype
Component_Bank.prototype.constructor = ComponentTemplate

module.exports = Component_Bank
