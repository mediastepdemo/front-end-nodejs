'use strict'

import ComponentTemplate from '../ComponentTemplate'
import Forge from '../../Forge'
import Modal_Custom from '../../modals/custom/Modal_Custom'
import Utility from '../../../js/src/utility_c'
import log from '../../../js/src/log'
import styles from './files.css'

/**
 * This is Files Component for Client Onboarding.
 *
 */

var Component_Files = function (ctrl, container, ownerObj) {
  ComponentTemplate.call(this, ctrl, container, ownerObj)

  this.name = 'Files'
  var parentDOM = container
  var parentDomID = container.id // onboarding-files-container
  var c = ctrl
  var self = this
  var text = c.lang.filesComponent
  var engagementID = c.currentUser.lastOnboardedEID // TODO - currently we use this component only in the Onboarding page. This should be changed when we are changing our flow in the future.
  var gdriveFoldersMapping = []

  var compID
  do {
    compID = Utility.randomString(8, Utility.ALPHANUMERIC)
  } while (document.getElementById('component-files-' + compID))
  this.id = 'component-files-' + compID

  var deleteStorageModal
  var numOfStorage = 0
  var localSelectedFolder = []

  /**
   * when they setup an integration and disconnected by clicking trashcan icon, if there's no other integration set up,
   * it will show initial page.
   **/
  var displayOriginalPage = function () {
    document.getElementById('files-current-integration-' + compID).style.display = 'none'
    document.getElementById('files-ask-integration-' + compID).style.display = 'inherit'
    document.getElementById('files-add-another-btn').style.display = 'none'
    document.getElementById('onboard-files-continue').style.display = 'none'
    document.getElementById('files-skipBtn-' + compID).style.display = 'block'
    document.getElementById('files-cancelBtn-' + compID).style.display = 'none'
    document.getElementById('files-component-description-' + compID).style.display = 'block'
    document.getElementById('files-footer-div-' + compID).style.display = 'block'
  }

  var removeStorage = function (storageId, parentPath, folderID) {
    // eliminate the folder element in localSelectedFolder array
    if (document.getElementById('storage-type-' + storageId).innerText === 'Local') {
      for (let i = 0; i < localSelectedFolder.length; i++) {
        let files = localSelectedFolder[i]
        let tmpPathString = files[0].webkitRelativePath
        let tmpPathArray = tmpPathString.split('/')
        let tmpParentPath = tmpPathArray[0]

        if (tmpParentPath === parentPath) {
          localSelectedFolder.splice(i, 1)
          --numOfStorage
        }
      }

      let parentNode = document.getElementById('storage-trashIcon-' + storageId).parentElement
      if ($(parentNode).prev().length === 0) {
        $(parentNode).next().remove()
      }
      parentNode.remove()

      if (numOfStorage === 0) {
        displayOriginalPage()
      }
    } else if (document.getElementById('storage-type-' + storageId).innerText === 'Google Drive') {
      // server side (inactivate consumer)
      var email = document.getElementById('account-name-' + storageId).innerText
      c.gdriveManager.removeGDrive(email, folderID)
      c.gdriveManager.on('disconnect-result', (removeResult) => {
        if (removeResult.msg === 'success') {
          --numOfStorage
          document.getElementById('storage-trashIcon-' + storageId).parentElement.remove()
          gdriveFoldersMapping.forEach((element, i) => {
            if (element.domID === storageId) {
              gdriveFoldersMapping.splice(i, 1)
            }
          })
          if (numOfStorage === 0) {
            displayOriginalPage()
          }
        } else {
          self.trigger('flash', { content: removeResult.msg, type: 'ERROR' })
        }
      })
    }

    deleteStorageModal.close()
  }

  var buildModal = function () {
    // remove storage warning modal
    var deleteStorageContainer = Forge.build({ 'dom': 'div', 'id': 'files-deleteStorage-container-' + compID })
    var deleteSettings = {
      modalName: 'files-deleteStorage',
      close: { 'window': true, 'global': false },
      width: '484px',
      header: { 'show': true, 'color': '#363a3c', 'icon': 'warning' },
      position: { 'bottom': '0px' },
      title: 'Remove Folder?'
    }
    deleteStorageModal = new Modal_Custom(c, deleteSettings)

    document.getElementById(deleteStorageModal.contentID).innerHTML = ''
    document.getElementById(deleteStorageModal.contentID).appendChild(deleteStorageContainer)
  }

  this.isComplete = function () {
    return true
  }

  /**
   * @return a DOM object
   */
  var buildContent = function (settings) {
    if (!parentDOM) {
      // console.error('No parent dom exists.');
      c.displayMessage('Something went wrong while we were loading the page.')
      return
    }

    // elements for the components
    var componentTitle = Forge.build({ 'dom': 'p', 'id': 'component-title-' + compID, 'class': 'component-title', 'text': text.title })
    var componentHeader = Forge.build({ 'dom': 'p', 'id': 'files-component-description-' + compID, 'class': 'component-description', 'style': 'width: 534px;', 'text': text.subTitle })
    var componentBody = Forge.build({ 'dom': 'p', 'id': 'component-body-' + compID, 'class': 'component-body', 'style': 'text-align: center;' })
    var askIntegrationDiv = Forge.build({ 'dom': 'div', 'id': 'files-ask-integration-' + compID, 'style': 'display: inherit;' })
    var currentIntegrationDiv = Forge.build({ 'dom': 'div', 'id': 'files-current-integration-' + compID, 'style': 'display: none;' })
    var feedbackDiv = Forge.build({ dom: 'div', class: 'files-feedback-container'})
    var feedbackOne = Forge.build({ dom: 'p', text: text.feedbackOne + ' ', class: 'files-feedback' })
    var letusknow = Forge.build({ dom: 'a', text: c.lang.letusknow, class: 'files-feedback files-letusknow' })
    var feedbackTwo = Forge.build({ dom: 'p', text: ' ' + text.feedbackTwo, class: 'files-feedback' })
    feedbackDiv.appendChild(feedbackOne)
    feedbackDiv.appendChild(letusknow)
    feedbackDiv.appendChild(feedbackTwo)
    var componentFooterContainer = Forge.build({ 'dom': 'div', 'class': 'component-footerDiv', 'id': 'files-footer-div-' + compID })
    var footerText1 = Forge.build({ 'dom': 'p', 'id': 'onboard-footer-text1-' + compID, ' class': 'component-footer-text', 'text': text.footer })
    componentFooterContainer.appendChild(footerText1)
    var skipBtn = Forge.buildBtn({ 'id': 'files-skipBtn-' + compID, 'text': 'Skip', 'type': 'secondary', 'margin': '48px auto 0', padding: '0px 32px' }).obj
    if (parentDomID === 'onboarding-files-container') {
      skipBtn.innerText = 'Skip'
    } else {
      skipBtn.innerText = 'Back'
    }
    skipBtn.style.display = 'block'
    skipBtn.addEventListener('click', function () {
      if (parentDomID === 'onboarding-files-container') {
        self.trigger('move-to-next')
      } else {
        self.trigger('back-to-google')
      }
    })
    letusknow.addEventListener('click', function () {
      c.displayFileStorageModal()
    })

    /* ------------------------------------------------------------------*
    *                    Ask Integration elements                       *
    * ------------------------------------------------------------------ */
    var fileStorageContainer = Forge.build({ 'dom': 'div', 'style': 'display: inline-flex; margin-top: 48px;' })

    var googleDriveBox = Forge.build({ 'dom': 'a', 'id': 'files-google-container-' + compID, 'class': 'files-storage-container' })
    var googleLogo = Forge.build({ 'dom': 'img', 'id': 'files-googleDrive-img-' + compID, 'src': 'core/components/files/img/google-drive-logo-black.svg', 'class': 'files-storage-icon' })
    var googleText = Forge.build({ 'dom': 'div', 'text': 'Google Drive', 'class': 'files-storage-text' })
    var gdriveEmptyBtn = Forge.buildBtn({ 'type': 'primary', 'id': 'gdriveEmptyBtn', 'text': 'empty' }).obj
    gdriveEmptyBtn.style.display = 'none'
    googleDriveBox.appendChild(googleLogo)
    googleDriveBox.appendChild(googleText)
    googleDriveBox.appendChild(gdriveEmptyBtn)

    // TODO - once OneDrive works, remove 'oneDriveDisabled' in the oneDriveBox class
    var oneDriveBox = Forge.build({ 'dom': 'a', 'class': 'files-storage-container oneDriveDisabled', 'id': 'files-oneDrive-container-' + compID })
    var oneDriveLogo = Forge.build({ 'dom': 'img', 'id': 'files-oneDrive-img-' + compID, 'src': 'core/components/files/img/oneDrive-logo-black.svg', 'class': 'files-storage-icon', 'style': 'margin-left: -24px;' })
    var oneDriveText = Forge.build({ 'dom': 'div', 'text': 'One Drive', 'class': 'files-storage-text' })
    oneDriveBox.appendChild(oneDriveLogo)
    oneDriveBox.appendChild(oneDriveText)

    var localDriveBox = Forge.build({ 'dom': 'a', 'class': 'files-storage-container', 'id': 'localDrive-container-' + compID })
    var localStorageIcon = Forge.build({ 'dom': 'div', 'id': 'localStorage-icon-' + compID, 'class': 'auvicon-folder files-storage-icon', 'style': 'margin-left: -52px; padding-top: 10px;' })
    var localText = Forge.build({ 'dom': 'div', 'text': 'Local', 'class': 'files-storage-text', 'style': 'margin-top: -15px' })
    var openLocalDrive = Forge.build({ 'dom': 'input', 'id': 'files-openLocalDrive', 'type': 'file', 'style': 'display: none;' })
    openLocalDrive.webkitdirectory = true
    openLocalDrive.multiple = false
    localDriveBox.appendChild(localStorageIcon)
    localDriveBox.appendChild(localText)
    localDriveBox.appendChild(openLocalDrive)

    fileStorageContainer.appendChild(googleDriveBox)
    // fileStorageContainer.appendChild(oneDriveBox)
    fileStorageContainer.appendChild(localDriveBox)

    /* ----------------------------------------------------------------- *
    *                   Current Intgration elements                      *
    * ------------------------------------------------------------------ */
    var storageDiv = Forge.build({ 'dom': 'div', 'id': 'storage-table-div', 'class': 'storage-table-div' })
    var storageHeaderDiv = Forge.build({ 'dom': 'div', 'id': 'storage-header-div-' + compID, 'class': 'storage-header-div' })
    var storageNameHeader = Forge.build({ 'dom': 'span', 'text': 'Storage', 'style': 'width: 287px;', 'class': 'storage-cell header' })
    var accountNameHeader = Forge.build({ 'dom': 'span', 'text': 'Account Name', 'class': 'storage-cell header' })
    var folderNameHeader = Forge.build({ 'dom': 'span', 'text': 'Folder Name', 'class': 'storage-cell header' })
    var storageBodyDiv = Forge.build({ 'dom': 'div', 'id': 'storage-body-div', 'style': 'display: block; margin-left: -16px;' })
    var addAnotherBtn = Forge.buildBtn({ 'type': 'link', 'id': 'files-add-another-btn', 'text': '+ Add Another', 'margin': '24px 0;' }).obj
    addAnotherBtn.style.padding = '0'
    addAnotherBtn.style.display = 'block'
    var continueBtn = Forge.buildBtn({ 'type': 'primary', 'id': 'onboard-files-continue', 'text': 'Continue', 'margin': '78px auto 0;' }).obj
    continueBtn.style.display = 'block'
    var cancelBtn = Forge.buildBtn({ 'text': 'Cancel', 'id': 'files-cancelBtn-' + compID, 'type': 'light', 'margin': '32px auto 0' }).obj
    cancelBtn.style.display = 'none'

    storageDiv.appendChild(storageHeaderDiv)
    storageDiv.appendChild(storageBodyDiv)
    storageDiv.appendChild(addAnotherBtn)
    storageHeaderDiv.appendChild(storageNameHeader)
    storageHeaderDiv.appendChild(accountNameHeader)
    storageHeaderDiv.appendChild(folderNameHeader)

    // append everything into parentDOM
    parentDOM.appendChild(componentTitle)
    parentDOM.appendChild(componentBody)

    // append steps
    componentBody.appendChild(currentIntegrationDiv)
    componentBody.appendChild(askIntegrationDiv)
    askIntegrationDiv.appendChild(componentHeader)
    askIntegrationDiv.appendChild(fileStorageContainer)
    askIntegrationDiv.appendChild(feedbackDiv)
    askIntegrationDiv.appendChild(skipBtn)
    askIntegrationDiv.appendChild(cancelBtn)
    askIntegrationDiv.appendChild(componentFooterContainer)

    currentIntegrationDiv.appendChild(storageDiv)
    currentIntegrationDiv.appendChild(continueBtn)
  }

  /**
   * builds storage card in the storage list table.
   *
   * @param {string} storageType
   * @param {string} parentPath
   * @param {string} userEmail (Not for local drive)
   */
  var buildStorageCard = function (storageType, parentPath, userEmail, folderID) {
    do {
      var storageId = Utility.randomString(8, Utility.ALPHANUMERIC)
    } while (document.getElementById(storageId))

    var oneStorageDiv = Forge.build({ 'dom': 'div', 'id': storageId, 'style': 'display: block;' })
    var storageImgNameDiv = Forge.build({ 'dom': 'div', 'id': 'storage-img-name-div', 'style': 'width: 287px; display: inline-block;' })
    var storageImg
    var storageName
    var accountName
    if (storageType === 'Local') {
      storageImg = Forge.build({ 'dom': 'i', 'class': 'auvicon-folder storage-img' })
      storageName = Forge.build({ 'dom': 'span', 'text': storageType, 'class': 'storage-cell', 'style': 'float: left;', 'id': 'storage-type-' + storageId })
      accountName = Forge.build({ 'dom': 'span', 'text': c.currentUser.firstName + '\'s Computer', 'class': 'storage-cell' })
    } else if (storageType === 'Google Drive') {
      storageImg = Forge.build({ 'dom': 'img', 'src': 'core/components/files/img/google-drive-logo.svg', 'style': 'float:left; margin-right: 16px;' })
      storageName = Forge.build({ 'dom': 'span', 'text': storageType, 'class': 'storage-cell', 'style': 'float: left;', 'id': 'storage-type-' + storageId })
      accountName = Forge.build({ 'dom': 'span', 'text': userEmail, 'class': 'storage-cell', 'id': 'account-name-' + storageId })
    } else if (storageType === 'One Drive') {
      // TODO - to be added

    } else {
      // console.error('Wrong storage type');
      // c.displayMessage('Something went wrong while we were loading the page.');
    }

    gdriveFoldersMapping.push({ id: folderID, domID: storageId, name: parentPath })

    var folderName = Forge.build({ 'dom': 'span', 'id': 'storage-folderName-' + storageId, 'text': parentPath, 'class': 'storage-cell', 'style': 'width: 200px;' })
    var trashIcon = Forge.build({ 'dom': 'i', 'class': 'auvicon-trash storage-trashIcon', 'id': 'storage-trashIcon-' + storageId })
    if (numOfStorage > 0) {
      var hr = Forge.build({ 'dom': 'hr', 'style': 'margin-top: 15px; width: 762px; margin-left: 9px;' })
      document.getElementById('storage-body-div').appendChild(hr)
    }

    storageImgNameDiv.appendChild(storageImg)
    storageImgNameDiv.appendChild(storageName)
    oneStorageDiv.appendChild(storageImgNameDiv)
    oneStorageDiv.appendChild(accountName)
    oneStorageDiv.appendChild(folderName)
    oneStorageDiv.appendChild(trashIcon)
    document.getElementById('storage-body-div').appendChild(oneStorageDiv)

    // hide storage selection component and show add another btn & continue btn
    document.getElementById('files-ask-integration-' + compID).style.display = 'none'
    document.getElementById('files-add-another-btn').style.display = 'inherit'
    document.getElementById('onboard-files-continue').style.display = 'block'

    document.getElementById('storage-trashIcon-' + storageId).addEventListener('click', function () {
      var deleteMessageDiv = Forge.build({ 'dom': 'div', 'style': 'display: block; margin: 54px auto 0px;' })
      var deleteMessage1 = Forge.build({ 'dom': 'span', 'class': 'files-deleteStorage-msg', 'text': 'Are you sure you want to remove the folder' })
      var storageDiv = Forge.build({ 'dom': 'div', 'style': 'block', 'id': 'files-deleteStorage-div-' + compID })
      var deleteBtnGroup = Forge.build({ 'dom': 'div', 'style': 'display: block; margin: 48px auto 72px;' })
      var deleteCancelBtn = Forge.buildBtn({ 'text': 'Cancel', 'id': 'files-deleteStorage-cancelBtn', 'type': 'light', 'margin': '0 8px 0 0' }).obj
      deleteCancelBtn.style.display = 'inline-block'
      var removeBtn = Forge.buildBtn({ 'text': 'Remove', 'type': 'warning', 'id': 'files-deleteStorage-removeBtn' }).obj
      removeBtn.style.display = 'inline-block'

      deleteMessageDiv.appendChild(deleteMessage1)
      deleteBtnGroup.appendChild(deleteCancelBtn)
      deleteBtnGroup.appendChild(removeBtn)

      document.getElementById('files-deleteStorage-container-' + compID).innerHTML = ''
      document.getElementById('files-deleteStorage-container-' + compID).appendChild(deleteMessageDiv)
      document.getElementById('files-deleteStorage-container-' + compID).appendChild(storageDiv)
      document.getElementById('files-deleteStorage-container-' + compID).appendChild(deleteBtnGroup)

      removeBtn.addEventListener('click', function (e) {
        removeStorage(storageId, parentPath, folderID)
      })
      deleteCancelBtn.addEventListener('click', function () { deleteStorageModal.close() })

      if (document.getElementById('storage-type-' + storageId).innerText === 'Local') {
        var localDiv = Forge.build({ 'dom': 'div' })
        var localStorageIcon = Forge.build({ 'dom': 'div', 'id': 'localStorage-icon-' + compID, 'class': 'auvicon-folder files-storage-icon', 'style': 'border: transparent; color: #599ba1;' })
        var localText = Forge.build({ 'dom': 'div', 'id': 'remove-storage-text', 'text': document.getElementById('storage-folderName-' + storageId).innerText, 'class': 'files-storage-text', 'style': 'margin-top: -15px; color: #707070; padding-left: 8px;' })
        localDiv.appendChild(localStorageIcon)
        localDiv.appendChild(localText)
        document.getElementById('files-deleteStorage-div-' + compID).appendChild(localDiv)

        deleteStorageModal.open()
      } else if (document.getElementById('storage-type-' + storageId).innerText === 'Google Drive') {
        var gdriveDiv = Forge.build({ 'dom': 'div' })
        var googleLogo = Forge.build({ 'dom': 'img', 'src': 'core/components/files/img/google-drive-logo.svg', 'class': 'files-storage-icon', 'style': 'border: transparent;' })
        var googleText = Forge.build({ 'dom': 'div', 'id': 'remove-storage-text', 'text': document.getElementById('storage-folderName-' + storageId).innerText, 'class': 'files-storage-text', 'style': 'color: #707070; padding-left: 8px;' })
        gdriveDiv.appendChild(googleLogo)
        gdriveDiv.appendChild(googleText)
        document.getElementById('files-deleteStorage-div-' + compID).appendChild(gdriveDiv)

        deleteStorageModal.open()
      } else if (document.getElementById('storage-type-' + storageId).innerText === 'One Drive') {
        // TODO - one drive not available yet.

      }
    })
  }

  var addFilesEventListeners = function () {
    document.getElementById('files-google-container-' + compID).addEventListener('mouseover', function () {
      document.getElementById('files-googleDrive-img-' + compID).style.borderRightColor = '#599ba1'
      document.getElementById('files-googleDrive-img-' + compID).src = 'core/components/files/img/google-drive-logo.svg'
    })
    document.getElementById('files-google-container-' + compID).addEventListener('mouseleave', function () {
      document.getElementById('files-googleDrive-img-' + compID).style.borderRightColor = '#e1e1e1'
      document.getElementById('files-googleDrive-img-' + compID).src = 'core/components/files/img/google-drive-logo-black.svg'
    })
    document.getElementById('files-google-container-' + compID).addEventListener('click', function () {
      c.gdriveManager.openPicker({ option: 'FOLDER', engagementID: engagementID })
      c.gdriveManager.on('setup-result', (setupResult) => {
        if (setupResult.result === 'success') {
          if (setupResult.folder === 'multiple') {
            buildStorageCard('Google Drive', 'multiple', setupResult.email)
          } else {
            buildStorageCard('Google Drive', setupResult.folder, setupResult.email, setupResult.folderID)
          }

          numOfStorage++

          // change the askIntegrationDiv to currentIntegrationDiv here.
          document.getElementById('files-ask-integration-' + compID).style.display = 'none'
          document.getElementById('files-current-integration-' + compID).style.display = 'inherit'

          document.getElementById('progressOverlay').style.display = 'none'
        } else {
          document.getElementById('progressOverlay').style.display = 'none'
          self.trigger('flash', { content: setupResult.msg, type: 'ERROR' })
        }
      })
    })

    // TODO - uncomment the followings once oneDrive is built.
    // document.getElementById('files-oneDrive-container-' + compID).addEventListener('mouseover', function () {
    //   document.getElementById('files-oneDrive-img-' + compID).style.borderRightColor = '#599ba1'
    //   document.getElementById('files-oneDrive-img-' + compID).src = 'core/components/files/img/oneDrive-logo.svg'
    // })
    // document.getElementById('files-oneDrive-container-' + compID).addEventListener('mouseleave', function () {
    //   document.getElementById('files-oneDrive-img-' + compID).style.borderRightColor = '#e1e1e1'
    //   document.getElementById('files-oneDrive-img-' + compID).src = 'core/components/files/img/oneDrive-logo-black.svg'
    // })
    // document.getElementById('files-oneDrive-container-' + compID).addEventListener('click', function () {
    //   // TODO - currently not supported.

    // })
    document.getElementById('localDrive-container-' + compID).addEventListener('mouseover', function () {
      document.getElementById('localStorage-icon-' + compID).style.borderRightColor = '#599ba1'
      document.getElementById('localStorage-icon-' + compID).style.color = '#599ba1'
    })
    document.getElementById('localDrive-container-' + compID).addEventListener('mouseleave', function () {
      document.getElementById('localStorage-icon-' + compID).style.borderRightColor = '#e1e1e1'
      document.getElementById('localStorage-icon-' + compID).style.color = '#363a3c'
    })
    document.getElementById('localDrive-container-' + compID).addEventListener('click', function () {
      document.getElementById('files-openLocalDrive').click()
    })
    document.getElementById('files-openLocalDrive').addEventListener('change', function (evt) {
      if (evt.target.files.length > 0) { // user chose a folder.
        var pathString = evt.target.files[0].webkitRelativePath
        var pathArray = pathString.split('/')
        var parentPath = pathArray[0]
        var sameFolder = false

        // check if there's same folder selected.
        for (let i = 0; i < localSelectedFolder.length; i++) {
          let files = localSelectedFolder[i]
          let tmpPathString = files[0].webkitRelativePath
          let tmpPathArray = tmpPathString.split('/')
          let tmpParentPath = tmpPathArray[0]

          if (tmpParentPath === parentPath) {
            sameFolder = true
            self.trigger('flash', { content: 'You have selected a folder with same name. If they are different folder, please change the folder name.', type: 'ERROR' })
          }
        }
        if (!sameFolder) {
          localSelectedFolder.push(evt.target.files)
          buildStorageCard('Local', parentPath)
          numOfStorage++
        }

        document.getElementById('files-ask-integration-' + compID).style.display = 'none'
        document.getElementById('files-current-integration-' + compID).style.display = 'inherit'
        document.getElementById('files-add-another-btn').style.display = 'inherit'
        document.getElementById('onboard-files-continue').style.display = 'block'
      } else {
        // user clicked cancel btn.
        if (numOfStorage === 0) {
          displayOriginalPage()
        } else {
          document.getElementById('files-add-another-btn').style.display = 'inherit'
          document.getElementById('onboard-files-continue').style.display = 'block'
          document.getElementById('files-ask-integration-' + compID).style.display = 'none'
        }
      }
    }, false)

    // second step elements
    document.getElementById('files-add-another-btn').addEventListener('click', function () {
      document.getElementById('files-add-another-btn').style.display = 'none'
      document.getElementById('files-ask-integration-' + compID).style.display = 'inherit'
      document.getElementById('onboard-files-continue').style.display = 'none'
      document.getElementById('files-cancelBtn-' + compID).style.display = 'block'
      document.getElementById('files-component-description-' + compID).style.display = 'none'
      document.getElementById('files-ask-integration-' + compID).style.marginTop = '10px'
      document.getElementById('files-skipBtn-' + compID).style.display = 'none'
      document.getElementById('files-footer-div-' + compID).style.display = 'none'
    })

    document.getElementById('files-cancelBtn-' + compID).addEventListener('click', function () {
      document.getElementById('files-add-another-btn').style.display = 'inherit'
      document.getElementById('files-ask-integration-' + compID).style.display = 'none'
      document.getElementById('onboard-files-continue').style.display = 'block'
      document.getElementById('files-cancelBtn-' + compID).style.display = 'none'
      document.getElementById('files-component-description-' + compID).style.display = 'block'
      document.getElementById('files-ask-integration-' + compID).style.marginTop = '10px'
      document.getElementById('files-skipBtn-' + compID).style.display = 'block'
      document.getElementById('files-footer-div-' + compID).style.display = 'block'
    })

    document.getElementById('onboard-files-continue').addEventListener('click', function () {
      // Local Files Integration
      if (localSelectedFolder.length > 0) {
        let totalNumLocalFiles = 0
        let counter = 0
        let failFileCounter = 0
        let successFileCounter = 0
        // TODO - maybe in the future, we could do 'click to view' in the flash alert and show list of files that were failed.
        let failFiles = ''

        for (let k = 0; k < localSelectedFolder.length; k++) {
          let files = localSelectedFolder[k]
          totalNumLocalFiles += files.length
        }
        var localIntegrations = {}
        for (var j = 0; j < localSelectedFolder.length; j++) {
          (function (j) {
            var files = localSelectedFolder[j]
            var firstFile = files[0] || null
            if (firstFile) {
              var parentFolderName = firstFile.webkitRelativePath.split('/')[0]
              localIntegrations[parentFolderName] = { totalFiles: localSelectedFolder[j].length, counter: 0, fileIDs: [] }
            }
            for (var i = 0; i < files.length; i++) {
              document.getElementById('progressOverlay').style.display = 'inherit'

              var file = files[i]

              // .DS_Store doesn't have file type but still gets included in the array.
              // since the user can't view those files in their computer, it is not included as totalNumLocalFiles
              if (!file.type) {
                totalNumLocalFiles--
                localIntegrations[parentFolderName].totalFiles--
              } else if (!Utility.checkMimeType(file.type)) {
                counter++
                failFileCounter++
                failFiles += '\n' + file.name
                localIntegrations[parentFolderName].totalFiles--
              } else {
                counter++

                var fileUpload_Complete = function (err, data) {
                  if (err) {
                    failFileCounter++
                    failFiles += '\n' + file.name
                  } else {
                    successFileCounter++
                    log(data)
                    if (data && data.file && data.file.path) {
                      var folderName = data.file.path.split('/')[1]
                      if (localIntegrations[folderName]) {
                        localIntegrations[folderName].counter++
                        localIntegrations[folderName].fileIDs.push(data.file._id)
                        if (localIntegrations[folderName].counter && localIntegrations[folderName].counter === localIntegrations[folderName].totalFiles) {
                          self.trigger('create-activity', {
                            engagementID: engagementID,
                            operation: 'CREATE',
                            type: 'Integration',
                            original: {},
                            updated: { folderName: folderName, files: localIntegrations[folderName].fileIDs, integrationType: 'LOCAL' }
                          })
                        }
                      }
                    }
                  }
                }

                self.trigger('upload-file', {
                  file: file,
                  callback: fileUpload_Complete,
                  engagementID
                })
              }
            }
          })(j)
        }

        if (counter === totalNumLocalFiles) {
          document.getElementById('progressOverlay').style.display = 'none'
          if (failFileCounter === 0) {
            self.trigger('flash', { content: totalNumLocalFiles + ' files in Local Storage were uploaded successfully.', type: 'SUCCESS' })
          } else {
            self.trigger('flash', { content: 'Unable to upload ' + failFileCounter + ' of the ' + totalNumLocalFiles + ' files in Local Storage.', type: 'ERROR' })
          }
        }
      }

      // googleDrive Folder Integration.
      c.gdriveManager.integrateGDrive(engagementID)
      c.gdriveManager.on('integration-result', (result) => {
        if (result.status === 200) {
          if (result.code === 0) {
            self.trigger('flash', { content: result.msg, type: 'SUCCESS' })
          } else {
            self.trigger('flash', { content: result.msg, type: 'ERROR' })
          }
        } else if (result.status === 500) {
          self.trigger('flash', { content: result.msg, type: 'ERROR' })
        } else {
          self.trigger('flash', { content: 'Unknown Error status. Please try again.', type: 'ERROR' })
        }
      })

      if (parentDomID === 'settings-add-integration') {
        self.trigger('back-to-google', true)
      } else {
        self.trigger('move-to-next')
      }
    })
  }

  var responseSocketEvent = function (tagName, data) {
    log('Component_Files: response to Socket Event > add-Integration')
    log(data)
    c.currentIntegrations.gdrive.uid = data.uid
  }

  this.setupListeners = function () {
    log('Component_Files: Setting up listeners ...')
    c.registerForEvent('add-integration', 'Component_Files_001', responseSocketEvent)
  }

  buildModal()
  buildContent()
  addFilesEventListeners()
}

Component_Files.prototype = ComponentTemplate.prototype
Component_Files.prototype.constructor = ComponentTemplate

module.exports = Component_Files
