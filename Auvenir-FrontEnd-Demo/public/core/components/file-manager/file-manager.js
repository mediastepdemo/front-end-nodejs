/* global _ */
'use strict'

import ComponentTemplate from '../ComponentTemplate'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import Utility from '../../../js/src/utility_c'
import Modal_Custom from '../../modals/custom/Modal_Custom'
import styles from './file-manager.css'

/**
 * This is file manager components that display lists of files.
 */

var Component_FileManager = function (ctrl, container, ownerObj, currentEngagement) {
  ComponentTemplate.call(this, ctrl, container, ownerObj)
  var c = ctrl
  this.name = 'File Manager'
  const componentID = Utility.randomString(16, Utility.ALPHANUMERIC)
  const parentDOM = container
  const parentDomID = container.id // m-fm-table-div (modal)
  const self = this
  const text = c.lang.fileManager

  var fileNameLength = (parentDomID === 'm-fm-table-div') ? 28 : 42
  var engagementID = currentEngagement._id
  var deleteModal = null
  var downloadZipModal = null
  var fileDictionary = {} // key : file._id, value : {name: file.name, owner: file.owner, category: fileCategory, uploadedDate: file.uploadedDate, path: file.path }

  // for checkbox (bulk-action) functionality
  var checkIDs = []
  var selectedOneFile = '' // for file-manager modal.

  // for folder structure system
  var fileSystem = { path: '/', children: [] } // Children is list of File or Folder Objects.
  var currentPath // Current Path of filesystem Object.
  var currentFolderName = '/'
  var currentFiles // files that shows currently on the screen. When the user going up&down the hierarchy, the lists change. {fileID: file._id, domID: 'module-fm-row' + file._id, selectedID: fileTableRow.id, dom: fileTableRow, fileName: fileNameModifier(file)}

  // for view management
  var isRoot = true // only true when it's folder structure and no other filter is being done.
  var previousSort = { arrowDomID: null, type: null, ascending: null, headerID: null, sortedArray: [] }
  var currentFilter = {
    btnDOM: null,
    type: ''
  }
  var currentSearch = {
    keyword: ''
  }
  var docsEmpty

  const initialize = function () {
    log('FileManager_Component: Initializing ...')

    buildModal()
    buildContent()
    registerEvents()
    // loadData(currentEngagement.files, c.currentUser.type)
  }

  /**
   * Setting up File Ssytem that will be used in this Widget.
   *
   * @param: {array} fileList: the array of file list
   */
  const setupFileSystem = function (flatten) {
    log('FileManager_Component: Setting up File System')
    return new Promise((resolve, reject) => {
      if (!flatten) {
        // reset File System
        fileSystem = { path: '/', children: [] }
        let fileIDs = Object.keys(fileDictionary)

        fileIDs.forEach((fileID, i) => {
          let fileObj = fileDictionary[fileID]
          insertFileToFileSystem(fileObj)

          if (i === fileIDs.length - 1) {
            return resolve('done')
          }
        })
      } else {
        fileSystem = { path: '/', children: [] }
        let fileIDs = Object.keys(fileDictionary)

        fileIDs.forEach((fileID, i) => {
          let fileObj = fileDictionary[fileID]
          fileSystem.children.push(fileObj)

          if (i === fileIDs.length - 1) {
            return resolve('done')
          }
        })
      }
    })
  }

  /**
   * Insert File to File System.
   */
  const insertFileToFileSystem = (fileObj) => {
    // When file exists with that fileID.
    if (fileObj.path === '/' || !fileObj.path) {
      // If this object's path is '/'
      fileSystem.children.push(fileObj)
    } else {
      let strPath = ''
      let listPath = fileObj.path.split('/') // last item of listPath will be always empty string.
      let currentWorkingPath = fileSystem

      for (let j = 1; j < listPath.length - 1; j++) {
        let index = -1
        for (let k = 0; k < currentWorkingPath.children.length; k++) {
          if (currentWorkingPath.children[k].fileType === 'folder' && currentWorkingPath.children[k].name === listPath[j]) {
            index = k
            strPath = strPath + '/' + listPath[j]

            let childFiles = getChildrenFiles(currentWorkingPath.children[k])
            currentWorkingPath.children[k].owner = childFiles[0].owner
          }
        }
        if (index >= 0) {
          currentWorkingPath = currentWorkingPath.children[index]
        } else {
          let folderData = {}
          folderData.fileType = 'folder'
          folderData.name = listPath[j]
          folderData.children = []
          folderData.path = strPath + '/' + folderData.name + '/'
          strPath = folderData.path
          folderData.dateUploaded = fileObj.dateUploaded
          folderData.owner = fileObj.owner
          currentWorkingPath.children.push(folderData)
          currentWorkingPath = currentWorkingPath.children[currentWorkingPath.children.length - 1]
        }
      }
      currentWorkingPath.children.push(fileObj)
    }
  }

  /**
   * get all files of directory and sub directroies on folder.
   */
  var getChildrenFiles = function (folder) {
    var childrenFiles = []
    for (var i = 0; i < folder.children.length; i++) {
      if (folder.children[i].fileType === 'folder') {
        var childrenList = getChildrenFiles(folder.children[i])
        for (var j = 0; j < childrenList.length; j++) {
          childrenFiles.push(childrenList[j])
        }
      } else {
        childrenFiles.push(folder.children[i])
      }
    }

    return childrenFiles
  }

  /**
   * Set Current Path for Front end.
   */
  var setCurrentPathView = function (strPath) {
    var pvPath // Previous Path
    var folderPath = strPath.split('/')
    currentPath = fileSystem

    var titlePathSpan = document.getElementById('module-filemanager-titlePath')
    titlePathSpan.innerHTML = ''
    for (var i = 1; i < folderPath.length - 1; i++) {
      pvPath = currentPath
      for (var j = 0; j < pvPath.children.length; j++) {
        if (pvPath.children[j].name === folderPath[i]) {
          var pathIcon = Forge.build({'dom': 'img', 'src': 'images/icons/filePath.png', style: 'margin-left:10px;'})
          var pathTitle = Forge.build({'dom': 'span', 'text': pvPath.children[j].name, 'class': 'module-fm-folderPath', value: pvPath.children[j].path})

          pathTitle.addEventListener('click', function (evt) {
            evt.stopPropagation()
            currentFolderName = evt.target.getAttribute('value')
            previousSort = { arrowDomID: null, type: null, ascending: null, headerID: null, sortedArray: []}
            populateFileTable()
          })
          titlePathSpan.appendChild(pathIcon)
          titlePathSpan.appendChild(pathTitle)

          currentPath = pvPath.children[j]  // Set Current Path.
        }
      }
    }
  }
  // count the number of items checked in the current page.
  var countSelectedItem = function () {
    return new Promise((resolve, reject) => {
      let checkedItemCtr = 0
      let checkedItemIDs = []
      checkIDs.forEach((item, i) => {
        if (document.getElementById(item) && document.getElementById(item).checked) {
          checkedItemCtr++
          checkedItemIDs.push(item)
        }
        if (i === checkIDs.length - 1) {
          resolve({ numOfItems: checkedItemCtr, items: checkedItemIDs })
        }
      })
    })
  }

  /**
   * This function is used only for file-manager modal so that only one file is chosen.
   * @param {String} fileID
   */
  var selectOnlyOneFile = function (fileID) {
    if (selectedOneFile.length > 0) {
      document.getElementById('file-check-' + selectedOneFile + componentID).checked = false
      selectedOneFile = fileID
    } else {
      selectedOneFile = fileID
    }
  }

  var findCheckedItems = function (items) {
    return new Promise((resolve, reject) => {
      let selectedItems = []

      items.forEach((checkboxID, j) => {
        // checked if it's folder
        if (checkboxID.startsWith('folder-check-') && document.getElementById(checkboxID).checked) {
          let folderName = checkboxID.substr(13, checkboxID.length - 13 - componentID.length)

          // get selected item from currentPath and insert them into folders.
          currentPath.children.forEach((child, i) => {
            if (child.fileType === 'folder' && child.name === folderName) {
              selectedItems.push(child)
            }
          })
        } else { // it's file
          let fileID = checkboxID.substr(11, checkboxID.length - 11 - componentID.length)
          currentPath.children.forEach((child, i) => {
            if (fileID === child._id) {
              selectedItems.push(child)
            }
          })
        }
        if (j === items.length - 1) {
          resolve(selectedItems)
        }
      })
    })
  }

  /**
   * This function is responsible for downloaidng checked files
   */
  var downloadSelectedFiles = function () {
    countSelectedItem().then((result) => {
      let settings = c.getSettings()
      let { numOfItems, items } = result
      if (numOfItems === 0) {
        self.trigger('flash', { 'content': 'You have not selected any items.', type: 'ERROR' })
      } else {
        // when there's only one FILE that was checked.
        if (numOfItems === 1 && items[0].startsWith('file-check-')) {
          // extract fileID from the domID
          let fileID = items[0].substr(11, items[0].length - 11 - componentID.length)

          currentPath.children.forEach((child, i) => {
            if (!child.fileType && child._id === fileID) {
              if (child.source) { // files from integration
                if (child.source.name === 'Google Drive') {
                  if (child.source.fid) {
                    window.open(`${settings.protocol}://${settings.domain}:${settings.port}/googleDrive/downloadFile/${child.source.fid}/${child.name}`, '_blank')
                  } else {
                    self.trigger('flash', { content: `There was an error while downloading ${child.name}.`, type: 'ERROR'})
                  }
                } else {
                  self.trigger('flash', { content: `There was an error while downloading ${child.name}.`, type: 'ERROR'})
                }
              } else {
                window.open(`${settings.protocol}://${settings.domain}:${settings.port}/downloadFile/${child._id}/${engagementID}/${child.name}`, '_blank')
              }
            }
          })
        // when there's more than one file checked.
        } else if (items.length > 0) {
          findCheckedItems(items).then((result) => {
            let selectedItemsJSON = {}
            result.forEach((item, i) => {
              selectedItemsJSON['item' + i] = item

              if (i === result.length - 1) {
                let path = `/download-folder?eID=${engagementID}`
                let fd = new FormData()
                fd.append('items', JSON.stringify(selectedItemsJSON))
                let req = new XMLHttpRequest()
                req.onreadystatechange = () => {
                  if (req.readyState === 4 && req.status === 200) {
                    // show zip file ready modal
                    let downloadMessageDiv = Forge.build({ 'dom': 'div', 'style': 'display: block; margin: 20px auto 0px; width: 360px;' })
                    let pictureDiv = Forge.build({ 'dom': 'div', 'style': 'display: block; margin: 34px auto 0px;' })
                    let folderPicture = Forge.build({ 'dom': 'img', 'src': '../../../images/illustrations/box.svg', 'style': 'width: 160px;height:158px; margin-left: 16px;' })
                    let downloadMessage1 = Forge.build({ 'dom': 'span', 'class': 'files-deleteStorage-msg', 'text': 'A zip file containing your selected items are ready.' })
                    let downloadBtnGroup = Forge.build({ 'dom': 'div', 'style': 'display: block; margin: 32px auto 48px;' })
                    let downloadBtn = Forge.buildBtn({ 'text': 'Download', 'type': 'primary', 'id': 'fm-downloadBtn' }).obj
                    downloadBtn.style.display = 'inline-block'

                    pictureDiv.appendChild(folderPicture)
                    downloadMessageDiv.appendChild(downloadMessage1)
                    downloadBtnGroup.appendChild(downloadBtn)

                    $(`#${downloadZipModal.contentID}`).children()[0].innerHTML = ''
                    $(`#${downloadZipModal.contentID}`).children()[0].appendChild(pictureDiv)
                    $(`#${downloadZipModal.contentID}`).children()[0].appendChild(downloadMessageDiv)
                    $(`#${downloadZipModal.contentID}`).children()[0].appendChild(downloadBtnGroup)

                    downloadZipModal.open()

                    downloadBtn.addEventListener('click', (e) => {
                      window.open(`${settings.protocol}://${settings.domain}:${settings.port}/${JSON.parse(req.response).url}/${c.currentEngagement.name}`, '_blank')
                      downloadZipModal.close()
                    })
                  } else if (req.readyState === 4 && req.status === 500) {
                    self.trigger('flash', { content: req.response.msg, type: 'ERROR'})
                  }
                }
                req.open('POST', path, true)
                req.send(fd)
              }
            })
          })
        }
      }
    })
  }

  /**
   * This function is responsible for deleting multiple files.
   * It will trigger socket event delete-file with list of checked files
   */
  var deleteSelectedFiles = function () {
    var deleteMessageDiv = Forge.build({ 'dom': 'div', 'style': 'display: block; margin: 20px auto 0px; width: 340px;' })
    var pictureDiv = Forge.build({ 'dom': 'div', 'style': 'display: block; margin: 34px auto 0px;' })
    var folderPicture = Forge.build({ 'dom': 'img', 'src': '../../../images/illustrations/folder-with-x.svg', 'style': 'width: 160px;height:158px; margin-left: 16px;' })
    var deleteMessage1 = Forge.build({ 'dom': 'span', 'class': 'files-deleteStorage-msg', 'text': 'All the files and folders you have selected will be deleted from your engagement. Would you like to continue?' })
    var deleteBtnGroup = Forge.build({ 'dom': 'div', 'style': 'display: block; margin: 32px auto 48px;' })
    var deleteCancelBtn = Forge.buildBtn({ 'text': 'Cancel', 'id': 'files-deleteStorage-cancelBtn', 'type': 'light', 'margin': '0 8px 0 0' }).obj
    deleteCancelBtn.style.display = 'inline-block'
    var removeBtn = Forge.buildBtn({ 'text': 'Delete', 'type': 'warning', 'id': 'fm-removeBtn' }).obj
    removeBtn.style.display = 'inline-block'

    pictureDiv.appendChild(folderPicture)
    deleteMessageDiv.appendChild(deleteMessage1)
    deleteBtnGroup.appendChild(deleteCancelBtn)
    deleteBtnGroup.appendChild(removeBtn)

    $(`#${deleteModal.contentID}`).children()[0].innerHTML = ''
    $(`#${deleteModal.contentID}`).children()[0].appendChild(pictureDiv)
    $(`#${deleteModal.contentID}`).children()[0].appendChild(deleteMessageDiv)
    $(`#${deleteModal.contentID}`).children()[0].appendChild(deleteBtnGroup)

    removeBtn.addEventListener('click', function () {
      log('Deleting selected files')

      countSelectedItem().then((result) => {
        let { numOfItems, items } = result
        if (numOfItems === 0) {
          deleteModal.close()
          self.trigger('flash', { 'content': 'You have not selected any items.', type: 'ERROR' })
        } else {
          findCheckedItems(items).then((result) => {
            let deletableItems = []
            let notDeletableItems = []
            result.forEach((item, i) => {
              let todo = _.find(c.currentTodos, (todo) => {
                if (todo.requests) {
                  todo.requests.forEach((request) => {
                    return (request.currentFile && request.currentFile.toString() === item._id.toString())
                  })
                }
              })
              if (item.owner._id.toString() !== c.currentUser._id.toString()) {
                notDeletableItems.push(item)
              } else if (todo) {
                notDeletableItems.push(item)
              } else {
                deletableItems.push(item)
              }

              if (i === result.length - 1) {
                if (notDeletableItems.length > 0) {
                  self.trigger('flash', { content: 'You do not have access to all of the selected items.', type: 'ERROR' })
                }
                if (deletableItems.length > 0) {
                  let data = {}
                  data.engagementID = engagementID
                  data.selectedItems = deletableItems
                  c.sendEvent({ name: 'delete-file', data })
                  document.getElementById('progressOverlay').style.display = 'inherit'
                }
                deleteModal.close()
              }
            })
          })
        }
      })
    })
    deleteCancelBtn.addEventListener('click', function () {
      deleteModal.close()
    })
    deleteModal.open()
  }

  this.addExistingFileToRequest = function (requestID, todoID) {
    countSelectedItem().then((result) => {
      let { numOfItems, items } = result
      let originalRequest = null
      if (numOfItems === 0) {
        self.trigger('flash', { 'content': 'You have not selected any item.', type: 'ERROR' })
      } else {
        c.currentEngagement.todos.forEach((todo) => {
          if (todo.requests.length > 0) {
            todo.requests.forEach((request) => {
              if (request._id.toString() === requestID.toString()) {
                originalRequest = request
              }
            })
          }
        })
        let data = { fileID: selectedOneFile, requestID, engagementID, todoID, lastAction: 'add-existing-file', originalRequest }
        c.sendEvent({ name: 'add-existing-file', data })
      }
    })
  }

  /**
   * This function is responsible for deleting file by triggering deleteFile() function.
   * @memberof Widget_FileManager
   *
   * @param {String} tag    - The delete-file event tag to be triggered.
   * @param {Object} result - The file that was deleted is returned.
  */
  var handle_deleteFileEvent = function (tag, data) {
    log('FileManager: Event triggered for ' + tag)

    if (!data.code) { // code: 0
      let { successFiles, failFiles, engagement, activityList } = data.result
      if (failFiles.length > 0) {
        self.trigger('flash', { content: 'Some files were uploaded to a request and could not be deleted.', type: 'ERROR'})
      } else {
        self.trigger('flash', { content: 'All files were deleted successfully.', type: 'SUCCESS'})
      }

      if (successFiles.length > 0) {
        if (c.currentEngagement._id.toString() === engagement._id.toString()) {
          c.currentEngagement = engagement
          for (let j = 0; j < successFiles.length; j++) {
            c.currentEngagement.files.forEach((file, i) => {
              if (file._id.toString() === successFiles[j]._id.toString()) {
                c.currentEngagement.files[i].status = 'INACTIVE'
              }
            })
            if (j === successFiles.length - 1) {
              c.currentFiles = c.currentEngagement.files
            }
          }
        }
        successFiles.forEach((file, i) => {
          if (fileDictionary[file._id]) {
            delete fileDictionary[file._id]
          }
          if (i === successFiles.length - 1) {
            refresh()
          }
        })

        // TODO - create activity
        if (activityList.length > 0) {
          activityList.forEach((activity) => {
            c.createActivity(activity)
          })
        }
      }
    } else {
      self.trigger('flash', { content: 'There was an error while deleting the files.'})
    }
  }

  const buildModal = function () {
    log('FileManager_Component: Building Modal...')
    var deleteFilesContainer = Forge.build({ 'dom': 'div', 'id': 'delete-files-container'})
    var deleteSettings = {
      modalName: 'delete-files',
      close: { 'window': true, 'global': false },
      width: '484px',
      header: { 'show': true, 'color': '#363a3c', 'icon': 'warning' },
      position: { 'bottom': '0px' },
      title: 'Delete Files?'
    }
    deleteModal = new Modal_Custom(c, deleteSettings)

    document.getElementById(deleteModal.contentID).innerHTML = ''
    document.getElementById(deleteModal.contentID).appendChild(deleteFilesContainer)

    var downloadZipContainer = Forge.build({ 'dom': 'div', 'id': 'download-zip-container'})
    var downloadZipSettings = {
      modalName: 'download-zip',
      close: { 'window': true, 'global': false },
      width: '484px',
      header: { 'show': true, 'color': '#363a3c' },
      position: { 'bottom': '0px' },
      title: 'Ready To Download'
    }
    downloadZipModal = new Modal_Custom(c, downloadZipSettings)

    document.getElementById(downloadZipModal.contentID).innerHTML = ''
    document.getElementById(downloadZipModal.contentID).appendChild(downloadZipContainer)
  }

  /**
   * @return a DOM object
   */
  const buildContent = function () {
    log('FileManager_Component: Building Content...')
    if (!parentDOM) {
      c.displayMessage('Something went wrong while we were loading the page.')
      return
    }
    container.innerHTML = ''
    // elements for the components
    var fileManagerContent = Forge.build({'dom': 'div', 'id': 'm-fm-container-' + componentID, 'class': 'filemanager-container', 'style': 'display:inherit'})
    container.appendChild(fileManagerContent)

    var titleDiv = Forge.build({'dom': 'div', 'id': 'module-filemanager-titlediv', 'class': 'module-fm-titleDiv', 'style': 'height:72px;'})
    var widgetTitle = Forge.build({'dom': 'h4', 'id': 'module-filemanager-title', 'class': 'module-fm-title'})
    var widgetTitleText = Forge.build({'dom': 'span', id: 'module-filemanager-titleSpan', 'text': 'Files', 'style': 'cursor:pointer;'})
    var widgetTitlePath = Forge.build({ 'dom': 'span', 'id': 'module-filemanager-titlePath' })
    var hr = Forge.build({ 'dom': 'hr', 'id': 'm-fm-title-hr' })
    widgetTitle.appendChild(widgetTitleText)
    widgetTitle.appendChild(widgetTitlePath)
    titleDiv.appendChild(widgetTitle)
    titleDiv.appendChild(hr)

    widgetTitleText.addEventListener('click', function (evt) {
      currentFolderName = '/'
      previousSort = {arrowDomID: null, type: null, ascending: null, headerID: null, sortedArray: []}
      populateFileTable()
      widgetTitlePath.innerHTML = ''
    })

    docsEmpty = Forge.build({'dom': 'div', 'id': 'docsEmpty', 'style': 'display:none;'})
    var checklist = Forge.build({'dom': 'img', 'src': 'images/views/docsEmpty.svg', 'class': 'module-fm-emptyClipboard'})

    var textDocs = "You haven't added any files yet."

    var emptyText = Forge.build({'dom': 'p', 'text': textDocs, 'class': 'module-fm-emptyText'})

    docsEmpty.appendChild(checklist)
    docsEmpty.appendChild(emptyText)

    var tableContainer = Forge.build({ 'dom': 'div', 'class': 'module-fm-fileManTable', 'id': 'fileManagerTable', 'style': 'display:inherit;' })
    var tbToolHeader = Forge.build({ 'dom': 'div', 'class': 'module-fm-tbToolHeader', 'style': 'height:64px; padding: 12px 0px 0px;' })
    var table = Forge.build({ 'dom': 'table', 'id': 'wFilesTable-' + componentID, 'class': 'module-fm-table' })

    var filterDropHolder = Forge.build({dom: 'div', class: 'fm-filterDdl-holder'})
    var filterDrop = `
      <div id="fm-filter-ddl" class="ui dropdown fm-filterDdl">
        <div id="fm-filter-filterName" class="text">${text.filters}</div>
        <i class="dropdown icon fm-filterDdl-icon"></i>
        <div id="fm-filter-menu" class="menu">
          <div id="fm-filter-uploadedBy" class="item">
            ${text.uploadedBy}
            <div id="fm-filter-uploadedBy-menu" class="menu fm-filter-clear"></div>
          </div>
          <div id="fm-filter-category" class="item">
            ${text.category}
            <div id="fm-filter-category-menu" class="menu fm-filter-clear"></div>
          </div>
          <div id="fm-filter-fileType" class="item">
            ${text.fileType}
            <div id="fm-filter-fileType-menu" class="menu fm-filter-clear"></div>
          </div>
          <div id="fm-filter-undoFilter" class="item">
            ${text.undoFilter}
          </div>
        </div>
      </div>`
    filterDropHolder.innerHTML = filterDrop

    $('#module-fm-filter-ul').width(80)
    var trashIcon = Forge.build({ 'dom': 'i', 'class': 'auvicon-trash module-fm-trashIcon', 'id': 'module-fm-trashIcon' })
    var downloadIcon = Forge.build({'dom': 'i', 'class': 'auvicon-line-download module-fm-download', 'id': 'module-fm-download'})
    var searchInput = Forge.build({'dom': 'input', 'type': 'text', 'id': 'comp-fm-search', 'placeholder': 'Search...', 'class': 'auv-input', 'style': 'width:150px; height: 33px; margin:0px 10px; float:right' })
    let searchTime = null
    searchInput.onkeydown = function () {
      clearTimeout(searchTime)
      searchTime = setTimeout(() => {
        let undoFilterBtn = document.getElementById('fm-filter-undoFilter')
        // undoFilterBtn.click()
        document.getElementById('fm-filter-menu').style.display = 'none'

        let searchTerm = document.getElementById('comp-fm-search').value
        search(c.currentEngagement.files).then((searchResult) => {
          if (searchTerm.trim() === '') {
            // when user empty the search field, go back to folder view.
            loadData(searchResult, c.currentUser.type, false)
          } else if (searchResult.length === 0) {
            document.getElementById('comp-fm-tbody-' + componentID).innerHTML = ''
            // table.querySelector('tbody').innerHTML = ''
          } else {
            loadData(searchResult, c.currentUser.type, true) // load with flatten file view
          }
        })
      }, 500)
    }

    tbToolHeader.appendChild(filterDropHolder)
    if (parentDomID !== 'm-fm-table-div') {
      tbToolHeader.appendChild(trashIcon)
      tbToolHeader.appendChild(downloadIcon)
    }
    tbToolHeader.appendChild(searchInput)
    tableContainer.appendChild(tbToolHeader)
    tableContainer.appendChild(docsEmpty)
    tableContainer.appendChild(table)
    fileManagerContent.appendChild(titleDiv)
    fileManagerContent.appendChild(tableContainer)

    trashIcon.addEventListener('click', () => {
      deleteSelectedFiles()
    })
    downloadIcon.addEventListener('click', () => {
      downloadSelectedFiles()
    })
  }

  /**
   * This function finds category for the file in the engagements[i].todos[i].requests[i].currentFile
   * @param {JSON} File Object
   * @return {Promise} resolve
   */
  var findTodoAndCategory = function (file) {
    return new Promise((resolve, reject) => {
      let fileCategory = { _id: 'uncategorized', name: 'uncategorized', color: '#B5B5B5' }
      let fileTodo = ''
      if (c.currentEngagement.todos.length > 0) {
        c.currentEngagement.todos.forEach((todo) => {
          if (todo.requests.length > 0) {
            todo.requests.forEach((request) => {
              if (request.currentFile && (request.currentFile.toString() === file._id.toString())) {
                c.currentEngagement.categories.forEach((category) => {
                  if (todo.category && todo.category._id) {
                    if (category._id.toString() === todo.category._id.toString()) {
                      fileTodo = todo.name
                      fileCategory = todo.category
                    } else {
                      fileTodo = todo.name
                    }
                  } else {
                    if (todo.category && category._id.toString() === todo.category.toString()) {
                      fileTodo = todo.name
                      fileCategory = category
                    } else {
                      fileTodo = todo.name
                    }
                  }
                })
              }
            })
          }
        })
      }
      resolve({ todo: fileTodo, category: fileCategory })
    })
  }

  /**
   *  Setup local file Dictionary
   */
  var setupFileDictionary = function (files) {
    log('FileManager_Component: Setting up file dictionary ...')
    return new Promise((resolve, reject) => {
      fileDictionary = {}
      files.forEach((file, i) => {
        if (file.status === 'ACTIVE') {
          findTodoAndCategory(file).then((result) => {
            let fileCategory = result.category
            let fileTodo = result.todo
            fileDictionary[file._id] = { _id: file._id, name: file.name, owner: file.owner, todo: fileTodo, category: fileCategory, dateUploaded: file.dateUploaded, path: file.path, source: file.source, status: file.status, bucket: file.bucket }
          })
        }

        if (i === files.length - 1) {
          return resolve()
        }
      })
    })
  }

  /**
   * Refresh the Files table.
   */
  const refresh = () => {
    log('FileManager_Component: Refreshing ...')

    if (currentFilter.btnDOM) { // there was a filter view.
      currentFilter.btnDOM.click()
    } else {
      loadData(c.currentEngagement.files, c.currentUser.type, false)
    }
  }

  let findAllClientTodos = function (todo) {
    return todo.clientAssignee
  }

  /**
   * @return {Array} filteredFile
   */
  const filteredFilesByUser = function (files, userType) {
    return new Promise((resolve, reject) => {
      let userIDString = c.currentUser._id.toString()
      let filteredFiles = []
      let visibleTodosForLeadClient = _.filter(c.currentTodos, findAllClientTodos)
      let visibleTodosForGeneralClient = _.filter(visibleTodosForLeadClient, (todo) => {
        return todo.clientAssignee._id.toString() === userIDString
      })

      files.forEach((file, i) => {
        if (file.status === 'ACTIVE') {
          let addFilesInRequest = function (request) {
            if (request.currentFile && request.currentFile.toString() === file._id.toString()) {
              filteredFiles.push(file)
            }
          }
          if (userType === 'CLIENT') {
            // user is lead client
            if (userIDString === c.currentEngagement.business.keyContact._id) {
              if (file.owner.type === 'CLIENT') {
                // show all files uploaded by their client.
                filteredFiles.push(file)
              } else if (file.owner.type === 'AUDITOR') {
                // show files that are uploaded by auditor to the todos that one of their clients is assigned to
                visibleTodosForLeadClient.forEach((todo) => {
                  _.forEach(todo.requests, addFilesInRequest)
                })
              }
            } else if (userIDString === file.owner._id) {
              // user is the owner of the file
              filteredFiles.push(file)
            } else {
              // user is general client and see files that they are assigned to.
              visibleTodosForGeneralClient.forEach((todo) => {
                _.forEach(todo.requests, addFilesInRequest)
              })
            }
          } else if (userType === 'AUDITOR') {
            // user is either the owner of the file, lead auditor of the engagement
            if (userIDString === file.owner._id.toString() || userIDString === c.currentEngagement.firm.currentOwner._id.toString()) {
              filteredFiles.push(file)
            } else if (file.owner.type === 'CLIENT') {
              // grab all client files
              filteredFiles.push(file)
            }
          }
        }
        if (i === files.length - 1) {
          // filter out all the files uploaded from comments
          if (c.currentEngagement.todos.length > 0) {
            c.currentEngagement.todos.forEach((todo) => {
              if (todo.comments.length > 0) {
                todo.comments.forEach((comment) => {
                  if (comment.file) {
                    _.remove(filteredFiles, (oneFile) => {
                      return oneFile._id.toString() === comment.file.toString()
                    })
                  }
                })
              }
            })
          }
          resolve(filteredFiles)
        }
      })
    })
  }

  const loadData = this.loadData = function (files, userType, flatten) {
    log('FileManager_Component: Loading Data ...')
    checkIDs = []
    if (files.length > 0) {
      filteredFilesByUser(files, userType).then((filteredFiles) => {
        if (filteredFiles.length > 0) {
          setupFileDictionary(filteredFiles).then((resolved) => {
            setupFileSystem(flatten).then((resolved) => {
              let searchInput = document.getElementById('comp-fm-search')
              // Populating the filter dropdown
              // setup for uploadedBy filter
              let uploadedByMenu = document.getElementById('fm-filter-uploadedBy-menu')
              if (isRoot) {
                uploadedByMenu.innerHTML = ''
              }
              let uploadedByArray = []

              // populating uploadedBy filter menu
              filteredFiles.forEach((item, i) => {
                const findSameOwner = (owner) => {
                  return owner._id.toString() === item.owner._id.toString()
                }
                if (!uploadedByArray.find(findSameOwner)) {
                  if (isRoot) {
                    uploadedByArray.push(item.owner)
                    let newItem = Forge.build({dom: 'div', id: 'fm-filter-uploadedBy-' + item.owner._id, text: item.owner.firstName + ' ' + item.owner.lastName, class: 'item'})
                    uploadedByMenu.appendChild(newItem)
                    newItem.addEventListener('click', (e) => {
                      if (searchInput.value.trim() !== '') {
                        searchInput.value = ''
                      }

                      currentFilter.btnDOM = newItem
                      currentFilter.type = 'Uploaded By'
                      document.getElementById('fm-filter-filterName').innerText = `${currentFilter.type}`
                      filteredFilesByUser(c.currentEngagement.files, userType).then((filteredFiles) => {
                        if (filteredFiles.length > 0) {
                          setupFileDictionary(filteredFiles).then((resolved) => {
                            setupFileSystem(flatten).then((resolved) => {
                              filterData('uploadedBy', item.owner._id, filteredFiles).then((filterResult) => {
                                isRoot = false
                                loadData(filterResult, c.currentUser.type, true)
                              })
                            })
                          })
                        }
                      })
                    })
                  }
                }
              })

              // setup for category filter
              let categoryMenu = document.getElementById('fm-filter-category-menu')
              if (isRoot) {
                categoryMenu.innerHTML = ''
              }
              let categoryArray = c.currentEngagement.categories
              if (!categoryArray.find((category) => { return category._id === 'uncategorized' })) {
                categoryArray.push({ _id: 'uncategorized', name: 'uncategorized', color: '#B5B5B5'}) // add uncategorized option.
              }
              if (isRoot) {
                categoryArray.forEach((category) => {
                  let newCategory = Forge.build({ dom: 'div', id: 'fm-filter-category-' + category._id, text: category.name, class: 'item' })
                  categoryMenu.appendChild(newCategory)
                  newCategory.addEventListener('click', (e) => {
                    if (searchInput.value.trim() !== '') {
                      searchInput.value = ''
                    }

                    currentFilter.btnDOM = newCategory
                    currentFilter.type = 'Category'
                    document.getElementById('fm-filter-filterName').innerText = `${currentFilter.type}`
                    filteredFilesByUser(c.currentEngagement.files, userType).then((filteredFiles) => {
                      if (filteredFiles.length > 0) {
                        setupFileDictionary(filteredFiles).then((resolved) => {
                          setupFileSystem(flatten).then((resolved) => {
                            filterData('category', category._id, filteredFiles).then((filterResult) => {
                              isRoot = false
                              loadData(filterResult, c.currentUser.type, true)
                            })
                          })
                        })
                      }
                    })
                  })
                })
              }

              // setup for fileType filter
              let fileTypeMenu = document.getElementById('fm-filter-fileType-menu')
              fileTypeMenu.innerHTML = ''
              let fileTypeArray = ['Image', 'PDF', 'Word', 'Excel', 'Powerpoint', 'CSV', 'Text File']
              fileTypeArray.forEach((filetype) => {
                let newFiletype = Forge.build({ dom: 'div', id: 'fm-filter-fileType-' + filetype, text: filetype, class: 'item' })
                fileTypeMenu.appendChild(newFiletype)
                newFiletype.addEventListener('click', (e) => {
                  if (searchInput.value.trim() !== '') {
                    searchInput.value = ''
                  }

                  currentFilter.btnDOM = newFiletype
                  currentFilter.type = 'File Type'
                  document.getElementById('fm-filter-filterName').innerText = `${currentFilter.type}`
                  filteredFilesByUser(c.currentEngagement.files, userType).then((filteredFiles) => {
                    if (filteredFiles.length > 0) {
                      setupFileDictionary(filteredFiles).then((resolved) => {
                        setupFileSystem(flatten).then((resolved) => {
                          filterData('fileType', filetype, filteredFiles).then((filterResult) => {
                            isRoot = false
                            loadData(filterResult, c.currentUser.type, true)
                          })
                        })
                      })
                    }
                  })
                })
              })

              // setup for undo filter
              let undoFilter = document.getElementById('fm-filter-undoFilter')
              undoFilter.addEventListener('click', (e) => {
                document.getElementById('fm-filter-filterName').innerText = text.filters
                currentFilter.btnDOM = null
                currentFilter.type = ''
                loadData(c.currentEngagement.files, c.currentUser.type, false)
              })

              docsEmpty.style.display = 'none'
              document.getElementById('wFilesTable-' + componentID).style.display = 'table'
              populateFileTable()
            })
          })
        } else {
          docsEmpty.style.display = 'inherit'
          document.getElementById('wFilesTable-' + componentID).style.display = 'none'
        }
      })
    } else {
      docsEmpty.style.display = 'inherit'
      document.getElementById('wFilesTable-' + componentID).style.display = 'none'
    }
  }

  /**
   * This function populates content for file table.
   */
  var populateFileTable = function () {
    log('Loading files & populating file table')

    checkIDs = [] // reset the checked items when user moves to other folder.
    currentFiles = []

    var tbl = document.getElementById('wFilesTable-' + componentID)
    tbl.innerHTML = ''
    var tr = Forge.build({'dom': 'thead', 'class': 'module-fm-tableHeader'})
    var tbody = Forge.build({dom: 'tbody', id: 'comp-fm-tbody-' + componentID})
    var tdCheckHolder = Forge.build({ dom: 'td', style: 'width: 38px;' })
    var tdCheckDiv = Forge.build({dom: 'div', class: 'ui fitted checkbox', style: 'margin-top: 5px; margin-left: 19px;'})
    var tdCheckBox = Forge.build({dom: 'input', type: 'checkbox'})
    var tdCheckLabel = Forge.build({ dom: 'label' })

    tdCheckDiv.appendChild(tdCheckBox)

    tdCheckDiv.appendChild(tdCheckLabel)
    if (parentDomID !== 'm-fm-table-div') {
      tdCheckHolder.appendChild(tdCheckDiv)
    }
    tdCheckBox.addEventListener('click', function () {
      if (tdCheckBox.checked) {
        for (let i = 0; i < checkIDs.length; i++) {
          if (!document.getElementById(checkIDs[i]).disabled) {
            document.getElementById(checkIDs[i]).checked = true
          }
        }
      } else {
        for (let i = 0; i < checkIDs.length; i++) {
          document.getElementById(checkIDs[i]).checked = false
        }
      }
    })

    var tdName = Forge.build({'dom': 'td', 'text': c.lang.name, 'id': 'module-fm-thName', 'class': 'module-fm-tdHeader', style: 'width: 32%;text-align: left;'})
    var sortName = Forge.build({'dom': 'span', 'class': 'icon auvicon-sort-down', 'style': 'color:#6d6d6d;cursor:pointer;font-size:11px;margin-left:4px; margin-top:3px;', 'id': 'module-fm-thNameIcon'})
    tdName.appendChild(sortName)
    sortName.addEventListener('click', function () {
      let data = {}
      data.type = 'Name'
      data.arrowDomID = sortName.id
      data.headerID = tdName.id
      if (sortName.classList.contains('auvicon-sort-down') && (sortName.style.color === 'rgb(109, 109, 109)')) { // color:#6d6d6d
        data.ascending = true
      } else if (sortName.classList.contains('auvicon-sort-down') && (sortName.style.color === 'rgb(89, 155, 161)')) { // color: #599ba1
        data.ascending = false
      } else if (sortName.classList.contains('auvicon-sort-up')) {
        data.ascending = true
      }
      sort(data)
    })

    var tdTodo = Forge.build({'dom': 'td', 'text': text.todo, 'class': 'module-fm-tdHeader', 'id': 'module-fm-thTodo', 'style': ' width: 20%; padding-right:25px; text-align: left;'})
    var sortTodo = Forge.build({'dom': 'span', 'class': 'icon auvicon-sort-down', 'style': 'color:#6d6d6d;cursor:pointer;font-size:11px;margin-left:4px; margin-top:3px;', 'id': 'module-fm-thTodoIcon'})
    tdTodo.appendChild(sortTodo)
    sortTodo.addEventListener('click', () => {
      // if todo info is not available, don't change this?
      let data = {}
      data.type = 'Todo'
      data.arrowDomID = sortTodo.id
      data.headerID = tdTodo.id
      if (sortTodo.classList.contains('auvicon-sort-down') && (sortTodo.style.color === 'rgb(109, 109, 109)')) { // color:#6d6d6d
        data.ascending = true
      } else if (sortTodo.classList.contains('auvicon-sort-down') && (sortTodo.style.color === 'rgb(89, 155, 161)')) { // color: #599ba1
        data.ascending = false
      } else if (sortTodo.classList.contains('auvicon-sort-up')) {
        data.ascending = true
      }
      sort(data)
    })

    var tdCategory = Forge.build({'dom': 'td', 'text': text.category, 'class': 'module-fm-tdHeader', 'id': 'module-fm-thCategory', 'style': 'width:16%; padding-right:25px; text-align: left;'})
    var sortCategory = Forge.build({'dom': 'span', 'class': 'icon auvicon-sort-down', 'style': 'color:#6d6d6d;cursor:pointer;font-size:11px;margin-left:4px; margin-top:3px;', 'id': 'module-fm-thCategoryIcon'})
    tdCategory.appendChild(sortCategory)
    sortCategory.addEventListener('click', () => {
      let data = {}
      data.type = 'Category'
      data.arrowDomID = sortCategory.id
      data.headerID = tdCategory.id
      if (sortCategory.classList.contains('auvicon-sort-down') && (sortCategory.style.color === 'rgb(109, 109, 109)')) { // color:#6d6d6d
        data.ascending = true
      } else if (sortCategory.classList.contains('auvicon-sort-down') && (sortCategory.style.color === 'rgb(89, 155, 161)')) { // color: #599ba1
        data.ascending = false
      } else if (sortCategory.classList.contains('auvicon-sort-up')) {
        data.ascending = true
      }
      sort(data)
    })

    var tdDate = Forge.build({'dom': 'td', 'text': text.uploadedDate, 'class': 'module-fm-tdHeader', 'id': 'module-fm-thDate', style: 'width: 14%; text-align: left;'})
    var sortDate = Forge.build({'dom': 'span', 'class': 'icon auvicon-sort-down', 'style': 'color:#6d6d6d;cursor:pointer;font-size:11px;margin-left:4px; margin-top:3px;', 'id': 'module-fm-thDateIcon'})
    tdDate.appendChild(sortDate)
    sortDate.addEventListener('click', () => {
      let data = {}
      data.type = 'UploadedDate'
      data.arrowDomID = sortDate.id
      data.headerID = tdDate.id
      if (sortDate.classList.contains('auvicon-sort-down') && (sortDate.style.color === 'rgb(109, 109, 109)')) { // color:#6d6d6d
        data.ascending = true
      } else if (sortDate.classList.contains('auvicon-sort-down') && (sortDate.style.color === 'rgb(89, 155, 161)')) { // color: #599ba1
        data.ascending = false
      } else if (sortDate.classList.contains('auvicon-sort-up')) {
        data.ascending = true
      }
      sort(data)
    })

    var tdUploadedBy = Forge.build({ 'dom': 'td', 'text': text.uploadedBy, 'class': 'module-fm-tdHeader', 'id': 'module-fm-thUploadedBy', 'style': 'width: 14%; padding-right: 25px; text-align: left;' })
    var sortUploadedBy = Forge.build({'dom': 'span', 'class': 'icon auvicon-sort-down', 'style': 'color:#6d6d6d;cursor:pointer;font-size:11px;margin-left:4px; margin-top:3px;', 'id': 'module-fm-thUploadedByIcon'})
    tdUploadedBy.appendChild(sortUploadedBy)
    sortUploadedBy.addEventListener('click', () => {
      let data = {}
      data.type = 'UploadedBy'
      data.arrowDomID = sortUploadedBy.id
      data.headerID = tdUploadedBy.id
      if (sortUploadedBy.classList.contains('auvicon-sort-down') && (sortUploadedBy.style.color === 'rgb(109, 109, 109)')) { // color:#6d6d6d
        data.ascending = true
      } else if (sortUploadedBy.classList.contains('auvicon-sort-down') && (sortUploadedBy.style.color === 'rgb(89, 155, 161)')) { // color: #599ba1
        data.ascending = false
      } else if (sortUploadedBy.classList.contains('auvicon-sort-up')) {
        data.ascending = true
      }
      sort(data)
    })

    tr.appendChild(tdCheckHolder)
    tr.appendChild(tdName)
    tr.appendChild(tdTodo)
    tr.appendChild(tdCategory)
    tr.appendChild(tdDate)
    tr.appendChild(tdUploadedBy)
    tbl.appendChild(tr)
    tbl.appendChild(tbody)

    if (fileSystem !== null) {
      if (currentFolderName === '/' || currentFolderName === '') {
        currentPath = fileSystem
        for (let i = 0; i < currentPath.children.length; i++) {
          addFile(currentPath.children[i], i)
        }
      } else {
        setCurrentPathView(currentFolderName)

        if (currentPath) {
          for (let i = 0; i < currentPath.children.length; i++) {
            addFile(currentPath.children[i], i)
          }
        }
      }
    }
  }

  /**
   * Search Files for searching term in filemanager.
   * @param: currentPathChildren {Array} currentPathChildren
   * @return: resolve, reject {Promise}
   */
  var search = function (engagementFiles) {
    return new Promise((resolve, reject) => {
      let searchResult = []
      let searchTerm = document.getElementById('comp-fm-search').value
      if (searchTerm.trim() === '') {
        resolve(c.currentEngagement.files)
      } else {
        let ctr = 0
        _.forEach(engagementFiles, (item) => {
          let fileDefinition = fileDictionary[item._id]
          let date = Utility.dateProcess(item.dateUploaded, {month: {type: 'integer', length: 'short'}, output: 'MM/DD/YY'})
          let owner = item.owner.firstName + ' ' + item.owner.lastName
          let regex = new RegExp(searchTerm, 'i')
          if (fileDefinition) {
            if (regex.test(fileDefinition.name) || (fileDefinition.todo ? regex.test(fileDefinition.todo) : false) || (fileDefinition.category ? regex.test(fileDefinition.category.name) : false) || regex.test(date) || regex.test(owner)) {
              searchResult.push(item)
            }
          }
          ctr++

          if (ctr === engagementFiles.length) {
            if (searchResult.length > 0) {
              let onlyFiles = _.flatMap(searchResult, (oneItem) => {
                if (oneItem.fileType === 'folder') {
                  return getChildrenFiles(oneItem)
                } else {
                  return oneItem
                }
              })
              resolve(onlyFiles)
            } else {
              resolve([])
            }
          }
        })
      }
    })
  }

  /**
   * Filter the files view based on the field that the user has chosen.
   * @param {String} type
   * @param {JSON} value
   * @param {Array} files
   */
  const filterData = function (type, value, files) {
    return new Promise((resolve, reject) => {
      let searchResult = []
      let fileTypes = {
        'Image': ['jpg', 'png'],
        'PDF': ['pdf'],
        'Excel': ['xlsx', 'xls'],
        'Powerpoint': ['pptx', 'ppt'],
        'Word': ['doc', 'docx'],
        'CSV': ['csv'],
        'Text File': ['txt']
      }
      files.forEach((file, i) => {
        switch (type) {
          case 'uploadedBy':
            if (file.owner._id.toString() === value.toString()) {
              searchResult.push(file)
            }
            break
          case 'category':
            if (value === 'uncategorized') {
              // fileID is the key in the fileDictionary JSON object
              _.forEach(fileDictionary, (fileDefinition, fileID) => {
                if (fileDefinition.category.name === 'uncategorized') {
                  if (fileDefinition._id.toString() === file._id.toString()) {
                    searchResult.push(file)
                  }
                }
              })
            } else {
              if (fileDictionary[file._id]) {
                if (fileDictionary[file._id].category._id.toString() === value.toString()) {
                  searchResult.push(file)
                }
              }
            }
            break
          case 'fileType':
            fileTypes[value].forEach((oneType) => {
              if (file.name.endsWith('.' + oneType)) {
                searchResult.push(file)
              }
            })
        }

        if (i === files.length - 1) {
          resolve(searchResult)
        }
      })
    })
  }

  /**
   * This function changes the sort arrow color and direction.
   * @param {JSON} data
   */
  const changeSortArrow = function (data) {
    let previousSortArrow = document.getElementById(previousSort.arrowDomID)
    let previousSortHeader = document.getElementById(previousSort.headerID)
    let currentSortArrow = document.getElementById(data.arrowDomID)
    let currentSortHeader = document.getElementById(data.headerID)

    if (data.ascending) {
      currentSortArrow.className = 'icon auvicon-sort-down'
      currentSortHeader.style.color = '#599ba1'
      currentSortArrow.style.color = '#599ba1'
    } else {
      currentSortArrow.className = 'icon auvicon-sort-up'
      currentSortHeader.style.color = '#599ba1'
      currentSortArrow.style.color = '#599ba1'
    }

    if (previousSort.arrowDomID !== null && previousSort.type !== data.type) {
      previousSortArrow.className = 'icon auvicon-sort-down'
      previousSortArrow.style.color = '#6d6d6d'
      previousSortHeader.style.color = '#6d6d6d'
    }
  }

  /**
   *
   * @param {JSON} data
   * @return {Boolean}
   */
  const checkAllValueSame = function (data) {
    let sameObject = _.filter(previousSort.sortedArray, [data.type, previousSort.sortedArray[0][data.type]])
    if (sameObject.length !== previousSort.sortedArray.length) {
      return false // all values in that field are not same.
    } else {
      return true // all values in that field are same.
    }
  }

  /**
   * This Function is responsible for sorting files according to sorting type.
   * @param {object} data is json object that consisting sorting type information.
   */
  var sort = function (data) {
    let fTable = document.getElementById('wFilesTable-' + componentID)
    let tableNodes = $(fTable.querySelector('tbody')).children() // childNodes array

    changeSortArrow(data)

    // modifying the data in previousSort
    if (previousSort.arrowDomID !== null) { // on the same page. (same level of hierarchy)
      if (previousSort.type === data.type) { // clicked on the same field to sort
        previousSort.ascending = data.ascending
        previousSort.sortedArray.reverse()

        // This will prevent sort function to sort other fields together when all Todos or Categories or UploadedDate or UploadedBy are same.
        if (!checkAllValueSame(data)) {
          previousSort.sortedArray.forEach((item) => {
            fTable.querySelector('tbody').appendChild(item.DOM)
          })
        }
      } else {
        previousSort.arrowDomID = data.arrowDomID
        previousSort.type = data.type
        previousSort.ascending = data.ascending
        previousSort.headerID = data.headerID

        // Re-sort the items in previousSort.sortedArray by new type
        if (!checkAllValueSame(data)) {
          previousSort.sortedArray = _.sortBy(previousSort.sortedArray, [data.type])
          previousSort.sortedArray.forEach((item) => {
            fTable.querySelector('tbody').appendChild(item.DOM)
          })
        }
      }
    } else { // on the different page or just loaded the Files page.
      previousSort.arrowDomID = data.arrowDomID
      previousSort.type = data.type
      previousSort.ascending = data.ascending
      previousSort.headerID = data.headerID

      // Grab all the rows in the table.
      _.forEach(tableNodes, (oneRow, i) => {
        let item = {
          Index: i,
          DOM: oneRow,
          Name: $($(oneRow).children()[1]).children()[1].innerText,
          Todo: $(oneRow).children()[2].innerText,
          Category: $(oneRow).children()[3].innerText,
          UploadedDate: $(oneRow).children()[4].innerText,
          UploadedBy: $(oneRow).children()[5].innerText
        }
        previousSort.sortedArray.push(item)
      })

      if (!checkAllValueSame(data)) {
        previousSort.sortedArray = _.sortBy(previousSort.sortedArray, [data.type])
        previousSort.sortedArray.forEach((item) => {
          fTable.querySelector('tbody').appendChild(item.DOM)
        })
      }
    }
  }

  /**
   * This function returns the file name
   * @memberof Widget_FileManager
   * @param {Object} data - data of file
  */
  var fileNameModifier = function (data) {
    if (data && data.name) {
      if (data.name.length > fileNameLength) {
        let modifiedName = data.name.substr(0, fileNameLength - 2) + '...'
        return modifiedName
      } else {
        return data.name
      }
    } else {
      return ''
    }
  }

  /**
   * This function is responsible for building the DOM elements for information related to all the files.
   * The data of the file is viewed by fileViewBtn.
   * @memberof Widget_FileManager
   * @param {Object} data - data of the file
  */
  var addFile = function (data, fileIndex) {
    log('File Manager: Adding Files on DOM...')

    if (!data) {
      c.displayMessage('No file data to add.')
      return
    }

    var fileID
    var checkID
    var fileOwner = Utility.GetFullNameFromUser(data.owner)

    if (data.fileType === 'folder') {
      fileID = data.name // 'folder-' + data.name
      checkID = 'folder-check-' + fileID + componentID
    } else {
      fileID = data._id
      checkID = 'file-check-' + fileID + componentID
    }

    var fileTableRow = Forge.build({'dom': 'tr', 'id': 'module-fm-row-' + fileID + componentID, 'class': 'module-fm-tr', 'style': 'border-bottom: 1px solid #e7e8e7;'})
    var fileCheckHolder = Forge.build({ dom: 'td', style: 'width: 38px;' })
    var fileCheckDiv = Forge.build({ dom: 'div', class: 'ui fitted checkbox', style: 'margin-top: 10px;' })
    if (parentDomID !== 'm-fm-table-div') {
      fileCheckDiv.style.marginLeft = '20px'
    }
    var fileCheckBox = Forge.build({dom: 'input', type: 'checkbox', id: checkID})
    var fileCheckLabel = Forge.build({ dom: 'label' })
    fileCheckDiv.appendChild(fileCheckBox)
    fileCheckDiv.appendChild(fileCheckLabel)
    if (parentDomID !== 'm-fm-table-div') { // don't add checkbox in the modal_fileManager
      fileCheckHolder.appendChild(fileCheckDiv)
    } else {
      if (data.fileType !== 'folder') {
        fileCheckHolder.appendChild(fileCheckDiv)
      }
    }
    var dateText = ''
    if (data.dateUploaded) {
      var strDate = Utility.dateProcess(data.dateUploaded, {month: {type: 'integer', length: 'short'}, output: 'MM/DD/YY'})
      dateText = strDate
    }

    var fileTypeSrc = ''
    if (data.fileType && data.fileType === 'folder') {
      fileTypeSrc = 'icon auvicon-line-folder'
    } else {
      if (data.name.endsWith('.pdf')) {
        fileTypeSrc = 'images/icons/icon_pdf.svg'
      } else if (data.name.endsWith('.docx') || data.name.endsWith('.doc')) {
        fileTypeSrc = 'images/icons/icon_doc.svg'
      } else if (data.name.endsWith('.txt')) {
        fileTypeSrc = 'images/icons/icon_txt.svg'
      } else if (data.name.endsWith('.xls') || data.name.endsWith('.xlsx')) {
        fileTypeSrc = 'images/icons/icon_xls.svg'
      } else {
        fileTypeSrc = 'icon auvicon-line-file'
      }
    }

    var fileNameDiv = Forge.build({'dom': 'td', 'id': 'module-fm-fileName-div-' + fileID + componentID, 'class': 'module-fm-td', 'style': 'padding-right: 30px;position:relative; text-align: left;'})
    var fileName
    var fileNamePopup = Forge.build({ 'dom': 'div', 'id': 'module-fm-fileName-popup-' + fileID + componentID, 'text': data.name || '', 'class': 'module-fm-td-popup', 'style': '' })
    var fileType
    if (fileTypeSrc.startsWith('icon')) {
      if (data.fileType === 'folder') {
        fileType = Forge.build({ 'dom': 'span', 'class': fileTypeSrc, 'id': 'module-fm-icon-' + fileID + componentID, 'style': 'color:#b5b5b5;font-size: 24px; margin-left: 4px; vertical-align: middle;' })
      } else {
        fileType = Forge.build({'dom': 'span', 'class': fileTypeSrc, 'id': 'module-fm-icon-' + fileID + componentID, 'style': 'color:#b5b5b5;font-size: 29px; vertical-align: middle;'})
      }
      fileName = Forge.build({ 'dom': 'div', 'id': 'module-fm-fileName-' + fileID + componentID, 'text': fileNameModifier(data), 'style': 'margin: 14px 20px; font-size: 15px; display: inline-block;' })
    } else {
      fileType = Forge.build({ 'dom': 'img', 'src': fileTypeSrc, 'id': 'module-fm-icon-' + fileID + componentID, 'style': 'margin-left: 4px;' })
      fileName = Forge.build({ 'dom': 'div', 'id': 'module-fm-fileName-' + fileID + componentID, 'text': fileNameModifier(data), 'style': 'margin-left: 20px; font-size: 15px; display: inline-block;' })
    }
    fileNameDiv.appendChild(fileType)
    fileNameDiv.appendChild(fileName)
    fileNameDiv.appendChild(fileNamePopup)
    if (data.name.length > 46) {
      fileName.addEventListener('mouseover', (e) => {
        e.stopPropagation()
        fileNamePopup.style.display = 'inherit'
      })
      fileName.addEventListener('mouseout', (e) => {
        e.stopPropagation()
        fileNamePopup.style.display = 'none'
      })
    }
    var fileTodo = ''
    if (data.todo) {
      fileTodo = data.todo
      if (parentDomID === 'm-fm-table-div') {
        fileCheckBox.disabled = true
      }
    }
    var fileTodoRow = Forge.build({'dom': 'td', 'id': 'module-fm-fileTodo', 'class': 'module-fm-td', 'style': 'padding-right:32px; text-align: left;'})
    if (!data.fileType) {
      var fileTodoDiv = Forge.build({ 'dom': 'div', 'class': '', 'text': fileTodo, 'style': 'color: #363a3c;' })
      fileTodoRow.appendChild(fileTodoDiv)
    }

    var fileCategory
    if (data.category) {
      fileCategory = data.category
    }
    var fileCategoryRow = Forge.build({'dom': 'td', 'id': 'module-fm-fileCategory', 'class': 'module-fm-td', 'style': 'padding-right:32px; text-align: left;'})
    if (!data.fileType) {
      var fileCategoryDiv = Forge.build({ 'dom': 'div', 'class': 'module-fm-catDiv', 'text': fileCategory.name })
      fileCategoryDiv.style.background = fileCategory.color
      fileCategoryRow.appendChild(fileCategoryDiv)
    }

    var uploadDate = Forge.build({'dom': 'td', 'class': 'module-fm-td', 'style': 'text-align: left;', 'text': dateText})

    var uploadedBy = Forge.build({'dom': 'td', 'class': 'module-fm-td', 'style': 'text-align: left;', 'text': fileOwner})

    fileTableRow.appendChild(fileCheckHolder)
    fileTableRow.appendChild(fileNameDiv)
    fileTableRow.appendChild(fileTodoRow)
    fileTableRow.appendChild(fileCategoryRow)
    fileTableRow.appendChild(uploadDate)
    fileTableRow.appendChild(uploadedBy)

    document.getElementById('wFilesTable-' + componentID).querySelector('tbody').appendChild(fileTableRow)
    currentFiles.push({fileID: data._id, domID: 'module-fm-row' + data._id, selectedID: fileTableRow.id, dom: fileTableRow, fileName: fileNameModifier(data)})

    checkIDs.push(checkID)

    fileCheckBox.addEventListener('click', () => {
      // if this component is in the modal, make it only select one (uncheck the previous checked item.)
      if (parentDomID === 'm-fm-table-div') {
        selectOnlyOneFile(fileID)
      }

      if (fileCheckBox.checked === true) {
        fileTableRow.className += ' module-fm-tr-active'
      } else {
        fileTableRow.classList.remove('module-fm-tr-active')
      }
    })

    // when click a row, show selected effect
    fileCheckBox.addEventListener('click', function () {
      $('#' + this.id).toggleClass('module-fm-selectedtr')
    })

    // when double click the folder
    if (data.fileType === 'folder') {
      fileTableRow.addEventListener('dblclick', function (evt) {
        currentFolderName = data.path
        // Going New Folder, reset previous Sort.
        previousSort = { arrowDomID: null, type: null, ascending: null, headerID: null, sortedArray: []}
        populateFileTable()
      })
    }
  }

  /**
   * Registering Event
   */
  const registerEvents = function () {
    log('FileManager: Registering Event Listeners ...')
    if (parentDomID !== 'm-fm-table-div') { // don't attach this event to the file-manager modal
      c.registerForEvent('delete-file', 'module-fm-00000DEL', handle_deleteFileEvent)
    }

    document.getElementById('fm-filter-ddl').addEventListener('click', (e) => {
      if (document.getElementById('fm-filter-menu').style.display === 'none') {
        document.getElementById('fm-filter-menu').style.display = 'block'
      } else {
        document.getElementById('fm-filter-menu').style.display = 'none'
      }
    })

    document.getElementById('fm-filter-uploadedBy').addEventListener('mouseover', (e) => {
      $('#fm-filter-uploadedBy-menu').css('display', 'inherit')
    })
    document.getElementById('fm-filter-uploadedBy').addEventListener('mouseout', (e) => {
      $('#fm-filter-uploadedBy-menu').css('display', 'none')
    })
    document.getElementById('fm-filter-category').addEventListener('mouseover', (e) => {
      $('#fm-filter-category-menu').css('display', 'inherit')
    })
    document.getElementById('fm-filter-category').addEventListener('mouseout', (e) => {
      $('#fm-filter-category-menu').css('display', 'none')
    })
    document.getElementById('fm-filter-fileType').addEventListener('mouseover', (e) => {
      $('#fm-filter-fileType-menu').css('display', 'inherit')
    })
    document.getElementById('fm-filter-fileType').addEventListener('mouseout', (e) => {
      $('#fm-filter-fileType-menu').css('display', 'none')
    })
  }

  initialize()
}

Component_FileManager.prototype = ComponentTemplate.prototype
Component_FileManager.prototype.constructor = ComponentTemplate

module.exports = Component_FileManager
