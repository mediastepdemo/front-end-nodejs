'use strict'

import ComponentTemplate from '../ComponentTemplate'
import Modal_Custom from '../../modals/custom/Modal_Custom'
import Utility from '../../../js/src/utility_c'
import Forge from '../../Forge'
import log from '../../../js/src/log'

import styles from './accounting.css'

var Component_Accounting = function (ctrl, container, ownerObj) {
  ComponentTemplate.call(this, ctrl, container, ownerObj)

  this.name = 'Accounting'
  var parentDOM = container
  var owner = ownerObj
  var c = ctrl
  var self = this

  this.isComplete = function () {
    return true
  }

  var accountingFields = [
    {id: 'acc-quickbooksBtn', name: 'Quickbooks', src: 'images/logos/accounting-software/quickbooks.png', 'imgClass': 'onboardAcc-quickImg'},
    {id: 'acc-sapBtn', name: 'Sap', src: 'images/logos/accounting-software/sap.png', 'imgClass': 'onboardAcc-sapImg'},
    {id: 'acc-netsuiteBtn', name: 'Netsuite', src: 'images/logos/accounting-software/netsuite.png', 'imgClass': 'onboardAcc-netsuiteImg'},
    {id: 'acc-freshbooksBtn', name: 'Freshbooks', src: 'images/logos/accounting-software/freshbooks.png', 'imgClass': 'onboardAcc-freshImg'},
    {id: 'acc-xeroBtn', name: 'Xero', src: 'images/logos/accounting-software/xero.png', 'imgClass': 'onboardAcc-xeroImg'},
    {id: 'acc-waveBtn', name: 'Wave Accounting', src: 'images/logos/accounting-software/wave.png', 'imgClass': 'onboardAcc-waveImg'}
  ]

  var accountingList = []

  /**
   * @return a DOM object
   */
  var buildContent = function (settings) {
    if (!parentDOM) {
      return
    }
    var componentContentDiv = Forge.build({'dom': 'div'})

    var title = Forge.build({'dom': 'p', 'text': 'Integrate with your Accounting Software.', 'class': 'component-title'})
    var subTitle = Forge.build({'dom': 'p', 'text': 'We will pull your General Ledger, Trial Balance and any other information required by your auditor', 'class': 'component-subTitle'})

    var dropDownHolder = Forge.build({'dom': 'div', 'class': 'ui-widget'})
    var accountingDropdown = Forge.buildInputAutoComplete({'id': 'accounting-selectAccDropdown', 'label': '', 'placeholder': 'Type to Select your software', 'width': '362px', 'list': accountingList, 'style': 'text-align: center; margin-top: 56px; margin-bottom:0px;'})
    dropDownHolder.appendChild(accountingDropdown)

    var accountingTicketHolder = Forge.build({'dom': 'div', 'id': 'accounting-ticketContainer', 'class': 'onboardFinancial-ticketContainer'})

    var removeHighlight = function () {
      document.getElementById('acc-quickbooksBtn').classList.remove('bankTicket-highlight')
      document.getElementById('acc-sapBtn').classList.remove('bankTicket-highlight')
      document.getElementById('acc-netsuiteBtn').classList.remove('bankTicket-highlight')
      document.getElementById('acc-freshbooksBtn').classList.remove('bankTicket-highlight')
      document.getElementById('acc-xeroBtn').classList.remove('bankTicket-highlight')
      document.getElementById('acc-waveBtn').classList.remove('bankTicket-highlight')
    }

    for (var i = 0; i < accountingFields.length; i++) {
      var ticketContainer = Forge.build({'dom': 'div', 'id': accountingFields[i].id, 'class': 'onboardFinancial-selectContainer'})
      var ticketLogo = Forge.build({'dom': 'div', 'class': 'onboardAcc-selectLogo ' + accountingFields[i].imgClass})
      var ticketImg = Forge.build({'dom': 'img', 'src': accountingFields[i].src, 'class': 'onboardFinancial-LogoImg'})
      var ticketTxt = Forge.build({'dom': 'div', 'text': accountingFields[i].name, 'class': 'onboardFinancial-text'})
      ticketLogo.appendChild(ticketImg)
      ticketContainer.appendChild(ticketTxt)
      ticketContainer.appendChild(ticketLogo)
      accountingTicketHolder.appendChild(ticketContainer)

      // sets padding for wave accounting
      if (ticketContainer.id === 'acc-waveBtn') {
        ticketTxt.style.padding = '6px 16px'
      }

      // adding event listener in a wrapper.
      if (typeof window.addEventListener === 'function') {
        (function (x) {
          ticketContainer.addEventListener('click', function () {
            accountingDropdown.children[1].value = accountingFields[x].name
            removeHighlight()
            document.getElementById(accountingFields[x].id).classList.add('bankTicket-highlight')
          })
        })(i)
      }
      accountingList.push(accountingFields[i].name)
    }

    var continueBtn = Forge.buildBtn({'text': 'Skip', 'type': 'light', 'width': '102px', 'margin': '36px auto 0px'}).obj
    continueBtn.style.display = 'block'

    var FooterHolder = Forge.build({'dom': 'div', 'class': 'onboardFinancial-footHolder'})
    var FooterLine = Forge.build({'dom': 'p', 'text': 'Not ready to integrate right now?', 'class': 'onboardFinancial-footLine'})
    var FooterLine2 = Forge.build({'dom': 'p', 'text': 'You can skip and find it in your settings later.', 'class': 'onboardFinancial-footLine'})
    FooterHolder.appendChild(FooterLine)
    FooterHolder.appendChild(FooterLine2)

    componentContentDiv.appendChild(title)
    componentContentDiv.appendChild(subTitle)
    componentContentDiv.appendChild(dropDownHolder)
    componentContentDiv.appendChild(accountingTicketHolder)
    componentContentDiv.appendChild(continueBtn)
    componentContentDiv.appendChild(FooterHolder)

    parentDOM.appendChild(componentContentDiv)

    continueBtn.addEventListener('click', function () {
      self.trigger('move-to-next')
    })
  }

  this.loadUserData = function () {
    log('Accounting Component: Loading User Data')
  }

  buildContent()
}

Component_Accounting.prototype = ComponentTemplate.prototype
Component_Accounting.prototype.constructor = ComponentTemplate

module.exports = Component_Accounting
