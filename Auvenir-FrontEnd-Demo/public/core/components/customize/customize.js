'use strict'

import ComponentTemplate from '../ComponentTemplate'
import Utility from '../../../js/src/utility_c'
import Forge from '../../Forge'
import log from '../../../js/src/log'
import Widget_EngagementInfo from '../../widgets/engagement-info/Widget_EngagementInfo'

import styles from './customize.css'

var Component_Customize = function (ctrl, container, owner) {
  ComponentTemplate.call(this, ctrl, container, owner)

  var parentDOM = container
  var c = ctrl
  var self = this
  var text = c.lang.customizeComponent

  // Check to see if the user has any templates
  var savedTemplates = []

  /**
   * @return a DOM object
   */
  var buildContent = function (settings) {
    if (!parentDOM) {
      return
    }

    var componentBody = Forge.build({'dom': 'p', 'id': 'customize-component-body', 'class': 'component-body'})
    var header = Forge.build({'dom': 'h3', 'id': 'customize-component-header', 'class': 'customize-header', 'text': text.header})

    var gridContainer = Forge.build({'dom': 'div', 'id': 'c-customize-todo', 'class': 'ui grid', 'style': 'margin: 73px auto 150px'})
    var rolloverDiv = Forge.build({'dom': 'div', 'class': 'eight wide column', 'style': 'border-right: 1px solid #accdd0;'})
    var rolloverImage = Forge.build({'dom': 'img', 'src': '../../images/create-engagement/file-box-green.png', 'style': 'margin-top: 7px'})
    var rolloverSubText = Forge.build({'dom': 'p', 'class': 'customize-subtext', 'text': text.rolloverSub})
    var rolloverBtn = Forge.buildBtn({
      'text': text.rollover,
      'id': 'customize-rollover-btn',
      'type': 'primary'
    }).obj

    rolloverBtn.addEventListener('click', function () {
      viewRolloverTemplates()
    })

    if (savedTemplates.length === 0) {
      rolloverBtn.disabled = true
      rolloverImage.src = '../../images/create-engagement/file-box-grey.png'
    }

    rolloverDiv.appendChild(rolloverImage)
    rolloverDiv.appendChild(rolloverSubText)
    rolloverDiv.appendChild(rolloverBtn)
    gridContainer.appendChild(rolloverDiv)

    var createDiv = Forge.build({'dom': 'div', 'class': 'eight wide column', 'style': 'border-left: 1px solid #accdd0;'})
    var createImage = Forge.build({'dom': 'img', 'src': '../../images/create-engagement/clipboard.png'})
    var createSubText = Forge.build({'dom': 'p', 'class': 'customize-subtext', 'text': text.createSub})
    var createAddBtn = Forge.buildBtn({
      'text': c.lang.create,
      'id': 'customize-create-btn',
      'type': 'primary'
    }).obj

    createAddBtn.addEventListener('click', function () {
      self.trigger('create')

      document.getElementById('m-ce-addBtn').style.display = ''
      document.getElementById('m-ce-cancelBtn').style.display = ''
      // let engName = document.getElementById('engagement-name').value
      // let engType = document.getElementById('engagement-type').value
      // let engagementInfo = {name: engName, type: engType}
      // let businessName = document.getElementById('engagement-company').value
      //
      // ctrl.submitNewEngagementRequest(c.currentFirm._id, c.currentUser._id, engagementInfo, businessName)
    })

    createDiv.appendChild(createImage)
    createDiv.appendChild(createSubText)
    createDiv.appendChild(createAddBtn)
    gridContainer.appendChild(createDiv)

    componentBody.appendChild(header)
    componentBody.appendChild(gridContainer)
    parentDOM.appendChild(componentBody)
  }

  buildContent()
}

Component_Customize.prototype = ComponentTemplate.prototype
Component_Customize.prototype.constructor = ComponentTemplate

module.exports = Component_Customize
