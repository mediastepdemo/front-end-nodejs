'use strict'

import WidgetTemplate from '../WidgetTemplate'
import log from '../../../js/src/log'
import Forge from '../../Forge'
import styles from 'Widget_MyClientData.css'

/**
 * The Widget_MyClientData is responsible for displaying the progress of My Client Data.
 * @class Widget_MyClientData
 *
 * @param {Object} ctrl     - A reference to the current instance of the Main Application Controller (Controller.js)
 * @param {Object} container - The container that is attached to the parent modal container used to generate the instance of this modal.
 */
module.exports = function (ctrl, container) {
  WidgetTemplate.call(this)

  this.name = 'My Client'
  this.parent = container
  var c = ctrl

  /*
   * Local instance of field values
   * Purpose: Comparison with received values when running getChanges()
   */
  var fields = {
    firstName: {id: 'w-mc-firstName', value: ''},
    lastName: {id: 'w-mc-lastName', value: ''},
    jobTitle: {id: 'w-mc-jobTitle', value: ''},
    company: {id: 'w-mc-company', value: ''}
  }

  /**
   * This function is responsible for loading the Client's info and displaying them in DOM elements.
   * @memberof Widget_MyClientData
   */
  this.loadClient = function () {
    log('Loading Client Info')

    if (!c.currentAudit || !c.currentAudit.auditInfo) {
      return
    }

    if (c.currentClient !== null) {
      var client = c.currentClient

      document.getElementById(fields.firstName.id).innerHTML = client.firstName + ' ' + client.lastName
      document.getElementById(fields.lastName.id).innerHTML = fields.lastName.value = client.lastName
      document.getElementById(fields.jobTitle.id).innerHTML = fields.jobTitle.value = client.jobTitle
      document.getElementById(fields.company.id).innerHTML = fields.company.value = ''

      document.getElementById('w-mc-email-link').href = 'mailto:' + client.email + '?subject=Question About Auvenir'

      document.getElementById('w-mc-choose-client').style.display = 'none'
      document.getElementById('w-mc-client-info').style.display = 'inherit'
    } else {
      document.getElementById('w-mc-choose-client').style.display = 'inherit'
      document.getElementById('w-mc-client-info').style.display = 'none'
    }
  }

  /**
   * This function load all input fields with the information send in data about Client.
   * @memberof Widget_MyClientData
   * @param {String} data - data of input file
   */
  this.loadData = function (data) {
    log('Loading Client Info')

    if (data.firstName !== null) {
      document.getElementById(fields.firstName.id).value = fields.firstName = data.firstName
    }
    if (data.lastName !== null) {
      document.getElementById(fields.lastName.id).value = fields.lastName = data.lastName
    }
    if (data.jobTitle !== null) {
      document.getElementById(fields.jobTitle.id).value = fields.jobTitle = data.jobTitle
    }
    if (data.company !== null) {
      document.getElementById(fields.company.id).value = fields.company = data.company
    }
  }

  /**
   * This function is responsible for building the DOM elements that are associated with the Widget My Client Data
   * and attaching them to parent modal container.
   * @memberof Widget_MyClientData
   */
  this.buildContent = function () {
    log('Building My Client Content')
    var parentDiv = Forge.build({'dom': 'div', 'style': 'padding:10px;'})

    var title = Forge.build({'dom': 'h4', 'text': 'Client Data', 'class': 'widget-title'})
    var titleHR = Forge.build({'dom': 'hr', 'class': 'widget-hr'})
    parentDiv.appendChild(title)
    parentDiv.appendChild(titleHR)

    var scheduleTemplatesContainer = Forge.build({
      'dom': 'div',
      'id': 'w-mc-schedule-templates',
      'style': 'display:inherit; margin-top:30px'
    })
    parentDiv.appendChild(scheduleTemplatesContainer)

    var schedulePhoto = Forge.build({
      'dom': 'img',
      'id': '',
      'src': 'images/widgets/folder.svg',
      'class': '',
      'style': 'width: 110px; height: 88px; margin-left: auto; margin-right: auto;display: block;'
    })
    scheduleTemplatesContainer.appendChild(schedulePhoto)

    var clientDataBtn = Forge.build({
      'dom': 'a',
      'id': 'w-mc-client-data-link',
      'class': 'scheduleWidgetButton',
      'text': 'View'
    })

    clientDataBtn.addEventListener('click', function () {
      parent.viewPage('docs')
      window.scrollTo(0, 0)
    })

    scheduleTemplatesContainer.appendChild(clientDataBtn)

    return parentDiv
  }

  return this
}
