'use strict'

import Utility from '../../js/src/utility_c'
import Forge from '../Forge'
import log from '../../js/src/log'
import styles from './WidgetTemplate.css'
/**
 * The Template Widget is responsible for displaying the progress of all widgets
 * @class WidgetTemplate
 *
 * @param {Object} ctrl - A reference to the current instance of the Main Application Controller (Controller.js)
 */
var WidgetTemplate = function (ctrl) {
  var self = this
  this.name = ''
  var c = null
  this._widgetID = ''
  this._uniqueID = ''

  // event object
  this.evts = {}

  // setup listener
  this.on = function (tag, cb) {
    if (typeof tag !== 'string' || typeof cb !== 'function') {
      return
    }
    if (!self.evts[tag]) {
      self.evts[tag] = cb
    } else {
      self.evts[tag] = cb
    }
  }

  // trigger events
  this.trigger = function (tag, evt, data) {
    if (this.evts[tag]) {
      this.evts[tag](evt, data)
    }
  }

  do {
    this._uniqueID = Utility.randomString(32, Utility.ALPHANUMERIC)
  } while (document.getElementById('widget-' + this._uniqueID))
  this._widgetID = 'widget-' + this._uniqueID

  var currentSettings = {
    width: 'auto',
    height: 'auto',
    background: '#fff',
    border: '1px solid black',
    color: '#000',
    fontSize: '12px',
    font: 'Verdana'
  }

    /**
     * This function is responsbile for returning true or false depending whether the parent object exists in the DOM.
     * @memberof WidgetTemplate
     */
  this.parentExists = function () {
    return !!(document.getElementById(this.parent))
  }

    /**
     * This function is responsible for updating the settings for the modal.
     * @memberof WidgetTemplate
     *
     * @param {Object} settings - The settings that needs to be updated.
     */
  var saveSettings = function (settings) {
    currentSettings.width = (settings.width) ? settings.width : currentSettings.width
    currentSettings.height = (settings.height) ? settings.height : currentSettings.height
    currentSettings.background = (settings.background) ? settings.background : currentSettings.background
    currentSettings.border = (settings.border) ? settings.border : currentSettings.border
    currentSettings.color = (settings.color) ? settings.color : currentSettings.color
    currentSettings.fontSize = (settings.fontSize) ? settings.fontSize : currentSettings.fontSize
    currentSettings.font = (settings.font) ? settings.font : currentSettings.font
    currentSettings.margin = (settings.margin) ? settings.margin : currentSettings.margin

    currentSettings.header = (settings.header) ? settings.header : null
    currentSettings.footer = (settings.footer) ? settings.footer : null
    currentSettings.top = (settings.top) ? settings.top : null
    currentSettings.right = (settings.right) ? settings.right : null
    currentSettings.bottom = (settings.bottom) ? settings.bottom : null
    currentSettings.left = (settings.left) ? settings.left : null
    currentSettings.overflow = (settings.overflow) ? settings.overflow : false
    currentSettings.draggable = (settings.draggable) ? settings.draggable : false
    currentSettings.resizable = (settings.resizable) ? settings.resizable : false
    currentSettings.widgetBorder = (settings.widgetBorder)
  }

    /**
     * This function is responsible for building the widgets header, content and body.
     * It also build DOM elements and attaches them to the parent modal container.
     * @memberof WidgetTemplate
     *
     * @param {Object} settings - The settings that needs to be updated.
     */
  this.buildWidget = function (settings) {
    log(this.name + ': Building widget')
    saveSettings(settings)

    var parentDom = document.getElementById(this.parent)
    if (currentSettings.widgetBorder) {
      var myWidget = Forge.build({'dom': 'div', 'id': this._widgetID, 'class': 'widget widgetBorder'})
    } else {
      var myWidget = Forge.build({'dom': 'div', 'id': this._widgetID, 'class': 'widget'})
    }

       /* HEADER */
    if (currentSettings.header && currentSettings.header.active) {
      var myWidgetHeader = Forge.build({'dom': 'div', 'id': 'widgetHeader-' + this._uniqueID, 'class': 'widgetHeader', 'style': 'position:absolute;top:0px;width:100%; height:20px;background:lightgray;'})
      var widgetClose = Forge.build({'dom': 'span', 'text': 'X', 'class': 'w-closeIcon', 'id': 'widgetClose-' + this._uniqueID})

      var title = 'N/A'
      if (currentSettings.header && currentSettings.header.title) {
        title = currentSettings.header.title
      }

      var widgetTitle = Forge.build({'dom': 'label', 'text': title, 'style': 'text-align: center'})
      widgetClose.addEventListener('click', function (event) {
        var widgetContainer = $('#' + this._widgetID)
        widgetContainer.fadeOut(150)

        document.getElementById(this._widgetID).parentNode.removeChild(document.getElementById('widgetClose-' + this._uniqueID))
      })

      var headHR = Forge.build({'dom': 'hr'})
      myWidgetHeader.appendChild(widgetTitle)
      myWidgetHeader.appendChild(widgetClose)
      myWidgetHeader.appendChild(headHR)
      myWidget.appendChild(myWidgetHeader)
    }

       /** CONTENT **/
    var myWidgetContent = Forge.build({'dom': 'div', 'id': 'widgetContent-' + this._uniqueID, 'class': 'widgetContent', 'style': 'width:inherit; height:inherit;'})
    var content = this.buildContent()

    if (content) {
      myWidgetContent.appendChild(content)
    } else {
      var warningDOM = Forge.build({'dom': 'span', 'text': 'buildContent() must return a dom object!'})
      myWidgetContent.appendChild(warningDOM)
    }
    myWidget.appendChild(myWidgetContent)

       /* FOOTER */
    if (currentSettings.footer && currentSettings.footer.active) {
      var myWidgetFooter = Forge.build({'dom': 'div', 'id': 'widgetFooter-' + this._uniqueID, 'class': 'widgetFooter', 'style': 'position:absolute;bottom:0px;width:100%; height:20px;background:lightgray'})
      var footHR = Forge.build({'dom': 'hr'})
      var footLabel = Forge.build({'dom': 'a', 'text': 'This is the bottom!'})

      myWidgetFooter.appendChild(footHR)
      myWidgetFooter.appendChild(footLabel)
      myWidget.appendChild(myWidgetFooter)
    }

    return myWidget
  }

    /**
     * This function is responsible for applying settings to each widget.
     * Applies currentsettings based on what is passed.
     * @memberof WidgetTemplate
     */
  this.applySettings = function () {
    if (currentSettings.top) {
      $('#' + this._widgetID)[0].style.top = currentSettings.top
    }

    if (currentSettings.bottom) {
      $('#' + this._widgetID)[0].style.bottom = currentSettings.bottom
    }

    if (currentSettings.left) {
      $('#' + this._widgetID)[0].style.left = currentSettings.left
    }

    if (currentSettings.right) {
      $('#' + this._widgetID)[0].style.right = currentSettings.right
    }

    $('#' + this._widgetID)[0].style.verticalAlign = 'top'
    $('#' + this._widgetID)[0].style.margin = (currentSettings.margin) ? currentSettings.margin : null
    $('#' + this._widgetID)[0].style.width = (currentSettings.width) ? currentSettings.width : null
    $('#' + this._widgetID)[0].style.height = (currentSettings.height) ? currentSettings.height : null
    $('#' + this._widgetID)[0].style.background = (currentSettings.background) ? currentSettings.background : null
    $('#' + this._widgetID)[0].style.border = (currentSettings.border) ? currentSettings.border : null
    $('#' + this._widgetID)[0].style.color = (currentSettings.color) ? currentSettings.color : null
    $('#' + this._widgetID)[0].style.fontSize = (currentSettings.fontSize) ? currentSettings.fontSize : null
    $('#' + this._widgetID)[0].style.font = (currentSettings.font) ? currentSettings.font : null

    if (currentSettings.overflow) {
      $('#' + this._widgetID)[0].style.overflow = 'auto'
      $('#' + this._widgetID)[0].style['overflow-x'] = 'hidden'
    }

    if (currentSettings.draggable) {
      $(function () {
        $('#' + this._widgetID).draggable({containment: 'parent'})
      })
    }

    if (currentSettings.resizable) {
      $(function () {
        $('#' + this._widgetID).resizable()
      })
    }
  }

    /**
     * This function is responsible for building DOM elements
     * @memberof WidgetTemplate
     */
  this.buildContent = function () {
    var warningDOM = Forge.build({'dom': 'span', 'text': 'BUILD CONTENT NOT YET SUPPORTED'})
    return warningDOM
  }

    /**
     * This function is responsible for displaying the parent block on the screen.
     * @memberof WidgetTemplate
     */
  this.show = function () {
    document.getElementById(this.parent).style.display = 'inherit'
  }

    /**
     * This function is responsible for hiding the parent block on screen.
     * @memberof WidgetTemplate
     */
  this.hide = function () {
    document.getElementById(this.parent).style.display = 'none'
  }

    /**
     * This function is responsible for registering the events.
     * @memberof WidgetTemplate
     */
  var registerEvents = function () {
    log('No events to register!')
  }

    /**
     * This function is responsible for tearing down all the engagements dashboards events.
     * @memberof WidgetTemplate
     */
  var teardownEvents = function () {
    log('No events to teardown!')
  }

  return this
}

module.exports = WidgetTemplate
