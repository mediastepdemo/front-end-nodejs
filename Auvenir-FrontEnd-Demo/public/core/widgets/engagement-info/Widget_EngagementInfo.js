'use strict'

import WidgetTemplate from '../WidgetTemplate'
import Forge from '../../Forge'
import moment from 'moment'
import styles from './Widget_EngagementInfo.css'

var Widget_EngagementInfo = function (ctrl, engagement) {
  WidgetTemplate.call(this)

  this.name = 'EngagementInfo'
  this.parent = null
  var c = ctrl
  var self = this

  var states = {
    'SET-SCHEDULE': {
      client: null,
      auditor: {step: 1, title: 'Status', svg: '/images/illustrations/auditStatus/computer.svg'}
    },
    'INVITE-CLIENT': {
      client: null,
      auditor: {step: 1, title: 'Status', svg: '/images/illustrations/auditStatus/mail.svg'}
    },
    'INVITE-PENDING': {
      client: null,
      auditor: {step: 1, title: 'Status', svg: '/images/illustrations/auditStatus/clock.svg'}
    },
    'DATA-GATHERING-START': {
      client: {step: 1, title: 'Status', svg: '/images/illustrations/auditStatus/noGas.svg'},
      auditor: {step: 1, title: 'Status', svg: '/images/illustrations/auditStatus/noGas.svg'}
    },
    'DATA-GATHERING-PROGRESS': {
      client: {step: 1, title: 'Status', svg: '/images/illustrations/auditStatus/halfGas.svg'},
      auditor: {step: 1, title: 'Status', svg: '/images/illustrations/auditStatus/halfGas.svg'}
    },
    'DATA-GATHERING-DONE': {
      client: {step: 1, title: 'Status', svg: '/images/illustrations/auditStatus/analyzeReady.svg'},
      auditor: {step: 1, title: 'Status', svg: '/images/illustrations/auditStatus/fullGas.svg'}
    },
    'DATA-ANALYSIS-START': {
      client: {step: 2, title: 'Status', svg: '/images/illustrations/auditStatus/computerMagnify.svg'},
      auditor: {step: 2, title: 'Status', svg: '/images/illustrations/auditStatus/analyzeReady.svg'}
    },
    'DATA-ANALYSIS-PROGRESS': {
      client: {step: 2, title: 'Status', svg: '/images/illustrations/auditStatus/computerMagnify.svg'},
      auditor: {step: 2, title: 'Status', svg: '/images/illustrations/auditStatus/clipboard.svg'}
    },
    'DATA-ANALYSIS-DISCREPANCIES': {
      client: {step: 2, title: 'Status', svg: '/images/illustrations/auditStatus/dblClipboardZoom.svg'},
      auditor: {step: 2, title: 'Status', svg: '/images/illustrations/auditStatus/dblClipboardZoom.svg'}
    },
    'ANALYSIS-COMPLETE': {
      client: {step: 2, title: 'Status', svg: '/images/illustrations/auditStatus/computerMagnify.svg'},
      auditor: {step: 2, title: 'Status', svg: '/images/illustrations/auditStatus/analyzeReady.svg'}
    },
    'DATA-SUBMITTED': {
      client: {step: 2, title: 'Status', svg: '/images/illustrations/auditStatus/computerMagnify.svg'},
      auditor: {step: 2, title: 'Status', svg: '/images/illustrations/auditStatus/analyzeReady.svg'}
    },
    'DATA-IN-REVIEW': {
      client: {step: 2, title: 'Status', svg: '/images/illustrations/auditStatus/computerMagnify.svg'},
      auditor: {step: 2, title: 'Status', svg: '/images/illustrations/auditStatus/analyzeReady.svg'}
    },
    'DATA-REQUESTED': {
      client: {step: 2, title: 'Status', svg: '/images/illustrations/auditStatus/computerMagnify.svg'},
      auditor: {step: 2, title: 'Status', svg: '/images/illustrations/auditStatus/analyzeReady.svg'}
    },
    'DATA-SUBMITTED (NEW)': {
      client: {step: 2, title: 'Status', svg: '/images/illustrations/auditStatus/computerMagnify.svg'},
      auditor: {step: 2, title: 'Status', svg: '/images/illustrations/auditStatus/analyzeReady.svg'}
    },
    'OPINION-UPLOADED': {
      client: {step: 2, title: 'Status', svg: '/images/illustrations/auditStatus/computerMagnify.svg'},
      auditor: {step: 2, title: 'Status', svg: '/images/illustrations/auditStatus/clipboard.svg'}
    },
    'OPINION-DOWNLOADED': {
      client: {step: 3, title: 'Status', svg: '/images/illustrations/auditStatus/clipboardPen.svg'},
      auditor: {step: 3, title: 'Status', svg: '/images/illustrations/auditStatus/clipboardGlass.svg'}
    },
    'OPINION-SIGNED': {
      client: {step: 3, title: 'Status', svg: '/images/illustrations/auditStatus/clipboardPen.svg'},
      auditor: {step: 3, title: 'Status', svg: '/images/illustrations/auditStatus/clipboardGlass.svg'}
    },
    'SIGNED-UPLOADED': {
      client: {step: 3, title: 'Status', svg: '/images/illustrations/auditStatus/clipboardGlass.svg'},
      auditor: {step: 3, title: 'Status', svg: '/images/illustrations/auditStatus/clipboardPen.svg'}
    },
    'COMPLETE': {
      client: {step: 3, title: 'Status', svg: '/images/illustrations/auditStatus/folder.svg'},
      auditor: {step: 3, title: 'Status', svg: '/images/illustrations/auditStatus/clipboardSeal.svg'}
    }
  }

  function displayTimeInfo () {
    if (engagement.dateCompleted) {
      return 'Completed ' + moment(engagement.dateCompleted).format('MMMM DD')
    }

    return 'Updated ' + moment(engagement.lastUpdated || engagement.dateCreated).calendar(null, {
      sameDay: '[Today]',
      lastDay: '[Yesterday]',
      lastWeek: '[Last] dddd',
      sameElse: 'MM/DD/YYYY'
    })
  }

  var registerEvents = function () {
    function eventTriggered (tag, result) {
      if (result.result.id === engagement._id) {
        if (result.result.fields.status) {
          engagement.status = result.result.fields.status
        }
      }
      self.refresh()
    }

    c.registerForEvent('update-engagement', 'w-ei-' + engagement._id, eventTriggered)
  }

  this.refresh = function () {
    var status = engagement.status

    if (!status) {
      return
    }

    if (engagement.lastUpdated) {
      document.getElementById('date-' + engagement._id).innerHTML = displayTimeInfo()
    }

    if (!status[status]) {
      return
    }

    var json = null
    if (c.currentUser.type === 'AUDITOR') {
      if (states[status].auditor) {
        json = states[status].auditor
      }
    } else if (c.currentUser.type === 'CLIENT') {
      if (states[status].client) {
        json = states[status].client
      }
    }
    if (json) {
      document.getElementById('w-ei-bgimg-' + engagement._id).src = json.svg
    }
  }

  this.buildContent = function () {
    function transformStatus (status) {
      status = status.toLowerCase().replace('-', ' ')
      if (status.indexOf('data gathering') >= 0) {
        status = 'data gathering'
      }
      return status.charAt(0).toUpperCase() + status.slice(1, status.length)
    }

    function getClient (acl) {
      var i
      for (i = 0; i < acl.length; i++) {
        if (acl[i].role === 'CLIENT') {
          return acl[i]
        }
      }
      return {}
    }

    function getBusinessLogo (business) {
      // Returns a random integer between min (inclusive) and max (exclusive)
      function getRandomArbitraryInt (min, max) {
        return parseInt(Math.random() * (max - min) + min)
      }

      var defaultLogoPrefix = '/images/logos/business/BusinessLogo'
      return business.logo || defaultLogoPrefix + getRandomArbitraryInt(1, 8) + '.svg'
    }

    var client = getClient(engagement.acl).userInfo || {}
    var business = engagement.business._id || {}

    var myWidget = Forge.build({
      'dom': 'div',
      'id': 'widget-' + engagement._id,
      'class': 'e-widget',
      'style': 'text-align:center;'
    })
    var myWidgetHeader = Forge.build({'dom': 'div', 'class': 'e-widget-header'})

    var myWidgetContent = Forge.build({'dom': 'div', 'class': 'e-widget-content'})
    var myWidgetProgress = Forge.build({
      'dom': 'div',
      'id': 'progress-' + engagement._id,
      'class': 'e-widget-status'
    })
    var myWidgetImg = Forge.build({
      dom: 'img',
      id: 'w-ei-bgimg-' + engagement._id,
      src: getBusinessLogo(business),
      class: 'e-widget-randomImg'
    })
    var myWidgetLabel = Forge.build({
      dom: 'label',
      id: 'w-ei-' + engagement._id,
      class: 'e-widget-label',
      text: transformStatus(engagement.status)
    })

    var myWidgetContentOverlay = Forge.build({'dom': 'div', 'class': 'e-widget-options'})
    var myWidgetContentOverlayButton = Forge.build({
      dom: 'input',
      type: 'button',
      id: 'view-' + engagement._id,
      class: 'btn e-widget-option-engagement',
      value: engagement.dateCompleted ? 'Download' : 'View'
    })

    var myWidgetFooter = Forge.build({'dom': 'div', 'class': 'e-widget-footer'})
    var myWidgetTitle = Forge.build({
      'dom': 'p',
      'class': 'e-widget-auditTitle',
      'id': 'auditTitle-' + engagement._id,
      'text': engagement.name,
      'style': 'padding-top: 20px'
    })

    var myWidgetDate = Forge.build({
      dom: 'p',
      class: 'e-widget-dateUpdated',
      id: 'date-' + engagement._id,
      text: displayTimeInfo()
    })

    myWidget.appendChild(myWidgetHeader)
    myWidget.appendChild(myWidgetContent)
    myWidgetContent.appendChild(myWidgetProgress)
    myWidgetProgress.appendChild(myWidgetImg)
    myWidgetProgress.appendChild(myWidgetLabel)

    myWidgetContent.appendChild(myWidgetContentOverlay)
    myWidgetContentOverlay.appendChild(myWidgetContentOverlayButton)

    myWidget.appendChild(myWidgetFooter)
    myWidgetFooter.appendChild(myWidgetTitle)
    myWidgetFooter.appendChild(myWidgetDate)

    myWidgetContentOverlayButton.addEventListener('click', function () {
      c.loadEngagement(engagement._id)
    })

    return myWidget
  }

  registerEvents()
  return this
}

Widget_EngagementInfo.prototype = Object.create(WidgetTemplate.prototype)
Widget_EngagementInfo.prototype.constructor = Widget_EngagementInfo

module.exports = Widget_EngagementInfo
