
'use strict'

import WidgetTemplate from '../WidgetTemplate'
import Utility from '../../../js/src/utility_c'
import log from '../../../js/src/log'
import Forge from '../../Forge'

/**
 * The Widget_AnalyticsHeadline is responsible for displaying the progress of Auvenir Users, Auditors, Businesses and Engagements
 * @class Widget_AnalyticsHeadline
 *
 * @param {Object} ctrl      - A reference to the current instance of the Main Application Controller (Controller.js)
   * @param {Object} container - The container that is attached to the parent modal container used to generate the instance of this modal.
 */
var Widget_AnalyticsHeadline = function (ctrl, container) {
  WidgetTemplate.call(this, ctrl)

  if (!container) {
    container = ''
  }
  this.name = 'Analysis Widget'

  this.parent = container
  var c = ctrl

  var uniqueID = ''
  var titleID = 'w-ah-title'
  var valueID = 'w-ah-value'
  var iconID = 'w-ah-icon'

  var icon = ''
  var title = ''
  var value = ''

  /**
   * This function is responsible for building DOM elements that are associated with the widget Analytics Headline,
   * And attachingthem to parent modal container.
   * @memberof Widget_AnalyticsHeadline
   */
  this.buildContent = function () {
    log('Building Analysis Content')

    do {
      uniqueID = Utility.randomString(8, Utility.ALPHANUMERIC)
      titleID = 'w-ah-title-' + uniqueID
      valueID = 'w-ah-value-' + uniqueID
      iconID = 'w-ah-icon-' + uniqueID
    } while (document.getElementById(titleID))

    var div3Node = Forge.build({'dom': 'div', 'style': 'position:absolute;top:0px;width:100%; color:white; font-family:Lato'})
    var pNode = Forge.build({'dom': 'p', 'id': titleID, 'text': 'Unknown Title', 'style': 'font-size:15px;margin-left:20px'})
    var h3Node = Forge.build({'dom': 'h3', 'id': valueID, 'text': 'Default state', 'style': 'color:white;text-align:left;font-size:38px;font-weight:bold;margin:20px 0px 10px 20px;white-space: nowrap; padding:0px'})
    var iNode = Forge.build({'dom': 'i', 'id': iconID, 'class': 'fa fa-comments fa-lg', 'style': 'font-size:60px;position:absolute;top:25px;right:20px; color: rgba(255,255,255,0.35)'})

    div3Node.appendChild(h3Node)
    div3Node.appendChild(pNode)
    div3Node.appendChild(iNode)

    return div3Node
  }

  /**
   * This function loads all the data on admin page.
   * @memberof Widget_AnalyticsHeadline
   * @param {String} json - Pass the Widget to be added.
   */
  this.loadData = function (json) {
    document.getElementById(titleID).innerHTML = json.title || ''
    document.getElementById(valueID).innerHTML = json.value || ''
    document.getElementById(iconID).className = 'fa ' + json.fa + ' fa-lg'
  }

  return this
}

Widget_AnalyticsHeadline.prototype = Object.create(WidgetTemplate.prototype)
Widget_AnalyticsHeadline.prototype.constructor = Widget_AnalyticsHeadline

module.exports = Widget_AnalyticsHeadline
