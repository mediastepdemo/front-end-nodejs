'user strict'

/* global gapi, google */
/**
 * This is a Google Drive client-side manager.
 * Using public methods provided, you can call picker for user to select folder(s) and
 * disconnect the integration with Auvenir.
 *
 * @lastUpadted 2016-12-22
 * @author Lucy Kang
 *
 * @param ctrl {Object}
 * @param config {JSON}
 */

var GDriveManager = function (config, ctrl) {
  var self = this
  var c = ctrl
  var gDriveConfig = config
  var origin = window.location.protocol + '//' + window.location.host
  var scopeStr = 'https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/userinfo.email'
  var pickerAPILoaded = false
  var googleAuth = null
  var authResponse = null
  var accessToken = null
  var tempConsumer = ''
  var gDriveSelectedFolders = []

  var user = {
    authID: c.currentUser.auth.id,
    id: c.currentUser._id,
    uid: c.currentIntegrations.gdrive.uid
  }

  // Use the API Loader script to load google.picker and gapi.auth.
  function onApiLoad () {
    gapi.load('client:auth2', onAuthApiLoad)
    gapi.load('picker', { 'callback': onPickerApiLoad })
  }

  // loading api
  function onAuthApiLoad () {
    gapi.auth2.init({
      client_id: gDriveConfig.clientID,
      scope: scopeStr
    }).then(() => {
      googleAuth = gapi.auth2.getAuthInstance()

      googleAuth.isSignedIn.listen((isSignedIn) => {
        if (isSignedIn) {
          let gdriveUserObj = googleAuth.currentUser.get()
          authResponse = gdriveUserObj.getAuthResponse()
          accessToken = authResponse.access_token
        } else {
          self.trigger('setup-result', { result: 'error', msg: "You are not signed in at Google. Couldn't integrate with Google Drive." })
        }
      })

      // TODO - change from businessID > userID when fixing gdrive-manager in the future
      var userFD = new FormData()
      userFD.append('userID', user.id)
      var createUserReq = new XMLHttpRequest()
      createUserReq.onreadystatechange = function () {
        if (createUserReq.readyState === 4 && createUserReq.status === 200) {
          self.trigger('add-integration', { name: 'Google Drive', uid: JSON.parse(createUserReq.response).uid, userID: user.id })
          user.uid = JSON.parse(createUserReq.response).uid
        } else if (createUserReq.readyState === 4 && createUserReq.status === 500) {
          self.trigger('setup-result', { result: 'error', msg: JSON.parse(createUserReq.response).msg.toString() })
        }
      }
      createUserReq.open('POST', '/googledrive/createUser', true)
      createUserReq.send(userFD)
    })
  }

  function onPickerApiLoad () {
    pickerAPILoaded = true
  }

  function handleAuthResult (authResult, option) {
    if (authResult && !authResult.error) {
      document.getElementById('progressOverlay').style.display = 'inherit'
      var path = '/googledrive/offlineAccess'
      // TODO - change from businessID > userID when fixing gdrive-manager in the future
      var fd = new FormData()
      fd.append('code', authResult.code)
      fd.append('authID', user.authID)
      fd.append('userID', user.id)
      fd.append('uid', user.uid)

      var offlineAccessReq = new XMLHttpRequest()

      offlineAccessReq.onreadystatechange = function () {
        if (offlineAccessReq.readyState === 4 && offlineAccessReq.status === 200) {
          // it was advised by Google to validate access_token to be less vulnerable to confused deputy problem.
          var validateReq = new XMLHttpRequest()
          validateReq.onreadystatechange = function () {
            if (validateReq.readyState === 4) {
              if (validateReq.status === 200) {
                if (JSON.parse(validateReq.response).aud === gDriveConfig.clientID) { // check with our clientID too
                  tempConsumer = JSON.parse(offlineAccessReq.response).userEmail
                  document.getElementById('progressOverlay').style.display = 'none'
                  createPicker(option)
                } else {
                  // access_token didn't match. (security issue)
                  self.trigger('setup-result', { result: 'error', msg: 'Unexpected Error occured.'})
                }
              } else if (validateReq.status === 400 || validateReq.status === 500) {
                self.trigger('setup-result', { result: 'error', msg: 'Unexpected Error occured. Please try again.' })
              }
            }
          }
          accessToken = JSON.parse(offlineAccessReq.response).access_token
          validateReq.open('POST', 'https://www.googleapis.com/oauth2/v3/tokeninfo?access_token=' + accessToken)
          validateReq.send()
        } else if (offlineAccessReq.readyState === 4 && offlineAccessReq.status === 500) {
          document.getElementById('progressOverlay').style.display = 'none'
          self.trigger('setup-result', { result: 'error', msg: JSON.parse(offlineAccessReq.response).msg.toString() })
        }
      }
      offlineAccessReq.open('POST', path, true)
      offlineAccessReq.send(fd)
    } else {
      if (authResult.error) {
        self.trigger('setup-result', { result: 'error', msg: 'There was an error while authenticating you in Google. Please try again.' })

        document.getElementById('files-authRetry').addEventListener('click', function () {
          googleAuth.grantOfflineAccess({ 'redirect_uri': 'postmessage' }).then((result) => { handleAuthResult(result, option) })
        })
      }
    }
  }

  // Create and render a Picker object for picking user Photos.
  function createPicker (setting) {
    if (pickerAPILoaded && accessToken) {
      let docsView
      let picker
      if (setting.option === 'FOLDER') {
        docsView = new google.picker.DocsView(google.picker.ViewId.FOLDERS)
          // .setParent('root') // By default, it shows both user's own and shared with me folders with all levels of folders.
          .setIncludeFolders(true)
          .setSelectFolderEnabled(true)

        let ownedByMeView = new google.picker.DocsView(google.picker.ViewId.FOLDERS)
          .setIncludeFolders(true)
          .setOwnedByMe(true)
          .setSelectFolderEnabled(true)
          .setLabel('Owned By Me')

        picker = new google.picker.PickerBuilder()
          .addView(docsView)
          .addView(ownedByMeView)
          .setAppId(gDriveConfig.appID)
          .setTitle(`Please select a folder you wish to import to your audit.`)
          .setOAuthToken(accessToken)
          .setCallback(pickerCallback)
          .setOrigin(origin)
          .setLocale('en') // en: English / en-GB: English Great Britain / fr: French / fr-CA: Canadian French
          .build()
        picker.setVisible(true)
      } else if (setting.option === 'FILE') {
        docsView = new google.picker.DocsView(google.picker.ViewId.DOCS)
          .setMimeTypes('application/pdf,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-powerpoint,application/vnd.oasis.opendocument.spreadsheet,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,image/png,image/bmp,image/jpeg,text/plain,text/csv,application/rtf,text/sgml,application/vnd.google-apps.document,application/vnd.google-apps.photo,application/vnd.google-apps.presentation,application/vnd.google-apps.spreadsheet')
          .setIncludeFolders(true)

        let wordsView = new google.picker.DocsView(google.picker.ViewId.DOCUMENTS)
        let pptView = new google.picker.DocsView(google.picker.ViewId.PRESENTATIONS)
        let excelView = new google.picker.DocsView(google.picker.ViewId.SPREADSHEETS)
        let imageView = new google.picker.DocsView(google.picker.ViewId.DOCS_IMAGES)
          .setMimeTypes('image/png,image/bmp,image/jpeg') // MimeTypes should not have spaces after comma. It wont work with spaces.
        let pdfView = new google.picker.DocsView(google.picker.ViewId.PDFS)
        let ownedByMeView = new google.picker.DocsView(google.picker.ViewId.DOCS) // setMimeTypes should be fixed.
          .setMimeTypes('application/pdf,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-powerpoint,application/vnd.oasis.opendocument.spreadsheet,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,image/png,image/bmp,image/jpeg,text/plain,text/csv,application/rtf,text/sgml,application/vnd.google-apps.document,application/vnd.google-apps.photo,application/vnd.google-apps.presentation,application/vnd.google-apps.spreadsheet')
          .setIncludeFolders(true)
          .setOwnedByMe(true)
          .setLabel('Owned By Me')

        picker = new google.picker.PickerBuilder()
          .addView(docsView)
          .addView(wordsView)
          .addView(pptView)
          .addView(excelView)
          .addView(imageView)
          .addView(pdfView)
          .addView(ownedByMeView)
          .setAppId(gDriveConfig.appID)
          .setTitle('Please select a file you wish to import to your audit.')
          .setSelectableMimeTypes('application/pdf,application/vnd.openxmlformats-officedocument.presentationml.presentation,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-powerpoint,application/vnd.oasis.opendocument.spreadsheet,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,image/png,image/bmp,image/jpeg,text/plain,text/csv,application/rtf,text/sgml,application/vnd.google-apps.document,application/vnd.google-apps.photo,application/vnd.google-apps.presentation,application/vnd.google-apps.spreadsheet')
          .setOAuthToken(accessToken)
          .setCallback((data) => { filePickerCallback(data, setting) })
          .setOrigin(origin)
          .setLocale('en') // en: English / en-GB: English Great Britain / fr: French / fr-CA: Canadian French
          .build()
        picker.setVisible(true)
      }
    } else {
      self.trigger('setup-result', { result: 'error', msg: 'Something went wrong while we were connecting to your Google Drive. <div id="files-createPickerRetry" class="files-retry">Retry?</div>' })
      document.getElementById('files-createPickerRetry').addEventListener('click', function () {
        createPicker(setting)
      })
    }
  }

  /**
   * When user select a file (inside of a folder), we get the following info by callback.
   * selectedItem = {description:"", driveSuccess:true, id:"0B1lFpqizMMeEYUhaay03MjhFYkk", isShared:true, lastEditedUtc:1474927299000,
   *        mimeType:"image/svg+xml", name:"logo-applestore.svg", parentId:"0B1lFpqizMMeEOV9VdGhyYjhjVmc", serviceId:"docs", sizeBytes:3173,
   *        type:"photo", url:"https://drive.google.com/a/auvenir.com/file/d/0B1lFpqizMMeEYUhaay03MjhFYkk/view?usp=drive_web" }
   * @param {*} data
   */
  const filePickerCallback = function (data, settings) {
    let selectedItem

    if (data[google.picker.Response.ACTION] === google.picker.Action.PICKED) {
      document.getElementById('progressOverlay').style.display = 'inherit'
      selectedItem = data[google.picker.Response.DOCUMENTS][0]

      let path = '/googledrive/fileUpload'
      let fd = new FormData()

      fd.append('userID', user.id)
      fd.append('authID', user.authID)
      fd.append('uid', user.uid)
      fd.append('email', tempConsumer)
      fd.append('gdriveID', selectedItem.id)
      fd.append('engagementID', settings.engagementID)
      fd.append('todoID', settings.todoID)
      fd.append('requestID', settings.requestID)
      if (accessToken.length > 0) {
        fd.append('accessToken', accessToken)
      }

      var req = new XMLHttpRequest()
      req.onreadystatechange = function () {
        if (req.readyState === 4 && req.status === 200) {
          let res = JSON.parse(req.response)
          self.trigger('file-upload-result', { type: 'success', file: res.file, requestID: res.requestID, todoID: settings.todoID, engagement: res.engagement, oldRequest: settings.oldRequest, msg: res.msg})
        } else if (req.readyState === 4 && req.status === 500) {
          var error = JSON.parse(req.response)
          self.trigger('file-upload-result', { type: 'error', msg: error.msg.toString() })
        }
      }
      req.open('POST', path, true)
      req.send(fd)
    }
  }

  /**
   * @description callback from picker api
   *
   * DO NOT DELETE THE COMMENT BELOW. IMPORTANT TO HAVE WHAT WE GET FROM GOOGLE DRIVE.
   * When user select a folder, we get the following info by callback.
   * selectedItem = { description:'', driveSuccess: true, id:'idFromGoogleDrive', isShared:true, lastEditedUtc: 14749272700,
   *        mimeType: 'application/vnd.google-apps.folder', name: 'nameOfFolder', serviceId:'docs', sizeBytes: 0,
   *        type:'folder', url:'urlToTheFolder' }
   * @param {any} data
   */
  function pickerCallback (data) {
    var selectedItem = []
    var selectedItemJSON = {}
    var validationPass = true

    if (data[google.picker.Response.ACTION] === google.picker.Action.PICKED) {
      document.getElementById('progressOverlay').style.display = 'inherit'

      selectedItem = data[google.picker.Response.DOCUMENTS]

      for (var i = 0; i < selectedItem.length; i++) {
        if (selectedItem[i].type !== 'folder' || selectedItem[i].mimeType !== 'application/vnd.google-apps.folder') {
          validationPass = false
          self.trigger('setup-result', { result: 'error', msg: 'You have selected wrong item. Please select only folders.' })
        }
        selectedItemJSON[ 'folder' + i ] = selectedItem[i]
      }

      if (validationPass === true) {
        // request to our server to save the info about the folder in db.
        var path = '/googledrive/saveSelectedFolders'
        var fd = new FormData()
        // TODO - change from businessID > userID when fixing gdrive-manager in the future
        fd.append('folder', JSON.stringify(selectedItemJSON))
        fd.append('userID', user.id)
        fd.append('authID', user.authID)
        fd.append('uid', user.uid)
        fd.append('email', tempConsumer)
        if (accessToken.length > 0) {
          fd.append('accessToken', accessToken)
        }

        let pos = gDriveSelectedFolders.findIndex((gdrive) => {
          return gdrive.consumer === tempConsumer
        })

        if (pos === -1) {
          gDriveSelectedFolders.push({ accessToken: accessToken, consumer: tempConsumer, folders: [] })
          for (let i = 0; i < selectedItem.length; i++) {
            gDriveSelectedFolders[0].folders.push({ id: selectedItem[i].id, name: selectedItem[i].name })
          }
        } else {
          for (let i = 0; i < selectedItem.length; i++) {
            let folderPos = gDriveSelectedFolders[pos].folders.findIndex((folder) => {
              return folder.id === selectedItem[i].id && folder.name === selectedItem[i].name
            })
            if (folderPos === -1) {
              gDriveSelectedFolders[pos].folders.push({ id: selectedItem[i].id, name: selectedItem[i].name })
            }
          }
        }

        var req = new XMLHttpRequest()

        req.onreadystatechange = function () {
          if (req.readyState === 4 && req.status === 200) {
            if (selectedItem.length === 1) {
              self.trigger('setup-result', { result: 'success', folder: selectedItem[0].name, email: tempConsumer, folderID: selectedItem[0].id})
            } else {
              self.trigger('setup-result', { result: 'error', msg: 'You cannot select more than one folder.' })
            }
          } else if (req.readyState === 4 && req.status === 500) {
            var error = JSON.parse(req.response)
            self.trigger('setup-result', { result: 'error', msg: error.msg.toString() })
          }
        }
        req.open('POST', path, true)
        req.send(fd)
      }
    }
  }

  this.openPicker = (option) => {
    googleAuth.grantOfflineAccess({ 'redirect_uri': 'postmessage' }).then((result) => { handleAuthResult(result, option) })
  }

  this.integrateGDrive = (engagementID) => {
    if (gDriveSelectedFolders.length > 0) {
      for (let i = 0; i < gDriveSelectedFolders.length; i++) {
        var path = '/googledrive/folderIntegration'
        var fd = new FormData()
        fd.append('userID', user.id)
        fd.append('engagementID', engagementID)
        fd.append('authID', user.authID)
        fd.append('email', gDriveSelectedFolders[i].consumer)
        fd.append('accessToken', gDriveSelectedFolders[i].accessToken)
        fd.append('uid', user.uid)

        var req = new XMLHttpRequest()

        req.onreadystatechange = function () {
          let res = {}
          if (req.readyState === 4 && req.status === 200) {
            res.msg = JSON.parse(req.responseText).msg.toString()
            res.code = JSON.parse(req.responseText).code
            if (res.code === 0) {
              self.trigger('integration-result', { status: 200, code: 0, msg: res.msg })
            } else {
              self.trigger('integration-result', { status: 200, code: 1, msg: res.msg })
            }
          } else if (req.readyState === 4 && req.status === 500) {
            res.msg = JSON.parse(req.responseText).msg.toString()
            res.code = JSON.parse(req.responseText).code
            self.trigger('integration-result', { status: 500, msg: res.msg })
          }
        }
        req.open('POST', path, true)
        req.send(fd)
      }
    }
  }

  this.getAllConsumers = () => {
    var xhr = new XMLHttpRequest()
    var path = '/googledrive/getAllConsumers'
    var fd = new FormData()
    // TODO - change from businessID > userID when fixing gdrive-manager in the future
    fd.append('uid', user.uid)
    fd.append('userID', user.id)
    fd.append('authID', user.authID)

    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          if (JSON.parse(xhr.responseText).msg) {
            self.trigger('get-gdrive-integrations', { status: 200, msg: JSON.parse(xhr.responseText).msg.toString()})
          } else {
            let consumers = JSON.parse(xhr.responseText).consumers.split(',')
            if (consumers.length === 0) {
              self.trigger('get-gdrive-integrations', { status: 200, msg: 'NOTFOUND' })
            } else {
              consumers.forEach((element) => {
                gDriveSelectedFolders.push({ accessToken: '', consumer: element, folders: [] })
              })
              self.trigger('get-gdrive-integrations', { status: 200, consumers: consumers })
            }
          }
        } else if (xhr.status === 500) {
          self.trigger('get-gdrive-integrations', { status: 500, msg: JSON.parse(xhr.responseText).msg.toString() })
        } else {
          self.trigger('get-gdrive-integrations', { status: 500, msg: 'Unknown Error while retrieving your integration information. Please refresh your page or contact Auvenir.' })
        }
      }
    }
    xhr.open('POST', path, true)
    xhr.send(fd)
  }

  this.getFolderInfos = (email) => {
    var getFolderReq = new XMLHttpRequest()
    var uri = '/googledrive/getFolderNames'
    var form = new FormData()
    form.append('uid', user.uid)
    form.append('userID', user.id)
    form.append('authID', user.authID)
    form.append('email', email)

    getFolderReq.onreadystatechange = () => {
      if (getFolderReq.readyState === 4) {
        if (getFolderReq.status === 200) {
          var resText = JSON.parse(getFolderReq.responseText)
          // code:0 => There was a folder integrated.
          // code:1 => The account was integrated but no folder was integrated.
          if (resText.code === 0) {
            let folders = resText.folderStr.split(',')
            let folderIDs = resText.folderIDStr.split(',')
            gDriveSelectedFolders.forEach((account) => {
              if (account.consumer === email) {
                account.folders = []
                folders.forEach((folder, j) => {
                  account.folders.push({id: folderIDs[j], name: folders[j]})
                })
              }
            })

            self.trigger('get-gdrive-folders', { status: 200, folders: folders})
          } else {
            self.trigger('get-gdrive-folders', { status: 200, msg: 'NOTFOUND' })
          }
        } else if (getFolderReq.status === 500) {
          self.trigger('get-gdrive-folders', { status: 500, msg: JSON.parse(getFolderReq.responseText).msg.toString()})
        }
      }
    }

    getFolderReq.open('POST', uri, false)
    getFolderReq.send(form)
  }

  this.removeGDrive = (email, folderID) => {
    if (folderID) {
      gDriveSelectedFolders.forEach((element, i) => {
        if (element.consumer === email) {
          if (element.folders.length > 1) {
            element.folders.forEach((folder, j) => {
              if (folder.id === folderID) {
                let path = '/googledrive/removeFolder'
                let fd = new FormData()
                // TODO - change from businessID > userID when fixing gdrive-manager in the future
                fd.append('email', email)
                fd.append('userID', user.id)
                fd.append('authID', user.authID)
                fd.append('uid', user.uid)
                fd.append('folderID', folderID)

                let removeFolderReq = new XMLHttpRequest()

                removeFolderReq.onreadystatechange = function () {
                  if (removeFolderReq.readyState === 4 && removeFolderReq.status === 200) {
                    self.trigger('disconnect-result', { msg: 'success' })
                    element.folders.splice(j, 1)
                  } else if (removeFolderReq.readyState === 4 && removeFolderReq.status === 500) {
                    self.trigger('disconnect-result', { msg: 'Failed to remove the folder ' + folder.name + ' Please try again.'})
                  }
                }

                removeFolderReq.open('POST', path, true)
                removeFolderReq.send(fd)
              } else {
                self.trigger('disconnect-result', { msg: 'Error occured while removing the folder ' + folder.name + ' Please try again.'})
              }
            })
          // if there's only 1 folder and the user is trying to remove that folder from the integration list,
          // just remove the integration since there's no files that are integrated yet.
          } else if (element.folders.length >= 0) {
            let path = '/googledrive/disconnect'
            let fd = new FormData()
            fd.append('email', email)
            fd.append('authID', user.authID)
            fd.append('uid', user.uid)

            let deactivateReq = new XMLHttpRequest()

            deactivateReq.onreadystatechange = function () {
              if (deactivateReq.readyState === 4 && deactivateReq.status === 200) {
                self.trigger('disconnect-result', { msg: 'success', email: email })
                if (element.folders.length === 1) {
                  gDriveSelectedFolders.splice(i, 1)
                }
              } else if (deactivateReq.readyState === 4 && deactivateReq.status === 500) {
                self.trigger('disconnect-result', { msg: 'Failed to remove disconnect your account ' + email + '. Please try again.', email: email })
              }
            }
            deactivateReq.open('POST', path, true)
            deactivateReq.send(fd)
          }
        }
      })
    } else {
      gDriveSelectedFolders.forEach((element, i) => {
        if (element.consumer === email) {
          let path = '/googledrive/disconnect'
          let fd = new FormData()
          fd.append('email', email)
          fd.append('authID', user.authID)
          fd.append('uid', user.uid)

          let deactivateReq = new XMLHttpRequest()

          deactivateReq.onreadystatechange = function () {
            if (deactivateReq.readyState === 4 && deactivateReq.status === 200) {
              self.trigger('disconnect-result', { msg: 'success', email: email })
              if (element.folders.length === 1) {
                gDriveSelectedFolders.splice(i, 1)
              }
            } else if (deactivateReq.readyState === 4 && deactivateReq.status === 500) {
              self.trigger('disconnect-result', { msg: 'Failed to remove disconnect your account ' + email + '. Please try again.', email: email })
            }
          }
          deactivateReq.open('POST', path, true)
          deactivateReq.send(fd)
        }
      })
    }
  }

  // Event Object
  this.evts = {}

  /**
   * Sets up a listener on any specific tag
   * @param {String}   tag - The name of the event
   * @param {Function} cb  - The callback function
   */
  this.on = function (tag, cb) {
    if (typeof tag !== 'string' || typeof cb !== 'function') {
      return
    }

    if (self.evts[tag] === undefined) {
      self.evts[tag] = cb
    } else {
      self.evts[tag] = cb
    }
  }

  /**
   * Trigger all Listening Objects Callbacks
   * @param {String} - The tag being triggered
   * @param {Object} - The event that occured (if available)
   * @param {Object} - The custom event data
   */
  this.trigger = function (tag, evt, data) {
    if (this.evts[tag] !== undefined) {
      this.evts[tag](evt, data)
    }
  }

  onApiLoad()
}

module.exports = GDriveManager
