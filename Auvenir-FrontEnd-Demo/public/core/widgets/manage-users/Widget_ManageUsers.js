'use strict'

import WidgetTemplate from '../WidgetTemplate'
import Utility from '../../../js/src/utility_c'
import log from '../../../js/src/log'
import Forge from '../../Forge'
import styles from './Widget_ManageUsers.css'

var Widget_ManageUsers = function (ctrl, container) {
  WidgetTemplate.call(this)

  this.name = 'Manage Users'
  this.parent = container
  var c = ctrl
  var currentMapping = []

  this.buildContent = function () {
    log('Building Manage Users Widget Content')

    var divContainer = Forge.build({'dom': 'div', 'style': 'width:100%'})

    var tableDom = Forge.build({
      'dom': 'table',
      'id': 'w-mu-table',
      'style': 'width:100%; font-size:16px; background:white;   color: #363a3c;'
    })
    var thead = Forge.build({'dom': 'thead', 'style': 'background: #fff; font-weight: bolder; border-bottom: 1px solid #efefef'})
    var td1 = Forge.build({'dom': 'td', 'text': 'Name', 'class': 'w-mu-tableHeadings', 'style': 'padding-left: 10px'})
    var td2 = Forge.build({'dom': 'td', 'text': 'User Type', 'class': 'w-mu-tableHeadings'})
    var td3 = Forge.build({'dom': 'td', 'text': 'Email', 'class': 'w-mu-tableHeadings'})
    var td4 = Forge.build({'dom': 'td', 'text': 'Date Created', 'class': 'w-mu-tableHeadings'})
    var td6 = Forge.build({'dom': 'td', 'text': 'Auditor', 'class': 'w-mu-tableHeadings'})
    var td7 = Forge.build({'dom': 'td', 'text': 'Status', 'class': 'w-mu-tableHeadings'})

    divContainer.appendChild(tableDom)
    tableDom.appendChild(thead)
    thead.appendChild(td1)
    thead.appendChild(td2)
    thead.appendChild(td3)
    thead.appendChild(td4)
    thead.appendChild(td6)
    thead.appendChild(td7)

    return divContainer
  }

  /**
   * @description This function modify front-end status based on the status passed in and
   * display corresponding icons next to the status.
   *
   * @param {any} userID
   * @param {any} status
   */
  var updateStatus = function (userID, status) {
    log('Updating user status to ' + status)

    for (var i = 0; i < currentMapping.length; i++) {
      if (currentMapping[i].user._id === userID) {
        currentMapping[i].user.status = status
        break
      }
    }
  }

  /**
   * Do the corresponding front-end action based on verify-audtior event response from socket
   *
   * @param: {string} tagName of event
   * @param: {json} the result of socket event 'verify-auditor'
   */
  var verifyAuditorResponse = function (tagName, data) {
    log('> Event: verify-auditor response')
    document.getElementById('progressOverlay').style.display = 'none'
    if (data.code) {
      c.displayMessage(data.msg, {'color': 'red'})
    } else {
      c.displayMessage(data.msg)
      updateStatus(data.userID, data.status)
    }
  }

  /**
   * Do the corresponding front-end action based on verify-audtior event response from socket
   *
   * @param: {string} tagName of event
   * @param: {json} the result of socket event 'onboard-user'
   */
  var onboardUserResponse = function (tagName, data) {
    log('> Event: onboard-user response', data)
    if (data.code) {
      c.displayMessage(data.msg, {'color': 'red'})
    } else {
      c.displayMessage(data.msg)
      updateStatus(data.userID, data.status)
    }
    document.getElementById('progressOverlay').style.display = 'none'
  }

  /**
   * Do the corresponding front-end action based on update-user-status event response from socket
   *
   * @param: {string} tagName of event
   * @param: {json} the result of socket event 'update-user-status'
   */
  var updateUserStatusResponse = function (tagName, data) {
    log('> Event: update-user-status-response')
    if (data.code) {
      c.displayMessage(data.msg, {'color': 'red'})
    } else {
      c.displayMessage(data.msg)
      updateStatus(data.userID, data.status)
    }
    document.getElementById('progressOverlay').style.display = 'none'
  }

  /**
   * Setup Listners for verifyauditor and onboard user.
   * When Verifying auditor and onboard user is succesful, we will change the status of users and also showing message.
   * If there is an error, we show the error message to admin.
   */
  this.setupListeners = function () {
    log('Admin DashBoard: Setting up listeners ...')

    c.registerForEvent('verify-auditor', 'MANAGE-VERAUD-000', verifyAuditorResponse)
    c.registerForEvent('onboard-user', 'MANAGE-ONBOARD-000', onboardUserResponse)
    c.registerForEvent('update-user-status', 'MANAGE-UPDUSERST-000', updateUserStatusResponse)
  }

  this.loadData = function (map) {
    var parentDom = document.getElementById('w-mu-table')
    currentMapping = map

    var tbody = Forge.build({'dom': 'tbody'})
    parentDom.appendChild(tbody)
    for (var i = 0; i < currentMapping.length; i++) {
      var row = addAccount(currentMapping[i])
      tbody.appendChild(row)
    }
  }

  var addAccount = function (account) {
    var tr = Forge.build({'dom': 'tr', 'id': 'w-mu-row-' + account.user._id, 'style': 'border-bottom: 1px solid #efefef' })
    var td1 = Forge.build({
      'dom': 'td',
      'style': 'padding:20px 5px 20px 10px',
      'text': account.user.firstName + ' ' + account.user.lastName
    })
    var td2 = Forge.build({'dom': 'td', 'style': 'padding:20px 5px 20px 0px', 'text': account.user.type})
    var td3 = Forge.build({'dom': 'td', 'style': 'padding:20px 5px 20px 0px', 'text': account.user.email})

    var json = {
      military: false,
      month: {type: 'integer', length: 'short'},
      day: {append: false},
      output: 'MM/DD/YYYY'
    }
    var dStr = Utility.dateProcess(account.user.dateCreated, json)
    var td4 = Forge.build({'dom': 'td', 'style': 'padding:20px 5px 20px 0px', 'text': dStr})

    var auditor = ''
    if (account.user.type === 'CLIENT') {
      if (account.engagements.length > 0) {
        var acl = account.engagements[0].acl
        acl.forEach((aclItem) => {
          if (aclItem.role === 'AUDITOR') {
            currentMapping.forEach((map) => {
              if (map.user._id === aclItem.id) {
                auditor = map.user.email
              }
            })
          }
        })
      }
    }

    var td6 = Forge.build({'dom': 'td', 'style': 'padding:29px 5px 29px 0px', 'text': auditor})
    var td7 = Forge.build({'dom': 'td', 'style': 'padding:29px 5px 29px 0px', 'id': 'w-mu-status-' + account.user._id})

    var sel = Forge.build({'dom': 'select', 'id': 'w-mu-ddl-' + account.user._id, 'class': account.user.email})
    var op1 = Forge.build({'dom': 'option', 'id': 'active-' + account.user._id, 'value': 'ACTIVE', 'text': 'Active'})
    var op1a = Forge.build({'dom': 'option', 'id': 'inactive-' + account.user._id, 'value': 'INACTIVE', 'text': 'Inactive'})
    var op2 = Forge.build({'dom': 'option', 'id': 'locked-' + account.user._id, 'value': 'LOCKED', 'text': 'Locked'})
    var op3 = Forge.build({'dom': 'option', 'id': 'onboarding-' + account.user._id, 'value': 'ONBOARDING', 'text': 'Onboarding'})
    var op4 = Forge.build({'dom': 'option', 'id': 'pending-' + account.user._id, 'value': 'PENDING', 'text': 'Pending'})
    var op5 = Forge.build({'dom': 'option', 'id': 'waitlist-' + account.user._id, 'value': 'WAIT-LIST', 'text': 'Wait Listed'})

    if (account.user.type !== 'ADMIN') {
      td7.appendChild(sel)
      sel.appendChild(op1)
      sel.appendChild(op1a)
      sel.appendChild(op2)
      if (account.user.type === 'AUDITOR') {
        sel.appendChild(op3)
        sel.appendChild(op5)
      }
      if (account.user.type === 'CLIENT') {
        sel.appendChild(op3)
        sel.appendChild(op4)
        sel.appendChild(op5)
      }
    } else {
      var textNode = document.createTextNode('Active')
      td7.appendChild(textNode)
    }

    // set initial status to be displayed
    let userStatus = account.user.status
    if (userStatus === 'ACTIVE') {
      op1.selected = true
    } else if (userStatus === 'INACTIVE') {
      op1a.selected = true
    } else if (userStatus === 'LOCKED') {
      op2.selected = true
    } else if (userStatus === 'ONBOARDING') {
      op3.selected = true
    } else if (userStatus === 'PENDING') {
      op4.selected = true
    } else if (userStatus === 'WAIT-LIST') {
      op5.selected = true
    }

    sel.addEventListener('change', function (e) {
      updateUserStatus(account.user, this.value)
    })

    tr.appendChild(td1)
    tr.appendChild(td2)
    tr.appendChild(td3)
    tr.appendChild(td4)
    tr.appendChild(td6)
    tr.appendChild(td7)

    return tr
  }

  var updateUserStatus = function (user, status) {
    var warningContainer = Forge.build({'dom': 'div', 'style': 'width:100%;text-align:center;border:0px solid gray'})
    var description1 = Forge.build({
      'dom': 'p',
      'style': 'font-size:15px',
      'text': 'Are you sure you want to change user status from'
    })
    var description2 = Forge.build({
      'dom': 'p',
      'style': 'margin-bottom:15px; font-size:15px',
      'text': user.status + ' to ' + status
    })
    warningContainer.appendChild(description1)
    warningContainer.appendChild(description2)

    var btnContainer = Forge.build({'dom': 'div', 'style': 'width: auto; margin: 20px auto 10px;'})
    var confirmBtn2 = Forge.buildBtn({'text': 'Confirm', 'type': 'primary', 'margin': 'auto'}).obj
    confirmBtn2.style.display = 'inline-block'
    confirmBtn2.style.marginRight = '15px'
    var cancelBtn = Forge.buildBtn({ 'text': 'Cancel', 'type': 'default', 'margin': 'auto' }).obj
    cancelBtn.style.display = 'inline-block'
    btnContainer.appendChild(confirmBtn2)
    btnContainer.appendChild(cancelBtn)
    warningContainer.appendChild(btnContainer)

    confirmBtn2.addEventListener('click', function () {
      if (status === 'ACTIVE') {
        if (user.status === 'INACTIVE' || user.status === 'LOCKED' || user.status === 'ONBOARDING') {
          if (user.lastLogin !== null) {
            document.getElementById('progressOverlay').style.display = 'inherit'
            c.sendEvent({ name: 'update-user-status', data: { userID: user._id, status: status }})
          } else {
            document.getElementById('w-mu-ddl-' + user._id).value = user.status
            c.displayMessage('User cannot be ACTIVE if they have not gone through ONBOARDING process yet.', { color: 'red' })
          }
        } else {
          document.getElementById('w-mu-ddl-' + user._id).value = user.status
          c.displayMessage('The user cannot be ACTIVE without going through ONBOARDING.', { color: 'red' })
        }
      } else if (status === 'ONBOARDING') {
        if (user.status === 'PENDING') {
          c.displayMessage('PENDING client can only be onboarded when INVITED by auditor.')
        } else {
          if (user.type === 'CLIENT') {
            document.getElementById('progressOverlay').style.display = 'inherit'
            c.sendEvent({ name: 'onboard-user', data: { userID: user._id } })
          } else if (user.type === 'AUDITOR') {
            document.getElementById('progressOverlay').style.display = 'inherit'
            c.sendEvent({ name: 'verify-auditor', data: { userID: user._id } })
          } else {
            c.displayMessage('Not a valid user type.', { color: 'red' })
          }
        }
      } else {
        document.getElementById('progressOverlay').style.display = 'inherit'
        c.sendEvent({ name: 'update-user-status', data: { userID: user._id, status: status }})
      }
    })

    cancelBtn.addEventListener('click', function () {
      document.getElementById('w-mu-ddl-' + user._id).value = user.status
      c.closeMessage()
    })

    c.displayMessage(warningContainer)
  }

  return this
}

Widget_ManageUsers.prototype = Object.create(WidgetTemplate.prototype)
Widget_ManageUsers.prototype.constructor = Widget_ManageUsers

module.exports = Widget_ManageUsers
