'use strict'

import WidgetTemplate from '../WidgetTemplate'
import Utility from '../../../js/src/utility_c'
import log from '../../../js/src/log'
require('./Widget_MyClient.css')

/**
 * My Client widget, identifying the auditors current client,
 * ability to invite the client to an engagement, and once they have
 * accepted the invitation, the ability to send the client a message
 * (send a message modal to be built)
 * @class Widget_MyClient
 * @param {Function} ctrl      - The Controller
 * @param {Object}   container - The parent element of the widget
 * @returns The widget
 */
var Widget_MyClient = function (ctrl, container) {
  WidgetTemplate.call(this)

  this.name = 'My Client'
  this.parent = container
  const c = ctrl
  let clientAcl

  const componentContainerId = 'engagement-my-client-widget'
  const actionID = 'w-mc-emailLink-' + this._uniqueID

  this.buildContent = function () {
    log('Building My Client Content')

    let eng
    if (c.currentEngagement) {
      eng = c.myEngagements.find((e) => {
        return e._id === c.currentEngagement._id
      })
    }

    let nameField
    let company = ''
    let buttonLabel = 'Select Client'
    let companyClass = 'w-mc-company'

    if (eng) {
      clientAcl = eng.acl.find((a) => {
        return (a.role === 'CLIENT') && a.userInfo
      })
      if (clientAcl) {
        const client = clientAcl.userInfo
        // profilePicture = client.profilePicture || profilePicture
        nameField = client.firstName + ' ' + client.lastName

        if (clientAcl.status === 'INVITED') {
          buttonLabel = 'Resend'
        } else if (clientAcl.status === 'ACTIVE') {
          buttonLabel = 'Send a Message'
        } else {
          buttonLabel = 'Invite'
        }
      } else {
        nameField = 'No Client Selected'
        companyClass = 'w-mc-companyEmpty'
      }

      if (eng.businessID && eng.businessID.name) {
        company = eng.businessID.name
      }
    }

    return $(`<div style='padding: 10px 10px 26px;' id='${componentContainerId}'>
      <h4 class='widget-title'>My Client</h4>
      <hr/>
      <div style='margin-top:20px; text-align: center'>
        <div id='w-mc-displaySettings--${this._uniqueID}' class='w-mc-settings'>
          <div id='main'>
            <div id='w-mc-menuContainer-${this._uniqueID}' class='w-mc-menuContainer' style='display:none'>
              <p class='w-mc-settingsMenuContainerOption'>Edit Profile</p>
              <p class='w-mc-settingsMenuContainerOption' id='w-mc-removeClient'>Remove Client</p>
            </div>
          </div>
        </div>
        <div>
          <p id='w-mc-firstName-${this._uniqueID}' class='w-mc-name'>${nameField}</p>
          <p id='w-mc-company-${this._uniqueID}' class=${companyClass}>${company}</p>
        </div>
        <button id='${actionID}' class='w-mc-inviteBtn' style='outline: none;'>${buttonLabel}</button>
      </div>
    </div>`)[0]
  }

  this.refresh = () => {
    $(`#${componentContainerId}`).replaceWith(this.buildContent())

    const actionBtn = $(`#${actionID}`)
    if (clientAcl) {
      if (['INVITED', 'PENDING'].indexOf(clientAcl.status) !== -1) {
        actionBtn.on('click', () => {
          c.sendEngagementInvite(c.currentEngagement._id, clientAcl.userInfo.email)
        })
      } else if (clientAcl.status === 'ACTIVE') {
        actionBtn.on('click', () => {
          // TODO - there's no sending message to user anymore.
          // c.displaySendMsgModal()
        })
      }
    } else {
      actionBtn.on('click', () => {
        c.displayInviteClientModal()
      })
    }
  }

  c.registerForEvent('update-engagement', 'w-mc-0001', this.refresh.bind(this))
  c.registerForEvent('select-client', 'w-mc-select-client-0001', this.refresh.bind(this))
  return this
}

export default Widget_MyClient
