'use strict'

import Utility from '../../../js/src/utility_c'
import log from '../../../js/src/log'
import Forge from '../../Forge'
import styles from './flash-alert.css'
/**
 * The FlashAlert class is used for creating an instance of a flash alert in the form of a notification banner at the top of the screen for the user.
 * @class FlashAlert
 * @param elementID {string} customId for flashAlert Dom element
 * @return {Object} FlashAlert - the entire Flash alert
 */
var FlashAlert = function (elementID) {
  var self = this

  var idPrefix = 'flashAlert-'
  var flashAlertId = null
  var uniqueID = null

  var delay = 3000
  var bgColor = '#fcfcfc'
  var closeTimer = null // the timer is used for hiding the flashAlert

  if (elementID) {
    flashAlertId = elementID
  } else {
    do {
      uniqueID = Utility.randomString(8, Utility.ALPHANUMERIC)
    } while (document.getElementById(idPrefix + uniqueID))
    flashAlertId = idPrefix + uniqueID
  }

  /**
   * Builds the flash alert
   * Creates a unique ID, assigns it to the flash alert, builds all the elements,
   * then adds an event listener to close the flash when it is clicked.
   * It also sets a display none timeout after 5 sec, this is as a precaution as the animation runs through changing the height.
   */
  var buildDom = function () {
    log('Building Flash Alert DOM')

    var alertContainer = Forge.build({'dom': 'div', 'id': flashAlertId, 'class': 'fl-a-container'})
    var flashTextHolder = Forge.build({'dom': 'div', 'class': 'fl-a-textHolder'})
    var alertTextContainer = Forge.build({'dom': 'p', 'id': flashAlertId + '-mainText', 'class': 'fl-a-text', 'text': ''})
    var flashDismiss = Forge.build({'dom': 'div', 'class': 'fl-a-dismiss auvicon-line-circle-ex'})

    flashTextHolder.appendChild(alertTextContainer)
    alertContainer.appendChild(flashTextHolder)
    alertContainer.appendChild(flashDismiss)

    alertContainer.addEventListener('click', self.close)

    document.body.appendChild(alertContainer)
  }

  /**
   * @param options {background: String, closeDelay: Number} background should be hex color, closeDelay should be millisecond
   */
  this.setSettings = function (options) {
    if (!options) {
      return
    }

    if (options.bgColor) {
      self.setBackground(options.bgColor)
    }

    if (options.delay) {
      self.setDelay(options.delay)
    }
  }

  /**
   * config the flashAlert dismiss delay
   * @param options {closeDelay: Number} value should be millisecond
   */
  this.setDelay = function (closeDelay) {
    if (typeof closeDelay === 'number' && closeDelay > 0) {
      delay = closeDelay
    }
  }

  /**
   * Sets the background color if it passes validation
   */
  this.setBackground = function (color) {
    var hex_regex = /^#([0-9a-f]{3}|[0-9a-f]{6})$/i
    if (typeof color === 'string' && color.test(hex_regex)) {
      bgColor = color
    }
  }

  /**
   * set the flashAlert content and settings
   * @param options {content: String, type:ENUM(SUCCESS, ERROR), settings:{background:String, closeDelay: Number}}
   */
  this.flash = function (options) {
    self.open(options)
    closeTimer = setTimeout(self.close, delay)
  }

  /**
   * display the flashAlert and set the timer to hide the flashAlert
   */
  this.open = function (options) {
    if (!options) {
      return
    }

    var content = options.content
    var type = options.type
    var settings = options.settings

    var alertTextContainer = document.getElementById(flashAlertId + '-mainText')
    var alertContainer = document.getElementById(flashAlertId)

    // if flash alert node is deleted by other code block
    // re-create one
    if (!alertTextContainer || !alertContainer) {
      buildDom()
      alertTextContainer = document.getElementById(flashAlertId + '-mainText')
      alertContainer = document.getElementById(flashAlertId)
    }

    if (settings && settings.bgColor) {
      self.setBackground(settings.bgColor)
    }
    if (settings && settings.delay) {
      self.setDelay(settings.delay)
    }

    alertTextContainer.innerHTML = content || ' '
    switch (type) {
      case 'ERROR':
        alertContainer.classList.add('fl-a-orange')
        break
      case 'SUCCESS':
        alertContainer.classList.remove('fl-a-orange')
        break
      default:
        alertContainer.style.background = bgColor
    }

    alertContainer.classList.remove('fl-a-container-hide')
    alertContainer.classList.add('fl-a-container-show')
  }

  /**
   * hide the flashAlert and clear the timer
   */
  this.close = function () {
    var alertContainer = document.getElementById(flashAlertId)

    if (!alertContainer) {
      return
    }

    clearTimeout(closeTimer)
    alertContainer.classList.remove('fl-a-container-show')
    alertContainer.classList.add('fl-a-container-hide')
  }

  buildDom()
}

module.exports = FlashAlert
