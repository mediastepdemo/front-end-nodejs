'use strict'

import WidgetTemplate from '../WidgetTemplate'
import Utility from '../../../js/src/utility_c'
import log from '../../../js/src/log'
import Forge from '../../Forge'
import styles from './Widget_ActivityFeed.css'

var Widget_ActivityFeed = function (ctrl, container) {
  WidgetTemplate.call(this)

  this.name = 'Activity Widget'
  const VISIBLE_OPERATION = ['CREATE', 'UPDATE', 'DELETE', 'INVITE', 'ONBOARD']
  const THIRTY_MIN = 30 * 60 * 1000
  const AUDITOR_MAXHEIGHT = 140
  const CLIENT_MAXHEIGHT = 165

  var parent = container
  var c = ctrl
  var self = this
  var activityList = []
  var activityLogList = []

  var registerEvents = function () {
    log('Activity: Registering Event Listeners ...')
    c.registerForEvent('create-activity', 'widget-activity-00002ADD', createActivityResponse)
    c.registerForEvent('get-csv-string', 'widget-activity-00004ADD', getCSVStringResponse)
    if (ctrl.currentUser.type === 'AUDITOR') {
      $(document).ready(function () {
        $('#engagementActivityLink').click(function () {
          var widget = document.getElementById(self._widgetID)
          let widgetHeight = parseInt(widget.style.height)
          let tableHeight = $('#activityTable').height()
          if (widgetHeight - tableHeight < AUDITOR_MAXHEIGHT) {
            widget.style.overflowY = 'auto'
            widget.style.overflowX = 'hidden'
          } else {
            widget.style.overflowY = 'hidden'
            widget.style.overflowX = 'hidden'
          }
        })
      })
    } else if (ctrl.currentUser.type === 'CLIENT') {
      $(document).ready(function () {
        $('#h-dashboardLink').click(function () {
          var widget = document.getElementById(self._widgetID)
          let widgetHeight = parseInt(widget.style.height)
          let tableHeight = $('#activityTable').height()
          if (widgetHeight - tableHeight < CLIENT_MAXHEIGHT) {
            widget.style.overflowY = 'auto'
            widget.style.overflowX = 'hidden'
          } else {
            widget.style.overflowY = 'hidden'
            widget.style.overflowX = 'hidden'
          }
        })
      })
    }
  }

  /**
   * Setting Border height depends on length of activity table and
   * Setting overflow if Table Height is bigger than allowed height.
   */
  this.setBorder = function () {
    document.getElementById('activityTable').style.height = 'auto'
    document.getElementById('activityTable').style.height = document.getElementById('activityTable').offsetHeight - 45 + 'px'
    var widget = document.getElementById(self._widgetID)
    let widgetHeight = parseInt(widget.style.height)
    let tableHeight = $('#activityTable').height()
    if (widgetHeight - tableHeight < CLIENT_MAXHEIGHT) {
      widget.style.overflowY = 'auto'
      widget.style.overflowX = 'hidden'
    } else {
      widget.style.overflowY = 'hidden'
      widget.style.overflowX = 'hidden'
    }
  }

  this.buildContent = function () {
    var parentDiv = Forge.build({'dom': 'div', 'style': 'padding:10px;'})
    var title = Forge.build({'dom': 'h4', 'text': 'Activity Feed', 'class': 'widget-title'})
    var downloadActivity = Forge.build({dom: 'div', class: 'auvicon-line-download', style: 'float: right; font-size: 20px; cursor: pointer; padding: 8px;'})
    var searchDiv = Forge.build({'dom': 'div', 'id': 'w-activity-searchDiv', 'class': 'module-fm-search', style: 'display: none;'})

    var searchInput = Forge.build({'dom': 'input', 'id': 'w-activity-searchInput', 'class': 'module-fm-input module-fm-noShow', 'style': 'margin-left:5px;border:0px;width:170px;margin-bottom:5px;font-size:14px;font-family:Lato;', 'placeholder': 'Start typing to search...'})
    var searchIcon = Forge.build({'dom': 'span', 'class': 'icon-search module-fm-float', 'style': 'font-size:22px;', 'id': 'w-activity-searchIcon'})

    title.appendChild(downloadActivity)
    downloadActivity.addEventListener('click', () => {
      c.getCSVString(activityLogList, ['date', 'user', 'message'])
    })

    searchDiv.appendChild(searchIcon)
    searchDiv.appendChild(searchInput)
    title.appendChild(searchDiv)

    searchDiv.addEventListener('mouseover', function () {
      document.getElementById('w-activity-searchDiv').classList.add('module-fm-borderBottom')
      document.getElementById('w-activity-searchIcon').classList.remove('module-fm-float')
      document.getElementById('w-activity-searchInput').classList.remove('module-fm-noShow')
    })

    searchDiv.addEventListener('mouseout', function () {
      if (document.activeElement.tagName !== 'INPUT') {
        // If Search Input is active, Should not hide search box.
        document.getElementById('w-activity-searchDiv').classList.remove('module-fm-borderBottom')
        document.getElementById('w-activity-searchIcon').classList.add('module-fm-float')
        document.getElementById('w-activity-searchInput').classList.add('module-fm-noShow')
      }
    })

    searchInput.addEventListener('blur', function (e) {
      document.getElementById('w-activity-searchDiv').classList.remove('module-fm-borderBottom')
      document.getElementById('w-activity-searchIcon').classList.add('module-fm-float')
      document.getElementById('w-activity-searchInput').classList.add('module-fm-noShow')
    })

    searchInput.addEventListener('keyup', function () {
      search(this.value)
    })

    var activityTitleHR = Forge.build({'dom': 'hr', 'class': 'widget-hr'})

    parentDiv.appendChild(title)
    parentDiv.appendChild(activityTitleHR)

    var activityTable = Forge.build({'dom': 'div', 'style': 'width:100%;border-left: 2px solid #ddd;margin-left:62px', 'id': 'activityTable'})

    parentDiv.appendChild(activityTable)

    return parentDiv
  }

  var addDayToActivityTable = function (strDate, dateID) {
    // add start date height
    var dayTable = Forge.build({'dom': 'div', 'style': 'position:relative;margin-top:20px', 'id': 'w-activity-date-' + dateID})
    if (document.getElementById('activityTable').childNodes.length > 1) {
      dayTable.style['margin-top'] = '20px'
    }
    var date = Forge.build({'dom': 'div', 'class': 'w-activity-date'})
    var dateText = Forge.build({'dom': 'div', 'text': strDate, 'class': 'w-activity-date-text'})
    var br = Forge.build({'dom': 'br'})
    date.appendChild(dateText)
    dayTable.appendChild(date)
    dayTable.appendChild(br)
    document.getElementById('activityTable').appendChild(dayTable)
    return dayTable
  }

  this.loadData = function (activities) {
    document.getElementById('activityTable').innerHTML = ''
    activityLogList = []
    if (activities) {
      activityList = []
      for (var i = 0; i < activities.length; i++) {
        if (~VISIBLE_OPERATION.indexOf(activities[i].operation) && activities[i].timestamp >= c.currentUser.dateCreated) {
          addActivity(activities[i])
        }
      }
      // Example of calling getCSVString
      // c.getCSVString(activityLogList, ['date', 'user', 'message'])
    }
  }

  /**
   * This function responsible responsible for update request
   */
  var createActivityResponse = function (tag, data) {
    self.loadData(c.currentActivities)
  }

  var getCSVStringResponse = function (tag, data) {
    let csvString = data.result
    // TODO: Do something with this csvString
  }

  var search = function (term) {
    for (var i = 0; i < activityList.length; i++) {
      var activityContent = activityList[i].text
      if (term === '') {
        document.getElementById(activityList[i].domID).style.display = 'inherit'
      } else {
        if (activityContent.indexOf(term) === -1) {
          document.getElementById(activityList[i].domID).style.display = 'none'
        } else {
          document.getElementById(activityList[i].domID).style.display = 'inherit'
        }
      }
    }
  }

  /**
   * Add Listener onto Any File name to open taht file which is only active file
   */
  var fileClickEvent = function (dom) {
    let file
    for (var i = 0; i < c.currentFiles.length; i++) {
      if (c.currentFiles[i]._id === dom.getAttribute('value') && c.currentFiles[i].status === 'ACTIVE') {
        file = c.currentFiles[i]
      }
    }
    if (file) {
      // Add File Event Listener for active files.
      dom.style.cursor = 'pointer'
      dom.addEventListener('click', function (evt) {
        if (file && file.source) {
          if (file.source.name === 'Google Drive') {
            if (file.source.fid) {
              window.open(`https://${c.getSettings().domain}/googleDrive/downloadFile/${file.source.fid}/${file.name}?option=view`, '_blank')
            } else {
              c.displayMessage('Can\'t Download file.')
            }
          } else {
            c.displayMessage('Can\'t Download file.')
          }
        } else if (file) {
          window.open(`https://${c.getSettings().domain}/downloadFile/${this.getAttribute('value')}/${c.currentEngagement._id}/${dom.innerText}?option=view`, '_blank')
        }
      })
    }
  }
  var findAssigneeName = function (data) {
    if (c.currentEngagement || data.id !== '') {
      const aclIndex = c.currentEngagement.acl.findIndex((acl) => {
        return acl.id.toString() === data.id.toString() && acl.role === data.role
      })
      if (aclIndex !== -1) {
        return c.currentEngagement.acl[aclIndex].userInfo.firstName + ' ' + c.currentEngagement.acl[aclIndex].userInfo.lastName
      }
    }
    return ''
  }
  var addActivity = function (data) {
    let strDate = Utility.dateProcess(data.timestamp, {month: {type: 'string', length: 'short'}, output: 'MM D'})
    let dateDOMID = Utility.dateProcess(data.timestamp, {month: {type: 'string', length: 'short'}, output: 'MM-D'})
    let dateTableDOM

    let now = new Date()
    let strCommentFileValue
    let strOriginalFileName, strUpdatedFileName
    let strOriginalRequestValue, strUpdatedRequestValue
    let strOriginalEngagementValue, strUpdatedEngagementValue
    let strOriginalTodoValue, strUpdatedTodoValue
    var inviteeName = ''

    let strUploadedTime, strName, strAction
    if (now - data.timestamp <= THIRTY_MIN) {
      // If upload happened in 30min, uploaded time is Just Now.
      strUploadedTime = 'Just Now'
    } else {
      strUploadedTime = Utility.dateProcess(data.timestamp, {month: {type: 'string', length: 'short'}, output: 'hh:mm'})
    }

    let nowDate = Utility.dateProcess(now, {month: {type: 'string', length: 'short'}, output: 'MM D'})
    if (nowDate === strDate) {
      strDate = 'Today'
      dateDOMID = 'today'
    }

    if (document.getElementById('w-activity-date-' + dateDOMID)) {
      // If the activity date is already exists in our activity.
      dateTableDOM = document.getElementById('w-activity-date-' + dateDOMID)
    } else {
      dateTableDOM = addDayToActivityTable(strDate, dateDOMID)
    }

    if (data.userID._id.toString() === ctrl.currentUser._id.toString() && data.operation !== 'ONBOARD') {
      strName = 'You'
    } else if (data.operation === 'ONBOARD') {
      if (data.changes.updated) {
        // Set Email as name if name does not exists.
        if (data.changes.updated.firstName) {
          strName = data.changes.updated.firstName + ' ' + data.changes.updated.lastName
        } else {
          strName = data.changes.updated.email
        }
      }
    } else {
      // Set Email as name if name does not exists.
      if (data.userID.firstName) {
        strName = data.userID.firstName + ' ' + data.userID.lastName
      } else {
        strName = data.userID.email
      }
    }

    if (data.type.includes('Comment')) {
      if (data.changes.updated && data.changes.updated.file) {
        let fileIndex = c.currentEngagement.files.findIndex((file) => {
          return file._id.toString() === data.changes.updated.file.toString()
        })
        if (fileIndex !== -1) {
          strCommentFileValue = {text: c.currentEngagement.files[fileIndex].name, value: data.changes.updated.todoID}
        }
      }
    } else if (data.type.includes('Engagement')) {
      if (data.changes) {
        if (data.operation === 'CREATE') {
          if (data.changes.updated && data.changes.updated.name) {
            strUpdatedEngagementValue = {text: data.changes.updated.name, value: data.changes.updated._id}
          }
        } else if (data.operation === 'DELETE') {
          if (data.type.includes('user') && data.changes.original && data.changes.original.name) {
            strOriginalEngagementValue = {text: data.changes.original.name, value: data.changes.original._id}
          }
        }
      }
    } else if (data.type === 'File') {
      if (data.changes.original && data.changes.original._id && Array.isArray(data.changes.original._id)) {
        if (data.changes.original._id.length > 1) {
          strOriginalFileName = {text: data.changes.original._id.length + ' files'}
        } else {
          for (let i = 0; i < ctrl.currentFiles.length; i++) {
            if (ctrl.currentFiles[i]._id.toString() === data.changes.original._id[0].toString()) {
              strOriginalFileName = {text: ctrl.currentFiles[i].name, value: data.changes.original._id[0]}
            }
          }
          if (!strOriginalFileName) {
            strOriginalFileName = {text: '1 file'}
          }
        }
      } else {
        if (data.changes.original && data.changes.original.name) {
          strOriginalFileName = {text: data.changes.original.name, value: data.changes.original._id}
        }
        if (data.changes.updated && data.changes.updated.name) {
          strUpdatedFileName = {text: data.changes.updated.name, value: data.changes.updated._id}
        }
      }
// TODO - code cleanup
    // } else if (data.type === 'Request') {
    //   if (data.changes && data.changes.original && data.changes.original.name) {
    //     strOriginalRequestName = {text: data.changes.original.name, value: data.changes.original._id}
    //   }
    //   if (data.changes && data.changes.updated && data.changes.updated.name) {
    //     let reqID
    //     if (data.changes.original._id) {
    //       reqID = data.changes.original._id
    //     } else if (data.changes.updated._id) {
    //       reqID = data.changes.updated._id
    //     }
    } else if (data.type.includes('Request')) {
      if (data.changes) {
        if (data.operation === 'CREATE' || data.operation === 'DELETE') {
          if (data.changes.original && data.changes.original.name) {
            strOriginalRequestValue = {text: data.changes.original.name, value: data.changes.original._id}
          }
          if (data.changes.updated && data.changes.updated.name) {
            strUpdatedRequestValue = {text: data.changes.updated.name, value: data.changes.updated._id}
          }
        } else if (data.operation === 'UPDATE') {
          if (data.type.includes('name') && data.changes.original && data.changes.original.name) {
            strOriginalRequestValue = {text: data.changes.original.name, value: data.changes.original._id}
          } else if ((data.type.includes('file') || data.type.includes('withdraw')) && data.changes.original && data.changes.original.currentFile) {
            let fileIndex = c.currentEngagement.files.findIndex((file) => {
              return file._id.toString() === data.changes.original.currentFile.toString()
            })

            if (fileIndex !== -1) {
              strOriginalRequestValue = {text: c.currentEngagement.files[fileIndex].name, value: data.changes.original._id}
            }
          }

          if (data.type.includes('name') && data.changes.updated && data.changes.updated.name) {
            strUpdatedRequestValue = {text: data.changes.updated.name, value: data.changes.updated._id}
          } else if ((data.type.includes('file') || data.type.includes('withdraw')) && data.changes.updated && data.changes.updated.currentFile) {
            let fileIndex = c.currentEngagement.files.findIndex((file) => {
              return file._id.toString() === data.changes.updated.currentFile.toString()
            })

            if (fileIndex !== -1) {
              strUpdatedRequestValue = {text: c.currentEngagement.files[fileIndex].name, value: data.changes.original._id}
            }
          }
        }
      }
    } else if (data.type === 'Integration') {
      if (data.changes.updated && data.changes.updated.folderName) {
        strUpdatedFileName = {text: data.changes.updated.folderName}
      }
    } else if (data.type.includes('Todo')) {
      if (data.operation === 'CREATE' || data.operation === 'DELETE') {
        if (data.changes.original && data.changes.original.name) {
          strOriginalTodoValue = {text: data.changes.original.name, value: data.changes.original._id}
        }

        if (data.changes.original && data.changes.original !== undefined && data.changes.updated && data.changes.updated.name) {
          strUpdatedTodoValue = {text: data.changes.updated.name, value: data.changes.original._id}
        }
      } else if (data.operation === 'UPDATE') {
        if (data.type.includes('name') && data.changes.original && data.changes.original.name) {
          strOriginalTodoValue = {text: data.changes.original.name, value: data.changes.original._id}
        } else if (data.type.includes('description') && data.changes.original && data.changes.original.description) {
          strOriginalTodoValue = {text: data.changes.original.description, value: data.changes.original._id}
        } else if (data.type.includes('category') && data.changes.original && data.changes.original.category) {
          let categoryIndex = c.currentEngagement.categories.findIndex((category) => {
            return category._id.toString() === data.changes.original.category.toString()
          })
          if (categoryIndex !== -1) {
            strOriginalTodoValue = {text: c.currentEngagement.categories[categoryIndex].name, value: data.changes.original._id}
          }
        } else if (data.type.includes('dueDate') && data.changes.original && data.changes.original.dueDate) {
          let date = new Date(data.changes.original.dueDate)
          let formattedDate = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear()
          strOriginalTodoValue = {text: formattedDate, value: data.changes.original._id}
        }

        if (data.changes.updated && data.changes.updated.name) {
          strUpdatedTodoValue = {text: data.changes.updated.name, value: data.changes.updated._id}
        }
        if (data.type.includes('description') && data.changes.updated && data.changes.updated.description) {
          strUpdatedTodoValue = {text: data.changes.updated.description, value: data.changes.original._id}
        } else if (data.type.includes('category') && data.changes.updated && data.changes.updated.category) {
          let categoryIndex = c.currentEngagement.categories.findIndex((category) => {
            return category._id.toString() === data.changes.updated.category.toString()
          })
          if (categoryIndex !== -1) {
            strUpdatedTodoValue = {text: c.currentEngagement.categories[categoryIndex].name, value: data.changes.original._id}
          }
        } else if (data.type.includes('dueDate') && data.changes.updated && data.changes.updated.dueDate) {
          let date = new Date(data.changes.updated.dueDate)
          let formattedDate = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear()
          strUpdatedTodoValue = {text: formattedDate, value: data.changes.original._id}
        } else if (data.type.includes('completed') && data.changes.updated && data.changes.updated.completed !== null) {
          strUpdatedTodoValue = {text: data.changes.updated.completed ? 'complete' : 'incomplete', value: data.changes.original._id}
        } else if (data.type.includes('auditorAssignee') && data.changes.updated && data.changes.updated.auditorAssignee) {
          let name = findAssigneeName({id: data.changes.updated.auditorAssignee, role: 'AUDITOR'})
          strUpdatedTodoValue = {text: (name !== '') ? name : data.changes.updated.auditorAssignee, value: data.changes.original._id}
        } else if (data.type.includes('clientAssignee') && data.changes.updated && data.changes.updated.clientAssignee) {
          let name = findAssigneeName({id: data.changes.updated.clientAssignee, role: 'CLIENT'})
          strUpdatedTodoValue = {text: (name !== '') ? name : data.changes.updated.clientAssignee, value: data.changes.original._id}
        }
      }
    }

    if (data.operation === 'CREATE') {
      if (data.type === 'File' ||
        (data.type === 'Integration' && data.changes.updated &&
        data.changes.updated.integrationType && ~['LOCAL', 'GDRIVE'].indexOf(data.changes.updated.integrationType))) {
        strAction = 'uploaded'
      } else if (data.type === 'Template') {
        strAction = 'set template for Requests'
      } else if (data.type === 'Comment') {
        strAction = 'added a new comment '
      } else {
        strAction = 'created'
        if (data.type === 'Request') {
          strAction += ' a new request '
        } else if (data.type === 'Engagement') {
          strAction += ' a new engagement '
        } else if (data.type === 'Todo') {
          strAction += ' a new todo.'
        }
      }
    } else if (data.operation === 'DELETE') {
      if (data.type.includes('Engagement') && data.type.includes('user')) {
        strAction = 'removed'
      } else {
        strAction = 'deleted'
      }
    } else if (data.operation === 'UPDATE') {
      if (data.type === 'File' && data.changes.updated && data.changes.updated.requestID) {
        strAction = 'sorted'
      } else if (data.type.includes('Request')) {
        if (data.type.includes('file')) {
          if (strOriginalRequestValue) {
            strAction = 'replaced'
          } else {
            strAction = 'uploaded'
          }
        } else if (data.type.includes('withdraw')) {
          strAction = 'removed'
        } else if (data.type.includes('name')) {
          strAction = 'updated'
        }
      } else {
        strAction = 'updated'
      }
    } else if (data.operation === 'INVITE') {
      strAction = 'invited'
      if (data.changes.updated.firstName) {
        inviteeName = data.changes.updated.firstName
      }
      if (data.changes.updated.lastName) {
        inviteeName = inviteeName + ' ' + data.changes.updated.lastName
      }
    } else if (data.operation === 'ONBOARD') {
      strAction = 'joined the engagement '
    }
    var activityObject = {}
    var activityDOM = Forge.build({'dom': 'div', 'class': 'w-activity-activityDIV', 'id': 'w-activity-' + data._id})
    activityObject.domID = 'w-activity-' + data._id
    activityObject.text = ''
    var timeStamp = Forge.build({'dom': 'div', 'class': 'w-activity-timeStamp'})
    var timeStampIcon = Forge.build({'dom': 'span', 'class': 'icon auvicon-line-clock w-activity-timeStampIcon'})
    var timeStampText = Forge.build({'dom': 'div', 'class': 'w-activity-timeStampText', 'id': 'w-activity-timestamp-' + data._id, 'text': strUploadedTime})
    var borderCircle = Forge.build({'dom': 'span', 'class': 'w-activity-border-circle'})
    activityObject.time = strUploadedTime

    timeStamp.appendChild(borderCircle)
    timeStamp.appendChild(timeStampIcon)
    timeStamp.appendChild(timeStampText)

    activityDOM.appendChild(timeStamp)

    let activityText
    if (c.currentUser.type === 'AUDITOR') {
      activityText = Forge.build({'dom': 'div', 'class': 'w-activity-text'})
    } else {
      activityText = Forge.build({'dom': 'div', 'class': 'w-activity-text-client'})
    }

    let activityUserName = Forge.build({'dom': 'span', 'class': 'w-activity-name', 'id': 'w-activity-username-' + data._id, text: strName})
    activityText.appendChild(activityUserName)
    activityText.innerHTML = activityText.innerHTML + ' ' + strAction + ' '
    activityObject.text = strName + ' ' + strAction
    if ((strUpdatedFileName && strUpdatedFileName.text) || (strOriginalFileName && strOriginalFileName.text)) {
      if (data.operation === 'UPDATE' && data.type === 'File' && strUpdatedFileName && strUpdatedFileName.text) {
        // When Name updates...
        if (strOriginalFileName && strOriginalFileName.text) {
          let activityOldFileNameDOM = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-oldFilename-' + data._id, 'text': strOriginalFileName.text, 'value': strOriginalFileName.value})
          activityText.innerHTML = activityText.innerHTML + ' name from  '
          activityObject.text = activityObject.text + ' name from '
          activityText.appendChild(activityOldFileNameDOM)
          activityObject.text = activityObject.text + ' ' + strOriginalFileName.text
          if (strOriginalFileName.value) {
            fileClickEvent(activityOldFileNameDOM)
          }
        }
        activityText.innerHTML = activityText.innerHTML + ' to'
        activityObject.text = activityObject.text + ' to'
      }

      if (data.type === 'Integration') {
        activityText.innerHTML = activityText.innerHTML + ' folder'
        activityObject.text = activityObject.text + ' folder'
      }
      if (strUpdatedFileName) {
        let activityNewFileName = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-newFilename-' + data._id, 'text': ' ' + strUpdatedFileName.text, 'value': strUpdatedFileName.value})
        activityObject.text = activityObject.text + ' ' + strUpdatedFileName.text
        if (strUpdatedFileName.value) {
          fileClickEvent(activityNewFileName)
        }
        activityText.appendChild(activityNewFileName)
      } else {
        let activityNewFileName = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-newFilename-' + data._id, 'text': ' ' + strOriginalFileName.text, 'value': strOriginalFileName.value})
        if (strOriginalFileName.value) {
          fileClickEvent(activityNewFileName)
        }
        activityText.appendChild(activityNewFileName)
        activityObject.text = activityObject.text + ' ' + strOriginalFileName.text
      }
    }
    if (data.operation === 'INVITE' && inviteeName) {
      let inviteeNameDOM = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-inviteeName-' + data._id, 'text': ' ' + inviteeName})
      let engName = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-engName-' + data._id, 'text': ' ' + c.currentEngagement.name})
      activityText.appendChild(inviteeNameDOM)
      activityText.appendChild(document.createTextNode('  to the engagement.'))
      activityObject.text = activityObject.text + ' ' + inviteeName + ' to the engagement.'
    }
    if (data.operation === 'ONBOARD') {
      let engName = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-engName-' + data._id, 'text': ' ' + c.currentEngagement.name})
      activityText.appendChild(engName)
      activityObject.text = activityObject.text + ' ' + c.currentEngagement.name
      activityText.appendChild(document.createTextNode('.'))
      activityObject.text = activityObject.text + '.'
    }

    if (data.type.includes('Comment')) {
      let todoName
      if (data.changes.updated.todoID) {
        let todoIndex = c.currentEngagement.todos.findIndex((todo) => {
          return todo._id.toString() === data.changes.updated.todoID.toString()
        })

        if (todoIndex !== -1) {
          todoName = c.currentEngagement.todos[todoIndex].name
        }
      }
      if (data.operation === 'CREATE') {
        if (todoName) {
          let activityToDoName = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-todoValue-' + data._id, 'text': todoName})
          activityText.appendChild(document.createTextNode(' in '))
          activityObject.text = activityObject.text + ' in '
          activityText.appendChild(activityToDoName)
          activityObject.text = activityObject.text + ' ' + todoName
        } else {
          activityText.appendChild(document.createTextNode(' in a unnamed todo'))
          activityObject.text = activityObject.text + ' in a unnamed todo'
        }

        if (strCommentFileValue) {
          let activityCommentFileValue = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-commentFileValue-' + data._id, 'text': strCommentFileValue.text})
          activityText.appendChild(document.createTextNode(' attached with '))
          activityObject.text = activityObject.text + ' attached with '
          activityText.appendChild(activityCommentFileValue)
          activityObject.text = activityObject.text + ' ' + strCommentFileValue.text
        }
      }
      activityText.appendChild(document.createTextNode('.'))
      activityObject.text = activityObject.text + '.'
    }

    if (data.type.includes('Request')) {
      let todoName
      if (data.changes.updated.todoID) {
        let todoIndex = c.currentEngagement.todos.findIndex((todo) => {
          return todo._id.toString() === data.changes.updated.todoID.toString()
        })

        if (todoIndex !== -1) {
          todoName = c.currentEngagement.todos[todoIndex].name
        }
      }
      if (data.operation === 'CREATE') {
        if (strUpdatedRequestValue) {
          let activityNewRequestName = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-newRequestName-' + data._id, text: strUpdatedRequestValue.text})
          activityText.appendChild(activityNewRequestName)
          activityObject.text = activityObject.text + ' ' + strUpdatedRequestValue.text
          activityText.appendChild(document.createTextNode(' in '))
          activityObject.text = activityObject.text + ' in '
        }
      } else if (data.operation === 'UPDATE') {
        if (strUpdatedRequestValue || strOriginalRequestValue) {
          if (data.type.includes('name')) {
            if (strOriginalRequestValue && strUpdatedRequestValue) {
              let activityOldRequestValue = Forge.build({ 'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-oldRequestValue-' + data._id, 'text': strOriginalRequestValue.text })
              let activityUpdatedRequestValue = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-newRequestValue-' + data._id, 'text': ' ' + strUpdatedRequestValue.text, 'value': strUpdatedRequestValue.value})
              activityText.appendChild(document.createTextNode(` a request's name from `))
              activityObject.text = activityObject.text + ` a request's name from `
              activityText.appendChild(activityOldRequestValue)
              activityObject.text = activityObject.text + ' ' + strOriginalRequestValue.text
              activityText.appendChild(document.createTextNode(' to'))
              activityObject.text = activityObject.text + ' to'
              activityText.appendChild(activityUpdatedRequestValue)
              activityObject.text = activityObject.text + ' ' + strUpdatedRequestValue.text
              activityText.appendChild(document.createTextNode(' in '))
              activityObject.text = activityObject.text + ' in '
            }
          } else if (data.type.includes('file')) {
            if (strOriginalRequestValue) {
              let activityOldRequestValue = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-oldRequestValue-' + data._id, 'text': strOriginalRequestValue.text})
              activityText.appendChild(document.createTextNode(' '))
              activityObject.text = activityObject.text + ' '
              activityText.appendChild(activityOldRequestValue)
              activityObject.text = activityObject.text + ' ' + strOriginalRequestValue.text
              activityText.appendChild(document.createTextNode(' with '))
              activityObject.text = activityObject.text + ' with '
            }

            if (strUpdatedRequestValue) {
              let activityNewRequestValue = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-newRequestValue-' + data._id, 'text': ' ' + strUpdatedRequestValue.text, 'value': strUpdatedRequestValue.value})
              let activityRequestName = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-requestName-' + data._id, 'text': data.changes.original.name})
              activityText.appendChild(activityNewRequestValue)
              activityObject.text = activityObject.text + ' ' + strUpdatedRequestValue.text
              activityText.appendChild(document.createTextNode(' in request '))
              activityObject.text = activityObject.text + ' in request '
              activityText.appendChild(activityRequestName)
              activityObject.text = activityObject.text + ' ' + data.changes.original.name
              activityText.appendChild(document.createTextNode(' within '))
              activityObject.text = activityObject.text + ' within '
            } else {
              activityText.appendChild(document.createTextNode(' in '))
              activityObject.text = activityObject.text + ' in '
            }
          } else if (data.type.includes('withdraw')) {
            if (strOriginalRequestValue) {
              let activityOldRequestValue = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-oldRequestValue-' + data._id, 'text': strOriginalRequestValue.text})
              activityText.appendChild(document.createTextNode(' '))
              activityObject.text = activityObject.text + ' '
              activityText.appendChild(activityOldRequestValue)
              activityObject.text = activityObject.text + ' ' + strOriginalRequestValue.text
              activityText.appendChild(document.createTextNode(' from request '))
              activityObject.text = activityObject.text + ' from request '
              let activityRequestName = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-requestName-' + data._id, 'text': data.changes.original.name})
              activityText.appendChild(activityRequestName)
              activityObject.text = activityObject.text + ' ' + data.changes.original.name
              activityText.appendChild(document.createTextNode(' within '))
              activityObject.text = activityObject.text + ' within '
            }
          }
        }
      } else if (data.operation === 'DELETE') {
        if (strOriginalRequestValue) {
          activityText.appendChild(document.createTextNode(' request '))
          let activityRequestName = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-requestName-' + data._id, 'text': strOriginalRequestValue.text, 'value': strOriginalRequestValue.value})
          activityText.appendChild(activityRequestName)
          activityObject.text = activityObject.text + ' ' + strOriginalRequestValue.text
          activityText.appendChild(document.createTextNode(` from `))
          activityObject.text = activityObject.text + ` from `
        }
      }

      if (todoName) {
        let activityToDoName = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-ToDoName-' + data._id, text: todoName})
        activityText.appendChild(activityToDoName)
        activityObject.text = activityObject.text + ' ' + todoName
        activityText.appendChild(document.createTextNode('.'))
        activityObject.text = activityObject.text + '.'
      } else {
        activityText.appendChild(document.createTextNode('a unnamed todo.'))
        activityObject.text = activityObject.text + 'a unnamed todo.'
      }
    }

    if (data.type.includes('Engagement')) {
      if (data.operation === 'CREATE') {
        if (strOriginalEngagementValue || strUpdatedEngagementValue) {
          let activityNewEngagementValue = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-engagementValue-' + data._id, text: strUpdatedEngagementValue.text})
          activityText.appendChild(activityNewEngagementValue)
          activityObject.text = activityObject.text + ' ' + strUpdatedEngagementValue.text
          activityText.appendChild(document.createTextNode('.'))
          activityObject.text = activityObject.text + '.'
        }
      } else if (data.operation === 'DELETE') {
        if (strOriginalEngagementValue || strUpdatedEngagementValue) {
          if (data.type.includes('user')) {
            let engagementName = c.currentEngagement.name
            let activityEngagementValue = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-engagementValue-' + data._id, text: strOriginalEngagementValue.text})
            let activityEngagementName = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-engagementName-' + data._id, text: engagementName})
            activityText.appendChild(activityEngagementValue)
            activityObject.text = activityObject.text + ' ' + strOriginalEngagementValue.text
            activityText.appendChild(document.createTextNode(' from the engagement '))
            activityObject.text = activityObject.text + ' from the engagement '
            activityText.appendChild(activityEngagementName)
            activityObject.text = activityObject.text + ' ' + engagementName
            activityText.appendChild(document.createTextNode('.'))
            activityObject.text = activityObject.text + '.'
          }
        }
      }
    }

    if (data.type.includes('Todo')) {
      /* if (data.operation === 'CREATE') {
        let activityNewTodoName = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-newTodoName-' + data._id, text: strUpdatedTodoValue.text})
        activityText.appendChild(activityNewTodoName)
        activityObject.text = activityObject.text + ' ' + strUpdatedTodoValue.text
        activityText.appendChild(document.createTextNode('.'))
        activityObject.text = activityObject.text + '.'
      } else */
      if (data.operation === 'UPDATE') {
        if (strOriginalTodoValue || strUpdatedTodoValue) {
          let todoName = data.changes.original.name
          let strType
          if (data.type.includes('name')) {
            strType = 'name'
          } else if (data.type.includes('description')) {
            strType = 'description'
          } else if (data.type.includes('category')) {
            strType = 'category'
          } else if (data.type.includes('dueDate')) {
            strType = 'due date'
          } else if (data.type.includes('completed')) {
            strType = 'status'
          } else if (data.type.includes('auditorAssignee')) {
            strType = 'auditor assignee'
          } else if (data.type.includes('clientAssignee')) {
            strType = 'client assignee'
          }

          if (data.type.includes('description')) {
            let activityToDoName = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-todoName-' + data._id, 'text': todoName})
            activityText.appendChild(activityToDoName)
            activityObject.text = activityObject.text + ' ' + todoName
            activityText.appendChild(document.createTextNode(` todo's ${strType}.`))
            activityObject.text = activityObject.text + ` todo's ${strType}.`
          } else if (strOriginalTodoValue) {
            let activityOldToDoValue = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-oldTodoValue-' + data._id, 'text': strOriginalTodoValue.text})
            let prefixText = ''
            if (!(data.type.includes('name'))) {
              if (todoName) {
                let activityToDoName = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-todoName-' + data._id, 'text': todoName})
                activityText.appendChild(activityToDoName)
                activityObject.text = activityObject.text + ' ' + todoName
              } else {
                prefixText = ' a unnamed'
              }
            } else {
              prefixText = ' a'
            }
            prefixText = prefixText + ` todo's ${strType} from `
            activityText.appendChild(document.createTextNode(prefixText))
            activityObject.text = activityObject.text + prefixText
            activityText.appendChild(activityOldToDoValue)
            activityObject.text = activityObject.text + ' ' + strOriginalTodoValue.text
            activityText.appendChild(document.createTextNode(' to'))
            activityObject.text = activityObject.text + ' to'
          } else if (data.type.includes('completed') || data.type.includes('auditorAssignee') ||
                     data.type.includes('clientAssignee') || data.type.includes('category') ||
                     data.type.includes('dueDate')) {
            if (todoName) {
              let activityToDoName = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-todoName-' + data._id, 'text': todoName})
              activityText.appendChild(activityToDoName)
              activityObject.text = activityObject.text + ' ' + todoName
            } else {
              activityText.appendChild(document.createTextNode('a unnamed'))
              activityObject.text = activityObject.text + 'a unnamed'
            }
            activityText.appendChild(document.createTextNode(` todo's ${strType} to `))
            activityObject.text = activityObject.text + ` todo's ${strType} to `
          } else {
            activityText.appendChild(document.createTextNode(` a unnamed todo's ${strType} to `))
            activityObject.text = activityObject.text + ` a unnamed todo's ${strType} to `
          }
          if (strUpdatedTodoValue !== undefined && !data.type.includes('description')) {
            let newValue = strUpdatedTodoValue || { text: 'unknown', value: todoName }

            let activityNewTodoValue = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-newTodoValue-' + data._id, 'text': ' ' + newValue.text, 'value': newValue.value})
            activityText.appendChild(activityNewTodoValue)
            /* if (activityNewTodoValue.value) {
              requestClickEvent(activityNewTodoValue)
            } */
            activityObject.text = activityObject.text + ' ' + newValue.text
            activityText.appendChild(document.createTextNode('.'))
            activityObject.text = activityObject.text + '.'
          }
        }
      } else if (data.operation === 'DELETE') {
        if (strOriginalTodoValue) {
          activityText.appendChild(document.createTextNode(' todo '))
          activityObject.text = activityObject.text + ' ' + ' todo '
          let activityTodoName = Forge.build({'dom': 'span', 'class': 'w-activity-activeColor', 'id': 'w-activity-todoName-' + data._id, 'text': strOriginalTodoValue.text, 'value': strOriginalTodoValue.value})
          activityText.appendChild(activityTodoName)
          activityObject.text = activityObject.text + ' ' + strOriginalTodoValue.text
        } else {
          activityText.appendChild(document.createTextNode(' a untitled todo'))
        }
        activityText.appendChild(document.createTextNode(`.`))
        activityObject.text = activityObject.text + `.`
      }
    }

    let activityLog = {}
    let date = new Date(data.timestamp)
    let options = { weekday: 'long', year: 'numeric', month: 'short', day: 'numeric', hour: '2-digit', minute: '2-digit' }
    let dateString = date.toLocaleTimeString('en-us', options)
    activityLog.date = dateString
    if (data.userID.firstName) {
      activityLog.user = data.userID.firstName + ' ' + data.userID.lastName
    } else {
      activityLog.user = data.userID.email
    }
    activityLog.message = activityObject.text
    activityLogList.push(activityLog)
    activityList.push(activityObject)
    self.trigger('add-activity', activityList)
    activityDOM.appendChild(activityText)
    dateTableDOM.appendChild(activityDOM)
  }

  registerEvents()

  return this
}

Widget_ActivityFeed.prototype = Object.create(WidgetTemplate.prototype)
Widget_ActivityFeed.prototype.constructor = Widget_ActivityFeed

module.exports = Widget_ActivityFeed
