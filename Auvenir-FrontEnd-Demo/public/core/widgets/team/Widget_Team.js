'use strict'

import WidgetTemplate from '../WidgetTemplate'
import log from '../../../js/src/log'
import Forge from '../../Forge'

import styles from './Widget_Team.css'

var Widget_Team = function (ctrl, container) {
  WidgetTemplate.call(this)

  this.name = 'Team Widget'
  this.parent = container
  var self = this
  var c = ctrl
  var text = c.lang.teamWidget
  var checkIDs = []
  var trashIDs = []
  var rowIDs = []
  var teamMembers = []
  var contacts = []

  this.buildContent = function () {
    var parentDiv = Forge.build({'dom': 'div', id: 'team-parentContainer'})

    var addBtn = Forge.buildBtn({'text': text.addBtn, id: 'team-addMember-btn', 'type': 'primarySmall', margin: '0px'}).obj
    var inviteBtn = Forge.buildBtn({'text': text.inviteBtn, id: 'team-inviteMember-btn', 'type': 'primarySmall', margin: '0px 0px 0px 10px'}).obj
    var members = Forge.build({dom: 'div'})
    members.appendChild(addBtn)
    members.appendChild(inviteBtn)
    var memberBtns = members.innerHTML

    var tableContainer = Forge.build({dom: 'div'})
    tableContainer.innerHTML = `
      <table class="ui very basic table">
        <div class="w-team-btnHolder">
          <div id="team-bulk-dropdown" class="ui dropdown team-bulkDdl">
           <div id ="team-bulk-dropdown-inner" class="text team-bulkDdlTxt">${text.bulk}</div>
           <i class="dropdown icon"></i>
           <div id="team-bulk-menu" class="menu">
             <div id="team-delete-btn" class="item">${c.lang.delete}</div>
           </div>
         </div>
         <div class="auv-inputDD-container team-ddContainer" id="team-addmem-dropdown">
           <input id="team-input-addMem" class="auv-input auv-inputDD team-ddInput">
           <ul id="team-myDropdown" class="ddlLink inputDdl team-ddl">
           </ul>
         </div>
         ${memberBtns}
        </div>
        <thead>
          <tr class="left aligned">
            <th class="w-team-checkTitle w-team-tableTitle" style="width: 30px; line-height: 10px !important; padding-left: 20px !important">
              <input type="checkbox" id="team-checkAll">
            </th>
            <th class="w-team-tableHead">${text.memberTitle}</th>
            <th class="w-team-tableHead">${text.roleTitle}</th>
            <th class="w-team-tableHead w-team-tablePermission">${text.permissionTitle}</th>
            <th></th>
          </tr>
        </thead>
        <tbody id="w-team-tableBody">
        </tbody>
      </table>`
    parentDiv.appendChild(tableContainer)

    var emptyList = Forge.build({'dom': 'div', 'id': 'team-emptyDiv', style: 'width: 500px; margin: auto'})
    var teamImg = Forge.build({'dom': 'img', 'src': 'images/illustrations/illustration-clients.svg', 'class': 'w-team-emptyClipboard'})
    var emptyText = Forge.build({'dom': 'p', 'text': text.empty, 'class': 'w-team-emptyText'})

    emptyList.appendChild(teamImg)
    emptyList.appendChild(emptyText)

    var tableContainerDiv = Forge.build({'dom': 'div', 'style': 'margin-top: -20px;'})
    parentDiv.appendChild(emptyList)
    parentDiv.appendChild(tableContainerDiv)

    return parentDiv
  }

  /* Add Event Listeners to all elements. Resets every time its called by removing any event listener first. */
  this.addEventListeners = function () {
    /* Show Bulk action items */
    $('#team-bulk-dropdown').off('click').on('click', function () {
      $('#team-bulk-menu').toggleClass('show')
      return false
    })

    /* Bulk Delete on click */
    $('#team-delete-btn').off('click').on('click', function () {
      for (var i = 0; i < checkIDs.length; i++) {
        if (document.getElementById(checkIDs[i]).checked === true) {
          var splitCheckID = checkIDs[i].split('-')
          var index = splitCheckID[splitCheckID.length - 1]
          for (var j = 0; j < teamMembers.length; j++) {
            // Match the trash index to the teamMember index
            var secondIndex = j.toString()
            if (index === secondIndex) {
              let deleteUserID = teamMembers[i].id
              // removeTeamMember requires UserID and engagementID as arguments
              c.removeTeamMember(deleteUserID, c.currentEngagement._id)
            }
          }
        }
      }
    })

    /* Check/Uncheck all Checkboxes */
    $('#team-checkAll').off('click').on('click', function () {
      if (document.getElementById('team-checkAll').checked) {
        for (var i = 0; i < checkIDs.length; i++) {
          document.getElementById(checkIDs[i]).checked = true
          document.getElementById(trashIDs[i]).style.display = 'block'
          document.getElementById(rowIDs[i]).style.background = '#e9e9e9'
        }
      } else {
        for (var j = 0; j < checkIDs.length; j++) {
          document.getElementById(checkIDs[j]).checked = false
          document.getElementById(trashIDs[j]).style.display = 'none'
          document.getElementById(rowIDs[j]).style.background = '#fff'
        }
      }
    })

    /* Show trash can on selected checkboxes and highlight background */
    if (checkIDs.length > 0) {
      checkIDs.forEach(function (entry) {
        document.getElementById(entry).addEventListener('click', function () {
          for (var i = 0; i < checkIDs.length; i++) {
            if (checkIDs[i] === entry) {
              if (document.getElementById(checkIDs[i]).checked === true) {
                document.getElementById(trashIDs[i]).style.display = 'block'
                document.getElementById(rowIDs[i]).style.background = '#e9e9e9'
              } else {
                document.getElementById(trashIDs[i]).style.display = 'none'
                document.getElementById(rowIDs[i]).style.background = '#fff'
              }
            }
          }
        })
      })
    }

    /* Delete Team Member on Trash click */
    if (trashIDs.length > 0) {
      trashIDs.forEach(function (entry) {
        document.getElementById(entry).addEventListener('click', function () {
          var splitID = entry.split('-')
          var index = splitID[splitID.length - 1]
          for (var i = 0; i < teamMembers.length; i++) {
            // Match the trash index to the teamMember index
            var secondIndex = i.toString()
            if (index === secondIndex) {
              let deleteUserID = teamMembers[i].id
              // removeTeamMember requires UserID and engagementID as arguments
              c.removeTeamMember(deleteUserID, c.currentEngagement._id)
            }
          }
        })
      })
    }

    /* Add New Member Toggle */
    $('#team-addMember-btn').off('click').on('click', function () {
      $('#team-addmem-dropdown').toggleClass('team-ddShow')
    })

    $('#team-input-addMem').off('click').on('click', function () {
      document.getElementById('team-myDropdown').classList.toggle('show')
    })

    var dropdownOne = document.getElementById('team-bulk-menu')
    var dropdownTwo = document.getElementById('team-myDropdown')
    window.onclick = function (event) {
      if (!event.target.matches('#team-bulk-dropdown-inner')) {
        if (dropdownOne.classList.contains('show')) {
          dropdownOne.classList.remove('show')
        }
      }
      if (!event.target.matches('#team-input-addMem')) {
        if (dropdownTwo.classList.contains('show')) {
          dropdownTwo.classList.remove('show')
        }
      }
    }

    $('#team-myDropdown a').off('click').on('click', function () {
      $('#team-input-addMem').val($(this).text())

      var user = document.getElementById('team-input-addMem').value
      for (var i = 0; i < contacts.length; i++) {
        if (contacts[i].firstName + ' ' + contacts[i].lastName === user) {
          var data = {
            userID: contacts[i]._id,
            engagementID: c.currentEngagement._id,
            role: contacts[i].jobTitle,
            firstName: contacts[i].firstName,
            lastName: contacts[i].lastName
          }
          c.addTeamMember(data)
        }
      }

      if (dropdownTwo.classList.contains('show')) {
        dropdownTwo.classList.remove('show')
      }
    })

    /* Open the Add New Member Modal */
    $('#team-inviteMember-btn').off('click').on('click', function () {
      c.displayInviteNewMemberModal()
    })
  }

  /* Loads the team members from the controller and adds them to the table */

  this.addTeamList = function () {
    checkIDs = []
    trashIDs = []
    rowIDs = []
    teamMembers = c.currentTeam
    $('#w-team-tableBody').empty()
    for (var i = 0; i < teamMembers.length; i++) {
      var teamCheck = 'team-checkbox-' + i
      var teamTrash = 'team-trash-' + i
      var teamRow = 'team-row-' + i
      checkIDs.push(teamCheck)
      trashIDs.push(teamTrash)
      rowIDs.push(teamRow)
      var teamRole = teamMembers[i].role
      var teamName
      var teamPermission = teamMembers[i].permission
      if (teamMembers[i].status === 'Pending') {
        teamName = `<td class='w-team-namePending'>${teamMembers[i].name} (Pending)</td>`
      } else {
        teamName = `<td>${teamMembers[i].name}</td>`
      }

      $('#' + teamRow).remove()
      var memberRow = `
        <tr id=${teamRow}>
          <td style="line-height: 10px !important; padding-left: 20px">
            <input type="checkbox" id=${teamCheck}>
          </td>
          ${teamName}
          <td style="font-weight: bold">${teamRole}</td>
          <td>${teamPermission}</td>
          <td><span id=${teamTrash} class="auvicon-trash w-team-trash"></span></td>
        </tr>`
      $(memberRow).prependTo('#w-team-tableBody')
    }
    self.loadContacts()
    self.addEventListeners()
    self.checkEmpty()
  }

  /* Gets the response from the controller after inviting a new member and calls the addTeamList function */
  this.loadNewMember = function (member) {
    var newMember = {
      name: member.name,
      role: member.role,
      permission: member.permission,
      id: member.id
    }
    teamMembers.push(newMember)
    self.addTeamList()
    document.getElementById('team-input-addMem').innerHTML = ''
    $('#team-input-addMem').val('')
    if (!newMember.isNewUser) {
      self.loadContacts()
    }
  }

  /* Build the dropdown list of pre-existing contacts */
  this.loadContacts = function () {
    contacts = c.myContacts
    document.getElementById('team-myDropdown').innerHTML = ''
    for (var i = 0; i < contacts.length; i++) {
      let aclIndex = c.currentEngagement.acl.findIndex((acl) => {
        return contacts[i]._id.toString() === acl.id.toString()
      })

      if (c.currentUser.type === contacts[i].type && aclIndex === -1) {
        let aTagId = 'team-ddl-a-' + contacts[i]._id
        let jQueryATagId = '#' + aTagId
        let memberLi = `
          <li class="ddlItem auv-inputDdl">
            <a class="ddlText auv-inputDdl-text" id=${aTagId}>${contacts[i].firstName} ${contacts[i].lastName}</a>
          </li>`
        document.getElementById('team-myDropdown').innerHTML += memberLi
      }
    }
  }

  /* The response from the controllers removeTeamMember via the Engagement.js */
  this.removeTeamMember = function (member) {
    for (var i = 0; i < teamMembers.length; i++) {
      if (teamMembers[i].id === member) {
        var rowID = '#team-row-' + i
        $(rowID).remove()
        checkIDs.splice(i, 1)
        trashIDs.splice(i, 1)
        rowIDs.splice(i, 1)
        teamMembers.splice(i, 1)
      }
    }
    self.checkEmpty()
  }

  /* This removes the empty container */
  this.checkEmpty = function () {
    if (teamMembers.length < 1) {
      document.getElementById('team-emptyDiv').style.display = 'inherit'
    } else {
      document.getElementById('team-emptyDiv').style.display = 'none'
    }
  }

  return this
}

Widget_Team.prototype = Object.create(WidgetTemplate.prototype)
Widget_Team.prototype.constructor = Widget_Team

module.exports = Widget_Team
