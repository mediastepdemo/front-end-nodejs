/* globals io, attemptSocketConnect */

'use strict'

import log from '../js/src/log'

/**
 * This is the Socket object
 *
 * @class Socket
 * @param {Object} options
 * @param {number} [options.port=8080] Pass the port to be used.
 * @param {String} [options.protocol=https] Pass the protocol to be used.
 * @param {String} [options.domain=auvenir.com] Pass the domain to be used.
 * @param {Function} connected The connection function callback
 * @param {Function} disconnected The disconnection function callback
 * @param {Function} error The error function callback
 *
 * @property {Object} client A reference to the io client connection created. We can query this in order to get more info about the current connection.
 * @property {}
 */
var Socket = function (options, connected, disconnected, error) {
  var self = this

  var client = null

  /* Parameters */
  var connectedFunc = connected
  var disconnectedFunc = disconnected
  var errorFunc = error

  var reconnectCb = null

  var SOCKET_PORT = options.port
  var SOCKET_URL = options.protocol + '://' + options.domain
  var SOCKET_IO_URL = '../js/lib/socket.io-client/socket.io.js'

  /**
   * Responsible for triggering the initialization of the Socket object and executing the
   * appropriate functionality.  Downloads and setups the socket library, as well as attempts
   * an initial socket connection to the server.
   * @memberof Socket
   */
  var init = function () {
    log('Socket: Initializing socket setup ...')

    if ((typeof connectedFunc !== 'function') || (typeof disconnectedFunc !== 'function') || (typeof errorFunc !== 'function')) {
      return
    }

    installResources(function (err) {
      if (err) {
        errorFunc()
      } else {
        if (err && err.code && err.code === 3) {
          log('Connection is already establish. No need to attempt to connect.')
        } else {
          log('Socket: Resources downloaded')
          attemptConnection()
        }
      }
    })
  }

  /**
   * Will attempt to download the resources needed in order to establish a socket connection
   * to the desired server.
   * @memberof Socket
   * @param {Function} callback - The function that will be called when a result has occured
   *                              while loading the socket.io resource.
   */
  var installResources = function (callback) {
    log('Socket: Downloading and Installing socket.io script ...')
    if (!window.io) {
      var script = document.createElement('script')
      script.type = 'text/javascript'
      script.src = SOCKET_URL + ':' + SOCKET_PORT + '/' + SOCKET_IO_URL

      script.onload = function () {
        log('Socket: Script loaded. ')
        // do stuff with the script
        if (window.io) {
          callback()
        } else {
          callback({code: 1, msg: 'Failed to download and set up socket.io'})
        }
      }

      script.onerror = function (err) {
        if (err) {
          log('Error', err)
        }
        log('Socket: Unable to append script! ')
        callback({code: 2, msg: 'Error occuring while appending socket.io'})
      }

      log('Socket: Appending socket.io script ...')
      document.head.appendChild(script)
    } else {
      log('Socket: Socket.io already installed, checking for active socket connection ...')
      if (client === null) {
        callback()
      } else {
        log('Socket: Socket connection already exists, doing nothing.')
        callback({code: 3, msg: 'Connection already exists'})
      }
    }
  }

  /**
   * Will attempt to connect to a socket and set up the appropriate event handlers.
   * @memberof Socket
   */
  var attemptConnection = function () {
    log('Socket: Attempting to create socket connection ...')
    if (client !== null) {
      client.connect()
    } else {
      client = io.connect(SOCKET_URL + ':' + SOCKET_PORT, {
        reconnection: true,
        reconnectionDelay: 1000,
        reconnectionDelayMax: 5000,
        reconnectionAttempts: 3,
        secure: true
      })

      // Client side code for when socket is connected
      client.on('connect', function () {
        log('Socket: Connected')

        if (reconnectCb !== null) {
          reconnectCb(null)
          reconnectCb = null
          self.reconnected = true
        } else {
          connectedFunc()
        }
      })

      // Client side code for when socket is disconnected
      client.on('disconnect', function () {
        log('Socket: Disconnected')

        client.disconnect() // Stops the client from continually polling the server if the server goes offline

        disconnectedFunc()
      })

      // tell socket.io to never give up :)
      client.on('error', function (err) {
        if (reconnectCb !== null) {
          reconnectCb(err)
          reconnectCb = null
        }

        errorFunc(err)
      })

      // handles a reconnection attempt
      client.on('reconnecting', function () {
        log('Socket: Attempting to reconnect ...')
      })

      // handles a failed reconnection attempt
      client.on('reconnect_failed', function () {
        var err = 'Socket: Connection timed out'
        if (reconnectCb !== null) {
          reconnectCb(err)
          reconnectCb = null
        }
        errorFunc()
      })
    }
  }

  /**
   * Will either re-download the socket.io libraries and set it up before connecting,
   * or attempt to re-connect directly if the library is still set up and available to user
   * @memberof Socket
   * @instance
   */
  this.connectSocket = function () {
    log('Socket: attempting to create a socket connection ...')
    if (client !== null) {
      if (client.disconnected) {
        client.connect()
      } else {
        log('Socket: Active socket connection detected')
      }
    } else {
      attemptSocketConnect()
    }
  }

  /**
   * Will disconnect the current socket connection if it is still active.
   * @memberof Socket
   * @instance
   */
  this.disconnectSocket = function () {
    log('Socket: manually attempting to disconnect the socket connection ...')
    if (client !== null) {
      if (client.connected) {
        client.disconnect()
      }
    }
  }

  /**
   * Responsible for setting a listener for a specific tag event.
   * @memberof Socket
   * @instance
   * @param {String}   tag  - The event tag to be looking for.
   * @param {Function} func - The callback function to be triggered when the event occurs.
   */
  this.listen = function (tag, func) {
    client.on(tag, func)
  }

  /**
   * Responsible for detaching a tag event from the socket object.
   * @memberof Socket
   * @instance
   * @param {String}   tag  - The event tag to be looking for.
   * @param {Function} func - The callback function to be triggered when the event occurs.
   */
  this.detach = function (tag, func) {
    client.removeListener(tag, func)
  }

  /**
   * Responsible for emitting a tag event to the server with JSON payload
   * @memberof Socket
   * @instance
   * @param {String} tag  - The event tag that will be emitted to identify the event.
   * @param {JSON}   data - The callback function to be triggered when the event occurs.
   */
  this.emit = function (tag, data) {
    client.emit(tag, data)
  }

  /**
   * Attempt to connect a socket connection.
   * @param {function} callback - The callback function that should be called with an updated on the attempted socket connection.
   * @memberof Socket
   */
  this.tryConnect = function (callback) {
    log('Socket: manually triggering attempt to connect ...')
    reconnectCb = callback

    // This starts to poll for a connection over and over
    self.connectSocket()
  }

  /**
   * Checks to see if the socket connection is still connected
   * @memberof Socket
   */
  this.isConnected = function () {
    return client.connected
  }

  // Initialize the Socket object.
  init()

  return this
}

module.exports = Socket
