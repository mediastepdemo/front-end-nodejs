var en = {
  admin: 'Admin',
  add: 'Add',
  back: 'Back',
  cancel: 'Cancel',
  category: 'Category',
  contact: 'Contact',
  preview: 'Preview',
  continue: 'Continue',
  create: 'Create',
  close: 'Close',
  saveAndClose: 'Save & Close',
  delete: 'Delete',
  exit: 'Exit',
  general: 'General',
  invite: 'Invite',
  lead: 'Lead',
  letusknow: 'Let us know',
  loading: 'Loading',
  name: 'Name',
  next: 'Next',
  password: 'Password',
  review: 'Review',
  role: 'Role',
  search: 'Search...',
  send: 'Send',
  status: 'Status',
  submit: 'Submit',
  pleaseType: 'Please type here',
  personalComponent: {
    header: 'Please Confirm your Information',
    nameLabel: 'First and Last Name',
    nameError: 'Please enter your first and last name',
    emailLabel: 'Email Address',
    emailError: 'Please type a valid email address.',
    reEmailLabel: 'Re-enter to confirm',
    reEmailError: 'Your emails don\'t match',
    roleFirmLabel: 'Role in Firm',
    roleCompanyLabel: 'Role in Company',
    roles: {
      partner: 'Partner',
      it: 'IT',
      manager: 'Manager',
      auditjunior: 'Audit Junior',
      managingpartner: 'Managing Partner',
      auditsenior: 'Audit Senior',
      consultant: 'Consultant',
      seniormanager: 'Senior Manager',
      other: 'Other (Please specify)'
    },
    roleError: 'Please Enter a valid role',
    phoneLabel: 'Phone Number',
    phoneError: 'Please enter a valid 10-digit phone number.',
    referralLabel: 'How did you hear about Auvenir?',
    firstCheckLabel: 'I agree to the',
    and: 'and',
    privacy: 'privacy policy',
    terms: 'terms of service',
    secondCheckLabel: 'I hereby confirm that I am a Chartered Professional Accountant in good standing with my applicable provincial',
    secondCheckLabelTwo: 'institute(s) and am in compliance with any associated public or professional accounting regulations (as applicable)',
    conference: 'Conference',
    press: 'Press',
    publication: 'Industry Publication',
    referral: 'Referral',
    search: 'Search',
    onAd: 'Online Advertisement',
    printAd: 'Print Advertisement',
    assistant: 'Administration Assistant',
    other: 'Other (Please specify)'
  },
  firmComponent: {
    header: 'Please Provide Your Firm Information',
    nameLabel: 'Firm Name',
    nameError: 'Please enter your name',
    checkOne: 'Firm name has changed in the past 2 years.',
    prevName: 'Firm Previous Name',
    website: 'Firm Website',
    streetAddress: 'Street Address',
    suite: 'Suite/ Office Number',
    postal: 'Postal Code/ Zip Code',
    city: 'City',
    province: 'Province/ State',
    country: 'Country',
    numberEmployees: 'Number of Employees',
    phone: 'Phone Number',
    affiliated: 'I am affiliated with another accounting firm.',
    affiliatedName: 'Affiliated Firm\'s Name',
    logoBtn: 'Update Logo',
    logoWarn: 'Once uploaded, your firm\'s logo will appear on the client\'s view of engagements you create for them. To remove, un-check the box.'
  },
  securityComponent: {
    header: 'Create Your Password',
    subText: 'Create a password consisting of a minimum of 8 characters including a capital and lower case letter, a number and a special character (!@#$%^&*).',
    subTextError: 'Please enter a password and least 8 characters, have a capital and lower case letter, a number and a special character (!@#$%^&*)',
    createPass: 'Create Password',
    confirmPass: 'Confirm Password',
    btn: 'Create Account'
  },
  epilogueComponent: {
    header: 'Your Account Has Been Created!',
    subTextOne: 'Thank you for creating an account. We will be sending a verification email to',
    subTextTwo: 'within 24 hours. Please click the link in the email to get started on your engagements.'
  },
  workingPaperSetupComponent: {
    headerLabel: 'Working Paper Setup',
    nameLabel: 'Name Your Working Paper',
    nameError: '*Please name your Working Paper.',
    typeLabel: 'Select Working Paper Type',
    typeLabelError: 'Accounts Receivable is the only ‘Working Paper Type’ available for selection at launch of the Working Papers Generation',
  },
  workingPaperProcedureComponent: {
    headerLabel: 'Test(s) & Procedures Confirmation (1 of 1 selected test types)',
    testTypeLabel: 'Select Test Type(s) & Confirm Procedures:',
    ARAnalyticalProceduresTitle: 'AR_Analytical_Procedures',
    ARAnalyticalProceduresContent: 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.',
    ARConfirmationTestingTitle: 'AR_Confirmation_Testing',
    ARConfirmationTestingContent: 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.',
    ARAging_AnalysisTitle: 'AR_Aging_Analysis',
    ARAging_AnalysisContent: 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.',
    CreditNotesCutoffTestingTitle: 'Credit_Notes_Cutoff_Testing',
    CreditNotesCutoffTestingContent: 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.',
    CreditBalancesInARTitle: 'Credit_Balances_in_AR',
    CreditBalancesInARContent: 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.',
    CreditRiskAnalysisTitle: 'Credit_Risk_Analysis',
    CreditRiskAnalysisContent: 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo.',
    testTypeConfirmText7: 'By selecting this check box, I confirm the above procedural steps are appropriate for the purposes of [ this test ].',
  },
  workingPaperTechiqueComponent: {
    performanceMaterialityLabel: 'Performance Materiality',
    sampleSizeLabel: 'Sample Size',
    sampleSizeLabelError: 'Please set a sample size',
    samplingTechniqueLabel: 'Select sampling technique',
    samplingTechniqueLabelError: 'Please set a sampling technique',
    RiskOfOverRelianceLabel: 'Risk of Over Reliance',
    RiskOfOverRelianceLabelError: 'Please set a Risk of Over Reliance',
    EPDRLabel: 'EPDR',
    EPDRLabelError: 'Please set a EPDR',
    TRDLabel: 'TRD',
    TRDLabelError: 'Please set a TRD',
  },
  workingPaperEvidenceComponent: {
    clientAssigneeLabel: 'Client Assignee',
    clientAssigneeLabelError: 'Please set a client assignee',
    dueDateLabel: 'Due Date',
    dueDateLabelError: 'Please set a due date',
    dueDatePastError: 'Please set a future due date',
    confirmationLetterOpenAddressLabel: 'Confirmation Letter Opening Address',
    confirmationLetterOpenAddressLabelError: 'Please set a Confirmation Letter Opening Address',
    confirmationLetterClosingAddressLabel: 'Confirmation Letter Closing Address',
    confirmationLetterClosingAddressLabelError: 'Please set a Confirmation Letter Closing Address',
    confirmationCheck: `By checking this box, I allow my client's customer to provide authentication using digital signatures and acknowledge associated risks`,
  },
  workingPaperSamplesComponent: {
    performanceMaterialityLabel: 'Performance Materiality',
    thDateLabel: 'Date',
    thAccountLabel: 'Account',
    thDescriptionLabel: 'Description',
    thCustomerLabel: 'Customer',
    thAmountLabel: 'Amount',
  },
  setupComponent: {
    nameLabel: 'Name Your Engagement',
    nameError: '*Please name your engagement.',
    typeLabel: 'Select Engagement Type',
    typeLabelError: '*Please Select Engagement Type',
    companyLabel: 'Company Name',
    companyError: '*Please name your company.',
    deadlineLabel: 'Set Reporting Deadline',
    deadlineLabelError: 'Please Set Reporting Deadline',
    deadlinePastError: 'Please set a future deadline',
    audit: 'Financial Audit',
    notice: 'Notice to reader / Compilation',
    other: 'Other (Please specify)',
    dateRangeStart: 'Select a Date Range of Bank Statements to be requested from your client.',
    dateRangeStartError: 'Please Select a Date Range of Bank Statements to be requested from your client.',
    dates: 'MM/DD/YYYY'
  },
  teamComponent: {
    addNew: 'Add New Team Member',
    header: 'Create your team',
    singleSub: 'I don\'t need to add any team members to this engagement',
    multiSub: 'I\'d like to add team members to this engagement',
    addBtn: 'Add Members',
    addAnother: 'Add Another Member',
    nameLabel: 'Enter the team member\'s full name',
    nameError: '*Please name your team member',
    emailLabel: 'Enter the email address of the team member you\'d like to invite to Auvenir. Once they accept the invite they will join your team for this engagement',
    emailError: '*Please enter a valid email',
    reEmailLabel: 'Re-enter email address',
    reEmailError: '*Your emails don\'t match',
    roleLabel: 'Enter their role in your firm',
    roleError: '*Please enter your team member\'s job title',
    permissionLabel: 'Select the permission level you\'d like to assign to them',
    inviteBtn: 'Send Invitation',
    sentInfo: 'Invitation sent to',
    at: 'at'
  },
  customizeComponent: {
    header: 'Create your To-Do list',
    rolloverSub: 'Rollover a Saved Template',
    rollover: 'Rollover',
    createSub: 'Create your To-Do list'
  },
  createEngagementModal: {
    headerTitle: 'New Engagement',
    setupTitle: 'SET-UP',
    teamTitle: 'TEAM',
    customizeTitle: 'CUSTOMIZE'
  },
  createWorkingPaperModal: {
    headerTitle: 'Create Your Working Papers',
    setupTitle: 'SET UP',
    procedureTitle: 'PROCEDURES',
    techniqueTitle: 'TECHNIQUE',
    samplesTitle: 'SAMPLES',
    evidenceTitle: 'EVIDENCE',
    successTitle: 'SUCCESS'
  },
  viewPreviewModal: {
    headerTitle: 'New Engagement',
    setupTitle: 'SET-UP',
    teamTitle: 'TEAM',
    customizeTitle: 'CUSTOMIZE'
  },
  categoryModal: {
    addHeaderTitle: 'Add New Category',
    editHeaderTitle: 'Edit Categories',
    nameLabel: 'New Category Name',
    colorLabel: 'Select Color',
    btnCreate: 'Create',
    btnDelete: 'Delete',
    btnArchive: 'Archive',
    nameError: 'Not a valid name.',
    markComplete: 'Mark As Complete?',
    desEditModal: 'Edit or remove any of your categories by clicking on it.',
    deleteToDo: 'Delete To-Do?',
    markCompleteDes: 'The request ITEM is still empty. \n Would you like to mark this To-Do as Complete?', // TODO - fix the term!!
    deleteToDoDes: 'Are you sure you\'d like to delete these To-Dos? Once deleted, you will not be able to retrieve any documents uploaded on the comments in the selected To-Dos.'
  },
  firmAffiliationModal: {
    header: 'Firm Affiliation Definition',
    body: [
      'To be “affiliated” with another firm, means that the audit firm is part of a “network of firms”. A “network firm” means an entity that is, or that a reasonable observer would conclude to be, part of a larger structure of co-operating entities that shares:',
      '(a) common quality control policies and procedures that are designed, implemented and monitored across the larger structure;',
      '(b) common business strategy that involves agreement to achieve common strategic objectives;',
      '(c) the use of a common brand name, including the use of common initials and the use of the common brand name as part of, or along with, a firm name when a partner of the firm signs an audit or review engagement report; or',
      '(d) professional resources, such as',
      '(i) common systems that enable the exchange of information such as client data, billing or time records;',
      '(ii) partners and staff;',
      '(iii) technical departments that consult on technical or industry specific issues, transactions or events for assurance engagements;',
      '(iv) audit methodology or audit manuals; or',
      '(v) training courses and facilities,',
      'where such professional resources are significant.' ]
  },
  teamWidget: {
    memberTitle: 'Team Member',
    roleTitle: 'Role in Firm',
    permissionTitle: 'Permission Level',
    bulk: 'Bulk Actions',
    addBtn: 'Add Member',
    inviteBtn: 'Invite New Member',
    empty: 'You haven\'t added any team members yet.'
  },
  inviteMemberModal: {
    subTitle: 'Invite New Member',
    enterName: 'Enter the team member\'s full name',
    nameError: 'Please enter a first and last name.',
    fullNameError: 'Please enter a full name.',
    emailLabel: 'Enter the email address of the team member you\'d like to invite to Auvenir. Once they accept the invite, they will join your team for this engagement.',
    reEmailLabel: 'Re-enter email address',
    emailError: 'Please enter a valid email.',
    reEmailError: 'The two emails do not match.',
    role: 'Enter their role in your company.',
    roleError: 'Please enter a valid role.',
    permission: 'Select the permission level you\'d like to assign to them'
  },
  fileManager: {
    todo: 'To-Do',
    category: 'Category',
    uploadedBy: 'Uploaded By',
    uploadedDate: 'Uploaded Date',
    fileType: 'File Type',
    filters: 'Filters',
    undoFilter: 'Undo Filter'
  },
  deleteFileModal: {
    title: 'Delete Files?',
    subText: 'Are you sure you would like to delete these selected files?'
  },
  contacts: {
    uploadedBy: 'Uploaded By',
    uploadedDate: 'Uploaded Date',
    fileType: 'File Type',
    filters: 'Filters',
    auditFirm: 'Audit Firm/ Client',
    type: 'Type',
    invitationSent: 'Invitation Sent',
    resendInvite: 'Resend Invite'
  },
  welcomeComponent: {
    header: 'Welcome to Auvenir!',
    getStarted: 'Get Started',
    auditorLabel: 'You have been invited by:',
    engagementLabel: 'To join the following engagement:'
  },
  businessComponent: {
    title: 'Please Confirm your Business Information',
    nameInput: 'Business Name',
    nameChangeCheckbox: 'The legal name changed within the last two years',
    publiclyListedCheckbox: 'The entity is publicly listed or a component of a publicly listed company',
    overseasCheckbox: 'The entity has operations overseas, including parent or subsidiaries',
    parentInput: 'Please list parent company, owners, or shareholders with >20%',
    yearInput: 'Fiscal Year End',
    accountingFramework: 'Accounting Framework',
    industryInput: 'Industry',
    aspe: 'ASPE',
    ifrs: 'IFRS',
    nfpgaap: 'NFP GAAP',
    usgaap: 'US GAAP',
    none: 'None of the above'
  },
  industryList: [{text: 'Agriculture, Forestry, Fishing and Hunting' },
    {text: 'Mining, Quarrying, and Oil and Gas Extraction' },
    {text: 'Utilities' },
    {text: 'Construction' },
    {text: 'Manufacturing' },
    {text: 'Wholesale Trade' },
    {text: 'Retail Trade' },
    {text: 'Transportation and Warehousing' },
    {text: 'Information' },
    {text: 'Finance and Insurance' },
    {text: 'Real Estate' },
    {text: 'Professional, Scientific, and Technical Services' },
    {text: 'Management of Companies and Enterprises' },
    {text: 'Administrative and Support and Waste Management and Remediation Services' },
    {text: 'Educational Services' },
    {text: 'Health Care and Social Assistance' },
    {text: 'Arts, Entertainment, and Recreation' },
    {text: 'Accommodation and Food Services' },
    {text: 'Other Services (except Public Administration)' },
    {text: 'Public Administration' }],
  inviteClientModal: {
    inputTitle: 'Select the client you would like to invite',
    subTitle: 'Invite Your Client',
    select: 'Select',
    newClient: 'Add New Client',
    invite: 'Invite'
  },
  newClientModal: {
    name: 'Enter the client’s full name ',
    nameError: 'Please enter your first and last name.',
    email: 'Enter their email address of the client you’d like to invite to Auvenir. \n Once they accept the invite they will join this engagement. ',
    emailError: 'Not a valid email.',
    verifyEmail: 'Re-enter email address',
    verifyEmailError: 'Emails must match.',
    role: 'Select their role in their company.',
    roleError: 'Not a valid role.',
    cancel: 'Cancel',
    invite: 'Invite'
  },
  filesComponent: {
    title: 'Integrate With Your File Storage',
    subTitle: 'Please select the directory where you keep your engagement documents. You will also be able to upload files from your local storage.',
    feedbackOne: 'Don\'t see you file storage option?',
    feedbackTwo: 'and we will look into adding it!',
    footer: 'Not ready to integrate right now? You can skip and find it in your settings later.'
  },
  letUsKnowModal: {
    title: 'Let Us Know',
    subtext: 'Please type your preferred file storage option and we will keep it in mind for future releases. We appreciate your feedback.'
  },
  fileManagerModal: {
    title: 'Add File from Files Page'
  },
  bankIntegrationModal: {
    title: 'Bank Integration',
    connecting: 'Connecting',
    userName: 'User Name',
    success: 'You have been successfully linked to ',
    fail: 'It appears the credentials were incorrect. Be advised, your account may be frozen after several unsuccessful login attempts.',
    unsuccessful: 'Unsuccessful login attempt.',
    retry: 'Retry',
    authorizing: 'Authorizing',
    exitBankOne: 'By exiting, you will terminate the login process and return to the previous screen.',
    exitBankTwo: 'Are you sure you\'d like to exit?',
    forgot: 'Forgot Password?',
    securityQuestion: 'Security Question:'
  },
  bankComponent: {
    bank: 'Bank',
    account: 'Account Number',
    accSubTitle: 'Once integrated, your bank statements will appear in the Files section of your engagements.'
  },
  engagementPreview: {
    company: 'Company',
    engagementName: 'Engagement Name',
    status: 'Status',
    auditAssignee: 'Audit Assignee',
    completedToDos: 'Completed To-Dos',
    clientAssignee: 'Client Assignee',
    completedDocs: 'Completed Docs',
    lastActivity: 'Last Activity',
    dueDate: 'Due Date',
    filters: 'Filters',
    all: 'All',
    typeOfEngagement: 'Type of Engagement',
    financialAudit: 'Financial Audit',
    review: 'Review',
    noticeToReaderCompilation: 'Notice to Reader / Compilation',
    other: 'Other',
    due: 'Due',
    newEngagement: 'New Engagement',
    engagementTT: 'Click here to create your first audit engagement',
    overdue: 'Overdue',
    toDos: 'To-Dos',
    outstanding: 'Oustanding',
    total: 'Total',
    documents: 'Documents',
    noEngagement: 'You don\'t have any engagements yet. Click the button above to create one.'
  }
}
module.exports = en
