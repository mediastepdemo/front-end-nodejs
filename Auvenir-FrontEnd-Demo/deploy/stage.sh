#!/usr/bin/env bash

# Ascend to the project root
while true; do
    if [[ ! -f Dockerfile ]]; then
      cd ..
  else
      break
  fi
done

# Check the proceeding environment is serving correctly:
status=$(curl -I -s -L http://ariel.auvenir.com | grep "HTTP/1.1")
check=$(echo "${status}" | grep 200)
[[ -z $check ]] && echo "Cannot update Stage! Ariel must be running with the code to prove the QA branch is ok." && exit 1
echo "Ariel is responding with a 200 Status, proceeding to update the stage branch and the Staging server"

user=$(git config user.name)
approved=0

# Quick Authentication

case $user in
"niall-byrne")
  approved=1
  ;;
"mahenderrajaram")
  approved=1
  ;;
"mattfelske")
  approved=1
  ;;
esac

[[ ${approved} -ne 1 ]] && echo "You're not authorized to proceed." && exit 1

# Make sure no unfinished work gets lost

check=$(git diff --exit-code)
[[ $? -ne 0 ]] && echo "You need to commit all changes before proceeding." && exit 1

check=$(git diff --cached --exit-code)
[[ $? -ne 0 ]] && echo "You need to commit all changes before proceeding." && exit 1

# Confirmation

echo "Pushing the latest qa changes to the staging environment."
echo "Make sure your current branch has no outstanding commits."
echo "Type 'yes' to proceed..."

read confirmation

if [[ ${confirmation} != 'yes' ]]; then
    echo "Terminating."
    exit 1
fi

# Perform code promotion

git checkout stage 2>&1 > null
git pull

set -e

git pull origin qa --no-edit
git checkout qa -- npm-shrinkwrap.json
git stage npm-shrinkwrap.json
git push

echo "An automated build has been started for the stage environment."
echo "Staging will update shortly."

