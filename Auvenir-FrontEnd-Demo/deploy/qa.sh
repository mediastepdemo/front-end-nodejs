#!/usr/bin/env bash

# Ascend to the project root
while true; do
    if [[ ! -f Dockerfile ]]; then
      cd ..
  else
      break
  fi
done

# Check the proceeding environment is serving correctly:
status=$(curl -I -s -L http://cadet.auvenir.com | grep "HTTP/1.1")
check=$(echo "${status}" | grep 200)
[[ -z $check ]] && echo "Cannot promote code cadet is not currently responding." && exit 1
echo "Cadet is responding with a 200 Status, proceeding with code promotion"

user=$(git config user.name)
approved=0

# Quick Authentication

case $user in
"niall-byrne")
  approved=1
  ;;
"mahenderrajaram")
  approved=1
  ;;
"mattfelske")
  approved=1
  ;;
esac

[[ ${approved} -ne 1 ]] && echo "You're not authorized to proceed." && exit 1

# Make sure no unfinished work gets lost

check=$(git diff --exit-code)
[[ $? -ne 0 ]] && echo "You need to commit all changes before proceeding." && exit 1

check=$(git diff --cached --exit-code)
[[ $? -ne 0 ]] && echo "You need to commit all changes before proceeding." && exit 1

# Confirmation

echo "Pushing the latest dev changes to the qa environment."
echo "Make sure your current branch has no outstanding commits."
echo "Type 'yes' to proceed..."

read confirmation

if [[ ${confirmation} != 'yes' ]]; then
    echo "Terminating."
    exit 1
fi

# Perform code promotion

git checkout qa 2>&1 > /dev/null
git pull

set -e

git pull origin dev --no-edit
git checkout dev -- npm-shrinkwrap.json
git stage npm-shrinkwrap.json
git push

echo "An automated build has been started for the qa environment."
