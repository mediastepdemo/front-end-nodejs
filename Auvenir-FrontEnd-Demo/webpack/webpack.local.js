// Webpack Local Config
var path = require('path')
var webpack = require('webpack')
var HappyPack = require('happypack')
var appConfig = require('../config')

var webpackConfig = {
  entry: {
    app: [
      `webpack-hot-middleware/client?path=${appConfig.server.protocol}://${appConfig.server.domain}:${appConfig.server.port}/__webpack_hmr&reload=true&timeout=20000`,
      'babel-polyfill',
      './public/js/src/start.js'
    ]
  },
  output: {
    path: path.join(__dirname, '../public/dist'),
    filename: '[name].js',
    publicPath: '/dist/'
  },
  devtool: 'source-map',
  module: {
    loaders: [
      {
        test: /\.js?$/,
        // Run JS Files through babel-loader
        loader: 'happypack/loader',
        // Skip any files outside of the following directories
        include: [
          path.resolve(__dirname, '../public/js/src'),
          path.resolve(__dirname, '../public/core'),
          path.resolve(__dirname, '../plugins/utilities')
        ]
      },
      {
        test: /\.css$/,
        // Run CSS through the style loader, or backup to css loader
        loader: 'style-loader!css-loader'
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)$/,
        // Run images and fonts through url-loader
        loader: 'url-loader?limit=100000'
      },
      {
        // Run json through the json-loader
        test: /\.json?$/,
        loader: 'json-loader'
      }
    ]
  },
  performance: {hints: false},
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HappyPack({
      cache: true,
      threads: 2,
      loaders: [
        {
          path: 'babel-loader',
          query: {
            plugins: [
              'transform-runtime',
              'transform-object-rest-spread'
            ],
            presets: ['es2015']
          }
        }
      ]
    })
  ]
}

module.exports = webpackConfig
