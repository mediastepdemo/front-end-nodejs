// Webpack Deloitte Config
var path = require('path')
var webpack = require('webpack')

var webpackConfig = {
  entry: {
    app: [
      'babel-polyfill',
      './public/js/src/start.js'
    ]
  },
  output: {
    path: path.join(__dirname, '../public/dist'),
    filename: '[name].js',
    publicPath: '/dist/'
  },
  module: {
    loaders: [
      {
        test: /\.js?$/,
        // Run JS Files through babel-loader
        loader: 'babel-loader',
        // Skip any files outside of the following directories
        include: [
          path.resolve(__dirname, '../public/js/src'),
          path.resolve(__dirname, '../public/core')
        ],
        // Options to configure babel with
        query: {
          plugins: ['transform-runtime', 'transform-object-rest-spread'],
          presets: ['es2015']
        }
      },
      {
        test: /\.css$/,
        // Run CSS through the style loader, or backup to css loader
        loader: 'style-loader!css-loader'
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)$/,
        // Run images and fonts through url-loader
        loader: 'url-loader?limit=100000'
      },
      {
        // Run json through the json-loader
        test: /\.json?$/,
        loader: 'json-loader'
      }
    ]
  },
  performance: {hints: false},
  plugins: [
    new webpack.optimize.UglifyJsPlugin()
  ]
}

module.exports = webpackConfig
