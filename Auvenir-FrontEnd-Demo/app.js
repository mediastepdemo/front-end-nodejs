#!/usr/bin/env node
var path = require('path')

global.rootDirectory = __dirname
global.fileServeRoot = path.join(rootDirectory, 'public')

const appConfig = require(rootDirectory + '/config')

// Load Azure Application Insights
if (appConfig.env !== 'local') {
  var appInsights = require('applicationinsights')
  appInsights.setup(appConfig.integrations.azure.appinsights)
  appInsights.start()
}

const Logger = require(rootDirectory + '/plugins/logger/logger')
const { info, error } = Logger.init(__filename)
var fs = require('fs')
var http = require('http')
var https = require('https')
var mongoose = require('mongoose')
mongoose.Promise = global.Promise
var express = require('express')
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')
var helmet = require('helmet')
var passport = require('passport')
var expressSession = require('express-session')
var uuid = require('node-uuid')
var flash = require('connect-flash')
var initPassport = require('./passport/init')
var redis = require('redis')
var redisClient = createRedisClient('client')
var redisSubscriber = createRedisClient('subscriber')
var redisPublisher = createRedisClient('publisher')
var RedisStore = require('connect-redis')(expressSession)
var sockets = require('./sockets')
const Models = require('./models')
const Finicity = require('./finicity')

info('                   Starting up                     ')
info('    _    _   _ __     __ _____  _   _  ___  ____   ')
info('   / \\  | | | |\\ \\   / /| ____|| \\ | ||_ _||  _ \\  ')
info('  / _ \\ | | | | \\ \\ / / |  _|  |  \\| | | | | |_) | ')
info(' / ___ \\| |_| |  \\ V /  | |___ | |\\  | | | |  _ <  ')
info('/_/   \\_\\\\___/    \\_/   |_____||_| \\_||___||_| \\_\\ ')
info(`                                                   `)

var app = express()
// Setup Webpack HMR
if (appConfig.env === 'local') {
  // Create & configure a webpack compiler
  var webpack = require('webpack')
  var webpackDevMiddleware = require('webpack-dev-middleware')
  var webpackHotMiddleware = require('webpack-hot-middleware')
  var webpackConfig = require('./webpack')
  var compiler = webpack(webpackConfig)
  // Attach the dev middleware to the compiler & the server
  app.use(webpackDevMiddleware(compiler, {
    noInfo: true,
    publicPath: webpackConfig.output.publicPath
  }))
  // Attach the hot middleware to the compiler & the server
  app.use(webpackHotMiddleware(compiler, {
    path: '/__webpack_hmr',
    stats: { chunks: false, modules: false, warnings: false },
    reload: true,
    heartbeat: 10 * 1000
  }))
  info('Webpack HMR now listening for changes')
}

// Setup Express Middleware
app.set('views', path.join(__dirname, 'public/views'))
app.set('view engine', 'jade')
app.use(helmet())
app.use(bodyParser.urlencoded({ extended: false, limit: '4mb' }))
app.use(bodyParser.json())
app.use(cookieParser())
app.use(express.static(global.fileServeRoot))
app.set('trust proxy', 1)
let protocolIsSecure = appConfig.server.protocol.toLowerCase() === 'https'
app.use(expressSession({
  secret: 'mySecretKey',
  resave: false,
  saveUninitialized: true,
  genid: function () {
    // use UUIDs for session IDs
    return uuid.v4()
  },
  cookie: {
    httpOnly: true,
    secure: protocolIsSecure,
    sameSite: 'lax',
    // 30 minutes for non-local env
    maxAge: appConfig.env === 'local' ? null : 30 * 60 * 1000
  },
  rolling: true,
  secure: protocolIsSecure,
  store: new RedisStore({ client: redisClient })
}))
app.use(passport.initialize())
app.use(passport.session())
initPassport(passport)
app.use(flash())
app.use(Logger.middleware)

// Core Routes
var securityMiddleware = require('./routes/securityHelper')
var initRoutes = require('./routes/index')
var initAPIRoutes = require('./routes/api')
var initQuickBooks = require('./routes/quickbooks')
var initGoogleDrive = require('./routes/googleDrive')
var initMarketingPageAPI = require('./routes/mpApi')

app.use(securityMiddleware.enableSTS())
app.use('/', securityMiddleware.setAllowControl({ allowCredentials: true }), initRoutes(passport, appConfig))
app.use('/api', securityMiddleware.setAllowControl({ allowCredentials: true }), initAPIRoutes(passport, appConfig))
app.use('/quickbooks', initQuickBooks(passport, appConfig))
app.use('/googleDrive', initGoogleDrive(passport, appConfig))
app.use('/404', (req, res, next) => {
  res.render('error')
})
app.use((req, res, next) => {
  res.redirect('/404')
})

var httpServer = null
var httpsServer = null

if (appConfig.server.protocol === 'https') {
  // Setup HTTP Server
  httpServer = http.createServer(function (req, res) {
    info('Redirecting to https ...')
    res.writeHead(301, { 'Location': 'https://' + req.headers['host'] + req.url })
    res.end()
  })
  httpServer.listen(appConfig.server.redirect || 1337, function () {
    info(`HTTP server listening and redirecting from port ${httpServer.address().port}`)
  })

  // Setup HTTPS Server TODO: Move to NGNIX
  httpsServer = https.createServer({
    key: getSecurityCertsFiles('rKey'),
    cert: getSecurityCertsFiles('rCert'),
    ca: getSecurityCertsFiles('rCA'),
    passphrase: getSecurityCertsFiles('passphrase'),
    ciphers: [
      'ECDHE-RSA-AES256-SHA384',
      'DHE-RSA-AES256-SHA384',
      'ECDHE-RSA-AES256-SHA256',
      'DHE-RSA-AES256-SHA256',
      'ECDHE-RSA-AES128-SHA256',
      'DHE-RSA-AES128-SHA256',
      'HIGH',
      '!aNULL',
      '!eNULL',
      '!EXPORT',
      '!DES',
      '!RC4',
      '!MD5',
      '!PSK',
      '!SRP',
      '!CAMELLIA'
    ].join(':'),
    honorCipherOrder: true
  }, app)
} else {
  httpsServer = http.createServer(app)
}

// SETUP FINICITY
const finicity = new Finicity(appConfig, app, Models.connections['finicity'])
// finicity.updateInstitutions()

httpsServer.listen(process.env.PORT || appConfig.server.port || 1337, function () {
  info(`${appConfig.server.protocol.toUpperCase()} server listening on port ${httpsServer.address().port}`)
})
// Setup Websockets
sockets.init(httpsServer, redisClient, redisSubscriber, redisPublisher, appConfig, finicity)

// --- Helper Functions ---
function createRedisClient (name) {
  var client

  if (appConfig.env === 'local') {
    client = redis.createClient({ host: appConfig.redis.domain, port: appConfig.redis.port })
  } else {
    client = redis.createClient(appConfig.redis.port, appConfig.redis.domain, {auth_pass: appConfig.redis.auth_pass, tls: {servername: appConfig.redis.servername}})
  }
  client.on('error', function (err) {
    error(`Failed to connect to Redis ${name}`, { err })
  })
  client.on('connect', function () {
    info(`Successfully connected to Redis ${name}`)
  })
  return client
}

function getSecurityCertsFiles (key) {
  var certs = appConfig.security.ssl_certs
  return key === 'passphrase' ? certs[key] : fs.readFileSync(certs[key])
}
