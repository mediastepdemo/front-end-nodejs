/* eslint no-console: 0 */

var fs = require('fs')
var Terms_EN = fs.readFileSync('./Terms_EN.html', 'utf8')
var Terms_FR = fs.readFileSync('./Terms_FR.html', 'utf8')
var Privacy_EN = fs.readFileSync('./Privacy_EN.html', 'utf8')
var Privacy_FR = fs.readFileSync('./Privacy_FR.html', 'utf8')
var Cookies_EN = fs.readFileSync('./Cookies_EN.html', 'utf8')
var Cookies_FR = fs.readFileSync('./Cookies_FR.html', 'utf8')

// Convert HTML to valid JSON
var terms = JSON.stringify({
  title: 'Terms of Service',
  title_FR: '',
  EN: Terms_EN.replace(/\\n/g, '\\n'),
  FR: Terms_FR.replace(/\\n/g, '\\n')
})

var privacy = JSON.stringify({
  title: 'Privacy Statement',
  EN: Privacy_EN.replace(/\\n/g, '\\n'),
  FR: Privacy_FR.replace(/\\n/g, '\\n')
})
var cookies = JSON.stringify({
  title: 'Cookie Use',
  EN: Cookies_EN.replace(/\\n/g, '\\n'),
  FR: Cookies_FR.replace(/\\n/g, '\\n')
})

// Create Terms of Use
fs.writeFile('../../public/data/terms.json', terms, function (err) {
  if (err) return console.log(err)
  console.log('Created terms.json')
})

// Create Privacy Policy
fs.writeFile('../../public/data/privacy.json', privacy, function (err) {
  if (err) return console.log(err)
  console.log('Created privacy.json')
})

// Create Privacy Policy
fs.writeFile('../../public/data/cookies.json', cookies, function (err) {
  if (err) return console.log(err)
  console.log('Created cookies.json')
})
