# Agreement Creator

The create-agreement script can be used to generate a valid json object from the cookies, privacy, and terms html files.

## How to use

Simply run *node create-agreements.js* and it will generate/update the terms.json, privacy.json, and cookies.json files in the /public/data folder with the latest agreements.

Note: To change the agreement effective date, please change the value set inside create-agreements.js
