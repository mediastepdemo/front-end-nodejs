// Create a developer account for testing purposes
// To use run the following command:
// node create-developer.js [name] [email] [phone]
/* eslint no-console: 0 */

console.logCopy = console.log.bind(console)
console.errorCopy = console.error.bind(console)
console.warnCopy = console.warn.bind(console)

console.log = function (data) {
  console.logCopy('\x1b[36m\x1b[0m' + data)
}

console.warn = function (data) {
  console.warnCopy('\x1b[36m\x1b[33m' + data + '\x1b[0m')
}

console.error = function (data) {
  console.errorCopy('\x1b[36m\x1b[31m' + data + '\x1b[0m')
}

var Logger = require(rootDirectory + '/plugins/logger/logger')
global.Logger = Logger

var Models = require('../models')
var Utility = require('../plugins/utilities/utility')

console.log('Retrieving parameters set ...')
var args = process.argv
var appConfig = require('../config')

if (args.length < 3 || args.length > 3) {
  console.log('ERROR: Incorrect number of parameters passed in.')
  console.log('Please use the following.')
  console.log('\tnode ./create-developer.js <email>')
  process.exit()
} else {
  var email = args[2]

  console.log('Read in ' + email)
  if (!Utility.isEmailValid(email)) {
    console.error('Invalid email address.')
    process.exit()
  }
  console.log('Retrieving data models ...')

  Models.Auvenir.User.findOne({ email: email }, function (err, oneUser) {
    if (err) {
      console.error('Error detected while trying to find a user.')
      console.error(err.stack)
      process.exit()
    }

    if (oneUser) {
      Utility.generateDeveloperCreds(function (err, result) {
        if (err) {
          console.error('Unable to generate a unique developer API key')
          process.exit()
        }
        var apiKey
        oneUser.developer = { apiKey: apiKey }
        oneUser.save(function (err) {
          if (err) {
            console.error(err)
            process.exit()
          } else {
            console.log('Active user has been updated with dev credentials')
            console.log(oneUser)
            process.exit()
          }
        })
      })
    } else {
      Utility.scrapeUserData({email: email}, function (err, scrapings) {
        if (err) {
          console.error('Error occured while scraping user data')
          process.exit()
        }

        var userData = scrapings.user
        Utility.generateUniqueAuthID(function (err, result) {
          if (err) {
            console.error('Unable to geenrate a unique user authentication ID')
            process.exit()
          }

          userData.auth = { id: result.authID }

          Utility.generateDeveloperCreds(function (err, result) {
            if (err) {
              console.error('Unable to generate a unique developer API key')
              process.exit()
            }

            var apiKey = result.apiKey
            userData.auth.developer = { apiKey: apiKey }

            var newUser = new Models.Auvenir.User(userData)
            newUser.type = 'ADMIN'
            newUser.status = 'ACTIVE'
            newUser.save(function (err) {
              if (err) {
                console.error(err)
                process.exit()
              } else {
                console.log('New user has been added with dev credentials')
                console.log(newUser)
                process.exit()
              }
            })
          })
        })
      })
    }
  })
}
