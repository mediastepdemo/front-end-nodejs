/* eslint no-console: 0 */

console.log('Running Mapping Loader ...')
console.log('Retrieving parameters set ...')

var appConfig = require('../config')

console.log('Environment Parameter set to: ' + appConfig.env)
if (appConfig.env !== 'local' && appConfig.env !== 'dev' && appConfig.env !== 'test' && appConfig.env !== 'prod') {
  console.log('ERROR: Incorrect environment parameters value used.')
  console.log('Please user one of the following values.')
  console.log('\tLOCAL: Used when you are trying to run a node server on your own machine.  Will run off of localhost.')
  console.log('\tDEV: For the development server deployment.')
  console.log('\tTEST: For the test server deployment.')
  console.log('\tPROD: For the production server deployment.')
  process.exit()
} else {
  console.log('Starting up required modules...')
  var MONGO_SERVER_URL = appConfig.mongo.URI.auvenir
  var mongooseOPTIONS = appConfig.mongo.options
  var mongoose = require('mongoose')
  console.log('Setting up Mongoose database connection for ' + MONGO_SERVER_URL + ' ...')
  mongoose.connect(MONGO_SERVER_URL, mongooseOPTIONS)

  var Mapping = require('../models/mapping')

  var mappings = [
    { key: 'account-ss1', fieldKey: 'fName', model: 'User', field: 'firstName' },
    { key: 'account-ss1', fieldKey: 'lName', model: 'User', field: 'lastName' },
    { key: 'account-ss1', fieldKey: 'phone', model: 'User', field: 'phone' },
    { key: 'account-ss1', fieldKey: 'title', model: 'User', field: 'firstName' },
    { key: 'account-ss1', fieldKey: 'photo', model: 'User', field: 'firstName' },
    { key: 'account-ss1', fieldKey: 'twoFactor', model: 'User', field: 'auth.2fa' },
    { key: 'account-ss1', fieldKey: 'keyContact', model: 'User', field: 'keyContact' }
  ]

  processNext(mappings, 0)
  var processNext = function (m, pos) {
    Mapping.findOneAndUpdate(m[pos], m[pos], {upsert: true, new: true}, function (err, oneMapping) {
      if (err) {
        console.log(err)
      } else {
        if (oneMapping) {
          console.log(oneMapping)
        } else {
          console.log('Not Found')
        }
      }
      pos++
      if (pos < m.length) {
        processNext(m, pos)
      } else {
        console.log('Done adding all mappings.')
        process.exit()
      }
    })
  }
}
