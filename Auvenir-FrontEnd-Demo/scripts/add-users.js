/* eslint no-console: 0 */

var emails = [ 'fvaldes@investnor.com',
  'rkliman@seic.com',
  'ssoyland@deloitte.com',
  'cselph@deloitte.com',
  'nonoka.yamazaki@isidentsu.com',
  'esyed@deloitte.com',
  'liz.claire.morris@gmail.com',
  't6-tanaka@nri.co.jp',
  'hkt@hokutokuramoto.jp',
  'ttsubata@nulss.com',
  'gfolkers@refine-it.nl',
  'Ftsang@alfa.nl',
  'Martin.van.vliet@exact.com',
  'Martinvanvliet@xmsnet.nl',
  'eric@simplekyc.com',
  'sjnanji@gmail.com',
  'mark.kubisheski@genworth.com',
  'Cameronamills@gmail.com',
  'crjohnston42@gmail.com',
  'Chamomileadultservices@gmail.com',
  'joris.joppe@analys.io',
  'cameronamills@gmail.com',
  'cameron@growwithbamboo.com',
  'martin.van.vliet@exact.com',
  'TANYA@5TILL.COM',
  'mudassarshaikh469@gmail.com',
  'Soeren.ploschke@finlab.de',
  'alexander.leppink@bdo.nl',
  'ircnr1@informationevolution.com',
  'krishnawillb@gmail.com',
  'ircbe1@informationevolution.com',
  'sambath@inforamtionevolution.com',
  'dion.lisle@firstdata.com',
  'puneet.talwar@kalyptorisk.com',
  'ryota.hayashi@finatext.com',
  'perrytelle@hoornwijckgroep.nl',
  'dsalib@hotmail.com',
  'kanikathakar@hotmail.com',
  'hbootsma@deloitte.nl',
  'mearke@ziggo.nl',
  'alonso@banregiolabs.com',
  'alonso.trevino@banregiolabs.com',
  'pbarrar@ferstcapital.com',
  'studioit708@gmail.com',
  'nmusakhanyan@gmail.com',
  'george.jose.a@gmail.com',
  'christophe@ehda.co',
  'c.anthis@ccseducation.com',
  'jan@ehda.co',
  'james.pipe@huddle.com',
  'pplastovets@deloitte.ua',
  'maciej.malesa@accenture.com' ]

console.log('')
console.log('Starting up required modules...')
var appConfig = require('../config')

console.log('Retrieving data models ...')
var Utility = require('../plugins/utilities/utility')

var processed = 0
var added = 0
var errors = 0

addUser(emails[processed])

function addUser (email) {
  var json = { email: email }
  var suffix = '@auvenir.com'
  var index = email.indexOf(suffix)
  if ((index !== -1) && ((email.length - index) === suffix.length)) {
    console.log('Skipping. Auvenir employee detected.')
    checkNext()
  } else {
    json.acl = 'CLIENT'
  }

  console.log('Add user account ' + email)

  Utility.createUserAccount(json, function (err, result) {
    if (err) {
      console.log(err)
      errors++
      checkNext()
    } else {
      console.log('User account created for ' + email)
      added++
      checkNext()
    }
  })
}

function checkNext () {
  processed++
  if (processed < emails.length) {
    addUser(emails[processed])
  } else {
    console.log('Done processing ' + emails.length + ' emails')
    console.log(added + ' user accounts added successfully')
    console.log(errors + ' occured')
    process.exit()
  }
}
