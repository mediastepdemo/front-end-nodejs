/* eslint no-console: 0 */

var appConfig = require('../config')
var Models = require('../models')

// Create the Default Template
var DefaultTemplate = new Models.Auvenir.Template({
  ACL: [],
  name: 'default',
  categories: [ 'Financials' ],
  requests: [
    { category: 'Financials', name: 'General Ledger', description: 'Description', requests: [] },
    { category: 'Financials', name: 'Trial Balance', description: 'Description Placeholder', requests: [] },
    { category: 'Financials', name: 'Bank Statements', description: 'Description Placeholder', requests: [] }
  ]
})

var DummyAdmin = new Models.Auvenir.User({
  status: 'ONBOARDING',
  email: 'QA@auvenir.com',
  type: 'ADMIN',
  auth: {
    id: appConfig.integrations.qa.authID,
    developer: {
      apiKey: appConfig.integrations.qa.apiKey
    }
  },
  verified: false,
  lastLogin: null,
  dateCreated: new Date(),
  profilePicture: '',
  jobTitle: 'Auvenir Admin',
  lastName: 'Admin',
  firstName: 'Dummy',
  __v: 0
})

// Remove Existing Templates
Models.Auvenir.Template.remove({ name: DefaultTemplate.name })
  .then(function () {
    // Save new default templates
    return DefaultTemplate.save()
  })
  .then(function () {
    // Remove old dummy admin
    return Models.Auvenir.User.remove({ email: DummyAdmin.email })
  })
  .then(function () {
    // Save the dummy admin account (temporary)
    return DummyAdmin.save()
  })
  .then(function () {
    console.log('Finished populating mongodb.')
    process.exit()
  })
  .catch(function (err) {
    console.error('Failed to populate mongodb with necessary data:', err)
    return
  })
