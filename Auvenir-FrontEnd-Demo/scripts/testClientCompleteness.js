/* eslint no-console: 0 */

console.log('Retrieving parameters ...')
var args = process.argv

if (args.length > 2) {
  console.log('ERROR: Incorrect number of parameters passed in.')
  console.log('Please use the following.')
  console.log('\tnode ./testClientCompleteness.js')
  process.exit()
} else {
  console.log('Starting up required modules...')
  var ClientCompleteness = require('../plugins/clientCompleteness/ClientCompleteness')
  var ourAuditPeriod = {
    'periodStart': '2014-06-01',
    'periodEnd': '2015-01-31',
    'quarter1End': '2015-08-31',
    'quarter2End': '2015-11-30',
    'quarter3End': '2015-02-27',
    'quarter4End': '2015-05-31'
  }

  var ourChartofAccounts = [
    {
      'accountNumber': '101',
      'accountName': 'Cash',
      'category': 'cash'
    },
    {
      'accountNumber': '201',
      'accountName': 'Accounts Receivable',
      'category': 'accountsReceivable'
    },
    {
      'accountNumber': '301',
      'accountName': 'Accounts Payable',
      'category': 'accountsPayable'
    }
  ]

  var ourTrialBalance = {
    previousPeriod: {
      effectiveDate: '2014-05-31',
      accounts: [
        {
          number: 'number1',
          closingBalance: 100.00
        }
      ]
    },
    currentPeriod: {
      effectiveDate: '2014-06-01',
      accounts: [
        {
          number: 'number1',
          openingBalance: 10.00
        }
      ]
    }
  }

  var ourBankAccountList = [
    {
      'bankName': 'RBC',
      'accountName': 'Chequing 1',
      'accountNumber': '123-345567',
      'chartAccountNumber': '101'
    },
    {
      'bankName': 'RBC',
      'accountName': 'Chequing 2',
      'accountNumber': '123-3888567',
      'chartAccountNumber': '101'
    }
  ]

  var bankStmtMay = {
    'id': 'idOfStmtInDatabase',
    'bankName': 'RBC',
    'accountName': 'Chequing 1',
    'accountNumber': '123-345567',
    'openingDate': '2014-05-01',
    'closingDate': '2014-05-31',
    'openingBalance': 530.24,
    'closingBalance': 340.67
  }
  var bankStmtJun = {
    'id': 'idOfStmtInDatabase',
    'bankName': 'RBC',
    'accountName': 'Chequing 1',
    'accountNumber': '123-345567',
    'openingDate': '2014-06-01',
    'closingDate': '2014-06-30',
    'openingBalance': 340.68,
    'closingBalance': 1530.24
  }

  var journalEntries = [
    {
      '_id': 'abcdef',
      'headerNumber': 'test-01',
      'description': 'test-01 header description',
      'effectiveDate': '2014-06-04',
      'postedDate': '2014-06-04'
    },
    {
      '_id': 'abcdef1',
      'headerNumber': 'test-02',
      'description': '     ',
      'effectiveDate': '2014-06-04',
      'postedDate': '2014-06-04'
    },
    {
      '_id': 'abcde2',
      'headerNumber': 'test-03',
      'description': 'test-03 header description',
      'effectiveDate': '2014-06-28',
      'postedDate': '2014-06-28'
    }
  ]
  var lines = [{'headerID': 'abcdef',
    'postType': 'Debit',
    'lineNumber': '356',
    'details': ''}]

  var TestClient = new ClientCompleteness('debug')
  var result

  // Set the audit period.
  console.log('')
  console.log('Test ClientCompleteness.setAuditPeriod ...')
  console.log('The audit period to set is: ' + JSON.stringify(ourAuditPeriod))
  result = TestClient.setAuditPeriod(ourAuditPeriod)
  console.log('Response: ' + JSON.stringify(result))
  if (result.code === 0) {
    console.log('ClientCompleteness.setAuditPeriod worked!')
  } else {
    console.log('ClientCompleteness.setAuditPeriod failed!')
  }

  // Set the Chart of Accounts.
  console.log('')
  console.log('Test ClientCompleteness.setChartOfAccounts ...')
  console.log('The Chart of Accounts to set is: ' + JSON.stringify(ourChartofAccounts))
  result = TestClient.setChartOfAccounts(ourChartofAccounts)
  console.log('Response: ' + JSON.stringify(result))
  if (result.code === 0) {
    console.log('ClientCompleteness.setChartOfAccounts worked!')
  } else {
    console.log('ClientCompleteness.setChartOfAccounts failed!')
  }

  // Set the trial balance.
  console.log('')
  console.log('Test ClientCompleteness.setTrialBalance ...')
  console.log('The Trial Balance to set is: ' + JSON.stringify(ourTrialBalance))
  result = TestClient.setTrialBalance(ourTrialBalance)
  console.log('Response: ' + JSON.stringify(result))
  if (result.code === 0) {
    console.log('ClientCompleteness.setTrialBalance worked!')
  } else {
    console.log('ClientCompleteness.setTrialBalance failed!')
  }

  // Set the Bank Account List.
  console.log('')
  console.log('Test ClientCompleteness.setBankAccountList ...')
  console.log('The bank account list to set is: ' + JSON.stringify(ourBankAccountList))
  result = TestClient.setBankAccountList(ourBankAccountList)
  console.log('Response: ' + JSON.stringify(result))
  if (result.code === 0) {
    console.log('ClientCompleteness.setBankAccountList worked!')
  } else {
    console.log('ClientCompleteness.setBankAccountList failed!')
  }

  // Set the bank statements.
  console.log('')
  console.log('Test ClientCompleteness.setBankStatement ...')
  console.log('The bank statement to set is: ' + JSON.stringify(bankStmtJun))
  result = TestClient.setBankStatement(bankStmtJun)
  console.log('Response: ' + JSON.stringify(result))
  if (result.code === 0) {
    console.log('ClientCompleteness.setBankStatement worked!')
  } else {
    console.log('ClientCompleteness.setBankStatement failed!')
  }

  console.log('')
  console.log('Test ClientCompleteness.setBankStatement ...')
  console.log('The bank statement to set is: ' + JSON.stringify(bankStmtMay))
  result = TestClient.setBankStatement(bankStmtMay)
  console.log('Response: ' + JSON.stringify(result))
  if (result.code === 0) {
    console.log('ClientCompleteness.setBankStatement worked!')
  } else {
    console.log('ClientCompleteness.setBankStatement failed!')
  }

  // Set the journal entries.
  console.log('')
  console.log('Test ClientCompleteness.setGlJournalEntries ...')
  result = TestClient.setGlJournalEntries(journalEntries)
  console.log('Response: ' + JSON.stringify(result))
  if (result.code === 0) {
    console.log('ClientCompleteness.setGlJournalEntries worked!')
  } else {
    console.log('ClientCompleteness.setGlJournalEntries failed!')
  }

  // Set the Lines.
  console.log('')
  console.log('Test ClientCompleteness.setGlLines ...')
  result = TestClient.setGlLines(lines)
  console.log('Response: ' + JSON.stringify(result))
  if (result.code === 0) {
    console.log('ClientCompleteness.setGlLines worked!')
  } else {
    console.log('ClientCompleteness.setGlLines failed!')
  }

  // Run all the tests.
  console.log('')
  console.log('Test ClientCompleteness.runclientCompleteness ...')
  result = TestClient.runclientCompleteness()
  console.log('Response: ' + JSON.stringify(result))
  if (result.code === 0) {
    console.log('ClientCompleteness.runclientCompleteness worked!')
  } else {
    console.log('ClientCompleteness.runclientCompleteness failed!')
  }

  // Run the individual test.

  // Check if we all the bank statements are valid.
  console.log('')
  console.log('Test ClientCompleteness.areBankStatmentsValid ...')
  result = TestClient.areBankStatmentsValid()
  console.log('Response: ' + JSON.stringify(result))
  if (result.code === 0) {
    console.log('ClientCompleteness.areBankStatmentsValid worked!')
  } else {
    console.log('ClientCompleteness.areBankStatmentsValid failed!')
  }

  // Check if we have all the bank statements.
  console.log('')
  console.log('Test ClientCompleteness.haveAllBankStatements ...')
  result = TestClient.haveAllBankStatements()
  console.log('Response: ' + JSON.stringify(result))
  if (result.code === 0) {
    console.log('ClientCompleteness.haveAllBankStatements worked!')
  } else {
    console.log('ClientCompleteness.haveAllBankStatements failed!')
  }

  // Check if we have all the bank accounts.
  console.log('')
  console.log('Test ClientCompleteness.haveAllBankAccounts ...')
  result = TestClient.haveAllBankAccounts()
  console.log('Response: ' + JSON.stringify(result))
  if (result.code === 0) {
    console.log('ClientCompleteness.haveAllBankAccounts worked!')
  } else {
    console.log('ClientCompleteness.haveAllBankAccounts failed!')
  }

  // Check bank account in chart of accounts.
  console.log('')
  console.log('Test ClientCompleteness.isBankAccountInChartOfAccounts ...')
  result = TestClient.isBankAccountInChartOfAccounts()
  console.log('Response: ' + JSON.stringify(result))
  if (result.code === 0) {
    console.log('ClientCompleteness.isBankAccountInChartOfAccounts worked!')
  } else {
    console.log('ClientCompleteness.isBankAccountInChartOfAccounts failed!')
  }

  // Check the trial balance.
  console.log('')
  console.log('Test ClientCompleteness.isGlTrialBalance ...')
  result = TestClient.isGlTrialBalance()
  console.log('Response: ' + JSON.stringify(result))
  if (result.code === 0) {
    console.log('ClientCompleteness.isGlTrialBalance worked!')
  } else {
    console.log('ClientCompleteness.isGlTrialBalance failed!')
  }

  // Check the journal entries for invalid dates.
  console.log('')
  console.log('Test ClientCompleteness.allJournalDatesValid ...')
  result = TestClient.allJournalDatesValid()
  console.log('Response: ' + JSON.stringify(result))
  if (result.code === 0) {
    console.log('ClientCompleteness.allJournalDatesValid worked!')
  } else {
    console.log('ClientCompleteness.allJournalDatesValid failed!')
  }

  // Check the journal entries for blank descriptions.
  console.log('')
  console.log('Test ClientCompleteness.allJournalDescNotBlank ...')
  result = TestClient.allJournalDescNotBlank()
  console.log('Response: ' + JSON.stringify(result))
  if (result.code === 0) {
    console.log('ClientCompleteness.allJournalDescNotBlank worked!')
  } else {
    console.log('ClientCompleteness.allJournalDescNotBlank failed!')
  }

  // Check the journal entries for blank descriptions.
  console.log('')
  console.log('Test ClientCompleteness.allJournalLinesValid ...')
  result = TestClient.allJournalLinesValid()
  console.log('Response: ' + JSON.stringify(result))
  if (result.code === 0) {
    console.log('ClientCompleteness.allJournalLinesValid worked!')
  } else {
    console.log('ClientCompleteness.allJournalLinesValid failed!')
  }

  // All finished testing.
  console.log('')
  console.log('Finished testing ClientCompleteness..')
  process.exit()
}
