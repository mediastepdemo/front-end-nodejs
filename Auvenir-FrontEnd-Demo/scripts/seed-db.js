/**
 * seed-db.js - Seeds the database with dummy users, businesses and engagements
 * Deliberately stacking events in the event loop to ensure that data that depends on other data
 * is created first. e.g. a business needs a user, an engagement needs two users, etc
 */
/* eslint no-console: 0 */

var Utility = require('../plugins/utilities/utility')
var Models = require('../models')
var async = require('async')
var mongoose = require('mongoose')
var faker = require('faker')
var _ = require('lodash')
var debug = require('debug')('seed-db')

var level = process.env.SEED_DB_LEVEL || 'all'

debug('seed level: %s', level)

function getRandomInt (min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min
}

function mongoSetup (cbk) {
  async.waterfall([
    function (cbk) {
      mongoose.connect(require('../config').current.mongoURI, cbk)
    },
    function (cbk) {
      mongoose.connection.db.dropDatabase(cbk)
    }
  ], cbk)
}

function generateUsers (config) {
  function fakeEmail () {
    return 'auvenir-dummy-' + faker.internet.email()
  }

  function createClients (cbk) {
    var amountOfClient = config.client
    if (!_.isNumber(amountOfClient)) {
      return cbk(new Error('Should specify the number of clients'))
    }
    async.map(new Array(amountOfClient), function (c, cbk) {
      Utility.createUserAccount({email: fakeEmail(), type: 'CLIENT'}, function (err, results) {
        if (err) {
          return cbk(err)
        }
        if (!results) {
          return cbk(new Error('No results'))
        }
        if (!results.user) {
          return cbk(new Error('No user'))
        }
        var user = results.user
        user.status = 'ACTIVE'
        user.firstName = faker.name.firstName()
        user.lastName = faker.name.lastName()
        user.phone = faker.phone.phoneNumber()
        user.jobTitle = faker.name.jobTitle()
        // user.profilePicture = faker.image.avatar()
        user.verified = true

        user.save(function (err, user) {
          if (err) {
            return cbk(err)
          }
          debug('create %s\t login link: %s\n', user.type, 'http://localhost/checkToken?token=' + user.auth.access.token + '&email=' + user.email)
          cbk(null, user)
        })
      })
    }, cbk)
  }

  function createAuditors (cbk) {
    var amountOfAuditor = config.auditor
    if (!_.isNumber(amountOfAuditor)) {
      return cbk(new Error('Should specify the number of auditors'))
    }
    async.map(new Array(amountOfAuditor), function (c, cbk) {
      Utility.createUserAccount({email: fakeEmail(), type: 'AUDITOR'}, function (err, results) {
        if (err) {
          return cbk(err)
        }
        if (!results) {
          return cbk(new Error('No results'))
        }
        if (!results.user) {
          return cbk(new Error('No user'))
        }
        var user = results.user
        user.status = 'ACTIVE'
        user.firstName = faker.name.firstName()
        user.lastName = faker.name.lastName()
        user.phone = faker.phone.phoneNumber()
        user.jobTitle = faker.name.jobTitle()
        // user.profilePicture = faker.image.avatar()
        user.verified = true

        user.save(function (err, user) {
          if (err) {
            return cbk(err)
          }
          debug('create %s, login link: %s\n', user.type, 'http://localhost/checkToken?token=' + user.auth.access.token + '&email=' + user.email)
          cbk(null, user)
        })
      })
    }, cbk)
  }

  return function () {
    var args = Array.prototype.slice.call(arguments)
    var cbk = args[args.length - 1]
    async.parallel({
      clients: createClients,
      auditors: createAuditors
    }, cbk)
  }
}

function generateBusinessesAndFirms (results, cbk) {
  var auditors = results.auditors
  var clients = results.clients

  if (!_.isArray(auditors) || !_.isArray(clients)) {
    return cbk(new Error('Require a list of clients and auditors'))
  }

  async.waterfall([
    function (cbk) {
      mongoose.connection.db.dropCollection('businesses', function (err) {
        cbk(err)
      })
    },
    function (cbk) {
      mongoose.connection.db.dropCollection('firms', function (err) {
        cbk(err)
      })
    },
    function (cbk) {
      async.parallel({
        businesses: function (cbk) {
          async.map(clients, function (client, cbk) {
            var business = new Models.Auvenir.Business({
              acl: [{id: client._id, role: 'member'}],
              name: faker.company.companyName(),
              logo: faker.image.business(),
              address: {
                unit: faker.address.secondaryAddress(),
                streetAddress: faker.address.streetAddress(),
                city: faker.address.city(),
                stateProvince: faker.address.state(),
                postalCode: faker.address.zipCode(),
                country: faker.address.country()
              }
            })
            business.save(function (err) {
              cbk(err, business)
            })
          }, cbk)
        },
        firms: function (cbk) {
          async.map(auditors, function (auditor, cbk) {
            var firm = new Models.Auvenir.Firm({
              acl: [{id: auditor._id, role: 'member'}],
              name: faker.company.companyName(),
              logo: faker.image.business()
            })
            firm.save(function (err) {
              cbk(err, firm)
            })
          }, cbk)
        }
      }, function (err, {businesses, firms}) {
        cbk(err, {
          clients,
          auditors,
          businesses,
          firms
        })
      })
    }
  ], cbk)
}

function generateTemplates (results, cbk) {
  Models.Auvenir.Templates.insert({
    name: 'default',
    categories: ['Financials'],
    requests: [
      {category: 'Financials', name: 'General Ledger', description: 'Description Placeholder', requests: []},
      {category: 'Financials', name: 'Trial Balance', description: 'Description Placeholder', requests: []},
      {category: 'Financials', name: 'Bank Statements', description: 'Description Placeholder', requests: []}
    ]
  }, function (err) {
    cbk(err, results)
  })
}

function generateEngagement (results, cbk) {
  var auditors = results.auditors
  var clients = results.clients
  const businesses = results.businesses
  const firms = results.firms

  function genEngObj (clientId, auditorId) {
    const clientBusinessIndex = _.findIndex(businesses, (b) => {
      let found = false
      b.acl.forEach((acl) => {
        if (acl.id.toString() === clientId.toString()) {
          found = true
        }
      })
      return found
    })
    const businessID = businesses[clientBusinessIndex]._id.toString()

    const auditorFirmIndex = _.findIndex(firms, (f) => {
      let found = false
      f.acl.forEach((acl) => {
        if (acl.id.toString() === auditorId.toString()) {
          found = true
        }
      })
      return found
    })
    const firmID = firms[auditorFirmIndex]._id.toString()

    return {
      name: faker.name.firstName(),
      dueDate: faker.date.future(),
      auditorId,
      clientId,
      businessID,
      firmID
    }
  }

  if (!_.isArray(auditors) || !_.isArray(clients)) {
    return cbk(new Error('Require a list of clients and auditors'))
  }

  async.map(clients, function (client, cbk) {
    var auditor = auditors[getRandomInt(0, 1)]
    Models.Auvenir.Engagement.create(genEngObj(client._id, auditor._id), (err, engagement) => {
      if (err) {
        return cbk(err)
      }
      debug('Engagement %s \tcreated for Client %s \tand Auditor %s\n', engagement.name, client.email, auditor.email)
      cbk(null, engagement)
    })
  }, function (err, engagements) {
    cbk(err, {
      clients: clients,
      auditors: auditors,
      engagements: engagements
    })
  })
}

function generateConversationForRequests (results, cbk) {
  var engagements = results.engagements
  if (!_.isArray(engagements)) {
    return cbk(new Error('Require a list of engagements'))
  }

  async.each(engagements, function (engagement, cbk) {
    async.waterfall([
      function (cbk) {
        Models.Auvenir.Request.find({engagementID: engagement._id}, function (err, requests) {
          cbk(err, requests)
        })
      },
      function (requests, cbk) {
        async.each(requests, function (request, cbk) {
          async.waterfall([
            function (cbk) {
              var conversation = new Models.Auvenir.Conversation({
                acl: [{
                  id: engagement.acl[0].id,
                  role: 'member'
                }, {
                  id: engagement.acl[1].id,
                  role: 'member'
                }],
                title: faker.lorem.words(),
                status: 'ACTIVE'
              })
              conversation.save(function (err, conversation) {
                cbk(err, conversation)
              })
            },
            function (conversation, cbk) {
              var message = new Models.Auvenir.Message({
                conversationID: conversation._id,
                from: engagement.acl[0].id,
                content: faker.lorem.paragraphs(),
                attachments: [],
                status: 'OK'
              })
              message.save(function (err) {
                cbk(err, conversation)
              })
            },
            function (conversation, cbk) {
              debug('create conversation: %s\tfor request: %s\twithin engagement: %s\n', conversation.title, request.name, engagement.name)
              request.conversationID = conversation._id
              request.save(function (err) {
                cbk(err)
              })
            }
          ], cbk)
        }, cbk)
      }
    ], cbk)
  }, cbk)
}

debug('----start----')
var tasks = [
  mongoSetup,
  generateUsers({client: 3, auditor: 2}),
  generateBusinessesAndFirms,
  generateTemplates
]
switch (level) {
  case 'all':
    tasks = tasks.concat([
      generateEngagement,
      generateConversationForRequests
    ])
    break
  case 'user':
    break
}
async.waterfall(tasks, function (err) {
  if (err) {
    debug(err)
    return process.exit(1)
  }
  debug('----done----')
  process.exit(0)
})
