/* eslint no-console: 0 */
console.log('Starting up required modules...')
var appConfig = require('../config')
var Models = require('../models')

var models = [
  { 'name': 'Activity', 'obj': Models.Auvenir.Activity },
  { 'name': 'Business', 'obj': Models.Auvenir.Business },
  { 'name': 'Engagement', 'obj': Models.Auvenir.Engagement },
  { 'name': 'File', 'obj': Models.Auvenir.File },
  { 'name': 'Firm', 'obj': Models.Auvenir.Firm },
  { 'name': 'Template', 'obj': Models.Auvenir.Template },
  { 'name': 'User', 'obj': Models.Auvenir.User }
]

var processed = 0

removeDocuments(models[processed].obj)

function removeDocuments (model) {
  Models.remove({}, function (err, data) {
    if (err) {
      console.log(err)
      checkNext()
    } else {
      if (data.result) {
        if (data.result.ok === 1) {
          console.log(models[processed].name + ': cleared ' + data.result.n + ' entries.')
          checkNext()
        } else {
          console.error('Remove operation was not OK')
          checkNext()
        }
      } else {
        console.error('Error: Unknown result returned.')
        checkNext()
      }
    }
  })
}

function checkNext () {
  processed++
  if (processed < Models.length) {
    removeDocuments(Models[processed].obj)
  } else {
    console.log('Done clearing')
    process.exit()
  }
}
