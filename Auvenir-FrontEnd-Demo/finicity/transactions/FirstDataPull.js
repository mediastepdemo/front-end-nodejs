const logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = logger.init(__filename)
const errors = require(rootDirectory + '/finicity/plugins/errorCodes')
const moment = require('moment')
const async = require('async')
const apiUtilities = require(rootDirectory + '/finicity/plugins/apiUtilities')
const dbFinUtilities = require(rootDirectory + '/finicity/plugins/dbFinUtilities')
const dbDataUtilities = require(rootDirectory + '/finicity/plugins/dbDataUtilities')
const statusCodes = require(rootDirectory + '/finicity/plugins/statusCodes')

/**
 * This function performs the first pull of transactions from Finicity for the
 * accounts that were selected during the authorization session.
 *
 * @param {Object}   finicity   - the local config variables
 * @param {Object}   session  - the current session
 * @param {Function} callback - the callback function
 */
var FirstDataPull = function (finicity, data, session, callback) {
  // The API and database utilites.
  var apiUtils = new apiUtilities(finicity)
  var dbData = new dbDataUtilities(finicity)
  var finData = new dbFinUtilities(finicity)

  var ownerID = session.ownerID
  var consumerID = session.consumerID
  var finCustomerID = session.finCustomerID

  // Send the status to the caller.
  // @TODO: emit callback event here ... maybe as a parm in callback
  // var options = {
  //   url: session.xxxxx,
  //   headers: {
  //     'Content-Type': 'application/json'
  //   },
  //   json: {
  //     'name': 'finicity',
  //     'userID': session.userID,
  //     'consumerID': session.consumerUID,
  //     'status': statusCodes.retrieving
  //   }
  // }
  // request.post(options, function (error, response, body) {
  //   if (error) {
  //     error(error)
  //   }
  // })

  if (!data.bankStatementDateRange || !data.bankStatementDateRange.startDate || !data.bankStatementDateRange.endDate) {
    return callback(errors.NO_DATE_RANGE)
  }
  // Get Unix epoch times for back statement date range
  const fromDate = Math.round(data.bankStatementDateRange.startDate.getTime() / 1000.0)
  const toDate = Math.round(data.bankStatementDateRange.endDate.getTime() / 1000.0)

  // Get the consumer because it has the institutionID.
  finData.findConsumerById(consumerID, function (err, consumer) {
    if (err || !consumer) {
      error('Error finding the consumer.')
      error(JSON.stringify(err, null, 2))
      callback(err)
      return
    }
    var institutionID = consumer.institutionID

    // Find all of the consumer accounts that were selected.
    finData.findConsumerAccounts(consumerID, function (err, accounts) {
      if (err) {
        error('Error finding the consumer accounts.')
        error(JSON.stringify(err, null, 2))
        callback(err)
        return
      }

      // Go through each consumer account and pull the transactions from Finicity.
      async.map(accounts, function (anAccount, callback) {
        // If historical transactions have not been loaded, then move to the next one.
        if (!anAccount.loaded) {
          error('An account is selected but transactions have not been loaded.')
          error(JSON.stringify(anAccount, null, 2))
          callback(null, anAccount)
          return
        }

        // Find the data account that corresponds to the consumer account.
        dbData.findDataAccount(ownerID, institutionID, anAccount.number, anAccount.name, function (err, dataAccount) {
          if (err || !dataAccount) {
            error('Error finding the data account')
            error(JSON.stringify(err, null, 2))
            callback(err)
            return
          }

          // Determine the date range over which the transactions are to be pulled.
          var balance = dataAccount.balance

          // Get the account transactions and save them.
          apiUtils.getSaveAccountTransactions(dataAccount._id, finCustomerID, anAccount.finAccountID, fromDate, toDate, balance, function (err, dates) {
            if (err) {
              error('Error saving account transactions.')
              error(err)
              callback(err, anAccount)
              return
            }

            // Update the consumer account with the actual date range of transactions pulled.
            finData.updateConsumerAccountDates(anAccount._id, dates.startDate, dates.endDate, function (err, id) {
              if (err) {
                error('Error updaing the consumer account with the transaction dates.')
                error(err)
              }
              callback(err, anAccount)
            })
          })
        })
      }, function (err, accounts) {
        if (err) {
          error(err)
        }

        callback(err)

        // Send the status to the caller provided in the connect endpoint.
        // @TODO: emit callback event here ... maybe as a parm in callback
        // var options = {
        //   url: session.xxxxx,
        //   headers: {
        //     'Content-Type': 'application/json'
        //   },
        //   json: {
        //     'name': 'finicity',
        //     'userID': session.userID,
        //     'consumerID': session.consumerUID,
        //     'status': statusCodes.synced
        //   }
        // }
        // request.post(options, function (error, response, body) {
        //   if (error) {
        //     error(error)
        //   }
        // })
      })
    })
  })
}

module.exports = FirstDataPull
