const logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = logger.init(__filename)
const moment = require('moment')
const async = require('async')
const validate = require('./validate')
const utilities = require('./utilities')
const errors = require(rootDirectory + '/finicity/plugins/errorCodes')
const apiUtilities = require(rootDirectory + '/finicity/plugins/apiUtilities')
const dbFinUtilities = require(rootDirectory + '/finicity/plugins/dbFinUtilities')

/**
 * This handles the accounts that were selected by the user.
 *
 * @param {Object}  finicity - the app local variables initially created in app.js
 * @param {Object}  data   - the data send from the client in the socket event
 */
module.exports = function (finicity, data) {
  var apiUtils = new apiUtilities(finicity)
  var finData = new dbFinUtilities(finicity)

  /**
   * This function updates accounts. It goes through an array of source accounts.
   * If a match is found in the array of updates, it uses this account to update
   * the source account.
   *
   * @param  {Array} accounts - an array of account objects to be updated
   * @param  {Array} updates  - an array of updated account objects
   * @return {Array}          - the updated array of account objects
   */
  var updateAlteredAccounts = function (accounts, updates) {
    // Go through each of the array of source accounts.
    var updtdAccounts = []
    accounts.map(function (anAccount, index) {
      // See if there is a match found in the array of updates.
      var found = false
      for (var i = 0; i < updates.length; i++) {
        if (anAccount.finID === updates[i].id) {
          // Found a match so update the source account.
          var newAccount = finData.createAccount(updates[i])
          newAccount.transactions = anAccount.transactions
          updtdAccounts.push(newAccount)
          found = true
          break
        }
      }
      if (!found) {
        updtdAccounts.push(anAccount)
      }
    })

    return updtdAccounts
  }

  /**
   * This function activates the accounts that were selected and updates the
   * selection status.
   *
   * @param  {String}   finCustomerID    - the customer ID
   * @param  {String}   finInstitutionID - the institution ID
   * @param  {Array}    accounts         - an array of account objects of all
   *                                       the accounts for the institution
   * @param  {Array}    accountsSelected - an array of account objects for the
   *                                       selected accounts
   * @param  {Function} mainCallback     - the callback function
   * @return {Array}                     - an array of updated accounts is returned
   *                                       through the callback, otherwise an error
   */
  var activateAndSelect = function (finCustomerID, finInstitutionID, accounts, accountsSelected, mainCallback) {
    // Now process the selected accounts.
    async.waterfall([
      function (callback) {
        // Activate accounts that have been selected and have a status of pending.
        apiUtils.activateCustomerAccounts(finCustomerID, finInstitutionID, accountsSelected, function (err, alreadyActivated, newlyActivated) {
          if (err) {
            callback(err, 1)
          } else {
            var selected = alreadyActivated
            if (newlyActivated) {
              selected = selected.concat(newlyActivated)
              accounts = updateAlteredAccounts(accounts, newlyActivated)
            }
            callback(err, 1, accounts, selected)
          }
        })
      },
      function (task, accounts, selectedAccounts, callback) {
        async.map(accounts, function (anAccount, callback) {
          var found = false
          for (var i = 0; i < selectedAccounts.length; i++) {
            if (anAccount.finID === selectedAccounts[i].id) {
              found = true
              break
            }
          }
          if (found) {
            anAccount.selected = true
            anAccount.dateSelected = moment().unix()
          }

          callback(null, anAccount)
        }, function (err, result) {
          callback(err, 2, result)
        })
      }
    ], function (err, task, accounts) {
      // If there were no errors return the accounts.
      if (err) {
        error('Error happened during task: ' + task)
        error(JSON.stringify(err, null, 2))
        mainCallback(err)
      } else {
        mainCallback(null, accounts)
      }
    })
  }

  /**
   * This function loads the historical transactions for the accounts.
   *
   * @param  {String}   finCustomerID - the customer ID
   * @param  {Array}    accounts      - an array of account objects
   * @param  {Function} callback      - the callback function
   * @return {Array}                  - an array of updated accounts is returned
   *                                    through the callback, otherwise an error
   */
  var loadHistoricTransactions = function (finCustomerID, accounts, mfa, callback) {
    // Load historic transactions for all accounts of the given institution.
    apiUtils.loadHistoricTransactions(finCustomerID, accounts, mfa, function (err, updtdAccounts) {
      if (err) {
        error('Error happened loading historic transactions.')
        error(JSON.stringify(err, null, 2))
      }
      callback(err, updtdAccounts)
    })
  }

  /**
   * This function gets the error that was returned by the loadHistoricTransactions()
   * function and creates a new error object.
   *
   * @param  {Object} err - the error object
   * @return {Object}     - the new error object
   */
  var getLoadHistoricError = function (err) {
    var error = {}
    switch (err.code) {
      case errors.finicity.FAIL_MFATIMEOUT.code:
        error = {code: 4, msg: errors.finicity.FAIL_MFATIMEOUT.msg}
        break

      case errors.finicity.FAIL_MFAREQUIRED.code:
        var result = {
          mfaChallenge: err.mfaChallenge,
          mfaSession: err.mfaSession,
          account: err.account
        }
        error = {
          code: 3,
          msg: errors.finicity.FAIL_MFAREQUIRED.msg,
          result: result
        }
        break

      case errors.finicity.FAIL_MFA.code:
        error = {code: 2, msg: errors.finicity.FAIL_MFA.msg}
        break

      default:
        error = {code: 1, msg: apiUtils.getError(err)}
    }

    return error
  }

  /**
   * This function processes the accounts that were selected or the answer to a
   * security question (MFA).
   *
   * @param  {String}   reason       - the reason the function is being called,
   *                                   either 'MFA' or 'SELECT'
   * @param  {Function} mainCallback - callback function
   * @return {Object}                - an object indicaing the result is returned
   *                                   through the callback
   */
  this.processAccountSelectOrMfa = function (reason, mainCallback) {
    var session, finCustomerID, finInstitutionID

    // If processing an MFA, validate the MFA information from the client.
    var mfaInfo = null
    if (reason === 'MFA') {
      var valid = validate.mfa(data)
      if (valid.code !== 0) {
        mainCallback(valid)
        return
      }
      mfaInfo = data.mfaInfo
      utilities.mfaRemoveImages(mfaInfo)
    }

    async.waterfall([
      function (callback) {
        // Validate the user data received on the socket.
        validate.userData(data, finicity, function (err, result) {
          if (!err) {
            // Get the session information.
            session = result.session
            finCustomerID = session.finCustomerID
            finInstitutionID = session.institution.finID
          }
          callback(err, 1)
        })
      },
      function (task, callback) {
        var accounts = session.accounts
        if (reason === 'MFA') {
          // Get the accounts.
          callback(null, 3, accounts)
        } else {
          // Activate the selected accounts if required and save the selection.
          var accountsSelected = data.accountsSelected
          activateAndSelect(finCustomerID, finInstitutionID, accounts, accountsSelected, function (err, updtdAccounts) {
            callback(err, 3, updtdAccounts)
          })
        }
      },
      function (task, accounts, callback) {
        // Load historic transactions for all accounts.
        loadHistoricTransactions(finCustomerID, accounts, mfaInfo, function (err, updtdAccounts) {
          accounts = updateAlteredAccounts(accounts, updtdAccounts)
          callback(null, 4, err, accounts)
        })
      }
    ], function (err, task, loadHistoricalErr, accounts) {
      if (err) {
        error('Error happened during task: ' + task)
        error(JSON.stringify(err, null, 2))
        mainCallback({code: 1, msg: apiUtils.getError(err)})
        return
      }

      // Determine what the next state should be.
      if (loadHistoricalErr) {
        var histError = getLoadHistoricError(loadHistoricalErr)
        if (histError.code === 3) {
          newState = 'ACCOUNT_MFA'
          stateInfo = {'mfa': histError.result, 'accounts': accounts}
        } else {
          mainCallback(histError)
          return
        }
      } else {
        newState = 'LOADED'
        stateInfo = accounts
      }

      // Update the session information.
      var finData = new dbFinUtilities(finicity)
      finData.updateSession(session, newState, stateInfo, function (err) {
        if (err) {
          mainCallback({code: 1, msg: err.msg})
        } else if (loadHistoricalErr) {
          mainCallback(histError)
        } else {
          // Success.
          mainCallback({code: 0})
        }
      })
    })
  }
}
