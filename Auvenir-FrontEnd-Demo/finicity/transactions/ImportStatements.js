const logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = logger.init(__filename)
const errors = require(rootDirectory + '/finicity/plugins/errorCodes')
const _ = require('lodash')
const __ = require(rootDirectory + '/plugins/utilities/_lodash')
const moment = require('moment')
const async = require('async')
const tmp = require('tmp')
const dbFinUtilities = require(rootDirectory + '/finicity/plugins/dbFinUtilities')
const dbDataUtilities = require(rootDirectory + '/finicity/plugins/dbDataUtilities')
const statusCodes = require(rootDirectory + '/finicity/plugins/statusCodes')
const Access = require(rootDirectory + '/plugins/utilities/accessControls')
const Models = require(rootDirectory + '/models')

/**
 * This function imports the selected accounts as statement files into the engagement.
 * accounts that were selected during the authorization session.
 *
 * @param {Object}   finicity   - the local config variables
 * @param {Function} callback - the callback function
 */
const ImportStatements = function (finicity, session, data, callback) {
  // The API and database utilites.
  const dbData = new dbDataUtilities(finicity)
  const finData = new dbFinUtilities(finicity)
  const ownerID = session.ownerID
  const consumerID = session.consumerID
  const institutionName = session.institution.name
  const userID = data.currentUserID
  const engagementID = data.engagementID
  const fromDate = moment(data.bankStatementDateRange.startDate).format('YYYY_MM_DD')
  const toDate = moment(data.bankStatementDateRange.endDate).format('YYYY_MM_DD')

  Access.engagement(engagementID, userID, (err, engagement) => {
    if (err) return callback(err)
    // Get the consumer because it has the institutionID.
    finData.findConsumerById(consumerID, function (err, consumer) {
      if (err) return callback(err)
      if (!consumer) return callback('Consumer not found')
      const institutionID = consumer.institutionID
      // Find all of the consumer accounts that were selected.
      finData.findConsumerAccounts(consumerID, (err, accounts) => {
        if (err) return callback(err)
        async.each(accounts, (account, callback) => {
          // If historical transactions have not been loaded, then move to the next one.
          if (!account.loaded) {
            warn(`Account (_id='${account._id}', number=${account.number}, name='${account.name}) is selected but transactions have not been loaded`)
            return callback()
          }
          // Find the data account that corresponds to the consumer account.
          dbData.findDataAccount(ownerID, institutionID, account.number, account.name, function (err, dataAccount) {
            if (err) return callback(err)
            if (!dataAccount) return callback('Data account not found')
            const maskedLength = dataAccount.number.length - 4
            const masked = (dataAccount.number.substr(0, maskedLength).replace(/\w/g, 'X') + dataAccount.number.substr(maskedLength)).replace(/[\s\-]/g, '_')
            const filename = `${institutionName}_${masked}_from_${fromDate}_to_${toDate}.csv`
            dbData.exportTransactionsAsCSV(dataAccount._id, filename, (err, csvFile) => {
              if (err) return callback(err)
              var file = new Models.Auvenir.File()
              file.name = filename
              file.size = `${csvFile.length}`
              file.path = filename
              file.status = 'ACTIVE'
              file.owner = userID
              file.bucket = {
                name: 'fs',
                id: csvFile._id
              }
              file.save((err) => {
                if (err) return callback(err)
                engagement.files.push(file._id)
                engagement.save((err) => {
                  if (err) return callback(err)
                  callback()
                })
              })
            })
          })
        }, (err) => {
          callback(err)
        })
      })
    })
  })
  function createFile (callback) {
    let file = new Models.Auvenir.File({
      name: fileInfo.title,
      size: fileInfo.fileSize ? fileInfo.fileSize : 'unknown',
      path: '/',
      sortType: 'AUTO',
      status: 'ACTIVE',
      owner: __.toObjectID(userID)
    })
    file.save((err) => {
      if (err) {
        error(moduleName + ' ERROR: saving coreFile object.')
        return callback(err)
      }
      return callback(err, coreFile, fileInfo, GDConsumer, engagement, type)
    })
  }
}

module.exports = ImportStatements
