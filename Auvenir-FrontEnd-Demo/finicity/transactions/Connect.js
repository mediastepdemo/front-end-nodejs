const logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = logger.init(__filename)
const _ = require('lodash')
const __ = require(rootDirectory + '/plugins/utilities/_lodash')
const errors = require(rootDirectory + '/finicity/plugins/errorCodes')
const validate = require('./validate')
const APIUtilities = require(rootDirectory + '/finicity/plugins/apiUtilities')
const DBFinUtilities = require(rootDirectory + '/finicity/plugins/dbFinUtilities')

/**
 * This function will get the login details from Finicity that are required by
 * the client to build the login screen that is specific to the institution.
 *
 * @param {Object}   finicity   - the app local variables initially created in app.js
 * @param {Object}   data     - the data send from the client in the socket event
 * @param {Function} callback - the callback function
 */
var Connect = function (finicity, data, callback) {
  // Get the required parameters from the body.
  const uid = data.currentBusinessID
  const userID = data.currentUserID
  const finInstitutionID = data.institutionID

  // Validate the parameters.
  if (!__.isObjectID(uid)) {
    return callback(_.clone(errors.api.MISSING_UID))
  }
  if (!__.isObjectID(userID)) {
    return callback(_.clone(errors.api.MISSING_USERID))
  }
  if (!_.isNumber(finInstitutionID)) {
    return callback(_.clone(errors.api.MISSING_INSTITUTION))
  }

  // Start the to create a connection to an institution.
  new APIUtilities(finicity).startConnection(uid, userID, finInstitutionID, (err, result) => {
    if (err) return callback(err)
    _.assign(result, errors.api.OK)
    return callback(result)
  })
}

module.exports = Connect
