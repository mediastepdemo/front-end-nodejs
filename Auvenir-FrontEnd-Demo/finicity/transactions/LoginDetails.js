const logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = logger.init(__filename)
const errors = require(rootDirectory + '/finicity/plugins/errorCodes')
const validate = require('./validate')
const apiUtilities = require(rootDirectory + '/finicity/plugins/apiUtilities')
const dbFinUtilities = require(rootDirectory + '/finicity/plugins/dbFinUtilities')

/**
 * This function will get the login details from Finicity that are required by
 * the client to build the login screen that is specific to the institution.
 *
 * @param {Object}   finicity   - the app local variables initially created in app.js
 * @param {Object}   data     - the data send from the client in the socket event
 * @param {Function} callback - the callback function
 */
var LoginDetails = function (finicity, data, callback) {
  // Validate the query parameters.
  validate.userData(data, finicity, function (err, result) {
    if (err) {
      callback(err)
      return
    }

    // Get the session information.
    var session = result.session
    var integration = session.provider
    var finInstitutionID = session.institution.finID

    // Get the institution details needed to create the authorization page.
    var apiUtils = new apiUtilities(finicity)
    apiUtils.getInstitutionDetails(integration, finInstitutionID, function (err, details) {
      if (err) {
        error(JSON.stringify(err, null, 2))
        callback({code: 1, msg: apiUtils.getError(err)})
        return
      }

      // Get the institution and loginForm
      var loginDetail = {
        code: 0,
        institutionRecord: details.result.institution,
        loginInfo: details.result.loginForm
      }

      // Update the session information.
      var finData = new dbFinUtilities(finicity)
      finData.updateSession(session, 'LOGIN', loginDetail, function (err) {
        if (err) {
          callback({code: 2, msg: err.msg})
        } else {
          // Success, return the details that are needed for the authorization page.
          callback(loginDetail)
        }
      })
    })
  })
}

module.exports = LoginDetails
