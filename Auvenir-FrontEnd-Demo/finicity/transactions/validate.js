const logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = logger.init(__filename)
const errors = require(rootDirectory + '/finicity/plugins/errorCodes')
const dbFinUtilities = require(rootDirectory + '/finicity/plugins/dbFinUtilities')

/**
 * This function validates the login information.
 *
 * @param  {Array}  data - the data received from the client in a socket event
 * @return {Object}      - an object indicating the result
 */
var login = function (data) {
  // Make sure we have the Login object.
  if (!data || !data.loginInfo) {
    return {code: 1, msg: errors.client.MISSING_LOGININFO.msg}
  }

  // Make sure we have login information.
  var loginInfo = data.loginInfo
  if (!loginInfo || !Array.isArray(loginInfo) || loginInfo.length < 1) {
    return {code: 1, msg: errors.client.MISSING_LOGININFO.msg}
  }

  // Loop through each object of the login information.
  for (var i = 0, num = loginInfo.length; i < num; i++) {
    if (!loginInfo[i].value || loginInfo[i].value.trim().length < 1) {
      return {code: 2, msg: errors.client.MISSING_LOGINVALUE.msg}
    }
  }

  // Login information is complete.
  return {code: 0}
}

/**
 * This function validates the MFA information.
 *
 * @param  {Object} data - the data received from the client in a socket event
 * @return {Object}      - an object indicating the result
 */
var mfa = function (data) {
  // Make sure we have the MFA object.
  if (!data.mfaInfo) {
    return {code: 1, msg: errors.client.MISSING_MFAINFO.msg}
  }

  // Make sure we have MFA information.
  var mfaInfo = data.mfaInfo
  if (!mfaInfo || !mfaInfo.mfaChallenge || !mfaInfo.mfaChallenge.questions) {
    return {code: 1, msg: errors.client.MISSING_MFAINFO.msg}
  }

  var questions = mfaInfo.mfaChallenge.questions
  if (!Array.isArray(questions) || questions.length < 1) {
    return {code: 2, msg: errors.client.MISSING_MFAINFO.msg}
  }

  // Loop through each question of the MFA information.
  for (var i = 0, num = questions.length; i < num; i++) {
    if (!questions[i].answer || questions[i].answer.trim().length < 1) {
      return {code: 3, msg: errors.client.MISSING_MFAANSWER.msg}
    }
  }

  // MFA information is complete.
  return {code: 0}
}

/**
 * This function validates the user information received from the client in a
 * socket event.
 *
 * @param  {Object}   data     - the data received from the client in a socket event
 * @param  {Object}   finicity   - the app local variables initially created in app.js
 * @param  {Function} callback - the callback function
 * @return {Object}            - the status is return through the callback
 */
var userData = function (req, finicity, callback) {
  // Make sure we have the user information.
  if (!req) {
    callback({code: 1, msg: errors.client.MISSING_USERDATA.msg})
    return
  }

  // Check the sessionID.
  if (typeof req.sessionID !== 'string') {
    callback({code: 2, msg: errors.client.FAIL_SESSIONID.msg})
    return
  }

  // Check that the ID exists in the database.
  var finData = new dbFinUtilities(finicity)
  finData.findSession(req.sessionID, function (err, theSession) {
    if (err || !theSession) {
      callback({code: 3, msg: errors.client.FAIL_SESSIONNOTFOUND.msg})
    } else {
      callback(null, {code: 0, session: theSession})
    }
  })
}

module.exports = {login, mfa, userData}
