const logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = logger.init(__filename)
const async = require('async')
const validate = require('./validate')
const utilities = require('./utilities')
const errors = require(rootDirectory + '/finicity/plugins/errorCodes')
const apiUtilities = require(rootDirectory + '/finicity/plugins/apiUtilities')
const dbFinUtilities = require(rootDirectory + '/finicity/plugins/dbFinUtilities')
const accountTypes = require(rootDirectory + '/finicity/plugins/accountTypes')

/**
 * This function handles validating the information received from the login
 * and MFA screens. This is done by sending the information to Finicity in the
 * add all accounts endpoint. When successful, a list of accounts is received
 * from Finicity which is then sent to the client to be presented to the user to
 * allow them to select the accounts they want.
 *
 * @param {Object}   finicity       - the app local variables initially created in app.js
 * @param {Object}   data         - the data send from the client in the socket event
 * @param {String}   type         - LOGIN or MFA
 * @param {Function} mainCallback - the callback function
 */
var Authorize = function (finicity, data, type, mainCallback) {
  var session

  async.waterfall([
    function (callback) {
      // Validate the query parameters.
      validate.userData(data, finicity, function (err, result) {
        session = result.session
        callback(err)
      })
    },
    function (callback) {
      // Is an MFA or Login being authorized?
      var mfa, valid
      if (type === 'MFA') {
        mfa = true
        valid = validate.mfa(data)
        if (valid.code !== 0) {
          callback(valid)
          return
        }
        authInfo = data.mfaInfo
        utilities.mfaRemoveImages(authInfo)
      } else {
        mfa = false
        valid = validate.login(data)
        if (valid.code !== 0) {
          callback(valid)
          return
        }
        authInfo = data.loginInfo
      }

      callback(null, mfa, authInfo)
    },
    function (mfa, authInfo, callback) {
      // Get the required information from the session.
      var finInstitutionID = session.institution.finID
      var finCustomerID = session.finCustomerID

      // Get all the accounts of the institution.
      var apiUtils = new apiUtilities(finicity)
      apiUtils.addAllCustomerAccounts(finCustomerID, finInstitutionID, mfa, authInfo, function (err, accounts) {
        if (err) {
          error(JSON.stringify(err, null, 2))
          switch (err.code) {
            case errors.finicity.FAIL_MFATIMEOUT.code:
              callback({code: 4, msg: errors.finicity.FAIL_MFATIMEOUT.msg})
              break

            case errors.finicity.FAIL_CREDENTIALS.code:
              callback({code: 2, msg: errors.finicity.FAIL_CREDENTIALS.msg})
              break

            case errors.finicity.FAIL_MFAREQUIRED.code:
              var result = {mfaChallenge: err.mfaChallenge, mfaSession: err.mfaSession}
              callback(
                null,
                {code: 3, msg: errors.finicity.FAIL_MFAREQUIRED.msg, result: result},
                'LOGIN_MFA',
                result
              )
              break

            case errors.finicity.FAIL_MFA.code:
              callback({code: 2, msg: errors.finicity.FAIL_MFA.msg})
              break

            default:
              callback({code: 1, msg: apiUtils.getError(err)})
          }
        } else {
          callback(
            null,
            {code: 0, result: accounts, accountTypes: accountTypes},
            'ACCOUNT_LIST',
            accounts
          )
        }
      })
    }
  ], function (err, result, newState, stateInfor) {
    if (err) {
      mainCallback(err)
      return
    }

    // Update the session information.
    var finData = new dbFinUtilities(finicity)
    finData.updateSession(session, newState, stateInfor, function (err) {
      if (err) {
        mainCallback({code: 1, msg: err.msg})
      } else {
        // Success, return the details that are needed for the authorization page.
        mainCallback(result)
      }
    })
  })
}

module.exports = Authorize
