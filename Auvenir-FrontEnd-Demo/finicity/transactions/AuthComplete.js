const logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = logger.init(__filename)
const errors = require(rootDirectory + '/finicity/plugins/errorCodes')
const moment = require('moment')
const async = require('async')
const validate = require('./validate')
const apiUtilities = require(rootDirectory + '/finicity/plugins/apiUtilities')
const dbFinUtilities = require(rootDirectory + '/finicity/plugins/dbFinUtilities')
const dbDataUtilities = require(rootDirectory + '/finicity/plugins/dbDataUtilities')
const statusCodes = require(rootDirectory + '/finicity/plugins/statusCodes')

/**
 * This function is called when a user exits the authorization process or it
 * completes normally. It will save accounts in the database and delete accounts
 * from Finicity that are not being used.
 *
 * @param {String}   action       - is EXIT or DONE to identify if it is being
 *                                  called because the user exited the authorization
 *                                  or if it completed normally
 * @param {Object}   finicity       - the local config variables
 * @param {Object}   data         - the data received from the client
 * @param {Function} mainCallback - the callback function
 */
var AuthComplete = function (action, finicity, data, mainCallback) {
  // The API and database utilites.
  var apiUtils = new apiUtilities(finicity)
  var dbData = new dbDataUtilities(finicity)
  var finData = new dbFinUtilities(finicity)

  /**
   * This function creates a consumer document in the database if a new one is
   * required.
   *
   * @param  {ObjectId} owner_id       - the ObjectId of the Owners document
   * @param  {String}   userID         - the userID
   * @param  {Number}   finLoginID     - the institution login ID
   * @param  {ObjectId} institution_id - the Mongo objectId of the institution
   * @param  {Function} callback       - the callback function
   * @return {[type]}                  - nothing
   */
  var findOrCreateConsumer = function (owner_id, userID, finLoginID, institution_id, callback) {
    // Get the finLoginID that is being used.
    apiUtils.findOrCreateConsumer(owner_id, userID, finLoginID, institution_id, function (err, consumer) {
      if (err) {
        callback(err)
      } else {
        callback(null, consumer)
      }
    })
  }

  /**
   * This function gets the account information from Finicity for the given
   * institution. Then it updates the rawSource of each account in the session.
   *
   * @param  {String}   finCustomerID - the Finicity customer ID
   * @param  {String}   institution   - the institution record object
   * @param  {Array}    accounts      - array of account objects
   * @param  {Function} callback      - the callback function
   * @return {}                       - nothing
   */
  var getAccountInformation = function (finCustomerID, institution, accounts, callback) {
    // Get out if there are no accounts.
    var numAccounts = accounts.length
    if (numAccounts < 1) {
      saveCallback(null, accounts)
      return
    }

    // Get all the customer accounts from Finicity for the institution.
    apiUtils.getCustomerAccountsInstitution(finCustomerID, institution.finID, function (err, result) {
      if (err) {
        error('Error getting the customer accounts by institution!')
        error(err)
        callback(err)
        return
      }

      // Find the accounts of the institution and update them with the info from Finicity.
      var institutionAccounts = result.accounts
      async.map(institutionAccounts, function (anInstitutionAccount, callback) {
        for (var i = 0; i < numAccounts; i++) {
          if (accounts[i].finID === anInstitutionAccount.id) {
            accounts[i].rawSource = anInstitutionAccount
            break
          }
        }
        callback(null)
      }, function (err) {
        callback(err, accounts)
      })
    })
  }

  /**
   * This function saves the accounts in the database. Only selected accounts
   * are added to the database.
   *
   * @param  {String}   integration    - the integration that is being used
   * @param  {ObjectId} owner_id       - the Mongo objectId of the owner
   * @param  {Object}   institution_id - the Mongo objectId of the institution
   * @param  {Array}    accounts       - array of account objects
   * @param  {Function} mainCallback   - the callback function
   * @return {}                        - nothing
   */
  var saveAccounts = function (integration, owner_id, institution_id, accounts, mainCallback) {
    // Get out if there are no accounts.
    if (accounts.length < 1) {
      mainCallback(null, accounts)
      return
    }
    var dateCreated = moment().unix()

    // Go through each account and save the accounts that were selected.
    async.map(accounts, function (anAccount, callback) {
      // Don't save if there is no account or if the account was not selected.
      if (!anAccount || !anAccount.selected) {
        callback(null, anAccount)
        return
      }

      // Format and save the account.
      var accountDoc = dbData.formatAccount(integration, owner_id, institution_id, anAccount, dateCreated)
      dbData.findOrCreateDataAccount(accountDoc, function (err, id) {
        if (err) {
          callback(err, anAccount)
        } else {
          callback(null, anAccount)
        }
      })
    }, function (err, accounts) {
      if (err) {
        error('***** Error saving account information')
      }
      mainCallback(err, accounts)
    })
  }

  /**
   * This function saves consumer accounts. A consumer account is saved for each
   * of the accounts, both selected and not selected.
   *
   * @param  {ObjectId} consumer_id    - the Mongo objectId of the consumer
   * @param  {Array}    accounts       - array of account objects
   * @param  {Function} mainCallback   - the callback function
   * @return {}                        -  nothing
   */
  var saveConsumerAccounts = function (consumer_id, accounts, mainCallback) {
    // Get out if there are no accounts.
    if (accounts.length < 1) {
      callback(null, accounts)
      return
    }
    var dateCreated = moment().unix()

    // Go through each account and save a consumer account.
    async.map(accounts, function (anAccount, callback) {
      // Format and save the consumerAccount.
      var consumerAcctDoc = finData.formatConsumerAccount(consumer_id, anAccount, dateCreated)
      finData.findOrCreateConsumerAccount(consumerAcctDoc, function (err, id) {
        if (err) {
          callback(err, anAccount)
        } else {
          callback(null, anAccount)
        }
      })
    }, function (err, accounts) {
      if (err) {
        error('***** Error saving consumer account information')
      }
      mainCallback(err, accounts)
    })
  }

  /**
   * This function tells Finicity to delete an account. An account should be
   * deleted if it is not in the database or if it is in the database but it was
   * not selected.
   *
   * @param  {String}   finCustomerID  - the customer ID
   * @param  {Array}    accounts       - array of account objects
   * @param  {String}   which          - identifies if ALL or NOTALL of the
   *                                     accounts are to be deleted
   * @param  {Function} deleteCallback - the callback function
   * @return {}                        - nothing
   */
  var deleteAccounts = function (finCustomerID, accounts, which, deleteCallback) {
    // Get out if there are no accounts.
    if (accounts.length < 1) {
      deleteCallback(null, accounts)
      return
    }

    // Go through each account and delete from Finicity if required.
    async.map(accounts, function (anAccount, callback) {
      anAccount.error = false
      anAccount.deleted = false

      if (which === 'NOTALL' && anAccount.selected) {
        callback(null, anAccount)
        return
      }

      // Find the account in the database to see if it was selected.
      // TODO: Is it necessary to find it in the database?
      dbData.findCustomerDataAccount(finCustomerID, anAccount.finID, function (err, theAccount) {
        if (err) {
          anAccount.error = true
        } else if (!theAccount) {
          apiUtils.deleteCustomerAccount(finCustomerID, anAccount.finID, function (err, result) {
            if (err) {
              anAccount.error = true
            } else {
              anAccount.deleted = true
            }
          })
        }
        callback(err, anAccount)
      })
    }, function (err, updtAccounts) {
      deleteCallback(err, updtAccounts)
    })
  }

  // Validate the query parameters.
  validate.userData(data, finicity, function (err, result) {
    if (err) {
      mainCallback(err)
      return
    }

    // Get the session information.
    var session = result.session

    /*
     * The institution/account authorization process has either completed
     * successfully or the user has cancelled. If cancelled, then it is necessary
     * to delete any accounts not previously added to Finicity. If finished
     * successfully, then add the institution and accounts to the database.
     */
    async.waterfall([

      function (callback) {
        //
        if (action === 'EXIT') {
          callback(null)
        } else {
          async.waterfall([
            // Save the institution.
            function (callback) {
              var institutionRecord = dbData.formatInstitution(session.provider, session.institution)
              dbData.findOrCreateDataInstitution(institutionRecord, function (err, savedId) {
                callback(err, savedId)
              })
            },
            // Find or create the consumer.
            function (institution_id, callback) {
              findOrCreateConsumer(session.ownerID, session.userID, session.finLoginID, institution_id, function (err, theConsumer) {
                if (!err) {
                  session.consumerID = theConsumer._id
                  session.consumerUID = theConsumer.consumerUID
                }
                callback(err, institution_id)
              })
            },
            // Get the most recent account information from Finicity.
            function (institution_id, callback) {
              getAccountInformation(session.finCustomerID, session.institution, session.accounts, function (err, result) {
                callback(err, institution_id)
              })
            },
            // Save the accounts and consumer accounts.
            function (institution_id, callback) {
              async.parallel([
                function (callback) {
                  saveAccounts(session.provider, session.ownerID, institution_id, session.accounts, function (err) {
                    callback(err)
                  })
                },
                function (callback) {
                  saveConsumerAccounts(session.consumerID, session.accounts, function (err) {
                    callback(err)
                  })
                }
              ], function (err) {
                callback(err)
              })
            },
            function (callback) {
              // @TODO: DOWNLOAD TRANSACTIONS AND CREATE A CSV FILE FOR EACH ACCOUNT
              callback()
            }
          ], function (err) {
            callback(err)
          })
        }
      // },
      // // Delete the accounts at Finicity not selected.
      // function (callback) {
      //   // var toDelete = (action === 'EXIT') ? 'ALL' : 'NOTALL'
      //   deleteAccounts(session.finCustomerID, session.accounts, toDelete, 'ALL' (err, updtAccounts) {
      //     callback(err)
      //   })
      //   callback()
      // },
      // // Remove the session from the database.
      // function (callback) {
      //   finData.removeSession(session._id, function (err) {
      //     callback(err)
      //   })
      }
    ], function (err) {
      // Send the status to the caller
      // @TODO: emit callback event here ... maybe as a parm in callback
      // var status
      // if (err || action === 'EXIT') {
      //   status = statusCodes.disconnected
      // } else {
      //   status = statusCodes.connected
      // }
      //
      // var options = {
      //   url: session.xxxxx,
      //   headers: {
      //     'Content-Type': 'application/json'
      //   },
      //   json: {
      //     'name': 'finicity',
      //     'userID': session.userID,
      //     'consumerID': session.consumerUID,
      //     'status': status
      //   }
      // }
      // request.post(options, function (error, response, body) {
      //   if (error) {
      //     error(error)
      //   }
      // })

      if (err) {
        mainCallback({code: 1, msg: err.msg})
      } else {
        mainCallback({code: 0, session: session})
      }
    })
  })
}

module.exports = AuthComplete
