/**
 * This function removes the keys image and imageChoices from the MFA challenge
 * object. These are removed because they are not required when they are sent
 * to FInicity.
 *
 * @param  {Object} mfaInfo - the MFA challenge object
 * @return {Object}         - the modified MFA challenge object
 */
var mfaRemoveImages = function (mfaInfo) {
  var questions = mfaInfo.mfaChallenge.questions
  for (var i = 0, num = questions.length; i < num; i++) {
    if (questions[i].imageChoices) {
      delete questions[i].imageChoices
    }
    if (questions[i].image) {
      delete questions[i].image
    }
  }
}

module.exports = {mfaRemoveImages}
