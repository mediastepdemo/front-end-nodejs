const logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = logger.init(__filename)
var mongoose = require('mongoose')

/*
 * This schema is based upon the Finicity transaction record. So it is very specific
 * to Finicity.
 */

var transactionSchema = mongoose.Schema({
  accountID: { type: mongoose.Schema.ObjectId, ref: 'Accounts' },

  integration: { type: String },                // The integration from which the transaction was retrieved
  dateCreated: { type: Number },                // The date when this document was added to the database

  // The following fin IDs may not be required. They are only valid at the time a transaction
  // is added with Finicity.
  finID: { type: String, index: true },   // The integrations ID of the transaction
  finAccountID: { type: String, index: true },   // The integrations ID of the account to which this transaction belongs
  finCustomerID: { type: String, index: true },   // The integrations ID of the customer associated with this account

  amount: { type: Number },                // Total amount of the transaction
  bonusAmount: { type: Number },                // Portion of transaction allocated to bonus
  checkNum: { type: Number },                // The cheque number of the transaction
  finDateCreated: { type: Number },                // A timestamp of when the transaction was added to the integrations system
  description: { type: String },                // The description of the transaction
  escrowAmount: { type: Number },                // Portion of transaction allocated to escrow
  feeAmount: { type: Number },                // Portion of transaction allocated to fee
  interestAmount: { type: Number },                // Portion of transaction allocated to interest
  memo: { type: String },                // The memo field of the transaction
  postedDate: { type: Number },                // A timestamp of when the transaction was posted or cleared by the institution
  principalAmount: { type: Number },                // Portion of transaction allocated to principal
  status: { type: String },                // One of active, pending or shadow
  transactionDate: { type: Number },                // A timestamp of when the transaction occurred
  type: { type: String },                // The type of the transaction (eg. atm, cash, check)
  unitQuantity: { type: Number },                // The number of units in the transaction
  unitValue: { type: Number },                // The value of each unit in the transaction
  categorization: {                                  // The categorization of the transaction
    normalizedPayeeName: { type: String },           // A normalized payee derived from the description and memo fields
    category: { type: String },           // Category (eg. ATM Fee, Arts, Bonus, Buy, Deposit)
    scheduleC: { type: String },           // Schedule C (eg. Advertising, Benefits, Check, Internet)
    sic: { type: Number }           // The payee's sic code
  },
  cusipNo: { type: String },
  currentBalance: { type: Number, default: null },
  raw: { type: Object }

}, {
  collection: 'transactions'
})

module.exports = function (connection) {
  return connection.model('Transactions', transactionSchema)
}
