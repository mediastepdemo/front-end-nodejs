const logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = logger.init(__filename)
var mongoose = require('mongoose')

/*
 * This schema is based upon the Finicity institution record. So it is very specific
 * to Finicity.
 */

var institutionSchema = mongoose.Schema({
  integration: { type: String, default: 'finicity' }, // The integration from which the institution was retrieved
  dateCreated: { type: Number },              // The date when this document was added to the database

  finID: { type: String, index: true }, // The integrations ID of the institution
  name: { type: String },              // The name of the institution
  typeDescription: { type: String },              // Account type. One of Banking, Investments, Credit Cards/Accounts, Workplace Retirement, Mortgages and Loans, Insurance
  urlHomeApp: { type: String },              // The URL of the institution's primary home page
  urlLogonApp: { type: String },              // The URL of the institution's login page
  urlProductApp: { type: String },              //
  phone: { type: String },              // The institution's primary phone number
  currency: { type: String },              // The institutions primary currency
  email: { type: String },              // The institution's primary contact email
  specialText: { type: String },              // Any special text found on the institution's website
  address: {                                      // Parent element for the address of the institution's headquarters
    addressLine1: { type: String },              // Address information for the institution's headquarters
    addressLine2: { type: String },              // Address information for the institution's headquarters
    city: { type: String },              // The city of the institution's headquarters
    state: { type: String },              // Two-letter code for the state of the institution's headquarters
    postalCode: { type: String },              // The postal code of the institution's headquarters
    country: { type: String }              // The country of the institution's headquarters
  },
  raw: { type: Object }
}, {
  collection: 'institutions'
})

module.exports = function (connection) {
  return connection.model('Institutions', institutionSchema)
}
