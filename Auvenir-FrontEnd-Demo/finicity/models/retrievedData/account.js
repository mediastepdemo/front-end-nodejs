const Logger = require(rootDirectory + '/plugins/logger/logger')
const { info, warn, error } = Logger.init(__filename)
var mongoose = require('mongoose')

/*
 * This schema is based upon the Finicity account record. So it is very specific
 * to Finicity.
 */

var accountSchema = mongoose.Schema({
  ownerID: { type: mongoose.Schema.ObjectId, ref: 'Owners' },
  institutionID: { type: mongoose.Schema.ObjectId, ref: 'Institutions' },

  integration: { type: String, default: 'finicity' },              // The integration from which the account was retrieved
  dateCreated: { type: Number },              // The date when this document was added to the database

  // The following fin IDs may not be required. They are only valid at the time an account
  // is added with Finicity.
  finID: { type: String, index: true }, // The integrations ID of the account
  finCustomerID: { type: String, index: true }, // The integrations ID of the customer associated with this account
  finInstitutionID: { type: String, index: true }, // The integrations ID of the institution to which this account belongs
  finLoginID: { type: Number, index: true }, // The institution login ID

  number: { type: String },              // The account number from the institution
  name: { type: String },              // The account name from the institution
  status: { type: String },              // pending during account discovery, always active following successful account activation
  type: { type: String },              // The account type (eg. checking, savings, cd)
  balance: { type: Number },              // The cleared balance of the account as-of balanceDate
  balanceDate: { type: Number },              // A timestamp of when the balance was captured
  currency: { type: String },
  finDateCreated: { type: Number },              // A timestamp of when the account was added to the Finicity system
  lastTransactionDate: { type: Number },              // A timestamp of when the account was last modified
  detail: { type: Object },              // Optional element with other details, dependent on the institution and the account type
  order: { type: Array },
  position: { type: Array },
  raw: { type: Object }
}, {
  collection: 'accounts'
})

module.exports = function (connection) {
  return connection.model('Accounts', accountSchema)
}
