const logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = logger.init(__filename)
var mongoose = require('mongoose')

var status = ['ACTIVE', 'INACTIVE']

var consumerSchema = mongoose.Schema({
  ownerID: { type: mongoose.Schema.ObjectId, ref: 'Owners' },
  institutionID: { type: mongoose.Schema.ObjectId, ref: 'Institutions' },

  consumerUID: { type: String, index: true },
  userID: { type: String },
  status: { type: String, enum: status, default: 'ACTIVE' },
  integration: { type: String, default: 'finicity' },
  dateCreated: { type: Number },
  finLoginID: { type: Number, index: true }
}, {
  collection: 'consumers'
})

module.exports = function (connection) {
  return connection.model('Consumers', consumerSchema)
}
