const logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = logger.init(__filename)
var mongoose = require('mongoose')

var type = ['STATUS', 'PULL']

var recordSchema = mongoose.Schema({
  consumerAccountID: { type: mongoose.Schema.ObjectId, ref: 'ConsumerAccounts' },

  type: { type: String, enum: type },
  dateCreated: { type: Number },                  // The date when this document was added to the database

  pulled: { type: Boolean, default: false },
  datePulled: { type: Number, default: null },
  pullFailCode: { type: String, default: null },

  askFromDate: { type: Number, default: null },
  askToDate: { type: Number, default: null },
  rcvdFromDate: { type: Number, default: null },
  rcvdToDate: { type: Number, default: null },
  rcvdCount: { type: Number, default: 0 },

  aggregationStatusCode: { type: Number, default: null },   // The status of the most recent aggregation attempt
  aggregationSuccessDate: { type: Number, default: null },   // A timestamp of the last successful aggregation of the account
  aggregationAttemptDate: { type: Number, default: null }   // A timestamp of the last aggregation attempt, whether successful or not
}, {
  collection: 'Records'
})

module.exports = function (connection) {
  return connection.model('Records', recordSchema)
}
