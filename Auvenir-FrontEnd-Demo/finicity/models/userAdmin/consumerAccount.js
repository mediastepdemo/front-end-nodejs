const logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = logger.init(__filename)
var mongoose = require('mongoose')

var status = ['ACTIVE', 'INACTIVE']

var consumerAccountSchema = mongoose.Schema({
  consumerID: { type: mongoose.Schema.ObjectId, ref: 'Consumers' },

  status: { type: String, enum: status, default: 'ACTIVE' },
  dateCreated: { type: Number },

  selected: { type: Boolean, default: false }, // Was the account selected by the user
  dateSelected: { type: Number, default: null },  // The date the account was selected

  finAccountID: { type: String, default: null },  // The Finicity ID of the account, only valid if selected
  number: { type: String },                  // The account number
  name: { type: String },                  // The account name
  type: { type: String },                  // The account type (eg. checking, savings, cd)

  loaded: { type: Boolean, default: false }, // Have the historical transactions been loaded
  dateLoaded: { type: Number, default: null },  // The date, in unix epoch time, that the transactions were loaded
  loadFailCode: { type: String, default: null },  // Error code if there was an error loading historical transactions

  txnFromDate: { type: Number, default: null },
  txnToDate: { type: Number, default: null }
}, {
  collection: 'consumerAccounts'
})

module.exports = function (connection) {
  return connection.model('ConsumerAccounts', consumerAccountSchema)
}
