const logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = logger.init(__filename)
var mongoose = require('mongoose')

var states = ['INIT', 'LOGIN', 'LOGIN_MFA', 'ACCOUNT_LIST', 'ACCOUNT_MFA', 'LOADED']
var status = ['ACTIVE', 'INACTIVE']

var accountSchema = mongoose.Schema({
  dateCreated: { type: Number, default: null },
  selected: { type: Boolean, default: false },
  dateSelected: { type: Number, default: null },
  error: { type: Boolean },
  deleted: { type: Boolean },
  transactions: {
    loaded: { type: Boolean, default: false }, // Have the historical transactions been loaded.
    dateLoaded: { type: Number, default: null },  // The date unix epoch time the transactions were loaded.
    loadFailCode: { type: String, default: null }  // Error code if there was an error loading historical trans.
  },

  finID: { type: String, default: null },
  number: { type: String, default: null },
  name: { type: String, default: null },
  status: { type: String, default: null },
  type: { type: String, default: null },
  finLoginID: { type: Number, index: true },
  rawSource: { type: Object, default: {} }
})

var authSessionSchema = mongoose.Schema({
  ownerID: { type: mongoose.Schema.ObjectId, ref: 'Owners' },
  consumerID: { type: mongoose.Schema.ObjectId, ref: 'Consumers' },

  dateCreated: { type: Number, default: null },
  expires: { type: Number, default: 0 },
  state: { type: String, default: 'INIT', enum: states },
  previousState: { type: String, default: 'INIT', enum: states },

  socketID: { type: String, default: null },
  provider: { type: String, default: null },
  finCustomerID: { type: String, default: null, index: true },
  finLoginID: { type: Number, default: null },

  uid: { type: String, default: null, index: true },
  userID: { type: String, default: null },

  ownerUID: { type: String, default: null },
  consumerUID: { type: String, default: null },

  loginForm: {
    dateCreated: { type: Number, default: null },
    formDetails: { type: Array, default: [] }
  },
  mfa: {
    dateCreated: { type: Number, default: null },
    details: { type: Object, default: {} }
  },
  institution: {
    dateCreated: { type: Number, default: null },
    finID: { type: String, default: null },
    name: { type: String, default: null },
    authorized: { type: Boolean, default: false },
    rawSource: { type: Object, default: {} }
  },
  accounts: [accountSchema]
}, {
  collection: 'authSessions'
})

module.exports = function (connection) {
  return connection.model('AuthSessions', authSessionSchema)
}
