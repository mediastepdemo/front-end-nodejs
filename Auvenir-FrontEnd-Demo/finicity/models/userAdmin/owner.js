const logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = logger.init(__filename)
var mongoose = require('mongoose')

var status = ['ACTIVE', 'INACTIVE']

var ownerSchema = mongoose.Schema({
  ownerUID: { type: String, index: true },
  uid: { type: String, index: true },
  status: { type: String, default: 'ACTIVE', enum: status },
  dateCreated: { type: Number },
  finCustomer: {
    name: { type: String, default: null },
    finID: { type: String, default: null },
    dateCreated: { type: Number, default: null }
  }
}, {
  collection: 'owners'
})

module.exports = function (connection) {
  return connection.model('Owners', ownerSchema)
}
