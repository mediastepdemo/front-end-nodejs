// FILE: finicity/index.js
//
//      Implementation of the Finicity Integration API
//
const logger = require(rootDirectory + '/plugins/logger/logger')
const {info, error} = logger.init(__filename)
const __ = require(rootDirectory + '/plugins/utilities/_lodash')
const moment = require('moment')
const Utility = require(rootDirectory + '/plugins/utilities/utility')
const apiUtilities = require('./plugins/apiUtilities')
const Connect = require('./transactions/Connect')
const LoginDetails = require('./transactions/LoginDetails')
const Authorize = require('./transactions/Authorize')
const Accounts = require('./transactions/Accounts')
const AuthComplete = require('./transactions/AuthComplete')
const FirstDataPull = require('./transactions/FirstDataPull')
const ImportStatements = require('./transactions/ImportStatements')

  /** Finicity Integration */
module.exports = class Finicity {
  /** Constructor
   *
   *  @param config - server configuration
   *  @param app - the Expres app
   *  @param db - the Mongo database to be used by Finicity
   */
  constructor (config, app, db) {
    // API CALLS
    this.FN_CONNECT = 'fn-connect'
    this.FN_LOGIN_FORM_REQUEST = 'fn-loginFormRequest'
    this.FN_LOGIN_SUBMIT = 'fn-loginSubmit'
    this.FN_LOGIN_SECURITY_SUBMIT = 'fn-loginSecuritySubmit'
    this.FN_ACCOUNT_LIST_SELECTED_REQUEST = 'fn-accountListSelectedRequest'
    this.FN_SELECTION_SECURITY_SUBMIT = 'fn-selectionSecuritySubmit'
    this.FN_LOGIN_EXIT = 'fn-loginExit'
    this.FN_LOGIN_ERROR_EXIT = 'fn-loginErrorExit'
    this.FN_LOGIN_COMPLETE = 'fn-loginComplete'

    // API RESPONSE CODES
    this.FN_SERVER_ERROR = 'fn-serverError'
    this.FN_LOGIN_FORM_RESPONSE = 'fn-loginFormResponse'
    this.FN_ACCOUNT_LIST = 'fn-accountList'
    this.FN_LOGIN_FAILURE = 'fn-loginFailure'
    this.FN_LOGIN_SECURITY = 'fn-loginSecurity'
    this.FN_MFA_TIMEOUT = 'fn-mfaTimeout'
    this.FN_ACCOUNT_LIST_SELECTED_RESPONSE = 'fn-accountListSelectedResponse'
    this.FN_SELECTION_SECURITY_FAILURE = 'fn-selectionSecurityFailure'
    this.FN_SELECTION_SECURITY = 'fn-selectionSecurity'

    const cf = config.finicity
    this.base_url = cf.base_url
    this.app_key = cf.app_key
    this.partner_id = cf.partner_id
    this.partner_secret = cf.partner_secret
    this.token_duration = cf.token_duration
    this.historic_months = cf.historic_months
    this.refresh_interval_milliseconds = cf.refresh_interval_hours * 60 /* MINUTES */ * 60 /* SECONDS */ * 1000
    this.public_server = config.publicServer.url
    /* MILLISECONDS */
    this.appToken = {
      value: null,
      expires: moment().unix() - 1
    }
    this.db = db

    this.status = {
      available: false,
      httpCode: null,
      finCode: null
    }

    this.institutions = {
      count: 0,
      dateCreated: 0,
      records: [],
      recordsShort: []
    }

    this[this.FN_CONNECT] = this.connect
    this[this.FN_LOGIN_FORM_REQUEST] = this.loginFormRequest
    this[this.FN_LOGIN_SUBMIT] = this.loginSubmit
    this[this.FN_LOGIN_SECURITY_SUBMIT] = this.loginSecuritySubmit
    this[this.FN_ACCOUNT_LIST_SELECTED_REQUEST] = this.accountListSelectedRequest
    this[this.FN_SELECTION_SECURITY_SUBMIT] = this.selectionSecuritySubmit
    this[this.FN_LOGIN_EXIT] = this.loginExit
    this[this.FN_LOGIN_ERROR_EXIT] = this.loginErrorExit
    this[this.FN_LOGIN_COMPLETE] = this.loginComplete
    this.apiUtilities = new apiUtilities(this)
  }

  updateInstitutions () {
    this.apiUtilities.getFinAppToken((err) => {
      if (err) {
        error(JSON.stringify(err, null, 2))
        this.status.available = false
        this.status.httpCode = err.moreInfo.statusCode
        this.status.finCode = err.moreInfo.details.code
        return
      }
      this.status.available = true
      this.getInstitutions()
      // Continue to get institutions from Finicity at a regular interval.
      setInterval(() => {
        this.getInstitutions()
      }, this.refresh_interval_milliseconds)
    })
  }

  connect (data, callback) {
    Connect(this, data, callback)
  }

  loginFormRequest (data, callback) {
    LoginDetails(this, data, (result) => {
      if (result.code !== 0) {
        result.name = this.FN_SERVER_ERROR
      } else {
        result.name = this.FN_LOGIN_FORM_RESPONSE
      }
      callback(result)
    })
  }

  authResultCallback (result, callback) {
    switch (result.code) {
      case 0:
        // Success
        result.name = this.FN_ACCOUNT_LIST
        break

      case 2:
        // Login failed.
        result.name = this.FN_LOGIN_FAILURE
        break

      case 3:
        // MFA required.
        result.code = 0
        result.name = this.FN_LOGIN_SECURITY
        break

      case 4:
        // MFA timeout.
        result.name = this.FN_MFA_TIMEOUT
        break

      default:
        // All other errors.
        result.name = this.FN_SERVER_ERROR
    }
    callback(result)
  }

  loginSubmit (data, callback) {
    Authorize(this, data, 'LOGIN', (result) => {
      this.authResultCallback(result, callback)
    })
  }

  loginSecuritySubmit (data, callback) {
    Authorize(this, data, 'MFA', (result) => {
      this.authResultCallback(result, callback)
    })
  }

  selectionResultCallback (result, callback) {
    switch (result.code) {
      case 0:
        // Success
        result.name = this.FN_ACCOUNT_LIST_SELECTED_RESPONSE
        break

      case 2:
        // MFA failed.
        result.name = this.FN_SELECTION_SECURITY_FAILURE
        break

      case 3:
        // MFA required.
        result.code = 0
        result.name = this.FN_SELECTION_SECURITY
        break

      case 4:
        // MFA timeout.
        result.name = this.FN_MFA_TIMEOUT
        break

      default:
        // All other errors.
        result.name = this.FN_SERVER_ERROR
    }
    callback(result)
  }

  accountListSelectedRequest (data, callback) {
    if (data.code !== 0) {
      error(`during selection of accounts, code=${data.code}`)
      return callback({code: 1, msg: 'Error during selection of accounts.'})
    }

    const theAccounts = new Accounts(this, data)
    theAccounts.processAccountSelectOrMfa('SELECT', (result) => {
      this.selectionResultCallback(result, callback)
    })
  }

  selectionSecuritySubmit (data, callback) {
    const theAccounts = new Accounts(this, data)
    theAccounts.processAccountSelectOrMfa('MFA', (result) => {
      this.selectionResultCallback(result, callback)
    })
  }

  loginExit (data, callback) {
    AuthComplete('EXIT', this, data, (result) => {
      callback(result)
    })
  }

  loginErrorExit (data, callback) {
    AuthComplete('EXIT', this, data, (result) => {
      callback(result)
    })
  }

  loginComplete (data, callback) {
    Utility.getBankStatementDateRange(data, (err, bankStatementDateRange) => {
      if (err) return callback(err)
      data.bankStatementDateRange = bankStatementDateRange
      AuthComplete('DONE', this, data, (result) => {
        const session = result.session
        if (result.code === 0) {
          FirstDataPull(this, data, session, (err) => {
            if (err) return callback(err)
            ImportStatements(this, session, data, (err) => {
              if (err) return callback(err)
              callback({code: 0, session: session})
            })
          })
        }
      })
    })
  }

  /**
   * This function gets all the institutions from Finicity and saves them locally.
   * If the created date from Finicity is more recent than what has already been
   * saved, the local copy is replaced by the new copy.
   *
   * @return Institution
   */
  getInstitutions () {
    // Get the institutions from this.
    this.apiUtilities.getAllInstitutions((err, result) => {
      if (err) {
        error(err)
        return
      }

      // Only replace the local information if there has been a change.
      var institutions = this.institutions
      if (result.dateCreated > institutions.dateCreated) {
        // Save the institutions locally.
        this.institutions.records = result.institutions
        this.institutions.count = result.count
        this.institutions.dateCreated = result.dateCreated

        // Create and save a reduced record for each institution.
        this.institutions.recordsShort = result.institutions.map((i) => {
          return {
            id: i.id,
            name: i.name,
            accountTypeDescription: i.accountTypeDescription,
            urlHomeApp: i.urlHomeApp,
            tpCurrencyCode: i.tpCurrencyCode
          }
        })
      }
    })
  }
}
