
module.exports = {

  finicity: {
    bmo: {supported: true, id: 3415},
    cibc: {supported: true, id: 3417},
    pcfinancial: {supported: true, id: 3872},
    rbc: {supported: true, id: 1411},
    scotiabank: {supported: true, id: 3406},
    td: {supported: true, id: 1492},
    finbank: {supported: true, id: 101732},
    search: {supported: true, id: -1}
  }
}

