
module.exports = {

  OK: {code: 0, msg: 'Success'},

  core: {
    OK: {code: 'core-000', msg: 'Success'}
  },

  api: {
    OK: {code: 0, msg: 'Success'},
    NOT_AUTHORIZED: {code: 'api-001', msg: 'Error, not authorized.'},
    ROUTE_NOTAUTHORIZED: {code: 'api-002', msg: 'The route is not authorized.'},
    MISSING_UID: {code: 'api-003', msg: 'Error, missing or invalid unique ID.'},
    MISSING_USERID: {code: 'api-004', msg: 'Error, missing or invalid user ID.'},
    MISSING_INSTITUTION: {code: 'api-005', msg: 'Error, missing or invalid institution ID.'},
    ACCOUNT_NOTLOADED: {code: 'api-007', msg: 'Error, account must be loaded before getting transactions.'},
    INVALID_DATERANGE: {code: 'api-008', msg: 'Error, the from date must be less than the to date.'},
    INVALID_USER: {code: 'api-009', msg: 'Error, the user was not found.'},
    FAIL_GETTRANSACTIONS: {code: 'api-010', msg: 'Error, not able to get all transactions.'},
    MISSING_DATE: {code: 'api-011', msg: 'Error, missing or invalid date range.'},
    MISSING_CUSTOMERID: {code: 'api-012', msg: 'Error, missing or invalid customer ID.'},
    NO_INSTITUTIONS: {code: 'api-013', msg: 'Error, there are no institutions.'},
    NO_ACCOUNTS: {code: 'api-014', msg: 'Error, there are no accounts for the institution.'},
    EXISTING_SESSION: {code: 'api-015', msg: 'An integration is already in progress for that institution.'},
    MISSING_CALLBACKURL: {code: 'api-016', msg: 'Error, missing or invalid callback URL.'},
    MISSING_ACCOUNTID: {code: 'api-017', msg: 'Error, missing or invalid accountID.'},
    MISSING_STARTDATE: {code: 'api-018', msg: 'Error, missing or invalid start date.'},
    MISSING_ENDDATE: {code: 'api-019', msg: 'Error, missing or invalid end date.'},
    MISSING_INDEX: {code: 'api-020', msg: 'Error, missing or invalid index.'},
    MISSING_LIMIT: {code: 'api-021', msg: 'Error, invalid limit.'},
    MISSING_OWNERID: {code: 'api-022', msg: 'Error, missing or invalid ownerID.'},
    MISSING_CONSUMERID: {code: 'api-023', msg: 'Error, missing or invalid consumerID.'},
    MISSING_ENGAGEMENTID: {code: 'api-024', msg: 'Error, missing or invalid engagementID.'},
    NO_DATE_RANGE: {code: 'api-025', msg: 'Error, no bank statement date range given for engagement'}
  },

  db: {
    OK: {code: 'db-000', msg: 'Success'},
    FAIL_OWNERFIND: {code: 'db-001', msg: 'Error finding an owner document.'},
    MISSING_UID: {code: 'db-002', msg: 'Error, missing or invalid UID.'},
    FAIL_OWNERSAVE: {code: 'db-003', msg: 'Error saving a new owner document.'},
    MISSING_OWNERIDUID: {code: 'db-004', msg: 'Error, missing or invalid ownerID and UID.'},
    FAIL_USERUPDATE: {code: 'db-005', msg: 'Error updating owner document.'},
    MISSING_CUSTOMERDATA: {code: 'db-006', msg: 'Error, missing customer data.'},
    MISSING_CUSTOMERID: {code: 'db-007', msg: 'Error, missing customer ID.'},
    FAIL_ADDINSITUTION: {code: 'db-008', msg: 'Error adding new institution.'},
    FAIL_ADDACCOUNTS: {code: 'db-009', msg: 'Error adding new accounts to institution.'},
    FAIL_ENABLEACCOUNTS: {code: 'db-010', msg: 'Error enabling accounts of an institution.'},
    FAIL_UPDTACCOUNTS: {code: 'db-011', msg: 'Error updating accounts of an institution.'},
    MISSING_INSTITUTION: {code: 'db-012', msg: 'Error, missing or invalid institution ID.'},
    FAIL_FINDSESSION: {code: 'db-013', msg: 'Error finding a session document.'},
    FAIL_SAVESESSION: {code: 'db-014', msg: 'Error saving a session document.'},
    FAIL_REMOVESESSION: {code: 'db-015', msg: 'Error removing a session document.'},
    FAIL_NOSESSION: {code: 'db-016', msg: 'Error, no session document found to remove.'},
    INVALID_STATE: {code: 'db-017', msg: 'Error, invalid state.'},
    FAIL_UPDATESESSION: {code: 'db-018', msg: 'Error updating session.'},
    FAIL_CONSUMERFIND: {code: 'db-019', msg: 'Error finding a consumer document.'},
    FAIL_CONSUMERSAVE: {code: 'db-020', msg: 'Error saving a new consumer document.'},
    FAIL_CONSUMERACCTSAVE: {code: 'db-020', msg: 'Error saving a new consumer account document.'},
    FAIL_FINDCONSUMERACCT: {code: 'db-021', msg: 'Error finding consumer accounts.'},
    FAIL_CONSUMERACCTUPDT: {code: 'db-022', msg: 'Error updating consumer account document.'},
    FAIL_CONSUMERFINDID: {code: 'db-023', msg: 'Error finding a consumer document by ID.'},
    FAIL_CONSUMERFINDUID: {code: 'db-024', msg: 'Error finding a consumer document by UID.'}
  },

  dbData: {
    MISSING_INSTITUTIONID: {code: 'dataDb-001', msg: 'Error, missing or invalid institution ID.'},
    MISSING_UID: {code: 'dataDb-002', msg: 'Error, missing or invalid UID.'},
    MISSING_AUTHID: {code: 'dataDb-003', msg: 'Error, missing or invalid authID.'},
    MISSING_ACCOUNTID: {code: 'dataDb-004', msg: 'Error, missing or invalid account ID.'},
    MISSING_TRANSACTIONID: {code: 'dataDb-005', msg: 'Error, missing or invalid transaction ID.'},
    FAIL_INSTITUTION: {code: 'dataDb-006', msg: 'Error, finding or updating an institution.'},
    FAIL_FINDACCOUNT: {code: 'dataDb-007', msg: 'Error, finding a customer account.'},
    FAIL_ACCOUNT: {code: 'dataDb-008', msg: 'Error, finding or updating an account.'},
    FAIL_DATAACCOUNT: {code: 'dataDb-009', msg: 'Error, finding or an account.'},
    FAIL_TRANSACTIONCNT: {code: 'dataDb-010', msg: 'Error, finding the number of transactions.'},
    FAIL_TRANSACTIONS: {code: 'dataDb-011', msg: 'Error, finding the transactions.'},
    FAIL_TRANSACTION: {code: 'dataDb-012', msg: 'Error, finding the transaction.'}
  },

  finicity: {
    OK: {code: 'fin-000', msg: 'Success'},
    NOT_SUPPORTED: {code: 'fin-001', msg: 'Not currently supported.'},
    FAIL_PARTNERAUTH: {code: 'fin-002', msg: 'Error on POST partners authentication.'},
    FAIL_DELETEACCOUNT: {code: 'fin-003', msg: 'Error on DELETE customer account.'},
    FAIL_INSTITUTIONS: {code: 'fin-004', msg: 'Error on GET list of institutions.'},
    FAIL_INSTITUTION: {code: 'fin-005', msg: 'Error on GET single institution.'},
    FAIL_DETAILS: {code: 'fin-006', msg: 'Error on GET institution details.'},
    FAIL_LOGINFORM: {code: 'fin-007', msg: 'Error on GET login form.'},
    FAIL_ADDCUSTOMER: {code: 'fin-008', msg: 'Error on POST add customer.'},
    FAIL_GETCUSTOMER: {code: 'fin-009', msg: 'Error on GET customer.'},
    FAIL_DELETECUSTOMER: {code: 'fin-010', msg: 'Error on DELETE customer.'},
    FAIL_CUSTOMERACCT: {code: 'fin-011', msg: 'Error on GET customer accounts.'},
    FAIL_ADDALLACCOUNTS: {code: 'fin-012', msg: 'Error on POST add all customer accounts.'},
    FAIL_HISTORICTRANS: {code: 'fin-013', msg: 'Error on POST load historic transactions.'},
    FAIL_ACCOUNTTRANS: {code: 'fin-014', msg: 'Error on GET account transactions.'},
    FAIL_NORESULT: {code: 'fin-015', msg: 'Error, result is not available.'},
    FAIL_NOINSTITUTIONS: {code: 'fin-016', msg: 'Error, institutions are not available.'},
    FAIL_CREDENTIALS: {code: 'fin-017', msg: 'Error, invalid login credentials.'},
    FAIL_MFAREQUIRED: {code: 'fin-018', msg: 'Error, MFA required.'},
    FAIL_MFA: {code: 'fin-019', msg: 'Error, missing or invalid MFA answer.'},
    FAIL_MFATIMEOUT: {code: 'fin-020', msg: 'MFA session expired.'},
    FAIL_ACTIVATEACCOUNTS: {code: 'fin-021', msg: 'Error on POST activate customer accounts.'},
    FAIL_CUSTOMERACCTINST: {code: 'fin-022', msg: 'Error on GET customer accounts by institution.'}
  },

  client: {
    OK: {code: 'client-000', msg: 'Success'},
    MISSING_LOGININFO: {code: 'client-001', msg: 'Error, submitted login information is not complete.'},
    MISSING_LOGINVALUE: {code: 'client-002', msg: 'Error, submitted login information is missing submitted values.'},
    MISSING_MFAINFO: {code: 'client-003', msg: 'Error, submitted MFA information is not complete.'},
    MISSING_MFAANSWER: {code: 'client-004', msg: 'Error, submitted MFA information is missing the answer.'},
    MISSING_USERDATA: {code: 'client-005', msg: 'Error, missing sessionID.'},
    FAIL_SESSIONID: {code: 'client-006', msg: 'Error, invalid sessionID.'},
    FAIL_SESSIONNOTFOUND: {code: 'client-007', msg: 'Error, the given session does not exist.'}
  }
}
