
module.exports = {
  'connected': 'CONNECTED',
  'disconnected': 'DISCONNECTED',
  'retrieving': 'RETRIEVING',
  'synced': 'SYNCED'
}
