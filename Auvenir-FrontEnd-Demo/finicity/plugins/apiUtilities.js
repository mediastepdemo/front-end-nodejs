const logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = logger.init(__filename)
const moment = require('moment')
const async = require('async')
const makeUuid = require('uuid')
const APIFinicity = require('./apiFinicity')
const DBFinUtilities = require('./dbFinUtilities')
const DBDataUtilities = require('./dbDataUtilities')
const institutionInfo = require('./institutionInfo')
const errors = require('./errorCodes')

// Some constants.
const PROVIDER = 'finicity'
const ACCOUNT_STATUS_PENDING = 'pending'
const ACCOUNT_TYPE_UNKNOWN = 'unknown'

module.exports = class APIUtilities {

  constructor (finicity) {
    this.finicity = finicity
    this.finAPI = new APIFinicity(finicity)
    this.finData = new DBFinUtilities(finicity)
    this.dbData = new DBDataUtilities(finicity)
  }

  providerValid (provider) {
    return (provider === PROVIDER)
  }

  institutionValid (institution) {
    if (typeof institutionInfo[PROVIDER] === 'undefined') {
      return false
    }

    if (typeof institution === 'undefined' ||
        typeof institutionInfo[PROVIDER][institution] === 'undefined') {
      return false
    }

    return true
  }

  getInstitutionID (institution) {
    // Is the institution supported?
    if (!institutionInfo[PROVIDER][institution].supported) {
      return false
    }

    return institutionInfo[PROVIDER][institution].id
  }

  getError (error) {
    let msg = 'Integration error ' + error.code + ' '
    const info = error.moreInfo
    if (info && info.details) {
      msg += '. Status code ' + info.statusCode
      msg += ', code ' + info.details.code
      msg += ', message "' + info.details.msg + '".'
    } else {
      msg += error.msg
    }

    return msg
  }

  /**
   * This function checks if the given date is valid.
   *
   * @param  {date} date - the date to validate. Expected to be ISO 8601.
   * @return {Boolean}   - true if valid, false otherwise.
   */
  validateDate (date) {
    if (!date) {
      return false
    }
    return moment(date, moment.ISO_8601, true).isValid()
  }

  /**
   * This fuction checks if the Finicity app token has expired. The appToken is
   * an object that contains a value and an expiration time. So this function
   * checks if the current time is less than the expiration time.
   *
   * @param  {object} appToken - the Finicity app token
   * @return {Boolean}         - true if token expired, false otherwise
   */
  tokenExpired (appToken) {
    // Get the current time in seconds since UNIX epoch.
    const secondsNow = moment().unix()
    return !((secondsNow < appToken.expires))
  }

  /**
   * This function gets the Finicity app token. The token expires after a set
   * duration. If the token has not expired it is returned. If it has expired
   * a new one is obtained from Finicity.
   *
   * @param  {Function} callback - the callback function
   * @return {String}            - the Finicity app token is return through the callback
   */
  getFinAppToken (callback) {
    let self = this
    let finicity = this.finicity
    if (self.tokenExpired(finicity.appToken)) {
      // The Finicity app token has expired. Get a new one from Finicity.
      self.finAPI.partnerAuth(function (err, data) {
        if (err) {
          callback(err)
        } else {
          // New token received, calculate new expiration and save.
          const tokenDuration = finicity.token_duration
          const secondsNow = moment().unix()
          finicity.appToken.value = data.result
          finicity.appToken.expires = secondsNow + tokenDuration
          callback(null, data.result)
        }
      })
    } else {
      // The token has not expired. Return the current one.
      callback(null, finicity.appToken.value)
    }
  }

  /**
   * This function creates a new customer name. The customer name is used when
   * adding a customer to Finicity. In return, Finicity provides a finCustomerID
   * that is used when adding accounts and getting transactins. The customer
   * name must be unique following these rules:
   * - minimum 6 characters and maximum 255
   * - any mix of uppercase, lowercase, numeric and special characters !@#$%&*_-+
   *
   * @param  {String} ownerUID - the owner UID from the application using this
   *                             integration service
   * @param  {String} uid      - the UID from the application using this
   *                             integration service
   * @return {String}          - the new user name
   */
  createCustomerName (ownerUID, uid) {
    const newCustomerName = 'Auvneir_' + ownerUID
    if (newCustomerName.length > 6) {
      newCustomerName.slice(0, 254)
    }
    return newCustomerName
  }

  /**
   * This function creates a new Finicity customer for the application using
   * this integration service.
   *
   * @param  {String}   uid      - the UID from the application using this
   *                               integration service
   * @param  {String}   ownerUID - the owner UID from the application using
   *                               this integration service
   * @param  {Function} callback - the callback function
   * @return {String}            - the new customer ID is returned through the callback
   */
  createCustomer (uid, ownerUID, callback) {
    let self = this
    // Get the Finicity app token required for Finicity API calls.
    self.getFinAppToken(function (err, appToken) {
      if (err) {
        callback(err)
        return
      }

      // Add a new Finicity customer.
      const newName = self.createCustomerName(ownerUID, uid)
      self.finAPI.addCustomer(appToken, newName, function (err, result) {
        if (err) {
          callback(err)
          return
        }

        // Create the new customer object.
        const newData = {
          name: newName,
          finID: result.result.id,
          dateCreated: result.result.createdDate
        }
        callback(null, newData)
      })
    })
  }

  /**
   * This function creates a new owner UID for use by the application (onwer)
   * using this integration service. The owner UID must be unique and is used to
   * validate request over the API and is also used to identify what is written
   * to the database.
   *
   * @param  {String} uid - the unique ID from the application using this
   *                        integration service that identifies an owner
   * @return {String}     - the ownerUID
   */
  createOwnerUID (uid) {
    // TODO should the new UID be check against the DB to ensure it is unique?
    return 'FinOwner_' + moment().unix() + '_' + makeUuid.v4()
  }

  /**
   * This function finds or creates an owner for the application that is using
   * this integration service. If an owner is found, its ownerUID is returned.
   * Otherwise a unique ownerUID is created that the application MUST use in all
   * subsequent communication with this service.
   *
   * @param  {String}   uid      - a unique ID from the application using this
   *                               integration service
   * @param  {Function} callback - the callback function
   * @return {String}            - the owner UID is returned through the callback
   */
  findOrCreateOwner (uid, callback) {
    let self = this
    // Find the matching owner documment.
    self.finData.findOwner(uid, null, function (err, theOwner) {
      if (err) {
        callback(err)
        return
      }

      // Found a matching owner document, so return the existing ownerUID.
      if (theOwner) {
        callback(null, theOwner)
        return
      }

      /*
       * There is no owner document. Create a new ownerUID, create a new Finicity
       * customer, then create and save the new owner document.
       */
      const ownerUID = self.createOwnerUID(uid)
      const createdDate = moment().unix()  // Time in seconds since UNIX epoch.
      self.createCustomer(uid, ownerUID, function (err, customerData) {
        if (err) {
          callback(err)
          return
        }
        self.finData.createOwner(uid, ownerUID, customerData, function (err, newowner) {
          if (err) {
            callback(err)
          } else {
            callback(null, newowner)
          }
        })
      })
    })
  }

  /**
   * This function creates a new owner UID for use by the application (onwer)
   * using this integration service. The ownerUID must be unique and is used to
   * validate request over the API and is also used to identify what is written
   * to the database.
   *
   * @param  {String} uid - the unique ID from the application using this
   *                        integration service that identifies a user.
   * @return {String}     - the consumerUID
   */
  createConsumerUID (uid) {
    // TODO should the new UID be check against the DB to ensure it is unique?
    return 'FinConsumer_' + moment().unix() + '_' + makeUuid.v4()
  }

  /**
   * This function finds a consumer. If a consumer is not found, a new one is
   * created.
   *
   * @param  {ObjectId} owner_id       - the ObjectId of the Owners document
   * @param  {String}   userID         - the userID
   * @param  {Number}   finLoginID     - the institution login ID
   * @param  {ObjectId} institution_id - the Mongo objectId of the institution
   * @param  {Function} callback       - the callback function
   * @return {}                        - returned through the callback
   */
  findOrCreateConsumer (owner_id, userID, finLoginID, institution_id, callback) {
    let self = this
    self.finData.findConsumer(owner_id, userID, finLoginID, function (err, consumer) {
      if (err || consumer) {
        callback(err, consumer)
        return
      }

      /*
       * There is no consumer document. Create a new consumerUID then create a
       * new consumer document.
       */
      const consumerUID = self.createConsumerUID(userID)
      self.finData.createConsumer(owner_id, userID, finLoginID, consumerUID, institution_id, function (err, consumer) {
        callback(err, consumer)
      })
    })
  }

  /**
   * This function searches the local finicity insitutions to find a record that
   * matches the given id.
   *
   * @param  {String} id - the id of the institution to find
   * @return {Array}     - an array containing the institution record
   */
  findInstitutionRecord (id) {
    const allRecords = this.finicity.institutions.records
    const foundRecord = []
    for (let i = 0; i < this.finicity.institutions.count; i++) {
      if (allRecords[i].id === id) {
        foundRecord.push(allRecords[i])
        break
      }
    }
    return foundRecord
  }

  /**
   * This function gets all the institutions supported by Finicity.
   *
   * @param  {Function} callback - the callback function
   * @return {Object}            - the institutions are returned through the callback
   */
  getAllInstitutions (callback) {
    let self = this
    // Get the Finicity app token required for Finicity API calls.
    self.getFinAppToken(function (err, appToken) {
      if (err) {
        callback(err)
        return
      }

      // Get the institutions.
      self.finAPI.getInstitutions(appToken, '*', function (err, result) {
        if (err) {
          callback(err)
          return
        }

        // Check that we received a result.
        if (typeof result.result === 'undefined') {
          callback({
            code: errors.finicity.FAIL_NORESULT.code,
            msg: errors.finicity.FAIL_NORESULT.msg
          })
          return
        }
        if (typeof result.result.institutions === 'undefined') {
          callback({
            code: errors.finicity.FAIL_NOINSTITUTIONS.code,
            msg: errors.finicity.FAIL_NOINSTITUTIONS.msg
          })
        } else {
          callback(null, {
            provider: PROVIDER,
            count: result.result.found,
            dateCreated: result.result.createdDate,
            institutions: result.result.institutions
          })
        }
      })
    })
  }

  /**
   * This function starts the process of creating an integration with an
   * institution.
   *
   * @param  {String}   uid              - a unique ID that owns any data retrieved
   *                                       using the integration
   * @param  {String}   userID           - the ID of the user that owns the integration
   * @param  {String}   finInstitutionID - the Finicity ID of the institution being
   *                                       integrated with
   * @param  {Function} callback         - the callback function
   * @return {}
   */
  startConnection (uid, userID, finInstitutionID, callback) {
    let self = this
    // Create a new owner for this UID if one does not already exist.
    self.findOrCreateOwner(uid, function (err, owner) {
      if (err) {
        callback(err)
        return
      }

      // Create a new session if one does not already exist.
      const owner_id = owner._id
      const ownerUID = owner.ownerUID
      const finCustomerID = owner.finCustomer.finID
      self.finData.findOrCreateSession(owner_id, ownerUID, PROVIDER, finInstitutionID, finCustomerID, uid, userID, function (err, result) {
        if (err) {
          callback(err)
          return
        }

        // Only allow one open session to the same institution for an owner.
        // if (result.existingSession) {
        //   callback(errors.api.EXISTING_SESSION)
        //   return
        // }

        // Get the URI of the redirect page and return the response.
        const reply = {
          sessionID: result.session_id,
          ownerID: owner.ownerUID
        }
        callback(null, reply)
      })
    })
  }

  /**
   * This function gets the details for the given institution. The details
   * consist of an institution record and a login form.
   *
   * @param  {String}   integration      - the integration that supports the institution
   *                                       ID, currently this is 'finicity'.
   * @param  {Integer}  finInstitutionID - the institution ID
   * @param  {Function} mainCallback     - the callback function
   * @return {String}                    - the HTML markup for the login page is returned
   *                                       through the callback
   */
  getInstitutionDetails (integration, finInstitutionID, mainCallback) {
    let self = this
    async.waterfall([
      function (callback) {
        self.getFinAppToken(function (err, appToken) {
          callback(err, appToken)
        })
      },
      function (appToken, callback) {
        self.finAPI.getInstitutionDetails(appToken, finInstitutionID, function (err, result) {
          callback(err, result)
        })
      }
    ], function (err, result) {
      mainCallback(err, result)
    })
  }

  /**
   * This function gets the login form for the given institution.
   *
   * @param  {String}   uid              - the user ID from the application using
   *                                       this integration service
   * @param  {String}   provider         - the provider that supports the institution
   *                                       ID, currently this is 'finicity'.
   * @param  {Integer}  finInstitutionID - the institution ID
   * @param  {Function} callback         - the callback function
   * @return {String}                    - the HTML markup for the login page is returned
   *                                       through the callback
   */
  getInstitutionLoginForm (uid, provider, finInstitutionID, callback) {
    let self = this
    // Get the Finicity app token required for Finicity API calls.
    self.getFinAppToken(function (err, appToken) {
      if (err) {
        callback(err)
        return
      }

      // Get the institution details.
      self.finAPI.getInstitutionLoginForm(appToken, finInstitutionID, function (err, result) {
        if (err) {
          callback(err)
        } else {
          callback(null, result.result)
        }
      })
    })
  }

  // getAllTransactions = function(theOwner, fromDate, toDate, mainCallback) {
  //
  //   // Validate the customer ID.
  //   const finCustomerID = theOwner.finCustomer.finID;
  //   if (finCustomerID === null) {
  //     mainCallback({
  //       code: errors.api.MISSING_CUSTOMERID.code,
  //       msg:  errors.api.MISSING_CUSTOMERID.msg,
  //     });
  //     return;
  //   }
  //
  //   // Validate the institution.
  //   const institutions = theOwner.customerData.institutions;
  //   if (   institutions === null
  //     || typeof institutions === 'undefined'
  //     || institutions.length === 0
  //   ) {
  //     mainCallback({
  //       code: errors.api.NO_INSTITUTIONS.code,
  //       msg:  errors.api.NO_INSTITUTIONS.msg,
  //     });
  //     return;
  //   }
  //
  //   // Get transactions for each account in each institution.
  //   async.map(institutions, function(anInstitution, callback) {
  //     const accounts      = anInstitution.accounts;
  //     const finInstitutionID = anInstitution.finID;
  //
  //     self.getAllAccountTransactions(uid, finCustomerID, finInstitutionID, accounts, fromDate, toDate, function(err, updtdAccounts) {
  //       if (err) {
  //         if (err.code == errors.api.NO_ACCOUNTS.code) {
  //           callback(null, {code: 0});
  //           return;
  //         }
  //       }
  //
  //       this.finData.updateAccounts(uid, finInstitutionID, updtdAccounts, function(err) {
  //         if (err) {
  //           error('Error saving updated accounts to database.');
  //           callback({code: 1, msg: self.getError(err)});
  //         } else {
  //           callback(null, {code: 0});
  //         }
  //       });
  //     });
  //   }, function(err) {
  //     mainCallback(err);
  //   });
  // };

  addAllCustomerAccounts (finCustomerID, finInstitutionID, mfa, authInfo, mainCallback) {
    let self = this
    async.waterfall([
      function (callback) {
        // Get the Finicity app token required for Finicity API calls.
        self.getFinAppToken(function (err, appToken) {
          callback(err, appToken)
        })
      },
      function (appToken, callback) {
        // For the customer and institution, add all the user's accounts.
        self.finAPI.addAllAccounts(appToken, finCustomerID, finInstitutionID, mfa, authInfo, function (err, result) {
          callback(err, result)
        })
      }
    ], function (err, result) {
      if (err) {
        mainCallback(err)
      } else {
        mainCallback(null, result.result.accounts)
      }
    })
  }

  getCustomerAccounts (finCustomerID, mainCallback) {
    let self = this
    async.waterfall([
      function (callback) {
        // Get the Finicity app token required for Finicity API calls.
        self.getFinAppToken(function (err, appToken) {
          callback(err, appToken)
        })
      },
      function (appToken, callback) {
        // Get the accounts of the given customer.
        self.finAPI.getCustomerAccounts(appToken, finCustomerID, function (err, result) {
          callback(err, result)
        })
      }
    ], function (err, result) {
      mainCallback(err, result)
    })
  }

  getCustomerAccountsInstitution (finCustomerID, finInstitutionID, mainCallback) {
    let self = this
    async.waterfall([
      function (callback) {
        // Get the Finicity app token required for Finicity API calls.
        self.getFinAppToken(function (err, appToken) {
          callback(err, appToken)
        })
      },
      function (appToken, callback) {
        // Get the accounts of the given customer.
        self.finAPI.getCustomerAccountsInstitution(appToken, finCustomerID, finInstitutionID, function (err, result) {
          callback(err, result)
        })
      }
    ], function (err, result) {
      mainCallback(err, result)
    })
  }

  deleteCustomerAccount (finCustomerID, finAccountID, mainCallback) {
    let self = this
    async.waterfall([
      function (callback) {
        // Get the Finicity app token required for Finicity API calls.
        self.getFinAppToken(function (err, appToken) {
          callback(err, appToken)
        })
      },
      function (appToken, callback) {
        // Delete the account of the given customer.
        self.finAPI.deleteCustomerAccount(appToken, finCustomerID, finAccountID, function (err, result) {
          callback(err, result)
        })
      }
    ], function (err, result) {
      mainCallback(err, result)
    })
  }

  activateCustomerAccounts (finCustomerID, finInstitutionID, acctSelected, callback) {
    // Determine if there are accounts that need to be activated.
    const toBeActivated = []
    const alreadyActivated = []
    let found = false
    for (let i = 0; i < acctSelected.length; i++) {
      if (acctSelected[i].status === ACCOUNT_STATUS_PENDING &&
          acctSelected[i].type === ACCOUNT_TYPE_UNKNOWN
      ) {
        // Create a new array of account objects to be activated.
        const anAccount = {
          id: acctSelected[i].id,
          number: acctSelected[i].number,
          name: acctSelected[i].name,
          balance: acctSelected[i].balance,
          type: acctSelected[i].answer,
          status: acctSelected[i].status
        }
        toBeActivated.push(anAccount)
        found = true
      } else {
        alreadyActivated.push(acctSelected[i])
      }
    }
    if (!found) {
      warn('no accounts to activate')
      callback(null, acctSelected)
      return
    }

    // Get the Finicity app token required for Finicity API calls.
    self.getFinAppToken(function (err, appToken) {
      if (err) {
        callback(err)
        return
      }

      self.finAPI.activateCustomerAccounts(appToken, finCustomerID, finInstitutionID, toBeActivated, function (err, result) {
        if (err) {
          callback(err)
        } else {
          callback(null, alreadyActivated, result.result.accounts)
        }
      })
    })
  }

  loadHistoricTransactions (finCustomerID, accounts, mfa, callback) {
    let self = this
    // Get the Finicity app token required for Finicity API calls.
    self.getFinAppToken(function (err, appToken) {
      if (err) {
        callback(err, accounts)
        return
      }

      // Load historic transactions for each account.
      async.mapSeries(accounts, function (anAccount, callback) {
        /*
         * If an account is not selected or has already been loaded,
         * then do not load the historical transactions again.
         */
        if (!anAccount.selected || anAccount.transactions.loaded) {
          callback(null, anAccount)
        } else {
          let useMfa = null
          if (anAccount.transactions.loadFailCode === errors.finicity.FAIL_MFAREQUIRED.code) {
            useMfa = mfa
          }
          self.finAPI.loadHistoricTransactions(appToken, finCustomerID, anAccount.finID, useMfa, function (err, result) {
            anAccount.transactions.dateLoaded = moment().unix()
            if (err) {
              if (err === 123) {
                err = {
                  code: 'fin-019',
                  msg: 'Error, missing of invalid MFA answer.'
                }
              }
              anAccount.transactions.loaded = false
              anAccount.transactions.loadFailCode = err.code
              err.account = anAccount
              callback(err, anAccount)
            } else {
              anAccount.transactions.loaded = true
              anAccount.transactions.loadFailCode = null
              callback(null, anAccount)
            }
          })
        }
      }, function (err, result) {
        callback(err, result)
      })
    })
  }

  /**
   * This function processes an array of transactions received from Finicity and
   * saves them in the database. Before saving a transaction, the current balance
   * is calculated and saved with the transaction.
   *
   * @param  {ObjectId} account_id      - the Mongo objectId of the account against
   *                                      which the transactions are to be saved
   * @param  {Array}    theTransactions - an array of transactions objects from Finicity
   * @param  {Number}   balance         - the current account balance
   * @param  {Function} theCallback     - the callback function
   * @return {}                         - nothing
   */
  saveTransactions (account_id, theTransactions, balance, theCallback) {
    let self = this
    let newBalance = balance
    async.eachSeries(theTransactions, function (transaction, callback) {
      const aTransaction = self.dbData.formatTransaction(PROVIDER, account_id, transaction, newBalance)
      newBalance = Math.round((newBalance - transaction.amount) * 100) / 100
      self.dbData.findOrCreateDataTransaction(aTransaction, function (err, id) {
        callback(err)
      })
    }, function (err) {
      theCallback(err, newBalance)
    })
  }

  /**
   * This function gets transactions from Finicity and then saves them to the
   * database. It may not be possible to get all the transactions from Finicity
   * at once, so it is necessary to page through.
   *
   * @param  {ObjectId} account_id    - the Mongo objectId of the account against
   *                                    which the transactions are to be saved
   * @param  {String}   finCustomerID - the Finicity customerID
   * @param  {String}   finAccountID  - the Finicity accountID
   * @param  {Number}   fromDate      - the starting timestamp of the date range
   * @param  {Number}   toDate        - the ending timestamp of the date range
   * @param  {Integer}  start         - the starting index of the transactions to pull
   * @param  {Number}   endDate       - the timestamp of the last transaction (used to pass it through recursively).
   *                                    The endDate is passed because it is the first date found.
   * @param  {Number}   balance       - the current account balance
   * @param  {Function} callback      - the callback function
   * @return {}                       - returned through the callback
   */
  getAndSaveTransactions (account_id, finCustomerID, finAccountID, fromDate, toDate, start, endDate, balance, callback) {
    let self = this
    self.getFinAppToken(function (err, appToken) {
      if (err) {
        callback(err)
        return
      }
      // Get the transactions from Finicity for the customers accounts.
      self.finAPI.getCustomerAccountTransactions(appToken, finCustomerID, finAccountID, fromDate, toDate, start, function (err, transactions) {
        if (err) {
          callback({error: err, type: 'get', finAccountID: finAccountID})
          return
        }

        // Check if any transactions were received.
        const result = transactions.result
        if (result.transactions.length === 0) {
          callback(null, {startDate: fromDate, endDate: toDate})
          return
        }

        /*
         * Get the date of the first and last transactions. The transactions
         * are sorted in descending order, so the newest transactions are
         * received first.
         */
        if (start === 1) {
          endDate = result.transactions[0].postedDate
        }
        let startDate = null
        if (result.moreAvailable === 'false') {
          const index = result.transactions.length - 1
          startDate = result.transactions[index].postedDate
        }

        // Save these transactions to the DB.
        self.saveTransactions(account_id, result.transactions, balance, function (err, newBalance) {
          if (err) {
            callback({error: err, type: 'save', finAccountID: finAccountID})
            return
          }
          // If more transactions are available get them.
          if (result.moreAvailable === 'true') {
            // Adjust the starting point (index) and get the next set of transactions.
            const displaying = result.displaying
            const newStart = displaying + start
            self.getAndSaveTransactions(account_id, finCustomerID, finAccountID, fromDate, toDate, newStart, endDate, newBalance, callback)
          } else {
            callback(null, {startDate: startDate, endDate: endDate})
          }
        })
      })
    })
  }

  /**
   * This function starts the process of pulling transactions from Finicity and
   * saving them to the database.
   *
   * @param  {ObjectId} account_id    - the Mongo objectId of the account against
   *                                    which the transactions are to be saved
   * @param  {String}   finCustomerID - the Finicity customerID
   * @param  {String}   finAccountID  - the Finicity accountID
   * @param  {Number}   fromDate      - the starting timestamp of the date range
   * @param  {Number}   toDate        - the ending timestamp of the date range
   * @param  {Number}   balance       - the current account balance
   * @param  {Function} mainCallback  - the callback function
   * @return {}                       - returned through the callback
   */
  getSaveAccountTransactions (account_id, finCustomerID, finAccountID, fromDate, toDate, balance, mainCallback) {
    let self = this
    async.waterfall([
      // Get the Finicity app token required for Finicity API calls.
      function (callback) {
        self.getFinAppToken(function (err, appToken) {
          callback(err, appToken)
        })
      },
      function (appToken, callback) {
        let endDate
        self.getAndSaveTransactions(account_id, finCustomerID, finAccountID, fromDate, toDate, 1, endDate, balance, function (err, dates) {
          callback(err, dates)
        })
      }
    ], function (err, dates) {
      mainCallback(err, dates)
    })
  }

  /**
   * This function fomulates the from and to dates that will be used when
   * pulling transactions from Finicity for a given account.
   *
   * 1. If transactions have not been pulled.
   *  fromDate - take the date when transactons were loaded and subtract 3 years
   *             and make the time the start of the day. When historical
   *             transactions are loaded, Finicity attemps to get between 12
   *             to 24 months worth. So subtracting 3 years is just using a
   *             number to get beyond the 12 to 24 months. So yes, the fromDate
   *             given on the fuction call is ignored.
   *  toDate   - take the toDate given on the function call and make the time
   *             the end of the day.
   *
   * 2. The toDate is <= the toDate of transactions already pulled.
   *   - In this situation the transactions have already been pulled, so it
   *     should not be necessary to pull them again.
   *
   * 3. The todate is > the todate of transactions already pulled.
   *  fromDate - take the toDate of the last transactions pulled and add 1
   *             second. So start right after the most recent pull.
   *  toDate   - take the toDate given on the function call and make the time
   *             the end of the day.
   *
   * @param  {Object} theAccount - the account object
   * @param  {Number} fromDate   - the starting date
   * @param  {Number} toDate     - the ending date
   * @return {Object}            -
   */
  determinePullDates (theAccount, fromDate, toDate) {
    let theFromDate, theToDate
    const transactionRecord = theAccount.transactions

    // Make the to date the end of the day.
    theToDate = moment(toDate * 1000).endOf('day')
    theToDate = theToDate.unix()

    if (transactionRecord.getFromDate === null && transactionRecord.getToDate === null) {
      // The first time transactions are being saved.
      warn('First time transactions being pulled.')
      theFromDate = moment(transactionRecord.dateLoaded * 1000).subtract(3, 'years').startOf('day')
      theFromDate = theFromDate.unix()
    } else if (toDate <= transactionRecord.getToDate) {
      // Transactions have already been pulled for the date range.
      warn('Transactions already pulled for the date range.')
      return {code: 1}
    } else {
      // Pull transactions for a subsequent date range.
      warn('Pulling transactions for a subsequent date range.')
      theFromDate = moment(transactionRecord.getToDate * 1000).add(1, 'seconds')
      theFromDate = theFromDate.unix()
    }

    return {code: 0, result: {fromDate: theFromDate, toDate: theToDate}}
  }

  getAllAccountTransactions (uid, finCustomerID, finInstitutionID, accounts, fromDate, toDate, callback) {
    let self = this
    // Must have accounts to proceed.
    if (accounts === null ||
        typeof accounts === 'undefined' ||
        accounts.length === 0
    ) {
      callback({
        code: errors.api.NO_ACCOUNTS.code,
        msg: errors.api.NO_ACCOUNTS.msg
      })
      return
    }

    // For each account that is selected and loaded, get and save the transactions.
    async.map(accounts, function (anAccount, callback) {
      // If the account has not been selected, then move to the next one.
      if (!anAccount.selected) {
        callback(null, anAccount)
        return
      }
      // If historical transactions have not been loaded, then move to the next one.
      if (!anAccount.transactions.loaded) {
        error('An account is selected but transactions have not been loaded.')
        callback(null, anAccount)
        // callback(
        //     {
        //         code: errors.db.ACCOUNT_NOTLOADED.code,
        //         msg:  errors.db.ACCOUNT_NOTLOADED.msg
        //     },
        //     anAccount);
        return
      }

      // Determine the date range to use when getting transactions.
      const theDates = determinePullDates(anAccount, fromDate, toDate)
      if (theDates.code !== 0) {
        // Transactions have already been pulled for the range.
        callback(null, anAccount)
        return
      }

      // Now get the transactions.
      const transactions = anAccount.transactions
      const finAccountID = anAccount.finID
      const theFromDate = theDates.result.fromDate
      const theToDate = theDates.result.toDate
      let startDate
      self.getAndSaveTransactions(finCustomerID, finAccountID, theFromDate, theToDate, 1, startDate, function (err, dates) {
        if (err) {
          transactions.obtained = false
          transactions.obtainFailCode = err.error.code
        } else {
          transactions.obtained = true
          transactions.obtainFailCode = null
          transactions.getFromDate = theFromDate
          transactions.getToDate = theToDate
          if (!transactions.startDate) {
            transactions.startDate = dates.startDate
          }
          transactions.endDate = dates.endDate
        }
        transactions.dateObtained = moment().unix()
        callback(null, anAccount)
      })
    }, function (err, result) {
      callback(err, result)
    })
  }

  /**
   * This function gets owner information for a specific ownerUID.
   *
   * @param  {String}   ownerUID - the UID of the owner
   * @param  {Function} callback - the callback function
   * @return {}                  - returned through the callback
   */
  getOwnerInfo (ownerUID, callback) {
    let self = this
    // Get the specified owner.
    self.finData.findOwner(null, ownerUID, function (err, theOwner) {
      if (err) {
        callback(err)
      } else if (!theOwner) {
        callback(err, theOwner)
      } else {
        // Get the Consumers that are associated with the owner.
        self.finData.findConsumers(theOwner._id, function (err, theConsumers) {
          callback(err, theOwner, theConsumers)
        })
      }
    })
  }

  /**
   * This function gets consumer information for a specific consumerUID.
   *
   * @param  {String}   consumerUID - the UID of the consumer
   * @param  {Function} callback    - the callback function
   * @return {}                     - returned through the callback
   */
  getConsumerInfo (consumerUID, callback) {
    let self = this
    // Get the specified owner.
    self.finData.findConsumerByUID(consumerUID, function (err, theConsumer) {
      callback(err, theConsumer)
    })
  }

  /**
   * This function will get transactions for an account.
   *
   * @param  {String}   accountID - the objectId of the account
   * @param  {Number}   startDate - the start date in Unix Epoch time (seconds)
   * @param  {Number}   endDate   - the end date in Unix Epoch time (seconds)
   * @param  {Integer}  index     - the starting position
   * @param  {Integer}  limit     - the maximum number to get
   * @param  {Function} callback  - the callback function
   * @return {}                   - returned through the callback
   */
  getAccountDataTransactions (accountID, startDate, endDate, index, limit, callback) {
    this.dbData.findDataAccountTransactions(accountID, startDate, endDate, index, limit, function (err, count, transactions) {
      callback(err, count, transactions)
    })
  }

  /**
   * This function will get a single transaction.
   *
   * @param  {String}   transactionID - the objectID of the transaction
   * @param  {Function} callback      - the callback function
   * @return {}                       - returned through the callback
   */
  getDataTransaction (transactionID, callback) {
    this.dbData.findTransaction(transactionID, function (err, transaction) {
      callback(err, transaction)
    })
  }
}
