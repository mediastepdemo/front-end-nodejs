const logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = logger.init(__filename)
const __ = require(rootDirectory + '/plugins/utilities/_lodash')
var request = require('request')
var errors = require('./errorCodes')

module.exports = function (config) {
  var formatMoreInfo = function (error, response, body) {
    if (error) {
      return error
    }

    var statusCode, info
    if (typeof response !== 'undefined') {
      statusCode = response.statusCode
    }
    if (typeof body === 'undefined') {
      info = {'statusCode': statusCode}
    } else {
      var data = {'code': body.code, 'msg': body.message, 'body': body}
      info = {'statusCode': statusCode, 'details': data}
    }
    return info
  }

  this.partnerAuth = function (callback) {
    var options = {
      url: config.base_url + 'v2/partners/authentication',
      headers: {
        'Finicity-App-Key': config.app_key,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      json: {
        'partnerId': config.partner_id,
        'partnerSecret': config.partner_secret
      }
    }

    request.post(options, function (error, response, body) {
      if (error || response.statusCode !== 200) {
        callback({
          code: errors.finicity.FAIL_PARTNERAUTH.code,
          msg: errors.finicity.FAIL_PARTNERAUTH.msg,
          moreInfo: formatMoreInfo(error, response, body)
        })
      } else {
        callback(null, {
          code: errors.finicity.OK.code,
          msg: errors.finicity.OK.msg,
          result: body.token
        })
      }
    })
  }

  // Gets a list of institution records.
  this.getInstitutions = function (accessToken, searchString, callback) {
    // The seach string must be URLencoded.
    // Can use: querystring.stringify
    // encodeURIComponent
    var searchFor
    if (typeof searchString === 'undefined' || searchString === '') {
      searchFor = '*'
    } else {
      searchFor = searchString
    }
    // var encodedStr = encodeURIComponent(searchString);

    var options = {
      url: config.base_url + 'v1/institutions',
      headers: {
        'Finicity-App-Key': config.app_key,
        'Finicity-App-Token': accessToken,
        'Accept': 'application/json'
      },
      qs: {
        'search': searchFor
      }
    }

    request.get(options, function (error, response, body) {
      if (error || response.statusCode !== 200) {
        callback({
          code: errors.finicity.FAIL_INSTITUTIONS.code,
          msg: errors.finicity.FAIL_INSTITUTIONS.msg,
          search: searchString,
          moreInfo: formatMoreInfo(error, response, body)
        })
      } else {
        var jsonData = JSON.parse(body)
        callback(null, {
          code: errors.finicity.OK.code,
          msg: errors.finicity.OK.msg,
          search: searchString,
          result: jsonData
        })
      }
    })
  }

  // Gets a single institution record.
  this.getInstitution = function (accessToken, finInstitutionID, callback) {
    var options = {
      url: config.base_url + 'v1/institutions/' + finInstitutionID,
      headers: {
        'Finicity-App-Key': config.app_key,
        'Finicity-App-Token': accessToken,
        'Accept': 'application/json'
      }
    }

    request.get(options, function (error, response, body) {
      if (error || response.statusCode !== 200) {
        callback({
          code: errors.finicity.FAIL_INSTITUTION.code,
          msg: errors.finicity.FAIL_INSTITUTION.msg,
          finInstitutionID: finInstitutionID,
          moreInfo: formatMoreInfo(error, response, body)
        })
      } else {
        var jsonData = JSON.parse(body)
        callback(null, {
          code: errors.finicity.OK.code,
          msg: errors.finicity.OK.msg,
          finInstitutionID: finInstitutionID,
          result: jsonData
        })
      }
    })
  }

  // Gets one institution record and one login form.
  this.getInstitutionDetails = function (accessToken, finInstitutionID, callback) {
    var options = {
      url: config.base_url + 'v1/institutions/' + finInstitutionID + '/details',
      headers: {
        'Finicity-App-Key': config.app_key,
        'Finicity-App-Token': accessToken,
        'Accept': 'application/json'
      }
    }

    request.get(options, function (error, response, body) {
      if (error || response.statusCode !== 200) {
        callback({
          code: errors.finicity.FAIL_DETAILS.code,
          msg: errors.finicity.FAIL_DETAILS.msg,
          finInstitutionID: finInstitutionID,
          moreInfo: formatMoreInfo(error, response, body)
        })
      } else {
        var jsonData = JSON.parse(body)
        callback(null, {
          code: errors.finicity.OK.code,
          msg: errors.finicity.OK.msg,
          finInstitutionID: finInstitutionID,
          result: jsonData
        })
      }
    })
  }

  // Gets one login form.
  this.getInstitutionLoginForm = function (accessToken, finInstitutionID, callback) {
    var options = {
      url: config.base_url + 'v1/institutions/' + finInstitutionID + '/loginForm',
      headers: {
        'Finicity-App-Key': config.app_key,
        'Finicity-App-Token': accessToken,
        'Accept': 'application/json'
      }
    }

    request.get(options, function (error, response, body) {
      if (error || response.statusCode !== 200) {
        callback({
          code: errors.finicity.FAIL_LOGINFORM.code,
          msg: errors.finicity.FAIL_LOGINFORM.msg,
          finInstitutionID: finInstitutionID,
          moreInfo: formatMoreInfo(error, response, body)
        })
      } else {
        var jsonData = JSON.parse(body)
        callback(null, {
          code: errors.finicity.OK.code,
          msg: errors.finicity.OK.msg,
          finInstitutionID: finInstitutionID,
          result: jsonData
        })
      }
    })
  }

  this.addCustomer = function (accessToken, customerName, callback) {
    // TODO This must change for production.
    var options = {
      url: config.base_url + 'v1/customers/testing',
      headers: {
        'Finicity-App-Key': config.app_key,
        'Finicity-App-Token': accessToken,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      json: {
        'username': customerName
      }
    }

    request.post(options, function (error, response, body) {
      if (error || response.statusCode !== 201) {
        callback({
          code: errors.finicity.FAIL_ADDCUSTOMER.code,
          msg: errors.finicity.FAIL_ADDCUSTOMER.msg,
          customerName: customerName,
          moreInfo: formatMoreInfo(error, response, body)
        })
      } else {
        callback(null, {
          code: errors.finicity.OK.code,
          msg: errors.finicity.OK.msg,
          customerName: customerName,
          result: body
        })
      }
    })
  }

  this.getCustomer = function (accessToken, finCustomerID, callback) {
    var options = {
      url: config.base_url + 'v1/customers/' + finCustomerID,
      headers: {
        'Finicity-App-Key': config.app_key,
        'Finicity-App-Token': accessToken,
        'Accept': 'application/json'
      }
    }

    request.get(options, function (error, response, body) {
      if (error || response.statusCode !== 200) {
        callback({
          code: errors.finicity.FAIL_GETCUSTOMER.code,
          msg: errors.finicity.FAIL_GETCUSTOMER.msg,
          finCustomerID: finCustomerID,
          moreInfo: formatMoreInfo(error, response, body)
        })
      } else {
        callback(null, {
          code: errors.finicity.OK.code,
          msg: errors.finicity.OK.msg,
          finCustomerID: finCustomerID,
          result: body
        })
      }
    })
  }

  this.deleteCustomer = function (accessToken, finCustomerID, callback) {
    var options = {
      url: config.base_url + 'v1/customers/' + finCustomerID,
      headers: {
        'Finicity-App-Key': config.app_key,
        'Finicity-App-Token': accessToken,
        'Accept': 'application/json'
      }
    }

    request.delete(options, function (error, response, body) {
      if (error || response.statusCode !== 204) {
        callback({
          code: errors.finicity.FAIL_DELETECUSTOMER.code,
          msg: errors.finicity.FAIL_DELETECUSTOMER.msg,
          finCustomerID: finCustomerID,
          moreInfo: formatMoreInfo(error, response, body)
        })
      } else {
        callback(null, {
          code: errors.finicity.OK.code,
          msg: errors.finicity.OK.msg,
          finCustomerID: finCustomerID,
          result: body
        })
      }
    })
  }

  this.getCustomerAccounts = function (accessToken, finCustomerID, callback) {
    var options = {
      url: config.base_url + 'v1/customers/' + finCustomerID + '/accounts',
      headers: {
        'Finicity-App-Key': config.app_key,
        'Finicity-App-Token': accessToken,
        'Accept': 'application/json'
      }
    }

    request.get(options, function (error, response, body) {
      if (error || response.statusCode !== 200) {
        callback({
          code: errors.finicity.FAIL_CUSTOMERACCT.code,
          msg: errors.finicity.FAIL_CUSTOMERACCT.msg,
          finCustomerID: finCustomerID,
          moreInfo: formatMoreInfo(error, response, body)
        })
      } else {
        var jsonData = JSON.parse(body)
        callback(null, {
          code: errors.finicity.OK.code,
          msg: errors.finicity.OK.msg,
          finCustomerID: finCustomerID,
          result: jsonData
        })
      }
    })
  }

  this.getCustomerAccountsInstitution = function (accessToken, finCustomerID, finInstitutionID, callback) {
    var options = {
      url: config.base_url + 'v1/customers/' + finCustomerID +
        '/institutions/' + finInstitutionID + '/accounts',
      headers: {
        'Finicity-App-Key': config.app_key,
        'Finicity-App-Token': accessToken,
        'Accept': 'application/json'
      }
    }

    request.get(options, function (error, response, body) {
      if (error || response.statusCode !== 200) {
        callback({
          code: errors.finicity.FAIL_CUSTOMERACCTINST.code,
          msg: errors.finicity.FAIL_CUSTOMERACCTINST.msg,
          finCustomerID: finCustomerID,
          moreInfo: formatMoreInfo(error, response, body)
        })
      } else {
        var jsonData = JSON.parse(body)
        callback(null, {
          code: errors.finicity.OK.code,
          msg: errors.finicity.OK.msg,
          finCustomerID: finCustomerID,
          result: jsonData
        })
      }
    })
  }

  this.deleteCustomerAccount = function (accessToken, finCustomerID, finAccountID, callback) {
    var options = {
      url: config.base_url + 'v1' +
        '/customers/' + finCustomerID +
        '/accounts/' + finAccountID,
      headers: {
        'Finicity-App-Key': config.app_key,
        'Finicity-App-Token': accessToken
      }
    }

    request.delete(options, function (error, response, body) {
      if (error || response.statusCode !== 204) {
        callback({
          code: errors.finicity.FAIL_DELETEACCOUNT.code,
          msg: errors.finicity.FAIL_DELETEACCOUNT.msg,
          finCustomerID: finCustomerID,
          finAccountID: finAccountID,
          moreInfo: formatMoreInfo(error, response, body)
        })
      } else {
        callback(null, {
          code: errors.finicity.OK.code,
          msg: errors.finicity.OK.msg,
          finCustomerID: finCustomerID,
          finAccountID: finAccountID
        })
      }
    })
  }

  var handleCode203 = function (response, body, errInfo, reply) {
    reply.code = errors.finicity.FAIL_MFAREQUIRED.code
    reply.msg = errors.finicity.FAIL_MFAREQUIRED.msg
    reply.mfaSession = response.headers['mfa-session']
    reply.mfaChallenge = body
  }

  var handleCode400 = function (response, body, errInfo, reply) {
    switch (body.code) {
      case 10005:
        reply.code = errors.finicity.FAIL_CREDENTIALS.code
        reply.msg = errors.finicity.FAIL_CREDENTIALS.msg
        break

      default:
        reply.code = errInfo.code
        reply.msg = errInfo.msg
    }
  }

  var handleCode408 = function (response, body, errInfo, reply) {
    switch (body.code) {
      case 331:
        reply.code = errors.finicity.FAIL_MFATIMEOUT.code
        reply.msg = errors.finicity.FAIL_MFATIMEOUT.msg
        break

      default:
        reply.code = errInfo.code
        reply.msg = errInfo.msg
    }
  }

  var handleCode500 = function (response, body, errInfo, reply) {
    switch (body.code) {
      case 103:
        reply.code = errors.finicity.FAIL_CREDENTIALS.code
        reply.msg = errors.finicity.FAIL_CREDENTIALS.msg
        break

      case 185:
      case 187:
        reply.code = errors.finicity.FAIL_MFA.code
        reply.msg = errors.finicity.FAIL_MFA.msg
        break

      default:
        reply.code = errInfo.code
        reply.msg = errInfo.msg
    }
  }

  var checkStatusCode = function (response, body, errInfo, reply) {
    if (typeof body === 'undefined') {
      reply.code = errInfo.code
      reply.msg = errInfo.msg
      return
    }

    switch (response.statusCode) {
      case 203:
        handleCode203(response, body, errInfo, reply)
        break

      case 400:
        handleCode400(response, body, errInfo, reply)
        break

      case 408:
        handleCode408(response, body, errInfo, reply)
        break

      case 500:
        handleCode500(response, body, errInfo, reply)
        break

      default:
        reply.code = errInfo.code
        reply.msg = errInfo.msg
    }
  }

  this.addAllAccounts = function (accessToken, finCustomerID, institutionId, mfa, bodyData, callback) {
    var url = config.base_url + 'v1' +
      '/customers/' + finCustomerID +
      '/institutions/' + institutionId +
      '/accounts/addall'
    var headers = {
      'Finicity-App-Key': config.app_key,
      'Finicity-App-Token': accessToken,
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }
    var jsonData = {}

    if (!mfa) {
      var credentials = []
      var number = bodyData.length
      for (var i = 0; i < number; i++) {
        credentials[i] = {}
        credentials[i].id = bodyData[i].id
        credentials[i].name = bodyData[i].name
        credentials[i].value = bodyData[i].value
      }
      jsonData = {'credentials': credentials}
    } else {
      url += '/mfa'
      headers['MFA-Session'] = bodyData.mfaSession
      jsonData = {'mfaChallenges': bodyData.mfaChallenge}
    }

    var options = {
      url: url,
      headers: headers,
      json: jsonData
    }
    request.post(options, function (error, response, body) {
      var reply = {
        finCustomerID: finCustomerID,
        finInstitutionID: institutionId
      }

      if (error) {
        reply.code = errors.finicity.FAIL_ADDALLACCOUNTS.code
        reply.msg = errors.finicity.FAIL_ADDALLACCOUNTS.msg
        reply.moreInfo = error
        callback(reply)
      } else if (response.statusCode !== 200) {
        checkStatusCode(response, body, errors.finicity.FAIL_ADDALLACCOUNTS, reply)
        reply.moreInfo = formatMoreInfo(error, response, body)
        callback(reply)
      } else {
        reply.code = errors.finicity.OK.code
        reply.msg = errors.finicity.OK.msg
        reply.result = body
        callback(null, reply)
      }
    })
  }

  /*
   * accounts is:
   * 'accounts': [
   *      id:
   *      number:
   *      name:
   *      balance:
   *      type:
   *      status:
   * ]
   */
  this.activateCustomerAccounts = function (accessToken, finCustomerID, finInstitutionID, accounts, callback) {
    var options = {
      url: config.base_url + 'v2' +
        '/customers/' + finCustomerID +
        '/institutions/' + finInstitutionID +
        '/accounts',
      headers: {
        'Finicity-App-Key': config.app_key,
        'Finicity-App-Token': accessToken,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      json: {'accounts': accounts}
    }

    request.put(options, function (error, response, body) {
      var reply = {
        finCustomerID: finCustomerID,
        finInstitutionID: finInstitutionID
      }

      if (error || response.statusCode !== 200) {
        reply.code = errors.finicity.FAIL_ACTIVATEACCOUNTS.code
        reply.msg = errors.finicity.FAIL_ACTIVATEACCOUNTS.msg
        reply.moreInfo = formatMoreInfo(error, response, body)
        callback(reply)
      } else {
        reply.code = errors.finicity.OK.code
        reply.msg = errors.finicity.OK.msg
        reply.result = body
        callback(null, reply)
      }
    })
  }

  this.loadHistoricTransactions = function (accessToken, finCustomerID, finAccountID, mfa, callback) {
    var url = config.base_url + 'v1' +
      '/customers/' + finCustomerID +
      '/accounts/' + finAccountID +
      '/transactions/historic'
    var headers = {
      'Finicity-App-Key': config.app_key,
      'Finicity-App-Token': accessToken,
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }

    var options = {}
    if (!mfa) {
      headers['Content-Length'] = 0
    } else {
      url += '/mfa'
      headers['MFA-Session'] = mfa.mfaSession
      options.json = mfa.mfaChallenge
    }
    options.url = url
    options.headers = headers

    request.post(options, function (error, response, body) {
      const reply = {
        finCustomerID: finCustomerID,
        finAccountID: finAccountID
      }

      if (error) {
        reply.code = errors.finicity.FAIL_HISTORICTRANS.code
        reply.msg = errors.finicity.FAIL_HISTORICTRANS.msg
        reply.moreInfo = error
        callback(reply)
      } else if (response.statusCode !== 204) {
        var jsonData
        try {
          jsonData = JSON.parse(body)
        } catch (e) {
          jsonData = body
        }
        checkStatusCode(response, jsonData, errors.finicity.FAIL_HISTORICTRANS, reply)
        reply.moreInfo = formatMoreInfo(error, response, jsonData)
        callback(reply)
      } else {
        reply.code = errors.finicity.OK.code
        reply.msg = errors.finicity.OK.msg
        reply.result = body
        callback(null, reply)
      }
    })
  }

  this.getCustomerAccountTransactions = function (accessToken, finCustomerID, finAccountID, fromDate, toDate, start, callback) {
    var options = {
      url: config.base_url + 'v3' +
        '/customers/' + finCustomerID +
        '/accounts/' + finAccountID +
        '/transactions',
      headers: {
        'Finicity-App-Key': config.app_key,
        'Finicity-App-Token': accessToken,
        'Accept': 'application/json'
      },
      qs: {
        'fromDate': fromDate,
        'toDate': toDate,
        'start': start,
        'sort': 'desc'
      }
    }

    request.get(options, function (error, response, body) {
      var reply = {
        finCustomerID: finCustomerID,
        finAccountID: finAccountID,
        fromDate: fromDate,
        toDate: toDate
      }
      if (error || response.statusCode !== 200) {
        reply.code = errors.finicity.FAIL_ACCOUNTTRANS.code
        reply.msg = errors.finicity.FAIL_ACCOUNTTRANS.msg
        reply.moreInfo = formatMoreInfo(error, response, body)
        callback(reply)
      } else {
        var jsonData = JSON.parse(body)
        reply.code = errors.finicity.OK.code
        reply.msg = errors.finicity.OK.msg
        reply.result = jsonData
        callback(null, reply)
      }
    })
  }
}
