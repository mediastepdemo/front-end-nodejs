const logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = logger.init(__filename)
var moment = require('moment')

// Get the internal (plugin) dependencies.
var errors = require('./errorCodes')

module.exports = function (finicity) {
  // Get finicity and load the Data models.
  var Owners = require(rootDirectory + '/finicity/models/userAdmin/owner')(finicity.db)
  var Consumers = require(rootDirectory + '/finicity/models/userAdmin/consumer')(finicity.db)
  var ConsumerAccounts = require(rootDirectory + '/finicity/models/userAdmin/consumerAccount')(finicity.db)
  var Sessions = require(rootDirectory + '/finicity/models/userAdmin/authSession')(finicity.db)
  var self = this

  /**
   * This function formats the given information.
   *
   * @param  {object} error - an error object to be formatted
   * @return {object}       - an object is reqturned in the callback
   */
  var formatMoreInfo = function (error) {
    if (error) {
      return error
    }
  }

  /**
   * This function finds an owner document in the Owners collection.
   *
   * @param  {String}   uid      - a unique ID provided by the application using
   *                               this integration service. This can have a
   *                               value of null if it is not to be used.
   * @param  {String}   ownerUID - the owner ID assinged by this integration service
   *                               to the application using this service. This
   *                               can have a value of null if it is not to be used.
   * @param  {Function} callback - the callback function
   * @return {Object}            - the owner document is returned through the callback
   */
  this.findOwner = function (uid, ownerUID, callback) {
    // Make sure the required parameters are present.
    if (!ownerUID && !uid) {
      callback({
        code: errors.db.MISSING_OWNERIDUID.code,
        msg: errors.db.MISSING_OWNERIDUID.msg
      })
      return
    }

    // Create the query.
    var query
    if (ownerUID && uid) {
      query = {
        'ownerUID': ownerUID,
        'uid': uid
      }
    } else if (ownerUID) {
      query = {'ownerUID': ownerUID}
    } else {
      query = {'uid': uid}
    }

    // Find the owner document for the given query.
    Owners.findOne(query, function (err, theOwner) {
      if (err) {
        callback({
          code: errors.db.FAIL_OWNERFIND.code,
          msg: errors.db.FAIL_OWNERFIND.msg,
          moreInfo: formatMoreInfo(err)
        })
      } else {
        // Return the owner document that was found.
        callback(null, theOwner)
      }
    })
  }

  /**
   * This function creates an Owner document in the Owners collection.
   *
   * @param  {String}   uid          - a unique ID provided by the application
   *                                   using this integration service.
   * @param  {String}   ownerUID     - the owner ID assigned by this integration
   *                                   service to the application using this service
   * @param  {Object}   customerData - information about the Finicity customer
   * @param  {Function} callback     - the callback function
   * @return
   */
  this.createOwner = function (uid, ownerUID, customerData, callback) {
    var newOwner = new Owners()
    newOwner.ownerUID = ownerUID
    newOwner.uid = uid
    newOwner.dateCreated = moment().unix()
    newOwner.finCustomer = customerData

    newOwner.save(function (err, newOwner) {
      if (err) {
        callback({
          code: errors.db.FAIL_OWNERSAVE.code,
          msg: errors.db.FAIL_OWNERSAVE.msg,
          moreInfo: formatMoreInfo(err)
        })
      } else {
        callback(null, newOwner)
      }
    })
  }

  /**
   * This function queries the Consumers collection to find a match.
   *
   * @param  {ObjectId} owner_id    - the ObjectId of the Owners document
   * @param  {String}   userID      - the userID
   * @param  {Number}   finLoginID  - the institution login ID
   * @param  {Function} callback    - the callback function
   * @return {}                     - returned through the callback
   */
  this.findConsumer = function (owner_id, userID, finLoginID, callback) {
    // Create the query.
    var query = {
      'ownerID': owner_id
    }
    if (userID) {
      query.userID = userID
    }
    if (finLoginID) {
      query.finLoginID = finLoginID
    }

    Consumers.findOne(query, function (err, theConsumer) {
      if (err) {
        callback({
          code: errors.db.FAIL_CONSUMERFIND.code,
          msg: errors.db.FAIL_CONSUMERFIND.msg,
          moreInfo: formatMoreInfo(err)
        })
      } else {
        // Return the consum;er document that was found.
        callback(null, theConsumer)
      }
    })
  }

  /**
   * This function finds a consumer documment based on the objectId.
   *
   * @param  {ObjectId} consumer_id - the ObjectId of the Consumer document
   * @param  {Function} callback    - the callback function
   * @return {}                     - returned through the callback
   */
  this.findConsumerById = function (consumer_id, callback) {
    Consumers.findById(consumer_id, function (err, consumer) {
      if (err) {
        error(err)
        callback(errors.db.FAIL_CONSUMERFINDID)
      } else {
        callback(null, consumer)
      }
    })
  }

  /**
   * This function finds a consumer documment based on the UID.
   *
   * @param  {ObjectId} consumerUID - the Consumer UID
   * @param  {Function} callback    - the callback function
   * @return {}                     - returned through the callback
   */
  this.findConsumerByUID = function (consumerUID, callback) {
    Consumers.findOne({'consumerUID': consumerUID}, function (err, consumer) {
      if (err) {
        error(err)
        callback(errors.db.FAIL_CONSUMERFINDUID)
      } else {
        callback(null, consumer)
      }
    })
  }

  /**
   * This function finds consumer documents that have a specific ownerID.
   *
   * @param  {ObjectId} owner_id - the ObjectId of the Owner
   * @param  {Function} callback - the callback function
   * @return type]}              - returned through the callback
   */
  this.findConsumers = function (owner_id, callback) {
    Consumers.find({'ownerID': owner_id}, function (err, theConsumers) {
      if (err) {
        callback({
          code: errors.db.FAIL_CONSUMERFIND.code,
          msg: errors.db.FAIL_CONSUMERFIND.msg,
          moreInfo: formatMoreInfo(err)
        })
      } else {
        // Return the consumer documents that were found.
        callback(null, theConsumers)
      }
    })
  }

  /**
   * This function creates a new consumer in the Consumers collection.
   *
   * @param  {ObjectId} owner_id       - the ObjectId of the Owners document
   * @param  {String}   userID         - the userID
   * @param  {Number}   finLoginID     - the institution login ID
   * @param  {String}   consumerUID    - the consumer UID
   * @param  {ObjectId} institution_id - the Mongo objectId of the institution
   * @param  {Function} callback       - the callback function
   * @return {}                        - returned through the callback
   */
  this.createConsumer = function (owner_id, userID, finLoginID, consumerUID, institution_id, callback) {
    var consumer = new Consumers()
    consumer.ownerID = owner_id
    consumer.institutionID = institution_id
    consumer.consumerUID = consumerUID
    consumer.userID = userID
    consumer.dateCreated = moment().unix()
    consumer.finLoginID = finLoginID

    consumer.save(function (err, newConsumer) {
      if (err) {
        callback({
          code: errors.db.FAIL_CONSUMERSAVE.code,
          msg: errors.db.FAIL_CONSUMERSAVE.msg,
          moreInfo: formatMoreInfo(err)
        })
      } else {
        callback(null, newConsumer)
      }
    })
  }

  /**
   * This function finds a session, if one is not found that a new one is
   * created.
   *
   * @param  {String}   owner_id         - the ObjectId of the owner document
   * @param  {String}   ownerUID         - the unique UID of the owner
   * @param  {String}   provider         - the provider that supports the institution
   *                                       ID, currently this is 'finicity'.
   * @param  {String}   finInstitutionID - the institution ID that has been obtained
   *                                       from the provider
   * @param  {String}   finCustomerID    - the customer ID that was assigned by Finicity
   * @param  {String}   uid              - a unique ID provided by the application using
   *                                       this integration service.
   * @param  {String}   userID           - an ID provided by the application using this
   *                                       service identifying the user of the
   *                                       integration
   * @param  {Function} callback         - the callback function
   * @return {[type]}                      [description]
   */
  this.findOrCreateSession = function (owner_id, ownerUID, provider, finInstitutionID, finCustomerID, uid, userID, callback) {
    Sessions.findOne(
      {'ownerID': owner_id, 'institution.finID': finInstitutionID},
      function (err, session) {
        if (err) {
          callback({
            code: errors.db.FAIL_FINDSESSION.code,
            msg: errors.db.FAIL_FINDSESSION.msg,
            moreInfo: formatMoreInfo(err)
          })
          return
        }

        // A session already exists for this owner to the institution.
        if (session) {
          callback(null, {existingSession: true, session_id: session._id})
          return
        }

        var newSession = new Sessions()
        newSession.ownerID = owner_id
        newSession.dateCreated = moment().unix()
        newSession.provider = provider
        newSession.uid = uid
        newSession.userID = userID
        newSession.ownerUID = ownerUID
        newSession.finCustomerID = finCustomerID
        newSession.institution.finID = finInstitutionID
        newSession.save(function (err, savedSession) {
          if (err) {
            callback({
              code: errors.db.FAIL_SAVESESSION.code,
              msg: errors.db.FAIL_SAVESESSION.msg,
              moreInfo: formatMoreInfo(err)
            })
          } else {
            callback(null, {existingSession: false, session_id: savedSession._id})
          }
        })
      }
    )
  }

  /**
   * This function finds a session document.
   *
   * @param  {ObjectId} session_id - the ObjectId of the session to finds
   * @param  {Function} callback   - the callback function
   * @return {}                    - returned through the callback
   */
  this.findSession = function (session_id, callback) {
    Sessions.findById(session_id, function (err, session) {
      if (err || !session) {
        callback({
          code: errors.db.FAIL_FINDSESSION.code,
          msg: errors.db.FAIL_FINDSESSION.msg,
          moreInfo: formatMoreInfo(err)
        })
      } else {
        callback(null, session)
      }
    })
  }

  /**
   * This functions creates an account document. These are saved with the
   * session.
   *
   * @param  {Object} anAccount - the account source as received from Finicity
   * @param  {Number} epochTime - the Unix Epoch time to use as the created date. Optional.
   * @return {Object}           - the account ready to save in the database
   */
  this.createAccount = function (anAccount, epochTime) {
    if (!epochTime) {
      epochTime = moment().unix()
    }
    var newAccount = {
      dateCreated: epochTime,
      selected: false,
      finID: anAccount.id,
      number: anAccount.number,
      name: anAccount.name,
      status: anAccount.status,
      type: anAccount.type,
      finLoginID: anAccount.institutionLoginId,
      rawSource: anAccount
    }

    return newAccount
  }

  /**
   * This function finds the first loginId within the array of accounts. the
   * LoginID is the institutionLoginId that is assigned by Finicity.
   *
   * @param  {array} accounts - the accounts to search
   * @return {Number}         - the finLoginID
   */
  var getLoginId = function (accounts) {
    var theLoginId
    for (var i = 0; i < accounts.length; i++) {
      if (accounts[i].finLoginID) {
        theLoginId = accounts[i].finLoginID
        break
      }
    }

    return theLoginId
  }

  /**
   * This function updates a session document.
   *
   * @param  {Object}   session   - the session to be updated
   * @param  {String}   newState  - the new state of the session
   * @param  {Object}   stateInfo - information to update the savedSession
   * @param  {Function} callback  - the callback function
   * @return {}                   - returned through the callback
   */
  this.updateSession = function (session, newState, stateInfo, callback) {
    // Create the database update based upon the new state.
    var update = {}
    var epochTime = moment().unix()
    switch (newState) {
      case 'LOGIN':
        update = {
          'loginForm.dateCreated': epochTime,
          'loginForm.formDetails': stateInfo.loginInfo,
          'mfa.dateCreated': null,
          'mfa.details': {},
          'institution.dateCreated': epochTime,
          'institution.name': stateInfo.institutionRecord.name,
          'institution.authorized': false,
          'institution.rawSource': stateInfo.institutionRecord,
          'accounts': []
        }
        break

      case 'LOGIN_MFA':
        update = {
          'mfa.dateCreated': epochTime,
          'mfa.details': stateInfo
        }
        break

      case 'ACCOUNT_LIST':
        var newAccounts = []
        stateInfo.map(function (anAccount) {
          var addAccount = self.createAccount(anAccount, epochTime)
          newAccounts.push(addAccount)
        })
        update = {
          'institution.authorized': true,
          'accounts': newAccounts
        }
        if (session.state === 'LOGIN_MFA') {
          update.mfa = { dateCreated: null, details: {} }
        }
        break

      case 'ACCOUNT_MFA':
        update = {
          'mfa.dateCreated': epochTime,
          'mfa.details': stateInfo.mfa,
          'accounts': stateInfo.accounts
        }
        break

      case 'LOADED':
        update = {
          'finLoginID': getLoginId(stateInfo),
          'mfa.dateCreated': null,
          'mfa.details': {},
          'accounts': stateInfo
        }
        break

      default:
        // Not a valid state.
        callback({
          code: errors.db.INVALID_STATE.code,
          msg: errors.db.INVALID_STATE.msg
        })
        return
    }
    update.state = newState
    update.previousState = session.state

    // Update the session with the new information.
    Sessions.findOneAndUpdate(
      {'_id': session._id},
      {$set: update},
      {new: true},
      function (err, theSession) {
        if (err) {
          callback({
            code: errors.db.FAIL_UPDATESESSION.code,
            msg: errors.db.FAIL_UPDATESESSION.msg,
            moreInfo: formatMoreInfo(err)
          })
        } else {
          callback(null, theSession._id)
        }
      }
    )
  }

  /**
   * This function removes a session document from the collection.
   *
   * @param  {ObjectId} session_id - the ObjectId of the session to removes
   * @param  {Function} callback   - the callback function
   * @return {}
   */
  this.removeSession = function (session_id, callback) {
    Sessions.findOneAndRemove({'_id': session_id}, function (err, session) {
      if (err) {
        callback({
          code: errors.db.FAIL_REMOVESESSION.code,
          msg: errors.db.FAIL_REMOVESESSION.msg,
          moreInfo: formatMoreInfo(err)
        })
      } else if (!session) {
        callback({
          code: errors.db.FAIL_NOSESSION.code,
          msg: errors.db.FAIL_NOSESSION.msg,
          moreInfo: formatMoreInfo(err)
        })
      } else {
        callback(null, session)
      }
    })
  }

  /**
   * This function formats a consumer account document.
   *
   * @param  {ObjectId} consumer_id    - the the Mongo objectId of the consumer to
   *                                     which this consmerAccount belongs
   * @param  {Object}   account        - the account record object
   * @param  {Number}   dateCreated    - optional, the created date to use in Unix Epoch time
   * @return {Object}                  -
   */
  this.formatConsumerAccount = function (consumer_id, account, dateCreated) {
    if (!dateCreated) {
      dateCreated = moment().unix()
    }

    var formatted = {
      consumerID: consumer_id,

      status: (account.selected) ? 'ACTIVE' : 'INACTIVE',
      dateCreated: dateCreated,

      selected: account.selected,
      dateSelected: account.dateSelected,
      finAccountID: account.finID,
      number: account.number,
      name: account.name,
      type: account.type,
      loaded: account.transactions.loaded,
      dateLoaded: account.transactions.dateLoaded,
      loadFailCode: account.transactions.loadFailCode
    }

    return formatted
  }

  /**
   * This function finds a consumer account document. If one does not already
   * exist a new one is created.
   *
   * @param  {Object}   consumerAccount - a
   * @param  {Function} callback        - the callback function
   * @return {}                         -
   */
  this.findOrCreateConsumerAccount = function (consumerAccount, callback) {
    // Only add the consumerAccount is it does not already exist.
    ConsumerAccounts.findOneAndUpdate(
      {
        'consumerID': consumerAccount.consumerID,
        'finAccountID': consumerAccount.finAccountID
      },
      {$setOnInsert: consumerAccount},
      {upsert: true, new: true},
      function (err, theConsumerAccount) {
        if (err) {
          callback({
            code: errors.db.FAIL_CONSUMERACCTSAVE.code,
            msg: errors.db.FAIL_CONSUMERACCTSAVE.msg,
            moreInfo: formatMoreInfo(err)
          })
        } else {
          callback(null, theConsumerAccount._id)
        }
      }
    )
  }

  /**
   * This function finds consumer accounts documents belonging to a given consumer
   * that are ACTIVE and were selected.
   *
   * @param  {ObjectId} consumer_id - the Mongo objectId of the consumer
   * @param  {Function} callback    - the callback function
   * @return {}                     - returned through the callback
   */
  this.findConsumerAccounts = function (consumer_id, callback) {
    ConsumerAccounts.find(
      {'consumerID': consumer_id, 'status': 'ACTIVE', 'selected': true},
      function (err, consumerAccounts) {
        if (err) {
          callback({
            code: errors.db.FAIL_FINDCONSUMERACCT.code,
            msg: errors.db.FAIL_FINDCONSUMERACCT.msg,
            moreInfo: formatMoreInfo(err)
          })
        } else {
          callback(null, consumerAccounts)
        }
      }
    )
  }

  /**
   * This function updates a consumer account document with the transaction
   * dates.
   *
   * @param  {ObjectId} consumer_id - the Mongo objectId of the consumer
   * @param  {Number}   fromDate    - the from date in Unix epoch time
   * @param  {Number}   toDate      - the to date in Unix epoch time
   * @param  {Function} callback    - the callback function
   * @return {}                     - returned through the callback
   */
  this.updateConsumerAccountDates = function (consumer_id, fromDate, toDate, callback) {
    ConsumerAccounts.findByIdAndUpdate(
      consumer_id,
      {$set: {'txnFromDate': fromDate, 'txnToDate': toDate}},
      function (err, theConsumerAccount) {
        if (err) {
          callback({
            code: errors.db.FAIL_CONSUMERACCTUPDT.code,
            msg: errors.db.FAIL_CONSUMERACCTUPDT.msg,
            moreInfo: formatMoreInfo(err)
          })
        } else {
          callback(null, theConsumerAccount._id)
        }
      }
    )
  }
}
