const logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = logger.init(__filename)
var moment = require('moment')
var mongoose = require('mongoose')
var errors = require('./errorCodes')

module.exports = function (finicity) {
  var DataInstitutions = require(rootDirectory + '/finicity/models/retrievedData/institution')(finicity.db)
  var DataAccounts = require(rootDirectory + '/finicity/models/retrievedData/account')(finicity.db)
  var DataTransactions = require(rootDirectory + '/finicity/models/retrievedData/transaction')(finicity.db)
  const FileManager = require(rootDirectory + '/plugins/file-manager')

  /**
   * This function formats the given information.
   *
   * @param  {object} error - an error object to be formatted
   * @return {object}       - an object is reqturned in the callback
   */
  var formatMoreInfo = function (error) {
    if (error) {
      return error
    }
  }

  /**
   * This function takes an institution record object that was returned from
   * an integration and creates an object in the format required for saving
   * to the database.
   *
   * @param  {String} integration - the integration that created the institution object
   * @param  {Object} institution - the institution record object
   * @param  {Number} dateCreated - optional, the created date to use in Unix Epoch time
   * @return {Object}             - the institution object formatted for the DB
   */
  this.formatInstitution = function (integration, institution, dateCreated) {
    var formatted = {}

    if (integration === 'finicity') {
      // Take a Finicity institution record and format it for the database.
      var rawInstitution = institution.rawSource
      if (!dateCreated) {
        dateCreated = moment().unix()
      }
      formatted = {
        integration: integration,
        dateCreated: dateCreated,

        finID: rawInstitution.id,
        name: rawInstitution.name,
        typeDescription: rawInstitution.accountTypeDescription,
        urlHomeApp: rawInstitution.urlHomeApp,
        urlLogonApp: rawInstitution.urlLogonApp,
        urlProductApp: rawInstitution.urlProductApp,
        phone: rawInstitution.phone,
        currency: rawInstitution.currency,
        email: rawInstitution.email,
        specialText: rawInstitution.specialText,
        address: {
          addressLine1: rawInstitution.address.addressLine1,
          addressLine2: rawInstitution.address.addressLine2,
          city: rawInstitution.address.city,
          state: rawInstitution.address.state,
          postalCode: rawInstitution.address.postalCode,
          country: rawInstitution.address.country
        }
      }
    }

    return formatted
  }

  /**
   * This function takes an account record object that was returned from
   * an integration and creates an object in the format required for saving
   * to the database.
   *
   * @param  {String}   integration    - the integration that created the institution object
   * @param  {ObjectId} owner_id       - the Mongo objectId of the owner
   * @param  {ObjectId} institution_id - the Mongo objectId of the institution to
   *                                     which this account belongs
   * @param  {Object}   account        - the account record object
   * @param  {Number}   dateCreated    - optional, the created date to use in Unix Epoch time
   * @return {Object}                  - the account object formatted for the DB
   */
  this.formatAccount = function (integration, owner_id, institution_id, account, dateCreated) {
    var formatted = {}

    if (integration === 'finicity') {
      // Take a Finicity account record and format it for the database.
      var rawAccount = account.rawSource
      if (!dateCreated) {
        dateCreated = moment().unix()
      }

      formatted = {
        ownerID: owner_id,
        institutionID: institution_id,

        integration: integration,
        dateCreated: dateCreated,

        finID: rawAccount.id || null,
        finCustomerID: rawAccount.customerId || null,
        finInstitutionID: rawAccount.institutionId || null,
        finLoginID: rawAccount.institutionLoginId || null,

        number: rawAccount.number,
        name: rawAccount.name,
        type: rawAccount.type,
        status: rawAccount.status,
        balance: rawAccount.balance,
        raw: rawAccount
      }

      // The following may not be provided by Finicity. Save only if available.
      if (typeof rawAccount.balanceDate !== 'undefined') { formatted.balanceDate = rawAccount.balanceDate }
      if (typeof rawAccount.currency !== 'undefined') { formatted.currency = rawAccount.currency }
      if (typeof rawAccount.createdDate !== 'undefined') { formatted.finDateCreated = rawAccount.createdDate }
      if (typeof rawAccount.lastTransactionDate !== 'undefined') { formatted.lastTransactionDate = rawAccount.lastTransactionDate }
      if (typeof rawAccount.order !== 'undefined') { formatted.order = rawAccount.order }
      if (typeof rawAccount.position !== 'undefined') { formatted.position = rawAccount.position }
      if (typeof rawAccount.detail !== 'undefined') { formatted.detail = rawAccount.detail }

      // Leaving this code here, but commented out for now. At the moment the data
      // schema does not expand the detail portion of the account. But it migh be
      // done in the future. Or it is possible that the schema will be redueced to
      // the bare minimum required and whatever is received from Finicity is just
      // saved in 'raw'.

      // Add the optional details if present.
      // if (!rawAccount.detail) {
      // } else {
      //   console.warn(JSON.stringify(rawAccount, null, 2));
      //   var optionalDetail = {};
      //   var theDetails     = rawAccount.detail;
      //   switch (rawAccount.type) {
      //     case 'checking':
      //     case 'savings':
      //     case 'cd':
      //     case 'moneyMarket':
      //       // Optional detail if type is checking, savings, cd, or moneyMarket
      //       optionalDetail = {
      //         availableBalanceAmount:  theDetails.availableBalanceAmount,   // The available balance (typically the current balance minus any pending transactions)
      //         interestYtdAmount:       theDetails.interestYtdAmount,        // Interest accrued year-to-date
      //         periodInterestRate:      theDetails.periodInterestRate,       // The interest rate for the current period
      //         periodInterestAmount:    theDetails.periodInterestAmount ,    // Interest accrued during the current period
      //       };
      //       break;
      //
      //     case 'creditCard':
      //     case 'lineOfCredit':
      //       // Optional detail if type is creditCard or lineOfCredit
      //       optionalDetail = {
      //         creditMaxAmount:         theDetails.creditMaxAmount,          // The account's credit limit
      //         creditAvailableAmount:   theDetails.creditAvailableAmount,    // The available credit (typically the credit limit minus the current balance)
      //         paymentMinAmount:        theDetails.paymentMinAmount,         // Minimum payment due
      //         paymentDueDate:          theDetails.paymentDueDate,           // Due date for the next payment
      //         lastPaymentAmount:       theDetails.lastPaymentAmount,        // The amount received in the last payment
      //         lastPaymentDate:         theDetails.lastPaymentDate,          // The date of the last payment
      //         interestRate:            theDetails.interestRate,             // The account's current interest rate
      //         cashAdvanceInterestRate: theDetails.cashAdvanceInterestRate,  // Interest rate for cash advances
      //       };
      //       break;
      //
      //       case 'investment':
      //         // Optional detail if type is investment
      //         optionalDetail = {
      //           availableCashBalance:  theDetails.availableCashBalance,     // Amount available for cash withdrawal
      //         };
      //         break;
      //
      //     case 'mortgage':
      //     case 'loan':
      //       // Optional detail if type is mortgage or loan
      //       optionalDetail = {
      //         interestRate:            theDetails.interestRate,             // The interest rate
      //         nextPaymentDate:         theDetails.nextPaymentDate,          // Due date for the next payment
      //         nextPayment:             theDetails.nextPayment,              // Minimum payment due
      //         escrowBalance:           theDetails.escrowBalance,            // The escrow balance
      //         payoffAmount:            theDetails.payoffAmount,             // The amount required to payoff the loan
      //         principalBalance:        theDetails.principalBalance,         // The principal balance
      //         ytdInterestPaid:         theDetails.ytdInterestPaid,          // Interest paid year-to-date
      //         ytdPrincipalPaid:        theDetails.ytdPrincipalPaid,         // Principal paid year-to-date
      //         lastPaymentAmount:       theDetails.lastPaymentAmount,        // The amount of the last payment
      //         lastPaymentReceiveDate:  theDetails.lastPaymentReceiveDate,   // The date of the last payment
      //       };
      //       break;
      //   }
      //
      //   formatted.details = optionalDetail;
      // }
    }

    return formatted
  }

  /**
   * This function takes a transaction record object that was returned from
   * an integration and creates an object in the format required for saving
   * to the database.
   *
   * @param  {String}   integration - the integration that created the institution object
   * @param  {ObjectId} account_id  - the Mongo objectId of the account to
   *                                  which this transaction belongs
   * @param  {Object}   transaction - the transaction record object
   * @param  {Number}   balance     - the current account balance
   * @param  {Number}   dateCreated - optional, the created date to use in Unix Epoch time
   * @return {Object}               - the transaction object formatted for the DB
   */
  this.formatTransaction = function (integration, account_id, transaction, balance, dateCreated) {
    var formatted = {}

    if (integration === 'finicity') {
      // Take a Finicity transaction record and format it for the database.
      if (!dateCreated) {
        dateCreated = moment().unix()
      }

      formatted = {
        accountID: account_id,

        integration: integration,
        dateCreated: dateCreated,

        finID: transaction.id,
        finAccountID: transaction.accountId,
        finCustomerID: transaction.customerId,

        description: transaction.description || null,
        raw: transaction,
        currentBalance: balance
      }

      if (typeof transaction.institutionTransactionId !== 'undefined') { formatted.transactionID = transaction.institutionTransactionId }
      if (typeof transaction.amount !== 'undefined') { formatted.amount = transaction.amount }
      if (typeof transaction.bonusAmount !== 'undefined') { formatted.bonusAmount = transaction.bonusAmount }
      if (typeof transaction.checkNum !== 'undefined') { formatted.checkNum = transaction.checkNum }
      if (typeof transaction.createdDate !== 'undefined') { formatted.finDateCreated = transaction.createdDate }
      if (typeof transaction.escrowAmount !== 'undefined') { formatted.escrowAmount = transaction.escrowAmount }
      if (typeof transaction.feeAmount !== 'undefined') { formatted.feeAmount = transaction.feeAmount }
      if (typeof transaction.interestAmount !== 'undefined') { formatted.interestAmount = transaction.interestAmount }
      if (typeof transaction.memo !== 'undefined') { formatted.memo = transaction.memo }
      if (typeof transaction.postedDate !== 'undefined') { formatted.postedDate = transaction.postedDate }
      if (typeof transaction.principalAmount !== 'undefined') { formatted.principalAmount = transaction.principalAmount }
      if (typeof transaction.status !== 'undefined') { formatted.status = transaction.status }
      if (typeof transaction.transactionDate !== 'undefined') { formatted.transactionDate = transaction.transactionDate }
      if (typeof transaction.type !== 'undefined') { formatted.type = transaction.type }
      if (typeof transaction.unitQuantity !== 'undefined') { formatted.unitQuantity = transaction.unitQuantity }
      if (typeof transaction.unitValue !== 'undefined') { formatted.unitValue = transaction.unitValue }
      if (typeof transaction.categorization !== 'undefined') { formatted.categorization = transaction.categorization }
      if (typeof transaction.cusipNo !== 'undefined') { formatted.cusipNo = transaction.cusipNo }
    }

    return formatted
  }

  /**
   * This function finds a financial instituion. If one does not already exist
   * a new one is created.
   *
   * @param  {Object}   institution - the object containing the institution. It
   *                                  must be formatted as defined in the
   *                                  institution schema.
   * @param  {Function} callback    - the callback function
   * @return {}                     -
   */
  this.findOrCreateDataInstitution = function (institution, callback) {
    // Make sure there is an institution ID.
    var finInstitutionID = institution.finID
    if (typeof finInstitutionID === 'undefined' || finInstitutionID === null) {
      callback(errors.dbData.MISSING_INSTITUTIONID)
      return
    }

    // Only add the institution is it does not already exist.
    DataInstitutions.findOneAndUpdate(
      {'finInstitutionID': finInstitutionID},
      {$setOnInsert: institution},
      {upsert: true, new: true},
      function (err, theInstitution) {
        if (err) {
          callback({
            code: errors.dbData.FAIL_INSTITUTION.code,
            msg: errors.dbData.FAIL_INSTITUTION.msg,
            moreInfo: formatMoreInfo(err)
          })
        } else {
          callback(null, theInstitution._id)
        }
      }
    )
  }

  /**
   * This function finds an account in the database.
   *
   * @param  {String}   finCustomerID - the id of the customer to which the
   *                                    account belongs
   * @param  {String}   finAccountID  - the id of the account to find
   * @param  {Function} callback      - the callback
   * @return {}                       -
   */
  this.findCustomerDataAccount = function (finCustomerID, finAccountID, callback) {
    DataAccounts.findOne(
      {'finCustomerID': finCustomerID, 'finID': finAccountID},
      function (err, account) {
        if (err) {
          callback({
            code: errors.dbData.FAIL_FINDACCOUNT.code,
            msg: errors.dbData.FAIL_FINDACCOUNT.msg,
            moreInfo: formatMoreInfo(err)
          })
        } else {
          callback(null, account)
        }
      }
    )
  }

  this.findDataAccount = function (owner_id, institution_id, number, name, callback) {
    DataAccounts.findOne(
      {'ownerID': owner_id, 'institutionID': institution_id, 'number': number, 'name': name},
      function (err, account) {
        if (err) {
          callback({
            code: errors.dbData.FAIL_DATAACCOUNT.code,
            msg: errors.dbData.FAIL_DATAACCOUNT.msg,
            moreInfo: formatMoreInfo(err)
          })
        } else {
          callback(null, account)
        }
      }
    )
  }

  /**
   * This function finds a financial instituions account. If one does not
   * already exist a new one is created.
   *
   * @param  {Object}   account  - the object containing the account. It must
   *                               be formatted as defined in the account schema.
   * @param  {Function} callback - the callback function
   * @return {}                  -
   */
  this.findOrCreateDataAccount = function (account, callback) {
    // Validate the presence of the institution ID and account ID.
    var finInstitutionID = account.finInstitutionID
    if (typeof finInstitutionID === 'undefined' || finInstitutionID === null) {
      callback(errors.dbData.MISSING_INSTITUTIONID)
      return
    }

    var finID = account.finID
    if (typeof finID === 'undefined' || finID === null) {
      callback(errors.dbData.MISSING_ACCOUNTID)
      return
    }

    // Add current time in seconds since UNIX epoch.
    account.dateCreated = moment().unix()

    // Only add the account if it does not already exist.
    DataAccounts.findOneAndUpdate(
      {
        'ownerID': account.ownerID,
        'finInstitutionID': finInstitutionID,
        'finID': finID},
      {$setOnInsert: account},
      {upsert: true, new: true},
      function (err, theAccount) {
        if (err) {
          error(err)
          callback({
            code: errors.dbData.FAIL_ACCOUNT.code,
            msg: errors.dbData.FAIL_ACCOUNT.msg,
            moreInfo: formatMoreInfo(err)
          })
        } else {
          callback(null, theAccount._id)
        }
      }
    )
  }

  /**
   * This function finds a financial instituions account transaction. If one
   * does not already exist a new one is created.
   *
   * @param  {Object}   transaction - the object containing the transaction. It
   *                                  must be formatted as defined in the
   *                                  transaction schema.
   * @param  {Function} callback    - the callback function
   * @return {}                     -
   */
  this.findOrCreateDataTransaction = function (transaction, callback) {
    // Validate the presence of the account ID and transaction ID.
    var finAccountID = transaction.finAccountID
    if (typeof finAccountID === 'undefined' || finAccountID === null) {
      callback(errors.dbData.MISSING_ACCOUNTID)
      return
    }
    var finID = transaction.finID
    if (typeof finID === 'undefined' || finID === null) {
      callback(errors.dbData.MISSING_TRANSACTIONID)
      return
    }

    // Only add the transaction if it does not already exist.
    DataTransactions.findOneAndUpdate(
      {
        'accountID': transaction.accountID,
        'finAccountID': finAccountID,
        'finID': finID
      },
      {$setOnInsert: transaction},
      {upsert: true, new: true},
      function (err, theTransaction) {
        if (err) {
          callback(err)
        } else {
          callback(null, theTransaction._id)
        }
      }
    )
  }

  /**
   * This function finds transactions.
   *
   * @param  {String}   accountID - the objectId of the account
   * @param  {Number}   startDate - the start date in Unix Epoch time (seconds)
   * @param  {Number}   endDate   - the end date in Unix Epoch time (seconds)
   * @param  {Integer}  index     - the starting position
   * @param  {Integer}  limit     - the maximum number to get
   * @param  {Function} callback  - the callback function
   * @return {}                   - returned through the callback
   */
  this.findDataAccountTransactions = function (accountID, startDate, endDate, index, limit, callback) {
    var findQuery = { 'accountID': mongoose.Types.ObjectId(accountID) }
    findQuery.postedDate = {}
    findQuery.postedDate['$gt'] = startDate
    findQuery.postedDate['$lt'] = endDate

    DataTransactions.find(findQuery).count(function (err, count) {
      if (err) {
        error(err)
        callback(errors.dbData.FAIL_TRANSACTIONCNT)
        return
      }

      // @steve, this is missing in the api. Just a note.
      // var sortQuery = {};
      // if (options.sortBy && options.sortType)
      //   sortQuery[options.sortBy] = options.sortType;

      // DataTransactions.find(findQuery).skip(index).limit(limit).sort(sortQuery).exec(function(err, transactions) {
      DataTransactions.find(findQuery).skip(index).limit(limit).exec(function (err, transactions) {
        if (err) {
          error(err)
          callback(errors.dbData.FAIL_TRANSACTIONS)
        } else {
          callback(null, count, transactions)
        }
      })
    })
  }

  /**
   * Export transactions as CSV
   * @param  {String}   accountID - the objectId of the account
   * @param  {Function} callback  - the callback function
   * @return {file}               - returned through the callback
   */
  this.exportTransactionsAsCSV = function exportTransactionsAsCSV (accountID, filename, callback) {
    const findQuery = { 'accountID': mongoose.Types.ObjectId(accountID) }
    let cancel = false
    const writer = new FileManager.Writer({
      filename: filename,
      content_type: 'text/csv',
      encrypt: false
    })
    writer.open()
    writer.on('error', (err) => {
      cancel = true
      callback(err)
    })
    writer.on('close', (file) => {
      cancel = true
      callback(null, file)
    })
    writer.write(`ID, Transaction Date, Posted Date, Memo, Description, Amount\n`)
    const cursor = DataTransactions.find(findQuery).cursor()
    cursor.on('data', (t) => {
      const id = t.finID || ''
      const transactionDate = t.transactionDate ? moment(t.transactionDate * 1000).format('MM/DD/YYYY') : ''
      const postedDate = t.postedDate ? moment(t.postedDate * 1000).format('MM/DD/YYYY') : ''
      const memo = t.memo || ''
      const description = t.description || ''
      const amount = t.amount || ''
      writer.write(`"${id}","${transactionDate}","${postedDate}","${memo}","${description}",${amount}\n`)
    })
    cursor.on('close', () => {
      // This grievous hack brought to you by Mongoose which fires the 'close' event and then fires ONE MORE 'data' event.
      // Basically this puts importer.close() to the end of the dispatch queue so the data event processes first.
      setTimeout(() => { writer.close() }, 1.0)
    })
  }

  /**
   * This function finds a single transaction.
   *
   * @param  {String}   transactionID - the objectID of the transaction
   * @param  {Function} callback      - the callback function
   * @return {}                       - returned through the callback
   */
  this.findTransaction = function (transactionID, callback) {
    DataTransactions.findById(mongoose.Types.ObjectId(transactionID), function (err, transaction) {
      if (err) {
        error(err)
        callback(errors.dbData.FAIL_TRANSACTION)
      } else {
        callback(null, transaction)
      }
    })
  }
}
