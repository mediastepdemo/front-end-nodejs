const {Schema} = require('mongoose')
const connections = require('../connections')

const userSchema = Schema({
  uid: { type: String, unique: true, required: true },
  authID: { type: String, required: true },
  status: { type: String, default: 'ACTIVE', enum: ['ACTIVE', 'INACTIVE'] }
}, { collection: 'users' })

module.exports = connections.finicity.model('User', userSchema)
