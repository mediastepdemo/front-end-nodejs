const {Schema} = require('mongoose')
const connections = require('../connections')

const accountSchema = Schema({
  uid: { type: String, required: true },
  status: { type: String, default: 'ACTIVE', enum: ['ACTIVE', 'INACTIVE'] }
}, { collection: 'accounts' })

module.exports = connections.finicity.model('Account', accountSchema)
