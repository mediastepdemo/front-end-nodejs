const {Schema} = require('mongoose')
const connections = require('../connections')

const SCHEMA_CHANGES = {original: {},
  updated: {}}

const activitySchema = Schema({
  userID: { type: Schema.ObjectId, ref: 'User'},
  engagementID: { type: Schema.ObjectId, ref: 'Engagement', default: null },
  type: { type: String, required: true},
  operation: {type: String, required: true, enum: ['CREATE', 'READ', 'UPDATE', 'DELETE', 'DOWNLOAD', 'UPLOAD', 'INVITE', 'ONBOARD']},
  changes: SCHEMA_CHANGES,
  timestamp: {type: Date, default: Date.now }
}, { collection: 'activities' })

module.exports = connections.auvenir.model('Activity', activitySchema)
