const { Schema } = require('mongoose')
const connections = require('../connections')

const fileSchema = Schema({
  name: { type: String, required: true },
  size: { type: String, required: true },
  owner: { type: Schema.ObjectId, required: true, ref: 'User' },
  path: { type: String, required: true },
  scanned: { type: Boolean, default: false },
  status: {
    type: String,
    default: 'ACTIVE',
    enum: [
      'ACTIVE',
      'INACTIVE',
      'SCANNING',
      'SECURE',
      'INSECURE',
      'UPLOADING',
      'ERROR'
    ]
  },
  source: {
    name: { type: String }, // Google Drive
    uid: { type: String }, // GoogleDrive.User.uid
    fid: { type: Schema.ObjectId } // GdriveObj._id
  },
  bucket: {
    name: { type: String },
    id: { type: Schema.ObjectId } // fs.files._ID
  },
  dateUploaded: { type: Date, default: Date.now },
  tags: [ { type: String } ]
}, { collection: 'files' })

module.exports = connections.auvenir.model('File', fileSchema)
