const {Schema} = require('mongoose')
const connections = require('../connections')

const AGREEMENT = Schema({
  agreementID: { type: String },
  timeStamp: { type: Date }
}, { _id: false })

const INTEGRATION = Schema({
  name: { type: String },
  uid: { type: String }
}, {_id: false})

const userSchema = Schema({
  type: { type: String, required: true, uppercase: true, enum: ['ADMIN', 'CLIENT', 'AUDITOR'] },
  email: { type: String, required: true, lowercase: true, trim: true, unique: true },
  password: {type: String, default: '', required: false},
  password_salt: {type: String, default: '', required: false},
  firstName: { type: String, default: '' },
  lastName: { type: String, default: '' },
  jobTitle: { type: String, default: '' },
  phone: { type: String, default: '' },
  dateCreated: { type: Date, default: Date.now },
  lastLogin: { type: Date, default: null },
  status: { type: String, required: true, uppercase: true, enum: ['WAIT-LIST', 'PENDING', 'ONBOARDING', 'ACTIVE', 'LOCKED', 'INACTIVE'] },
  skipOnboard: { type: Boolean, default: true },
  passwordResetRequired: { type: Boolean, default: false },
  verified: { type: Boolean, default: false },
  integrations: [ INTEGRATION ],
  agreements: [ AGREEMENT ],
  referral: { type: String, default: '' },
  auth: {
    id: { type: String, default: '' },
    access: {
      token: { type: String, default: '' },
      passwordResetRequired: { type: Boolean, default: false },
      expires: { type: Date, default: null }
    },
    developer: { // TODO - code cleanup after db model change
      apiKey: { type: String, default: '' }
    }
  },
  notifications: {
    engagementInvite: {
      email: { type: Boolean, default: false }
    },
    joinEngagement: {
      email: { type: Boolean, default: false }
    },
    documentUploaded: {
      email: { type: Boolean, default: false }
    },
    newTodo: {
      email: { type: Boolean, default: false }
    },
    newComment: {
      email: { type: Boolean, default: false }
    },
    newRequest: {
      email: { type: Boolean, default: false }
    },
    todoCompleted: {
      email: { type: Boolean, default: false }
    }
  },
  lastOnboardedEID: { type: String, default: '' }
}, { collection: 'users' })

module.exports = connections.auvenir.model('User', userSchema)
