const { Schema } = require('mongoose')
const connections = require('../connections')

const ACL = Schema({
  id: { type: Schema.ObjectId, ref: 'User' }
}, {_id: false})

const TODO = Schema({
  name: { type: String, required: true },
  description: { type: String, required: true },
  category: { type: String },
  requests: [{ type: String }]
}, {_id: false})

var templateSchema = Schema({
  acl: [ ACL ],
  name: { type: String, required: true },
  categories: [{ name: { type: String }, color: { type: String }}],
  todos: [TODO],
  sourceEngagement: { type: Schema.ObjectId, ref: 'Engagement', required: true }
}, { collection: 'templates' })

module.exports = connections.auvenir.model('Template', templateSchema)
