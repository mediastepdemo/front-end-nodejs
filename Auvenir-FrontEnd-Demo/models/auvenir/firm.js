const mongoose = require('mongoose')
const connections = require('../connections')

const ADDRESS = {
  unit: { type: String, default: '' },
  streetAddress: { type: String, default: '' },
  city: { type: String, default: '' },
  stateProvince: { type: String, default: '' },
  postalCode: { type: String, default: '' },
  country: { type: String, default: '' }
}

const ACL = mongoose.Schema({
  id: { type: mongoose.Schema.ObjectId, ref: 'User' },
  admin: { type: Boolean, required: true }
}, {_id: false})

const firmSchema = mongoose.Schema({
  acl: [ ACL ],
  name: { type: String, required: true },
  logo: { type: String, default: '' },
  website: { type: String, default: ''},
  address: ADDRESS,
  size: { type: String, default: '' },
  phone: { type: String, default: '' },
  affiliated: { type: Boolean, default: false },
  affiliatedFirmName: { type: String, default: '' },
  logoDisplayAgreed: { type: Boolean, default: false },
  nameChange: { type: Boolean, default: false },
  previousName: {type: String, default: '' },
  memberID: {type: String, defulat: ''}
}, { collection: 'firms' })

module.exports = connections.auvenir.model('Firm', firmSchema)
