const staticMethods = require('./static')
const instanceMethods = require('./instance')

/**
 * add static and instance methods to schema
 * @param {Object} schema mongoose engagement schema
 * @return {null} no return
 */
module.exports = function (schema) {
  schema.statics = staticMethods
  schema.methods = instanceMethods
}
