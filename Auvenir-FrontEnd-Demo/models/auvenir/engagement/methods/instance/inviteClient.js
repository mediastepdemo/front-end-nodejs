const _ = require('lodash')
const {aclRole, aclStatus, engagementStatus} = require('../../resources')
const debug = require('debug')('auvenir:model:engagement:inviteClient')

/**
 * Update engagement status when auditor invite the client
 * @param {String} auditorID auditor _id
 * @param {String} clientID client _id
 * @param {Function} cbk callback an error if failed
 * @return {null} no return
 */
module.exports = function (auditorID, clientID, cbk) {
  debug(`Auditor ${auditorID} is inviting Client ${clientID} to join engagement ${this.id}`)
  let auditorInAcl = _.findIndex(this.acl, (acl) => {
    return (acl.role === aclRole.AUDITOR) && (acl.id.toString() === auditorID)
  })
  if (!~auditorInAcl) {
    return cbk({code: 403, msg: `auditor with id ${auditorID} has no permission with engagement ${this._id}`})
  }

  let clientIndexInAcl = _.findIndex(this.acl, (acl) => {
    return (acl.role === aclRole.CLIENT) && (acl.id.toString() === clientID)
  })
  if (!~clientIndexInAcl) {
    return cbk({code: 403, msg: `This engagement doesn't have the client yet`})
  }

  this.acl[clientIndexInAcl].status = aclStatus.INVITED
  this.status = engagementStatus.ACTIVE

  this.save((err) => {
    if (err) { // TODO - check how error is being handled.
      cbk(err, this)
    }

    debug(`Auditor ${auditorID} invited Client ${clientID} to join engagement ${this.id}`)
    this.fetchAdditionalInfo((err, engagement) => {
      cbk(err, engagement)
    })
  })
}
