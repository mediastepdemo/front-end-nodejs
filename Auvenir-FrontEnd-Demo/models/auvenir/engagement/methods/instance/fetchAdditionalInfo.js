const async = require('async')
const debug = require('debug')('auvenir:model:engagement:fetchAdditionalInfo')

/**
 * populate business, firm, user information to current engagement instance
 * @param {Function} cbk callback function
 * @return {null} no return
 */
module.exports = function (cbk) {
  const User = this.model('User')
  const File = this.model('File')
  const Activity = this.model('Activity')
  const Firm = this.model('Firm')
  let engagement = this.toJSON()

  const aclUserInfo = (cbk) => {
    async.map(this.acl, (acl, cbk) => {
      const aclObj = acl.toJSON()
      aclObj.userInfo = {}

      User.findOne({ _id: acl.id }).select({
        password: false,
        password_salt: false,
        integration: false,
        phone: false,
        dateCreated: false,
        auth: false,
        notifications: false,
        agreements: false,
        referral: false,
        type: false,
        lastOnboardedEID: false
      }).exec((err, aclUser) => {
        if (err) {
          return cbk(err)
        }
        if (!aclUser) {
          return cbk(`No user was found with _id: ${acl.id}`)
        }

        aclObj.userInfo = aclUser.toJSON()
        return cbk(null, aclObj)
      })
    }, cbk)
  }

  const business = (cbk) => {
    let business = {}
    let opts = [
      { path: 'business.keyContact', select: '_id firstName lastName email' },
      { path: 'business.id', select: '_id name' }
    ]

    this.populate(opts, (err, newEngagement) => {
      if (err) {
        debug('got error in business')
        return cbk(err)
      }
      debug(newEngagement)
      debug(newEngagement.business)

      if (newEngagement.business) {
        let _id = (newEngagement.business.id) ? newEngagement.business.id._id : ''
        let name = (newEngagement.business.id) ? newEngagement.business.id.name : ''
        let keyContact = (newEngagement.business.id) ? newEngagement.business.keyContact : ''
        business = {
          _id,
          name,
          keyContact
        }
      }
      debug(`business >>> ${JSON.stringify(business)}`)
      return cbk(null, business)
    })
  }

  const firm = (cbk) => {
    let firm = {}
    let opts = [
      { path: 'firm.creater', select: '_id firstName lastName email' },
      { path: 'firm.currentOwner', select: '_id firstName lastName email' },
      { path: 'firm.id', select: '_id name' }
    ]

    Firm.findOne({ _id: this.firm.id }, (err, oneFirm) => {
      if (err) {
        return cbk(err)
      }
      if (!oneFirm) {
        return cbk({msg: 'No Firm was found.'})
      }
      this.populate(opts, (err, newEngagement) => {
        if (err) {
          cbk(err)
        } else {
          let _id = newEngagement.firm.id._id
          let name = newEngagement.firm.id.name
          let logo = oneFirm.logo
          let logoDisplayAgreed = oneFirm.logoDisplayAgreed
          let creater = newEngagement.firm.creater
          let currentOwner = newEngagement.firm.currentOwner
          firm = {
            _id,
            name,
            logo,
            logoDisplayAgreed,
            creater,
            currentOwner
          }
          debug(`firm >>> ${JSON.stringify(firm)}`)
          cbk(null, firm)
        }
      })
    })
  }

  const file = (cbk) => {
    async.map(this.files, (file, cbk) => {
      File.findOne({ _id: file }).select({
        size: false,
        scanned: false,
        tags: false
      }).exec((err, oneFile) => {
        if (err) {
          return cbk(err)
        }
        if (!oneFile) {
          return cbk(`No file was found with _id: ${file}`)
        }

        let opts = [{ path: 'owner', select: '_id firstName lastName email type' }]
        oneFile.populate(opts, (err, newFile) => {
          if (err) {
            return cbk(err)
          }

          file = newFile.toJSON()
          debug(`file >>> ${JSON.stringify(file)}`)
          return cbk(null, file)
        })
      })
    }, cbk)
  }

  // TODO - maybe this can be gone
  const todo = (cbk) => {
    async.map(this.todos, (todo, cbk) => {
      const findCreatedBy = (cbk) => {
        User.findOne({ _id: todo.createdBy }, { firstName: 1, lastName: 1, email: 1 }).exec((err, oneUser) => {
          if (err) {
            return cbk(err)
          }
          if (!oneUser) {
            return cbk(`No User for todo.createdBy was found with _id: ${todo.createdBy}`)
          }

          return cbk(null, { createdBy: oneUser })
        })
      }

      const findAuditorAssignee = (cbk) => {
        User.findOne({ _id: todo.auditorAssignee }, { firstName: 1, lastName: 1, email: 1 }).exec((err, oneUser) => {
          if (err) {
            return cbk(err)
          }
          if (!oneUser) {
            return cbk(`No User for todo.auditorAssignee was found with _id: ${todo.auditorAssignee}`)
          }

          return cbk(null, { auditorAssignee: oneUser })
        })
      }

      const findClientAssignee = (cbk) => {
        if (todo.clientAssignee !== null) {
          User.findOne({ _id: todo.clientAssignee }, {_id: 1, firstName: 1, lastName: 1, email: 1 }).exec((err, oneUser) => {
            if (err) {
              return cbk(err)
            }
            if (!oneUser) {
              return cbk(`No User for todo.clientAssignee was found with _id: ${todo.clientAssignee}`)
            }

            return cbk(null, { clientAssignee: oneUser })
          })
        } else {
          return cbk(null, { clientAssignee: null })
        }
      }

      async.parallel([findCreatedBy, findAuditorAssignee, findClientAssignee], (err, userInfo) => {
        if (err) {
          cbk(err)
        }
        debug(`todo >>> ${JSON.stringify(userInfo)}`)
        todo.createdBy = userInfo[0].createdBy
        todo.auditorAssignee = userInfo[1].auditorAssignee
        todo.clientAssignee = userInfo[2].clientAssignee
        cbk(null, todo)
      })
    }, cbk)
  }

  const activity = (cbk) => {
    let engagementID = require('../../../../../plugins/utilities/utility').castToObjectId(engagement._id)
    Activity.findOne({engagementID: engagementID}, {}, {sort: {'timestamp': -1}}, function (err, activity) {
      if (err) {
        return cbk(err)
      }
      cbk(null, {lastUpdated: (activity) ? activity.timestamp : null})
    })
  }

  async.parallel({ aclUserInfo, business, firm, file, activity }, (err, result) => {
    if (err) {
      debug(err)
      cbk(err)
    } else {
      debug(`got result >>> ${JSON.stringify(result)}`)
      engagement.acl = result.aclUserInfo
      engagement.business = result.business
      engagement.firm = result.firm
      engagement.files = result.file
      if (result.activity && result.activity.lastUpdated) {
        engagement.lastUpdated = result.activity.lastUpdated
      }
      // engagement.todos = result.todo
      debug(`engagement >>> ${JSON.stringify(engagement)}`)
      cbk(null, engagement)
    }
  })
}
