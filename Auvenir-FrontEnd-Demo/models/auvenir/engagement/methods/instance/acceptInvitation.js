const _ = require('lodash')
const debug = require('debug')('auvenir:model:engagement:acceptInvitation')
const {aclRole, aclStatus, engagementStatus} = require('../../resources')

/**
 * client inside the acl accept the invitation, the engagement will transit to data gathering status
 * @param {String} clientId client user id
 * @param {Function} cbk callback function
 * @return {null} no return
 */

module.exports = function (userId, cbk) {
  debug('accept invitation')

  if (!userId) {
    return cbk({code: 403, msg: 'Invalid userId'})
  }

  let userIndex = _.findIndex(this.acl, (acl) => {
    return (acl.id.toString() === userId.toString()) && (acl.role === aclRole.CLIENT || acl.role === aclRole.AUDITOR)
  })

  if (userIndex === -1) {
    return cbk({code: 403, msg: 'The user is not in the acl list'})
  }

  let user = this.acl[userIndex]

  if (user.status === aclStatus.ACTIVE) {
    return cbk({code: 302, msg: 'The user has already accepted the invitation'})
  }

  if (user.status === aclStatus.INACTIVE) {
    return cbk({code: 403, msg: 'The user is not part of this engagement anymore.'})
  }

  user.status = aclStatus.ACTIVE
  this.status = engagementStatus.ACTIVE

  this.save((err) => {
    cbk(err)
  })
}
