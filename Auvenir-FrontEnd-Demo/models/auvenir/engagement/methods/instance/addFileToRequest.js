const _ = require('lodash')
const debug = require('debug')('auvenir:model:engagement:addFileToRequest')
const initLogger = require('../../../../../plugins/logger/logger').init
const {info, warn, error} = initLogger(__filename)

/**
 * Adding file to a request
 * @param {ObjectId} requestID
 * @param {ObjectId} todoID
 * @return {Function} cbk
 */

module.exports = function (requestID, todoID, fileID, cbk) {
  warn('Engagement Instance Function: Add File To Request.')

  if (!requestID || !todoID || !fileID) {
    return cbk({code: 1, msg: 'Not all valid ids were passed'})
  }

  this.todos.forEach((todo, i) => {
    if (todo._id.toString() === todoID.toString()) {
      todo.requests.forEach((request) => {
        if (request._id.toString() === requestID.toString()) {
          if (!_.isEmpty(request.currentFile)) {
            request.previousFiles.push(request.currentFile)
          }
          request.currentFile = fileID
        }
      })
    }
  })

  this.save((err) => {
    if (err) {
      return cbk(err)
    } else {
      return cbk(null)
    }
  })
}
