const _ = require('lodash')
const async = require('async')
const debug = require('debug')('auvenir:model:engagement')
const { aclRole, aclStatus } = require('../../resources')

/**
 * change the businessID.
 * @param {Object} data clientID, businessID
 * @param {Function} cbk callback function
 * @return {null} no return
 */
module.exports = function (data, cbk) {
  const {clientID, businessID} = data

  if (!clientID) {
    return cbk({code: 1, msg: 'No key contact client is passed.'})
  }
  if (!businessID) {
    return cbk({code: 403, msg: 'Invalid businessID'})
  }

  // empty the acl that was filled with other client users.
  var emptyClientACL = (cbk) => {
    if (!_.isEmpty(this.acl)) {
      this.acl.forEach((user, i) => {
        if (user.role === aclRole.CLIENT) {
          this.acl.splice(i, 1)
        }
      })
      cbk(null)
    } else {
      cbk(null)
    }
  }

  var newClientACL = (cbk) => {
    this.business.id = businessID
    this.business.keyContact = clientID
    this.acl.push({
      role: aclRole.CLIENT,
      lead: true,
      status: aclStatus.INVITED,
      id: clientID
    })
    this.status = 'ACTIVE'
    this.save((err) => {
      cbk(err)
    })
  }

  async.waterfall([emptyClientACL, newClientACL], (err) => {
    cbk(err, this)
  })
}
