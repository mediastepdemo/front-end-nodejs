const {getTasksByStatus} = require('../../resources/engagementTasksTemplate')

/**
 * TODO get a list of task base on current engagement's status, acl user status
 * @return {Array} an array of task
 */
module.exports = function () {
  const {status} = this
  return getTasksByStatus(status)
}
