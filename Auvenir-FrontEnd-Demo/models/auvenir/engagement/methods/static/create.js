const async = require('async')
const debug = require('debug')('auvenir:model:engagement:create-engagement')
const {aclRole, aclStatus, engagementStatus} = require('../../resources')
const Logger = require('../../../../../plugins/logger/logger')
const { info, error } = Logger.init(__filename)

/**
 * create an engagement
 * @param data {Object} provide auditorID
 * @param cbk {Function} callback(err, Mongoose instance: engagement)
 * @return {null} no return
 */
module.exports = function (data, cbk) {
  debug('Static Method: create', data)
  const {firmID, auditorID} = data

  const acl = [
    { id: auditorID, role: aclRole.AUDITOR, lead: true, status: aclStatus.ACTIVE }
  ]
  const newEngagement = new this({
    acl,
    status: engagementStatus.PLANNING,
    firm: {
      id: firmID,
      creater: auditorID,
      currentOwner: auditorID
    }
  })
  info(newEngagement)
  newEngagement.save((err, engagement) => {
    error(err)
    cbk(err, engagement)
  })
}
