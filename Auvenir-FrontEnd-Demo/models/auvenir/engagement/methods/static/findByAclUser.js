const debug = require('debug')('auvenir:model:engagement')
const {aclRole} = require('../../resources')
const initLogger = require('../../../../../plugins/logger/logger').init
const { info, warn, error } = initLogger(__filename)
const ObjectId = require('mongodb').ObjectID
/**
 * find a list of engagement by 'ACTIVE' or 'INVITED' user in engagement acl list
 * @param {Object} data role, status, id
 * @param {Function} cbk callback(err, Mongoose instance: engagement)
 * @return {null} no return
 */
module.exports = function (data, cbk) {
  debug('Static Methods: findByAclUser', data)

  const { role, id } = data
  const validRoles = Object.keys(aclRole).map((key) => {
    return aclRole[key]
  })

  const queryFilters = {id, 'status': {$in: ['ACTIVE', 'INVITED']}}
  if (role) {
    if (!~validRoles.indexOf(role)) {
      return cbk({code: 403, msg: 'Invalid user role'})
    } else {
      queryFilters.role = role
    }
  }

  this.find({acl: {$elemMatch: queryFilters}}, (err, engagements) => {
    warn(engagements.length ? `Found ${engagements.length} engagements` : 'Cannot find engagement')
    if (err) {
      return cbk(err)
    }

    cbk(null, engagements)
  })
}
