const path = require('path')
const fs = require('fs')

fs.readdirSync(__dirname).forEach((file) => { // eslint-disable-line no-sync
  if (file === 'index.js') {
    return
  }
  module.exports[path.basename(file, '.js')] = require(path.join(__dirname, file))
})
