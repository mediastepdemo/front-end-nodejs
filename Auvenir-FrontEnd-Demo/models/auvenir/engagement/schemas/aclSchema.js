const {Schema} = require('mongoose')
const _ = require('lodash')
const {aclStatus, aclRole} = require('../resources')

module.exports = Schema({
  role: { type: String, required: true, enum: _.values(aclRole) },
  id: { type: Schema.ObjectId, required: true },
  lead: { type: Boolean, required: true, default: false },
  status: { type: String, required: true, enum: _.values(aclStatus) }
}, {_id: false})
