const {Schema} = require('mongoose')
const commentSchema = require('./commentSchema')
const requestSchema = require('./requestSchema')

module.exports = Schema({
  name: { type: String, required: false, default: ''},
  description: { type: String, default: '' },
  status: { type: String, enum: ['ACTIVE', 'INACTIVE']},
  category: { type: Schema.ObjectId },
  dueDate: { type: Date },
  comments: [ commentSchema ],
  requests: [ requestSchema ],
  createdBy: { type: Schema.ObjectId, required: true, ref: 'User' },
  auditorAssignee: { type: Schema.ObjectId, ref: 'User', required: true },
  clientAssignee: { type: Schema.ObjectId, ref: 'User' },
  completed: {type: Boolean, default: false}
})
