const {Schema} = require('mongoose')

module.exports = Schema({
  name: { type: String, required: true, default: 'Untitled Request' },
  currentFile: { type: Schema.ObjectId, ref: 'File' },
  previousFiles: [{ type: Schema.ObjectId, ref: 'File' }],
  dateCreated: { type: Date, required: true, default: Date.now },
  dateUploaded: { type: Date },
  status: { type: String, default: 'ACTIVE' }
})
