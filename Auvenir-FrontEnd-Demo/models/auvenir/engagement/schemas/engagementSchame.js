const {Schema} = require('mongoose')
const _ = require('lodash')
const {engagementStatus} = require('../resources')
const aclSchema = require('./aclSchema')
const todoSchema = require('./todoSchema')

module.exports = Schema({
  acl: [aclSchema],
  name: { type: String, required: true, default: 'Untitled' },
  type: { type: String },
  status: {
    type: String,
    required: true,
    enum: _.values(engagementStatus)
  },
  previousStatus: {
    type: String,
    enum: _.values(engagementStatus)
  },
  business: {
    id: { type: Schema.ObjectId, ref: 'Business' },
    keyContact: { type: Schema.ObjectId, ref: 'User' }
  },
  firm: {
    id: { type: Schema.ObjectId, ref: 'Firm', required: true },
    creater: { type: Schema.ObjectId, ref: 'User', required: true },
    currentOwner: { type: Schema.ObjectId, ref: 'User', required: true }
  },
  bankStatementDateRange: {
    startDate: {type: Date, default: null},
    endDate: {type: Date, default: null}
  },
  todos: [todoSchema],
  files: [{ type: Schema.ObjectId, ref: 'File' }],
  categories: [{name: {type: String}, color: {type: String}}],
  dateCreated: {type: Date, default: Date.now},
  lastUpdated: {type: Date, default: null},
  dateCompleted: {type: Date, default: null},
  dueDate: {type: Date, default: null}
}, {collection: 'engagements'})
