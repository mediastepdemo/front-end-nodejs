const {Schema} = require('mongoose')

module.exports = Schema({
  content: { type: String, default: '' },
  sender: { type: Schema.ObjectId, ref: 'User', required: true },
  file: { type: Schema.ObjectId, ref: 'File' },
  dateCreated: { type: Date },
  lastViewed: { type: Date },
  status: { type: String, required: true, default: 'OK', enum: ['OK', 'READ', 'ERROR']}
})
