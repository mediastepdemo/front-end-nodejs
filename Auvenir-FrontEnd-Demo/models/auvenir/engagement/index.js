const {engagementSchema} = require('./schemas')
const methods = require('./methods')
var connections = require('../../connections')

methods(engagementSchema)

module.exports = connections.auvenir.model('Engagement', engagementSchema)
