const {SET_SCHEDULE} = require('./engagementStatusEnum')

const tasks = {}

tasks[SET_SCHEDULE] = {
  SELECT_CLIENT: {text: 'Select your client', done: false},
  SET_REQUEST_LIST: {text: `Edit the request list of files you'd like from your client`, done: false},
  INVITE_CLIENT: {text: 'Invite your client to join the engagement and start completing your requests', done: false}
}

module.exports = {
  getTasksByStatus (engagementStatus) {
    return tasks[engagementStatus] || {}
  }
}
