const aclRole = {
  CLIENT: 'CLIENT',
  AUDITOR: 'AUDITOR'
}

const aclStatus = {
  ACTIVE: 'ACTIVE',
  INVITED: 'INVITED',
  INACTIVE: 'INACTIVE'
}

module.exports = {
  aclRole,
  aclStatus
}
