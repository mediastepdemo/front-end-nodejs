const mongoose = require('mongoose')
const {businessSchema} = require('./schemas')
const methods = require('./methods')
var connections = require('../../connections')

methods(businessSchema)

module.exports = connections.auvenir.model('Business', businessSchema)
