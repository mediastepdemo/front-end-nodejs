const {Schema} = require('mongoose')
const ACL = require('./acl')
const Address = require('./address')

module.exports = Schema({
  acl: [ACL],
  name: {type: String, required: true},
  industry: { type: String, default: '' },
  address: Address,
  website: {type: String, default: ''},
  timezone: {type: String, default: ''},
  logo: {type: String, default: ''},
  accountingFramework: {type: String, default: ''},
  fiscalYearEnd: {type: Date, default: null},
  legalNameChanged: {type: Boolean, default: false},
  previousLegalName: {type: String, default: ''},
  publiclyListed: {type: Boolean, default: false},
  overseasOps: {type: Boolean, default: false},
  parentStakeholders: {type: String, default: ''}
}, {collection: 'businesses'})
