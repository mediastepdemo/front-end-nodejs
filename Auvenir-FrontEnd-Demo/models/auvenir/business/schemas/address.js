const {Schema} = require('mongoose')

module.exports = Schema({
  unit: {type: String, default: ''},
  streetAddress: {type: String, default: ''},
  city: {type: String, default: ''},
  stateProvince: {type: String, default: ''},
  postalCode: {type: String, default: ''},
  country: {type: String, default: ''}
}, {_id: false})
