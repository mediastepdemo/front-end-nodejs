const {Schema} = require('mongoose')

module.exports = Schema({
  id: { type: Schema.ObjectId, ref: 'User' },
  admin: { type: Boolean, required: true }
}, {_id: false})
