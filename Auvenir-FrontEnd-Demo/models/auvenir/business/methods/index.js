const staticMethods = require('./static')

/**
 * add static and instance methods to schema
 * @param {Object} schema mongoose business schema
 * @return {null} no return
 */
module.exports = function (schema) {
  schema.statics = staticMethods
}
