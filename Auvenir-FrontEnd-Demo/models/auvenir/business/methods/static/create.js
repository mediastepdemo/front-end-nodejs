const debug = require('debug')('auvenir:model:business')

/**
 * create a business
 * @param {Object} data clientId, address, business
 * @param {Function} cbk callback(err, Mongoose instance: business)
 * @return {null} no return
 */
module.exports = function (data, cbk) {
  debug('Static Methods: create', data)
  const {acl, address = {}, business = {}} = data

  if (!acl) {
    return cbk({code: 403, msg: 'Missing client acl information'})
  }

  const obj = Object.assign({}, {acl}, {address}, business)

  const newBusiness = new this(obj)

  newBusiness.save((err, business) => {
    debug('business created', business)
    cbk(err, business)
  })
}
