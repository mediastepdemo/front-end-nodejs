const {Schema} = require('mongoose')
const connections = require('../connections')

const consumerSchema = Schema({
  status: { type: String, required: true, enum: ['ACTIVE', 'INACTIVE', 'SETUP'] },
  uid: { type: String, required: true },
  userEmail: { type: String, required: true },
  refreshToken: { type: String, required: true }, // when the user choose the folder by client side
  tokenLastUsed: { type: Date }, // if not being used for 6 months, it expires.
  accessToken: { type: String, required: true },
  tokenExpiry: { type: String, required: true },
  idToken: { type: String, required: true },
  createdBy: { type: String, required: true }, // authID
  selectedFolder: [{ type: String }] // the _id of selectedFolder. If they didn't integrate a folder, it is empty.
}, { collection: 'consumers' })

module.exports = connections.gdrive.model('Consumer', consumerSchema)

// Refresh_Token expiration
// - The user has revoked access.
// - The token has not been used for six months.
// - The user changed passwords and the token contains Gmail scopes.
// - The user account has exceeded a certain number of token requests.
