const {Schema} = require('mongoose')
const connections = require('../connections')

const userSchema = Schema({
  uid: { type: String, required: true }, // alphanumeric created by Auvenir
  userID: { type: Schema.ObjectId, required: true },
  status: { type: String, required: true, enum: ['ACTIVE', 'INACTIVE', 'SETUP']}
}, { collection: 'users' })

module.exports = connections.gdrive.model('User', userSchema)
