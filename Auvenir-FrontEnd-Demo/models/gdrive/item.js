const {Schema} = require('mongoose')
const connections = require('../connections')

const gdriveObjSchema = Schema({
  consumerID: { type: Schema.ObjectId, required: true },
  fileID: { type: Schema.ObjectId }, // not needed for folders
  gdriveID: { type: String, required: true }, // from Google Drive
  parent: { type: String, default: null },    // selected folder _id
  type: { type: String, required: true, enum: ['FILE', 'FOLDER'] }
}, { collection: 'Item' })

module.exports = connections.gdrive.model('Item', gdriveObjSchema)
