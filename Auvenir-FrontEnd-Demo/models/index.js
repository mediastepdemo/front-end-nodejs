/******************************
 * MODELS
 *
 * Maps out all of the Mongoose data models that are being in the system
 * to the corresponding database
 ******************************/

var Models = {
  Auvenir: {
    Activity: require('./auvenir/activity'),
    Business: require('./auvenir/business'),
    Engagement: require('./auvenir/engagement'),
    File: require('./auvenir/file'),
    Firm: require('./auvenir/firm'),
    Template: require('./auvenir/template'),
    User: require('./auvenir/user')
  },
  Gdrive: {
    Item: require('./gdrive/item'),
    User: require('./gdrive/user'),
    Consumer: require('./gdrive/consumer')
  },
  Finicity: {
    User: require('./finicity/user'),
    Account: require('./finicity/account')
  },
  Quickbooks: {
    Account: require('./quickbooks/account'),
    Bank: require('./quickbooks/bank'),
    BankStatement: require('./quickbooks/bankStatement'),
    ChartAccount: require('./quickbooks/chartAccount'),
    Header: require('./quickbooks/header'),
    Ledger: require('./quickbooks/ledger'),
    Line: require('./quickbooks/line'),
    Transaction: require('./quickbooks/transaction'),
    User: require('./quickbooks/user')
  },
  connections: require('./connections')
}

module.exports = Models
