var mongoose = require('mongoose')
var connections = require('../connections')

var lineSchema = mongoose.Schema({
  engagementID: { type: mongoose.Schema.ObjectId, ref: 'Engagement' },
  headerID: { type: mongoose.Schema.ObjectId, ref: 'Header' },
  accountID: { type: mongoose.Schema.ObjectId, ref: 'ChartAccount' },
  lineNumber: { type: Number, default: 0 },
  details: { type: String, default: '' },
  amount: { type: Number, default: 0 },
  postType: { type: String, default: '' }   // 'Debit' or 'Credit'
}, { collection: 'lines' })

module.exports = connections.quickbooks.model('Line', lineSchema)
