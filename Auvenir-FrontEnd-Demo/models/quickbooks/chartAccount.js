var mongoose = require('mongoose')
var connections = require('../connections')

var chartAccountSchema = mongoose.Schema({
  engagementID: { type: mongoose.Schema.ObjectId },
  referenceId: { type: Number, default: null },
  name: { type: String, default: '' },
  nameFull: { type: String, default: '' },   // Available from QBO
  number: { type: String, default: '' },
  classification: { type: String, default: '' },
  category: { type: String, default: '' },
  subCategory: { type: String, default: '' },
  closeBalance: { type: Number, default: 0 },    // Balance at end of fiscal period
  openBalance: { type: Number, default: 0 },    // Balance at start of fiscal period
  currentBalance: { type: Number, default: 0 },    // Excludes child account
  totalBalance: { type: Number, default: 0 },    // Includes Child account.
  currency: { type: String, default: '' },
  parentReferenceId: { type: Number, default: null }, // Parent of this account.
  createTime: { type: Date, default: null }, // Available from QBO
  lastUpdateTime: { type: Date, default: null }, // Available from QBO
  active: { type: Boolean, default: false }
}, { collection: 'chartAccounts' })

module.exports = connections.quickbooks.model('ChartAccount', chartAccountSchema)
