var mongoose = require('mongoose')
var connections = require('../connections')

var bankStatementSchema = mongoose.Schema({
  acl: [],
  engagementID: { type: mongoose.Schema.ObjectId, ref: 'Engagement' },
  fileID: { type: mongoose.Schema.ObjectId },
  bank: String, // code
  accountNumber: String,
  openingBalance: Number,
  closingBalance: Number,
  openingDate: Date,
  closingDate: Date
}, { collection: 'bankstatements' })

module.exports = connections.quickbooks.model('BankStatement', bankStatementSchema)
