var mongoose = require('mongoose')
var connections = require('../connections')

var bankSchema = mongoose.Schema({
  acl: [],
  engagementID: { type: mongoose.Schema.ObjectId, ref: 'Engagement' },
  name: { type: String, default: '' },
  address: { type: String, default: '' },
  city: { type: String, default: '' },
  provinceState: { type: String, default: '' },
  country: { type: String, default: '' },
  keyContact: {
    firstName: { type: String, default: '' },
    lastName: { type: String, default: '' },
    email: { type: String, default: '' },
    phone: { type: String, default: '' },
    contactReq: { type: Boolean, default: false }
  },
  requestPermission: { type: Boolean, default: true }
}, { collection: 'banks' })

module.exports = connections.quickbooks.model('Bank', bankSchema)
