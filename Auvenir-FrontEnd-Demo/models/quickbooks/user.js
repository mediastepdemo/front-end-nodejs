var mongoose = require('mongoose')
var connections = require('../connections')

var userSchema = mongoose.Schema({
  uid: { type: String, unique: true, required: true },
  authID: { type: String, required: true },
  status: { type: String, default: 'ACTIVE', enum: ['ACTIVE', 'INACTIVE'] }
}, { collection: 'users' })

module.exports = connections.quickbooks.model('User', userSchema)
