var mongoose = require('mongoose')
var connections = require('../connections')

var transactionSchema = mongoose.Schema({
  acl: [],
  engagementID: { type: mongoose.Schema.ObjectId, ref: 'Engagement' },
  fileID: { type: mongoose.Schema.ObjectId },
  type: String, // Cheque, Bank Transfer, Debit, GIC, etc
  amount: Number,
  flowType: String, // Deposit, Withdrawal
  dateProcessed: Date,
  description: String,
  sourceType: String,  // BS, GL, R
  reference: { type: mongoose.Schema.ObjectId }, // BankStatement, Ledger, Receipt, etc
  identifier: String // Cheque number, etc.
}, { collection: 'transactions' })

module.exports = connections.quickbooks.model('Transaction', transactionSchema)
