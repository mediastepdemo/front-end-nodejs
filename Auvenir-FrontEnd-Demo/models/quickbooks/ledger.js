var mongoose = require('mongoose')
var connections = require('../connections')

var ledgerSchema = mongoose.Schema({
  source: { type: Object},
  engagementID: { type: mongoose.Schema.ObjectId, ref: 'Engagement' },
  fileID: { type: mongoose.Schema.ObjectId },
  startDate: { type: String, default: '' }, // Use String instead of Date since we don't care about time.
  endDate: { type: String, default: '' }, // Use String instead of Date since we don't care about time.
  income: { type: Number, default: 0 },
  cogs: { type: Number, default: 0 },
  grossProfit: { type: Number, default: 0 },
  expenses: { type: Number, default: 0 },
  netOperatingIncome: { type: Number, default: 0 },
  otherIncome: { type: Number, default: 0 },
  otherExpenses: { type: Number, default: 0 },
  netOtherIncome: { type: Number, default: 0 },
  netIncome: { type: Number, default: 0 },
  totalAssets: { type: Number, default: 0 },
  totalEquity: { type: Number, default: 0 },
  totalLiabilities: { type: Number, default: 0 }   // This includes totalEquity.
}, { collection: 'ledgers' })

module.exports = connections.quickbooks.model('Ledger', ledgerSchema)
