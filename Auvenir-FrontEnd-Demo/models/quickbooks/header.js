var mongoose = require('mongoose')
var connections = require('../connections')

var headerSchema = mongoose.Schema({
  engagementID: { type: mongoose.Schema.ObjectId, ref: 'Engagement' },
  ledgerID: { type: mongoose.Schema.ObjectId, ref: 'Ledger' },
  effectiveDate: { type: Date, default: null },
  postedDate: { type: Date, default: null },
  headerNumber: { type: String, default: '' },
  description: { type: String, default: '' },
  txnType: { type: String, default: '' },
  createdBy: { type: String, default: '' },    // User who created the transaction
  created: { type: Date, default: null },  // Time the transaction was created
  modifiedBy: { type: String, default: '' },    // User who last modified the transaction
  modified: { type: Date, default: null }   // Time the transaction was last modified
}, { collection: 'headers' })

module.exports = connections.quickbooks.model('Header', headerSchema)
