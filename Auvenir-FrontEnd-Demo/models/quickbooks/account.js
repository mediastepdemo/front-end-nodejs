var mongoose = require('mongoose')
var connections = require('../connections')

var accountSchema = mongoose.Schema({
  uid: { type: String, required: true },
  status: { type: String, default: 'ACTIVE', enum: ['ACTIVE', 'INACTIVE'] }
}, { collection: 'accounts' })

module.exports = connections.quickbooks.model('Account', accountSchema)
