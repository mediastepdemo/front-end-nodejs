/******************************
 * CONNECTIONS
 *
 * Connect to all MongoDB Databases and expose the connections for models
 ******************************/
var appConfig = require('../config')
var mongoose = require('mongoose')
var Logger = require(rootDirectory + '/plugins/logger/logger')
const { info, error } = Logger.init(__filename)

var connections = {}
var URI

for (let i = 0; i < appConfig.mongo.dbs.length; i++) {
  URI = appConfig.mongo.URI[appConfig.mongo.dbs[i]]
  connections[appConfig.mongo.dbs[i]] = mongoose.createConnection(URI, appConfig.mongo.options)
  connections[appConfig.mongo.dbs[i]].on('connected', function () {
    info(`Successfully connected to ${appConfig.mongo.URI[appConfig.mongo.dbs[i]]}`)
  })
  connections[appConfig.mongo.dbs[i]].on('error', function (data) {
    error(`Failed to connect to ${appConfig.mongo.URI[appConfig.mongo.dbs[i]]}`, data)
  })
}

module.exports = connections
