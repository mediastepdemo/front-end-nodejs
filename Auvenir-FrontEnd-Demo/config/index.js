/* eslint no-console: 0 */

var convict = require('convict')
var schema = require('./schema')
var request = require('request')
var fs = require('fs')
var _ = require('lodash')
var appConfig = null

function makeServerURL (srv) {
  let protocol = (srv.protocol !== '') ? srv.protocol.toLowerCase() : 'http'
  let port = (srv.port !== '') ? srv.port : '80'
  if ((protocol === 'http' && port === '80') || (protocol === 'https' && port === '443')) {
    return protocol + '://' + srv.domain
  }
  return protocol + '://' + srv.domain + ':' + port
}

try {
  // Set the schema for environment variables
  var config = convict(schema)
  var env = config.get('env')
  var conf = config.get('conf')

  if (conf) {
    // If a configuration file list is specified, used those files instead of the
    // env setting
    conf = conf.split(',')
    if (conf.length === 1) {
      console.log(`Loading configuration from the following file ${conf[0]}`)
    } else {
      console.log(`Loading configuration from the following list of files ${conf}`)
    }
    console.log(`Legacy environment and override settings will be ignored`)
    config.loadFile(conf)
    config.validate({strict: true})
  } else {
    // Load the proper config based on the NODE_ENV environment variables
    console.log(`Setting up ${env} environment variables...`)
    var configFile = `${__dirname}/../override-env.json`
    if (fs.existsSync(configFile)) {
      console.log('Overriding environment variables...')
      console.log(`Loading environment variables from '${configFile}'`)
      config.loadFile(configFile)
    } else {
      configFile = `${__dirname}/${env}.json`
      if (fs.existsSync(configFile)) {
        console.log(`Loading environment variables from '${configFile}'`)
        config.loadFile(configFile)
      } else {
        throw new Error(`override-env.json does not exist`)
      }
    }
    // Perform validation
    config.validate({strict: true})
    console.log(`Environment successfully set to ${env}`)
  }
  // Modify config
  appConfig = config.getProperties()
  let s = appConfig.server
  let ps = appConfig.publicServer
  if (ps.protocol === '') ps.protocol = s.protocol
  if (ps.domain === '') ps.domain = s.domain
  if (ps.port === '') ps.port = s.port
  appConfig.server.url = makeServerURL(appConfig.server)
  appConfig.publicServer.url = makeServerURL(appConfig.publicServer)
  appConfig.marketingServer.url = makeServerURL(appConfig.marketingServer)
  appConfig.server.allowOrigin = _.map(appConfig.server.allowOrigin, (url) => { return url.toLowerCase().trim() })
  appConfig.server.allowOrigin.push(appConfig.marketingServer.url)
  console.log(`Effective application configuration: ${JSON.stringify(appConfig, null, 4)}`)
  // MongoDB Setup
  var mongo = appConfig.mongo
  var dbs = appConfig.mongo.dbs
  var auth = (mongo.username && mongo.password) ? `${mongo.username}:${mongo.password}@` : ''
  var mongo1 = `${mongo.host_one}:${mongo.port_one}`
  appConfig.mongo.URI = {}
  // Set MongoDB URI
  if (mongo.useReplica) {
    // Add the replica set if defined
    var mongo2 = mongo.host_two ? `,${mongo.host_two}:${mongo.port_two}` : ''
    var mongo3 = mongo.host_three ? `,${mongo.host_three}:${mongo.port_three}` : ''
    for (let i = 0; i < dbs.length; i++) {
      appConfig.mongo.URI[dbs[i]] = `mongodb://${auth}${mongo1}${mongo2}${mongo3}/${dbs[i]}?authSource=admin`
    }
    delete appConfig.mongo.options.server
  } else {
    for (let i = 0; i < dbs.length; i++) {
      appConfig.mongo.URI[dbs[i]] = `mongodb://${auth}${mongo1}/${dbs[i]}?authSource=admin`
    }
    delete appConfig.mongo.options.replset
  }
  // New Relic Setup
  // appConfig.integrations.newrelic.app_name = `${appConfig.integrations.newrelic.app_name} - ${env.toUpperCase()}`
} catch (err) {
  console.log('Environment variables did not pass validation', err)
  if (appConfig && env !== 'local') {
    // Post Notification to Slack
    var options = {
      text: `WARNING!!! Missing environment variable in the ${env} environment! ${err}`,
      channel: 'dev-circle'
    }
    request.post(appConfig.integrations.slack.URL, { body: JSON.stringify(options) }, function (err, res, body) {
      if (err) {
        console.log('Failed to notify Slack of configuration error')
      } else {
        if (body === 'ok') {
          console.log('Successfully notified slack of environment variable issue')
        } else {
          console.log('Failed to notify Slack of configuration error')
        }
      }
    })
  }
}

module.exports = appConfig
