var LocalStrategy = require('passport-local').Strategy
var Models = require('../models')
var bCrypt = require('bcrypt-nodejs')
var initLogger = require(rootDirectory + '/plugins/logger/logger').init
const {error} = initLogger(__filename)

module.exports = function (passport) {
  passport.use('signupCPA', new LocalStrategy({
    passReqToCallback: true // allows us to pass back the entire request to the callback
  }, function (req, username, password, done) {
    var findOrCreateCPA = function () {
      Models.Auvenir.User.findOne({ email: username }, function (err, user) {
        if (err) {
          error('Error in CPA Signup: ', {err})
          return done(err)
        }

        if (user) {
          return done(null, false, { message: 'CPA already exists with email.' })
        } else {
          var newBusiness = new Models.Auvenir.Business()
          newBusiness.permissions = []
          newBusiness.type = 'FIRM'
          newBusiness.name = req.body.businessName
          newBusiness.websiteURL = req.body.website

          newBusiness.save(function (err) {
            if (err) {
              error('Error occured during the SAVE operation for the new business', {err})
              return done(err)
            }
            var newUser = new Models.Auvenir.User()
            newUser.businessID = newBusiness._id
            newUser.accountPermissions = [{portal: 'CPA'}]
            newUser.username = username
            newUser.userNameHash = bCrypt.hashSync(username)
            newUser.email = req.body.username
            newUser.originalEmail = req.body.username
            newUser.password = createHash(password)
            newUser.firstName = req.body.firstName
            newUser.lastName = req.body.lastName
            newUser.phone = req.body.phone

            newUser.save(function (err) {
              if (err) {
                error('Error occured during the SAVE operation for the new cpa', {err})
                return done(err)
              }
              newBusiness.update({$set: {permissions: [{ userid: newUser._id, role: 'OWNER' }]}}, function (err, result) {
                if (err) {
                  error('Error occured during the UPDATE operation for the new business', {err})
                  return done(err)
                }
                return done(null, newUser)
              })
            })
          })
        }
      })
    }
    // Delay the execution of findOrCreateUser and execute the method
    // in the next tick of the event loop
    process.nextTick(findOrCreateCPA)
  })
)

  // Generates hash using bCrypt
  var createHash = function (password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null)
  }
}
