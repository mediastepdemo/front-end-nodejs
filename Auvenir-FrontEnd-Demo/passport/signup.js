var LocalStrategy = require('passport-local').Strategy
var Models = require('../models')
var Utility = require('../plugins/utilities/utility')
var Inspector = require('../plugins/inspector/inspector')
var initLogger = require(rootDirectory + '/plugins/logger/logger').init
const { info, error } = initLogger(__filename)

module.exports = function (passport) {
  passport.use('signup', new LocalStrategy({
    passReqToCallback: true // allows us to pass back the entire request to the callback
  }, function (req, username, password, done) {
    var findOrCreateUser = function () {
      Models.Auvenir.User.findOne({ email: username }, function (err, user) {
        if (err) {
          error('Error in Client Signup: ' + err)
          return done(err)
        }

        if (user) {
          info('User already exists with email: ' + username)
          return done(null, false, { message: 'User already exists with email.' })
        } else {
          var inspector = new Inspector()
          inspector.retrieveData(username, function (err, intel) {
            if (err) {
              error('Holmes broke his leg.', {err})
              return done(err)
            }

            var bizData = {
              permissions: [],
              type: 'CLIENT',
              name: intel.company.name,
              websiteURL: intel.company.url,
              phone: intel.company.phone,
              address: intel.company.address,
              unit: intel.company.unit,
              city: intel.company.city,
              country: intel.company.country,
              stateProvince: intel.company.state,
              postalCode: intel.company.postalCode,
              logo: intel.company.logo
            }

            Utility.createBusiness(bizData, function (err, newBusiness) {
              if (err) {
                error('Error occured during the SAVE operation for the new business', {err})
                return done(err)
              }

              var userData = {
                businessID: newBusiness._id,
                accountPermissions: [{portal: 'CLIENT'}],
                username: username,
                email: username,
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                profilePicture: intel.person.avatar[0],
                jobTitle: intel.person.jobPosition
              }

              Utility.createUser(userData, function (err, newUser) {
                if (err) {
                  error('Error occured during the SAVE operation for the new user', {err})
                  return done(err)
                }

                newBusiness.update({$set: {permissions: [{ userid: newUser._id, role: 'OWNER' }]}}, function (err, result) {
                  if (err) {
                    error('Error occured during the UPDATE operation for the new business', {err})
                    return done(err)
                  }
                  var engagementData = { name: 'Engagement Placeholder',
                    firmID: null,
                    clientID: newBusiness._id,
                    status: 'ACTIVE',
                    permissions: [{ userid: newUser._id, access: ['CLIENT'] }] }

                  Utility.createEngagement(engagementData, function (err, newEngagement) {
                    if (err) {
                      error('Error occured during the insert of an engagement', {err})
                      return done(err)
                    }
                    return done(null, newUser)
                  })
                })
              })
            })
          })
        }
      })
    }
    // Delay the execution of findOrCreateUser and execute the method
    // in the next tick of the event loop
    process.nextTick(findOrCreateUser)
  })
  )
}
