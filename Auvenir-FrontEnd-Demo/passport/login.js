/* globals Logger */

var LocalStrategy = require('passport-local').Strategy
var Models = require('../models')
var bCrypt = require('bcrypt-nodejs')
var notp = require('notp')
var initLogger = require(rootDirectory + '/plugins/logger/logger').init
const {info} = initLogger(__filename)

module.exports = function (passport) {
  passport.use('login', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
  },
  function (req, username, password, done) {
    // check in mongo if a user with username exists or not
    Models.Auvenir.User.findOne({ 'email': username, 'accountPermissions.portal': 'CLIENT' }, function (err, user) {
      if (err) {
        return done(err)
      }

      if (!user) {
        info('User "' + username + '" not found.')
        return done(null, false, req.flash('message', 'Incorrect credentials.'))
      }

      if (!isValidPassword(user, password)) {
        info('User "' + username + '" exists, but password was incorrect.')
        incrementFailures(user, 'password')
        return done(null, false, req.flash('message', 'Incorrect credentials.'))
      }

      if (!isValidChallenge(user, req)) {
        info('User "' + username + '" failed 2FA challenge.')
        incrementFailures(user, '2fa')
        return done(null, false, req.flash('message', 'Incorrect credentials.'))
      }

      if (user.status !== 'ACTIVE') {
        user.update({$set: {'status': 'ACTIVE'}}, function (err, result) {
          if (err) return done(err)

          return done(null, user)
        })
      } else {
        return done(null, user)
      }
    })
  })
)

  var isValidPassword = function (user, password) {
    return bCrypt.compareSync(password, user.password)
  }

  var isValidChallenge = function (user, req) {
    if (!user.twoFactorEnabled) {
      return true
    }

    return notp.totp.verify(req.body.challenge, user.twoFactorSecret, { window: 3 })
  }

  var incrementFailures = function (user, type) {
    return
  }
}
