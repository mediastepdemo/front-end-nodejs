/* globals Logger */

var api = require('./api')
var login = require('./login')
var signup = require('./signup')
var signupCPA = require('./signupCPA')
var Models = require('../models')

var initLogger = require(rootDirectory + '/plugins/logger/logger').init
const {info, error} = initLogger(__filename)

module.exports = function (passport) {
  passport.serializeUser(function (user, done) {
    if (user instanceof Models.Auvenir.User) {
      info('serializing user: ')
      done(null, {id: user._id})
    } else {
      error(' * Serializing Error: Unknown mongoose user model used.', user)
      done(null)
    }
  })

  passport.deserializeUser(function (data, done) {
    if (data) {
      Models.Auvenir.User.findById(data.id, function (err, user) {
        if (err) {
          done(err)
        } else {
          done(null, user)
        }
      })
    } else {
      error(' * Deserializing Error: Unknown mongoose user model used. ' + data.type)
      done(null)
    }
  })

  api(passport)
  login(passport)
  signup(passport)
  signupCPA(passport)
}
