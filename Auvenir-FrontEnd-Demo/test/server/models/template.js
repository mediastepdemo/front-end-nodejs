/* globals describe, it, before, after */

var assert = require('chai').assert
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var Template = require('../../../models/template')
var Models = require('../../../models')

describe('Model: Template', function () {
  var x = new Template()

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      done()
    })
  })
  describe('#Check-Keys', function (done) {
    var a = x.toObject()
    var myKeys = Object.keys(a)
    it('check if the keys are the same', function (done) {
      var keys = ['_id', 'sections', 'type', 'name', 'acl']
      assert.sameMembers(myKeys, keys, 'same members')
      done()
    })
  })
  describe('#create', function () {
    describe('#default-values', function () {
      it('acl should be array bt default', function () {
        assert.isArray(x.acl)
      })
      it('Name of template should be "" by default', function () {
        assert.equal(x.name, '')
      })
      it('type should be "" by default', function () {
        assert.isString(x.type)
      })
      after(function (done) {
        Template.remove({name: ''}, function (err) {
          if (err) throw err
          done()
        })
      })
    })

    describe('#custom-values', function () {
      before(function (done) {
        x.name = 'Template1'
        x.sections = ['SEC1']
        x.save(function (err) {
          if (err) throw err
          done()
        })
      })
      it('name should be "Template1"', function () {
        assert.equal(x.name, 'Template1')
        assert.typeOf(x.name, 'String')
      })
      it('sections should be SEC1', function () {
        assert.equal(x.sections, 'SEC1')
      })
    })
  })
  describe('#read', function () {
    it('should read the name of Template from the database', function (done) {
      Models.Auvenir.Template.find({}, {name: 'Template1'}, function (err, oneUser) {
        if (err) throw err
      })
      Models.Auvenir.Template.findOne({}, {sections: 'SEC1'}, function (err, oneUser) {
        if (err) throw err
        done()
      })
    })
  })
  describe('#update', function () {
    before(function (done) {
      x.type = 'TXT'
      x.sections = 'SEC2'

      x.save(function (err) {
        if (err) throw err
        done()
      })
    })
    it('should update the type and section of Template in database', function (done) {
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the Template with sec "TXT"', function (done) {
      Template.remove({type: 'TXT'}, function (err, updatedd) {
        if (err) throw err
        done()
      })
    })
  })
})
