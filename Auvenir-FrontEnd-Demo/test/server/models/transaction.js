/* globals describe, it, before, after */

var assert = require('chai').assert
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var Transaction = require('../../../models/transaction')
var Models = require('../../../models')

describe('Model: Transaction', function () {
  var x = new Transaction()

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      done()
    })
  })
  describe('#Check-Keys', function (done) {
    var a = x.toObject()
    var myKeys = Object.keys(a)
    it('check if the keys are the same', function (done) {
      var keys = ['_id', 'acl']
      assert.sameMembers(myKeys, keys, 'same members')
      done()
    })
  })
  describe('#create', function () {
    describe('#default-values', function () {
      it('EngagementID should be ref to "Engagement"', function () {
        assert.deepProperty({ engagementID: { ref: 'Engagement' } }, 'engagementID.ref')
      })
      it('Type of transaction should be undefined by default', function () {
        assert.isUndefined(x.type)
      })
      it('references should be undefined by default', function () {
        assert.equal(x.reference, undefined)
      })
      after(function (done) {
        Transaction.remove({type: undefined}, function (err) {
          if (err) throw err
          done()
        })
      })
    })

    describe('#custom-values', function () {
      before(function (done) {
        x.type = 'GIC'
        x.amount = '25000'
        x.flowType = 'Withdrawl'
        x.save(function (err) {
          if (err) throw err
          done()
        })
      })
      it('type should be "GIC"', function () {
        assert.equal(x.type, 'GIC')
        assert.typeOf(x.type, 'String')
      })
      it('amount should be 25,000', function () {
        assert.equal(x.amount, '25000')
      })
      it('flowType should be Withdrawl', function () {
        assert.equal(x.flowType, 'Withdrawl')
      })
    })
  })
  describe('#read', function () {
    it('should read the type of Transaction from the database', function (done) {
      Models.Transaction.find({}, {type: 'Task1'}, function (err, oneUser) {
        if (err) throw err
      })
      Models.Transaction.findOne({}, {amount: '25'}, function (err, oneUser) {
        if (err) throw err
        done()
      })
    })
  })
  describe('#update', function () {
    before(function (done) {
      x.type = 'Cheque'
      x.sourceType = 'BS'

      x.save(function (err) {
        if (err) throw err
        done()
      })
    })
    it('should update the type and sourceType of Transaction in database', function (done) {
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the Transaction with type "Cheque"', function (done) {
      Transaction.remove({type: 'Cheque'}, function (err, updatedd) {
        if (err) throw err
        done()
      })
    })
  })
})
