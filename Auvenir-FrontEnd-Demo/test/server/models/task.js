/* globals describe, it, before, after */

var assert = require('chai').assert
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var Task = require('../../../models/task')
var Models = require('../../../models')

describe('Model: Task', function () {
  var x = new Task()

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      done()
    })
  })
  describe('#Check-Keys', function (done) {
    var a = x.toObject()
    var myKeys = Object.keys(a)
    it('check if the keys are the same', function (done) {
      var keys = ['_id', 'dateCreated', 'acl']
      assert.sameMembers(myKeys, keys, 'same members')
      done()
    })
  })
  describe('#create', function () {
    describe('#default-values', function () {
      it('EngagementID should be ref to "Engagement"', function () {
        assert.deepProperty({ engagementID: { ref: 'Engagement' } }, 'engagementID.ref')
      })
      it('Name of task should be undefined by default', function () {
        assert.isUndefined(x.name)
      })
      it('Due Date should be undefined by default', function () {
        assert.equal(x.dateDue, undefined)
      })
      after(function (done) {
        Task.remove({name: undefined}, function (err) {
          if (err) throw err
          done()
        })
      })
    })

    describe('#custom-values', function () {
      before(function (done) {
        x.name = 'Task1'
        x.referenceKey = 'REF11'
        x.dateDue = new Date(2017, 4, 20, 16, 5)
        x.save(function (err) {
          if (err) throw err
          done()
        })
      })
      it('name should be "Task1"', function () {
        assert.equal(x.name, 'Task1')
        assert.typeOf(x.name, 'String')
      })
      it('ReferenceKey should be REF11', function () {
        assert.equal(x.referenceKey, 'REF11')
      })
      it('Due Date should be Sat May 20th 2017', function () {
        assert.equalDate(x.dateDue, new Date(2017, 4, 20, 16, 5))
      })
    })
  })
  describe('#read', function () {
    it('should read the name of Task from the database', function (done) {
      Models.Auvenir.Task.find({}, {name: 'Task1'}, function (err, oneUser) {
        if (err) throw err
      })
      Models.Auvenir.Task.findOne({}, {referenceKey: 'REF11'}, function (err, oneUser) {
        if (err) throw err
        done()
      })
    })
  })
  describe('#update', function () {
    before(function (done) {
      x.type = 'TXT'

      x.save(function (err) {
        if (err) throw err
        done()
      })
    })
    it('should update the type of Task in database', function (done) {
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the Task with type "TXT"', function (done) {
      Task.remove({type: 'TXT'}, function (err, updatedd) {
        if (err) throw err
        done()
      })
    })
  })
})
