/* globals describe, it, before, after */

var assert = require('chai').assert
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var Resource = require('../../../models/resource')
var Models = require('../../../models')

describe('Model: Resource', function () {
  var x = new Resource()

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      done()
    })
  })
  describe('#Check-Keys', function (done) {
    var a = x.toObject()
    var myKeys = Object.keys(a)
    it('check if the keys are the same', function (done) {
      var keys = ['_id', 'dateCreated']
      assert.sameMembers(myKeys, keys, 'same members')
      done()
    })
  })
  describe('#create', function () {
    describe('#default-values', function () {
      it('Type of resource should be undefined by default', function () {
        assert.isUndefined(x.type)
      })
      it('Text should be undefined by default', function () {
        assert.equal(x.text, undefined)
      })
      after(function (done) {
        Resource.remove({type: undefined}, function (err) {
          if (err) throw err
          done()
        })
      })
    })

    describe('#custom-values', function () {
      before(function (done) {
        x.type = 'IMG'
        x.text = 'Resource1'
        x.dateCreated = new Date()
        x.save(function (err) {
          if (err) throw err
          done()
        })
      })
      it('Type should be "IMG"', function () {
        assert.equal(x.type, 'IMG')
        assert.typeOf(x.type, 'String')
      })
      it('Text should be Resource1', function () {
        assert.equal(x.text, 'Resource1')
      })
      it('Date should be Todays Date', function () {
        assert.equalDate(x.dateCreated, new Date())
      })
    })
  })
  describe('#read', function () {
    it('should read the type of Resource from the database', function (done) {
      Models.Resource.find({}, {type: 'IMG'}, function (err, oneUser) {
        if (err) throw err
      })
      Models.Resource.findOne({}, {dateCreated: new Date()}, function (err, oneUser) {
        if (err) throw err
        done()
      })
    })
  })
  describe('#update', function () {
    before(function (done) {
      x.type = 'TXT'

      x.save(function (err) {
        if (err) throw err
        done()
      })
    })
    it('should update the type of Resource in database', function (done) {
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the Resource with type "TXT"', function (done) {
      Resource.remove({type: 'TXT'}, function (err, updatedd) {
        if (err) throw err
        done()
      })
    })
  })
})
