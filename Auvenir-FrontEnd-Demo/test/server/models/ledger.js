/* globals describe, it, before, after */

var assert = require('chai').assert
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var Ledger = require('../../../models/ledger')
var Models = require('../../../models')

describe('Model: Ledger', function () {
  var x = new Ledger()

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      done()
    })
  })
  describe('#Check-Keys', function (done) {
    var a = x.toObject()
    var myKeys = Object.keys(a)
    it('check if the keys are the same', function (done) {
      var keys = ['_id', 'totalLiabilities', 'totalEquity', 'totalAssets', 'netIncome', 'netOtherIncome', 'otherExpenses',
        'otherIncome', 'netOperatingIncome', 'expenses', 'grossProfit', 'cogs', 'income', 'endDate', 'startDate']
      assert.sameMembers(myKeys, keys, 'same members')
      done()
    })
  })
  describe('#create', function () {
    describe('#default-values', function () {
      it('Source should be Type of Object', function () {
        assert.equal(x.source, undefined)
      })
      it('StartDate should be "" by default', function () {
        assert.equal(x.startDate, '')
      })
      it('Expenses should be 0 by default', function () {
        assert.isUndefined(x.epenses)
      })
      after(function (done) {
        Ledger.remove({source: undefined}, function (err) {
          if (err) throw err
          done()
        })
      })
    })

    describe('#custom-values', function () {
      before(function (done) {
        x.source = 'Source'
        x.startDate = '22/01/2016'
        x.expenses = '3000'

        x.save(function (err) {
          if (err) throw err
          done()
        })
      })
      it('Source should be "Source" date', function () {
        assert.equal(x.source, 'Source')
      })
      it('Expenses should be "3000"', function () {
        assert.equal(x.expenses, '3000')
        assert.isNumber(x.expenses)
      })
      it('Start Date should be Todays Date', function () {
        assert.equal(x.startDate, '22/01/2016')
        assert.typeOf(x.startDate, 'String')
      })
      it('EnagagementID should be ref to "Engagement"', function () {
        assert.deepProperty({ engagementID: { ref: 'Engagement' } }, 'engagementID.ref')
      })
    })
  })
  describe('#read', function () {
    it('should read the source from the database', function (done) {
      Models.Ledger.find({}, {source: 1}, function (err, oneUser) {
        if (err) throw err
        done()
      })
    })
    it('should read the fileID from the database', function (done) {
      Models.Ledger.find({}, {fileID: 1}, function (err, oneUser) {
        if (err) throw err
        done()
      })
    })
  })
  describe('#update', function () {
    before(function (done) {
      x.income = '5000'

      x.save(function (err) {
        if (err) throw err
        done()
      })
    })
    it('should update the Ledger in database', function (done) {
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the Ledger of source "Source"', function (done) {
      Ledger.remove({source: 'Source'}, function (err, updatedd) {
        if (err) throw err
        done()
      })
    })
  })
})
