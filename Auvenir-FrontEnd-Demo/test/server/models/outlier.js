/* globals describe, it, before, after */

var assert = require('chai').assert
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var Outlier = require('../../../models/outlier')
var Models = require('../../../models')

describe('Model: Outlier', function () {
  var x = new Outlier()

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      done()
    })
  })
  describe('#Check-Keys', function (done) {
    var a = x.toObject()
    var myKeys = Object.keys(a)
    it('check if the keys are the same', function (done) {
      var keys = ['_id', 'evidence', 'references', 'acl']
      assert.sameMembers(myKeys, keys, 'same members')
      done()
    })
  })
  describe('#create', function () {
    describe('#default-values', function () {
      it('ReferenceKey should be undefiend default', function () {
        assert.isUndefined(x.referenceKey)
      })
      it('References should be empty array of object by default', function () {
        assert.isArray(x.references, {})
      })
      after(function (done) {
        Outlier.remove({referenceKey: undefined}, function (err) {
          if (err) throw err
          done()
        })
      })
    })

    describe('#custom-values', function () {
      before(function (done) {
        x.referenceKey = 'REFF23'
        x.severity = 'Medium'
        var references = {
          type: 'ledger', objID: '123asb'
        }
        x.references = references

        x.save(function (err) {
          if (err) throw err
          done()
        })
      })
      it('EngagementID should be ref to "Engagement"', function () {
        assert.deepProperty({ engagementID: { ref: 'Engagement' } }, 'engagementID.ref')
      })
      it('ReferenceKey should be REFF23', function () {
        assert.equal(x.referenceKey, 'REFF23')
        assert.typeOf(x.referenceKey, 'String')
      })
      it('Severity should be "Medium"', function () {
        assert.equal(x.severity, 'Medium')
      })
      it('References should be type "ledger" and objID"123asb"', function () {
        assert.deepEqual({ type: 'ledger', objID: '123asb' }, { type: 'ledger', objID: '123asb' })
      })
    })
  })
  describe('#read', function () {
    it('should read the Severity of Outlier from the database', function (done) {
      Models.Auvenir.Outlier.find({}, {severity: 'Medium'}, function (err, oneUser) {
        if (err) throw err
      })
      Models.Auvenir.Outlier.findOne({}, {referenceKey: 'REFF23'}, function (err, oneUser) {
        if (err) throw err
        done()
      })
    })
  })
  describe('#update', function () {
    before(function (done) {
      x.severity = 'Low'
      x.referenceKey = 'SSEC12'
      x.references = {
        type: 'ledger', objID: '123asb'
      }

      x.save(function (err) {
        if (err) throw err
        done()
      })
    })
    it('should update the Outlier referenceKey and references and severity in database', function (done) {
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the Outlier with referenceKey "SSEC12"', function (done) {
      Outlier.remove({referenceKey: 'SSEC12'}, function (err, updatedd) {
        if (err) throw err
        done()
      })
    })
  })
})
