/* globals describe, it, before, after */

var assert = require('chai').assert
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var Messages = require('../../../models/messages')
var Models = require('../../../models')

describe('Model: Messages', function () {
  var x = new Messages()

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      done()
    })
  })
  describe('#Check-Keys', function (done) {
    var a = x.toObject()
    var myKeys = Object.keys(a)
    it('check if the keys are the same', function (done) {
      var keys = ['_id', 'content', 'dateCreated', 'attachments', 'status']
      assert.sameMembers(myKeys, keys, 'same members')
      done()
    })
  })
  describe('#create', function () {
    describe('#default-values', function () {
      it('From should be undefined by default', function () {
        assert.isUndefined(x.from)
      })
      it('Date created should be todays date by default', function () {
        assert.equalDate(x.dateCreated, new Date())
      })
      after(function (done) {
        Messages.remove({sender: undefined}, function (err) {
          if (err) throw err
          done()
        })
      })
    })

    describe('#custom-values', function () {
      before(function (done) {
        x.dateCreated = new Date()
        x.content = 'Test'
        x.subject = 'Test Cases'

        x.save(function (err) {
          if (err) throw err
          done()
        })
      })
      it('DateCreated should be Todays date', function () {
        assert.equalDate(x.dateCreated, new Date())
        assert.typeOf(x.dateCreated, 'Date')
      })
      it('Content should be "Test"', function () {
        assert.equal(x.content, 'Test')
      })
      it('Subject should be "Test Cases"', function () {
        assert.equal(x.subject, 'Test Cases')
      })
    })
  })
  describe('#read', function () {
    it('should read the content of messages from the database', function (done) {
      Models.Auvenir.Message.find({}, {content: 'Test'}, function (err, oneUser) {
        if (err) throw err
      })
      Models.Auvenir.Message.findOne({}, {subject: 'Test Cases'}, function (err, oneUser) {
        if (err) throw err
        done()
      })
    })
  })
  describe('#update', function () {
    before(function (done) {
      x.dateViewed = new Date(2017, 4, 20, 16, 5)
      x.status = 'SENDING'

      x.save(function (err) {
        if (err) throw err
        done()
      })
    })
    it('should update the Messages info in database', function (done) {
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the Messages with senders info', function (done) {
      Messages.remove({subject: 'Test Cases'}, function (err, updatedd) {
        if (err) throw err
        done()
      })
    })
  })
})
