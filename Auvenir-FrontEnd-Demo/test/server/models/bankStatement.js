/* globals describe, it, before, after */

var assert = require('chai').assert
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var BankStatement = require('../../../models/bankStatement')
var Models = require('../../../models')

describe('Model: BankStatement', function () {
  var x = new BankStatement()

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      done()
    })
  })
  describe('#Check-Keys', function (done) {
    var a = x.toObject()
    var myKeys = Object.keys(a)
    it('check if the keys are the same', function (done) {
      var keys = ['_id', 'acl']
      assert.sameMembers(myKeys, keys, 'same members')
      done()
    })
  })
  describe('#create', function () {
    describe('#default-values', function () {
      it('EngagementID should be undefined by default', function () {
        assert.equal(x.engagementID, undefined)
      })
      it('Bank should be "" by default', function () {
        assert.equal(x.bank, undefined)
      })
      it('Account Number should be "" by default', function () {
        assert.equal(x.accountNumber, undefined)
      })
      it('openingBalance should be undefined by default', function () {
        assert.equal(x.openingBalance, undefined)
      })
      it('Opening Date should be undefined by default', function () {
        assert.equal(x.openingDate, undefined)
      })
      after(function (done) {
        BankStatement.remove({bank: ''}, function (err) {
          if (err) throw err
          done()
        })
      })
    })

    describe('#custom-values', function () {
      before(function (done) {
        x.bank = 'Scotia'
        x.accountNumber = '1234'
        x.openingBalance = '230.00'
        x.openingDate = new Date()

        x.save(function (err) {
          if (err) throw err
          done()
        })
      })
      it('Bank should be "Scotia"', function () {
        assert.equal(x.bank, 'Scotia')
        assert.typeOf(x.bank, 'string')
      })
      it('Account Number should be "1234"', function () {
        assert.equal(x.accountNumber, '1234')
        assert.typeOf(x.accountNumber, 'string')
      })
      it('openingBalance should be "$230.00" String', function () {
        assert.equal(x.openingBalance, '230.00')
      })
      it('Opening Date should be Todays date', function () {
        assert.equalDate(x.openingDate, new Date())
        assert.typeOf(x.openingDate, 'Date')
      })
    })
  })
  describe('#read', function () {
    it('should read the Bank Name SCOTIA from the database', function (done) {
      Models.BankStatement.find({}, {name: 'Scotia'}, function (err, oneUser) {
        if (err) throw err
        done()
      })
    })
    it('should read the Opening Balance from the database', function (done) {
      Models.BankStatement.findOne({}, {openingBalance: 1}, function (err, oneUs) {
        if (err) throw err
        done()
      })
    })
  })
  describe('#update', function () {
    before(function (done) {
      x.bank = 'BMO'
      x.accountNumber = '12345'
      x.closingDate = new Date(2017, 4, 20, 16, 5)
      x.save(function (err) {
        if (err) throw err
        done()
      })
    })
    it('should update the Bank Statement info in database of the user', function (done) {
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the Bank Statement of name BMO', function (done) {
      BankStatement.remove({bank: 'BMO'}, function (err, updatedd) {
        if (err) throw err
        done()
      })
    })
  })
  after(function (done) {
    mongoose.disconnect(function () {
      done()
    })
  })
})
