/* globals describe, it, before, after */

var assert = require('chai').assert
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var Conversation = require('../../../models/conversation')
var Models = require('../../../models')

describe('Model: Conversation', function () {
  var x = new Conversation()

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      done()
    })
  })
  describe('#Check-Keys', function (done) {
    var a = x.toObject()
    var myKeys = Object.keys(a)
    it('check if the keys are the same', function (done) {
      var keys = ['_id', 'members', 'title', 'dateCreated', 'lastUpdated']
      assert.sameMembers(myKeys, keys, 'same members')
      done()
    })
  })
  describe('#create', function () {
    describe('#default-values', function () {
      it('DateCreated should be todays date by default', function () {
        assert.equalDate(x.dateCreated, new Date())
      })
      it('People should be ref to "Engagement"', function () {
        assert.deepProperty({ people: { ref: 'Engagement' } }, 'people.ref')
      })
      after(function (done) {
        Conversation.remove({dateCreated: new Date()}, function (err) {
          if (err) throw err
          done()
        })
      })
    })

    describe('#custom-values', function () {
      before(function (done) {
        x.dateCreated = new Date()
        x.title = 'Test'
        x.save(function (err) {
          if (err) throw err
          done()
        })
      })
      it('DateCreated should be Todays date', function () {
        assert.equalDate(x.dateCreated, new Date())
        assert.typeOf(x.dateCreated, 'Date')
      })
      it('Title should be Test', function () {
        assert.equal(x.title, 'Test')
      })
    })
  })
  describe('#read', function () {
    it('should read the people from the database', function (done) {
      Models.Auvenir.Conversation.find({}, {people: []}, function (err, oneUser) {
        if (err) throw err
        done()
      })
    })
  })
  describe('#update', function () {
    before(function (done) {
      x.dateCreated = new Date(2017, 4, 20, 16, 5)

      x.save(function (err) {
        if (err) throw err
        done()
      })
    })
    it('should update the Conversation of the User in database', function (done) {
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the Conversation of dateCreated', function (done) {
      Conversation.remove({dateCreated: new Date(2017, 4, 20, 16, 5)}, function (err, updatedd) {
        if (err) throw err
        done()
      })
    })
  })
})
