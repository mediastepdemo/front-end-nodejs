/* globals describe, it, before, after */

var assert = require('chai').assert
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var Mapping = require('../../../models/mapping')
var Models = require('../../../models')

describe('Model: Mapping', function () {
  var x = new Mapping()

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      done()
    })
  })
  describe('#Check-Keys', function (done) {
    var a = x.toObject()
    var myKeys = Object.keys(a)
    it('check if the keys are the same', function (done) {
      var keys = ['_id', 'field', 'model', 'fieldKey', 'key']
      assert.sameMembers(myKeys, keys, 'same members')
      done()
    })
  })
  describe('#create', function () {
    describe('#default-values', function () {
      it('Key should be "" by default', function () {
        assert.isString(x.key, '')
      })
      it('Model should be "" by default', function () {
        assert.equal(x.model, '')
      })
      after(function (done) {
        Mapping.remove({key: ''}, function (err) {
          if (err) throw err
          done()
        })
      })
    })

    describe('#custom-values', function () {
      before(function (done) {
        x.fieldKey = '123'
        x.field = 'field1'

        x.save(function (err) {
          if (err) throw err
          done()
        })
      })
      it('Field Key should be "123"', function () {
        assert.equal(x.fieldKey, '123')
        assert.isString(x.fieldKey, 'String')
      })
      it('Field should be of name "field1"', function () {
        assert.equal(x.field, 'field1')
      })
    })
  })
  describe('#read', function () {
    it('should read the people from the database', function (done) {
      Models.Mapping.find({}, {fieldKey: '123'}, function (err, oneUser) {
        if (err) throw err
        done()
      })
    })
  })
  describe('#update', function () {
    before(function (done) {
      x.key = 'abc'

      x.save(function (err) {
        if (err) throw err
        done()
      })
    })
    it('should update the Mapping of the key in database', function (done) {
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the Mapping of key "abc"', function (done) {
      Mapping.remove({key: 'abc'}, function (err, updatedd) {
        if (err) throw err
        done()
      })
    })
  })
})
