/* globals describe, it, before, after */

var assert = require('chai').assert
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var Bank = require('../../../models/bank')
var Models = require('../../../models')

describe('Model: Bank', function () {
  var x = new Bank()

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      done()
    })
  })
  describe('#Check-Keys', function (done) {
    var a = x.toObject()
    var myKeys = Object.keys(a)
    it('check if the keys are the same', function (done) {
      var keys = ['_id', 'requestPermission', 'keyContact', 'country', 'provinceState', 'city', 'address', 'name', 'acl']
      assert.sameMembers(myKeys, keys, 'same members')
      done()
    })
  })
  describe('#create', function () {
    describe('#default-values', function () {
      it('EngagementID should be undefined by default', function () {
        assert.equal(x.engagementID, undefined)
      })
      it('Bank Name should be "" by default', function () {
        assert.equal(x.name, '')
        assert.typeOf(x.name, 'String')
      })
      it('City name should be "" by default', function () {
        assert.equal(x.city, '')
        assert.typeOf(x.city, 'String')
      })
      it('KeyContact firstName should be "" by default', function () {
        assert.equal(x.keyContact.firstName, '')
        assert.typeOf(x.keyContact.firstName, 'String')
      })
      it('RequestPermission should be true by default', function () {
        assert.equal(x.requestPermission, true)
        assert.typeOf(x.requestPermission, 'Boolean')
      })
      after(function (done) {
        Bank.remove({name: ''}, function (err) {
          if (err) throw err
          done()
        })
      })
    })

    describe('#custom-values', function () {
      before(function (done) {
        x.name = 'Scotia'
        x.city = 'Toronto'
        x.country = 'Canada'
        x.keyContact.firstName = 'Test'

        x.save(function (err) {
          if (err) throw err
          done()
        })
      })
      it('Name of Bank should be "Scotia"', function () {
        assert.equal(x.name, 'Scotia')
        assert.typeOf(x.name, 'string')
      })
      it('City should be "Toronto"', function () {
        assert.equal(x.city, 'Toronto')
      })
      it('Country should be typeof String', function () {
        assert.typeOf(x.country, 'string')
      })
      it('KeyContact firstName should be "Test"', function () {
        assert.equal(x.keyContact.firstName, 'Test')
      })
    })
  })
  describe('#read', function () {
    it('should read the Bank Name SCOTIA from the database', function (done) {
      Models.Bank.find({}, {name: 'Scotia'}, function (err, oneUser) {
        if (err) throw err
        // console.log(oneUser);
        done()
      })
    })
    it('should read the RequestPermission True from the database', function (done) {
      Models.Bank.findOne({}, {requestPermission: 1}, function (err, oneUs) {
        if (err) throw err
        // console.log(oneUs);
        done()
      })
    })
  })
  describe('#update', function () {
    before(function (done) {
      x.name = 'BMO'
      x.requestPermission = false
      x.keyContact.firstName = 'Test10'
      x.save(function (err) {
        if (err) throw err
        done()
      })
    })
    it('should update the Bank info in database of the user', function (done) {
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the Bank of name BMO', function (done) {
      Bank.remove({name: 'BMO'}, function (err, updatedd) {
        if (err) throw err
        done()
      })
    })
  })
  after(function (done) {
    mongoose.disconnect(function () {
      done()
    })
  })
})
