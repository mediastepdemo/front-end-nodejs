/* globals describe, it, before, after */

var assert = require('chai').assert
var expect = require('chai').expect
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var Engagement = require('../../../models/engagement')
var Models = require('../../../models')

describe('Models: Engagement', function () {
  var x = new Engagement()

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      done()
    })
  })
  describe('#Check-Keys', function (done) {
    var a = x.toObject()
    var myKeys = Object.keys(a)
    it('check if the keys are the same', function (done) {
      var keys = ['_id', 'dateCompleted', 'dateSigned', 'reportReady', 'filesSubmitted', 'analysisComplete',
        'auditStarted', 'lastUpdated', 'dueDate', 'dateCreated', 'status', 'name', 'sections', 'type', 'acl']
      assert.sameMembers(myKeys, keys, 'same members')
      done()
    })
  })
  describe('#create', function () {
    describe('#default-values', function () {
      it('The type should be "Audit" by default', function () {
        assert.equal(x.type, 'Audit')
        assert.typeOf(x.type, 'String')
      })
      it('Engagement name should be ""', function () {
        assert.equal(x.name, '')
        assert.typeOf(x.name, 'String')
      })
      it('Status should be "SET-REQUESTS"', function () {
        assert.equal(x.status, 'SET-REQUESTS')
        assert.typeOf(x.status, 'String')
      })
      it('Date created should be Todays Date', function () {
        assert.equalDate(x.dateCreated, new Date())
        assert.typeOf(x.dateCreated, 'date')
      })
      it('Due Date should be null', function () {
        expect(x.dueDate).to.be.equal(null)
      })
      it('Last Updated Date should be null', function () {
        assert.equal(x.lastUpdated, null)
      })
      it('Audit Started Date should be null', function () {
        assert.equal(x.auditStarted, null)
      })
      it('Analysis Complete Date should be null', function () {
        assert.equal(x.analysisComplete, null)
      })
      it('Files Submitted on Date should be null', function () {
        assert.equal(x.filesSubmitted, null)
      })
      it('Report Ready on Date should be null', function () {
        assert.equal(x.reportReady, null)
      })
      it('Date Signedshould be null', function () {
        assert.equal(x.dateSigned, null)
      })
      it('Date Completed should be null', function () {
        assert.equal(x.dateCompleted, null)
      })
      after(function (done) {
        Engagement.remove({name: ''}, function (err) {
          if (err) throw err
          done()
        })
      })
    })

    describe('#custom-values', function () {
      before(function (done) {
        x.name = 'Test10'
        x.type = 'Unknown'
        x.status = 'INVITE-CLIENT'
        x.dueDate = new Date(2017, 4, 20, 16, 5)
        x.auditStarted = new Date(2017, 4, 30, 5)
        x.save(function (err) {
          if (err) throw err
          done()
        })
      })
      it('The type should be "Unknown" by Custom', function () {
        assert.equal(x.type, 'Unknown')
        assert.typeOf(x.type, 'string')
      })
      it('Engagement name should be "Test10"', function () {
        assert.equal(x.name, 'Test10')
        assert.typeOf(x.name, 'string')
      })
      it('Status should be "INVITE-CLIENT"', function () {
        assert.equal(x.status, 'INVITE-CLIENT')
        assert.typeOf(x.status, 'string')
      })
      it('Due Date should be "Sat May 20th 2017"', function () {
        assert.equalDate(x.dueDate, new Date(2017, 4, 20, 16, 5))
        assert.typeOf(x.dueDate, 'date')
      })
      it('Audit Started Date should be "Sat May 30th 2017"', function () {
        assert.equalDate(x.auditStarted, new Date(2017, 4, 30, 16, 5))
        assert.typeOf(x.auditStarted, 'date')
      })
    })
  })
  describe('#read', function () {
    it('should read the Engagement Name Test10 from the database', function (done) {
      Models.Auvenir.Engagement.find({}, {name: 'Test10'}, function (err, oneUser) {
        if (err) throw err
        done()
      })
    })
    it('should read the Engagement Due Date from the database', function (done) {
      Models.Auvenir.Engagement.findOne({}, {dueDate: 1}, function (err, oneUs) {
        if (err) throw err
        done()
      })
    })
    it('should read the Engagement Status from the database', function (done) {
      Models.Auvenir.Engagement.find({}, {status: 'INVITE-CLIENT'}, function (err, oneUs) {
        if (err) throw err
        done()
      })
    })
  })
  describe('#update', function () {
    before(function (done) {
      x.name = 'Shivani'
      x.status = 'SET-SCHEDULE'
      x.dateCreated = new Date(2015, 4, 10, 16, 5)
      x.save(function (err) {
        if (err) throw err
        done()
      })
    })
    it('should update the name and status of the engagement', function (done) {
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the engagement of name Shivani', function (done) {
      Engagement.remove({name: 'Shivani'}, function (err, updatedd) {
        if (err) throw err
        done()
      })
    })
  })
  after(function (done) {
    mongoose.disconnect(function () {
      done()
    })
  })
})
