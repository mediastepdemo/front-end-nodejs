/* globals describe, it, before, after */

var assert = require('chai').assert
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var File = require('../../../models/file')
var Models = require('../../../models')

describe('Model: File', function () {
  var x = new File()

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      done()
    })
  })
  describe('#Check-Keys', function (done) {
    var a = x.toObject()
    var myKeys = Object.keys(a)
    it('check if the keys are the same', function (done) {
      var keys = ['_id', 'path', 'size', 'dateUploaded', 'name', 'referenceKey', 'acl', 'sortedBy']
      assert.sameMembers(myKeys, keys, 'same members')
      done()
    })
  })
  describe('#create', function () {
    describe('#default-values', function () {
      it('File Name should be "" by default', function () {
        assert.equal(x.name, '')
        assert.typeOf(x.name, 'String')
      })
      it('Engagement ID should be "String" by default', function () {
        assert.equal(x.engagementID, undefined)
      })
      it('Reference key be "String" by default', function () {
        assert.equal(x.referenceKey, '')
        assert.typeOf(x.referenceKey, 'String')
      })
      it('Size of file should be ""', function () {
        assert.equal(x.size, '')
      })
      it('Date uploaded should be Todays Date', function () {
        assert.equalDate(x.dateUploaded, new Date())
        assert.typeOf(x.dateUploaded, 'date')
      })
      after(function (done) {
        File.remove({name: ''}, function (err) {
          if (err) throw err
          done()
        })
      })
    })

    describe('#custom-values', function () {
      before(function (done) {
        x.name = 'Test'
        x.referenceKey = 'REF1'
        x.size = '25KB'
        x.dateUploaded = new Date(2017, 4, 20, 16, 5)
        x.save(function (err) {
          if (err) throw err
          done()
        })
      })
      it('File Name should be "Test"', function () {
        assert.equal(x.name, 'Test')
        assert.typeOf(x.name, 'String')
      })
      it('Reference Key should be "REF1"', function () {
        assert.equal(x.referenceKey, 'REF1')
        assert.typeOf(x.referenceKey, 'String')
      })
      it('Size of file should be "25KB"', function () {
        assert.equal(x.size, '25KB')
        assert.typeOf(x.size, 'String')
      })
      it('Date Uploaded should be "Sat May 20th 2017"', function () {
        assert.equalDate(x.dateUploaded, new Date(2017, 4, 20, 16, 5))
        assert.typeOf(x.dateUploaded, 'Date')
      })
    })
  })
  describe('#read', function () {
    it('should read the File Name from the database', function (done) {
      Models.Auvenir.File.find({}, {name: 'Test'}, function (err, oneUser) {
        if (err) throw err
        done()
      })
    })
    it('should read the File Size from the database', function (done) {
      Models.Auvenir.File.findOne({}, {size: 1}, function (err, oneUs) {
        if (err) throw err
        done()
      })
    })
    it('should read the Engagement ID  from the database', function (done) {
      Models.Auvenir.File.findOne({}, {engagementID: 1}, function (err, oneUs) {
        if (err) throw err
        done()
      })
    })
  })
  describe('#update', function () {
    before(function (done) {
      x.name = 'Files'
      x.size = '500KB'
      x.referenceKey = 'REF20'
      x.dateUploaded = new Date(2017, 4, 30, 16, 5)
      x.save(function (err) {
        if (err) throw err
        done()
      })
    })
    it('should update the Files in Database', function (done) {
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the File of name Files', function (done) {
      File.remove({name: 'Files'}, function (err, updatedd) {
        if (err) throw err
        done()
      })
    })
  })
  after(function (done) {
    mongoose.disconnect(function () {
      done()
    })
  })
})
