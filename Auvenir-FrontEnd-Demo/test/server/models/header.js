/* globals describe, it, before, after */

var assert = require('chai').assert
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var Header = require('../../../models/header')
var Models = require('../../../models')

describe('Model: Header', function () {
  var x = new Header()

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      done()
    })
  })
  describe('#Check-Keys', function (done) {
    var a = x.toObject()
    var myKeys = Object.keys(a)
    it('check if the keys are the same', function (done) {
      var keys = ['_id', 'modified', 'modifiedBy', 'created', 'createdBy', 'txnType', 'description', 'headerNumber', 'postedDate', 'effectiveDate']
      assert.sameMembers(myKeys, keys, 'same members')
      done()
    })
  })
  describe('#create', function () {
    describe('#default-values', function () {
      it('Created on Date should be null by default', function () {
        assert.isNull(x.created)
      })
      it('Header Number should be "" by default', function () {
        assert.isString(x.headerNumber, '')
      })
      after(function (done) {
        Header.remove({headerNumber: ''}, function (err) {
          if (err) throw err
          done()
        })
      })
    })

    describe('#custom-values', function () {
      before(function (done) {
        x.headerNumber = '10'
        x.save(function (err) {
          if (err) throw err
          done()
        })
      })
      it('DateCreated should be Todays date', function () {
        assert.equal(x.headerNumber, '10')
        assert.isString(x.headerNumber, 'String')
      })
      it('LedgerId should be ref to "Ledger"', function () {
        assert.deepProperty({ ledgerID: { ref: 'Ledger' } }, 'ledgerID.ref')
      })
    })
  })
  describe('#read', function () {
    it('should read the LedgerId from the database', function (done) {
      Models.Header.find({}, {headerNumber: 1}, function (err, oneUser) {
        if (err) throw err
        done()
      })
    })
  })
  describe('#update', function () {
    before(function (done) {
      x.headerNumber = '50'

      x.save(function (err) {
        if (err) throw err
        done()
      })
    })
    it('should update the Header in database', function (done) {
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the Conversation of dateCreated', function (done) {
      Header.remove({headerNumber: '50'}, function (err, updatedd) {
        if (err) throw err
        done()
      })
    })
  })
})
