/* globals describe, it, before, after */

var assert = require('chai').assert
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var BusinessInfo = require('../../../models/businessInfo')
var Models = require('../../../models')

describe('Model: BusinessInfo', function () {
  var x = new BusinessInfo()

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      done()
    })
  })
  describe('#Check-Keys', function (done) {
    var a = x.toObject()
    var myKeys = Object.keys(a)
    it('check if the keys are the same', function (done) {
      var keys = ['_id', 'addresses', 'acl']
      assert.sameMembers(myKeys, keys, 'same members')
      done()
    })
  })
  describe('#create', function () {
    describe('#default-values', function () {
      it('Name of BusinessInfo should be undefined by default', function () {
        assert.equal(x.name, undefined)
      })
      it('Addresses should be "" by default', function () {
        assert.equal(x.addresses, '')
      })
      it('fiscalYearEnd should be undefined by default', function () {
        assert.equal(x.fiscalYearEnd, undefined)
      })
      it('integration quickbook token should be undefined', function () {
        assert.equal(x.integration.quickbooks.token, undefined)
      })
      after(function (done) {
        BusinessInfo.remove({name: ''}, function (err) {
          if (err) throw err
          done()
        })
      })
    })

    describe('#custom-values', function () {
      before(function (done) {
        x.name = 'Test'
        x.phone = '000-000-0000'
        x.fiscalYearEnd = new Date(2017, 4, 20, 16, 5)
        x.integration.quickbooks.multiCurrency = 'true'
        x.save(function (err) {
          if (err) throw err
          done()
        })
      })
      it('Name of BusinessInfo should be "Test"', function () {
        assert.equal(x.name, 'Test')
        assert.typeOf(x.name, 'String')
      })
      it('Phone should be "000-000-0000"', function () {
        assert.equal(x.phone, '000-000-0000')
        assert.typeOf(x.phone, 'String')
      })
      it('FiscalYearEnd should be "Sat May 20th 2017"', function () {
        assert.equal(x.fiscalYearEnd, new Date(2017, 4, 20, 16, 5))
      })
      it('integration.quickbooks.multiCurrency should be "True"', function () {
        assert.equal(x.integration.quickbooks.multiCurrency, true)
        assert.typeOf(x.integration.quickbooks.multiCurrency, 'Boolean')
      })
    })
  })
  describe('#read', function () {
    it('should read the BusinessInfo Name from the database', function (done) {
      Models.Auvenir.BusinessInfo.find({}, {name: 'Test'}, function (err, oneUser) {
        if (err) throw err
        done()
      })
    })
    it('should read the BusinessInfo integration.quickbooks.token from the database', function (done) {
      Models.Auvenir.BusinessInfo.findOne({}, {'_id': 1}, function (err, oneUs) {
        if (err) throw err
        done()
      })
    })
  })
  describe('#update', function () {
    before(function (done) {
      x.name = 'Test1'
      x.integration.quickbooks.token = 'abc123'
      x.addresses = '52 George St'
      x.country = 'Canada'
      x.save(function (err) {
        if (err) throw err
        done()
      })
    })
    it('should update the database of BusinessInfo', function (done) {
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the BusinessInfo of name Test1', function (done) {
      BusinessInfo.remove({name: 'Test1'}, function (err, updatedd) {
        if (err) throw err
        done()
      })
    })
  })
  after(function (done) {
    mongoose.disconnect(function () {
      done()
    })
  })
})
