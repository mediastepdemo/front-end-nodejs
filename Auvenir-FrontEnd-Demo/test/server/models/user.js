/* globals describe, it, before, after */

var assert = require('chai').assert
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var User = require('../../../models/user')
var Models = require('../../../models')

describe('Model: User', function () {
  var x = new User()

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      done()
    })
  })
  describe('#Check-Keys', function (done) {
    var a = x.toObject()
    var myKeys = Object.keys(a)
    it('check if the keys are the same', function (done) {
      var keys = ['_id', 'auth', 'timeZone', 'language', 'verified', 'currentSubsection', 'currentSection', 'status', 'lastLogin', 'dateCreated',
        'profilePicture', 'jobTitle', 'phone', 'lastName', 'firstName', 'email', 'acl']
      assert.sameMembers(myKeys, keys, 'same members')
      done()
    })
  })
  describe('#create', function () {
    describe('#default-values', function () {
      it('Email should be "" by default', function () {
        assert.equal(x.email, '')
        assert.typeOf(x.email, 'String')
      })
      it('FirstName should be "" by default', function () {
        assert.equal(x.firstName, '')
        assert.typeOf(x.firstName, 'String')
      })
      it('Phone should be "" by default', function () {
        assert.equal(x.phone, '')
        assert.typeOf(x.phone, 'String')
      })
      it('Date created should be Todays Date by default', function () {
        assert.equalDate(x.dateCreated, new Date())
        assert.typeOf(x.dateCreated, 'Date')
      })
      it('Status should be "" by default', function () {
        assert.equal(x.status, '')
        assert.typeOf(x.status, 'String')
      })
      it('Current Section should be "Unknown" by default', function () {
        assert.equal(x.currentSection, 'unknown')
      })
      it('Verified should be "False" by default', function () {
        assert.equal(x.verified, false)
        assert.typeOf(x.verified, 'Boolean')
      })
      it('Time Zone should be "" by default', function () {
        assert.equal(x.timeZone, '')
        assert.typeOf(x.timeZone, 'String')
      })
      it('Auth id should be "" by default', function () {
        assert.equal(x.auth.id, '')
        assert.typeOf(x.auth.id, 'String')
      })
      after(function (done) {
        User.remove({firstName: ''}, function (err) {
          if (err) throw err
          done()
        })
      })
    })

    describe('#custom-values', function () {
      before(function (done) {
        x.email = 'shivani@gmail.com'
        x.firstName = 'Test'
        x.phone = '000-000-0000'
        x.dateCreated = new Date(2017, 4, 20, 16, 5)
        x.status = 'ONBOARDING'
        x.currentSection = 'SE1'
        x.verified = 'True'
        x.save(function (err) {
          if (err) throw err
          done()
        })
      })
      it('Email should be "shivani@gmail.com"', function () {
        assert.equal(x.email, 'shivani@gmail.com')
        assert.typeOf(x.email, 'string')
      })
      it('FirstName should be "Test"', function () {
        assert.equal(x.firstName, 'Test')
      })
      it('First Name should be typeof String', function () {
        assert.typeOf(x.firstName, 'string')
      })
      it('Phone should be "000-000-0000"', function () {
        assert.equal(x.phone, '000-000-0000')
        assert.typeOf(x.phone, 'string')
      })
      it('Date created should be "Sat May 20th 2017" ', function () {
        assert.equalDate(x.dateCreated, new Date(2017, 4, 20, 16, 5))
        assert.typeOf(x.dateCreated, 'Date')
      })
      it('Status should be "ONBOARDING"', function () {
        assert.equal(x.status, 'ONBOARDING')
        assert.typeOf(x.status, 'string')
      })
      it('CurrentSection should be "SE1"', function () {
        assert.equal(x.currentSection, 'SE1')
        assert.typeOf(x.currentSection, 'string')
      })
      it('Verified should be "True"', function () {
        assert.equal(x.verified, true)
        assert.typeOf(x.verified, 'Boolean')
      })
    })
  })
  describe('#read', function () {
    it('should read the User FirstName Test from the database', function (done) {
      Models.Auvenir.User.find({}, {firstName: 'Test'}, function (err, oneUser) {
        if (err) throw err
        done()
      })
    })
    it('should read the User Status from the database', function (done) {
      Models.Auvenir.User.findOne({}, {status: 1}, function (err, oneUs) {
        if (err) throw err
        done()
      })
    })
    it('should read the User Auth.ID from the database', function (done) {
      Models.Auvenir.User.find({}, {'auth.id': 1}, function (err, oneUs) {
        if (err) throw err
        done()
      })
    })
  })
  describe('#update', function () {
    before(function (done) {
      x.firstName = 'Shivani'
      x.status = 'ACTIVE'
      x.dateCreated = new Date(2015, 4, 10, 16, 5)
      x.currentSection = 'SE2'
      x.auth.id = '12adadad'
      x.save(function (err) {
        if (err) throw err
        done()
      })
    })
    it('should update the database of user', function (done) {
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the User of name Shivani', function (done) {
      User.remove({firstName: 'Shivani'}, function (err, updatedd) {
        if (err) throw err
        done()
      })
    })
  })
  after(function (done) {
    mongoose.disconnect(function () {
      done()
    })
  })
})
