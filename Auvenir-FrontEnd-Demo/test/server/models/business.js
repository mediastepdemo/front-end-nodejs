/* globals describe, it, before, after */

var assert = require('chai').assert
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var Business = require('../../../models/business')
var Models = require('../../../models')

describe('Model: Business', function () {
  var x = new Business()

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      done()
    })
  })
  describe('#Check-Keys', function (done) {
    var a = x.toObject()
    var myKeys = Object.keys(a)
    it('check if the keys are the same', function (done) {
      var keys = ['_id', 'fiscalYearEnd', 'accountingSoftware', 'accountingFramework', 'industry', 'structure', 'addresses', 'phone', 'websiteURL', 'name', 'domain', 'acl']
      assert.sameMembers(myKeys, keys, 'same members')
      done()
    })
  })
  describe('#create', function () {
    describe('#default-values', function () {
      it('The domain should be "" by default', function () {
        assert.equal(x.domain, '')
        assert.typeOf(x.domain, 'String')
      })
      it('The name of the Business should be "String" by default', function () {
        assert.equal(x.name, '')
        assert.typeOf(x.name, 'String')
      })
      it('The Phone should be "String" by default', function () {
        assert.equal(x.phone, '')
        assert.typeOf(x.phone, 'String')
      })
      it('The logo should be "String"', function () {
        assert.equal(x.logo, undefined)
      })
      it('The FiscalYearEnd should be "Date"', function () {
        assert.equal(x.fiscalYearEnd, null)
      })
      after(function (done) {
        Business.remove({name: ''}, function (err) {
          if (err) throw err
          done()
        })
      })
    })

    describe('#custom-values', function () {
      before(function (done) {
        x.domain = 'Shivani@auvenir.com'
        x.name = 'Business'
        x.websiteURL = 'www.auvenir.com'
        x.phone = '000-000-0000'
        x.fiscalYearEnd = new Date(2017, 4, 20, 16, 5)
        x.save(function (err) {
          if (err) throw err
          done()
        })
      })
      it('The domain should be "Shivani@auvenir.com"', function () {
        assert.equal(x.domain, 'Shivani@auvenir.com')
        assert.typeOf(x.domain, 'String')
      })
      it('The name of the Business should be "Businessss"', function () {
        assert.equal(x.name, 'Business')
        assert.typeOf(x.name, 'String')
      })
      it('The Phone should be "000-000-0000"', function () {
        assert.equal(x.phone, '000-000-0000')
        assert.typeOf(x.phone, 'String')
      })
      it('The websiteURL should be "www.auvenir.com"', function () {
        assert.equal(x.websiteURL, 'www.auvenir.com')
        assert.typeOf(x.websiteURL, 'String')
      })
      it('The FiscalYearEnd should be "Sat May 20th 2017"', function () {
        assert.equalDate(x.fiscalYearEnd, new Date(2017, 4, 20, 16, 5))
        assert.typeOf(x.fiscalYearEnd, 'Date')
      })
    })
  })
  describe('#read', function () {
    it('should read the Business Name from the database', function (done) {
      Models.Auvenir.Business.find({}, {name: 'Business'}, function (err, oneUser) {
        if (err) throw err
        done()
      })
    })
    it('should read the Business Phone from the database', function (done) {
      Models.Auvenir.Business.findOne({}, {phone: 1}, function (err, oneUs) {
        if (err) throw err
        done()
      })
    })
  })
  describe('#update', function () {
    before(function (done) {
      x.name = 'Buss'
      x.phone = '123-456-7890'
      x.integration.quickbooks.token = 'abcdefg'
      x.fiscalYearEnd = new Date(2017, 4, 30, 16, 5)
      x.save(function (err) {
        if (err) throw err
        done()
      })
    })
    it('should update the database of business', function (done) {
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the Business of name Buss', function (done) {
      Business.remove({name: 'Buss'}, function (err, updatedd) {
        if (err) throw err
        done()
      })
    })
  })
  after(function (done) {
    mongoose.disconnect(function () {
      done()
    })
  })
})
