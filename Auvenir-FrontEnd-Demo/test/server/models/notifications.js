/* globals describe, it, before, after */

var assert = require('chai').assert
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var Notification = require('../../../models/notification')
var Models = require('../../../models')

describe('Model: Notification', function () {
  var x = new Notification()

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      done()
    })
  })
  describe('#Check-Keys', function (done) {
    var a = x.toObject()
    var myKeys = Object.keys(a)
    it('check if the keys are the same', function (done) {
      var keys = ['_id', 'referenceKey', 'description', 'message', 'type', 'dateCreated']
      assert.sameMembers(myKeys, keys, 'same members')
      done()
    })
  })
  describe('#create', function () {
    describe('#default-values', function () {
      it('Destination should be ref to "User"', function () {
        assert.deepProperty({ destination: { ref: 'User' } }, 'destination.ref')
      })
      it('DateCreated should be todays date by default', function () {
        assert.equalDate(x.dateCreated, new Date())
      })
      it('Message should be "" by default', function () {
        assert.isString(x.message, '')
      })
      after(function (done) {
        Notification.remove({message: ''}, function (err) {
          if (err) throw err
          done()
        })
      })
    })

    describe('#custom-values', function () {
      before(function (done) {
        x.dateCreated = new Date()
        x.message = 'Messages1'

        x.save(function (err) {
          if (err) throw err
          done()
        })
      })
      it('DateCreated should be Todays date', function () {
        assert.equalDate(x.dateCreated, new Date())
        assert.typeOf(x.dateCreated, 'Date')
      })
      it('Message should be "Messages1"', function () {
        assert.equal(x.message, 'Messages1')
      })
    })
  })
  describe('#read', function () {
    it('should read the Message of Notification from the database', function (done) {
      Models.Auvenir.Notification.find({}, {message: 'Messages1'}, function (err, oneUser) {
        if (err) throw err
      })
      Models.Auvenir.Notification.findOne({}, {type: ''}, function (err, oneUser) {
        if (err) throw err
        done()
      })
    })
  })
  describe('#update', function () {
    before(function (done) {
      x.dateCreated = new Date(2017, 4, 20, 16, 5).toUTCString()
      x.referenceKey = 'SSEC12'

      x.save(function (err) {
        if (err) throw err
        done()
      })
    })
    it('should update the Notification reference Key and date created in database', function (done) {
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the Notification with referenceKey "SSEC12"', function (done) {
      Notification.remove({referenceKey: 'SSEC12'}, function (err, updatedd) {
        if (err) throw err
        done()
      })
    })
  })
})
