/* globals describe, it, before, after */

var assert = require('chai').assert
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var Discrepancy = require('../../../models/discrepancy')
var Models = require('../../../models')

describe('Model: Discrepancy', function () {
  var x = new Discrepancy()

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      done()
    })
  })
  describe('#Check-Keys', function (done) {
    var a = x.toObject()
    var myKeys = Object.keys(a)
    it('check if the keys are the same', function (done) {
      var keys = ['_id', 'dateCompleted', 'dateCreated', 'evidence', 'references', 'severity', 'description', 'title', 'type', 'engagementID', 'acl']
      assert.sameMembers(myKeys, keys, 'same members')
      done()
    })
  })
  describe('#create', function () {
    describe('#default-values', function () {
      it('Title should be "" by default', function () {
        assert.equal(x.title, '')
      })
      it('Severity should be LOW by default', function () {
        assert.isDefined(x.severity, 'Defined to LOW')
      })
      after(function (done) {
        Discrepancy.remove({title: ''}, function (err) {
          if (err) throw err
          done()
        })
      })
    })

    describe('#custom-values', function () {
      before(function (done) {
        x.title = 'DiscrepancyOne'
        x.description = 'This will be description'

        x.save(function (err) {
          if (err) throw err
          done()
        })
      })
      it('Title should be "DiscrepancyOne"', function () {
        assert.equal(x.title, 'DiscrepancyOne')
        assert.isString(x.title)
      })
      it('Description should be strings', function () {
        assert.equal(x.description, 'This will be description')
      })
    })
  })
  describe('#read', function () {
    it('should read the Discrepancy Title from the database', function (done) {
      Models.Auvenir.Discrepancy.find({}, {title: 'DiscrepancyOne'}, function (err, oneUser) {
        if (err) throw err
        done()
      })
    })
  })
  describe('#update', function () {
    before(function (done) {
      x.title = 'DiscrepancyTwo'
      x.severity = 'HIGH'

      x.save(function (err) {
        if (err) throw err
        done()
      })
    })
    it('should update the Discrepancy in database', function (done) {
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the Discrepancy of Title "DiscrepancyTwo"', function (done) {
      Discrepancy.remove({title: 'DiscrepancyTwo'}, function (err, updatedd) {
        if (err) throw err
        done()
      })
    })
  })
})
