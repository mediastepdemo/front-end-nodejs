/* globals describe, it, before, after */

var assert = require('chai').assert
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var Developer = require('../../../models/developer')
var Models = require('../../../models')

describe('Model: Developer', function () {
  var x = new Developer()

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      done()
    })
  })
  describe('#Check-Keys', function (done) {
    var a = x.toObject()
    var myKeys = Object.keys(a)
    it('check if the keys are the same', function (done) {
      var keys = ['_id', 'email']
      assert.sameMembers(myKeys, keys, 'same members')
      done()
    })
  })
  describe('#create', function () {
    describe('#default-values', function () {
      it('Email should be "" by default', function () {
        assert.equal(x.email, '')
        assert.typeOf(x.email, 'String')
      })
      it('ApiKey should be String by default', function () {
        assert.isUndefined(x.apiKey, 'Not defined')
      })
      after(function (done) {
        Developer.remove({email: ''}, function (err) {
          if (err) throw err
          done()
        })
      })
    })

    describe('#custom-values', function () {
      before(function (done) {
        x.email = 'auvenir10@auvenir.com'
        x.apiTokenExpires = new Date(2017, 4, 20, 16, 5)

        x.save(function (err) {
          if (err) throw err
          done()
        })
      })
      it('Email should be "auvenir10@auvenir.com"', function () {
        assert.equal(x.email, 'auvenir10@auvenir.com')
        assert.typeOf(x.email, 'String')
      })
      it('ApiToken Expiry Date should be "Sat May 20th 2017"', function () {
        assert.equalDate(x.apiTokenExpires, new Date(2017, 4, 20, 16, 5))
      })
    })
  })
  describe('#read', function () {
    it('should read the developer email from the database', function (done) {
      Models.Developer.find({}, {email: 1}, function (err, oneUser) {
        if (err) throw err
        done()
      })
    })
  })
  describe('#update', function () {
    before(function (done) {
      x.email = 'Test@auvenir.com'

      x.save(function (err) {
        if (err) throw err
        done()
      })
    })
    it('should update the Developer Email in database', function (done) {
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the Developer of given email', function (done) {
      Developer.remove({email: 'Test@auvenir.com'}, function (err, updatedd) {
        if (err) throw err
        done()
      })
    })
  })
})
