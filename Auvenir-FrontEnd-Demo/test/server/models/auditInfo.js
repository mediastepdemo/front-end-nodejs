/* globals describe, it, before, after */

var assert = require('chai').assert
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var AuditInfo = require('../../../models/auditInfo')
var Models = require('../../../models')

describe('Model: AuditInfo', function () {
  var x = new AuditInfo()

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      done()
    })
  })
  describe('#Check-Keys', function (done) {
    var a = x.toObject()
    var myKeys = Object.keys(a)
    it('check if the keys are the same', function (done) {
      var keys = ['_id', 'auditors', 'teamMembers', 'accountants', 'lawyers', 'acl']
      assert.sameMembers(myKeys, keys, 'same members')
      done()
    })
  })
  describe('#create', function () {
    describe('#default-values', function () {
      it('Materiality should be nothing by default', function () {
        assert.equal(x.materiality, undefined)
      })
      it('Access Cards Ready should be "True or False" by default', function () {
        assert.equal(x.accessCardsReady, undefined)
      })
      it('Lawyers FirstName should be empty by default', function () {
        assert.equal(x.lawyers, '')
      })
      it('Client KeyContact jobTitle should be "String"', function () {
        assert.equal(x.clientKeyContact.jobTitle, undefined)
      })
      it('The FiscalYearEnd should be "Date"', function () {
        assert.equal(x.engagementID, undefined)
      })
      after(function (done) {
        AuditInfo.remove({'clientKeyContact.jobTitle': undefined}, function (err) {
          if (err) throw err
          done()
        })
      })
    })

    describe('#custom-values', function () {
      before(function (done) {
        x.materiality = '25'
        x.workspaceAvailable = 'true'
        x.lawyers = 'Dean'
        x.clientKeyContact.firstName = 'Test'
        x.clientKeyContact.jobTitle = 'New Job'
        x.save(function (err) {
          if (err) throw err
          done()
        })
      })
      it('Materiality should be "25"', function () {
        assert.equal(x.materiality, '25')
        assert.typeOf(x.materiality, 'Number')
      })
      it('WorkSpaceAvailable should be "True"', function () {
        assert.equal(x.workspaceAvailable, true)
        assert.typeOf(x.workspaceAvailable, 'Boolean')
      })
      it('Lawyers FirstName should be "Dean"', function () {
        assert.equal(x.lawyers, 'Dean')
      })
      it('ClientKeyContact.firstName should be "Test"', function () {
        assert.equal(x.clientKeyContact.firstName, 'Test')
        assert.typeOf(x.clientKeyContact.firstName, 'String')
      })
      it('ClientKeyContact.JobTitle should be "New Job"', function () {
        assert.equal(x.clientKeyContact.jobTitle, 'New Job')
        assert.typeOf(x.clientKeyContact.jobTitle, 'String')
      })
    })
  })
  describe('#read', function () {
    it('should read the AuditInfo Materiality from the database', function (done) {
      Models.AuditInfo.find({}, {materiality: '25'}, function (err, oneUser) {
        if (err) throw err
        // console.log(oneUser);
        done()
      })
    })
    it('should read the AuditInfo ClientKeyContact.userId from the database', function (done) {
      Models.AuditInfo.findOne({}, {'clientKeyContact.userID': 1}, function (err, oneUs) {
        if (err) throw err
        // console.log(oneUs);
        done()
      })
    })
  })
  describe('#update', function () {
    before(function (done) {
      x.accessCardsReady = 'True'
      x.clientKeyContact.required = 'false'
      x.clientKeyContact.firstName = 'Test1'
      x.clientKeyContact.userID = 'abc123'
      x.save(function (err) {
        if (err) throw err
        done()
      })
    })
    it('should update the database of AuditInfo', function (done) {
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the Business of name Buss', function (done) {
      AuditInfo.remove({materiality: '25'}, function (err, updatedd) {
        if (err) throw err
        done()
      })
    })
  })
  after(function (done) {
    mongoose.disconnect(function () {
      done()
    })
  })
})
