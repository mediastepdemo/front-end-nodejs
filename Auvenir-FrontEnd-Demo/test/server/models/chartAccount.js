/* globals describe, it, before, after */

var assert = require('chai').assert
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var ChartAccount = require('../../../models/chartAccount')
var Models = require('../../../models')

describe('Model: ChartAccount', function () {
  var x = new ChartAccount()

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      done()
    })
  })
  describe('#Check-Keys', function (done) {
    var a = x.toObject()
    var myKeys = Object.keys(a)
    it('check if the keys are the same', function (done) {
      var keys = ['_id', 'active', 'lastUpdateTime', 'createTime', 'parentReferenceId', 'currency', 'totalBalance', 'currentBalance', 'openBalance',
        'closeBalance', 'subCategory', 'category', 'classification', 'number', 'nameFull', 'name', 'referenceId']
      assert.sameMembers(myKeys, keys, 'same members')
      done()
    })
  })
  describe('#create', function () {
    describe('#default-values', function () {
      it('ReferenceId should be null by default', function () {
        assert.equal(x.referenceId, null)
      })
      it('NameFull should be "" by default', function () {
        assert.equal(x.nameFull, '')
        assert.typeOf(x.nameFull, 'String')
      })
      it('CloseBalance should be "0" by default', function () {
        assert.equal(x.closeBalance, '0')
        assert.typeOf(x.closeBalance, 'Number')
      })
      after(function (done) {
        ChartAccount.remove({nameFull: ''}, function (err) {
          if (err) throw err
          done()
        })
      })
    })

    describe('#custom-values', function () {
      before(function (done) {
        x.referenceId = '123'
        x.nameFull = 'Chartered'
        x.openBalance = '150'

        x.save(function (err) {
          if (err) throw err
          done()
        })
      })
      it('ReferenceId should be "123"', function () {
        assert.equal(x.referenceId, '123')
        assert.typeOf(x.referenceId, 'Number')
      })
      it('NameFull should be "Chartered"', function () {
        assert.equal(x.nameFull, 'Chartered')
      })
      it('Opening Balance should be "150"', function () {
        assert.equal(x.openBalance, '150')
      })
    })
  })
  describe('#read', function () {
    it('should read the ReferenceID 123 from the database', function (done) {
      Models.ChartAccount.find({}, {referenceId: '123'}, function (err, oneUser) {
        if (err) throw err
        done()
      })
    })
    it('should read the NameFull from the database', function (done) {
      Models.ChartAccount.findOne({}, {nameFull: 1}, function (err, oneUs) {
        if (err) throw err
        done()
      })
    })
    it('should read the openBalance from the database', function (done) {
      Models.ChartAccount.find({}, {'openBalance': 1}, function (err, oneUs) {
        if (err) throw err
        done()
      })
    })
  })
  describe('#update', function () {
    before(function (done) {
      x.referenceId = '456'
      x.nameFull = 'Test1'

      x.save(function (err) {
        if (err) throw err
        done()
      })
    })
    it('should update the ChartAccount of the User in database', function (done) {
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the ChartAccount of referenceId REFID', function (done) {
      ChartAccount.remove({referenceId: '456'}, function (err, updatedd) {
        if (err) throw err
        done()
      })
    })
  })
})
