/* globals describe, it, before, after */

var assert = require('chai').assert
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var Activity = require('../../../models/activity')
var Models = require('../../../models')
var Utility = require('../../../plugins/utilities/utility')

describe('Model: Activity', function () {
  var activity = new Activity()
  var activityWithData = {}
  var auditor = {}
  var client = {}
  var business = {}
  var template = new Models.Auvenir.Template.obj()
  var originalEngagementAudit = {}
  var updatedEngagementAudit = {}

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      mongoose.connection.db.dropDatabase(function (err) {
        if (err) {
          console.error('db not dropped, test will fail due to duplicate data')
          done()
        } else {
          console.log('db dropped, test ready to proceed...')

          // create auditor
          auditor.acl = ['CPA']
          auditor.username = 'MikeAuditor'
          auditor.email = 'mikeAuditor@mail.com'
          auditor.firstName = 'Mike'
          auditor.lastName = 'Smith'
          auditor.phone = '198-789-1234'
          auditor.jobTitle = 'CPA'
          auditor.profilePicture = null
          client.status = 'ACTIVE'

          // TODO - code cleanup after db model change
          //      - use .createUserAccount() function instead
          Utility.createUser(auditor, function (err, myAuditor) {
            if (err) {
              console.error(err)
            } else {
              auditor = myAuditor

              // create client
              client.acl = ['CLIENT']
              client.username = 'johnnyClient'
              client.email = 'johnnyClient@mail.com'
              client.firstName = 'John'
              client.lastName = 'Doe'
              client.phone = '123-456-7890'
              client.jobTitle = 'Accountant'
              client.profilePicture = null
              client.status = 'ONBOARDING'

              // TODO - code cleanup after db model change
              //      - use .createUserAccount() function instead
              Utility.createUser(client, function (err, myClient) {
                if (err) {
                  console.log(err)
                } else {
                  client = myClient

                  // create business
                  business.acl = [ { id: auditor._id, role: 'member' }, { id: client._id, role: 'member' } ]
                  business.domain = 'gmail.com'
                  business.name = 'Subaru Technica International'
                  business.websiteURL = 'www.awebsite.com'
                  business.phone = '987-654-3211'
                  business.address = '123 Random Street, Milton, ON'
                  business.industry = 'Automotive'

                  Utility.createBusiness(business, function (err, myBusiness) {
                    if (err) {
                      console.error(err)
                    } else {
                      business = myBusiness

                      // create original engagement
                      originalEngagementAudit.acl = [ { role: 'auditor', id: auditor._id }, { role: 'client', id: client._id } ]
                      originalEngagementAudit.currentUserID = auditor._id
                      originalEngagementAudit.name = 'Subaru Audit'
                      originalEngagementAudit.type = 'audit'
                      originalEngagementAudit.template = { name: 'default', type: 'audit' }
                      originalEngagementAudit.dueDate = new Date('2017', '03', '15')
                      originalEngagementAudit.client = client
                      originalEngagementAudit.auditor = auditor

                      // create template
                      template.name = 'default'
                      template.type = 'audit'
                      template.section = []
                      template.save(function (err) {
                        if (err) {
                          console.error(err)
                        } else {
                          Utility.createEngagement_Audit(originalEngagementAudit, function (err, result) {
                            if (err) {
                              console.error(err)
                            } else {
                              originalEngagementAudit = result.engagement

                              // create updatedEngagement
                              var updateData = {}
                              updateData.engagementID = originalEngagementAudit._id
                              updateData.name = 'Subaru Technica International Audit'
                              updateData.dueDate = new Date('2017', '08', '23')

                              Utility.updateEngagement(updateData, auditor._id.toString(), function (err, result) {
                                if (err) {
                                  console.error(err)
                                } else {
                                  updatedEngagementAudit = result.updatedEngagement

                                  // create valid activity log
                                  activityWithData.userID = auditor._id.toString()
                                  activityWithData.engagementID = originalEngagementAudit._id.toString()
                                  activityWithData.typeCrud = 'CREATE'
                                  activityWithData.typeActivity = 'Engagement'
                                  activityWithData.original = originalEngagementAudit
                                  activityWithData.updated = updatedEngagementAudit
                                  Utility.createActivity(
                                    activityWithData.userID, activityWithData.engagementID,
                                    activityWithData.typeCrud, activityWithData.typeActivity,
                                    activityWithData.original, activityWithData.updated,
                                    function (err, newActivity) {
                                      if (err) {
                                        console.log(err)
                                      } else {
                                        activityWithData = newActivity
                                      }
                                    }
                                  )
                                  done()
                                }
                              })
                            }
                          })
                        }
                      })
                    }
                  })
                }
              })
            }
          })
        }
      })
    })
  })

  describe('#Check Keys', function () {
    var activityObject = activity.toObject()
    var activityKeys = Object.keys(activityObject)

    it('Check if keys are the same', function (done) {
      var keys = ['_id', 'timestamp', 'changes', 'typeActivity', 'typeCrud']
      assert.sameMembers(activityKeys, keys, 'same members')
      done()
    })
  })

  describe('#create', function () {
    describe('#default-values', function () {
      it('engagementID should be undefined by default', function (done) {
        assert.equal(activity.engagementID, undefined)
        done()
      })
      it('typeCrud should be "" by default', function (done) {
        assert.equal(activity.typeCrud, '')
        assert.typeOf(activity.typeCrud, 'string')
        done()
      })
      it('typeActivity should be "" by default', function (done) {
        assert.equal(activity.typeActivity, '')
        assert.typeOf(activity.typeActivity, 'string')
        done()
      })
      it('changes should be [] by default', function (done) {
        assert.typeOf(activity.changes, 'array')
        done()
      })
      after(function (done) {
        done()
      })
    })
  })
  describe('#read', function () {
    it('should read the EngagementID from the database', function (done) {
      Models.Auvenir.Activity.find({engagementID: activityWithData.engagementID}, function (err, result) {
        if (err) throw err
        assert.notEqual(result, null)
        done()
      })
    })
    it('should read the userID from the database', function (done) {
      Models.Auvenir.Activity.find({userID: activityWithData.userID}, function (err, result) {
        if (err) throw err
        assert.notEqual(result, null)
        done()
      })
    })
    it('should read the typeCrud from the database', function (done) {
      Models.Auvenir.Activity.find({typeCrud: activityWithData.typeCrud}, function (err, result) {
        if (err) throw err
        assert.notEqual(result, null)
        done()
      })
    })
    it('should read the typeActivity from the database', function (done) {
      Models.Auvenir.Activity.find({typeActivity: activityWithData.typeActivity}, function (err, result) {
        if (err) throw err
        assert.notEqual(result, null)
        done()
      })
    })
    it('should read the changes from the database', function (done) {
      Models.Auvenir.Activity.find({changes: [{original: activityWithData.original}]}, function (err, result) {
        if (err) throw err
        assert.notEqual(result, null)
        done()
      })
    })
  })
  describe('#update', function () {
    it('should update the Activity Log with the specified engagementID in database', function (done) {
      Models.Auvenir.Activity.findByIdAndUpdate({_id: activityWithData._id}, {$set: {typeCrud: 'UPDATE'}}, {new: true}, function (err, result) {
        assert.notEqual(result, null)
      })
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the Activity with specified engagementID', function (done) {
      Models.Auvenir.Activity.remove({_id: activityWithData._id}, function (err, result) {
        assert.notEqual(result, null)
      })
      done()
    })
  })
})
