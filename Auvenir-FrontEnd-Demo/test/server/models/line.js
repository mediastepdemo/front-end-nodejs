/* globals describe, it, before, after */

var assert = require('chai').assert
var chai = require('chai')
chai.use(require('chai-datetime'))

var mongoose = require('mongoose')
var Line = require('../../../models/line')
var Models = require('../../../models')

describe('Model: Line', function () {
  var x = new Line()

  before(function (done) {
    var mongooseURL = 'mongodb://mongo:27017/unit'
    mongoose.connect(mongooseURL, function () {
      done()
    })
  })
  describe('#Check-Keys', function (done) {
    var a = x.toObject()
    var myKeys = Object.keys(a)
    it('check if the keys are the same', function (done) {
      var keys = ['_id', 'postType', 'amount', 'details', 'lineNumber']
      assert.sameMembers(myKeys, keys, 'same members')
      done()
    })
  })
  describe('#create', function () {
    describe('#default-values', function () {
      it('AccountID should be ref to "ChartAccount"', function () {
        assert.deepProperty({ accountID: { ref: 'ChartAccount' } }, 'accountID.ref')
      })
      it('Line Number should be undefined by default', function () {
        assert.equal(x.lineNumber, '0')
      })
      it('Post Type should be "" by default', function () {
        assert.equal(x.postType, '')
      })
      after(function (done) {
        Line.remove({lineNumber: '0'}, function (err) {
          if (err) throw err
          done()
        })
      })
    })

    describe('#custom-values', function () {
      before(function (done) {
        x.lineNumber = '60'
        x.postType = 'Debit'

        x.save(function (err) {
          if (err) throw err
          done()
        })
      })
      it('LineNumber should be "60"', function () {
        assert.equal(x.lineNumber, '60')
        assert.isNumber(x.lineNumber)
      })
      it('Post Type should be Debit', function () {
        assert.equal(x.postType, 'Debit')
      })
    })
  })
  describe('#read', function () {
    it('should read the people from the database', function (done) {
      Models.Line.find({}, {postType: 'Debit'}, function (err, oneUser) {
        if (err) throw err
        done()
      })
    })
  })
  describe('#update', function () {
    before(function (done) {
      x.postType = 'Credit'

      x.save(function (err) {
        if (err) throw err
        done()
      })
    })
    it('should update the Post Type in Line to Credit in database', function (done) {
      done()
    })
  })
  describe('#delete', function () {
    it('should delete the Line of of LineNumber 60', function (done) {
      Line.remove({lineNumber: '60'}, function (err, updatedd) {
        if (err) throw err
        done()
      })
    })
  })
})
