const path = require('path')
global.rootDirectory = path.normalize(path.join(__dirname, '..', '..'))
const appConfig = require(rootDirectory + '/config')
const testConfig = require(rootDirectory + '/test/config')
const Logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = Logger.init(__filename)
const mongoose = require('mongoose')
mongoose.Promise = global.Promise
const async = require('async')
const Models = require(rootDirectory + '/models')
const Utility = require(rootDirectory + '/plugins/utilities/utility')
const finicityErrorCodes = require(rootDirectory + '/finicity/plugins/errorCodes')
const io = require('socket.io-client')
const requestDefaults = {
  jar: true,
  encoding: 'utf8',
  json: true
}
if (testConfig.server.proxy !== '') {
  requestDefaults.proxy = testConfig.server.proxy
}
// const request = require('request').defaults(requestDefaults)
const request = require('request')

module.exports = class TestAPIV1 {

  /**
   * TestAPIV1()
   */
  constructor () {
    this.socket = null
    this.currentUser = null
    this.currentBusiness = null
    this.currentFirm = null
    this.currentEngagement = null
    this.engagements = []
    this.canceledFiles = []
    this.pendingUsers = []
    this.pendingBusiness = []
    this.eventHandlers = {}
    this.callback = null
    this.sessionCookie = null

    // SOCKET EVENTS
    this.EVENT_RESPONSE = 'event-response'
    this.LOGIN_RESPONSE = 'login-response'
    this.BROADCASTING = 'broadcasting'

    // REQUEST NAMES
    this.FN_CONNECT = 'fn-connect'
    this.FN_LOGIN_FORM_REQUEST = 'fn-loginFormRequest'
    this.FN_LOGIN_SUBMIT = 'fn-loginSubmit'
    this.FN_LOGIN_SECURITY_SUBMIT = 'fn-loginSecuritySubmit'
    this.FN_ACCOUNT_LIST_SELECTED_REQUEST = 'fn-accountListSelectedRequest'
    this.FN_SELECTION_SECURITY_SUBMIT = 'fn-selectionSecuritySubmit'
    this.FN_LOGIN_EXIT = 'fn-loginExit'
    this.FN_LOGIN_ERROR_EXIT = 'fn-loginErrorExit'
    this.FN_LOGIN_COMPLETE = 'fn-loginComplete'
    this.UPDATE_USER_PASSWORD = 'update-user-password'

    // RESPONSE NAMES
    this.SESSION_READY = 'session-ready'
    this.DEV_SIGNUP = 'dev-signup'
    this.GET_ADMIN_MAPPING = 'get-admin-mapping'
    this.LOGIN = 'login'
    this.ERROR_RESPONSE = 'error-response'
    this.ONBOARDING_COMPLETE = 'onboarding-complete'
    this.LOAD_ENGAGEMENT = 'load-engagement'
    this.CREATE_ENGAGEMENT = 'create-engagement'
    this.CREATE_ACTIVITY = 'create-activity'
    this.UPDATE_USER = 'update-user'
    this.UPDATE_BUSINESS = 'update-business'
    this.UPDATE_ENGAGEMENT = 'update-engagement'
    this.ADD_FILE = 'add-file'
    this.DELETE_FILE = 'delete-file'
    this.VERIFY_AUDITOR = 'verify-auditor'
    this.ONBOARD_USER = 'onboard-user'
    this.UPDATE_USER_STATUS = 'update-user-status'
    this.DEACTIVATE_ACCOUNT = 'deactivate-account'
    this.SEND_ENGAGEMENT_INVITE = 'send-engagement-invite'
    this.UPDATE_FILE = 'update-file'
    this.UPDATE_MULTIPLE_FILES = 'update-multiple-files'
    this.UPDATE_FIRM = 'update-firm'
    this.GET_ACTIVITIES = 'get-activities'
    this.GET_CONTACT_LIST = 'get-contact-list'
    this.SELECT_CLIENT = 'select-client'
    this.ADD_INTEGRATION = 'add-integration'
    this.ADD_TEAM_MEMBER = 'add-team-member'
    this.SEND_TEAM_MEMBER_INVITE = 'send-team-member-invite'
    this.CREATE_TODO = 'create-todo'
    this.UPDATE_TODO = 'update-todo'
    this.GET_USER_LIST = 'get-user-list'
    this.CREATE_CATEGORY = 'create-category'
    this.UPDATE_CATEGORY = 'update-category'
    this.DELETE_CATEGORY = 'delete-category'
    this.BULK_TODOS_UPDATE = 'bulk-todos-update'
    this.UNDO_BULK_ACTION = 'undo-bulk-action'
    this.ADD_MULTIPLE_TEAM_MEMBERS = 'add-multiple-team-members'
    this.SEND_MULTIPLE_TEAM_MEMBER_INVITES = 'send-multiple-team-member-invites'
    this.DELETE_ENGAGEMENT = 'delete-engagement'
    this.SEND_SUGGESTION = 'send-suggestion'
    this.GET_ENGAGEMENTS = 'get-engagements'
    this.FIND_CONTACTS = 'find-contacts'
    this.SET_NOTIFICATION = 'set-notification'
    this.FN_CONNECT = 'fn-connect'
    this.FN_SERVER_ERROR = 'fn-serverError'
    this.FN_LOGIN_FORM_RESPONSE = 'fn-loginFormResponse'
    this.FN_ACCOUNT_LIST = 'fn-accountList'
    this.FN_LOGIN_FAILURE = 'fn-loginFailure'
    this.FN_LOGIN_SECURITY = 'fn-loginSecurity'
    this.FN_MFA_TIMEOUT = 'fn-mfaTimeout'
    this.FN_ACCOUNT_LIST_SELECT_RESPONSE = 'fn-accountListSelectedResponse'
    this.FN_SELECTION_SECURITY_FAILURE = 'fn-selectionSecurityFailure'
    this.FN_SELECTION_SECURITY = 'fn-selectionSecurity'
  }

  /**
   * Registers a callback function to a specific model by adding it to an array
   * called 'eventHandlers'. We also allow events to specify a unique ID
   * that allows for events on a specific element versus all of them. We
   * also check for a type of operation following the CRUD methodology, except we
   * do not include READ as it isn't required at this time.
   *
   * @param tag:    String - The name of the event
   * @param uniqueID: String - A unique ID for source identification
   * @param callback: Function - What action to take
   * @return status: Boolean
   */
  addEventHandler(tag, uniqueID, callback) {
    if (typeof tag !== 'string' || typeof uniqueID !== 'string' || typeof callback !== 'function') {
      error(`invalid add event handler request`)
      return false
    }
    let handlers = this.eventHandlers[tag]
    if (!handlers) {
      this.eventHandlers[tag] = [{ id: uniqueID, cb: callback }]
      return
    }
    if (handlers.find((e) => { return e.id === uniqueId })) {
      error(`attempt to add duplicate event handler for tag=${tag} id=${uniqueId}`)
      return false
    }
    handlers.push({ id: uniqueID, cb: callback })
    error(`added event handler for tag=${tag} id=${uniqueId}`)
    return true
  }

  /**
   * Searches the appropriate model events and removes the event listener.
   *
   * @param tag: String
   * @param uniqueID: String
   * @return status: Boolean
   */
  removeEventHandler(tag, uniqueID) {
    if (typeof tag !== 'string' || typeof uniqueID !== 'string') {
      error(`invalid remove event handler request`)
      return false
    }
    const handlers = this.eventHandlers[tag]
    if (handlers) {
      for (let i = 0; i < handlers.length; i++) {
        if (handlers[i].id === uniqueID) {
          handlers.splice(i, 1)
          error(`removed event handler for tag=${tag} id=${uniqueId}`)
          return true
        }
      }
    }
    error(`attempt to remove unknown event handler for tag=${tag} id=${uniqueId}`)
    return false
  }

  /**
   * Call Server event through socket if event is verified Format.
   * @param payload: Json object of event data.
   */
  sendEvent(payload) {
    if (this.verifyRequest(payload)) {
      this.socket.emit('event', payload)
    } else {
      this.triggerEvent(this.ERROR_RESPONSE, {code: 11, msg: 'Invalid call format'})
    }
  }

  /**
   * Triggers the array of callback functions that are currently associated with
   * the given model. We pass it the model, the id affected, the operation that occured,
   * and the fields that changed.
   *
   * @param tag: String
   * @param resp: JSON
   */
  triggerEvent(tag, resp) {
    if (typeof tag !== 'string') {
      return
    }
    let handlers = this.eventHandlers[tag] || []
    let didHandle = false
    for (let i = 0; i < handlers.length; i++) {
      const obj = this.eventHandlers[tag][i]
      obj.cb(tag, resp)
      didHandle = true
    }
    if (!didHandle) {
      // IS THERE A GENERIC CALLBACK HANDLER
      const callback = this.callback
      if (callback) {
        return callback(tag, resp)
      }
      warn(`event ${tag} was not handled`)
    }
  }

  /**
   * Verify event payloadbefore it emit message through this.socket.
   * @param payload: JSON object of event
   */
  verifyRequest(req) {
    if (req.name && req.data) {
      info(`request: ${JSON.stringify(req, null, 4)}`)
      return true
    }
    error(`invalid request: ${JSON.stringify(req, null, 4)}`)
    return false
  }

  verifyResponse(resp) {
    if (resp.code === 0 && resp.name) return true
    if (resp.code === finicityErrorCodes.api.OK.code && resp.name) return true
    return false
  }

  /**
   * Login Response handler.
   * Loading appropriate page based on user type and status.
   * @param {String} data
   */
  verifyLoginResponse(resp) {
    if (resp.code === 0 && resp.result) {
      this.loadUserData(resp.result)
      if (resp.result.user) {
        this.currentUser = resp.result.user
        return true
      }
    }
    return false
  }

  /**
   * Socket has been Connected.
   * Setup all of the application listeners.
   */
  setupListeners() {
    const self = this
    this.socket.on(this.EVENT_RESPONSE, (resp) => {
      if (this.verifyResponse(resp)) {
        info(`${resp.name}: ${JSON.stringify(resp, null, 4)}`)
        this.triggerEvent(resp.name, resp)
        return
      }
      info(`${this.ERROR_RESPONSE}: ${JSON.stringify(resp, null, 4)}`)
      this.triggerEvent(this.ERROR_RESPONSE, resp)
    })
    this.socket.on(this.LOGIN_RESPONSE, (resp) => {
      if (this.verifyLoginResponse(resp)) {
        info(`${this.LOGIN_RESPONSE}: ${JSON.stringify(resp, null, 4)}`)
        this.triggerEvent(this.LOGIN_RESPONSE, resp)
        return
      }
      this.triggerEvent(this.ERROR_RESPONSE, resp)
    })
    // broadcasting to users online.
    this.socket.on(this.BROADCASTING, (resp) => {
      info(resp.msg)
    })
  }

  startSession (email) {
    async.waterfall([
      // FIND THE USER ACCOUNT IN THE DATABASE
      (done) => {
        Utility.findUserAccount(email, (err, user) => {
          if (!user) {
            return done(`user with email address '${email} not found`)
          }
          done(err, user)
        })
      },
      // GENERATE AN ACCESS TOKEN FOR THE USER
      (user, done) => {
        const token = Utility.secureRandomString(64)
        user.auth.access.token = Utility.createHash(token)
        user.auth.access.expires = Date.now() + Utility.SESSION_LENGTH
        user.save((err) => {
          done(err, user, token)
        })
      },
      // CHECK THE ACCESS TOKEN TO CREATE THE SESSION
      (user, token, done) => {
        const url = appConfig.server.url + '/checkToken'
        request.get({url: url, qs: {token: token, email: email}, followRedirect: false}, (err, resp) => {
          if (err) {
            return done(err)
          }
          if (resp.statusCode >= 400) {
            return done({code: resp.statusCode})
          }
          done(err, resp)
        })
      }
    ], (err, resp) => {
      if (err) {
        this.triggerEvent(this.ERROR_RESPONSE)
        return
      }
      this.sessionCookie = resp.headers['set-cookie']
      this.setupSocket(this.sessionCookie)
      this.triggerEvent(this.SESSION_READY)
    })
  }

  setupSocket(cookie) {
    this.socket = io(appConfig.publicServer.url, {extraHeaders: {cookie: cookie}})
    this.setupListeners()
  }

  /**
   * Called once we have a valid user connecting via socket server. This doesn't load the engagement
   * specific data, but instead focuses on the data that is required regardless of the user.
   */
  loadUserData(serverData) {
    if (serverData.user) {
      this.currentUser = serverData.user
    }
    if (serverData.business) {
      this.currentBusiness = serverData.business
    }
    if (serverData.firm) {
      this.currentFirm = serverData.firm
    }
    if (serverData.engagements) {
      this.currentEngagement = {}
      this.engagements = serverData.engagements
    }
  }

  /**
   * finicityConnect
   *
   * data: {
   *   "currentBusinessID": <mongodb-object-id>,
   *   "institutionID": <finicity-institution-id>
   * }
   *
   * event: {
   *   "sessionID": <mongodb-object-id>,
   *   "ownerID": "FinOwner_1495658468_24f52b83-d730-4854-8b39-fab95221ff73",
   *   "code": "api-000",
   *   "msg": "Success",
   *   "name": "fn-connect"
   * }
   */
  finicityConnect (data) {
    console.error(data);
    this.sendEvent({name: this.FN_CONNECT, data: data})
  }

  /**
   * finicityLoginFormRequest
   *
   * data: {
   *   "sessionID": <mongodb-object-id>,
   *   "ownerID": "FinOwner_1495658468_24f52b83-d730-4854-8b39-fab95221ff73"
   * }
   *
   * event: {
   * }
   */
  finicityLoginFormRequest (data) {
    this.sendEvent({name: this.FN_LOGIN_FORM_REQUEST, data: data})
  }

  finicityLoginSubmit (data) {
    this.sendEvent({name: this.FN_LOGIN_SUBMIT, data: data})
  }

  finicityLoginSecuritySubmit (data) {
    this.sendEvent({name: this.FN_LOGIN_SECURITY_SUBMIT, data: data})
  }

  finicityAccountListSelectedRequest (data) {
    this.sendEvent({name: this.FN_ACCOUNT_LIST_SELECTED_REQUEST, data: data})
  }

  finicitySelectionSecuritySubmit (data) {
    this.sendEvent({name: this.FN_SELECTION_SECURITY_SUBMIT, data: data})
  }

  finicityLoginExit (data) {
    this.sendEvent({name: this.FN_LOGIN_EXIT, data: data})
  }

  finicityLoginErrorExit (data) {
    this.sendEvent({name: this.FN_LOGIN_ERROR_EXIT, data: data})
  }

  /**
   * finicityLoginComplete
   *
   * data: {
   *    sessionID: '592b1e3de40f5ec125d1a063,
   *    engagementID: '59288875b6e264ff0dcecda1'
   * }
   *
   */
  finicityLoginComplete (data) {
    this.sendEvent({name: this.FN_LOGIN_COMPLETE, data: data})
  }

  post (route, data, callback) {
    const url = appConfig.server.url + route
    const headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }
    const options = {
      url: url,
      // headers: headers,
      json: data
    }
    info(`POST ${url}`)
    if (data) {
      info(`REQUEST BODY: ${JSON.stringify(data, null, 4)}`)
    }
    request.post(options, (err, res, body) => {
      if (err) {
        error(err)
        return callback(err)
      }
      info(`RESPONSE BODY: ${JSON.stringify(body, null, 4)}`)
      callback(null, body)
    })
  }

  weblogin (user, callback) {
    this.post('/weblogin', user, callback)
  }

  websignup (member, firm, callback) {
    this.post('/websignup', {member, firm}, callback)
  }

  updateUserPassword (userID, password, newPassword) {
    this.sendEvent({name: this.UPDATE_USER_PASSWORD, data: {userID, password, newPassword}})
  }
}
