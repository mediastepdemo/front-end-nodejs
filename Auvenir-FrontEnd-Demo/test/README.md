# Auvenir Test Suite

This is an overview of the Auvenir javascript test suite. It's powered by [Mocha]https://mochajs.org/ and [Chai]http://chaijs.com/

Currently, when you run **npm test** in the main folder it will trigger the integration tests.

## Integration Tests

These are currently the most useful tests, they basically walk through the steps you would manually do to test things.
It uses DOM element id's class names to move the mouse around and walk through as a robot-user (based on our logic).

The order in which the tests are run are specified in *weblogin.js*.

### New Tests

When creating a new test, follow these steps:

1. Create the file in either /admin, /auditor, /client, etc. based on which user type is required.
2. The name of the file should be descriptive and similar to the test names
3. Inside weblogin.js, require the file in the position you want the test to run

**NOTE:** A new test file should be completely independent of all other tests besides test constants (i.e. the dummy email addresses)

## Writing Tests

A good overview for how to write Selenium tests can be found [here]http://samples.leanpub.com/selenium-webdriver-recipes-in-nodejs-sample.pdf
