// HTTPS Hack
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'

// Add promise support if this does not exist natively.
if (!global.Promise) {
  global.Promise = require('q')
}

// Setup Chai
var chai = require('chai')
var expect = chai.expect
chai.use(require('chai-http'))
var appConfig = require('../../config')
var SERVER = chai.request.agent(`${appConfig.server.protocol}://${appConfig.server.domain}:${appConfig.server.port}`)
var API = chai.request.agent(`${appConfig.server.protocol}://${appConfig.server.domain}:${appConfig.server.port}/api/v1`)

// Connect to DB
var mongoose = require('mongoose')
mongoose.Promise = global.Promise
var Models = require('../../models')
before(function (done) {
  mongoose.connect('mongodb://localhost:27017/auvenir', done)
})
// Testing Accounts
var auditor = {email:'auvauditor@gmail.com'}
var client = {email:'auvclient@gmail.com'}
var admin = {email:'qa@auvenir.com'}

describe('###### Login ######', function () {
  // Auditor Registration & Login
  describe('AUDITOR:', function() {
    it('Should create a new auditor', function(done) {
      SERVER.post('/login')
        .send({'email':auditor.email, type:'AUDITOR'})
        .then(function (res) {
           expect(res).to.have.status(200)
           expect(res.body.code).to.equal(200)
           expect(res.body.email).to.equal(auditor.email)
           expect(res.body.msg).to.equal('Awaiting approval!')
        })
        .then(done)
        .catch(done)
    })
    it('Should alert already registered auditor', function(done) {
      SERVER.post('/login')
        .send({'email':auditor.email, type:'AUDITOR'})
        .then(function (res) {
          expect(res.body.code).to.equal(200)
          expect(res.body.msg).to.equal('Awaiting approval!')
        })
        .then(done)
        .catch(done)
    })
    it('Should have created a new auditor user object in MongoDB', function(done) {
      Models.Auvenir.User.findOne({ email: auditor.email })
        .then(function (user) {
          expect(user.email).to.equal(auditor.email)
          expect(user.status).to.equal('WAIT-LIST')
          expect(user.type).to.equal('AUDITOR')
          expect(user.auth.access.token).to.be.a('string')
          auditor = user // Store auditor for later tests
        })
        .then(done)
        .catch(done)
    })
    it('Should cleanup the test auditor from MongoDB', function(done) {
      Models.Auvenir.User.remove({ email: auditor.email, type: 'AUDITOR' })
        .then(function (res) {
          expect(res.result.ok).to.equal(1)
          expect(res.result.n).to.equal(1)
        })
        .then(done)
        .catch(done)
    })
  })
  // Client Registration & Login
  describe('CLIENT:', function () {
    it('Create a new client', function (done) {
      SERVER.post('/login')
        .send({'email': client.email, type: 'CLIENT'})
        .then(function (res) {
          expect(res).to.have.status(200)
          expect(res.body.code).to.equal(200)
          expect(res.body.email).to.equal(client.email)
          expect(res.body.msg).to.equal('Awaiting approval!')
        })
        .then(done)
        .catch(done)
    })
    it('Alert already registered client', function (done) {
      SERVER.post('/login')
        .send({'email': client.email, type: 'CLIENT'})
        .then(function (res) {
          expect(res.body.code).to.equal(200)
          expect(res.body.msg).to.equal('Awaiting approval!')
        })
        .then(done)
        .catch(done)
    })
    it('Should have created a new client user object in MongoDB', function(done) {
      Models.Auvenir.User.findOne({ email: client.email })
        .then(function (user) {
          expect(user.email).to.equal(client.email)
          expect(user.status).to.equal('WAIT-LIST')
          expect(user.type).to.equal('CLIENT')
          expect(user.auth.access.token).to.be.a('string')
          client = user // Store client for later tests
        })
        .then(done)
        .catch(done)
    })
    it('Should cleanup the test client from MongoDB', function(done) {
      Models.Auvenir.User.remove({ email: client.email, type: 'CLIENT' })
        .then(function (res) {
          expect(res.result.ok).to.equal(1)
          expect(res.result.n).to.equal(1)
        })
        .then(done)
        .catch(done)
    })
  })
  // Admin Registration & Login
  describe('ADMIN:', function() {
    it('Should create a new admin', function(done) {
      SERVER.post('/login')
        .send({'email':admin.email})
        .then(function (res) {
           expect(res).to.have.status(200)
           expect(res.body.code).to.equal(202)
           expect(res.body.email).to.equal(admin.email)
           expect(res.body.msg).to.equal('Check your Email!')
        })
        .then(done)
        .catch(done)
    })
    it('Should alert already registered admin', function(done) {
      SERVER.post('/login')
        .send({'email':admin.email})
        .then(function (res) {
          expect(res.body.code).to.equal(202)
          expect(res.body.msg).to.equal('Check your Email!')
        })
        .then(done)
        .catch(done)
    })
    it('Should have created a new admin user object in MongoDB', function(done) {
      Models.Auvenir.User.findOne({ email: admin.email })
        .then(function (user) {
          expect(user.email).to.equal(admin.email)
          expect(user.status).to.equal('ONBOARDING')
          expect(user.type).to.equal('ADMIN')
          expect(user.auth.access.token).to.be.a('string')
          admin = user // Store admin for later tests
        })
        .then(done)
        .catch(done)
    })
    it('Should cleanup the test admin from MongoDB', function(done) {
      Models.Auvenir.User.remove({ email: admin.email, type: 'ADMIN' })
        .then(function (res) {
          expect(res.result.ok).to.equal(1)
          expect(res.result.n).to.equal(1)
        })
        .then(done)
        .catch(done)
    })
  })
})
