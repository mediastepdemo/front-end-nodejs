
module.exports = {
  testConf: {arg: 'test-conf', default: '', format: String, doc: 'Test configuration file(s) (comma separated list)'},
  server: {
    proxy: {arg: 'proxy', default: '', format: String, doc: 'Proxy for debugging'}
  },
  user: {
    email: {arg: 'email', default: 'myemail@auvenir.com', format: 'email', doc: 'User email address for test logins'},
    password: {arg: 'password', default: 'mypassword', format: String, doc: 'User password for test logins'}
  },
  finicity: {
    auditor: {arg: 'auditor', default: 'auvauditor@gmail.com', format: 'email'},
    client: {arg: 'client', default: 'auvclient@gmail.com', format: 'email'},
    admin: {arg: 'admin', default: 'qa@auvenir.com', format: 'email'}
  }
}
