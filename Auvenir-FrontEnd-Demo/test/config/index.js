/* eslint no-console: 0 */

const convict = require('convict')
const schema = require('./schema')
const request = require('request')
const fs = require('fs')
const _ = require('lodash')
let testConfig = null

try {
  const config = convict(schema)
  let testConf = config.get('testConf')
  if (testConf) {
    testConf = testConf.split(',')
    if (testConf.length === 1) {
      console.log(`Loading test configuration from the following file ${testConf[0]}`)
    } else {
      console.log(`Loading test configuration from the following list of files ${testConf}`)
    }
    config.loadFile(testConf)
  }
  config.validate({strict: true})
  testConfig = config.getProperties()
  console.log(`Effective test configuration: ${JSON.stringify(testConfig, null, 4)}`)
} catch (err) {
  console.log('Unable to load test configuration', err)
  process.exit(err.code || 1)
}

module.exports = testConfig
