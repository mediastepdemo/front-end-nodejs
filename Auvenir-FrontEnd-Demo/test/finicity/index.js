#!/usr/bin/env node
const path = require('path')
global.rootDirectory = path.normalize(path.join(__dirname, '..', '..'))
const appConfig = require(rootDirectory + '/config')
const testConfig = require(rootDirectory + '/test/config')
const Logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = Logger.init(__filename)
const _ = require('lodash')
const async = require('async')
const TestAPIV1 = require(rootDirectory + '/test/api/TestAPIV1')
const institutionInfo = require(rootDirectory + '/finicity/plugins/institutionInfo').finicity
const errorCodes = require(rootDirectory + '/finicity/plugins/errorCodes')

info('Finicity API Test')

const emailID = 'tomwqw@gmail.com'
const institutionID = institutionInfo.finbank.id
const businessID = '5938765710f40e884dfbbf37'
const engagementID = '5938765710f40e884dfbbf38'
const bankUserid = 'demo'
const bankPassword = 'go'
let sessionID = null
let loginInfo = null
let mfaInfo;
let mfaType;
let accounts;

const api = new TestAPIV1()

api.addEventHandler(api.FN_LOGIN_FAILURE, '1', (tag, resp) => {
  error(tag)
})

api.addEventHandler(api.FN_SELECTION_SECURITY_FAILURE, '1', (tag, resp) => {
  error(tag)
})

api.addEventHandler(api.FN_MFA_TIMEOUT, '1', (tag, resp) => {
  error(tag)
})

api.addEventHandler(api.FN_LOGIN_SECURITY, '1', (tag, resp) => {
  if (data.result.mfaChallenge) {
    warn(`MFA challenge`)
  } else {
    if (resp.msg) {
      error(resp.msg);
      return
    }
    error(`Not able to create the MFA challenge for the selected institution.`)
  }
})

api.addEventHandler(api.FN_SELECTION_SECURITY, '1', (tag, resp) => {
  if (resp.result.mfaChallenge && data.result.account) {
    warn(`MFA challenge`)
  } else {
    if (resp.msg) {
      error(resp.msg);
      return
    }
    error(`Not able to create the MFA challenge for the selected institution.`)
  }
})

api.addEventHandler(api.LOGIN_RESPONSE, '1', (tag, resp) => {
  api.finicityConnect({
    code: 0,
    currentBusinessID: businessID,
    institutionID: institutionID
  })
})

api.addEventHandler(api.FN_CONNECT, '1', (tag, resp) => {
  sessionID = resp.sessionID
  api.finicityLoginFormRequest({
    sessionID: sessionID
  })
})

api.addEventHandler(api.FN_LOGIN_FORM_RESPONSE, '1', (tag, resp) => {
  loginInfo = resp.loginInfo
  loginInfo[0].value = bankUserid
  loginInfo[1].value = bankPassword
  api.finicityLoginSubmit({
    code: 0,
    sessionID: sessionID,
    loginInfo: loginInfo
  })
})

api.addEventHandler(api.FN_ACCOUNT_LIST, '1', (tag, resp) => {
  accounts = resp.result
  const account1 = accounts[3]
  account1.answer = true
  // const account2 = accounts[4]
  // account2.answer = true
  const selectedAccounts = [account1]
  api.finicityAccountListSelectedRequest({
    code: 0,
    sessionID: sessionID,
    accountsSelected: selectedAccounts
  })
})

api.addEventHandler(api.FN_ACCOUNT_LIST_SELECT_RESPONSE, '1', (tag, resp) => {
  api.finicityLoginComplete(({
    code: 0,
    sessionID: sessionID,
    engagementID: engagementID
  }))
})

api.addEventHandler(api.FN_)

api.startSession(emailID)

// let reply    = {code: 0, result: {userInfo: userInfo}};
// socket.emit('fn-loginFormRequest', reply);
//
//
// // Initialize variables.
// function initialize(reason) {
//   if (reason === INIT_STARTUP) {
//     userInfo = null;
//   }
//
//   if (reason === INIT_STARTUP || reason === INIT_LOGIN) {
//     loginInfo   = null;
//     institution = null;
//     bankAccLogo = null;
//   }
//
//   mfaInfo     = null;
//   mfaType     = null;
//   accountInfo = null;
// }

// /**
//  * Responsible for call createAccountList function with accounts data
//  * @param  {object} data  - accounts data
//  */
// socket.on('fn-accountList', function(data) {
//   console.log('Socket event: Received fn-accountList: ', data);
//
//   if (data.code !== 0) {
//     displayError(data);
//   } else if (!data.result || !data.accountTypes) {
//     if (!data.msg) {
//       data.msg = 'Not able to create the list of accounts for the selected institution.';
//     }
//     displayError(data);
//   } else {
//     createAccountList(data.result, data.accountTypes);
//     $('#accountsNextButton').hide();
//   }
// });
//
//
// //On submit accountList call showModal to show loginSuccessModal and after 5 sec hide it
// socket.on('fs-accountListSelectedResponse', function(data) {
//   console.log('Socket event: Received fs-accountListSelectedResponse: ', data);
//
//   if (data.code !== 0) {
//     displayError(data);
//   } else {
//     console.log('Socket event: Emitted fn-loginComplete');
//     socket.emit('fn-loginComplete', reply);
//     showModal('.loginSuccessModal');
//     setTimeout(function() {
//       $('.loginSuccessModal').addClass('hidden');
//       window.close();
//     }, 5000);
//   }
// });
//
// //Responsible for emit fn-loginExit function and close exitBankModal
// let onLoginExit = function() {
//   console.log('Socket event: Emitted fn-loginExit');
//   socket.emit('fn-loginExit', reply);
//   $('.exitBankModal').addClass('hidden');
//   window.close();
// };
//
// //Responsible for on click exitClose call onLoginExit function
// $('.exitClose').click(function() {
//   onLoginExit();
// });
//
// //Responsible for on click loginExitBTN call loginExit function
// $('.loginExitBTN').click(function() {
//   onLoginExit();
// });
//
// let onErrorExit = function() {
//   console.log('Socket event: Emitted fn-loginErrorExit');
//   socket.emit('fn-loginErrorExit', reply);
//   $('.errorModal').addClass('hidden');
//   window.close();
// };
//
// $('.exitErrorClose').click(function() {
//   onErrorExit();
// });
//
// //Responsible for hide loginError modal on click Cancle button
// $('.loginErrorBTN').click(function() {
//   onErrorExit();
// });
//
// // Responsible for emit (login/complete) and hide loginSuccessModal modal
// let onSuccessExit = function() {
//   // console.log('Socket event: Emitted fn-loginComplete');
//   // socket.emit('fn-loginComplete', reply);
//   $('.loginSuccessModal').addClass('hidden');
//   window.close();
// };
//
// //loginSuccessModal on click close icon call onSuccessExit function
// $('.exitSuccessClose').click(function() {
//   onSuccessExit();
// });
//
// //loginSuccessModal on click Continue button call onSuccessExit function
// $('.loginSuccessBTN').click(function() {
//   onSuccessExit();
// });
//
// /**
//  * Responsible for close current Modal and open bank exit modal if cancle button click show previous modal
//  * @param  {object} event -mouse event data
//  */
// let onClose = function(event) {
//   let goBackModal = event.toElement.offsetParent;
//   $(event.toElement.offsetParent).addClass('hidden goBackModal');
//   $('.exitBankModal').removeClass('hidden');
//   $('.exitBankModal').addClass('close');
//   if ($('.exitBankModal').hasClass('close')) {
//
//     $('.loginExitBackBTN').click(function(event) {
//       $('.goBackModal').removeClass('hidden');
//       $('input[type=text]').val('');
//       $('input[type=password]').val('');
//       $('.exitBankModal').addClass('hidden');
//       $('.modal').removeClass('goBackModal');
//       $('.securityImg').removeClass('active');
//       $('.submitSecurity').hide();
//       $('.submitLogin').hide();
//       $('#loginInput0').focus();
//       $('.itemdrop').prop('selectedIndex',0);
//       $('.securityAns0, .securityAnsBox, .itemdrop').focus();
//       $('#accountsNextButton').hide();
//       selectValidate();
//       $('.accountList').prop('checked', false);
//     });
//   }
// };
//
// /**
//  * Responsible for on click close call onClose function with mouse event data
//  * @param  {object} event -mouse click event object
//  */
// $('.close').click(function(event) {
//   // console.log(event);
//   onClose(event);
// });
//
// // on click Retry button(in loginFailed Modal) call showModal with call name loginModal and clear input field
// $('.loginFailedBTN').click(function() {
//   if (accountInfo) {
//     showModal(accountsSelectModal);
//   } else {
//     console.log('Socket event: Emitted fn-loginFormRequest: ', reply);
//     socket.emit('fn-loginFormRequest', reply);
//   }
//   // showModal('.loginModal');
//   // $('input[type=text]').val('');
//   // $('input[type=password]').val('');
//   // $('.submitLogin').hide();
//   // $('#loginInput0').focus();
//   // showModal('.loginModal');
// });
//
// $('#m-bi-sendBtn').click(function() {
//   console.error('yeppers');
// })
//
// // on click Back button(in selectAccount Modal) call showModal with call name loginModal and clear input field
// $('.selectAccountBackBTN').click(function() {
//   console.log('Socket event: Emitted fn-loginFormRequest: ', reply);
//   socket.emit('fn-loginFormRequest', reply);
//   $('.accountList').prop('checked', false);
//   $('.accountsNextBTN').hide();
//   // $('input[type=text]').val('');
//   // $('input[type=password]').val('');
//   // $('.submitLogin').hide();
//   // $('.accounts').text('');
//   // showModal('.loginModal');
//   // $('#loginInput0').focus();
// });
//
// //on click Next Button(in selectAccount Modal) push checked account details in selectedAccountList and emit fn-accountListSelectedRequest
// $('.accountsNextBTN').click(function() {
//   selectedAccountsList = [];
//   $('.accountList:checked').each(function(index, ele) {
//     let dropValue = $(ele)[0].parentNode.parentNode.nextSibling.nextSibling.nextSibling.childNodes[0].value;
//     let accountId = $(ele).attr('data-id');
//     for (let i = 0, il = accountInfo.length; i < il; i++) {
//       if (accountInfo[i].id == accountId) {
//         accountInfo[i].answer = dropValue;
//         selectedAccountsList.push(accountInfo[i]);
//       }
//     }
//   });
//
//   let reply = {code: 0, result: {userInfo: userInfo, accountsSelected: selectedAccountsList}};
//   console.log('Socket event: Emitted fn-accountListSelectedRequest: ', reply);
//   socket.emit('fn-accountListSelectedRequest', reply);
//
//   showModal('.accountsLoadingModal');
//   // $('.accounts').text('');
// });
//
// /**
//  * Responsible to call onSubmitLogin function with mouse event data
//  * @param  {object} event -mouse click event object
//  */
// $('.submitLogin').click(function(event) {
//   onSubmitLogin(event);
// });
//
// /**
//  * Responsible to call onSubmitSecurity function with mouse event data
//  * @param  {object} event -mouse click event object
//  */
// $('.submitSecurity').click(function(event) {
//   onSubmitSecurity(event);
// });
//
// /**
//  * Login field validation if value null then submit button hide
//  */
// let loginValidate = function(){
//   let loginInput = document.getElementsByClassName('loginFormInput');
//   for (i = 0; i < loginInput.length; i++) {
//     if (loginInput[i].value === '') {
//       $('.submitLogin').hide();
//       return false;
//     } else {
//       $('.submitLogin').show();
//     }
//   }
// };
//
// /**
//  * Responsilbe for create login form in loginModal as per data received from server and show Modal
//  * @param  {object} loginDetails -login form data
//  */
// let createLoginForm = function(loginDetails) {
//   initialize(INIT_LOGIN);
//   loginInfo   = loginDetails.loginInfo;
//   institution = loginDetails.institutionRecord;
//
//   loginInfo = sort(loginInfo, 'displayOrder');
//   loginInfo.forEach(function(currentValue, index, arr) {
//
//     let inputType = (currentValue.mask === 'true') ? 'password' : 'text';
//     let dom = '<div class="field">' +
//       '<label>' + currentValue.description + '</label>' +
//       '<input ' + ' type=' + inputType +
//       ' id=' + 'loginInput' + index + ' class="loginFormInput" name=' + currentValue.id +' autofocus>' + '</div>';
//     let instruction = '<p class="loginFormInstructions">'+currentValue.instructions+'</p>';
//     $('.loginForm').append(dom);
//     if (currentValue.instructions) {
//       $('.loginForm').append(instruction);
//     }
//   });
//   let forgotPassDiv = ' <label><a class="forgotPassword" href="' + institution.urlLogonApp + '">Forgot Password?</a></label>';
//   $('.institutionName').text(' ' + institution.name + ' Login');
//
//   $('input[type=password]').bind('change paste keyup', function() {
//     loginValidate();
//   });
//
//   $('input[type=text]').bind('change paste keyup', function() {
//     loginValidate();
//   });
//   // bankAccLogo = '/img/bank_noLogo.png';
//   // $('.logoimage').attr('src', '/img/bank_noLogo.png');
//
//   $('.loginForm').append(forgotPassDiv);
//
//   $.ajax({
//     url: 'https://logo.clearbit.com/' + institution.urlHomeApp + '',
//     type: 'HEAD',
//     error:
//       function() {
//         console.log('Logo not available from clearbit.');
//         bankAccLogo = '/img/bank_noLogo.png';
//         $('.logoimage').attr('src', bankAccLogo);
//       },
//     success:
//       function() {
//         $('.logoimage').attr('src', 'https://logo.clearbit.com/' + institution.urlHomeApp + '');
//         bankAccLogo = 'https://logo.clearbit.com/' + institution.urlHomeApp + '';
//       }
//   });
//
//   showModal('.loginModal');
//   loginValidate();
//   $('#loginInput0').focus();
// };
//
// /**
//  * Responsilbe for getting login input details and emit fn-loginSubmit
//  * @param  {object} event -mouse event data
//  */
// let onSubmitLogin = function(event) {
//   event.preventDefault();
//   for (let i = 0, il = loginInfo.length; i < il; i++) {
//     loginInfo[i].value = $('input[name=' + loginInfo[i].id + ']').val();
//   }
//
//   let reply = {code: 0, result: {userInfo: userInfo, loginInfo: loginInfo}};
//   console.log('Socket event: Emitted fn-loginSubmit: ', reply);
//   socket.emit('fn-loginSubmit', reply);
//
//   showModal('.loginLoaderModal');
//   $('input[type=text]').val('');
//   $('input[type=password]').val('');
//   $('.loginForm').text('');
//
//   return false;
// };
//
// /**
//  * Responsible for add active class if image click in sequrity question
//  * @param  {object} event -mouse click event
//  */
// let onSelectImage = function(event) {
//   $('.securityImg').removeClass('active');
//   $(event.currentTarget).addClass('active');
//   $('.submitSecurity').show();
// };
//
// /**
//  * Security Question modal field validation if value null then submit button hide
//  */
// let validate = function() {
//   let AnswerInput = document.getElementsByName('question');
//   if (AnswerInput) {
//     for (i = 0; i < AnswerInput.length; i++) {
//       if (AnswerInput[i].value === '') {
//         $('.submitSecurity').hide();
//         return false;
//       } else {
//         $('.submitSecurity').show();
//       }
//     }
//   }
// };
//
// //in securityQuestion modal select drop down have value then submit button show.
// let selectValidate = function() {
//   if ($('.itemdrop').val()) {
//     $('.submitSecurity').show();
//   }
// };
//
// /**
//  * Responsible for setup security question as per data received from server and show Modal
//  * @param  {object} mfaDetails -security question data
//  */
// let createSecurityQuestion = function(theMfaType, mfaDetails) {
//   mfaInfo = mfaDetails;
//   mfaType = theMfaType;
//
//   $('.selectedAccountTable tbody').text('');
//   if (mfaType === MFATYPE_SELECTION) {
//     $('.selectedAccountDiv').css('display', 'block');
//     let tableRowTemplate = '<tr><td>{{accountName}}</td><td>{{accountNumber}}</td></tr>';
//     let dom = $(tableRowTemplate
//       .replace('{{accountName}}',   mfaDetails.account.name)
//       .replace('{{accountNumber}}', mfaDetails.account.number));
//
//     $('.selectedAccountTable tbody').append(dom);
//   }
//
//   let questions = mfaInfo.mfaChallenge.questions;
//   $('.securityQuHeader').text('');
//   $('.securityQuestions').text('');
//   $('.tiny.images').text('');
//   questions.forEach(function(qData,index) {
//
//     let textDom = '<label class="questionText">' + qData.text + '</label>';
//     $('.securityQuestions').append(textDom);
//
//     if (qData.image && qData.imageChoices) {
//       $('.securityQuestions').append('<br/><img class="securityImgQuestion" src=' + qData.image + '> <br/>');
//       let imageArray = qData.imageChoices;
//       imageArray.forEach(function(currentValue, index, arr) {
//         let imgDom = '<label><img class="ui image securityImg" id="image1" onclick="onSelectImage(event)" src=' + currentValue.imageChoice + '>' +
//           '<input id="radio1" name="radiobtn" type="radio" value=' + currentValue.value + ' autofocus style="opacity:0;position: absolute;top: 177px;">' +
//           '</label>';
//         $('.tiny.images').append(imgDom);
//         $('.submitSecurity').hide();
//       });
//
//     } else if (qData.image) {
//       $('.securityQuestions').append('<br/><img class="securityImgQuestion" src=' + qData.image + '> <br/>');
//       $('.securityQuestions').append('<br/><input type="text" name="question" class="securityAnsBox">');
//
//     } else if (qData.choices) {
//       let choicesArray = qData.choices;
//       let div = '';
//       choicesArray.forEach(function(choiceVal) {
//         let choiseDom = '<option class="item" value=' + choiceVal.value + '>' + choiceVal.choice + '</option>';
//         div += choiseDom;
//       });
//       let finalDom = '<br/><select class="ui selection dropdown itemdrop">' + div + '</select>';
//       $('.securityQuestions').append(finalDom);
//
//     } else if (qData.imageChoices) {
//       let imageChoicesArray = qData.imageChoices;
//       imageChoicesArray.forEach(function(currentValue, index, arr) {
//         let imgDom = '<label><img class="ui image securityImg" id="image1" onclick="onSelectImage(event)" src=' + currentValue.imageChoice + '>' +
//           '<input id="radio1" name="radiobtn" type="radio" value=' + currentValue.value + ' style="opacity:0;position: absolute;top: 177px;">' +
//           '</label>';
//         $('.tiny.images').append(imgDom);
//         $('.submitSecurity').hide();
//       });
//
//     } else {
//       $('.securityQuestions').append('<br/><input type="text" name="question" class="securityAns' + index + '"><br/><br/>');
//     }
//   });
//
//   $('input[name="question"]').bind("change paste keyup", function() {
//     validate();
//   });
//
//   showModal('.securityQuestionModal');
//   validate();
//   selectValidate();
//   $('.securityAns0, .securityAnsBox, .itemdrop').focus();
// };
//
//
// /**
//  * Responsible for getting input details on securityModal and emit fn-loginSecuritySubmit with details
//  * @param  {object} event -mouse click event
//  */
// let onSubmitSecurity = function(event) {
//   event.preventDefault();
//   let questions   = mfaInfo.mfaChallenge.questions;
//   let newFormData = mfaInfo.mfaChallenge.questions;
//
//   showModal('.loginLoaderModal');
//
//   questions.forEach(function(question, Index) {
//     if (question.choices) {
//       question.answer = $('.itemdrop').val();
//     } else if (question.imageChoices) {
//       question.answer =  document.querySelector('input[name="radiobtn"]:checked').value;
//     } else if (question.image) {
//       question.answer = $('.securityAnsBox').val();
//     } else {
//       newFormData.forEach(function(list, Index) {
//         mfaInfo.mfaChallenge.questions[Index].answer = $('.securityAns' + Index + '').val();
//       });
//     }
//   });
//
//   let reply = {code: 0, result: {userInfo: userInfo, mfaInfo: mfaInfo}};
//   if (mfaType === MFATYPE_LOGIN) {
//     console.log('Socket event: Emitted fn-loginSecuritySubmit: ', reply);
//     socket.emit('fn-loginSecuritySubmit', reply);
//   } else {
//     console.log('Socket event: Emitted fn-selectionSecuritySubmit: ', reply);
//     socket.emit('fn-selectionSecuritySubmit', reply);
//   }
//
//   mfaInfo = null;
//   mfaType = null;
//
//   // after submit clear dom element
//   $('.securityQuHeader').text('');
//   $('.securityQuestions').text('');
//   $('.tiny.images').text('');
// };
//
// //on checkbox checked show next button in selectAccount Modal
// let onChecked = function(event) {
//   let checkbox = $('input[type=checkbox]') || [];
//   $('#accountsNextButton').hide();
//   for (let i = 0, il = checkbox.length; i < il; i++) {
//     if (checkbox[i].checked) {
//       if (checkbox[i].value === UNKNOWN_ACCOUNTTYPE) {
//         let dropValue = checkbox[i].parentNode.parentNode.nextSibling.nextSibling.nextSibling.childNodes[0].value;
//         if (!dropValue) {
//           $('#accountsNextButton').hide();
//           return;
//         }
//       }
//       $('#accountsNextButton').show();
//     }
//   }
// };
//
// //Responsible for call onChecked function on checkbox checked or unchecked
// let checkboxEvent = function() {
//   let checkbox = $('input[type=checkbox]') || [];
//   for (let i = 0, il = checkbox.length; i < il; i++) {
//     checkbox[i].addEventListener('change', onChecked);
//   }
// };
//
// /**
//  * Responsible for create account list as per data received from server and show Modal
//  * @param  {object} accountList -bank account data
//  */
// let createAccountList = function(accountList, accountTypes) {
//   initialize(INIT_ACCOUNTLIST);
//   $('.accounts').text('');
//   accountInfo = accountList;
//
//   for (let i = 0, il = accountInfo.length; i < il; i++) {
//     let accountValue = accountInfo[i].type;
//     if (accountInfo[i].type === UNKNOWN_ACCOUNTTYPE) {
//       let selectTag = '<option class="AccountTypeItem" value="">' + 'Select account type' + '</option>';
//       let div = selectTag;
//
//       for (let j = 0, jl = accountTypes.length; j < jl; j++) {
//         let accountTypeDom = '<option class="AccountTypeItem" value=' + accountTypes[j] + '>' + accountTypes[j] + '</option>';
//         div += accountTypeDom;
//       }
//       let finalDom = '<select class="ui selection dropdown accountTypeDrop"  onchange="onChecked()">' + div + '</select>';
//       accountValue = finalDom;
//     }
//
//     let oneAccount = '<div class="ui list"><div class="item"><span class="ui left floated">' +
//       '<label class="control control--checkbox"><input type="checkbox" class="accountList" name="accountList" data-id="' +
//       accountInfo[i].id + '" onclick="checkboxEvent();" value=' + accountInfo[i].type + '><div class="control__indicator">' +
//       '</div><img class="ui middle image bankAccLogo"></span><span class="header">' +
//       accountInfo[i].name + '&nbsp;' + accountInfo[i].number + '</span><br/><span class="accountDetails">' +
//       accountValue + '&nbsp;$' + accountInfo[i].balance + '</span></label></div></div>';
//     $('.accounts').append(oneAccount);
//   }
//   $('.bankAccLogo').attr('src', bankAccLogo);
//   showModal('.accountsSelectModal');
// };
//
// /**
//  * Responsible for sort array details
//  * @param  {object} array  -required to sort data
//  * @param  {string} sortBy -value by which sort the data
//  * @return {object}        -sorted data
//  */
// function sort(array, sortBy) {
//   let temp = null;
//   for (let i = 0, il = array.length - 1; i < il; i++) {
//     if (array[i][sortBy] > array[i + 1][sortBy]) {
//       temp = array[i];
//       array[i] = array[i + 1];
//       array[i + 1] = temp;
//     }
//   }
//   return array;
// }
//
// //depricated code
// //Responsible for setup margin-left and margin-top
// let positionModal = function() {
//   let modals = $('.modal');
//   for (let i = 0, il = modals.length; i < il; i++) {
//     let modal = modals[i];
//     modal.style['margin-left'] = '-' + (modal.clientWidth / 2) + 'px';
//     modal.style['margin-top'] = '-' + (modal.clientHeight / 2) + 'px';
//   }
// };
//
// // Responsible for call checkboxEvent function
// function ready() {
//   // positionModal();
//   checkboxEvent();
// }
// // document.body.onload = positionModal;
//
// //on ready call ready function
// ready();
