// Setup Chai
const chai = require('chai')
const expect = chai.expect

// Setup Selenium Chrome Browser
const webdriver = require('selenium-webdriver')
const Key = webdriver.Key
const By = webdriver.By
const until = webdriver.until
var Chrome = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build()
Chrome.manage().window().setSize(1400, 1000)

describe('~ Landing Page Checks ~', function () {
  this.timeout(5000)

  before(function (done) {
    Chrome.get('https://localhost').then(function () {
      done()
    })
  })

  it('Auvenir running at https://localhost', function (done) {
    Chrome.wait(until.titleIs('Auvenir - Automating financial audit!'), 5000).then(function () {
      done()
    })
  })

  after(function (done) {
    Chrome.quit().then(function () {
      done()
    })
  })
})
