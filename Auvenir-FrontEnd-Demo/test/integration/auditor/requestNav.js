// Setup Chai
const chai = require('chai')
const expect = chai.expect

// Setup HTTP agent
chai.use(require('chai-http'))
var appConfig = require('../../../config')
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
var SERVER = chai.request.agent(`${appConfig.server.protocol}://${appConfig.server.domain}`)
var API = chai.request.agent(`${appConfig.server.protocol}://${appConfig.server.domain}`)

// Setup Selenium Chrome Browser
const webdriver = require('selenium-webdriver')
const By = webdriver.By
const until = webdriver.until
var Chrome = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build()
Chrome.manage().window().setSize(1400, 1000)

// Import Constants
const constants = require('../constants')
var loginToken

describe('~ Create a New Engagement ~', function () {
  this.timeout(20000)

  before(function (done) {
    Chrome.get('https://localhost').then(function () {
      done()
    })
  })

  it('Login with an Auditor Account', function (done) {
    Chrome.findElement(By.id('emailField')).sendKeys(constants.auditorEmail)
    Chrome.findElement(By.id('emailField')).sendKeys(webdriver.Key.ENTER)
    Chrome.wait(until.elementIsVisible(Chrome.findElement(By.id('audLandEmailSent'))), 2000)
    .then(function () {
      done()
    })
  })

  it('Responds with "Check your Email!"', function (done) {
    Chrome.wait(until.elementTextIs(Chrome.findElement(By.id('audLand-modal-loginHeader')), 'Check your Email!'), 7500)
    .then(function () {
      done()
    })
  })

  it('Should get the login token from /getToken', function (done) {
    SERVER.get(`/getToken?email=${constants.auditorEmail}`)
      .then(function (res) {
        expect(res).to.have.status(200)
        expect(res.body.token).to.be.a('string')
        loginToken = res.body.token
      })
      .then(done)
      .catch(done)
  })

  it('Login with token', function (done) {
    Chrome.get(`https://localhost/checkToken?email=${constants.auditorEmail}&token=${loginToken}`)
    Chrome.wait(until.elementIsVisible(Chrome.findElement(By.id('newAuditBtn'))), 10000)
    .then(function () {
      done()
    })
  })

  it('Opens New Engagement Screen', function (done) {
    Chrome.sleep(500)
    Chrome.findElement(By.id('newAuditBtn')).click()
    Chrome.wait(until.elementIsVisible(Chrome.findElement(By.id('engagement-main'))), 5000)
    .then(function () {
      done()
    })
  })
})

describe('~ Tests Request Navigation ~', function () {
  it('Opens Requests Page', function (done) {
    Chrome.findElement(By.id('engagementRequestsLink')).click()
    Chrome.sleep(500)
    Chrome.wait(until.elementIsVisible(Chrome.findElement(By.id('requests-container'))), 5000)
    .then(function () {
      done()
    })
  })

  it('Creates New Category', function (done) {
    Chrome.findElement(By.id('req-nav-newCatBtn')).click()
    .then(function () {
      done()
    })
  })

  it('Name New Category', function (done) {
    Chrome.findElement(By.className('req-nav-catInput')).sendKeys('First New Category')
    .then(function () {
      done()
    })
  })

  it('Creates Second New Category', function (done) {
    Chrome.sleep(500)
    Chrome.wait(until.elementIsVisible(Chrome.findElement(By.id('req-nav-newCatBtn'))), 500)
    Chrome.findElement(By.id('req-nav-newCatBtn')).click()
    .then(function () {
      done()
    })
  })

  it('Name Second New Category', function (done) {
    Chrome.wait(until.elementIsVisible(Chrome.findElement(By.id('req-nav-catInput'))), 500)
    Chrome.findElement(By.className('req-nav-catInput')).sendKeys('Second New Category')
    .then(function () {
      done()
    })
  })
})

// Step One - Create Category

// Step Two - Add request

// Step Three - Change request name (Test for long names)

// Step Four - Create Subrequest (Test to limit of nested subs)

// Step Five - Moving Subrequest to request

// Step Six - Move subrequest back to other Subrequest

// Step Seven - Move request to different category

// TODO  check Activity Feed after required steps
