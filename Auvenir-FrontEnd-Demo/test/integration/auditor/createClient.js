// Setup Chai
const chai = require('chai')
const expect = chai.expect

// Setup HTTP agent
chai.use(require('chai-http'))
var appConfig = require('../../../config')
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
var SERVER = chai.request.agent(`${appConfig.server.protocol}://${appConfig.server.domain}`)
var API = chai.request.agent(`${appConfig.server.protocol}://${appConfig.server.domain}`)

// Setup Selenium Chrome Browser
const webdriver = require('selenium-webdriver')
const By = webdriver.By
const until = webdriver.until
var Chrome = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build()
Chrome.manage().window().setSize(1400, 1000)

// Import Constants
const constants = require('../constants')
var loginToken

describe('~ Create a New Client ~', function () {
  this.timeout(20000)

  before(function (done) {
    Chrome.get('https://localhost').then(function () {
      done()
    })
  })

  it('Login with an Auditor Account', function (done) {
    Chrome.findElement(By.id('emailField')).sendKeys(constants.auditorEmail)
    Chrome.findElement(By.id('emailField')).sendKeys(webdriver.Key.ENTER)
    Chrome.wait(until.elementIsVisible(Chrome.findElement(By.id('audLandEmailSent'))), 2000)
    .then(function () {
      done()
    })
  })

  it('Responds with "Check your Email!"', function (done) {
    Chrome.wait(until.elementTextIs(Chrome.findElement(By.id('audLand-modal-loginHeader')), 'Check your Email!'), 7500)
    .then(function () {
      done()
    })
  })

  it('Should get the login token from /getToken', function (done) {
    SERVER.get(`/getToken?email=${constants.auditorEmail}`)
      .then(function (res) {
        expect(res).to.have.status(200)
        expect(res.body.token).to.be.a('string')
        loginToken = res.body.token
      })
      .then(done)
      .catch(done)
  })

  it('Login with token', function (done) {
    Chrome.get(`https://localhost/checkToken?email=${constants.auditorEmail}&token=${loginToken}`)
    Chrome.wait(until.elementIsVisible(Chrome.findElement(By.id('h-clientListLink'))), 4000)
    .then(function () {
      done()
    })
  })

  it('Opens Create New Client Screen', function (done) {
    Chrome.findElement(By.id('h-clientListLink')).click()
    Chrome.findElement(By.id('clientList-newClientBtn')).click()
    .then(setTimeout(function () {
      Chrome.wait(until.elementIsVisible(Chrome.findElement(By.id('add-client-super-container'))), 4000)
    }), 1000)
    .then(function () {
      done()
    })
  })

  it('Creates a New Client', function (done) {
    Chrome.findElement(By.id('m-ac-legalName')).sendKeys('ABC Construction')
    Chrome.findElement(By.id('m-ac-industry')).sendKeys('Construction')
    Chrome.findElement(By.id('m-ac-accountingFramework')).sendKeys('GAAP')
    Chrome.findElement(By.id('m-ac-streetAddress')).sendKeys('140 Adelaide St W.')
    Chrome.findElement(By.id('m-ac-unit')).sendKeys('120')
    Chrome.findElement(By.id('m-ac-city')).sendKeys('Toronto')
    Chrome.findElement(By.id('m-ac-stateProvince')).sendKeys('Ontario')
    Chrome.findElement(By.id('m-ac-country')).sendKeys('Canada')
    Chrome.findElement(By.id('m-ac-postalCode')).sendKeys('M5V2K2')
    Chrome.findElement(By.id('m-ac-fullName')).sendKeys('Oliver Jones')
    Chrome.findElement(By.id('m-ac-email')).sendKeys(constants.clientEmail)
    Chrome.findElement(By.id('m-ac-phoneNumber')).sendKeys('5555555555')
    Chrome.findElement(By.id('add-client-super-container')).click()
    Chrome.findElement(By.id('m-ac-addBtn')).click()
    Chrome.wait(until.elementIsNotVisible(Chrome.findElement(By.id('add-client-super-container'))), 10000)
    .then(function () {
      done()
    })
  })

  after(function (done) {
    Chrome.quit().then(function () {
      done()
    })
  })
})
