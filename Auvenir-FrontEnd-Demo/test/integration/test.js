// Setup Chai
const chai = require('chai')

describe("--------====== Auvenir Test Suite ======--------\n", function () {
  require('./public/landing')

  describe("\n # Admin # \n", function () {
    require('./auditor/register')
    require('./admin/register')
    require('./admin/emailLogin')
    require('./admin/onboarding')
    require('./admin/activateAuditor')
  })

  describe("\n # Auditor # \n", function () {
    require('./auditor/emailLogin')
    require('./auditor/onboarding')
    require('./auditor/createClient')
    require('./auditor/createEngagement')
  })

  describe("\n # Client # \n", function () {
    require('./client/onboarding')
  })
  //require('./user/inbox') // Lucy
  //require('./auditor/requestNav') // Trevor
  //require('./auditor/requestDetails') // Trevor
  //require('./auditor/activityFeed') // Lucy
  //require('./user/deactivate') // Justin
  //require('./user/feedback') // Justin
  //require('./auditor/archiveEngagement')
})

// Move Files
// modal/message (template for message popups)
// modal/custom (template for modals)
// views/fileManager (login should be in components)
// views/Settings (split auditor/admin/client into components)
// views/settings/cards/* should all be one
// views/settings/cards/cart-integrations/CardIntegration with onboarding
// widgets/callToAction moves to components rendered from client dashboard
// widgets/gdriveManager should have a folder for front end plugins

// Missing Functionality
// modal/edit-client
// modal/delete-client

// Unknown Functionality
// modal/submit-audit
// widgets/Widget_team (R2?)

// Bonus
// modal/offline
// Improve test runner

// Potential R2 Tests
// require('./auditor/ChangeTemplate')
// require('./auditor/RemoveClient')

// Potential R3
// Connections
