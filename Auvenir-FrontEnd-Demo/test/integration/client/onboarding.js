// Setup Chai
const chai = require('chai')
const expect = chai.expect
const path = require('path')

// Setup HTTP agent
chai.use(require('chai-http'))
var appConfig = require('../../../config')
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
var SERVER = chai.request.agent(`${appConfig.server.protocol}://${appConfig.server.domain}`)
var API = chai.request.agent(`${appConfig.server.protocol}://${appConfig.server.domain}`)

// Setup Selenium Chrome Browser
const webdriver = require('selenium-webdriver')
const By = webdriver.By
const until = webdriver.until
var Chrome = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build()

// Import Constants
const constants = require('../constants')
var loginToken
var engagementID

describe('~ Complete Client Onboarding ~', function () {

  this.timeout(10000);

  before(function (done) {
    Chrome.get('https://localhost').then(function () {
      done()
    })
  })

  it('Should get the Login Token from /getToken', function (done) {
    SERVER.get(`/getToken?email=${constants.clientEmail}`)
      .then((res) => {
        expect(res).to.have.status(200)
        expect(res.body.token).to.be.a('string')
        loginToken = res.body.token
      })
      .then(done)
      .catch((err) => {
        done(err)
      })
  })

  it('Should get the EngagementID from /getEID', (done) => {
    SERVER.get(`/getEID?email=${constants.clientEmail}`)
      .then((res) => {
        expect(res).to.have.status(200)
        expect(res.body.eid).to.be.a('string')
        engagementID = res.body.eid
      })
      .then(done)
      .catch((err) => {
        done(err)
      })
  })

  it('Client accepts invitation', (done) => {
    Chrome.get(`https://localhost/acceptInvite?email=${constants.clientEmail}&token=${loginToken}&eid=${engagementID}`)
    Chrome.wait(until.elementIsVisible(Chrome.findElement(By.id('OnboardingPage'))), 5000)
      .then(() => {
        done()
      })
  })

  it('Completes Step 1 (Personal)', (done) => {
    Chrome.actions()
      .click(Chrome.findElement(By.id('agreement-personal')))
      .click(Chrome.findElement(By.id('personal-coninueBtn')))
      .perform()
      .then(() => {
        done()
      })
  })

  it('Completes Step 2 (Business)', (done) => {
    let BusinessFlow = Chrome.controlFlow()

    BusinessFlow.async(() => {
      chooseFiscalYear
      chooseAccFramwork
      goToNext
    })

    let chooseFiscalYear = BusinessFlow.execute(() => {
      Chrome.findElement(By.id('fiscal-year-end')).click()
      Chrome.findElement(By.xpath('//*[@id="ui-datepicker-div"]/table/tbody/tr[3]/td[7]')).click()
    })

    let chooseAccFramwork = BusinessFlow.execute(() => {
      Chrome.findElement(By.id('accounting-framework')).clear()
      Chrome.findElement(By.id('accounting-framework')).sendKeys('ASPE')
    })

    let goToNext = BusinessFlow.execute(() => {
      Chrome.sleep(500)
      Chrome.findElement(By.id('onboard-business-continue')).click()
      Chrome.wait(until.elementIsVisible(Chrome.findElement(By.id('onboarding-files-container'))), 5000)
      .then(() => {
        done()
      })
    })
  })

  it('Completes Step 3 (Files)', (done) => {
    let absolutePath
    let FilesFlow = Chrome.controlFlow()
    FilesFlow.async(() => {
      addFolderA
      addFolderB
      removeFolderA
    })

    let addFolderA = FilesFlow.execute(() => {
      absolutePath = path.resolve(__dirname, '../samples/folderA/')
      Chrome.findElement(By.id('files-openLocalDrive')).sendKeys(absolutePath)
    })

    let addFolderB = FilesFlow.execute(() => {
      Chrome.sleep(500)
      Chrome.findElement(By.id("files-add-another-btn")).click()
      absolutePath = path.resolve(__dirname, '../samples/folderB/')
      Chrome.findElement(By.id('files-openLocalDrive')).sendKeys(absolutePath)
    })

    let removeFolderA = FilesFlow.execute(() => {
      Chrome.sleep(500)

      let folderAElement = Chrome.findElement(By.xpath('//div[span[text()="folderA"]]'))
      folderAElement.findElement(By.className("storage-trashIcon")).click()
      Chrome.sleep(500)
      Chrome.findElement(By.xpath('//*[@id="files-deleteStorage-removeBtn"]')).click()
      Chrome.sleep(500)
      Chrome.findElement(By.id('onboard-files-continue')).click()
      Chrome.sleep(500)
      Chrome.wait(until.elementIsVisible(Chrome.findElement(By.id("onboarding-security-container"))), 5000)
        .then(() => {
          done()
        })
    })
  })

  it('Completes Step 4 (Security)', (done) => {
    let SecurityFlow = Chrome.controlFlow()

    SecurityFlow.async(() => {
      clickSkipBtn
      agreeSkipSecurity
    })

    let clickSkipBtn = SecurityFlow.execute(() => {
      Chrome.wait(until.elementIsVisible(Chrome.findElement(By.id('security-skip-btn'))), 5000)
      .then(() => {
        Chrome.findElement(By.id('security-skip-btn')).click()
        })

      Chrome.wait(until.elementIsVisible(Chrome.findElement(By.className('skip-security-text'))), 2000)
      .then(() => {
        done()
      })
    })

    let agreeSkipSecurity = SecurityFlow.execute(() => {
      Chrome.wait(until.elementIsVisible(Chrome.findElement(By.id('security-agreement-1'))), 1000)
        .then(() => {
        Chrome.sleep(500)
        Chrome.actions()
          .click(Chrome.findElement(By.id('security-agreement-1')))
          .click(Chrome.findElement(By.id('security-agreement-2')))
          .click(Chrome.findElement(By.id('security-agreement-3')))
          .click(Chrome.findElement(By.id('skip-security-acceptBtn')))
          .perform()
          .then(() => {
          Chrome.wait(until.elementIsNotVisible(Chrome.findElement(By.id('onboarding-security-container'))), 5000)
            .then(() => {
              done()
            })
        })
      })
    })
  })

  after((done) => {
    Chrome.quit().then(() => {
      done()
    })
  })
})
