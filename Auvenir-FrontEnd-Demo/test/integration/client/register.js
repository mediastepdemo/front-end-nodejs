// Setup Chai
const chai = require('chai')
const expect = chai.expect

// Setup Selenium Chrome Browser
const webdriver = require('selenium-webdriver')
const By = webdriver.By
const until = webdriver.until
var Chrome = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build()

// Import Constants
const constants = require('../constants')

describe('~ Client Registration ~', function(){

  this.timeout(40000);

  before(function(done){
    Chrome.get('https://localhost').then(function(){
      done()
    })
  })

  it('Register a new Client account', function(done) {
    Chrome.findElement(By.id('emailField')).sendKeys(constants.clientEmail)
    Chrome.findElement(By.id('emailField')).sendKeys(webdriver.Key.ENTER)
    Chrome.wait(until.elementIsVisible(Chrome.findElement(By.id('audLandEmailSent'))), 10000)
    .then(function(){
      done()
    })
  })

  it('Responds with "Awaiting Approval!"', function(done) {
    Chrome.wait(until.elementTextIs(Chrome.findElement(By.id('audLand-modal-loginHeader')),'Awaiting approval!'), 10000)
    .then(function(){
      done()
    })
  })

  after(function(done){
    Chrome.quit().then(function(){
      done()
    })
  })

})
