// Setup Chai
const chai = require('chai')
const expect = chai.expect

// Setup HTTP agent
chai.use(require('chai-http'))
var appConfig = require('../../../config')
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
var SERVER = chai.request.agent(`${appConfig.server.protocol}://${appConfig.server.domain}`)
var API = chai.request.agent(`${appConfig.server.protocol}://${appConfig.server.domain}`)

// Setup Selenium Chrome Browser
const webdriver = require('selenium-webdriver')
const By = webdriver.By
const until = webdriver.until
var Chrome = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build()
Chrome.manage().window().setSize(1400,1000)

// Import Constants
const constants = require('../constants')
var loginToken

describe('~ Activate a new Auditor\'s Account ~', function () {
  this.timeout(60000)

  before(function (done) {
    Chrome.get('https://localhost').then(function () {
      done()
    })
  })

  it('Admin send email in landing page.', function(done) {
    Chrome.findElement(By.id('emailField')).sendKeys(constants.adminEmail)
    Chrome.findElement(By.id('emailField')).sendKeys(webdriver.Key.ENTER)
    Chrome.wait(until.elementTextIs(Chrome.findElement(By.id('audLand-modal-loginHeader')), 'Check your Email!'), 7500)
    .then(function () {
      done()
    })
  })

  it('Admin gets the Login Token from /getToken', function(done){
    SERVER.get(`/getToken?email=${constants.adminEmail}`)
      .then(function (res) {
        expect(res).to.have.status(200)
        expect(res.body.token).to.be.a('string')
        loginToken = res.body.token
      })
      .then(done)
      .catch(done)
  })

  it('Admin logs in with Token', function(done) {
    Chrome.get(`https://localhost/checkToken?email=${constants.adminEmail}&token=${loginToken}`)
    Chrome.wait(until.elementIsVisible(Chrome.findElement(By.id('admin-main'))), 20000)
    .then(function () {
      done()
    })
  })

  it('Admin sets the Auditor ONBOARDING', function(done) {
    Chrome.findElement(By.className(constants.auditorEmail)).sendKeys('ONBOARDING')
    Chrome.sleep(2000)
    Chrome.wait(until.elementIsVisible(Chrome.findElement(By.id('msgContainer'))), 10000)
    .then(function () {
      done()
    })
  })

  it('Admin confirms the Auditor is ONBOARDING', function(done) {
    Chrome.findElement(By.xpath('//button[text()="Confirm"]')).click()
    Chrome.sleep(4000)
    Chrome.wait(until.elementTextContains(Chrome.findElement(
      By.xpath(`//*[@id="msgContainer"]/p/div`)), `Verified ${constants.auditorEmail} successfully.`))
    .then(function () {
      done()
    })
  })

  after(function (done) {
    Chrome.quit().then(function () {
      done()
    })
  })
})
