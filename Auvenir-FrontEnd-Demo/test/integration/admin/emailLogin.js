// Setup Chai
const chai = require('chai')
const expect = chai.expect

// Setup HTTP agent
chai.use(require('chai-http'))
var appConfig = require('../../../config')
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
var SERVER = chai.request.agent(`${appConfig.server.protocol}://${appConfig.server.domain}`)
var API = chai.request.agent(`${appConfig.server.protocol}://${appConfig.server.domain}`)

// Setup Selenium Chrome Browser
const webdriver = require('selenium-webdriver')
const By = webdriver.By
const until = webdriver.until
var Chrome = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build()
Chrome.manage().window().setSize(1400, 1000)

// Import Constants
const constants = require('../constants')
var loginToken

describe('~ Admin Email Login ~', function () {
  this.timeout(20000)

  before(function (done) {
    Chrome.get('https://localhost').then(function () {
      done()
    })
  })

  it('Admin sends email in landing page.', function(done) {
    Chrome.findElement(By.id('emailField')).sendKeys(constants.adminEmail)
    Chrome.findElement(By.id('emailField')).sendKeys(webdriver.Key.ENTER)
    Chrome.wait(until.elementIsVisible(Chrome.findElement(By.id('audLandEmailSent'))), 2000)
    .then(function () {
      done()
    })
  })

  it('Got a respond "Check your Email!"', function(done) {
    Chrome.wait(until.elementTextIs(Chrome.findElement(By.id('audLand-modal-loginHeader')),'Check your Email!'), 7500)
    .then(function () {
      done()
    })
  })

  it('Admin gets the login token from /getToken', function(done){
    SERVER.get(`/getToken?email=${constants.adminEmail}`)
      .then(function (res) {
        expect(res).to.have.status(200)
        expect(res.body.token).to.be.a('string')
        loginToken = res.body.token
      })
      .then(done)
      .catch(done)
  })

  it('Admin logs in with token', function(done) {
    Chrome.get(`https://localhost/checkToken?email=${constants.adminEmail}&token=${loginToken}`)
    Chrome.wait(until.elementIsVisible(Chrome.findElement(By.id('OnboardingPage'))), 10000)
    .then(function () {
      done()
    })
  })

  after(function (done) {
    Chrome.quit().then(function () {
      done()
    })
  })
})
