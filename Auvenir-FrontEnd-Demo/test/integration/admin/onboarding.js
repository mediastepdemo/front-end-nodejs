// Setup Chai
const chai = require('chai')
const expect = chai.expect

// Setup HTTP agent
chai.use(require('chai-http'))
var appConfig = require('../../../config')
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
var SERVER = chai.request.agent(`${appConfig.server.protocol}://${appConfig.server.domain}`)
var API = chai.request.agent(`${appConfig.server.protocol}://${appConfig.server.domain}`)

// Setup Selenium Chrome Browser
const webdriver = require('selenium-webdriver')
const By = webdriver.By
const until = webdriver.until
var Chrome = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build()
Chrome.manage().window().setSize(1400, 1000)

// Import Constants
const constants = require('../constants')
var loginToken

describe('~ Complete Admin Onboarding ~', function () {
  this.timeout(20000)

  before(function (done) {
    Chrome.get('https://localhost').then(function () {
      done()
    })
  })

  it('Login with an Admin Account', function (done) {
    Chrome.findElement(By.id('emailField')).sendKeys(constants.adminEmail)
    Chrome.findElement(By.id('emailField')).sendKeys(webdriver.Key.ENTER)
    Chrome.wait(until.elementTextIs(Chrome.findElement(By.id('audLand-modal-loginHeader')), 'Check your Email!'), 7500)
    .then(function () {
      done()
    })
  })

  it('Should get the Login Token from /getToken', function (done) {
    SERVER.get(`/getToken?email=${constants.adminEmail}`)
      .then(function (res) {
        expect(res).to.have.status(200)
        expect(res.body.token).to.be.a('string')
        loginToken = res.body.token
      })
      .then(done)
      .catch(done)
  })

  it('Login with Token', function (done) {
    Chrome.get(`https://localhost/checkToken?email=${constants.adminEmail}&token=${loginToken}`)
    Chrome.wait(until.elementIsVisible(Chrome.findElement(By.id('OnboardingPage'))), 10000)
    .then(function () {
      done()
    })
  })

  it('Step 1 (Personal)', function (done) {
    Chrome.findElement(By.id('personal-name')).clear()
    Chrome.findElement(By.id('personal-name')).sendKeys('John Smith')
    Chrome.findElement(By.id('personal-phoneNumber')).clear()
    Chrome.findElement(By.id('personal-phoneNumber')).sendKeys('5555555555')
    Chrome.findElement(By.id('agreement-personal')).click()
    Chrome.findElement(By.id('personal-coninueBtn')).click()
    Chrome.wait(until.elementIsVisible(Chrome.findElement(By.id('onboarding-security-container'))), 5000)
    .then(function () {
      done()
    })
  })

  it('Step 2a (Security)', function (done) {
    Chrome.findElement(By.xpath('//button[text()="Skip"]')).click()
    Chrome.wait(until.elementIsVisible(Chrome.findElement(By.className('skip-security-text'))), 2000)
    .then(setTimeout(function () {
      done()
    }, 1000))
  })

  it('Step 2b (Skip Security Agreement)', function (done) {
    Chrome.findElement(By.id('security-agreement-1')).click()
    Chrome.findElement(By.id('security-agreement-2')).click()
    Chrome.findElement(By.id('security-agreement-3')).click()
    Chrome.findElement(By.className('skip-security-acceptBtn')).click()
    Chrome.wait(until.elementIsNotVisible(Chrome.findElement(By.id('onboarding-security-container'))), 5000)
    .then(function () {
      done()
    })
  })

  after(function (done) {
    Chrome.quit().then(function () {
      done()
    })
  })
})
