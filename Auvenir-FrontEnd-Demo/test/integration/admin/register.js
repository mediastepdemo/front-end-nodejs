// Setup Chai
const chai = require('chai')
const expect = chai.expect

// Setup Selenium Chrome Browser
const webdriver = require('selenium-webdriver')
const By = webdriver.By
const until = webdriver.until
var Chrome = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build()
Chrome.manage().window().setSize(1400, 1000)

// Import Constants
const constants = require('../constants')

describe('~ Admin Registration ~', function () {
  this.timeout(10000)

  before(function (done) {
    Chrome.get('https://localhost').then(function () {
      done()
    })
  })

  it('Register with an Auvenir.com email', function (done) {
    Chrome.findElement(By.id('emailField')).sendKeys(constants.adminEmail)
    Chrome.findElement(By.id('emailField')).sendKeys(webdriver.Key.ENTER)
    Chrome.wait(until.elementIsVisible(Chrome.findElement(By.id('audLandEmailSent'))), 2000)
    .then(function () {
      done()
    })
  })

  it('Responds with "Check your Email!"', function (done) {
    Chrome.wait(until.elementTextIs(Chrome.findElement(By.id('audLand-modal-loginHeader')),'Check your Email!'), 7500)
    .then(function () {
      done()
    })
  })

  after(function (done) {
    Chrome.quit().then(function () {
      done()
    })
  })
})
