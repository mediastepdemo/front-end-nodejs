#!/usr/bin/env node
const path = require('path')
global.rootDirectory = path.normalize(path.join(__dirname, '..', '..'))
const appConfig = require(rootDirectory + '/config')
const testConfig = require(rootDirectory + '/test/config')
const Logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = Logger.init(__filename)
const _ = require('lodash')
const async = require('async')
const TestAPIV1 = require(rootDirectory + '/test/api/TestAPIV1')
const institutionInfo = require(rootDirectory + '/finicity/plugins/institutionInfo').finicity
const errorCodes = require(rootDirectory + '/finicity/plugins/errorCodes')

info('User Login Test')

const email = testConfig.user.email
const password = testConfig.user.password

const api = new TestAPIV1()
api.weblogin(user, (err, data) => {})
