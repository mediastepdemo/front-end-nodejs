#!/usr/bin/env node
const path = require('path')
global.rootDirectory = path.normalize(path.join(__dirname, '..', '..'))
const appConfig = require(rootDirectory + '/config')
const testConfig = require(rootDirectory + '/test/config')
const Logger = require(rootDirectory + '/plugins/logger/logger')
const {info, warn, error} = Logger.init(__filename)
const _ = require('lodash')
const async = require('async')
const TestAPIV1 = require(rootDirectory + '/test/api/TestAPIV1')
const institutionInfo = require(rootDirectory + '/finicity/plugins/institutionInfo').finicity
const errorCodes = require(rootDirectory + '/finicity/plugins/errorCodes')

info('User Login Test')

const email = testConfig.user.email
const password = testConfig.user.password

const api = new TestAPIV1()

const member = {
  firstName: 'Test Auditor',
  email: 'tom@bitblitz.biz',
  roleInFirm: 'QA',
  phone: '111-111-1111',
  hearFrom: 'N/A',
  agreements: [
    {agreementID: 'TAC-0', timeStamp: Date.now()},
    {agreementID: 'PP-0', timeStamp: Date.now()}
  ]
}

const firm = {
  name: 'Auvenir',
  website: 'www.auvenir.com',
  previousName: '',
  address: {
    fullAddress: '',
    streetAddress: '225 Richmond St W',
    suitNumber: '401',
    postalCode: 'M5V1W2',
    city: 'Toronto',
    stateProvince: 'Ontario'
  },
  memberID: '1000',
  numberOfEmployee: '11-20',
  phone: '111-111-1111',
  affiliatedFirmName: '',
  logo: '',
  logoDisplayAgreed: true
}

api.websignup(member, firm, (err, data) => {})
