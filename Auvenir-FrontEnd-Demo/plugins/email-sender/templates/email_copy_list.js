/**
 * EmailCopyList is a list of body copies that can be injected to the email htmlBody.
 * The header and footer are in the default.json
 */

var EmailContentList = {
  // active & onboarding / pending & waitlist are exactly same.
  // confirmEmail is not being used yet but have a template image on Avocode for this case. (~Lucy)
  active: {
    html: `
      <p class="lead">Welcome to Auvenir!</p><br><br>
      <p>Congratulations, you have been selected to be part of our exclusive beta.
        When you're ready to audit smarter, click the button below
        to create and/or login to your Auvenir account.
      </p><br><br>
      <center>
        <a class="loginButton" href='%url%/checkToken?token=%token%&email=%email%'>Login</a>
      </center><br><br>
      <p>We welcome your feedback, ideas and suggestions to make the audit experience better. Send us an email at
        <a style="color:#50baa8;" href="mailto:feedback@auvenir.com">feedback@auvenir.com</a>.
      </p><br><br>`
  },

  onboarding: {
    html: `
      <p class="lead">Welcome to Auvenir!</p><br><br>
      <p>Your account has been validated - when you are ready to audit smarter, click the button below to activate your account.
      </p><br><br>
      <center>
        <a class="loginButton" href='%url%/checkToken?token=%token%&email=%email%'>Get Started</a>
      </center><br><br>
      <p>We welcome your feedback, ideas and suggestions to make the audit experience better. Send us an email at
        <a style="color:#50baa8;" href="mailto:feedback@auvenir.com">feedback@auvenir.com</a>.
      </p><br><br>`
  },

  passwordReset: {
    html: `
      <p>Thank you for using our secure login system.</p><br>
      <p>You indicated that you need to reset your password.
      Click on the link below to reset your login password for your Auvenir account.</p><br><br>
      <center>
        <a class="loginButton" href='%url%/checkToken?token=%token%&email=%email%'>Reset Password</a>
      </center><br><br>
      <p>If you did not request this password reset, or you have any concerns about your Auvenir account, send us an email at
        <a style="color:#50baa8;" href="mailto:support@auvenir.com">support@auvenir.com</a>.
      </p><br><br>`
  },

  waitlist: {
    html: `
      <p>Thank you for your interest in the Auvenir Audit Smarter platform.</p><br><br>
      <p>You will receive a response shortly from our onboarding team - in the meantime, please click <a style='color:#50baa8;' href='https://update.auvenir.com'>here</a> for valuable content and updates.</p><br><br>
      <p>Meanwhile, you can follow us on:
        <a style='color:#50baa8;' href='https://twitter.com/auvenir'>Twitter</a>, <a style='color:#50baa8;' href='https://www.linkedin.com/company/auvenir'>LinkedIn</a> or <a style='color:#50baa8;' href='https://www.facebook.com/auvenir/'>Facebook</a> and
        e-mail us at <a style='color:#50baa8;' href='mailto:info@auvenir.com'>info@auvenir.com</a>.
      </p><br><br>`
  },

  pending: {
    html: `
      <p>Thank you for your interest in the Auvenir Audit Smarter platform.</p><br><br>
      <p>You will receive a response shortly from our onboarding team - in the meantime, please click <a style='color:#50baa8;' href='https://update.auvenir.com'>here</a> for valuable content and updates.</p><br><br>
      <p>Meanwhile, you can follow us on:
        <a style='color:#50baa8;' href='https://twitter.com/auvenir'>Twitter</a>, <a style='color:#50baa8;' href='https://www.linkedin.com/company/auvenir'>LinkedIn</a> or <a style='color:#50baa8;' href='https://www.facebook.com/auvenir/'>Facebook</a> and
        e-mail us at <a style='color:#50baa8;' href='mailto:info@auvenir.com'>info@auvenir.com</a>.
      </p><br><br>`
  },

  verified: {
    html: `
      <p class="lead">Welcome to Auvenir!</p><br><br>
      <p>Your account has been validated - when you are ready to audit smarter, click the button below to activate your account.
      </p><br><br>
      <center>
        <a class="loginButton" href='%url%/checkToken?token=%token%&email=%email%'>Get Started</a>
      </center><br><br>
      <p>We welcome your feedback, ideas and suggestions to make the audit experience better. Send us an email at
        <a style="color:#50baa8;" href="mailto:feedback@auvenir.com">feedback@auvenir.com</a>.
      </p><br><br>`
  },

  clientFilesReady: {
    html: `
      <p class="lead">Hi, %auditorFirstName%</p><br><br>
      <p>%userFirstName% %userLastName% has uploaded all of the files you have requested for
      %engagementName% audit. They are now ready for you to download and
      review. Click the link below to view the files before downloading.</p><br><br>
      <center>
        <a class="loginButton" href="%url%/checkToken?token=%token%&email=%email%&eid=%engagementID%">View files</a>
      </center><br><br>
      <p>We welcome your feedback, ideas and suggestions to make the audit experience better. Send us an email at
        <a style="color:#50baa8;" href="mailto:feedback@auvenir.com">feedback@auvenir.com</a>.
      </p><br><br>`
  },

  notifyClient: {
    html: `
      <p class="lead">Hi %userFirstName%</p><br><br>
      <p>Your audit opinion has been uploaded by your auditor!</p><br><br>
      <p>Login to Auvenir and go to your Files section to download.</p><br><br>
      <center>
        <a class="loginButton" href="%url%/checkToken?token=%token%&eid=%engagementID%&email=%email%">Check your engagement</a>
      </center><br><br>
      <p>We welcome your feedback, ideas and suggestions to make the audit experience better. Send us an email at
        <a style="color:#50baa8;" href="mailto:feedback@auvenir.com">feedback@auvenir.com</a>.
      </p><br><br>`
  },

  sendEngagementNotifyGeneralAuditor: {
    html: `
      <p class="lead">Hi %inviteeFirstName%</p><br><br>
      <p>You were added to %engagementName% by your team leader!</p><br><br>
      <p>Login to Auvenir and go to your Team section to review.</p><br><br>
      <center>
        <a class="loginButton" href="%url%/checkToken?token=%token%&eid=%engagementID%&email=%email%">Check your engagement</a>
      </center><br><br>
      <p>We welcome your feedback, ideas and suggestions to make the audit experience better. Send us an email at
        <a style="color:#50baa8;" href="mailto:feedback@auvenir.com">feedback@auvenir.com</a>.
      </p><br><br>`
  },

  sendEngagementNotifyLeadClient: {
    html: `
      <p class="lead">Hi %inviteeFirstName%,</p><br><br>
      <p>%inviterFirstName% has invited you to complete your engagement.
      Please click below to get started!</p><br><br>
      <center>
        <a class="loginButton" href='%url%/acceptInvite?token=%token%&eid=%engagementID%&email=%email%'>Start Your Engagement</a>
      </center><br><br>
      <p>Auvenir is on a mission to make financial audits smarter and more efficient.
      Our technology helps auditors and clients collaborate better for faster, easier engagements.</p>`
  },

  sendEngagementNotifyGeneralClient: {
    html: `
      <p class="lead">Hi %inviteeFirstName%,</p><br><br>
      <p>%inviterFirstName% has invited you to participate in the engagement.
      Please click below to get started!</p><br><br>
      <center>
        <a class="loginButton" href='%url%/acceptInvite?token=%token%&eid=%engagementID%&email=%email%'>Start Your Engagement</a>
      </center><br><br>
      <p>Auvenir is on a mission to make financial audits smarter and more efficient.
      Our technology helps auditors and clients collaborate better for faster, easier engagements.</p><br><br>
      <p>Here are some of the benefits.</p><br><br>
      <p>We welcome your feedback, ideas and suggestions to make the audit experience better. Send us an email at
        <a style="color:#50baa8;" href="mailto:feedback@auvenir.com">feedback@auvenir.com</a>.</p>`
  },

  sendEngagementInviteGeneralAuditor: {
    html: `
      <p class="lead">Hello %inviteeFirstName%,</p><br><br>
      <p>%inviterFirstName% has invited you to join the %engagementName%, click the button below to start.</p><br><br>
      <center>
        <a class="loginButton" href='%url%/acceptInvite?token=%token%&eid=%engagementID%&email=%email%'>Get Started</a>
      </center><br><br>
      <p>We welcome your feedback, ideas and suggestions to make the audit experience better. Send us an email at
        <a style="color:#50baa8;" href="mailto:feedback@auvenir.com">feedback@auvenir.com</a>.</p>`
  },

  sendEngagementInviteLeadClient: {
    html: `
      <p class="lead">Hi %inviteeFirstName%,</p><br><br>
      <p>%inviterFirstName% has invited you to complete your engagement.
      Please click below to get started!</p><br><br>
      <center>
        <a class="loginButton" href='%url%/acceptInvite?token=%token%&eid=%engagementID%&email=%email%'>Start Your Engagement</a>
      </center><br><br>
      <p>Auvenir is on a mission to make financial audits smarter and more efficient.
      Our technology helps auditors and clients collaborate better for faster, easier engagements.</p><br><br>
      <p>Here are some of the benefits.</p><br>
      <p>- Highly secure, cloud based platform to upload your documents</p>
      <p>- Customized, detailed notifications and task management system keeps everyone on schedule and on budget</p>
      <p>- Bank and accounting system integrations</p><br><br>
      <p>We welcome your feedback, ideas and suggestions to make the audit experience better. Send us an email at
        <a style="color:#50baa8;" href="mailto:feedback@auvenir.com">feedback@auvenir.com</a>.</p>`
  },

  sendEngagementInviteGeneralClient: {
    html: `
      <p class="lead">Hi %inviteeFirstName%,</p><br><br>
      <p>%inviterFirstName% has invited you to join Auvenir to participate in the engagement.
      Please click below to get started!</p><br><br>
      <center>
        <a class="loginButton" href='%url%/acceptInvite?token=%token%&eid=%engagementID%&email=%email%'>Start Your Engagement</a>
      </center><br><br>
      <p>Auvenir is on a mission to make financial audits smarter and more efficient.
      Our technology helps auditors and clients collaborate better for faster, easier engagements.</p><br><br>
      <p>Here are some of the benefits.</p><br><br>
      <p>We welcome your feedback, ideas and suggestions to make the audit experience better. Send us an email at
        <a style="color:#50baa8;" href="mailto:feedback@auvenir.com">feedback@auvenir.com</a>.</p>`
  },

  invitedToEngagement: {
    html: `
      <p class="lead">Hi %nameInvited%,</p><br><br>
      <p>%nameInvitedEngagement% from %nameOfFirmBusiness% has been invited to %engagementName%.</p><br><br>
      <p>Please click <a href='%url%/contacts'>here</a> to see your contacts.</p><br><br>
      <p>Click <a href='%url%/email-settings'>here</a> to change your email settings.</p><br><br>
      <p>If you need other assistance, contact us at <a style="color:#50baa8;" href="mailto:support@auvenir.com">support@auvenir.com</a>.</p><br><br>
      `
  },

  joinedYourEngagement: {
    html: `
      <p class="lead">Hi %nameJoined%,</p><br><br>
      <p>%nameJoinedEngagement% from %nameOfFirmBusiness% has joined %engagementName%.</p><br><br>
      <p>Please click <a href='%url%/contacts'>here</a> to see your contacts.</p><br><br>
      <p>Click <a href='%url%/email-settings'>here</a> to change your email settings.</p><br><br>
      <p>If you need other assistance, contact us at <a style="color:#50baa8;" href="mailto:support@auvenir.com">support@auvenir.com</a>.</p><br><br>
      `
  },

  commentedYourEngagement: {
    html: `
      <p class="lead">Hi %nameCommented%,</p><br><br>
      <p>%namePersonComment% from %nameOfFirmBusiness% has made a comment in %engagementName%.</p><br><br>
      <center>
        <a class="loginButton" href="%url%/checkToken?token=%token%&eid=%engagementID%&email=%email%">Check your engagement</a>
      </center><br><br>
      <!--<p>Please click <a href='%url%/comment'>here</a> to see the comment.</p><br><br>
      <p>Click <a href='%url%/email-settings'>here</a> to change your email settings.</p><br><br>-->
      <p>If you need other assistance, contact us at <a style="color:#50baa8;" href="mailto:support@auvenir.com">support@auvenir.com</a>.</p><br><br>
      `
  },

  createdTodoYourEngagement: {
    html: `
      <p class="lead">Hi %nameCreatedTodo%,</p><br><br>
      <p>%namePersonCreatedTodo% from %nameOfFirmBusiness% has created a To-Do in %engagementName%.</p><br><br>
      <!--<p>Please click <a href='%url%/To-Do'>here</a> to see the To-Do.</p><br><br>
      <p>Click <a href='%url%/email-settings'>here</a> to change your email settings.</p><br><br>-->
      <p>If you need other assistance, contact us at <a style="color:#50baa8;" href="mailto:support@auvenir.com">support@auvenir.com</a>.</p><br><br>
      `
  },

  createdNewRequestTodoYourEngagement: {
    html: `
      <p class="lead">Hi %nameCreatedRequestTodo%,</p><br><br>
      <p>%namePersonCreatedRequest% from %nameOfFirmBusiness% has created a new request in %engagementName%.</p><br><br>
      <<p>Please click <a href='%url%/request'>here</a> to see the request.</p><br><br>
      <p>Click <a href='%url%/email-settings'>here</a> to change your email settings.</p><br><br>
      <p>If you need other assistance, contact us at <a style="color:#50baa8;" href="mailto:support@auvenir.com">support@auvenir.com</a>.</p><br><br>
      `
  },

  uploadDocumentYourEngagement: {
    html: `
      <p class="lead">Hi %nameUploadDocument%,</p><br><br>
      <p>%namePersonCreatedRequest% from %nameOfFirmBusiness% has uploaded a document in the engagement %engagementName% for the todo %nameOfTodo% for %nameOfRequest%.</p><br><br>
      <p>Please click <a href='%url%/document'>here</a> to see your document.</p><br><br>
      <p>Click <a href='%url%/email-settings'>here</a> to change your email settings.</p><br><br>
      <p>If you need other assistance, contact us at <a style="color:#50baa8;" href="mailto:support@auvenir.com">support@auvenir.com</a>.</p><br><br>
      `
  },

  markedTodoCompleteYourEngagement: {
    html: `
      <p class="lead">Hi %nameMarkedTodo%,</p><br><br>
      <p>%namePersonCreatedRequest% from %nameOfFirmBusiness% has marked %nameOfTodo% as complete in %engagementName%.</p><br><br>
      <p>Please click <a href='%url%/To-Do'>here</a> to see the To-Do.</p><br><br>
      <p>Click <a href='%url%/email-settings'>here</a> to change your email settings.</p><br><br>
      <p>If you need other assistance, contact us at <a style="color:#50baa8;" href="mailto:support@auvenir.com">support@auvenir.com</a>.</p><br><br>`
  },

  sendSuggestion: {
    html: `
      <p class="lead">Hi, Auvenir</p><br><br>
      <p>%userFirstName% %userLastName% uses an %feedbackSource% not on the list:</p><br>
      <p>%userText%</p><br><br>
      <p>This was an automated message.</p>`
  },

  sendErrorMsg: {
    html: `
      <p>There was error in registering user's device. \nUser AuthID: %authID%\nError Message: %error%</p>`
  },

  contactPage: {
    html: `
      <p>The following submission was received from the Auvenir contact page.</p><br><br>
      <p>From: %name%</p>
      <p>Email: <a href="mailto:%email%">%email%</a></p>
      <p>Type: %type%</p>
      <p>Other Type: %otherType%</p><br><br>
      <p>%message%</p>
      <br><br>`
  }
}

var generateHtml = function (emailType) {
  var sectionInsert = EmailContentList[emailType].html
  var emailContent = `
  <head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Auvenir</title>
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet" type="text/css">

    <style type="text/css">
      *{
        margin:0;
        padding:0;
      }
      *{
        font-family:"Lato","Helvetica Neue","Helvetica",Helvetica,Arial,sans-serif;
        color:#363a3c;
      }
      img{
        max-width:100%;
      }
      .collapse{
        margin:0;
        padding:0;
      }
      body{
        -webkit-font-smoothing:antialiased;
        -webkit-text-size-adjust:none;
        width:100% !important;
        height:100%;
      }
      .loginButton{
        color:#fff;
        background-color:#599ba1;
        text-align:center;
        border:none;
        border-radius:4px;
        text-decoration:none;
        font-size:16px;
        font-weight:400;
        padding:10px 54px;
      }
      table.body-wrap{
        width:100%;
        position:relative;
        top:0;
      }
      buttonTD{
        text-align:center;
      }
      table.wrapper{
        border:none;
        padding:0;
        width:100%;
        position:relative;
        border-collapse:collapse;
      }
      table.footer-wrap{
        width:100%;
        clear:both !important;
        position:relative;
      }
      .footerElement{
        display:block;
        color:#707070;
      }
      .imageHolder{
        margin-bottom:10px;
      }
      .punchLine{
        margin-bottom:20px;
      }
      .AImage{
        width:60px;
        height:60px;
        margin-top:20px;
      }
      .collapse{
        margin:0 !important;
      }
      p,ul{
        font-weight:normal;
        font-size:14px;
        line-height:1.6;
        color:#707070;
      }
      p.lead{
        font-size:18px;
        font-weight:700;
        letter-spacing:.75px;
        line-height:0;
        margin-top:15px;
        color:#363a3c;
      }
      .container{
        display:block !important;
        max-width:600px !important;
        margin:0 auto !important;
        clear:both !important;
        border-radius:4px;
      }
      .content{
        padding:15px;
        max-width:600px;
        margin:0 auto;
        display:block;
      }
      .content table{
        width:100%;
      }
      .clear{
        display:block;
        clear:both;
      }
      .bg,.bg td{
        display:block !important;
        position:absolute !important;
        width:100% !important;
        height:200px !important;
      }
      .bg img{
        position:relative !important;
        display:block !important;
        min-height:100% !important;
        min-width:100% !important;
      }
      .mainTable{
        width:586px;
        background:#fff;
        padding:30px;
      }
      @media only screen and (max-width: 600px){
        a[class=btn]{
          display:block !important;
          margin-bottom:10px !important;
          background-image:none !important;
          margin-right:0 !important;
        }
      }
      @media only screen and (max-width: 600px){
        div[class=column]{
          width:auto !important;
          float:none !important;
        }
        table.social div[class=column]{
          width:auto !important;
        }
        .mainTable {
          padding:5px;
          width:350px;
        }
      }
      @media only screen and (max-width: 350px){
        .mainTable{
          padding:5px;
          width:300px;
        }
      }
    </style>
  </head>
  <body bgcolor="#F1F1F1">
    <table style="background:#142166;border-collapse:collapse;">
      <tr>
        <td valign="bottom" style="width:50%;">
          <div style="height:300px;background-color:#F1F1F1;">
          </div>
        </td>
        <td>
          <center>
            <img src="%asset_url%/images/logo.png" alt="logo.png" style="padding: 32px 0px">
            <table class="mainTable">
              <tr>
                <td>
                  ${sectionInsert}
                  <p>
                    Best Regards,<br>
                    <br>
                    -Andi,<br>
                    Auvenir Customer Success Team
                  </p>
                </td>
              </tr>
            </table>
          </center>
        </td>
        <td valign="bottom" style="width:50%;">
          <div style="height:300px;background-color:#F1F1F1;"></div>
        </td>
      </tr>
    </table>
    <div style="width:100%;">
      <center>
        <table bgcolor="#F1F1F1" style="width:100%;">
          <tr>
            <td align="center">
              <p style="margin-top:0px;">
                <span class="footerElement imageHolder">
                  <img class="AImage" src="%asset_url%/images/icon.png" alt="icon.png">
                </span>
                <span class="footerElement punchLine">Audit, Smarter.</span>
                <span class="footerElement">225 Richmond Street West, Suite 402, Toronto, ON M5V1W2</span>
                <span class="footerElement">This email is subject to Auvenir’s standard <a style="color:#50baa8;" href="%url%/terms">Terms of Service</a> and <a style="color:#50baa8;" href="%url%/privacy">Privacy Policy</a>.</span>
                <span class="footerElement">To unsubscribe, please
                <a style="color:#50baa8;" href="mailto:unsubscribe@auvenir.com?Subject=Unsubscribe">click here</a>.</span>
              </p>
            </td>
          </tr>
        </table>
      </center>
    </div>
  </body>`
  return emailContent
}

/**
 * EmailCopyList is a list of body copies that can be injected to the email htmlBody.
 * The header and footer are in the default.json
 */

var EmailCopyList = {
  // active & onboarding / pending & waitlist are exactly same.
  // confirmEmail is not being used yet but have a template image on Avocode for this case. (~Lucy)
  active: {
    subject: 'Sign in to Auvenir!',
    content: {
      html: generateHtml('active'),
      plain: `
        Welcome to Auvenir!\r\n\r\n
        Congratulations, you have been selected to be part of our exclusive beta.\r\n\r\n
        When you're ready to audit smarter, click the button below
        to create and/or login to your Auvenir account.\r\n\r\n
        %url%/checkToken?token=%token%&email=%email%\r\n\r\n
        We welcome your feedback, ideas and suggestions to make the audit experience better. Send us an email at
        feedback@auvenir.com.\r\n\r\n
        Best Regards,\r\n\r\n
        -Andi,\r\n
        Auvenir Customer Success Team`
    }
  },
  onboarding: {
    subject: 'Sign in to Auvenir!',
    content: {
      html: generateHtml('onboarding'),
      plain: `
        Welcome to Auvenir!\r\n\r\n
        Your account has been validated - when you are ready to audit smarter, click the button below to activate your account.\r\n\r\n
        %url%/checkToken?token=%token%&email=%email%\r\n\r\n
        We welcome your feedback, ideas and suggestions to make the audit experience better. Send us an email at
        feedback@auvenir.com.\r\n\r\n
        -Andi,\r\n
        Auvenir Customer Success Team`
    }
  },
  passwordReset: {
    subject: 'Reset your Auvenir password',
    content: {
      html: generateHtml('passwordReset'),
      plain: `
        Thank you for using our secure login system.\r\n\r\n
        You indicated that you need to reset your password.
        Click on the link below to reset your login password for your Auvenir account.\r\n\r\n
        %url%/checkToken?token=%token%&email=%email%\r\n\r\n
        If you did not request this password reset, or you have any concerns about your Auvenir account, send us an email at
        support@auvenir.com\r\n\r\n
        Best Regards,\r\n\r\n
        -Andi,\r\n
        Auvenir Customer Success Team`
    }
  },
  waitlist: {
    subject: 'Thank you for your interest in the Auvenir Audit Smarter Platform',
    content: {
      html: generateHtml('waitlist'),
      plain: `
        Thank you for your interest in the Auvenir Audit Smarter platform.\r\n\r\n
        You will receive a response shortly from our onboarding team - in the meantime, please click here(https://update.auvenir.com) for valuable content and updates.\r\n\r\n
        Meanwhile, you can follow us on: Twitter(https://twitter.com/auvenir), LinkedIn(https://www.linkedin.com/company/10419712) or Facebook(https://www.facebook.com/auvenir) and \r\n
        e-mail us at info@auvenir.com.\r\n\r\n
        -Andi,\r\n
        Auvenir Customer Success Team`
    }
  },
  pending: {
    subject: 'Thank you for your interest in the Auvenir Audit Smarter Platform',
    content: {
      html: generateHtml('pending'),
      plain: `
        Thank you for your interest in the Auvenir Audit Smarter platform.\r\n\r\n
        You will receive a response shortly from our onboarding team - in the meantime, please click here(https://update.auvenir.com) for valuable content and updates.\r\n\r\n
        Meanwhile, you can follow us on: Twitter(https://twitter.com/auvenir), LinkedIn(https://www.linkedin.com/company/10419712) or Facebook(https://www.facebook.com/auvenir) and \r\n
        e-mail us at info@auvenir.com.\r\n\r\n
        -Andi,\r\n
        Auvenir Customer Success Team`
    }
  },
  verified: {
    subject: 'Welcome to the Auvenir Audit Smarter Platform',
    content: {
      html: generateHtml('verified'),
      plain: `
        Welcome to Auvenir!\r\n\r\n
        Your account has been validated - when you are ready to audit smarter, click the button below to activate your account.\r\n\r\n
        %url%/checkToken?token=%token%&email=%email%\r\n\r\n
        We welcome your feedback, ideas and suggestions to make the audit experience better. Send us an email at
        feedback@auvenir.com.\r\n\r\n
        -Andi,\r\n
        Auvenir Customer Success Team`
    }
  },
  clientFilesReady: {
    subject: 'Client files for %engagementName% are ready!',
    content: {
      html: generateHtml('clientFilesReady'),
      plain: `
        Hi, %auditorFirstName%\r\n\r\n
        %userFirstName% %userLastName% has uploaded all of the files you have requested for\r\n
        %engagementName% audit. They are now ready for you to download and\r\n
        review. Click the link below to view the files before downloading.\r\n
        %url%/checkToken?token=%token%&email=%email%&eid=%engagementID%\r\n\r\n
        We welcome your feedback, ideas and suggestions to make the audit experience better. Send us an email at
        feedback@auvenir.com.\r\n\r\n
        Best Regards,\r\n\r\n
        -Andi,\r\n
        Auvenir Customer Success Team`
    }
  },
  notifyClient: {
    subject: 'Audit Opinion Ready for %engagementName%',
    content: {
      html: generateHtml('notifyClient'),
      plain: `
        Hi %userFirstName%\r\n\r\n
        Your audit opinion has been uploaded by your auditor!\r\n
        Login to auvenir and go to your Files section to download.\r\n\r\n
        %url%/checkToken?token=%token%&eid=%engagementID%&email=%email% \r\n
        We welcome your feedback, ideas and suggestions to make the audit experience better. Send us an email at
        feedback@auvenir.com.\r\n\r\n
        Best Regards,\r\n\r\n
        -Andi,\r\n
        Auvenir Customer Success Team`
    }
  },
  sendEngagementNotifyGeneralAuditor: {
    subject: 'You were added to %engagementName%',
    content: {
      html: generateHtml('sendEngagementNotifyGeneralAuditor'),
      plain: `
        Hi %inviteeFirstName%\r\n\r\n
        You were added to %engagementName% by your team leader!\r\n
        Login to auvenir and go to your Team section to review.\r\n\r\n
        %url%/checkToken?token=%token%&eid=%engagementID%&email=%email% \r\n
        We welcome your feedback, ideas and suggestions to make the audit experience better. Send us an email at
        feedback@auvenir.com.\r\n\r\n
        Best Regards,\r\n\r\n
        -Andi,\r\n
        Auvenir Customer Success Team`
    }
  },
  sendEngagementNotifyLeadClient: {
    subject: 'Invitation from %inviterFirstName% %inviterLastName% to complete your engagement on the Auvenir Platform',
    content: {
      html: generateHtml('sendEngagementNotifyLeadClient'),
      plain: `
        Hi, %inviteeFirstName%!\r\n\r\n
        %inviterFirstName% has invited you complete your engagement.
        Please click below to get started!\r\n\r\n
        %url%/acceptInvite?token=%token%&eid=%engagementID%&email=%email%\r\n\r\n
        Auvenir is on a mission to make financial audits smarter and more efficient. Our technology helps auditors and clients collaborate better for faster, easier engagements.\r\n\r\n
        Best Regards,\r\n\r\n
        -Andi,\r\n
        Auvenir Customer Success Team`
    }
  },
  sendEngagementNotifyGeneralClient: {
    subject: 'You were added to %engagementName%',
    content: {
      html: generateHtml('sendEngagementNotifyGeneralClient'),
      plain: `
        Hi, %inviteeFirstName%!\r\n\r\n
        %inviterFirstName% has invited you to participate in the engagement.
        Please click below to get started!\r\n\r\n
        %url%/acceptInvite?token=%token%&eid=%engagementID%&email=%email%\r\n\r\n
        Auvenir is on a mission to make financial audits smarter and more efficient. Our technology helps auditors and clients collaborate better for faster, easier engagements.\r\n\r\n
        We welcome your feedback, ideas and suggestions to make the audit experience better. Send us an email at
        feedback@auvenir.com.\r\n\r\n
        Best Regards,\r\n\r\n
        -Andi,\r\n
        Auvenir Customer Success Team`
    }
  },
  sendEngagementInviteGeneralAuditor: {
    subject: 'Invitation from %inviterFirstName% %inviterLastName% to join an engagement on the Auvenir Platform',
    content: {
      html: generateHtml('sendEngagementInviteGeneralAuditor'),
      plain: `
        Hello, %inviteeFirstName%!\r\n\r\n
        %inviterFirstName% has invited you to join the %engagementName%, click the button below to start.\r\n\r\n
        %url%/acceptInvite?token=%token%&eid=%engagementID%&email=%email%\r\n\r\n
        We welcome your feedback, ideas and suggestions to make the audit experience better. Send us an email at
        feedback@auvenir.com.\r\n\r\n
        Best Regards,\r\n\r\n
        -Andi,\r\n
        Auvenir Customer Success Team`
    }
  },
  sendEngagementInviteLeadClient: {
    subject: 'Invitation from %inviterFirstName% %inviterLastName% to complete your engagement on the Auvenir Platform',
    content: {
      html: generateHtml('sendEngagementInviteLeadClient'),
      plain: `
        Hi, %inviteeFirstName%!\r\n\r\n
        %inviterFirstName% has invited you complete your engagement.
        Please click below to get started!\r\n\r\n
        %url%/acceptInvite?token=%token%&eid=%engagementID%&email=%email%\r\n\r\n
        Auvenir is on a mission to make financial audits smarter and more efficient. Our technology helps auditors and clients collaborate better for faster, easier engagements.\r\n
        Here are some of the benefits.\r\n\r\n
        - Highly secure, cloud based platform to upload your documents\r\n
        - Customized, detailed notifications and task management system keeps everyone on schedule and on budget\r\n
        - Bank and accounting system integrations\r\n\r\n
        We welcome your feedback, ideas and suggestions to make the audit experience better. Send us an email at
        feedback@auvenir.com.\r\n\r\n
        Best Regards,\r\n\r\n
        -Andi,\r\n
        Auvenir Customer Success Team`
    }
  },
  sendEngagementInviteGeneralClient: {
    subject: 'Invitation from %inviterFirstName% %inviterLastName% to participate in an engagement on the Auvenir Platform',
    content: {
      html: generateHtml('sendEngagementInviteGeneralClient'),
      plain: `
        Hi, %inviteeFirstName%!\r\n\r\n
        %inviterFirstName% has invited you to join Auvenir to participate in the engagement.
        Please click below to get started!\r\n\r\n
        %url%/acceptInvite?token=%token%&eid=%engagementID%&email=%email%\r\n\r\n
        Auvenir is on a mission to make financial audits smarter and more efficient. Our technology helps auditors and clients collaborate better for faster, easier engagements.\r\n\r\n
        We welcome your feedback, ideas and suggestions to make the audit experience better. Send us an email at
        feedback@auvenir.com.\r\n\r\n
        Best Regards,\r\n\r\n
        -Andi,\r\n
        Auvenir Customer Success Team`
    }
  },
  invitedToEngagement: {
    subject: '%nameInvitedEngagement% has been invited to %engagementName% on Auvenir',
    content: {
      html: generateHtml('invitedToEngagement'),
      plain: `
        Hi %nameInvited%,\r\n\r\n
        %nameInvitedEngagement% from %nameOfFirmBusiness% has been invited to %engagementName%.\r\n\r\n
        Please click below to see your contacts.\r\n\r\n
        %url%/contacts\r\n\r\n
        Click below to change your email settings.\r\n\r\n
        %url%/email-settings\r\n\r\n
        If you need other assistance, contact us at support@auvenir.com.\r\n\r\n
        Best Regards,\r\n\r\n
        -Andi,\r\n
        Auvenir Customer Success Team`
    }
  },
  joinedYourEngagement: {
    subject: '%nameJoinedEngagement% has joined %engagementName% on Auvenir',
    content: {
      html: generateHtml('joinedYourEngagement'),
      plain: `
        Hi %nameJoined%,\r\n\r\n
        %nameJoinedEngagement% from %nameOfFirmBusiness% has joined %engagementName%.\r\n\r\n
        Please click below to see your contacts.\r\n\r\n
        %url%/contacts\r\n\r\n
        Click below to change your email settings.\r\n\r\n
        %url%/email-settings\r\n\r\n
        If you need other assistance, contact us at support@auvenir.com.\r\n\r\n
        Best Regards,\r\n\r\n
        -Andi,\r\n
        Auvenir Customer Success Team`
    }
  },
  commentedYourEngagement: {
    subject: '%namePersonComment% has commented in %engagementName% on Auvenir',
    content: {
      html: generateHtml('commentedYourEngagement'),
      plain: `
        Hi %nameCommented%,\r\n\r\n
        %namePersonComment% from %nameOfFirmBusiness% has made a comment in %engagementName%.\r\n\r\n
        Please click below to see the comment.\r\n\r\n
        %url%/comment\r\n\r\n
        Click below to change your email settings.\r\n\r\n
        %url%/email-settings\r\n\r\n
        If you need other assistance, contact us at support@auvenir.com.\r\n\r\n
        Best Regards,\r\n\r\n
        -Andi,\r\n
        Auvenir Customer Success Team`
    }
  },
  createdTodoYourEngagement: {
    subject: '%namePersonCreatedTodo% has created a To-Do in %engagementName% on Auvenir',
    content: {
      html: generateHtml('createdTodoYourEngagement'),
      plain: `
        Hi %nameCreatedTodo%,\r\n\r\n
        %namePersonCreatedTodo% from %nameOfFirmBusiness% has created a To-Do in %engagementName%.\r\n\r\n
        Please click below to see the To-Do.\r\n\r\n
        %url%/To-Do\r\n\r\n
        Click below to change your email settings.\r\n\r\n
        %url%/email-settings\r\n\r\n
        If you need other assistance, contact us at support@auvenir.com.\r\n\r\n
        Best Regards,\r\n\r\n
        -Andi,\r\n
        Auvenir Customer Success Team`
    }
  },
  createdNewRequestTodoYourEngagement: {
    subject: '%namePersonCreatedRequest% has created a new request in %engagementName% on Auvenir',
    content: {
      html: generateHtml('createdNewRequestTodoYourEngagement'),
      plain: `
        Hi %nameCreatedRequestTodo%,\r\n\r\n
        %namePersonCreatedRequest% from %nameOfFirmBusiness% has created a new request in %engagementName%.\r\n\r\n
        Please click below to see the request.\r\n\r\n
        %url%/request\r\n\r\n
        Click below to change your email settings.\r\n\r\n
        %url%/email-settings\r\n\r\n
        If you need other assistance, contact us at support@auvenir.com.\r\n\r\n
        Best Regards,\r\n\r\n
        -Andi,\r\n
        Auvenir Customer Success Team`
    }
  },
  uploadDocumentYourEngagement: {
    subject: '%namePersonCreatedRequest% has uploaded a document in %engagementName% on Auvenir',
    content: {
      html: generateHtml('uploadDocumentYourEngagement'),
      plain: `
        Hi %nameUploadDocument%,\r\n\r\n
        %namePersonCreatedRequest% from %nameOfFirmBusiness% has uploaded a document in the engagement %engagementName% for the todo %nameOfTodo% for %nameOfRequest%.\r\n\r\n
        Please click below to see your document.\r\n\r\n
        %url%/document\r\n\r\n
        Click below to change your email settings.\r\n\r\n
        %url%/email-settings\r\n\r\n
        If you need other assistance, contact us at support@auvenir.com.\r\n\r\n
        Best Regards,\r\n\r\n
        -Andi,\r\n
        Auvenir Customer Success Team`
    }
  },
  markedTodoCompleteYourEngagement: {
    subject: '%namePersonCreatedRequest% has marked a To-Do as complete in %engagementName% on Auvenir',
    content: {
      html: generateHtml('markedTodoCompleteYourEngagement'),
      plain: `
        Hi %nameMarkedTodo%,\r\n\r\n
        %namePersonCreatedRequest% from %nameOfFirmBusiness% has marked %nameOfTodo% as complete in %engagementName%.\r\n\r\n
        Please click below to see the To-Do.\r\n\r\n
        %url%/To-Do\r\n\r\n
        Click below to change your email settings.\r\n\r\n
        %url%/email-settings\r\n\r\n
        If you need other assistance, contact us at support@auvenir.com.\r\n\r\n
        Best Regards,\r\n\r\n
        -Andi,\r\n
        Auvenir Customer Success Team`
    }
  },
  sendSuggestion: {
    subject: 'Suggestion for %feedbackSource%',
    content: {
      html: generateHtml('sendSuggestion'),
      plain: `
        Hi, Auvenir\r\n\r\n
        %userFirstName% %userLastName% uses an %feedbackSource% not on the list: \r\n\r\n
        %userText%\r\n\r\n
        This was an automated message.`
    }
  },
  sendErrorMsg: {
    subject: 'ERROR registering user device.',
    content: {
      html: generateHtml('sendErrorMsg')
    }
  },
  contactPage: {
    subject: 'Submission from the Auvenir Contact Page',
    content: {
      html: generateHtml('contactPage'),
      plain:
        `The following submission was received from the Auvenir contact page\r\n\r\n
        From: %name%\r\n
        Email: %email%\r\n
        Type: %type%\r\n
        Other Type: %otherType%\r\n\r\n
        %message%`
    }
  }
}

module.exports = EmailCopyList
