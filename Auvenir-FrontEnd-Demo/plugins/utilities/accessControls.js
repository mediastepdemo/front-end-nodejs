/**
 * This modules is responsible for providing the appropriate access controls checks in order
 * to guarantee a that the appropriate users are allowed to access specific data model objects
 * at the time of the given operation.
 * @author Matt Felske
 */

var initLogger = require('../logger/logger').init
const { info, warn, error } = initLogger(__filename)

const _ = require('lodash')
const debug = require('debug')('auvenir:routeHelper')
const Models = require('../../models')
const Utility = require('./utility')
const async = require('async')
const ObjectId = require('mongodb').ObjectID

/**
 * User AccessCheck for file upload
 * @param {String}   userID       - The userID to lookup
 * @param {Function} callback
 */
const userCheck = (userID, callback) => {
  debug('Running User access check ...')
  if (_.isNull(userID) || _.isUndefined(userID)) {
    return callback('Access Control user ID is missing.')
  }
  if (_.isNull(callback) || _.isUndefined(callback)) {
    return callback('Access Control callback function is missing.')
  }

  const USER_INFO = {
    type: 1,
    email: 1,
    firstName: 1,
    lastName: 1,
    phone: 1,
    jobTitle: 1,
    profilePicture: 1,
    lastLogin: 1
  }
  var objID = (typeof userID === 'string') ? ObjectId(userID) : userID// Utility.castToObjectId(userID)

  Models.Auvenir.User.findOne({ _id: objID }, USER_INFO, (err, oneUser) => {
    if (err) {
      error({ err })
      return callback('Error occured while querying a user object')
    }
    if (!oneUser) {
      return callback('Unable to find the specific user.')
    } else {
      switch (oneUser.status) {
        case 'PENDING':
        case 'WAIT-LIST':
          return callback('User is not currently able to use the platform.')
        case 'LOCKED':
          return callback('User is locked out of the platform.')
        case 'ACTIVE':
        case 'ONBOARDING':
          return callback(null, oneUser)
        default:
          return callback('Unable to validate the user.')
      }
    }
  })
}

/**
 * Runs an access control check against a business object and returns the result.
 * @param {ObjectId} id       - The model ObjectId
 * @param {ObjectId} userID   - The userID to check against the object's acl.
 * @param {Function} callback -
 */
const businessCheck = (id, userID, callback) => {
  debug('Running Business access check ...')
  if (_.isNull(id) || _.isUndefined(id)) {
    return callback('Access Control model ID is missing.')
  }
  if (_.isNull(userID) || _.isUndefined(userID)) {
    return callback('Access Control user ID is missing.')
  }
  if (_.isNull(callback) || _.isUndefined(callback)) {
    return callback('Access Control callback function is missing.')
  }

  var objID = ObjectId(id)
  Models.Auvenir.Business.findOne({ _id: objID }, (err, oneBusiness) => {
    if (err) {
      error({ err })
      return callback('Error occured while attempting to query a business ID (' + objID + ')')
    }

    if (!oneBusiness) {
      return callback('Unable to find business object for given business ID (' + objID + ')')
    } else {
      var { acl } = oneBusiness
      for (var i = 0; i < acl.length; i++) {
        var entry = acl[i]
        if (entry.id.toString() === userID.toString()) {
          return callback(null, entry)
        }
      }
      return callback('Unable to find userID match in business object ACL.')
    }
  })
}

/**
 * Runs an access control check against a firm object and returns the result.
 * @param {ObjectId} id       - The model ObjectId
 * @param {ObjectId} userID   - The userID to check against the object's acl.
 * @param {Function} callback -
 */
const firmCheck = (id, userID, callback) => {
  debug('Running Firm access check ...')
  if (_.isNull(id) || _.isUndefined(id)) {
    return callback('Access Control model ID is missing.')
  }
  if (_.isNull(userID) || _.isUndefined(userID)) {
    return callback('Access Control user ID is missing.')
  }
  if (_.isNull(callback) || _.isUndefined(callback)) {
    return callback('Access Control callback function is missing.')
  }

  var objID = Utility.castToObjectId(id)
  Models.Auvenir.Firm.findOne({ _id: objID }, (err, oneFirm) => {
    if (err) {
      error({ err })
      return callback('Error occured while attempting to query a Firm ID (' + objID + ')')
    }

    if (!oneFirm) {
      return callback('Unable to find firm object for given Firm ID (' + objID + ')')
    } else {
      var { acl } = oneFirm
      for (var i = 0; i < acl.length; i++) {
        var entry = acl[i]
        if (entry.id.toString() === userID.toString()) {
          return callback(null, entry)
        }
      }
      return callback('Unable to find userID match in firm object ACL.')
    }
  })
}

/**
 * Runs an access control check against an engagement object and returns the result.
 * @param {ObjectId} id       - The model ObjectId
 * @param {ObjectId} userID   - The userID to check against the object's acl.
 * @param {Function} callback -
 */
const engagementCheck = (id, userID, callback) => {
  debug('Running Engagement access check ...')
  if (_.isNull(id) || _.isUndefined(id)) {
    return callback('Access Control model ID is missing.')
  }
  if (_.isNull(userID) || _.isUndefined(userID)) {
    return callback('Access Control user ID is missing.')
  }
  if (_.isNull(callback) || _.isUndefined(callback)) {
    return callback('Access Control callback function is missing.')
  }

  var objID = _.isString(id) ? ObjectId(id) : id
  Models.Auvenir.Engagement.findOne({ _id: objID }, (err, oneEngagement) => {
    if (err) {
      error({ err })
      return callback('Error occured while attempting to query an engagement ID (' + objID + ')')
    }

    if (!oneEngagement) {
      return callback('Unable to find engagement object for given engagement ID (' + objID + ')')
    } else {
      var { acl } = oneEngagement
      for (var i = 0; i < acl.length; i++) {
        var entry = acl[i]
        if (entry.id.toString() === userID.toString() && entry.status !== 'INACTIVE') {
          return callback(null, oneEngagement)
        }
      }
      return callback('User does not have an access to this engagement.')
    }
  })
}

const todoCheck = (id, engagementID, userID, callback) => {
  debug('Running Todo access check ...')
  if (_.isNull(id) || _.isUndefined(id)) {
    return callback('Access Control file ID is missing.')
  }
  if (_.isNull(engagementID) || _.isUndefined(engagementID)) {
    return callback('Access Control engagementID is missing.')
  }
  if (_.isNull(userID) || _.isUndefined(userID)) {
    return callback('Access Control user ID is missing.')
  }
  if (_.isNull(callback) || _.isUndefined(callback)) {
    return callback('Access Control callback function is missing.')
  }

  var objID = (_.isString(id)) ? ObjectId(id) : id
  engagementCheck(engagementID, userID, (err, oneEngagement) => {
    if (err) {
      return callback(err)
    }
    if (_.isEmpty(oneEngagement)) {
      return callback({ msg: 'No access to this engagement.'})
    }

    let todoStringIDs = _.map(oneEngagement.todos, (todo) => { return todo._id.toString() })
    if (!~_.findIndex(todoStringIDs, (oneTodoID) => { return oneTodoID === objID.toString() })) {
      return callback({ msg: 'The todo does not exist in the engagement.'})
    }

    return callback(null, oneEngagement)
  })

  // Models.Auvenir.Todo.findOne({ _id: objID }, (err, oneTodo) => {
  //   if (err) {
  //     return callback(err)
  //   }
  //   if (_.isEmpty(oneTodo)) {
  //     return callback({ msg: 'No Todo found.'})
  //   }

  //   engagementCheck(engagementID, userID, (err, oneEngagement) => {
  //     if (err) {
  //       return callback(err)
  //     }
  //     if (_.isEmpty(oneEngagement)) {
  //       return callback({ msg: 'No access to this engagement.'})
  //     }

  //     let todoStringIDs = _.map(oneEngagement.todos, (tID) => { return tID.toString() })
  //     if (!~_.findIndex(todoStringIDs, id.toString())) {
  //       return callback({ msg: 'The todo does not exist in the engagement.'})
  //     }

  //     return callback(null, { todo: oneTodo, engagement: oneEngagement })
  //   })
  // })
}

const requestCheck = (id, todoID, engagementID, userID, callback) => {
  debug('Running Request access check ...')
  if (_.isNull(id) || _.isUndefined(id)) {
    return callback('Access Control model ID is missing.')
  }
  if (_.isNull(userID) || _.isUndefined(userID)) {
    return callback('Access Control user ID is missing.')
  }
  if (_.isNull(todoID) || _.isUndefined(todoID)) {
    return callback('Access Control todo ID is missing.')
  }
  if (_.isNull(engagementID) || _.isUndefined(engagementID)) {
    return callback('Access Control user ID is missing.')
  }
  if (_.isNull(callback) || _.isUndefined(callback)) {
    return callback('Access Control callback function is missing.')
  }

  let objID = (_.isString(id)) ? ObjectId(id) : id

  todoCheck(todoID, engagementID, userID, (err, engagement) => {
    if (err) {
      callback(err)
    }

    let requests = _.flatMap(engagement.todos, (todo) => { return todo.requests })
    let requestStringIDs = _.map(requests, (request) => { return request._id.toString() })
    if (!~_.findIndex(requestStringIDs, (oneRequestID) => { return oneRequestID === objID.toString() })) {
      return callback({ msg: 'No Request found in the Todo'})
    }

    return callback(null, engagement)
  })

  // Models.Auvenir.Request.findOne({ _id: objID }, (err, oneRequest) => {
  //   if (err) {
  //     return callback(err)
  //   }
  //   if (_.isEmpty(oneRequest)) {
  //     return callback({ msg: 'No Request found.' })
  //   }

  //   todoCheck(todoID, engagementID, userID, (err, result) => {
  //     if (err) {
  //       callback(err)
  //     }
  //     let { todo, engagement } = result
  //     let requestStringIDs = _.map(todo.requests, (rID) => { return rID.toString() })
  //     if (!~_.findIndex(requestStringIDs, objID.toString())) {
  //       return callback({ msg: 'No Request found in the Todo'})
  //     }

  //     return callback(null, { request: oneRequest, todo, engagement })
  //   })
  // })
}

/**
 * Runs an access control check against a file object and returns the result.
 * @param {ObjectId} id       - The model ObjectId
 * @param {ObjectId} userID   - The userID to check against the object's acl.
 * @param {Function} callback -
 */
const fileCheck = (id, engagementID, userID, callback) => {
  info('Running File access check ...')
  if (_.isNull(id) || _.isUndefined(id)) {
    return callback('Access Control file ID is missing.')
  }
  if (_.isNull(userID) || _.isUndefined(userID)) {
    return callback('Access Control user ID is missing.')
  }
  if (_.isNull(engagementID) || _.isUndefined(engagementID)) {
    return callback('Access Control engagement ID is missing.')
  }
  if (_.isNull(callback) || _.isUndefined(callback)) {
    return callback('Access Control callback function is missing.')
  }

  var objID = (typeof id === 'string') ? ObjectId(id) : id
  async.waterfall([
    (cbk) => {
      info('Checking the engagement access')
      engagementCheck(engagementID, userID, (err, engagement) => {
        if (err) {
          return cbk(err)
        }
        cbk(null, engagement)
      })
    },
    (engagement, cbk) => {
      info('Grabbing the file object')
      Models.Auvenir.File.findOne({ _id: objID }, (err, oneFile) => {
        if (err) {
          cbk(err)
        } else if (!oneFile) {
          cbk({ msg: 'No file was found with this id.' })
        } else {
          cbk(null, engagement, oneFile)
        }
      })
    },
    (engagement, file, cbk) => {
      info('Check if the file belongs to the engagement.')
      let found = false
      engagement.files.forEach((oneFile, i) => {
        if (oneFile.toString() === file._id.toString()) {
          found = true
        }
        if (i === engagement.files.length - 1) {
          if (found) {
            return cbk(null, file, engagement)
          } else {
            return cbk({ msg: 'The engagement does not own the file.'})
          }
        }
      })
    }
  ], (err, file, engagement) => {
    if (err) {
      error({ err })
      return callback('Error occured while retrieving objects for access analysis.')
    } else {
      callback(null, {file, engagement})
    }
  })
}

module.exports = {
  user: userCheck,
  business: businessCheck,
  firm: firmCheck,
  engagement: engagementCheck,
  todo: todoCheck,
  request: requestCheck,
  file: fileCheck
}
