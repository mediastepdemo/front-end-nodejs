
const _ = require('lodash')
const ObjectID = require('mongodb').ObjectID

module.exports = {
  isObjectIDString: isObjectIDString,
  isObjectID: isObjectID,
  toObjectID: toObjectID,
  toArray: toArray,
  toString: toString,
  toTrimmedLowerCaseString: toTrimmedLowerCaseString,
  toSuperTrimmedString: toSuperTrimmedString,
  isInteger: isInteger,
  isNullOrUndefined: isNullOrUndefined,
  printable: printable,
  printableJSON: printableJSON
}

const CHECK_FOR_HEX_REG_EXP = new RegExp('^[0-9a-fA-F]{24}$')

function isObjectIDString (id) {
  return typeof id === 'string' && id.length === 24 && CHECK_FOR_HEX_REG_EXP.test(id)
}

function isObjectID (id) {
  if (id) {
    if (isObjectIDString(id)) return true
    if (typeof id.toString === 'function' && isObjectIDString(id.toString())) return true
  }
  return false
}

function toObjectID (ref) {
  if (ref instanceof ObjectID) return ref
  if (_.isObject(ref)) {
    const id = ref._id
    if (id instanceof ObjectID) return id
    if (isObjectIDString(id)) return ObjectID(id)
    if (typeof id === 'string') return id
  }
  if (isObjectIDString(ref)) return ObjectID(ref)
  return ref
}

function toArray (value) {
  if (_.isArray(value)) return value
  if (value === undefined || value === null) return []
  return [value]
}

function toString (value) {
  if (typeof value === 'string') return value
  if (value === undefined || value === null) return ''
  return '' + value
}

function toTrimmedLowerCaseString (value) {
  if (value === undefined || value === null) return ''
  if (typeof value !== 'string') value = '' + value
  return _.trim(value).toLowerCase()
}

function toSuperTrimmedString (value) {
  if (value === undefined || value === null) return ''
  if (typeof value !== 'string') value = '' + value
  return _.filter(value.split(' '), (p) => { return p !== '' }).join(' ')
}

function isInteger (value) {
  return (('' + value).search(/^-?[0-9]+$/) === 0)
}

function isNullOrUndefined (value) {
  return value === null || value === undefined
}

/**
 * Used to trace out the high level structure of __ during debugging.
 *
 * @param value (Object) Object who's structure we're trying to discern.
 * @param root (String) Prefixed to any output lines.
 * @param depth (int) How many levels deep should we show.
 * @returns (String) A text dump of the object property name hierarchy.
 */
function printable (value, root, depth) {
  if (!depth) {
    depth = 3
  }
  if (depth > 0) {
    depth--
    if (typeof (value) === 'object') {
      var output = ''
      for (var member in value) {
        output += printable(value[member], root + '.' + member, depth)
      }
      return output
    }
  }
  var type = typeof (value)
  if (type === 'function') return ''
  if (type === 'object') value = '[object]'
  return root + ' => ' + value + '\n'
}

function printableJSON (json, root, depth) {
  if (typeof json === 'string') {
    try {
      const obj = JSON.parse(json)
      return printable(obj, root, depth)
    } catch (e) {
      return printable(json, root, depth)
    }
  }
  return printable(json, root, depth)
}
