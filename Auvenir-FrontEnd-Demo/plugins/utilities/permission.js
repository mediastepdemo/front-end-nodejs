'use strict'

var Models = require('../../models')
var _ = require('lodash')
var async = require('async')
var initLogger = require('../logger/logger').init
const { info, warn, error } = initLogger(__filename)
const debug = require('debug')('auvenir:routeHelper')
const definePermission = require('../../public/js/src/definePermission')

/**
 * Require: import UserPermission, { Permissions } from '../utilities/permission.js'
 * Construct: let userPermission = new UserPermission(user, engagement)
 * Usages: if (userPermission.hasPermission(Permissions.FILE_W)) { updateFile(file) }
 */
module.exports = class UserPermission {
  constructor (user, engagement, cb) {
    info('Running constructor UserPermission')
    debug('Running constructor UserPermission')
    const self = this
    this.acl = {
      role: '',     // engagement.acl.role
      lead: false,  // engagement.alc.lead
      admin: false, // firms.acl.admin
      bAdmin: false, // businesses.acl.admin
      type: ''      // user.type
    }
    async.parallel({
      firm: function (cbk) {
        if (~['CLIENT', 'ADMIN'].indexOf(user.type)) { // client and admin doesn't have firms
          return cbk(null)
        }
        Models.Auvenir.Firm.findOne({acl: {$elemMatch: {id: user._id}}}, function (err, firm) {
          if (err) {
            return cbk(err)
          }
          if (firm) {
            firm.acl.forEach(function (item) {
              if (item.id.toString() === user._id.toString()) {
                self.acl.admin = item.admin
                return
              }
            })
          }
          return cbk(null, firm)
        })
      },
      business: function (cbk) {
        if (~['AUDITOR', 'ADMIN'].indexOf(user.type)) { // auditor and admin doesn't have business
          return cbk(null)
        }
        Models.Auvenir.Business.findOne({acl: {$elemMatch: {id: user._id}}}, function (err, business) {
          if (err) {
            return cbk(err)
          }
          if (business && business.acl) {
            business.acl.forEach(function (item) {
              if (item.id.toString() === user._id.toString()) {
                self.acl.bAdmin = item.admin
                return false
              }
            })
          }
          return cbk(null, business)
        })
      },
      engagements: function (cbk) {
        if (engagement && engagement.acl) {
          engagement.acl.forEach(function (item) {
            if (item.id.toString() === user._id.toString()) {
              self.acl.role = item.role
              self.acl.lead = item.lead
            }
          })
        }
        return cbk(null, engagement)
      }
    }, function (err, results) {
      if (err) cb(true)
      cb(results)
    })
  }

  canSeeAllClientTodo () {
    return (this.isAdminClient() || this.isLeadClient())
  }

  canSeeAllTodo () {
    /**
     * TODO:
     * if user is admin: return true
     * if user is lead: return true
     */

    return (this.isAdminAuditor() || this.isLeadAuditor())
  }

  hasPermissionInTodo (permission, todo) {
    /**
     * TODO:
     * if user is Auditor & is not lead & user is not assigned to Auditor assignee: return false
     * if user is Client & is not lead & user is not assigned to Client assignee: return false
     * else: return this.permissions.includes(permission)
     */
    var result = (this.getPermissionInTodo(todo).indexOf(permission) !== -1)

    info('call hasPermissionInTodo - permission: ' + permission + ' result: ' + result)

    return result
  }

  getPermissionInTodo (todo) {
    info('todo')
    info(todo)
    var permission = []
    if (this.isAdminAuditor() === true) {
      permission = definePermission.Roles.ADMIN_AUDITOR
    }
    if (this.isLeadAuditor() === true) {
      permission = permission.concat(definePermission.Roles.LEAD_AUDITOR)
    }
    if (this.isAuditor(todo) === true) {
      warn('running isAuditor')
      permission = permission.concat(definePermission.Roles.AUDITOR)
    }
    if (this.isAdminClient() === true) {
      permission = permission.concat(definePermission.Roles.ADMIN_CLIENT)
    }
    if (this.isLeadClient() === true) {
      permission = permission.concat(definePermission.Roles.LEAD_CLIENT)
    }
    if (this.isClient(todo) === true) {
      permission = permission.concat(definePermission.Roles.CLIENT)
    }

    info('call getPermissionInTodo')
    info(permission)

    return permission
  }

  isAdminAuditor () {
    var acl = this.acl
    info(this.acl)
    var result = (acl.admin === true && acl.role.toUpperCase() === 'AUDITOR')
    info('isAdminAuditor: ' + result + '\n')
    return result
  }

  isLeadAuditor () {
    var acl = this.acl
    var result = (acl.lead === true && acl.role.toUpperCase() === 'AUDITOR')
    info('isLeadAuditor: ' + result + '\n')
    return result
  }

  isAuditor (todo) {
    var result = false
    if (todo.auditorAssignee) {
      var acl = this.acl
      result = (todo.auditorAssignee.toString() === acl._id.toString() &&
      acl.lead === false && acl.role.toUpperCase() === 'AUDITOR')
    }
    error('IS AUDITOR')
    error(result)
    info('isAuditor: ' + result + '\n')
    return result
  }

  isAdminClient () {
    var acl = this.acl
    var result = (acl.admin === true && acl.role.toUpperCase() === 'CLIENT')
    info('isAdminClient: ' + result + '\n')
    return result
  }

  isLeadClient () {
    var acl = this.acl
    var result = (acl.lead === true && acl.role.toUpperCase() === 'CLIENT')
    info('isLeadClient: ' + result + '\n')
    return result
  }

  isClient (todo) {
    var result = false
    if (todo.clientAssignee) {
      var acl = this.acl
      result = (todo.clientAssignee.toString() === acl._id.toString() &&
      acl.lead === false && acl.role.toUpperCase() === 'CLIENT')
    }
    info('isClient: ' + result + '\n')
    return result
  }
}
